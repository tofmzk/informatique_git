# Exercices sur les dictionnaires

#### Exercice : Agenda

Dans un agenda, un contact est modélisé par un dictionnaire. La clé du dictionnaire est le nom des contacts. La valeur du dictionnaire est un tuple dont la première valeur est leur âge (type int) et la seconde leur ville natale (str).

Voici une petite liste de contact :

* Pierre a 37 ans et est né à Lille.
* Zoé a 21 ans et est née à Calais.
* Ali a 54 ans et est né à Bordeaux.
* Aymeric a 72 ans et est né à Lorient.

Créer un nouveau programme `agenda.py`.



1. Définissez le dictionnaire de contacts comme variable globale dans votre programme.

2. a. Donner la commande pour accéder à l'âge de Zoé

    b. Donner la commande pour accéder à la ville natale d'Ali.

    c.  Donner la commande pour décider si 'Jean' est dans le dictionnaire ou pas.

3. Définissez et codez une fonction `est_present(dic, nom)` qui renvoie `True` si le _nom_ est présent dans le dictionnaire _dic_ e t`False` sinon.

4. Définissez et codez une fonction `renvoyer_age(dic, nom)` qui, si le _nom_ est présent dans le dictionnaire _dic_, renvoie son âge.

5. Définissez et codez une fonction `renvoyer_ville(dic, nom)` qui, si le _nom_ est présent dans le dictionnaire _dic_, renvoie sa ville natale.

6. Définissez une fonction `renvoyer_nom(dic, ville)` qui renvoie un tableau de tous les noms dont la ville de naissance est _ville_.

7. Définissez et codez une fonction `ajouter(dic, nom, age, ville)` qui, si le _nom_ est n'est pas présent dans le dictionnaire _dic_, l'y ajoute.

 



#### Exercice : le SCRABBLE



Voici un descriptif des valeurs des lettres au scrabble français. 

- 0 point : **Joker** ×2 (appelés en français *jokers* ou *lettres blanches*)
- *1 point* : **E** ×15, **A** ×9, **I** ×8, **N** ×6, **O** ×6, **R** ×6, **S** ×6, **T** ×6, **U** ×6, **L** ×5
- *2 points* : **D** ×3, **M** ×3, **G** ×2
- *3 points* : **B** ×2, **C** ×2, **P** ×2
- *4 points* : **F** ×2, **H** ×2, **V** ×2
- *8 points* : **J** ×1, **Q** ×1
- *10 points* : **K** ×1, **W** ×1, **X** ×1, **Y** ×1, **Z** ×1



> * Définissez un dictionnaire _score_ qui prend les lettres majuscules en clés et leur score en valeur
> * Réalisez une fonction `calculer_score `qui prend une chaîne de caractère _mot_ en paramètre et renvoie le score du mot s'il est possible de l'écrire avec les lettres du scrabble et `None` sinon.
>
> _Remarques :_
>
> * Documentez la fonction
> * Réalisez deux Doctests.



Pour vous aider, voici un premier dictionnaire

```python
valeur_lettre = { 'A' : 1,
                'B' : 2,
                'C' : 2,
                'D' : 2,
                'E' : 1,
                'F' : 4,
                'G' : 2,
                'H' : 4,
                'I' : 1,
                'J' : 8,
                'K' : 10,
                'L' : 1, 
                'M' : 2,
                'N' : 1,
                'O' : 1,
                'P' : 3,
                'Q' : 8,
                'R' : 1, 
                'S' : 1,
                'T' : 1,
                'U' : 1,
                'V' : 4,
                'W' : 10,
                'X' : 10,
                'Y' : 10,
                'Z' : 10
                }        

```

________

Par Mieszczak Christophe

Licence CC BY SA
 # Les tableaux indexés 

## késako



Un `tableau indexé` est une _structure de données linéaire_ qui peut contenir des `éléments` (ou `items`) de types quelconques (variables, chaînes de caractères, tableaux indexés, …) placés les uns derrières les autres (c'est à dire de façon _séquentielle_) et repérés par un indice, en commençant par l'indice 0. 

En Python, l'implémentation de ces tableaux est le type _`list`_ .



On définit un tableau ainsi : 

```python
>>> tab = [0, 2, 4, 6, 8, 10, 12]
>>> type(tab)
???
```



On peut accéder aux éléments du tableau via l'`index` ou `indice` des éléments (d'où le nom `tableau indexé`):

```Python
>>> tab[0]
???
>>> tab[1]
???
>>> tab[6]
???
>>> tab[7]
???
```

l'instruction `len` renvoie le nombre d'`éléments` (ou `items`) du tableau qu'on appelle sa taille :

```python
>>> len(tab)
???
```

> Quel est l'indice du dernier élément d'un tableau :
>
> * de taille 3 ?
> * de taille 10 ?
> * de taille _n_ ?



Un tableau peut contenir autre chose que des entiers ou des flottants. Continuons dans la console.

```Python
>>> inventaire = [["tomate", 3],["salade", 4],["concombre", 1],["poivron", 0]]
>>> inventaire[0]
???
>>> len(inventaire)
???
>>> inventaire[0][1]
???
>>> ["tomate", 3][1]
???
>>> notes = [[10, 2],[13, 1]]
>>> len(notes)
???
>>> notes[0]
???
>>> notes[0][1]
???

```

> **Exercice :**
> Tapez dans la console la commande permettant de calculer la moyenne de la série de notes  du tableau _notes_ défini plus haut.
>
> ```python
> >>> ???
> ???
> ```
>
> 



## Parcourir un tableau

Un tableau possède un _itérateur_ qui permet de le parcourir de son premier _terme_ à son _dernier_.

Il y a différentes façon de procéder. Dans la console, testez et complétez la réponse :

```Python
>>> tab = [0, 2, 4, 6, 8, 10, 12]
>>> for i in range(len(tab)):
    		print(tab[i]) 
???
>>> for element in tab:
    		print(element)
???
>>> inventaire = [["tomate", 3],["salade", 4],["concombre", 1],["poivron", 0]]
>>> for element in inventaire :
   		print(element)
???
>>> for element in inventaire[0]:
    	print(element)
??? 
          
```

> **Exercice :**
>
> Ecrivez un code permettant de parcourir un tableau grâce à l'index _i_ de ses éléments en utilisant une boucle `tant que`.
>
> 

## In or not in that is the question



Testez dans la console et complétez :

```python
>>> 2 in [1, 2 , 'a', 4]
???
>>> a in [1, 2 , 'a', 4]
???
>>> 'a' in [1, 2 , 'a', 4]
???
>>> 5 not in [1, 2 , 'a', 4]
???
>>> 2 in [[12,2], [13, 3]]
???
>>> [13, 3] in [[12,2], [13, 3]]
???
```





## Choisir au hasard un élément dans un tableau



Le module `random` offre de nombreuses [fonctions](http://python-simple.com/python-modules-math/random.php). En ce qui concerne les tableaux, l'une d'elle est intéressante : `random.choice(tab)` renvoie un élément de _tab_ choisi au hasard de façon équiprobable parmi ses éléments.



Testons cela :

```Python
>>> import random
>>> random.choice(['pile', 'face'])
???
>>> random.choice(['pile', 'face'])
???
>>> random.choice(['pile', 'face'])
???
>>> random.choice(['pile', 'face'])
???
>>> random.choice(['pile', 'face'])
???
```



## Méthodes des tableaux

Les méthodes des tableaux sont des fonctions qui s'appliquent sur des tableaux.

### Ajouter une valeur à un tableau

C'est **LA** méthode à connaître ! 

On peut ajouter des valeurs après la création du tableau avec la méthode ` append ` (qui signifie "ajouter" en anglais): 

```Python
>>> tab = []
>>> tab.append(1)
>>> tab
???
>>> tab.append(9)
>>> tab
???
```

 _Remarque :_

On peut également ajouter un élement en utilisant l'opérateur `+` :

```python
>>> tab = tab + [5]
>>> tab
???
>>> tab2 = [1, 2, 3]
>>> tab = tab + tab2
>>> tab
???
```

Mais il ne faut pas confondre la méthode `append` et l'opérateur `+` :

* `append` ne renvoie rien mais modifie le tableau sur lequel il s'applique
* `+` renvoie un nouveau tableau mais ne modifie pas les tableaux qu'il a concaténés.

```python
>>> tab + [12]
???
>>> tab
???
>>> tab.append(12)
>>> tab
???
```



###  Supprimer une entrée avec son index 

 Il est parfois nécessaire de supprimer une entrée du tableau. Pour cela vous pouvez utiliser la fonction ` del `  :

```python
>>> tab = ["a", "b", "c"]
>>> del tab[1]
>>> tab
???
```



### Supprimer un élément avec sa valeur 

 Il est possible de supprimer une entrée avec sa valeur avec la méthode ` remove ` . C'est toujours la première occurrence de la valeur qui est supprimée. Si l'élément n'est pas présent dans le tableau, une exception (une erreur) est déclenché.

```python
>>> tab = ["a", "b", "c", "a"]
>>> tab.remove("a")
>>> tab
???
>>> tab.remove('d')
???
```



###  Inverser l'ordre des éléments d'un tableau

 Vous pouvez inverser les items d'un tableau  avec la méthode ` reverse ` . 

```python
>>> tab = ["a", "b", "c"]
>>> tab.reverse()
>>> tab
???
```



### Compter le nombre d'items d'un tableau

 Il est possible de compter le nombre d'items d'un tableau avec la fonction ` len ` . 

```python
>>> tab = [1, 2, 3, 5, 10]
>>> len(tab)
???
>>> inventaire = [["tomate", 3],["salade", 4],["concombre", 1],["poivron", 0]]
>>> len(inventaire)
???
>>> len(inventaire[0])
???
```



###  Compter le nombre d'occurrences d'un élément

 Pour connaître le nombre d'occurrences (le nombre d'apparitions) d'une valeur dans un tableau, vous pouvez utiliser la méthode ` count ` :

```python
>>> tab = ["a","a","a","b","c","c"]
>>> tab.count("a")
???
>>> tab.count("c")
???
```



### Trouver l'index d'un élément

 La méthode ` index ` vous permet de connaître la position de l'item cherché. Seul le premier index est renvoyé s'il y en a plusieurs. 

```python
>>> tab = ["a", "a", "a", "b", "c", "c", "b"]
>>> tab.index("b")
???
```



### Trouver le maximum/minimum d'un tableau

La fonction `max` dont le paramètre est un tableau, renvoie sa valeur maximale. la fonction `min` renvoie le minimum en utilisant un comparateur adapté au type de la valeur (ordre numérique, alphabétique ...).

```python
>>> tab= [1, 2, 3, 5, 10]
>>> max(tab)
???
>>> min(tab)
???
>>> tab = ['a', 'b','e','x','f']
>>> max(tab)
???
>>> min(tab)
???
```



## Générer un tableau

Il existe différentes façons de générer un tableau.

On peut générer une liste vide puis utiliser des boucles et la méthode `append` :

```python
def generer_tab(n) :
	tab = []
	for i in range(n) :
    	tab.append(i)
    return tab
 
```



Voici d'autres techniques :

```python
>>> tab = list(range(0, 10))
>>> tab
???
>>> tab = list('bonjour')
>>> tab 
???
>>> tab = [0] * 10
>>> tab
???
>>> tab = ['Hello'] * 5
>>> tab
???

```



On peut également utiliser une génération dite _par compréhension_ :

```python
>>> tab = [2 * i for i in range(0, 3)]
>>> tab
???
>>> tab1= [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
>>> tab2 = [nbre for nbre in tab1 if nbre % 2 == 0]
>>> tab2
???
```





## Mutabilité



#### Avec Python Tutor



Suivez ce lien vers [Python tutor](http://pythontutor.com/visualize.html#mode=edit) qui va nous aider à comprendre ce qui se passe en mémoire lorsqu"on travaille avec des tableaux et les conséquences qui en découlent.

* Commençons avec deux variables de type _int_ a et b  puis cliquez sur le bouton `Visualize Execution` 

    ![l](media\pythontutor1.jpg)

    

* Cliquez sur le bouton `Next` pour exécuter le code pas à pas.

    ![python tutor 2](media\pythontutor2.jpg)



> A la fin de ce code :
>
> * la variable a vaut ...
> * la variable b vaut .... : changer la valeur de a n'influe pas sur celle de b.



* Passons maintenant aux tableaux en prenant le code ci-dessous :

    ![python tutro 3](media\pythontutor3.jpg)

    

* Exécutez de nouveau le code pas à pas.

    

    > A la fin de ce code :
    >
    > * Que vaut ma_liste ?
    > * Que vaut copie_de_ma_liste ?
    >
    > Modifier ma_liste modifie également la copie !!
    >
    > On dit que les tableaux dynamiques de Python sont **MUTABLES**. 

    

#### Illustration de la mutabilité avec une fonction : l'effet de bord.

Considérons la fonction ci-dessous qui prend une liste en paramètre :

```Python
def test(tab):
    '''
        double toutes les valeurs du tableau
        : param tab (list) contient des entiers
        return (list) tableau modifié
        doctest:
        >>> test([1, 2, 3])
        [2, 3, 6]
    '''
    for i in range(len(tab)) :
        tab[i] = tab[i] * 2
    return liste
```



> Qu'est-ce que cette fonction est sensée faire ?



Copiez ce code dans Thonny et testez la fonction depuis la console :

```python
>>> test( [1, 2, 3])
???
```



Procédons un peu différemment :

```Python
>>> tab = [1, 2 ,3]
>>> test(tab)
???
>>> tab
???
```



> Il y a **un effet de bord** sur tab : il est modifié par la fonction !



Si on souhaite cet effet de bord, on le précise dans le Docstring. Ici le `return` devient alors inutile.

```python
def test(tab):
    '''
        double toutes les valeurs du tableau tab
        : param tab (list) contient des entiers
        : effet de bord sur tab
        : pas de return
        doctest:
        >>> test([1, 2, 3])
        [2, 3, 6]
    '''
    for i in range(len(tab)) :
        tab[i] = tab[i] * 2
    
```



Depuis la console :

```Python
>>> tab = [1, 2 ,3]
>>> test(tab)
>>> tab
???
```



_Remarque importante :_

Le `paradigme fonctionnel` est un mode de programmation dans lequel on ne doit utiliser que des variables locales dans les fonctions et ne faire aucun effet de bord.  Les variables passées en paramètre ne doivent en aucun cas être modifiées par les fonctions.

Ainsi, il arrive souvent que l'effet de bord ne soit pas souhaité.

Pour éviter cet effet de bord, le plus souvent, on définit un tableau vide qu'on construit on fur et à mesure à partir du tableau passé en paramètre.



Regardez le code ci-dessous :

```python
def test(tab):
    '''
        double toutes les valeurs du tableau tab
        : param tab (list) contient des entiers
        : pas d'effet de bord sur tab
        : pas de return
        doctest:
        >>> test([1, 2, 3])
        [2, 3, 6]
    '''
    tab_2 = []
    for nbre in tab :
        tab_2.append(nbre * 2)
    return tab_2
```

Testez depuis la console :

```python
>>> tab = [1, 2, 3]
>>> test(tab)
???
>>> tab
???
```



On peut également copier un tableau et modifier son double. Voyons comment ça marche juste après ...



## Copier un tableau

La méthode la plus simple est la suivante :

```Python
>>> tab = [1, 2, 3]
>>> ma_copie = tab[:]
>>> tab[0] = 2
>>> tab
???
>>> ma_copie
???
```



Mais cette méthode n'est pas _récursive_ : Si le tableau contient des tableaux, cela ne marche plus !

```Python
>>> tab = [[1,2], 2, 4]
>>> ma_copie = tab[:]
>>> tab[0][0] = 2
>>> tab
???
>>> ma_copie
???

```



Pour que la copie soit récursive, il faut utiliser le module `copy` :

```Python
>>> tab = [[1,2], 2, 4]
>>> import copy
>>> ma_copie = copy.deepcopy(tab)
>>> tab[0][0] = 2
>>> tab
???
>>> ma_copie
???

```



_Remarque :_

Faire une copie de tableau a un coût en temps d'exécution et en ressource mémoire. N'oublions pas qu'on peut travailler sur des tableaux immenses ! Aussi, on ne le fait que si cela est nécessaire.



> **Exercice :**
>
> Reprenez la fonction `test` et modifiez la pour qu'elle renvoie un tableau sans effet de bord sur le paramètre grâce à une copie.





_______

Par Mieszczak Christophe CC BY SA

_sources images : impressions d'écrans personnels_


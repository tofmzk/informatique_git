 # Les Tuples


## Une tableau ... non mutable	



Un tuple ressemble furieusement à un tableau... avec quelques différences notables.



* Comme un tableau, on accéde à ses valeurs avec des crochets.... Mais on le définit avec des parenthèses.

    ```python
    >>> mon_tuple = (2, 3, 9, 1)
    >>> mon_tuple [0]
???
    >>> len(mon_tuple)
    ???
    ```
    
    

* Une fois un tuple défini, on ne peut plus les modifier ! Cette particularité rend le tuple non mutable. Passé en paramètre dans une fonction, cette dernière ne pourra le modifier via un effet de bord.

  ```python
  >>> mon_tuple[1] = 2	
  ???
  ```

* La non mutabilité d'un tuple lui permet d'être utilisé comme clé dans un dictionnaire, contrairement à un tableau pour qui c'est impossible.

    ```python
    >>> mon_tuple = (1, 2)
    >>> mon_tab = [1, 2]
    >>> mon_dict = {}
    >>> mon_dict[mon_tuple] = 1
    >>> mon_dict
    ???
    >>> mon_dict[mon_tab] = 1
    ???
    ```

    

* On utilise souvent des tuples sans forcément s'en rendre compte !
  
	* L'utilisation des tuples est vraiment pratique pour faire des permutations. Ci-dessous `a,b` et `b, a` sont des tuples :
  
    ```python
        >>> a = 2
        >>> b = 3
        >>> a, b = b, a
        >>> a 
        ???
        >>> b
        ???
    ```
   * Considérons la fonction ci-dessous. Où utilise-t-on un tuple ?

        ```python
        def ordonne(a,b) :
            if a < b :
                a, b = b, a
            return a, b
            
        ```
  

_______

Par Mieszczak Christophe CC BY SA

Licence CC BY SA
 # Les dictionnaires



## Des Clés et des Valeurs



Un dictionnaire est structure de données linéaire qui stocke des éléments **non ordonnés** et référencés non pas par des indices entiers, comme les _tableaux ou les _tuples_, mais par des `clés` qui peuvent être de _presque_ tout type. On les déclare entre accolades mais, comme les _tableaux_ et les _tuples_, on utilise des crochets pour accéder aux valeurs.

```python
>>> mon_dict = {}
>>> type(mon_dict)
???
>>> mon_dict['carottes'] = 12
>>> mon_dict['poivrons'] = 4
>>> mon_dict
???
>>> mon_dict['carottes']
???
```

Certains types ne peuvent servir de clés. En fait, une injection (une clé est en _relation_ avec une et une seule valeur) doit être crée entre la clé et la valeur correspondante. On dit qu'on définit une _table de Hashage_. Cela n'est pas possible si la clé est _mutable_, comme un tableau par exemple.

```python
>>> tab = [1, 2]
>>> mon_dict[tab] = 'ca ne marchera pas'
???
```





On peut également le déclarer directement ainsi :

```python
>>> mon_dict = { 'carottes' : 12, 
                 'poivrons' : 4,
                 'tomates'  : 13
               }

```



Dans le dictionnaire _mon_dict_ :

* 'carottes', 'poivrons' et 'tomates' sont des `clés`
* 12, 4 et 13 sont les `valeurs` de ces clés
* le couple `(clé, valeur)` et un `item` du dictionnaire.



## Parcourir un dictionnaire

Bien qu'il ne soit pas ordonné, on peut parcourir un dictionnaire. On peut même le parcourir de plusieurs façons. On reprend dans la suite le dictionnaire `mon_dict` défini plus haut.



* Par clé : le parcours par défaut. On peut préciser tout de même que l'on parcours le dictionnaire selon les clés en ajoutant la méthode `keys()`:

    ``` python
    >>> for cle in mon_dict :
        	print(cle)
    ???
    >>> for cle in mon_dict.keys():
        	print(cle)
    ???
    ```

    

* Par valeurs, grâce à la méthode `values()`

    ```python
    >>> for valeur in mon_dict.values():
        	print(valeur)
    ???
    ```

* On peut également utiliser la méthode `items()` qui renvoie les couples (clé, valeur) du dictionnaire :

    ```python
    >>> for couple in mon_dict.items() :
        print(couple)
    ???
    >>> for cle, valeur in mon_dict.items() :
        	print(cle, ':', valeur)
    ???
    ```

    

## Quelques méthodes 

Comme les tableaux, les dictionnaires ont quelques méthodes intéressantes.

### Vérifier la présence d'une clé

On utilise l'instruction `in`  pour vérifier la présence d'une clé dans un dictionnaire.

```python
>>> 'carrotes' in mon_dict.keys()
???
>>> 'oignons' in mon_dict.keys()
???
```



### Supprimer une entrée via sa clé

On peut, comme avec une liste, utiliser la fonction  `del` :

``` python
>>> mon_dict = { 'carottes' : 0, 'salades' : 4}
>>> del mon_dict['carottes']
>>> mon_dict
???
```



### Fusionner deux dictionnaires

La méthode `update` permet de fusionner deux dictionnaires

```python
>>> mon_dict = { 'carottes' : 1, 'salades' : 4}
>>> mon_dict_2 = { 'poivrons' : 8, 'tomates' : 2}
>>> mon_dict.update(mon_dict_2)
>>> mon_dict
???
```









___________

Par Mieszczak Christophe CC BY SA

Licence CC BY SA
# Exercices sur les tableaux dynamiques

#### Exercice : parcourir un tableau

Chaque fonction sera documentée et présentera les assertions nécessaires. On n'utilisera pas ici les méthodes des tableaux existantes.

1. Coder un prédicat `est_present(element, tab)` qui renvoie `True` si l'élément et présent dans la liste et `False` sinon.

2. Réaliser une fonction `maximum(tab)` de paramètre _tab_, un tableau de nombres entiers, qui renvoie la valeur maximale du tableau.
3. Réaliser une fonction `renverser(tab)`  qui renvoie un tableau dont les éléments sont rangés dans l'ordre inverse.
4. Réaliser une fonction `compter(tab, element)` qui renvoie le nombre d'occurrence de l'_élément_ dans le tableau _tab_.
5. Réaliser une fonction `trouve_index(tab, element)` qui renvoie l'index de la première occurrence de _element_ dans le tableau.



#### Exercice : Générer des tableaux

1. Coder la fonction `generer_tab(n)` de paramètre un entier positif, qui renvoie un tableau contenant tous les nombres entier de 0 inclus à n inclus.
2. Coder la fonction `generer_tab_pair(n)` de paramètre un entier positif, qui renvoie un tableau contenant tous les nombres pairs de 0 inclus à n inclus.
3. Coder la fonction `generer_tab_inverse(n)` de paramètre un entier positif, qui renvoie un tableau contenant tous les nombres entiers négatifs de -n inclus à 0 inclus.
4. Réaliser une fonction `generer_tab_alea(n)`, de paramètre _n_, un entier positif, qui renvoie un tableau de _n_ nombres entiers choisis aléatoirement entre 0 et 100 inclus.



#### Exercice : nombres premiers

Un nombre est premier s'il a exactement deux diviseurs : 1 et lui-même. n dispose d'un tableau de n entiers positifs dont on ne veut garder que les nombres premiers. L'algorithme proposé ici n'est pas le meilleur... mai si lest simple à mettre en place.

Le premier nombre du tableau est 2 : il est premier et on va éliminer tous ses multiples strictement supérieur à 2. Le plus petit d'entre eux est 2².

Le second est 3 : il est premier et on va éliminer tous ses multiples  strictement supérieur à 3. Le plus petit d'entre eux et 3².

1. Trouver _à la main_ le 3 nombres premiers suivants.

2. Quel est le premier multiple d'un nombre entier donné lorsqu'on a déjà éliminer les multiples des nombres entiers le précédent ?

3. Coder la fonction `generer_tab_entiers(entier)` de paramètre un entier positif, qui renvoie un tableau contenant tous les nombres entier de 2 inclus à _entier_ inclus.

4. Coder la fonction `eliminer_multiple(n, tab)` de paramètres _n_, un entier positif, et tab, un tableau d'entiers positifs, qui enlève du tableau _tab_ tous les entiers multiples de n strictement supérieur à _n_.

5. Coder la fonction `generer_premier(n)` de paramètre _n_, un entier positif ,qui :

    * construit un tableau contenant tous les entiers de 2 à n inclus.

    * tant que _n²_ est inférieur à la longueur de tableau :
        * enlève les multiples de _n_ du tableau;
        * augmente _n_ de 1.

    * Pour finir,renvoie le tableau

#### Exercice : tableaux de valeurs 

1. Coder la fonction `f(x)` de paramètre un flottant, qui renvoie la valeur de _x_².

2. Coder la fonction `valeurs(a, b, n )` où _a_ et _b_ sont des flottants et _n_ un entier positif, qui renvoie un tableau de _n_ valeurs de `f(x)` pour _x_ allant de _a_ à _b_ avec un pas régulier. Les valeurs seront arrondies à $10^{-2 }$ près.

3. Coder la fonction `couples(a, b, n)`où _a_ et _b_ sont des flottants et _n_ un entier positif qui renvoie un tableau de tableau de _n_ éléments dont chaque élément est un couple du type (x, f(x)) pour _x_ allant de _a_ à _b_ avec un pas régulier. Les valeurs seront arrondies à $10^{-2 }$ près.

    

#### Exercice : ASCII ART

Pour dessiner en ascii art, en utilisant des caractères, on utilise le tableau _symboles = [' ', '(', ')' ,'/', 'C', '.', '"']_ dans lequel :

*  l'indice 0 correspond au caractère _espace_
*  l'indice 1 correspond au caractère ( 
*  l'indice 2 correspond au caractère )
*  l'indice 3 correspond au caractère /
*  l'indice 4 correspond au caractère C
*  l'indice 5 correspond au caractère .
*  l'indice 6 correspond au caractère "

Une image en ascii est alors codée dans un tableau de tableaux comme celui-ci :

_dessin_ = [[0, 0, 1, 2, 3, 2], [0, 0, 1, 5, 5, 2], [4, 1, 6, 2, 1, 6, 2]] 

* Chaque élément de _dessin_ est un tableau correspond à une ligne du dessin
* chaque chiffre correspond à un des symboles du tableau _symboles_.

1. Coder la fonction `dessiner_ligne(ligne)` qui affiche le dessin correspond au tableau ligne passé en paramètre. Par exemple, `dessiner_ligne([0, 0, 1, 2, 3, 2])` affichera ()/).

2. Coder la fonction `dessiner(tab)` qui affiche le dessin correspondant au tableau passé en paramètre. voici le début du code à compléter. Testez l'affichage en utilisant le tableau _dessin_.

3. On va mettenant encoder une image ascii art dans un tableau structuré comme _dessin_.

    a. Coder la fonction `indice(caractere)` où _caractere_ prend ses valeurs parmi les 7 caractères du tableau _symboles_ et qui renvoie l'indice du caractère dans le tableau _symboles_.

    b. Coder la fonction `encode(chaine)` qui renvoie le tableau correspondant au dessin de la _chaine_ passée en paramètre.



#### Exercice : Choix aléatoire 

Dans un tiroir , il y a 2 chaussettes rouges, 3 vertes et une bleue. 

1. Réaliser la fonction `simuler_chaussettes` de paramètre _n_, un entier, qui simule _n_ tirages aléatoires équiprobables successifs de 2 chaussettes avec remise et renvoie la fréquence de réussite de l'événement "les 2 chaussettes ont la même couleur".

​		On pourra utiliser le tableau `tiroir = ["R", "R", "V", "V", "V", "B"]` 

2. Même exercice mais sans remise.



#### Exercice  : Générer une liste

Donner un code python permettant de construire les tableaux suivants :

```python
1. tab1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, ..., 99]
2. tab2 = [2, 4, 6, 8, 10, 12, ..., 100]
3. tab3 = [99, 98, 97, 96, ..., 3, 2, 1]
4. tab4 = ['D', 'E', 'C', 'O', 'U', 'P', 'A', 'G', 'E']
5. tab5 = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]
6(**). tab6 = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97] 

```

### Exercice : comptine de _Pi_

Voici un extrait de la comptine de Maurice Decerf dédiée au nombre $\pi$ : 

_Que j'aime à faire apprendre ce nombre utile aux sages !
Immortel Archimède, artiste ingénieur,
Qui de ton jugement peut priser la valeur ?
Pour moi, ton problème eut de pareils avantages_

Dans ce  _poème_, le nombre de lettres de chaque mot correspond à une décimale de _pi_ :

| Que  | j    | aime | à    | faire | connaitre | ce   |
| ---- | ---- | ---- | ---- | ----- | --------- | ---- |
| 3    | 1    | 5    | 1    | 5     | 9         | 2    |



1. Coder la fonction `renvoyer_mot(chaine, i)` qui renvoie le mot écrit à partir de l'indice _i_ de la _chaine_ de caractères jusqu'au prochain espace ou la fin de la chaine.

2. Coder la fonction `renvoyer_tableau(chaine)` qui renvoie un tableau dont les éléments sont les mots contenus dans la _chaine_. 

3. Coder la fonction `renvoyer_tab_decimales(tab_mots)` qui renvoie un tableau contenant les décimales de _pi_ trouvées grâce à la comptine.

4. Combien de décimales de _pi_ connaissez-vous grâce à cette comptine ?

5. Codez une fonction `afficher_pi` qui affiche les décimale de _pi_ sont la forme ci-dessous :

    ```python
    3, 141 592 653 589 793 ....
    ```

    

##### Aide :

On stockera la comptine dans une chaine de caractères en enlevant toute la ponctuation. 

```python
comptine = 'Que j aime à faire apprendre ce nombre utile aux sages Immortel Archimède artiste ingénieur Qui de ton jugement peut priser la valeur Pour moi ton problème eut de pareils avantages Jadis mystérieux un problème bloquait Tout ladmirable procédé loeuvre grandiose Que Pythagore découvrit aux anciens Grecs Ô quadrature  Vieux tourment du philosophe Insoluble rondeur trop longtemps vous avez Défié Pythagore et ses imitateurs Comment intégrer l espace plan circulaire Former un triangle auquel il équivaudra nouvelle invention  Archimède inscrira Dedans un hexagone  appréciera son aire Fonction du rayon Pas trop ne s y tiendra Dédoublera chaque élément antérieur Toujours de l orbe calculée approchera Définira limite  enfin larc le limiteur De cet inquiétant cercle ennemi trop rebelle Professeur enseignez son problème avec zèle'
```





#### Exercice : parcourir un tableau de tableau

1. Coder la fonction `demande_valeur(m, p)` de paramètres deux entiers positifs avec _m_ < _p_ qui :

    * demande à l'utilisateur un réel entre _m_ et _p_ et, tant que la réponse n'est pas conforme, repose la question.
    * renvoie la valeur

2. Coder une fonction `entrer_notes(n)` de paramètre un entier positif _n_ qui, n fois de suite,  demande une note entre 0 et 20 puis un coefficient entre 0 et 4 et renvoie un tableau dont les élements sont eux même des tableaux du type [note, coefficient].

3. Coder la fonction `moyenne(tab)` dont le paramètre est un tableau de notes renvoyé par la fonction `entrer_notes`. La fonction `moyenne(tab)` renvoie la moyenne des notes présentes dans le tableau sans se préoccuper de leur coefficient.

4. Coder la fonction `moyenne(tab)` dont le paramètre est un tableau de notes renvoyé par la fonction `entrer_notes`. La fonction `moyenne(tab)` renvoie la moyenne des notes présentes dans le tableau en utilisant leur coefficient.

    

#### Exercice : Gestion d'un stock

Un stock est représenté par le tableau suivant : 

```python
stock = [['carotte', 10], ['tomate', 8], ['salade', 13],['navet`, 3]]
```

Dans les questions suivantes, _legume_ est du type _str_ et _nbre_ un entier positif. Vous rédigerez les Docstrings et mettrez les assertions nécessaires.

1. Définissez et codez le prédicat `est_present(stock,legume)` qui renvoie `True` si _legume_ est un légume du stock et `False` sinon.
2. Définissez et codez une fonction `renvoyer_stock(stock, legume)` qui renvoie le nombre de légumes de ce type disponibles s'il s'agit d'un légume du stock et 0 sinon.
3. Définissez et codez une fonction `nouveau_legume(stock,legume)` qui ajoute au stock le _legume_ passé en paramètre s'il n'est pas déjà présent dans le stock, avec un effectif de 0.
4. Définissez  et codez une fonction `ajouter(stock, legume, nbre)` qui, si le _legume_ passé en paramètre est présent dans le stock, augmente son effectif du _nbre_ passé en paramètre. 
5. Définissez  et codez une fonction `supprimer(stock, legume, nbre)` qui, si le _legume_ passé en paramètre est présent dans le stock et que le stock est suffisant, diminue son effectif du _nbre_ passé en paramètre. 





_______

Par Mieszczak Christophe

licence CC BY SA
# Traitement des données en table

Les sites web sont référencés dans des bases de données qu'utilisent le moteurs de recherche lorsque vous faites des requêtes, que vos données personnelles sont collectées en masse à chacun de vos _clics_ lorsque vous naviguez sur la toile, utilisez les réseaux sociaux, commandez un article en ligne, vous déplacez avec la géolocalisation de votre smartphone activée [...] que les sites marchands devaient référencer tous leurs articles et leurs caracéristiques,  que l'intelligence artificielle utilise des systèmes d'apprentissage qui nécessitent d'énormes quantité de données pour progresser, la recherche scientifique s'appuie la plupart du temps elle aussi sur des données ... la liste est sans fin !

**Les données sont partout, en quantité astronomique !!**



Quand on voit la difficulté qu'on peut avoir à gérer un simple tiroir mal rangé et rempli de chaussettes dépareillées, on peut se poser deux questions :

* Comment stocker/ranger ces données de manière efficace ?
* Comment les utiliser tout aussi efficacement ?



Il existe de nombreuses façon de faire cela. En terminale, en NSI, nous parlerons du langage SQL. En première, nous allons nous intéresser aux **données en table** que l'on stocke dans un tableau.

# Le format CSV



Le format `CSV`(`Comma-separated values`)est un format texte utilisé pour sauvegarder des données en utilisant une structure simple, en table.



Les fichiers `pokemon.csv`du TP se trouve dans le dossier `fournis`. Ouvrez le, **avec un éditeur de texte** (`Notepad++`, `Geany`, `Sublime text` ...)

Voici un extrait des données de ce fichier :

```csv
name;classification;attack;defense
Swablu;Cotton Bird Pokémon;40;60
Budew;Bud Pokémon;30;35
Minun;Cheering Pokémon;40;50
Metapod;Cocoon Pokémon;20;55
Chikorita;Leaf Pokémon;49;65
Pinsir;Stagbeetle Pokémon;155;120
Cinccino;Scarf Pokémon;95;60
Elgyem;Cerebral Pokémon;55;55
Foongus;Mushroom Pokémon;55;45
Ariados;Long Leg Pokémon;90;70
Aipom;Long Tail Pokémon;70;55
Accelgor;Shell Out Pokémon;70;40
Dialga;Temporal Pokémon;120;120
Starly;Starling Pokémon;55;30
Ledyba;Five Star Pokémon;20;30
Politoed;Frog Pokémon;75;75
Dodrio;Triple Bird Pokémon;110;70
Aggron;Iron Armor Pokémon;140;230
Mankey;Pig Monkey Pokémon;80;35
```



Il s'agit de données en table au format `csv`, au sujet des Pokémons, vous l'aviez deviné !

![logo pokémon _ source wikipedia - public domain](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/269px-International_Pok%C3%A9mon_logo.svg.png)

ce tbleua, appelé `relation`,  nous donne des informations sur ces petites bestioles selon plusieurs critères appelés `attributs`,  présents dans la première ligne et séparés par des `;`:

* `name ` désigne bien sûr le nom des bêbêtes;
* `classification` nous précise la classe du Pokémon;
* `attack` sa force d'attaque. Plus elle est élevée, plus le Pokémon est dangereux;
* `defense` son niveau de défense. Plus il est élevé, plus le Pokémon est résistant.



Sous les `attributs` , on trouve leurs `valeurs` elles aussi séparées par un `;`.



Remarquez que le _séparateur_ n'est pas toujours un `;`. C'est parfois une `,` ,par exemple après un enregistrement de `LibreOffice.calc`, et ça peut être n'importe quel caractère dont on décide qu'il joue le rôle de séparateur.



Pourriez-vous me dire, en observant ces données :

* Combien y-a-t-il de Pokémons ?

* Qui a la meilleure défense ?

* Quelles sont les caractéristiques de `Comfey` ?

* Quels sont les Pokémons ont une attaque supérieure à 70 ?

* Parmi les Pokémons qui ont une attaque supérieure à 140, quels sont les trois meilleurs en défense ?

* Existe-t-il des Pokémons dont l'attaque et la défense son supérieur à 100 ? Si oui, combien ?

  
  
  Pas facile ... Heureusement, il y a d'autres moyens de visualiser et de traiter ces données.



## Importer les données d'un fichier csv

Pour pouvoir exploiter ces données, il faut les importer dans une structure adaptée : un tableau ou un dictionnaire selon ce qu'on veut en faire. Ici ce sera un tableau.

Les fonctions pour faire cela sont déjà dans le dossier `fournis` qui porte bien son nom. Ouvrez `module_csv.py` et regardons le code en détails:

* `charger(nom_fichier, separateur = ';')` : 
    * essaie d'ouvrir la fichier nommé _nom_fichier_ et déclenche une exception sinon grâce à la structure `try ... except`;
    * la fonction qui permet de créer un _lien_ nommé _lecture_ vers le fichier est `lecture.open(nom_fichier,'r',encoding = 'utf_8')` : notez le codage de caractère utilisé !
    * `lecture.readlines()` lit toutes les lignes du fichier et renvoie un tableau contenant chaque ligne sous forme de chaine de caractères se terminant par `\n` en raison du retour à la ligne : `['name;classification;attack;defense\n', 'Swablu;Cotton Bird Pokémon;40;60\n', ...]`. 
    * On ferme le lien ouvert plus haut grâce à `lecture.close()`
    * il faut ensuite séparer chaque données des chaines à chaque `;` et enlever les `\n` : C'est ce que fait la boucle qui suit grâce, notemment à deux méthodes des chaines de caractères :
        * `rstrip('\n')` renvoie la partie à droite de `\n` :  `'Swablu;Cotton Bird Pokémon;40;60\n'` devient  `'Swablu;Cotton Bird Pokémon;40;60'`;
        * `split(separateur)` renvoie un tableau contennt les éléments de lar chaine en coupant à chaque séparateur : `'Swablu;Cotton Bird Pokémon;40;60'` devient `['Swablu', 'Cotton Bird  Pokémon', '40', '60']`.
    * Plus qu'à renvoyer le tableau obtenu.



> * Créez, à côté de `module_csv.py` un fichier `pokemon.py`
> * Dans ce fichier, importer le module puis chargez les données du fichier _pokemon.csv_ dans un tableau que vous nommerez _pokemon_ en utilisant le module.



Affichez le tableau obtenu dans la console : remarquez que les caractéristiques d'attaque et de défense sont des chaines de caractères ! Il faut donc les convertir au type _int_ pour pouvoir les exploiter.

> * Coder la fonction `convertir(tab)` qui prend en paramètre un tableau contenant les données des pokemons et qui modifie ce tableau par effet de bord en convertissant les valeeurs des attributs `attack` et `defense` en entier.
> * Convertissez le tableau de données _pokemon_



## Extraire des données

Maintenant qu'on a un tableau structuré comme il faut, on va pouvoir répondre aux questions posées au début.



On commence doucement :

> * Combien y-a-t-il de pokemon dans la table ?
>
>     ```python
>     ```
>
> * Qui a la meilleure défense ? (Pour celle là, il faudra coder une petite fonction dans _pokemon.py_ qui renverra le pokemon en question).
>
> * Quelles sont les caractéristiques de _Comfey_ ? (Pour celle là, il faudra coder une petite fonction  _pokemon.py_ qui renverra ces caractéristiques sous la forme d'un tableau).
>
>     ```python
>     ```
>
>     





En fait cette dernière question peut être résolue en une ligne en utilisant une requête _par compréhension_ qui va faire exactement la même chose que votre programme mais en l'écrivant d'une façon particulière et concise. Testez l'instruction ci-dessous dans la console :

```python
>>> [elt for elt in pokemon if elt[0] == 'Comfey']
???
```



en utilisant la même méthode répondez aux questions suivantes :

> * Quels sont les Pokémons ont une attaque supérieure à 70 ?
>
>     ```python
>     ```
>
> * Parmi les Pokémons qui ont une attaque supérieure à 140, quels sont les trois meilleurs en défense ? 
>
>     ```python
>     ```
>
> * Existe-t-il des Pokémons dont l'attaque et la défense son supérieur à 100 ? Si oui, combien ?
>
>     ```python
>     ```



## Sauvegardez un tableau dans un fichier csv

On voudrais maintenant obtenir la partie du tableau _pokemon_ en ne gardant que les Pokemons dont le type contient le mot 'Bird'.

> Obtenez par compréhension le tableau souhaité .
>
> ```python
> pokemon_bird = ...
> ```
>
> 



On veut maintenant sauvegarder ces données dans un fichier _csv_. Encore une fois, allez regarder le _module_csv_. Cette fois ci on a besoins d'une fonction secondaire, appelée par la fonction `sauver` car on ne peut écrire que du texte dans un fichier _csv_ et _pokemon_bird_ est un tableau de tableau de chaines et d'entiers : il faut convertir ce tableau et c'est le rôle de `convertir_csv`.

> * Aller voir le code de _convertir_csv_
>
> * testez :
>
>     ```python
>     >>> convertir_csv(pokemon_bird)
>     ???
>     ```
>
>     On retrouve le même format que nous avons importer tout au départ !



La fonction `sauver` va _écrire_ ces données dans le ficher spécifié. Pour cela, le code ressemble fort à celui de la fonction `charger` mais vous remarquerez que cette fois ci :

* qu'on commence par convertir la relation : `tab_csv = convertir_csv(relation)`;
* le lien se fait en écriture : `ecriture = open(nom_fichier,'w',encoding='utf-8')`;
* qu'au lieu de _lire_, on _écrit_ : `ecriture.writelines(tab_csv)`;
* et qu'on n'oublie pas de couper le lien : `ecriture.close()`



> * Testez :
>
>     ```python
>     >>> sauver(pokemon_bird, 'pokemon_bird.csv')
>     ```
>
> * Allez voir dans le répertoire courant et ouvrez  _pokemon_bird.csv_.





> Créez un fichier `pokemon_defensif.csv` qui contient les données des Pokémons dont la caractéristique de défense et supérieur à la caractéristique d'attaque.



## Trions un peu



Quand on a des données, on les trie pour les organiser. C'est pour cela que j'ai glissé le _module_tris_ dans le répertoire _fournis_.

> * Dans _pokemon.py_ copier et complétez le comparateur ci-dessous qui va nous permettre de trier les Pokémons par ordre alphabétique de leur nom :
>
>     ```python
>     def comp_nom(pokemon1, pokemon2):
>         '''
>         renvoie -1 si pokemon1<pokemon2, 1 si pokemon1 > pokemon2 et 0 sinon
>         en utilisant l'ordre alphabétique
>         : params
>         	pokemon1 (list) un enregistrement corresponand à un pokemon
>         	pokemon2 (list) un enregistrement corresponand à un pokemon
>         : return (int) 
>         '''
>     ```
>
> * Triez le tableau _pokemon_ en utilisant ce comparateur;
> * Sauvegardez pokemon dans le fichier _pokemon.csv_;
> * Ouvrez ce fichier pour vérifier le résultat.
>
> 



Si le critère de tri change, on change le comparateur et la suite est identique.



> * Codez un comparateur pour trier les Pokémons selon les valeurs de l'attribut `attack`
> * Triez le tableau _pokemon_ en utilisant ce comparateur;
> * Sauvegardez pokemon dans le fichier _pokemon.csv_;
> * Ouvrez ce fichier pour vérifier le résultat.



___________

Par Mieszczak Christophe

Licence CC BY SA

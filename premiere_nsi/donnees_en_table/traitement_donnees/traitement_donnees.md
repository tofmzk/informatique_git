# Traitement des données en table



## Introduction

Dans ce TP, nous allons importer une table à partir de son fichier au format _csv_ et les rendre exploitables en Python sans utiliser de module dédié.



Nous allons travailler sur le fichier fourni _villes_metropole_corse.csv_ qui recence 36567 communes de France.



Créez un fichier _villes.py_ dans lequel nous allons travailler tout au long de ce TP.



## Importer des données

Ouvrez le  _villes_metropole_corse.csv_ avec un éditeur de texte. 

```txt
Dept;Nom;CP;Population_2010;Surface_km2;Longitude;Latitude;Alt_Min;Alt_Max
1;OZAN;1190;618;6.6;4.91667;46.3833;170;205
1;CORMORANCHE-SUR-SAONE;1290;1058;9.85;4.83333;46.2333;168;211
1;PLAGNE;1130;129;6.2;5.73333;46.1833;560;922
1;TOSSIAT;1250;1406;10.17;5.31667;46.1333;244;501
1;POUILLAT;1250;88;6.23;5.43333;46.3333;333;770
1;TORCIEU;1230;698;10.72;5.4;45.9167;257;782
1;REPLONGES;1620;3500;16.6;4.88333;46.3;169;207
1;CORCELLES;1110;243;14.16;5.58333;46.0333;780;1081
1;PERON;1630;2143;26.01;5.93333;46.2;411;1501
1;RELEVANT;1990;466;12.38;4.95;46.0833;235;282
1;CHAVEYRIAT;1660;927;16.87;5.06667;46.1833;188;260
1;VAUX-EN-BUGEY;1150;1169;8.22;5.35;45.9167;252;681
1;MAILLAT;1430;668;11.31;5.55;46.1333;497;825
1;FARAMANS;1800;681;11.22;5.11667;45.9;255;306

```

Vous observez que les données sont simplement écrites séparées par des `;` mais aussi par les retours à la ligne et que la première ligne correspond aux attributs de notre collection.

Nous allons donc :

* importer toutes les lignes du fichier texte.
* enlever les chaines `\n` qu'on trouvera à chaque début de ligne (cf [caractères d'échappement](https://fr.wikipedia.org/wiki/Caract%C3%A8re_d%27%C3%A9chappement))
* scinder chaque ligne à chaque `;`

On obtiendra alors un tableau dont chaque élément est un autre tableau correspondant à une ligne de données.

````python
[
['Dept', 'Nom', 'CP', 'Population_2010', 'Surface_km2', 'Longitude', 'Latitude', 'Alt_Min', 'Alt_Max'],
['1', 'OZAN', '1190', '618', '6.6', '4.91667', '46.3833', '170', '205'], 
['1', 'CORMORANCHE-SUR-SAONE', '1290', '1058', '9.85', '4.83333', '46.2333', '168', '211'],
['1', 'PLAGNE', '1130', '129', '6.2', '5.73333', '46.1833', '560', '922'],
['1', 'TOSSIAT', '1250', '1406', '10.17', '5.31667', '46.1333', '244', '501'],
['1', 'POUILLAT', '1250', '88', '6.23', '5.43333', '46.3333', '333', '770'], .....
]
````



Le code nécessaire étant un peu complexe, le voici. Regardez bien chaque ligne pour en comprendre le fonctionnement.

```python
def lire_donnees(nom_fichier_CSV, separateur = ';'):
    """
        lit le fichier csv dans le repertoire data et revoie la liste des donnees
        : param nom_fichier_CSV (str)
        : param separateur (str)
        return tableau des données (list)
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    try :
        lecture = open(nom_fichier_CSV,'r',encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le ficier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
    lecture.close() # ferme le canal en lecture
    return toutes_les_lignes
```



> Depuis la console, importer le fichier  _villes_metropole_corse.csv_  dans une liste _villes_ en utilisant la fonction précédente et complétez la partie ci-dessous :
>
> ```python
> >>> villes = lire_donnees('villes_metropole_corse.csv')
> >>> type(villes)
> ???
> >>> ville[0]
> ???
> >>> ville[1]
> ???
> >>> len(villes)
> ???
> ```
>
> * A quoi correspond ville[0] ?
>
>     ```txt
>             
>     ```
>
>     
>
> * Quel est le type de chaque élément de ville[1] ?
>
>     ```txt
>             
>     ```
>
>     
>
> * A quels attributs des données correspondent les éléments de ville[0] ?
>
>     ```txt
>             
>     ```
>
>     
>
>     



## NamedTuple



Nous disposons maintenant d'une liste de liste de caractéristiques de villes. On pourrait déjà faire pas mal de petites choses (rechercher une ville dans la liste ou les villes de certaines altitudes par exemples) mais notre structure ne permet pas d'effectuer des tris par attributs de façon simple et rapide.

Cela est très gênant car les tris sont une des opérations les plus courantes lorsqu'on travaille sur les données.

Pour palier à ce problème, nous allons utiliser une nouvelle structure de données : les **namedTuples**.



Testez donc dans la console (et bien entendu, réfléchissez un peu aux résultats obtenus) :

```  python
>>> import collections
>>> from operator import attrgetter
>>> base_villes = collections.namedtuple('base_villes',['Dept', 'Nom', 'CP'])
>>> villes = []
>>> ville = base_villes('62', 'CALAIS', 62100)
>>> ville[0]
??? 
>>> ville.Nom
???
>>> ville[1]
???
>>> ville[2]
???
>>> ville.CP
???
>>> villes.append(ville)
>>> villes
???
>>> villes.append(base_villes('62', 'BOULOGNE', 62160))
>>> villes.append(base_villes('59', 'DUNKERQUE', 59183))
>>> villes.append(base_villes('59', 'LILLE', 59000))
>>> len(villes)
???
>>> for ville in villes:
    	print(ville.Nom, ':', ville.CP)
???
>>> sorted(villes, Nom)
>>> for ville in villes:
    	print(ville.Nom, ':', ville.CP)
???
>>>
>>> villes.sort(key = attrgetter('Nom'))
>>> for ville in villes:
    	print(ville.Nom, ':', ville.CP)
???
>>> villes.sort(key = attrgetter('CP'), reverse = True)
>>> for ville in villes:
    	print(ville.Nom, ':', ville.CP)
???      

```



Vous l'avez compris voici une structure très intéressante :

* Il s'agit d'une collection de namedTuples
* Tous les namedTuples d'une même collection possèdent les mêmes attributs (_keys_), ce qui n'est pas sans rappeler les _dictionnaires_.
* On peut facilement trier la collection de namedTuples selon n'importe quel attribut.



## Créer une collection de namedTuple



Nous allons ici créer une collection de nameTuples où chaque namedTuple est une ville qui a pour attributs _Dept_, _Nom_,  _CP_, _Population_, _Surface_, _Longitude_, _Latitude_, _Alt_Min_, _Alt_Max_.



N'oubliez pas les importations en haut du code :

```python
import collections
from operator import attrgetter
```





Voici le début de notre fonction :

```Python
def creer_collection(liste_villes):
    '''
    créer une collection de tupple nommé à partir de la liste passée en paramètre.
    : param liste_villes liste de données chargées avec la fonction liste_donnees
    type list
    : return une collection de namedtuples
    type namedtuple    
    
    '''
    # creons un tuple nommé 'base_villes' avec les attributs adéquats
    base_villes = collections.namedtuple('base_villes',
                                         ['Dept', 'Nom', 'CP', 'Population',
                                          'Surface', 'Longitude', 'Latitude',
                                          'Alt_Min', 'Alt_Max'])
```



Il faut maintenant, comme vu plus haut dans la console, créer une collection de namedTuples où chaque Tuple nommé _base_villes_ correspondra à une ligne de la liste _liste_villes_.



Essayez de le faire vous même avant de descendre voir la suite ...

















SI si si . Essayer par vous même encore un peu.



















Voilà donc comment faire :

```python
def creer_collection(liste_villes):
    '''
    créer une collection de tupples nommés à partir de la liste passée en paramètre.
    : param liste_villes liste de données chargées avec la fonction liste_donnees
    type list
    : return une collection de namedtuples
    type list of namedtuples
    
    '''
    # creons un tuple nommé 'villes' avec les attributs adéquats
    base_villes = collections.namedtuple('base_villes',
                                         ['Dept', 'Nom', 'CP', 'Population',
                                          'Surface', 'Longitude', 'Latitude',
                                          'Alt_Min', 'Alt_Max'])
    collection_villes = []
    for i in range(1, len(liste_villes)): # on commence après la ligne des attributs
        #création d'une ville
        ville = base_villes(   liste_villes[i][0], #Dept
                               liste_villes[i][1], #Nom
                               liste_villes[i][2], # CP
                               liste_villes[i][3], # Populaiton
                               liste_villes[i][4], # Surface
                               liste_villes[i][5], # Longitude
                               liste_villes[i][6], # Latitude 
                               liste_villes[i][7], # Alt_Min
                               liste_villes[i][8] # Alt-Max
                               )
        #ajout de la ville à la collection de villes
        collection_villes.append(ville)
    return collection_villes
```



Depuis la console :

```python
>>> liste_villes = lire_donnees('villes_metropole_corse.csv')
>>> collection_villes =  creer_collection(liste_villes)
>>> type(collection_villes)
???
>>> type[collection_villes[0])
???
>>> collection_villes[0]
???         
```





## Affichons les données



> Réaliser une fonction `afficher_villes` de paramètres _namedtuple_villes_ , une liste de namedTuple, et _nbre_villes_, un nombre maximum de villes à afficher valant par défaut 10, qui affiche dans la console les _nbre_villes_ premières villes de _namedtuple_villes_.
>
> ```python
> def afficher_villes(collection_villes, nbre_villes = 10):
>    	'''
>    	affiche les nbre_villes premières villes de la liste des villes
>    	: param collection_villes
>    	type list of namedtuples
>    	: param nbre_villes par défaut 10
>    	type int
>    	'''
> ```
>
>  
>
> Testez votre fonction en affichant les 5 premières villes obtenues à partir du fichier _villes_metropole_corse.csv_.
>
> ```python
> >>> liste_villes = lire_donnees('villes_metropole_corse.csv')
> >>> collection_villes = creer_collection(liste_villes)
> >>> afficher(collection_villes, 5)
> ???
> ```



## Conversion 



Par défaut, toutes les valeurs sont des chaines de caractères. Mais la plupart sont en réalité des nombres. Or, on ne trie pas de la même façon avec les mêmes comparateurs des nombres et des chaines de caractères. Nous allons donc réaliser une fonction pour convertir les données qui peuvent l'être au format numérique.



Pour rappel, testez et complétez :

```python
>>> int('12')
???
>>> float('12.3')
???
>>> int('12.3')
???
>>> int('2A')
???
```



> Définissez et réalisez une fonction `convertir_num` de parametre _liste_villes_, une liste de villes, qui convertit en entier ou en flottant, quand c'est possible, les éléments de _liste_villes_ par effet de bord.
>
> Allez ... un peu d'aide : 
>
> ```python
> def convertir_num(liste_villes):
>     	'''
>        convertit les valeurs numériques au format float ou int
>        : param liste_villes liste de villes
>        : effet de bord sur liste_villes
>        '''
>        for i in range(1,len(liste_villes)):  # on commence à la deuxième ligne, après la ligne des attributs
>            liste_villes[i][2] = int(liste_villes[i][2] ) # cp
>            liste_villes[i][3] = int(liste_villes[i][3] ) # population ... à vous de terminer
> ```
>
> 



_Remarques :_ 

* Un attribut semble pouvoir être converti mais ne le peut pas. Il s'agit de _Dept_. Celui de la corse est _'2A'_, la conversion au format numérique de cet attribut est donc impossible. Nous la laisserons au format _str_.

* On va appeler cette fonction avant le `return` dans la fonction `lire_donnees` qui devient :

    ```python
    def lire_donnees(nom_fichier_CSV, separateur = ';'):
        """
            lit le fichier csv dans le repertoire data et revoie la liste des donnees
            : param nom_fichier_CSV
            type str
            : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
            type str
            return dictionnaire 
            type dict
        """
        assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
        try :
            lecture = open(nom_fichier_CSV,'r',encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
        except FileNotFoundError :
            raise # renvoie une erreur si le ficier n'existe pas
        toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
        for i in range(0,len(toutes_les_lignes)): 
            toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
        lecture.close() # ferme le canal en lecture
        convertir_num(toutes_les_lignes) # on convertit les données adéquates au format numérique
        return toutes_les_lignes
    ```

    



## Comptons les données

Nous allons utiliser des _Doctests_. Copier_coller le code usuel en bas du votre :

```Python
################################################
###### doctest : test la correction desfonctions
################################################
if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = False) 

```







> Créez une fonction `compter_villes` de paramètres _collection_villes_ et _departement_ qui renvoie le nombre de villes du département précisé dans _collection_villes_.
>
> 
>
> ```python
> def compter_villes(collection_villes, departement):
> 	'''
> 	renvoie le nombre de communes dans le departement précisé
> 	: param collection_villes
> 	type list of namedtuples
> 	: param departement le département
> 	type str
> 	: return le nbre de villes recherchées
> 	type int
> 	>>> liste_villes = lire_donnees('villes_metropole_corse.csv')
> 	>>> collection_villes = creer_collection(liste_villes)
> 	>>> compter_villes(collection_villes, '80')
> 	782
> 	'''
> ```
>
> 
>
> * Combien y-a-t-il de communes référencées dans les Hauts-de-France ?
>
>   ```txt
>       
>   ```
>   
>   
>   
>    



> Créez une fonction `compter_villes_selon_altitude` de paramètres _collection_villes_, _altitude_ et _departement_ qui renvoie le nombre de villes du département précisé dans _collection_villes (tous les départements par défaut) dont l'altitude maximale est inférieure ou égale à    _altitude_. 
>
> ```python
> def compter_villes_selon_altitude(collection_villes, altitude, departement = ''):
>  	'''
>  	renvoie le nombre de communes dans le dpt précisé par défaut tous les dept.
>  	: param collection_villes
>  	type namedtuple
>  	: param altitude l'altitude de référence
>  	type float
>  	: param departement le département, par défaut tous les départements
>  	type str
>  	: return le nbre de villes recherchées
>  	type int
> 	>>> liste_villes = lire_donnees('villes_metropole_corse.csv')
> 	>>> convertir_num(liste_villes)
> 	>>> collection_villes = creer_collection(liste_villes)
> 	>>> compter_villes_selon_altitude(collection_villes, 10, '62')
>  	9
>  	'''
> ```
>
> 
>
> Testez votre fonction :
>
> ```python
> >>> liste_villes = lire_donnees('villes_metropole_corse.csv')
> >>> collection_villes = creer_collection(liste_villes)
> >>> compter_villes_selon_altitude(collection_villes, 10, '59')
> ???
> ```
> 
>

_Remarque :_

Ca commence à faire pas mal de lignes à taper dans la console ...

On pourra utiliser, et modifier au fur et à mesure des questions, une fonction `test` afin d'éviter d'avoir sans cesse à recopier les mêmes lignes dans la console. Il suffira d'appeler cette fonction depuis la console.

```python
def test() :
    liste_villes = lire_donnees('villes_metropole_corse.csv')
    collection_villes = creer_collection(liste_villes)
    return compter_villes_selon_altitude(collection_villes, 10, '59')
    
```



## Trier les données

C'est un des points forts des _namedTuples_. On trie une collection de _namedTuples_ en une ligne :

```python
nom_du_namedTuple.sort( key = attrgetter(attribut(s)_a_trier), reverse = False)
- nom_du_namedTuple est le namedTuple à trier
- attribut(s)_a_trier est un des attributs des namedTuples. On peut trier selon plusieurs attributs.
- reverse peut valoir False (par défaut croissant) ou True (ordre décroissant)
```



> Réalisez une fonction `trier`, de paramètres _collection_villes_, _attribut_ et _decroissant_ qui trie les namedtuples selon l'_attribut_ choisi.
>
> ```python
> def trier(namedtuple_villes, attribut, decroissant = False):
>     '''
>     Tri le NamedTuple des villes selon l'attribut choisi
>     : param  namedtuple_villes
>     type list of namedTples
>     : parma attribut : un attribut
>     type str
>     : param decroissant (par défaut false) ordre du tri
>     : EFFET DE BORD SUR namedtuple_villes    
>     '''
>     
> ```
>
> 



Votre fonction de test devient :

```python
def test() :
    liste_villes = lire_donnees('villes_metropole_corse.csv')
    collection_villes = creer_collection(liste_villes)
    trier(collection_villes, 'Population')
    afficher_villes(collection_villes, 10)
```

Testez là depuis la console !



Maintenant on peut trier la collection _collection_villes_.



> * les 5 villes d'altitudes minimales
>
>     ```txt
>     QUIMPER ( 29 / 29000 )  pop : 63550  surf: 84.45  lat : 48.0  long: -4.1  alt-min: -5.0  alt-max: 151.0
>     TREFFIAGAT ( 29 / 29730 )  pop : 2378  surf: 8.1  lat : 47.8167  long: -4.26667  alt-min: -1.0  alt-max: 26.0
>     PENMARCH ( 29 / 29760 )  pop : 5796  surf: 16.39  lat : 47.8123  long: 3.66223  alt-min: -1.0  alt-max: 23.0
>     ILE-AUX-MOINES ( 56 / 56780 )  pop : 629  surf: 3.2  lat : 47.5967  long: 1.15528  alt-min: -1.0  alt-max: 31.0
>     BADEN ( 56 / 56870 )  pop : 4137  surf: 23.53  lat : 47.6167  long: -2.91667  alt-min: -1.0  alt-max: 43.0
>     PLOEMEUR ( 56 / 56270 )  pop : 17805  surf: 39.72  lat : 47.7333  long: -3.43333  alt-min: -1.0  alt-max: 55.0
>     LE HEZO ( 56 / 56450 )  pop : 730  surf: 4.89  lat : 47.5833  long: -2.7  alt-min: -1.0  alt-max: 38.0
>     VILLEFRANCHE-SUR-MER ( 6 / 6230 )  pop : 5419  surf: 4.88  lat : 43.7  long: 7.31667  alt-min: 0.0  alt-max: 575.0
>     VALLAURIS ( 6 / 6220 )  pop : 28252  surf: 13.04  lat : 43.5833  long: 7.05  alt-min: 0.0  alt-max: 285.0
>     ANTIBES ( 6 / 6600 )  pop : 74120  surf: 26.48  lat : 43.5833  long: 7.11667  alt-min: 0.0  alt-max: 163.0
>     ```
>
>     
>
> * Afficher les 5 villes d'altitudes maximales
>
>     ```txt
>     VAL-D'ISERE ( 73 / 73150 )  pop : 1563  surf: 94.39  lat : 45.45  long: 6.98333  alt-min: 1785.0  alt-max: 3599.0
>     BONNEVAL-SUR-ARC ( 73 / 73480 )  pop : 241  surf: 82.72  lat : 45.3667  long: 7.05  alt-min: 1759.0  alt-max: 3642.0
>     SAINT-VERAN ( 5 / 5350 )  pop : 257  surf: 44.75  lat : 44.7  long: 6.86667  alt-min: 1756.0  alt-max: 3175.0
>     BESSANS ( 73 / 73480 )  pop : 343  surf: 128.08  lat : 45.3167  long: 6.99167  alt-min: 1673.0  alt-max: 3754.0
>     MOLINES-EN-QUEYRAS ( 5 / 5350 )  pop : 315  surf: 53.62  lat : 44.7333  long: 6.85  alt-min: 1625.0  alt-max: 3160.0
>     ```
>
>     
>
> * Afficher les 5 villes de plus grandes surfaces
>
>     ```txt
>     ARLES ( 13 / 13104 )  pop : 52661  surf: 758.93  lat : 43.6667  long: 4.63333  alt-min: 0.0  alt-max: 57.0
>     SAINTES-MARIES-DE-LA-MER ( 13 / 13460 )  pop : 2296  surf: 374.61  lat : 43.45  long: 4.425  alt-min: 0.0  alt-max: 6.0
>     LARUNS ( 64 / 64440 )  pop : 1283  surf: 248.96  lat : 42.9833  long: -0.416667  alt-min: 458.0  alt-max: 2973.0
>     MARSEILLE ( 13 / 13001 )  pop : 850726  surf: 240.62  lat : 43.2967  long: 5.37639  alt-min: 0.0  alt-max: 640.0
>     SAINT-MARTIN-DE-CRAU ( 13 / 13310 )  pop : 11180  surf: 214.87  lat : 43.6333  long: 4.81667  alt-min: 2.0  alt-max: 447.0
>     ```
>
>     
>
> * Afficher les 5 villes qui ont le plus d'habitants.
>
>     ```python
>     PARIS ( 75 / 75001 )  pop : 2243833  surf: 105.4  lat : 48.86  long: 2.34445  alt-min: 0.0  alt-max: 0.0
>     MARSEILLE ( 13 / 13001 )  pop : 850726  surf: 240.62  lat : 43.2967  long: 5.37639  alt-min: 0.0  alt-max: 640.0
>     LYON ( 69 / 69001 )  pop : 484344  surf: 47.87  lat : 45.7589  long: 4.84139  alt-min: 162.0  alt-max: 312.0
>     TOULOUSE ( 31 / 31000 )  pop : 441802  surf: 118.3  lat : 43.6  long: 1.43333  alt-min: 115.0  alt-max: 263.0
>     NICE ( 6 / 6000 )  pop : 343304  surf: 71.92  lat : 43.7  long: 7.25  alt-min: 0.0  alt-max: 520.0
>     ```
>     
>     







# Extraire des données



Il s'agit ici de créer de nouveaux namedTuples à partir de l'ensemble des données selon certains critères. Par exemples, quelles sont les villes situées à moins de 10m d'altitude ? Quelles sont les villes du département 62 situées à moins de 10m d'altitude et comptant plus de 5000 habitants ?



Voici une fonction qui renvoie une nouvelle collection. Quelle est le critère de sélection des villes ? 

```python
def renvoyer_villes_sous_altitude(collection_villes, altitude):
    '''
    renvoie une collection de namedtuple correspondant aux villes située à une altitude inférieure à altitude
    :param collection_villes une collection de villes
    type namedtuple
    : param altitude l'altitude d référence
    type float
    : return la collection de ville vérifiant la condition
    type list of namedtuples
    '''
    collection_villes_choisies = []
    for ville in collection_villes :
        #ajout de la ville à la liste de villes sous condition
        if ville.Alt_Max <= altitude :
            collection_villes_choisies.append(ville)
    return collection_villes_choisies
```



Votre fonction de test devient :

```python
def test() :
    liste_villes = lire_donnees('villes_metropole_corse.csv')
    collection_villes = creer_collection(liste_villes)
    collection_villes_basses = renvoyer_villes_sous_altitude(collection_villes, 5)
    return collection_villes_basses
```

Testez là depuis la console !

```python
>>> collection_villes_basses = test()	
>>> len(collection_villes_basses)
???
>>> afficher_villes(collection_villes_basses)
???

```



> Créez une fonction `renvoyer_villes_selon_noms` de paramètre _collection_villes_ , une collection de villes, et  _noms_, une liste de noms, qui renvoie une collection de _namedTuples_ des villes dont le nom est présent dans la liste  _noms_ précisée en paramètre.
>
>  
>
> Vou utiliserez le _doctest_ ci-dessous :
>
> ```python
> >>> liste_villes = lire_donnees('villes_metropole_corse.csv')
> >>> convertir_num(liste_villes)
> >>> collection = creer_collection(liste_villes)
> >>> renvoyer_villes_selon_noms(collection, ['CALAIS', 'LILLE'])
> [base_villes(Dept='59', Nom='LILLE', CP=59000, Population=227560, Surface=34.83, Longitude=3.06667, Latitude=50.6333, Alt_Min=17.0, Alt_Max=45.0), base_villes(Dept='62', Nom='CALAIS', CP=62100, Population=73636, Surface=33.5, Longitude=1.83333, Latitude=50.95, Alt_Min=0.0, Alt_Max=18.0)] 
>    ```






> Créez une fonction `renvoyer_villes` de paramètre _altitude_, _departement_ et _nbre_habitants_ qui renvoie une collection de _namedTuples_ des villes du département précisé situées en totalité à une altitude inférieure à l'altitude précisée et comptant plus du nombre d'habitants précisé.
>
>  
>
> Vou utiliserez le _doctest_ ci-dessous :
>
> ```python
> >>> liste_villes = lire_donnees('villes_metropole_corse.csv')
> >>> convertir_num(liste_villes)
> >>> collection_villes = creer_collection(liste_villes)
> >>> renvoyer_villes(collection_villes, 10, '62', 3000)
> [base_villes(Dept='62', Nom='COULOGNE', CP=62137, Population=5686, Surface=9.16, Longitude=1.88333, Latitude=50.9167, Alt_Min=0.0, Alt_Max=7.0)]
> ```



## Sauvegarder une collection



Grâce aux fonctions précédentes, on a soit modifié une collection par tri soit extrait une nouvelle collection. Ce serait bien de sauvegarder ces nouvelles données.



* Il faut créer une liste vide, que l'on nommera _liste_villes_ qui contiendra toutes les listes créée ci-après
* pour chaque namedTuple_ de la collection
    * créer une liste, que l'on nommera _ville_, contenant les valeurs de chaque attribut séparées par des `;`.
    * ajouter la ville à la liste _liste_villes_.
* convertir toutes les valeurs non numériques de la liste _liste_ville_ en chaines de caractères.
* sauvegarder la liste _liste_villes_ dans un fichier texte au format _csv_.



On va commencer par récupérer la liste des villes.

> Créez une fonction `créer_liste_villes` de paramètres _collection_villes_, une collectoin de villes, qui renvoie une liste contenant les listes des valeurs des attributs de chaque ville de la collection
>
> ```python
> def creer_liste_villes(collection_villes):
>  '''
>  renvoie une liste de villes à partir de la collection passée en paramètre
>  : param collection_villes une collection de villes
>  type list de namedtuples
>  : return une liste de villes
>  type list
>  '''
>  # premiere ligne avec les attributs
>  liste_villes =  [['Dept', 'Nom', 'CP', 'Population','Surface', 'Longitude', 'Latitude','Alt_Min', 'Alt_Max']]
>  # une boucle pourparcourir la collection et compléter la liste au fur et à mesure
>  for ville in collection_villes :
>      liste_villes.append([ ville.Dept,
>                            ville.Nom, # ... A vous de continuer
> ```
>
> 



Passons maintenant aux conversions :



> Créez une fonction `convertir_str` de paramètre _liste_villes_, une liste contenant les listes de chaque ville,  convertissant les données numériques de valeurs au format str.
>
> On s'inspirera fortement de la fonction `convertir_num` en utilisant la méthode `str` au lieu des méthodes `int` et `float`
>
> ```python
> def convertir_str(liste_villes):
>  '''
>  convertit les valeurs de type int et float de chaque ville de la liste en str
>  : param liste_villes
>  type liste
>  : effet de bord sur liste_villes
>  '''
> ```



Testons un peu dans la console :

``` python
>>> liste_villes = lire_donnees('villes_metropole_corse.csv')
>>> collection = creer_collection(liste_villes)
>>> petite_collection = renvoyer_villes_selon_nom(collection, ['CALAIS', 'LILLE'])
>>> petite_liste = creer_liste_villes(petite_collection)
>>> petite_liste[0] 
???
>>> petite_liste[1]
???
>>> convertir_str(petite_liste)
>>> petite_liste[0] 
???
>>> petite_liste[1]
???
```



Mais dans un fichier texte, chaque ligne est une chaine de caractères. Il faut donc 

> Regardez la fonction `convertir_csv`  ci-dessous. 
>
> ```python
>def convertir_csv(liste, separateur  = ';'):
> 	'''
> 	renvoie une liste de chaine compatible avec l'écriture d'un fichier en csv	
>  	: param : liste_villes une liste de villes
>  	type list
>  	: param  :separateur par défaut ';'
>  	type str
>  	return une liste de chaines
>  	type list
>  	'''
>  	liste_chaines = []
>  	for elt in liste:
>          chaine = ''
>    		for i in range(len(elt)) :
>          		chaine += elt[i]
>          		if i < len(elt) - 1 : # si on n'est pas en bout de ligne, alors on ajoute un separateur
>              		chaine += separateur
>      		chaine += '\n' # caractère d"chapement fin de ligne
>      		liste_chaines.append(chaine) # on ajoute la chaine générée à la liste des chaines.
>    	return liste_chaines
>    ```
>  Via la console faisons quelques tests :
> 
> ```python
>>>> liste_villes = lire_donnees('villes_metropole_corse.csv')
> >>> collection_villes = creer_collection(liste_villes)
> >>> collection_basses = renvoyer_villes_sous_altitude(collection_villes, 2)
> >>> liste_villes_basses = creer_liste_villes(collection_basses)
> >>> convertir_str(liste_villes_basses)
> >>> liste_villes_basses
> ???
> >>> liste = convertir_chaines(liste_villes_basses)
> >>> liste
> ???
>  
> ```
> 
>    



Reste à sauvegarder. Cela fonctionne un peu comme `lire_donnees` :

```python
def sauver(collection, nom_fichier_CSV, separateur = ';'):
    """
    écrit dans le fichier csv les données de la collection
    : param collection
    type list of namedtuple
    : param nom_fichier_CSV
    type str
    : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
    type str
    : ne renvoie rien mais crée un fichier ou écrit dan sun fichier existant
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    liste = creer_liste_villes(collection) # crée une liste à partir de la collection
    convertir_str(liste) # convertit en str les valeurs numériques de la liste
    liste = convertir_csv(liste) # transforme la liste de listes en liste de chaines
    try :
        ecriture=open(nom_fichier_CSV,'w',encoding='utf-8') # ouvre un canal en écriture vers fichier2
        ecriture.writelines(liste) # on écrit la liste de chaines
        ecriture.close() # on ferme le canal
    except:
        raise "!!!!!!!!!! impossible d'ecrire dans " +nom_fichier # affiche un msg d'erreur 
    

    
```



> Créez un fichier `villes_menacees.csv` de toutes les villes situées à moins de 10m d'altitude.



## Un petit bonus : folium



Le module `folium` est spécialisé dans la géolocalisation. Il n'est pas installé par défaut dans _Thonny_. Pour cela allez dans le menu _Tools_/_Manage package ..._ . Tapez _folium_ dans la zone de texte puis cliquer sur _Find package_ et enfin _Install_.

![installer folium](media/installer_folium.jpg)



`folium` est capable de générer une carte sur laquelle il affichera les points dont on passera les coordonnées géographiques en paramètres :

```python
>>> import folium
>>> coordonnee1 = ( 50.9167, 1.88333)
>>> carte = folium.Map(location = coordonnee1,zoom_start = 6) # initialise une carte avec les coordonées initiales
>>> folium.CircleMarker(coordonnee1, radius = 3).add_to(carte) # dessine un cercle sur la carte
>>> coordonnee2 = (51.05,2.36667)
>>> folium.CircleMarker(coordonnee2, radius = 3).add_to(carte) # dessine un second cercle sur la carte
>>> carte.save('carte_test.html') # créer un fichier HTML correspondant à la carte
```



Importez `folium` dans votre fichier `villes.py` avec les autres importations et insérez le code ci-dessous :

```Python

def creer_carte(collection_villes):
    '''
    en Utilisant folium, affiche les villes de la collection de villes
    : param collection_villes
    type list of namedtuples
    : Pas de return mais la création d'un fchier HTML dans le répertoire courant
    '''
    # par sécurité, on limite le nbre de villes à afficher à 400
    if len(collection_villes) > 400 :
        return 'trop de ville dans la liste'
    # on récupère dans la liste des villes la liste des coordonnées GPS
    coordonnees = [] # liste des futures coordonnées
    for ville in collection_villes :
        coordonnees.append((ville[6], ville[5])) # attention latitude puis longitude, c'est inversé dans la table
    # Création de la carte avec la première coordonnee    
    carte = folium.Map(location = coordonnees[0],zoom_start=6)
    #Ajout des autres coordonnées
    for coordonnee in coordonnees :
        folium.Map(location = coordonnee, zoom_start = 6).add_to(carte)
        folium.CircleMarker(coordonnee, radius = 3).add_to(carte)
    #sauvegarde de la carte au format HTML
    carte.save('carte_france.html')
```



> En utilisant des collections habilement extraites de la collection complète de villes :
>
> * Créer une carte représentant les villes de France où sont marquées :
>
>     * Paris
>     * Marseille
>     * Lille
>     * Strasbourg
>
>     
>
> * Créer une carte représentant les villes de France où sont marquées les villes situées à moins de 10m d'altitude.



____

Par Mieszczak Christophe CC - BY - SA


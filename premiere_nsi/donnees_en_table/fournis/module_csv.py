def charger(nom_fichier, separateur = ';'):
    """
        lit le fichier csv dans l'URL est spécifiée en paramètre
        et revoie un tableau des donnees
        : params
            nom_fichier (str)
            separateur (str) par défaut ;
        return tableau des données (list)
    """
    assert type(nom_fichier) is str,'nom_fichier is str'
    try :
        lecture = open(nom_fichier,'r',encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le ficier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    print(toutes_les_lignes)
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
    lecture.close() # ferme le canal en lecture
    return toutes_les_lignes

def convertir_csv(tab, separateur  = ';'):
    '''
    renvoie une tableau de chaines compatible avec l'écriture d'un fichier en csv	
    : params
        tab (list)
        separateur (str) par défaut ';'
    : return un tableau de chaines (tab)
    '''
    tab_chaines = []
    for elt in tab:
        chaine = ''
        for i in range(len(elt)) :
            chaine += elt[i]
            if i < len(elt) - 1 : # si on n'est pas en bout de ligne, alors on ajoute un separateur
                chaine += separateur
        chaine += '\n' # caractère d"chapement fin de ligne
        tab_chaines.append(chaine) # on ajoute la chaine générée à la liste des chaines.
    return tab_chaines

def sauver(relation, nom_fichier, separateur = ';'):
    """
    écrit dans le fichier csv les données de la collection
    : params
        relation (list) le tableau contenant les données de la relation
        nom_fichier (str)
        separateur (str) le séparateur utilisé dans le fichier csv. par défaut ;
    : ne renvoie rien mais crée un fichier ou écrit dan sun fichier existant
    """
    assert type(nom_fichier) is str,'nom_fichier_CSV is str'
    tab_csv = convertir_csv(relation)
    try :
        ecriture=open(nom_fichier,'w',encoding='utf-8') # ouvre un canal en écriture vers fichier2
        ecriture.writelines(tab_csv) # on écrit la liste de chaines
        ecriture.close() # on ferme le canal
    except:
        raise "!!!!!!!!!! impossible d'ecrire dans " +nom_fichier # affiche un msg d'erreur 
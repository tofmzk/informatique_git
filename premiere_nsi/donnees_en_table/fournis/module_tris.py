import time
import random

#####################################
#### LES COMPARATEURS
#####################################

def comp_defaut(elt1, elt2):
    '''
    si elt1 et elt2 sont rangés par ordre croissant alors renvoie 1 
    sinon si elt1 et elt2 sont rangés par ordre décroissant renvoie -1 
    sinon renvoie 0
    param elt1, elt 2 deux élément de la liste
    type int ou float
    >>> comp_defaut(2, 3)
    1
    >>> comp_defaut(3, 2)
    -1
    >>> comp_defaut(3,3)
    0
    '''
    if elt1 < elt2 : # ordre croissant
        return 1
    elif elt1 > elt2 : # ordre décroissant
        return -1
    else :
        return 0
    
def comp_longueur_mot(elt1, elt2):
    '''
    si elt1 et elt2 sont rangés par ordre croissant alors renvoie 1 
    sinon si elt1 et elt2 sont rangés par ordre décroissant renvoie -1 
    sinon renvoie 0
    param elt1, elt 2 deux éléments de type str
    >>> comp_longueur_mot('bonjour', 'hi')
    -1
    >>> comp_longueur_mot('hello', 'bonjour')
    1
    >>> comp_longueur_mot('hello', 'hallo')
    0
    '''
    if  len(elt1) < len(elt2): # ordre croissant
        return 1
    elif len(elt1) > len(elt2) : # ordre décroissant
        return -1
    else :
        return 0
        
#####################################
#### TRI PAR SELECTION
#####################################    

def minimum(liste, comparateur = comp_defaut ):
    '''
    renvoie l'indice de l'élement minimum de la liste en utilisant
    l'opérateur passé en paramètre
    param liste 
    type list
    param comparateur 
    type function
    return l'indice du minimum
    type int
    >>> minimum([4, 5, -7, 8, 0, 2])
    2
    '''
    if len(liste) == 0 :
        return -1
    indice_min = 0 #indice de départ
    # on parcours les elts de la liste et dès que l'un est plus petit que
    # liste[indice_min], on remplace indice_min par l'indice de cet elt
    for i in range(1, len(liste)):
        # comparons liste[i] et liste[indice_min]
        if comparateur(liste[i], liste[indice_min]) == 1 : # si ordre croissant
            indice_min = i            
    return indice_min
    
     
    
    
    
def echanger(liste, indice1, indice2):
    '''
    echange les élements d'indices indice1 et indice2 de la liste
    effet de bord sur liste
    param liste
    type list
    param indice1, indice2
    type integer
    >>> liste = [2, 3, 4, 5, 6]
    >>> echanger(liste, 0, 3)
    >>> liste == [5, 3, 4, 2, 6]
    True
    '''
    liste[indice1], liste[indice2] = liste[indice2], liste[indice1]
    
    

def tri_selection (liste, comparateur = comp_defaut) :
    '''
    modifie par effet de bord la liste en la triant grâce à l'opérateur 
    passé en paramètre
    effet de bord sur liste
    param liste la liste à trier
    type list
    param comparateur
    type function 
    '''
    # pour i allant de 0 à len(liste) - 1
    for i in range(0, len(liste) -1) :
        # on cherche le minimum de liste[i : len(liste)]
        indice_min = minimum(liste[i : len(liste)], comparateur)
        # on échange les indices i et indice_min + i
        echanger(liste, i, indice_min + i)
        


#####################################
#### TRI PAR SELECTION
#####################################
        
def inserer(liste, i, comparateur = comp_defaut):
    '''
    :Effet de bord: insère l'élément l[i] à sa place dans la tranche l[0:i+1]
    de sorte que cette tranche reste triée si l[0:i] l'est auparavant
    :param liste  une liste
    type list
    :param i indice de l'élément de l à insérer dans entre les élts d'indice 0 et i.
    type int
    :param comparateur 
    type function    
    >>> l = [1, 2, 4, 5, 3, 7, 6]
    >>> inserer(l, 4)
    >>> l == [1, 2, 3, 4, 5, 7, 6]
    True
    >>> inserer(l, 5)
    >>> l == [1, 2, 3, 4, 5, 7, 6]
    True
    >>> inserer(l, 6)
    >>> l == [1, 2, 3, 4, 5, 6, 7]
    True
    '''
    while i >0 and comparateur(liste[i - 1], liste[i]) != 1 :
        echanger(liste, i, i - 1)
        i -= 1
        
    
    

def tri_insertion(liste, comparateur = comp_defaut) :
    '''
    modifie par effet de bord la liste en la triant grâce à l'opérateur 
    passé en paramètre
    effet de bord sur liste
    param liste la liste à trier
    type list
    param comparateur
    type function
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_insertion(l)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    '''
    for i in range(1, len(liste)):
        inserer(liste, i, comparateur)


    
################################################
###### doctest : test la correction desfonctions
################################################
if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = False) 

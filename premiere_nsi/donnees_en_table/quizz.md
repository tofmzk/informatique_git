# Collecter des données 



Dans un projet précédent, nous avons réalisé un quizz en stockant les questions dans un tableau.

Nous allons maintenant structurer ces données dans un fichier _csv_ en y regroupant les questions de tous les élèves de la classe.



> * Créez un fichier _nom_prenom.csv_ dans lequel vous structurerez vos questions avec pour attributs _question_, _bonne_reponse_, _reponse_2_,_reponse_3_ et _reponse_4._
> * Dans le disque commun du groupe enregistrez votre fichier dans le repertoire spécialement créé à cet effet.
> * Créez maintenant sur votre espace personnel un fichier _quizz.csv_ regroupant les questions de tous les groupes de la classe.
> * Ouvrez ce fichier avec un tableur pour vérifier sa structure.
> * Modifier le code du projet quizz afin d'importer le fichier csv que vous venez de créer à la place de votre tableau de dix questions et testez votre programme (vous aurez besoin du _module_csv_)



____

Par Mieszczak Christophe CC - BY - SA


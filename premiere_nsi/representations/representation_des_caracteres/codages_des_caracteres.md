# Codages des caractères



## Objectifs



Nous savons coder n'importe quel entier, positif ou négatif, en base 2, 16 ou 10. Ce chapitre va aborder les différentes façons de coder les caractères.



Vous allez présentez la leçon dans un page web. Il ne s'agit pas de devenir des pros du HTML/CSS mais de comprendre comment de telles pages sont articulées tout en y présentant le cours de ce chapitre.



Il existe [énormément](https://fr.wikipedia.org/wiki/Codage_des_caract%C3%A8res#%C3%89volutions_de_l'ASCII_vers_les_jeux_de_caract%C3%A8res_cod%C3%A9s_sur_8_bits_et_la_norme_ISO_8859)  de codages différents.  Nous allons en voir 3 :

* le codage ASCII.
* le codage ISO.
* le codage unicode UTF-8.



## Page HTML 



Vous trouverez, dans le répertoire _codages de caractères_ un fichier HTML,  _codage_de_caractères.html_ , et , une page de style _style.css_. 

Pour commencer, ouvrez avec un éditeur de texte (Notepad++, Sublime_Text, Geany ...) le fichier HTML.



On y trouve la _structure_ classique d'une page HTML :

```HTML
<!DOCTYPE html>
<HTML>
	<TITLE>
		Codage des caractères
	</TITLE>
	<HEAD>
		<meta charset="utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css"  media="screen"/>  
	</HEAD>
	<BODY>
		
	</BODY>
<HTML>

	
```

* La première balise, `<!DOCTYPE html>`, précise le type du document.
* Tout le code HTML doit être placé entre les balises `<HTML>` et `</HTML>`
* les balises `<TITLE>` permettent de stipulé le tire visible dans la languette de la page.
* Les balises `<HEAD>` servent à indiquer les caractéristiques de la page :
    * **le type de codage de caractères. C'est la _méthode_ utilisée pour encoder les caractères : c'est justement l'objet de cette leçon**
    * les liens vers d'autres pages, comme la page _style.css_ dont on se servira plus tard.
* les balise `<BODY>` délimite la zone de code dans laquelle on mettra ce que l'internaute verra. C'est entre ces balises que l'on va travailler.



_Remarque :_ 

Notez les tabulations, c'est à dire les décalages : le contenu d'une paire de balises doit être tabulé d'un cran vers la droite par rapport à ces balises. Ces tabulations sont INDISPENSABLES à une bonne lecture du code.



## Le code ASCII



Voici un exemple d'aperçu de ce qu'il faut obtenir :

![aperçu page HTML](media/page html.jpg)

* Le titre général de la page est _Codages des caractères_.

	

* La première partie ici traitée a pour titre _Le code ASCII_.

	

* Un petit historique du code ASCII est précisé.

  ````markdown
  Le code ASCII est le premier type de codage de caractères utilisé en informatique. 
  A l’origine, autour de 1960, il fut décidé que les 127 caractères utilisés et présents sur le clavier (en incluant les caractères de contrôle  des premiers ordinateurs) auraient tous un code entier unique compris entre 0 et 127 : On avait besoin de 7 bits pour les coder. Ces caractères étant prévus pour être utilisés en anglais, les accents, entre autres, n’étaient pas présents … 
  
  Le code ASCII fut rapidement étendu à 256 caractères numérotés de 0 à 255 en  y ajoutant entre autre les caractères accentués. Ainsi, chaque caractère occupe un octet en mémoire c'est à dire 8 bits.  
  ````

  

* Une image pertinente (au minimum)  illustrant ce codage est présente. Sous votre image, vous ajouterai un lien vers le site source en précisant la licence de l'image ([Creative commons](https://fr.wikipedia.org/wiki/Licence_Creative_Commons), [Domaine Public](https://fr.wikipedia.org/wiki/Domaine_public), [licence libre](https://fr.wikipedia.org/wiki/Licence_libre) ...)



* Un petit tableau présente quelques exemples (vous ne prendrez pas les mêmes... ) 

	

	

* Une liste à puce présente les avantages de ce type de codage.

	```Markdown
	Un seul octet suffit pour coder tous les caractères : peu de ressources sont nécessaires.
	Ce codage convient à la pluspart des pays utilisant l'alphabet latin (le notre).
	```

	

* Une seconde liste en présente les inconvénients :

	````Markdown
	Comment faire s'il faut ajouter des caractères supplémentaires, comme un nouveau symbole monétaire par exemple ? 
	Les pays utilisant un alphabet non latin ne peuvent utiliser ce type de codage. Avec la mondialisation de l'informatique, il faut impérativement trouver une solution !
	````



## Balises nécessaires.

Nous allons commencer par placer les balises nécessaires  entre les balises `<BODY>`. Nous nous occuperons du style (couleur, arrière plan, type de police de caractères ...) seulement ensuite.



* Les balises de titre :

    ```HTML
    <H1>
    	Titre de niveau 1 (gros titre)
    </H1>
    <H2>
    	Titre de niveau 2 (plus petit)
    </H2>
    ```

* La balise _ligne de séparation_ :

    ```HTML
    <HR/>
    ```

* La balise de retour à la ligne ou retour chariot (Back Riot)

    ```html
    <BR/>
    ```

    

* Les balises _paragraphe_ qui ne contiennent que du texte :

 ```HTML
    <P>
    	ici j'écris mon texte.
    	<!-- la balise <br/> sert à aller à la ligne dans un paragraphe. -->
    </P>
 ```

* La balise image, notez qu'elle est seule : on dit qu'elle est _orpheline_ :

    ```html
    <IMG src="url de l'image" title="commentaire"/>
    ```

    

* Par défaut, les tableaux n'ont pas de bordure. On en ajoutera dans le CSS. Pour les voir, il faut, pour l'instant, utiliser la console. Voici leurs balises :

    ```HTML
    <TABLE> <!-- début du tableau -->
    	<TR> <!-- ouverture de la première ligne du tableau -->
    		<TD> 
    			cellule 1 ligne 1
    		</TD>
    		<TD>
    			cellule 2 ligne 1
    		</TD>
    	</TR> <!-- fin de la première ligne du tableau -->
    	<TR> <!-- ouverture de la deuxième ligne du tableau -->
    		<TD> 
    			cellule 1 ligne 2
    		</TD>
    		<TD>
    			cellule 2 ligne 2
    		</TD>
    	</TR> <!-- fin de la deuxième ligne du tableau -->
    </TABLE>	<!-- fin du tableau -->
    ```

* Les balises de liste ( les puces) :

    ```HTML
    <UL> <!-- série de puces non numérotées-->
    	<LI>
    		première puce
    	</LI>
    	<LI>
    		deuxième puce
    	</LI>
    </UL>
    
    <OL> <!-- série de puces numérotées-->
    	<LI>
    		première puce
    	</LI>
    	<LI>
    		deuxième puce
    	</LI>
    </OL>
    
    
    ```

* La balise de lien hypertexte :

    ```HTML
    <A href='url du lien'>
        description du lien 
    </A>
    ```

    
    
    > **En avant et amusez vous bien !!**
    >
    > **N'oubliez pas de sauvegarder de temps en temps, cela n'est pas automatique**

​    

## Un peu de style : le CSS

A côté du fichier _index.html_ se trouve le fichier _style.css_. ouvrez le avec votre éditeur de texte. Nous allons codée ici le style de la page HTML. 



Séparer le fond (en HTML) de la forme (en CSS) permet de modifier facilement l'aspect d'une page HTML sans être obligé de la réécrire. Seule la page CSS sera modifiée.



* On peut repérer les objets HTML dans la page CSS de plusieurs façon :

    * par leur balise. Toute les balises identiques seront impactée.

        ```CSS
        H1 {
        	/*ici on écrira le code du style des balises <H1>*/
        }
        ```

        

    * par un identifiant ajoutée dans la balise en HTML. Seule cette balise seraz concernée, deux balises ne pouvant partager un même identifiant.

        HTML : ajout de l'identifiant _id_

        ```HTML
        <H2 id = "titre_particulier">
        	Ceci est un titre très particulier
        </H2>
        ```

        CSS : on récupère l'identifiant en le nommant après un #

        ```CSS
        #titre_particulier{
        	/*ici on écrira le code du style de la balise qui a cet identifiant*/
        	
        }
        ```

        

    * par une classe ajoutée dans des balises en HTML. Plusieurs balises peuvent partager la même classe. Les balises de même classe seront toutes impactées par le style :

        HTML : ajout des classes _class_

        ```HTML
        <OL>
        	<LI class = "mes_puces">
                puce 1
            </LI>
            <LI class = "mes_puces">
                puce 2
            </LI>
                
        </OL>
        ```

        CSS : on récupère les classes en les nommant après un .

        ```CSS
        .mes_puces{
        	/*ici on écrira le code du style de toutes les balises qui partagent cette classe*/
        	
        }
        ```

* il faut maintenant connaître quelques fonctionnalités du CSS.

    * En ce qui concerne les couleurs :

        ```css
        color : red ; /* met la couleur en rouge */
        background-color : rgb(230, 230, 230); /*met l'arrière plan dans la couleur définie en rgb*/
        
        ```

    * En ce qui concerne le texte :

        ```CSS
        font-family : tahoma; /* choisit la police tahoma */
        font-size : 18px; /* fixe la taille de la police à 18 pixels*/
        font-weight : bold; /* met le texte en gras */
        font-style : italic; /* met le texte en italic*/
        text-decoration: underline; /* souligne le texte*/
        text-align : center; /* centre le texte dans son conteneur*/
        
        ```
    
    
    
* En ce qui concerne les dimensions, quand l'objet en a :
  
    ```CSS
    width : 300px; /* fixe la largeur à 300 pixels*/
    height : 350px; /* fixe la hauteur à 350 pixels*/
    ```
    
    
    
* En ce qui concerne les bordures :
  
    ```CSS
    
    border : 2px solid black; /* bordure de 2px continue en noir */
    border-radius : 20px; /* arrondi les angles sur 20px*/
    box-shadow : 10px 10px 10px grey; /* ajoute une ombre grise */
    ```
    
    
    ​    
    
* En ce qui concerne les marges extérieures :
  
    ``` css
    margin-left  : 0px; /* une marge de 0px à gauche, pas de marge donc*/
    margin-right  : 10px; /* une marge de 10px à droite*/
    margin-top  : 0px; /* une marge de 0px en haut, pas de marge donc*/
    margin-bottom : 10px; /* une marge de 10px en bas*/
    margin : 0px; /* supprime les marges extérieures*/
    margin : auto; /* marges automatiques (centrage)*/
    ```
    
* En ce qui concerne les marges intérieures :

    ```css
    padding-left  : 10px; /* une marge de 10px à gauche*/
    padding-right  : 10px; /* une marge de 10px à droite*/
    padding-top  : 10px; /* une marge de 10px en haut*/
    padding-bottom : 10px; /* une marge de 10px en bas*/
    padding : 0px; /* enlève les marge intérieures*/	
    padding : auto; /* marges automatiques (centrage)*/
    ```


* Si on veut que le style soit modifié lorsque la souris survole un objet (par exemple une image qui s'agrandit lorsque le curseur passe dessus), on utiliser _hover_ :

	```css
	img:hover{ /*ce style s'appliquera lorsque le souris survolera une balise img */
	    width : 900px;
	    height : 600px;
	}
	```

	

> **En avant et amusez vous bien !!**
>
> **N'oubliez pas de sauvegarder de temps en temps, cela n'est pas automatique**



## Le codage ISO 8859-1



 Le codage ISO est issu d'une volonté de normaliser le codage des caractères en utilisant toujours 8 bits et donc 255 possibilités. 

 La norme [ ISO 8859-1](http://fr.wikipedia.org/wiki/ISO_8859-1) , appelée aussi Latin-1 ou Europe occidentale, est la  première partie d'une norme plus complète appelée ISO 8859 (qui  comprend 16 parties) et qui permet de coder tous les caractères des  langues européennes ... ou presque car elle omet quelques caractères comme  la ligature œ.



La norme 8859-1 satisfera la plupart des européens mais ne résout pas les limitations du code ASCII :

* On n'a toujours ''seulement'' 255 caractères disponibles et donc les mêmes avantages que ceux du code ASCII.
* Il existe différents tableaux pour couvrir les besoins en caractères d'à peu près tout le monde sur terre 
* Puisqu'il y a plusieurs norme,  cela engendre des problèmes : 
	* laquelle choisir ? En cas de mauvais choix, la page sera illisible.
	* un caractère même peut être codé différemment dans deux normes différentes !
* Il n'est pas possible de coder dans la même page avec différents alphabets, le codage ISO imposant d'en choisir un ! Impossible donc d'écrire en français, en russe et en mandarin dans une même page web.



> Dans votre page HTML :
>
> * Créer une deuxième partie intitulée _le codage ISO 8859-1_
> * En vous inspirant de la première partie, présentez y le codage ISO, ses avantages et inconvénients. La page doit contenir une image et des puces, **au minimum**
> * Vous pourrez parcourir le web pour vous documenter davantage.



## Le codage UNICODE



L'[Unicode](https://fr.wikipedia.org/wiki/Unicode) est un standard informatique qui permet des échanges de textes dans **différentes langues**, à un niveau mondial.  Le répertoire Unicode peut contenir plus d’un million de caractères, ce qui est bien trop grand pour être codé par un seul octet.



Là où le code ASCII utilisait 7 bits (128 caractères) puis, comme l'ISO8859-1,  8bits, l' Unicode s'affranchit de cette limite en ouvrant la possibilité de coder sur _plusieurs_ bits. Combien ? Comment ? Ca dépend du codage Unicode utilisé car il en existe plusieurs. 



Le codage le plus utilisé à ce jour sur le Web est l'_[UTF-8](https://fr.wikipedia.org/wiki/UTF-8)_ pour _Universal character set Transformation Format_ :

* Si le premier octet commence par un _0_ alors il s'agit d'un codage sur un seul octet et  les 7 bits suivant codent les 127 caractères du code ASCII initial. Ce sont les caractères les plus utilisés au monde et ce système permet donc de les utiliser simplement avec un minimum de ressource. 

  * **0**1000001 code donc un un octet le caractère correspondant à 1000001, c'est à dire 65, en ASCII, c'est à dire A.

  * le codage sur un octet est donc de la forme  `0xxxxxxx` 

      

* Si le premier octet comment par un **110** alors le caractère est codé sur deux octets. Le deuxième octet doit commencer par **10** :

  * **110**00000 **10**000000 correspond donc à un codage sur 2 octets qui dispose de  11 bits libres soit 2048 caractères possibles, ce qui fait déjà _pas mal_ ... On en utilisera rarement d'autres.
  * Le codage sur deux octets est donc de la forme `110xxxxx 10xxxxxx`

  

* On peut continuer ainsi sur 3, 4 octets, ou plus :

	*  `1110xxxx 10xxxxxx 10xxxxxx`  : sur 3 octets pour 16 bits libres et ??? caractères encodables.

	*  `11110xxx 10xxxxxx 10xxxxxx 10xxxxxx` : sur 4 octets pour 21bits libres et ??? caractères encodables.

	
	

_Remarques :_

* Les 128 premiers caractères unicodes correspondent aux 128 caractères les plus utilisés au monde. Ainsi, la plupart des caractères d'un texte sont codés sur un seul octet et la taille du fichier est maitrisé.
* Les caractères accentués (entre autres) sont codé sur 2 octets. C'est pour cela qu'ils sont décodés bizarrement si on essaie de les lire en ISO : chaque bit est alors compris comme un caractère à part entière et vous en voyez donc deux ou lieu d'un.
* Les caractères ne sont pas tous alphabétiques, on y trouve également des symboles ou des dessins. Ils sont rangés par blocs (qu'il est possible de visualiser en utilisant par exemple _libreOffice_ en insérant des caractères spéciaux) . Pour afficher un caractère unicode, il faut utiliser le raccourci clavier `CTRL + MAJ + u` puis entrer le code du caractère souhaité (par exemple 2603 pour un joli bonhomme de neige ☃).



Python utilise le codage UTF-8. On pourra donc utiliser ses symboles dans nos interfaces en mode texte pour les rendre plus jolies. En revanche, il faudra se méfier de certains éditeurs (edupython...) qui n'encodent pas forcément en UTF-8.



Il existe des variantes de l'UTF-8, comme l'UTF-32 qui codent tous les caractères 4 octets. L'avantage est alors que chaque caractère est codé sur un nombre constant d'octets mais un texte prendra 4 fois plus de place qu'en codage ISO et beaucoup plus qu'en UTF-8.



> Dans votre page HTML :
>
> * Créer une nouvelle partie intitulée _le codage Unicode UTF-8_
> * En vous inspirant des parties précédentes, présentez y ce codage, ses avantages et inconvénients. La page doit contenir un tableau, une image et des puces, **au minimum**
> * Expliquez y le principe du codage sur plusieurs octets.
> * Présentez quelques caractères particuliers (emoji, caractères spéciaux ...)
> * Vous pourrez parcourir le web pour vous documenter davantage.





## Pour conclure sur le codage des caractères



L'UTF-8 est aujourd'hui le codage le plus universel et le plus utilisé. Un jour, peut-être, cela évoluera.

![evolution](https://upload.wikimedia.org/wikipedia/commons/a/a9/UnicodeGrow2b.png)



Vous savez maintenant pourquoi notre page HTML précise le type de codage des caractères dans la ligne :

````html
<meta charset="utf-8" />
````



Cette précision n'est pas exclusive au web : lorsque vous utiliser un éditeur, Notepad++, Geany, Thonny ou Edupython [...], le type de codage doit être précisé ou c'est le bug assuré. 



```` python
UnicodeEncodeError: 'ascii' codec can't encode character '\xe0' in position 49059: ordinal not in range(128)
````



Par défaut, la plupart des éditeur de texte utilise le codage UTF-8. Certains sont moins rigoureux et il faut être très vigilant ! 



Dorénavant, en haut de notre code Python, nous préciserons, pour éviter tout problème, le codage utilisé et choisiront l'UTF-8, même s'il est choisi par défaut par Python3

````Python
# -*- coding: utf-8 -*-
````

ou 

```Python
# coding: utf-8
```

>  
>
> Réaliser une dernière partie à votre page web avec cette conclusion.
>
>  

### Dernière remarque : les polices de caractères



* lien codage <-> police
* sans sérif ou sérif (sans ou avec le petit trait en plus de chaque côté de la lettre ) 
* monospace (mono) : largeur de police constante.





___________

Par Mieszczak Christophe CC BY SA

source image :

* première image : production personnelle 
* Graphique : [ wikipédia (CC BY SA)](https://commons.wikimedia.org/wiki/File:UnicodeGrow2b.png), [auteur : Krauss](https://commons.wikimedia.org/wiki/User:Krauss)








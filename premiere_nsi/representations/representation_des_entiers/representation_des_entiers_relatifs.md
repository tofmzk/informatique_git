# Les nombres entiers _relatifs_ et la base 2



## On aurait pu faire comme ça...

###  mais ça ne marche pas bien donc finalement non.



La première idée, pour coder un entier négatif, serait d'ajouter à gauche un bit :

* qui vaut 0 si le nombre est positif.
* qui vaut 1 si le nombre est négatif.



Par exemple, comme $`4 = 100_2 = 0100_2`$  on aurait, par conséquent,$`-4= 1100_2`$



Ca a l'air pas mal, simple, rapide... Il y en a qui ont essayé mais ils ont eu des problèmes....



_Exemple  :  additionnons 4 et 2, deux entiers positifs, en binaire_ 

​	$`4 = 100_2`$ et $`2 =010_2`$ , posons l'addition en base 2 :

![addition 4 + 2](media/add1.jpg)

​	Comme $`110_2`$ = 6 : ça fonctionne très bien.



_Exemple  : additionnons 4 et -2 en binaire sur 4 bits_



​	4 = $`0100_2`$ et -2 = $`1010_2`$ (on a choisit de coder sur 4 bits), posons l'addition en base 2 :

![addition 4 + - 2](media/add2.jpg)

​	Oups, sur 4 bits, $`1110~2`$=- 6  là ça coince .......... **Le codage ne convient pas !**



## Le complément à 2

Vous l'avez compris, ce n'est pas si simple. Il nous faut trouver un codage qui soit homogène avec l'addition.    



* Tout d'abord, on va décider sur combien de bits on code les entiers. Le bit de poids fort (à gauche) déterminera bien le signe de l'entier, 0 pour un positif et 1 pour un négatif. Bien entendu, le nombre de bits déterminera la taille maximum de l'entier à coder.

* Si l'entier est positif, on le code comme précédemment. on place cependant en plus un bit de poids fort à 0 et l'entier.

* Si l'entier est négatif, c'est là qu'on ruse :
	* on code l'entier opposé (sans le signe -) qui est positif comme vu plus haut.
	* on inverse les 0 et les 1 sur la série de bits obtenue : c'est le complément à 2.
	* on ajoute 1 (sans tenir compte du dépassement) pour obtenir le le codage en complément à 2 sur le nombre de bits choisi.



_Exemple :_



* Je décide de coder sur 4 bits. Le plus grand positif que je peux ainsi obtenir est 0111 :
	* **0**111 est positif car le bit de poids fort est 0
	* $`0111_2 = 1 \times 2^2 + 1 \times 2^1+ 1 \times 2^0 = 7`$  est le plus grand entier positif qu'on peut obtenir sur 4 bits.

	

* L'entier positif 4, sur 4 bits , se code comme d'habitude, 0100 .

    

* Pour coder -2 :
	* on remarque que, sur 4 bits, +2 = 0010
	* on prend le complément à 2 en inversant 0 et 1 et on obtient 1101
	* on ajoute 1  pour obtenir le codage en complément à 2 sur 4 bits de -2 qui est $`1110_2`$

![addition 0010 + 1](media/add3.jpg)

* Ainsi $`4=0100_2`$ et $`-2=1110_2`$. Reste à vérifier l'addition 4+ (-2) :

![ça marche](media/add4.jpg)

On obtient$` 0010_2 = 2`$ et l'addition fonctionne puisque, en effet, 4 + (-2) = 2



Un petit tableau pour bien comprendre :_positifs_

| bit de signe |       |       |       |       |       |       |       |       |          |
| ------------ | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | -------- |
| **0**        | **1** | **1** | **1** | **1** | **1** | **1** | **1** | **=** | **127**  |
| **0**        | *…*   | **=** | *…*   |       |       |       |       |       |          |
| **0**        | **0** | **0** | **0** | **0** | **0** | **1** | **0** | **=** | **2**    |
| **0**        | **0** | **0** | **0** | **0** | **0** | **0** | **1** | **=** | **1**    |
| **0**        | **0** | **0** | **0** | **0** | **0** | **0** | **0** | **=** | **0**    |
| **1**        | **1** | **1** | **1** | **1** | **1** | **1** | **1** | **=** | **−1**   |
| **1**        | **1** | **1** | **1** | **1** | **1** | **1** | **0** | **=** | **−2**   |
| **1**        | *…*   | **=** | *…*   |       |       |       |       |       |          |
| **1**        | **0** | **0** | **0** | **0** | **0** | **0** | **1** | **=** | **−127** |
| **1**        | **0** | **0** | **0** | **0** | **0** | **0** | **0** | **=** | **−128** |

_Remarque :_

* Sur 8 bits, on code les nombres sur 7 bits, le premier bit étant reservé au signe :

    * le plus grand nombre codable est $`01111111_2 = 2^7 - 1 = 127`$.
    * le plus petit nombre codable est $`10000000_2 = - 2^7 = -128`$.

* Sur _n_ bits, on utilise le premier bit pour le signe et il reste _n_ - 1 bits disponibles :

    * si le premier est un 0, le plus grand nombre positif codable est 0 suivi de _n_ - 1 bis égaux à 1. Il vaut $`2^{n - 1} - 1`$
    * si le premier est un 0, le plus petit nombre négatif codable est 1 suivi de _n_ - 1 bits égaux à 0. Il vaut $`-2^{n - 1}`$

    

>  **Exercice :** 
>
>  On code en complément à 2 sur 8 bits.
>
>  * Donner une représentation de 0.
>  * Donner une représentation de -0.
>  * Quel est le plus grand et le plus petit entier possible codable ainsi ?



> **Exercice :**
>
> On code en complément à 2 sur 8 bits.
>
> * donner l'écriture binaire de 42
> * En déduire l'écriture binaire de -42
> * En déduire l'écriture binaire de - (-42)  : que remarquez-vous ?



>  **Exercice :**
> 
>  En complément à 2 sur 5 bits :
> 
>  * quel est le plus grand entier positif représentable ?
>  * coder 14, -8 , 5 et -6
>  * A quoi correspondent en base 10 les nombres : $`01011_2, 10010_2, 100_2`$ ? 







________

Par Mieszczak Christophe CC BY SA

images : source personnelle




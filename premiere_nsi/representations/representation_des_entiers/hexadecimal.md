# Base 16 : l'Hexadécimal

### La base 16 ?



La base 2, c'est parfait pour les ordinateurs. Mais pour nous, pauvres humains, la nécessité d'utiliser un grand nombre de 1 et de 0 les uns derrières les autres est vite rédhibitoire !



Pour écrire les nombres entiers de façon plus condensé, il faut utiliser une base avec un plus grand nombre de symboles.  



**La base 16, l' hexadécimal, est un excellent candidat !!**

**En base 16, les 16 symboles utilisés sont : 0, 1, 2, 3, 4 , 5, 6, 7, 8, 9 , A, B, C, D, E et F.**



> **Exercice :**
>
> Convertissez en base 10 : A, B, C, D, E et F.



>  **Exercice :**
>
>  Comptez jusqu'à 20 en hexadécimal



>  **Exercice :**

>  Convertissez en base 10 : 
>
>  * $`A1_{16}`$
>  * $`FF_{16}`$



**Ainsi, si nous avons besoin de 8 bits pour coder un entier entre 0 et 255, 2 symboles suffisent en hexadécimal !!**



_Remarque :_

Pour passer directement de base 2 à base 8, il suffit de grouper les bits par 4. En effet $`2^4=16`$ et donc un paquet de 4 bits est représenté par un symbole en base 16.



$`11101001_2 =1110.1001_2`$ :	

* $`1110_2 = 14 = E_{16}`$

* $`1001_2=9=9_{16}`$

    

    Ainsi  $`11101001_2 = E9_{16}`$

    



_Encore une remarque :_

​	On fait la même chose dans l'autre sens !

​	soit $`FA_{16}`$ :

* $`F_{16}`$ =
* $`A_{16} =`$ 



​	Ainsi  $`FA_{16}`$ =

### Editeur Hexadécimal





_________

Par Mieszczak Christophe

Licence CC BY SA








































# Les nombres entiers _positifs_ et la base 2

## Un peu d'histoire

Les premières machines programmables étaient des automates programmables et mécaniques.

Les  premiers ordinateurs électroniques sont apparus dans les années 40. 

Le  transistor, composants principaux de nos micro-processeurs, restant encore à inventer, ils utilisaient des tubes à vide, communément appelés *lampes* ,  pour effectuer les opérations demandées. 

![tube à vide - wikipédia - domaine public](https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/VacuumTube1.jpg/100px-VacuumTube1.jpg)



La taille de ces lampes induits des ordinateurs très volumineux. Parmi les plus connus de ces  précurseurs, L'ENIAC (1946) comportait 18000 tubes à vide et pesait 30  tonnes pour une capacité de mémoire de 20 nombres de 10 chiffres et une  puissance de calcul permettant d'effectuer 5000 opérations par seconde  (contre plusieurs milliards sur les ordinateurs modernes).



 Pour programmer ces "monstres", il fallait manipuler des milliers  d'interrupteurs et de câbles et, de temps à autre, remplacer les tubes  ayant "grillé" lorsque des insectes commettaient l'erreur (fatale) de  vouloir se réchauffer contre eux, créant au passage l'origine de l'expression _avoir un bug_.



![ENIAC 1947 - source wikipédia - domaine public](https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Classic_shot_of_the_ENIAC.jpg/300px-Classic_shot_of_the_ENIAC.jpg)





Pour stocker un chiffre décimal (0 à 9), on utilisait alors 10  lampes, dont une seule à la fois était  active (ici pour représenter 4) :

 

![amploules en base 10 - source personnelle](media/ampoules_base_10.jpg)



Cette méthode de représentation était assez inefficace, puisque  n'utilisant à un moment donné que 10% des ressources dédiées à la  mémorisation d'un chiffre. Un autre mode de représentation, déjà utilisé  par certains concurrents de l'ENIAC, s'est rapidement imposé : le **binaire**, autrement dit la représentation en base 2 :

 

![ampoules en base 2 - source personnelle](media/ampoules_base_2.jpg)



Dans ce mode de représentation, 8 lampes permettent de représenter 256 valeurs différentes, au lieu de 10 auparavant. Par contre, la base  2 n'est pas naturelle pour l'être humain, habitué à compter en base 10.



Pour interpréter le résultat d'un calcul, il est donc nécessaire d'en  obtenir une représentation en base 10. Inversement, pour fournir des  données à un programme, nous préférons la plupart du temps les entrer  sous forme décimale. Pour que ces données soient utilisables par un  ordinateur travaillant en binaire, une étape de conversion est également  nécessaire.





## Compter en base 2



Les ordinateurs travaillent en base 2, aussi appelé binaire. Ils n'utilisent que deux symboles , 0 et 1. Comment coder en n'utilisant que ces deux symboles toutes les choses que nous utilisons quotidiennement en informatique, les nombres, les caractères, les images, les sons, les vidéos [etc ...] ?



Allons-y doucement et commençons à regarder comment cela se passe pour les nombres entiers. 



En fait, tout se passe comme pour les _Shadoks_ : on utilise les deux symboles 0 et 1 de la même façon que nous avons utilisé les quatre symboles **GA, ZO, BU, MHEU**. 



Complétez le tableau ci-dessous.



| Base 10 |                            Base 2                            |
| :-----: | :----------------------------------------------------------: |
|    0    |                         Facile **0**                         |
|    1    |                        Pas dur **1**                         |
|    2    | Plus de symbole !  Je fais donc une _petite poubelle_ contenant deux symboles et 0 symbole à côté ce qui me donne **10** |
|    3    |                      On continue **11**                      |
|    4    | Je fais une _super poubelle_ de 2 * 2 symboles avec 0 _petite poubelle_ de 2 symboles et 0 symbole à côté ce qui me donne **100** |
|    5    |                             101                              |
|    6    |                             110                              |
|    7    |                                                              |
|    8    |                                                              |
|    9    |                                                              |
|   10    |                                                              |
|   11    |                                                              |
|   12    |                                                              |



Noter `4=100` est mathématiquement faux. Il faut préciser la base, si le nombre exprimé ne l'est pas en _base 10_. Pour cela, on note la base utilisée en indice après le nombre. S'il n'y a pas d'indice, c'est qu'il s'agit d'une _base 10_ . Ainsi $`4 = 100_2`$.



## Paquets de bits

Bien souvent, on regrouppe les bits par paquet.

* Le bit de droite (1001010**0**) est appelé **bit de poids faible** car c'est celui qui a le moins de _poids_ dans la conversion en base 10.

* Le bit de gauche (**1**0010100) est appelé bit de **poids fort** car c'est celui qui a le plus de _poids_ dans la conversion en base 10.

    

Le regroupement le plus courant est l' **octet** qui est constitué de 8 bits.

_Exemple :_

$`10010100_2`$ est un octet dont :

* le bit de poids fort est 
* le bit de poids faible est 



_Remarques :_

* Le plus petit octet est  $`00000000_2 = 0`$ et le plus grand $`11111111_2 = 255`$.

* Ne pas confondre bit et byte :
    * 1 bit vaut 0 ou 1
    * un byte est groupe de bits appelé _mot_. Généralement il contient 8 bits mais ce n'est pas toujours le cas.



_Autres unités basées sur l'octet :_

Vous avez forcément déjà entendu parler de ces unités :

* un octet = 8 bits
* un kilooctet = 1000 octets  = $`10^3`$ octets
* un mégaoctet = 1000 kilo-octets = $`10^6`$ octets
* un gigaoctet = 1000 méga-octets = $`10^9`$ octets
* un téraoctet = 1000 giga-octets = $`10^{12}`$ octets



## De la base 2 à la base 10

Écrivez un algorithme permettant de réaliser cette conversion, c'est à dire une _recette de cuisine_ qu'il suffit de suivre pas à pas pour convertir un entier positif écrit en base 2 vers son écriture en base 10.



_Remarque :_ 

Une bonne connaissance des puissances de 2 est un gros avantage :

|  n   | 2^n  |
| :--: | :--: |
|  0   |  1   |
|  1   |  2   |
|  2   |  4   |
|  3   |  8   |
|  4   |  16  |
|  5   |  32  |
|  6   |  64  |
|  7   | 128  |
|  8   | 256  |
|  9   | 512  |
|  10  | 1024 |
|  11  | 2048 |
|  12  | 5096 |



Appliquer cet exercie pour trouver l'écriture en base 10 des nombres suivants :

* $`10011_2`$=
* $`1001000_2=`$

* $`0000111_2=`$



## De la base 10 vers la base 2.

### Par soustractions successives

C'est probablement l'algorithme le plus intuitif, à défaut d'être le plus efficace, pour convertir un entier _n_  écrit en base 10 dans son écriture en base 2.



* si _n_ vaut 0, sa conversion en binaire est $`0_2`$

* sinon on va écrire le nombre binaire recherché **de gauche à droite** en suivant l'algorithme ci-dessous :

    * On repère _p_, le plus grand entier tel que de $`n \geq 2^p`$ .

    * Tant que _p_ > 0  :

        * Si $`n \geq 2^p`$ alors :

            * le bit obtenu vaut 1
        * _n_ diminue de $`2^p`$ 
        * Sinon : 

            * le bit obtenu est 0

        
        * p diminue de 1
        
        

_Exemple : n = 18_



* _n_ ne vaut pas 0 donc on continue
* $`2^4 = 16~et~2⁵ = 32`$ donc _p_ = 4
* la boucle _tant que_ commence :

| p =  | $`2^p =`$ |  n=  | $`n \geq 2^p`$  ? |       conséquence(s)        | continue ? |
| :--: | --------- | :--: | :---------------: | :-------------------------: | :--------: |
|  4   | 16        |  18  |        oui        | n = 18 - 2⁴ = 2 et bit1 = 1 |    oui     |
|      |           |      |                   |                             |            |
|      |           |      |                   |                             |            |
|      |           |      |                   |                             |            |

Ainsi $`18 = 1???_2`$



> Faites tourner cet algorithme pour :
>
> * n = 9
> * n = 31
> * n = 227



_"Petite question :_ :

Dans cet algorithme, qu'est-ce qui nous permet d'affirmer que la boucle **Tant que** va se terminer à un moment ou un autre ?



 

> Un entier positif décroissant tel que lorsqu'il atteint 0 alors la boucle se terminera s'appelle un **variant de boucle**. 



#### Par divisions successives



Voici un algorithme très performant de conversion qui est, en revanche, un peu plus fin à comprendre. Nous allons effectuer une série de division euclidienne (avec dividende, diviseur, quotient et reste) par 2. **Chaque fois, le reste ne peut être que 0 ou 1**.



Reprenons, par exemple, le nombre entier en base 10 _n_ =75. Nous allons le diviser par 2.

![75/2](media/div1.jpg)

75 divisé par 2 vaut 37 et il reste  1. 

Divisions maintenant 37 par 2.

<img src="media/div2.jpg" alt="75/2" style="zoom:150%;" />

37 divisé par 2 vaut 18 et il reste 1.



On poursuit ainsi les divisions jusqu'à obtenir un quotient nul (en rouge).



<img src="media/div3.jpg" alt="75/2" style="zoom:150%;" />

Il n'y a plus qu'à remonte la série des restes pour obtenir l'écriture binaire de 75 : $`75 = 1001011_2`$

Ici, on n'obtient pas un octet mais seulement 7 bits. Si on veut écrire 75 sur un octet, on ajoute le 0 _muet_ à gauche : $`75=01001011_2`$



> Sur le même modèle que l'algorithme par soustraction, écrivez en français la marche à suivre pour convertir un entier positif de la base 10 vers la base 2.
>
> Appliquez le pour convertir 9, 31 et 227.



> 
>
> Quel est cette fois ci le variant de boucle (celui qui permet d'être certain que votre boucle s'arrête) ? 
>
>  
>
> 



Très bientôt, nous implémenterons ces algorithmes en Python...

_________________

Par Mieszczak Christophe 

licence CC - BY - SA

source images : wikipedia (domaine-public) ou personnelles.






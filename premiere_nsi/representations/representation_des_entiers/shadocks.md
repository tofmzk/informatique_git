# Les Shadoks apprennent à compter.



Un petit lien vers le site [WIKI Shadocks](https://shadoks.fandom.com/fr/wiki/Calcul_(Shadoks)) pour essayer de comprendre comment les Shadocks réussissent à compter avec les seuls 4 mots de leur bien maigre vocabulaire : **GA BU ZO** et **MEU**.



Pour compter en Shadock :

**0 GA**  et on écrit O

**1 BU** et on écrit −

**2 ZO** et on écrit ⨼

**3 MEU** et on écrit ◿

**4  ???  plus de symbole !!**



> >  Mais alors , comment écrire 4 en _Shadoks_ ?



* on met 4 shadocks dans une poubelle et on dit qu'on a une poubelle soit **BU** poubelle.
* pour ne pas confondre avec le **BU** du départ, on dit qu'il n'y a pas de shadock à côté de la poubelle soit **GA** shadock à côté



4 s'écrit **BUGA**



> > Comptez jusqu'à neuf en _Shadoks_.

| 0    | GA   |
| ---- | ---- |
| 1    | BU   |
| 2    | ZO   |
| Mh   | MEU  |
| 4    |      |
| 5    |      |
| 6    |      |
| 7    |      |
| 8    |      |
| 9    |      |
| 10   |      |





* Les _Shadoks_ utilisent 4 symboles pour compter : leur système numérique est en _base 4_ .

* Nous, humains du XXIème  siècle, en utilisons 10, nos dix symboles étant nos 10 chiffres 0,1,25,3,4,5,6,7,8 et 9 : notre système numérique est en _base 10_.

*  On peut ainsi compter dans différentes bases. Les babyloniens, de grands commerçants qui avaient besoin de manipuler de grands nombres, comptaient en bases 60 avec ... 60 symboles. 

  

OK. Nous savons compter en _base 4_ à la mode des _Shadoks_. Mais comment retrouver le nombre entier en base 10 correspondant à un nombre en _Shadok_ ? C'est simple. Considérons, par exemple, un nombre de **ZO MEU GA** _Shadoks_ :

* **GA** , qui correspond à 0, est le nombre d'unité : il y en a donc 0 _Shadok_ ici.
* **MEU**, qui correspond à 3,  est le nombre de petites poubelles contenant 4 _Shadoks_ :  il y a donc $`3 \times 4=12`$ _Shadocks_ ici.
* **ZO**, qui correspond à 2, est le nombre de grandes poubelles contenant chacune 4 petites poubelles de 4 _Shadoks_ : il y a donc $`2 \times 4 \times 4 = 2 \times 4^2 = 32`$ _Shadoks_ ici.



Ainsi  **ZO MHEU GA** _shadoks_ correspond, en _base 10_, à $`0 \times 4^0 + 3 \times 4^1 + 2 \times 4^2 = 44`$  _Shadoks_



> > A quel nombre en base 10 correspondent les nombres _Shadoks_ **BU BU ZO** et **MEU GA BU** ?



BUBUZO = 



MEUGABU = 



> > Saurez vous sauver le professeur du bûcher,même si brûler le professeur est intéressant aussi faut dire, en révélant le nombre écrit sur le panneau ?



BUZOGAMEU =

****





> > Plus difficile maintenant, le professeur a dénombré 21 shadoks hostiles à son système de numération et 94 shadoks favorables. Saurez vous écrire ces 2 entiers en écriture shadok ?



21 = 



94 = 

__________
Par Mieszczak Christophe licence CC BY SA
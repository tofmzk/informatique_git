# Le codage des entiers 

**Exercice 1 :**

Donner l'écriture décimale des entiers ci-dessous positifs codés en binaire sur un octet sans complément à 2.



$`00001000_2`$ = 

$`00001011_2`$ =

$`10000001_2`$=

$`11111110_2`$

$`01000010_2`$ =

________



**Exercice 2 :**

Donner l'écriture binaire des entiers positifs ci-dessous sur un octet sans complément à 2.



* 203 = 

* 47 = 

* 23 = 

* 237 = 

* 33 =

* 24 =

* 128 =

* 255 =

	

__________



**Exercice 3** :

Poser les additions de ces nombres binaires, tous positifs et codés sans complément à 2. Donner la valeur en base 10 du résultat.



* $`10_2+10_2`$
* $`101_2+11_2`$
* $`10001_2+11010_2`$
* $`1101_2+10101_2`$



__________



**Exercice 4 :**

Donner l'écriture décimale des entiers ci-dessous codés en complément à 2 sur un 5 octets.



* $`10110_2`$
* $`01001_2`$
* $`11110_2`$
* $`00110_2`$
* $`10011_2`$



___________



**Exercice 5 :**

Donner l'écriture binaire des entiers positifs ci-dessous en complément à 2 sur un 5 octets.



* 12
* -48
* -27
* 29
* -16



__________



**Exercice 6 :**

Convertir en hexadécimal :

* 203 =

* 47 = 

* 23 = 

* 237 = 

* 33 =

* 24 =

De même avec les entiers positifs ci-dessous codés sans compléments à 2.

* $`10110010_2`$

* $`10011001_2`$

	

_________



**Exercice 7 :**

Convertir en base 10.

* $`1F_{16}`$
* $`13_{16}`$
* $`45_{16}`$
* $`FF_{16}`$
* $`FAB_{16}`$

* $`ABCD~_{16}`$



_________



**Exercice 8 :**

Convertissant en base 10.



- Un homme a $`14_{16}`$ doigts, si on compte les orteils !

- En zone résidentielle, la vitesse est souvent limitée à $`1E_{16}`$ km/h.

- La vitesse sur les autoroutes françaises est limitée à $`82_{16}`$ km/h.

- Dans des conditions normales, l'eau bout à $`64_{16}`$ ºC.

- Le revenu annuel médian en France s'élève à $`6EA_{16}`$ euros.

- La population mondiale dépasse les $`1A0 000 000_{16}`$ habitants



___________



**Exercice 9 : QCM**



1. En binaire, un entier positif pair se termine toujours par un 0.

  [ ] Vrai	[ ] Faux	[ ] Vrai sauf en complément à 2

  

2. Sur un octet, le plus grand nombre positif que l'on peut écrire sans complément à 2 est :

  [ ] 255		[ ] 256 	[ ] 64	[ ] 65

  

3. Sur un octet, le plus grand nombre positif que l'on peut écrire en complément à 2 est :

​        [ ] 255		[ ] 256 	[ ] 64	[ ] 65



4. Le nombre codé $`1011001_2`$ représente, en base 10, :

	[ ] Obligatoirement un nombre négatif	

	[ ] Un nombre négatif s'il s'agit d'un codage en complément à 2 sur 7 bits

	[ ] Obligatoirement un nombre positif     

	

5. Combien de bits faut-il au minimum pour coder tout nombre entier entre -65 et 64 en complément à 2 ?

	[ ] 5		[  ] 6  	[ ] 7 	[ ] 8 	[ ] 9





___________

Par Mieszczak Christophe

Licence CC BY SA  

  

  




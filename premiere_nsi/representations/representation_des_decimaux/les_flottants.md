 # Codage des décimaux : les flottants



## Petit rappel : la base 10



Intéressons nous à l'écriture d'un nombre décimal en base 10, comme par exemple 223,25



* Nous savons tous ici, en base 10, que 223 se compose de 3 unités, 2 dizaines et 2 centaines. Cela vient de la série de divisions ci-dessous,  sur laquelle les restes fournissent le nombre d'unités (3), de dizaines (2) et de centaines (2 aussi)

    ![partie entière base 10](media\div1base10.jpg)

    On écrit alors 223 = 2 x 100 + 2 x 10 + 3 : c'est sa décomposition en base 10

    

* Pour la partie décimale, 0.25, en base dix également, on a l'habitude de lire 2 dixièmes et 5 centièmes. Cela vient cette fois de multiplication par 10 :

    * 0,25 x 10 = 2.5 : la partie entière indique le nombre de dixièmes (2) et la partie décimale restante est 0,5
    
    * 0,5 x 10 = 5 : la partie entière indique le nombre de centièmes (5) et il n'y a plus de partie décimale à _explorer_.
    
        On vient de voir que $`0,25 = 2 \times \frac{1}{10} + 5 \times \frac{1}{100}`$
    
        



Ainsi on peut écrire :  $`223,25 = 2 \times 100 + 2 \times 10 + 3 + 2 \times \frac{1}{10} + 5 \times \frac{1}{100}`$



En **notation scientifique**, on obtient : $`223,25= 2,2325 \times 10²`$



## En base 2



* Pour la partie entière, c'est exactement le même principe mais  en divisant cette fois par 2 : 

    ![partie entière base 10](media\div1base2.jpg) 

    On écrit alors : $`223 = 1 \times 2⁰ + 1 \times 2¹ + 1 \times 2² +1 \times 2³ + 1 \times 2⁴ + 0 \times 2⁵ + 1 \times 2⁶ +1 \times 2⁷`$ : C'est sa décomposition en base 2.

    Ainsi $`223 = 11111011_2`$ 

    

* Pour la partie décimale, on procède comme en base 10 mais en multipliant par 2 au lieu de multiplier par 10 :

    * 0.25 * 2 = **0**.5 : la partie entière vaut **0**, la partie décimale restante est 0.5
    * 0.5 * 2 = **1** : la parie entière vaut **1**. il n'y a plus rien après la virgule donc nous avons terminé

    On vient de voir que  $`0.25 = 0 \times 2^{-1} + 1 \times 2^{-2}`$ : c'est sa décomposition en base 2

    

Ainsi, on peut écrire :

* $`123,25 =  11011111,01_2`$
* En notation scientifique en base 2 : $`123,25 =  1,111101110_2 \times 2 ^7`$



> **Exercice : **
>
> Trouvez l'écriture scientifique en base 2 des nombres décimaux ci-dessous :
>
> * 8,5
> * 12,125





## C'est là qu'est l'os



Cherchons maintenant l'écriture scientifique de 0,1 en base 2.



* 0,1 * 2 = 0,2 : partie entière **0** , la partie décimale restante est 0,2
* 0,2 * 2 = 

















**!! SPOILER plus bas, ne défilez pas avant d'avoir trouvé ! !!**





































**On ne peut coder de façon exacte le nombre 0,1 en base 2.**

**Et ce n'est pas le seul dans ce cas : la plupart des décimaux sont dans le même cas !**

**On est donc contraint de faire une approximation !!**





Allons un peu dans la console de Thonny :

```Python
>>> 0.1 + 0.1 + 0.1 == 0.3
???

```



Ce n'est pas une erreur ou un bug de Python mais la conséquence d'une approximation liée au codage en base 2 :

```python
>>> 0.1 + 0.1 + 0.1
???
```





> **Il n'est pas possible de coder un nombre décimal en valeur exacte en base 2**.
>
> **On obtient une approximation du nombre décimal, et non le nombre en lui même.**
>
> **Cette approximation est appelée _nombre en virgule flottante_ et correspond au type _float_ en Python.**



## IEEE754



_Résumé des épisodes précédents_ :



Les nombres décimaux sont codés selon [la norme IEEE754](https://fr.wikipedia.org/wiki/IEEE_754) en informatique. Le principe est celui d'une notation scientifique en base 2 :



**Nombre = `bit de signe`. `mantisse` $`\times 2^{exposant} `$**



| 1 bit de signe |                      Exposant : 11 bits                      |     mantisse : 52 bits     |
| :------------: | :----------------------------------------------------------: | :------------------------: |
|   0(+), 1(-)   | Dans l'intervalle [-1022;1023], codé non pas en complément à 2 mais avec un décalage ([voir wikipédia pour entrer dans les détails](https://fr.wikipedia.org/wiki/IEEE_754)) | Partie décimale en binaire |



**Un décimal codé selon ce format est dit _codé en virgule flottante_ et est donc du type _flottant_ (_float_).**



_Remarque :_



L'erreur potentielle est faible : elle est de l'ordre de $`2^{-52}`$ soit environ $`10^{-16}`$ , c'est à dire 16 chiffres après la virgule. On a de la marge...



## Conséquence



**Le test d'égalité formelle ( le '==') entre deux flottants n'a AUCUN SENS !**



On va être contraint, si on en a besoin, de les comparer de façon approximative en utilisant moins de 16 chiffres après la virgules. Une dizaines suffiront largement.



> **Exercice :**
>
> Ecrivez, en Python, une fonction `approx` ,de paramètres _nombre1_ et _nombre2_ (les flottants à comparer) et _precision_ (un entier donnant le nombre de chiffres après la virgule souhaitée entre 0 et 16).
>
> * Cette fonction reverra `True` si $`|nombre1 - nombre2|< 10^{-precision}`$ et `False` sinon.
>
> * Documentez la fonction
>
> * Créez plusieurs DocTests renvoyant soit True soit False.
>
>   
>
>   _Remarque :_
>
>   On utilisera la fonction valeur absolue : [`abs`](https://www.w3schools.com/python/ref_func_abs.asp)
>
>   



Si nous avons besoin de comparer des flottants, on utilisera donc cette fonction.



> **Exercice :**
>
> Ecrivez une fonction `pythagore`de paramètres _a_, _b_, _c_ qui renvoie _True_ si le triangle de dimension _a_, _b_ et _c_, trois flottants, est rectangle et _False_ sinon.
>
> _Attention :_
>
> On ne sait pas lequel de _a_, _b_ ou _c_ est le plus grand côté ! Il faudra donc étudier tous les cas possibles !
>
> - Documentez la fonction
> - Créez plusieurs DocTests renvoyant soit True soit False.
>
> 



> **Exercice :**
>
> * Ecrivez, en Python, une fonction `f` de paramètre _x_, un flottant. Cette fonction renverra x³ + 3 * x² +3 * x +1 
> * Ecrivez, en Python, une fonction `g` de paramètre _x_, un flottant. Cette fonction renverra (x+1)³
> * Ecrivez en Python une fonction `egalite` de paramètres _f_ et _g_, deux fonctions Python renvoyant des valeurs flottantes. Elle devra :
>     * Comparer approximativement, avec une précision de 10 chiffres après la virgule, les fonctions _f_ et _g_ en prenant aléatoirement 1000 valeurs de _x_ dans l'intervalle [-10, 10].
>     * Si une des comparaisons est fausse, alors la fonction renverra `False`
>     * Si toutes les comparaisons sont vraie, alors la fonction renverra `True`



_Remarque :_

Evidemment, il s'agit de comparaison **approximative**. Si les deux fonctions sont égales 1000 fois à $`10^{-10}`$près, il y a de fortes chances qu'elles sont égales... mais ce n'est pas une certitude !

Modifier et `f` pour qu'elle renvoie $`x²`$ et `g` pour qu'elle renvoie $`x² + 10^{⁻11}`$.

* les deux fonctions sont-elles égales ?
* qu'en dit la fonction `egalite` ?



Une explication ??



________

Par Mieszczak Christophe CC BY SA

sources images : réalisations personnelles



 
# Le protocole du bit alterné



## Objectif



Le _protocole du bit alterné_ est un protocole qui était utilisé pour sécuriser les transmissions sur les résaux _ARPANET_ et le _réseau européén informatique_ et qui, aujourd'hui, est remplacé par des protocoles plus efficaces et plus complexes. 



Il s'agit ici de voir comment on peut essayer de sécuriser une transmission de données.



## Principe



Le protocole s'inscrit dans la couche _accès au réseau_ : tout se passe donc au niveau des trames.



Rappelez-vous que , dans la _couche transport_ , le protocole TCP s'est chargé de prévenir le destinataire du message de l'envoi de celui-ci et à attendu un accusé de reception avant de commencer. Le destinataire s'attend donc à l'arrivée des trames.



**Première trame**



*  L'émetteur, lorsque le premier paquet est encapsulé dans la trame no _1_ , le protocole lui ajoute le bit **0** (on notera cette trame $$T_1/0$$ dans la figure plus bas). Ce bit supplémentaire est appelé **drapeau** (flag). L'emetteur déclenche alors un chronomètre.



* Le destinataire, prévenu, s'attend à recevoir en premier une trame se terminant par un **0**. Dès que c'est la cas, il envoie un accusé de reception (_Ack_) en lui ajoutant le drapeau **1** ( on le notera _Ack/1_ dans la figure ci_dessous).



Au delà d'un certain délais sans retour d'accusé de récepetion (_Time out_) avec le bon drapeau, l'émetteur considère que la trame (ou le _Ack_) a été perdue et reprend exactement la même procédure. Quand l'accusé de reception convenable (avec un drapeau 1) arrive, il pourra passer à la trame suivante.



![étape 1 - source perso](media/bit_alterne_1.jpg)

**Deuxième trame** :

Le bon _Ack_ a été reçu, on passe à la trames suivante. On procède de la même façon mais en alternant le bit ajouté :

* L'émetteur ajoute cette fois un **1** à la trame.

* Le récepteur attend une trame avec un drapeau **1** et renvoie un accusé de réception auquel il ajoute un **0**.

    

![Etape 2 - source perso](media/bit_alterne_2.jpg)

**Troisième trame**:



De nouveau, on alterne les drapeaux :

* L'émetteur ajoute cette fois un **0** à la trame.
* Le récepteur attend une trame avec un drapeau **0** et renvoie un accusé de réception auquel il ajoute un **1**.



On continue ainsi de trame en trame, en alternant chaque fois le drapeau, jusqu'à la dernière trame.



![Etape 3 - source perso](media/bit_alterne_3.jpg)







## Pourquoi ce protocole ?

Si tout se passe bien :

* Les trames arrivent dans le bon ordre : pas besoin de les numérotées et de le remettre dans le bon ordre.

* Une trame perdue est renvoyée et donc récupérée.

    

Si une trame perdue (ou un _ack_ perdu) arrive un peu en retard, le drapeau permet au destinataire de ne pas s'en occuper :



![pourquoi ce protocole - source perso](media/bit_alterne_4.jpg)



>  Saurez vous trouvez le schéma équivalent pour le _Ack/0_ perdu ?



## Oui mais ...



Oui mais ... si notre trame perdue arrive encore plus en retard, cela peut avoir des conséquences ennuyeuses :



![oui mais _ source perso](media/bit_alterne_5.jpg)



> * Que va-t-il se passer si lorsque la  trame $T_2 /0$ va arriver au recepteur ?
> * Que fait l'émetteur lorsqu'il reçoit le dernier _Ack /1_ ?
> * Qu'arrive-t-il à la trame $T_2 /1$ ?



>  
>
>  Saurez-vous trouver un problème équivalent lié à un _Ack_ en retard ?
>
> 



____________

Par Christophe Mieszczak Licence CC BY SA

_source images personelle_








# Adresses IP

Une fois les machines physiquement connectées entre elles, il faut encore les configurer correctement pour leur permettre d'échanger des données : On crée alors un `réseau logique` .

## Deux versions d'IP.

L'adresse `IP` est une adresse numérique permettant d'identifier les  appareils connectés à un réseau. 

Actuellement, deux versions coexistent : `IPv4`  et `IPv6`.

### IP v4

Les  adresses IP, en version 4, prennent la forme d'une série de 4 nombres séparés par des  points :

* Ces 4 nombres sont des  entiers codés sur un octet (donc allant de 0 à 255), par exemple 192.168.0.1

* Une adresse `IPv4` se code donc sur 4 octets ou 32 bits.

    

Un rapide calcul: $`256^4 = 4 294 967 296`$, nous donne le nombre d'adresses `IPv4` possibles. 

Compte-tenu du développement d'internet, ce nombre est rapidement devenu trop petit. Pour palier à ce problème deux solutions cohabitent : l'adressage privée, qui permet d'utiliser plusieurs fois la même adresse dans différents réseaux privés, et l'adressage IPv6.

### IP v6

Afin d'augmenter le nombre d'adresses possibles, une  nouvelle version, `IPv6`, est entrée en service en 2010 et cohabite avec la norme `IPv4` : Les adresses IP ne  sont plus codées sur 4 octets mais sur 16 !

 Le nombre  d'adresses disponibles théoriquement est donc colossal : $`256^{16} = 340282366920938463463374607431768211456 `$  



Les machines qui ont besoin d'une adresse IP unique au monde utilisent, aujourd'hui et pour la plupart, une adresse IPv6. 



## Adresse IP 



### IP Privée

Dans un réseau local (un LAN pour Local Area Network), les machines utilisent une adresse `IP privée`  :

* Cette adresse permet de repérer de manière unique une machine dans un réseau local.

*  Elle n'est pas unique au monde : deux machines de deux LAN distincts peuvent utiliser la même adresse privée. 

* Pour ne pas mélanger les adresses `IP privée` et `IP publique`, des plages leurs sont allouées. Dans un réseau local, les adresses à utiliser commencent :

    * soit par `10.` suivi de 3 octets. Par exemple `10.1.23.128`.

    * soit par `172.16` suivi de 2 octets. Par exemple `172.16.28.1`.
    * soit par `192.168` suivi de deux octest. Par exemple `192.168.0.2`.



**Avantage** : Il n'est pas nécessaire d'assigner une adresse unique à toutes les machines donc il n'est pas nécessaire d'avoir un nombre d'adresses uniques gigantesque. 

**Inconvénient** : On ne peut donc pas trouver une route vers ou depuis une machine d'un autre réseau uniquement grâce à cette adresse : on dit qu'une IP privée seule n'est pas `routable`. 



### IP publique

Pour qu'une machine puisse être repérer de manière unique sur internet,  il faut utiliser une adresse unqieu au monde : une `IP publique` .

* Seules les machines disposant d'une IP Publique peuvent être accessible directement sur internet. On dit alors qu'elles sont `routables`. C'est le cas des routeurs et des machines qui hébergent des serveurs.

*  Les plus anciennes machines disposant d'une IP publiques ont des adresses IPv4 mais, cette norme ne proposant plus assez d'adresses au vue du nombre exponentiellement croissant d'objets connectés, les plus récentes possèdent une adresse en IPv6?

* Chez les particuliers, seule la box possède une IP publique. Les autres machines font parties d'un réseau locale et ont des IP privées. Lorsqu'une machine de votre LAN envoie ou reçoit des données sur internet, la box fait le travail d'un concierge d'himmeuble : elle est capable de remplacer l'adresse IP privée d'une machine de son réseau par son adresse publique ou de faire l'inverse. On parle alors de `NAT` pour `Network Address Translation`.  Ce procédé permet, par l'intermédiaire de la box qui joue alors le rôle de `routeur NAT`, de rendre, en quelque sorte, l'IP privée `routable`.

    



Cette  adresse IP peut être soit **fixe** ou **dynamique** :

* Une adresse fixe, comme  son nom l'indique, ne change jamais. On est donc dans ce cas aisément  traçable. 

* Une l'adresse IP dynamique est attribuée par le FAI (**F**ournisseur d'**A**ccès **I**nternet) à chaque nouvelle  connexion de la box à internet. Cette adresse est  attribuée sur des  plages d'adresses IP réservées. Le traçage n'est donc pas immédiat mais  le FAI est capable de fournir les informations permettant  l'identification.

  

### Une petite manip  



Si vous êtes sous Windows 2000, Windows XP ou Windows Vista :
*  Allez sur **Démarrer**, puis **Exécuter** puis tapez **«** cmd /k ipconfig /all **».** 

Si vous êtes sous Windows 7, Windows 8, Windows 10  :
*   Allez dans **Démarrer**, puis **Exécuter** puis **Cmd**  Tapez la commande `ipconfig /all`   La liste de toutes les cartes Ethernet de votre PC apparaît, avec leurs adresses IP et les informations suivantes : Nom de la connexion dans les propriétés de Windows  Adresse MAC de la carte réseau ;  Adresse « IPv4 » privée de votre ordinateur.  

sous  Linux :
* Dans la console,  tapez la commande `ifconfig` (il peut être nécessaire d'installer les outils réseaux en tapant `sudo apt-get install net-tools` dans la console).

    

Pour connaitre l' adresse IP publique par laquelle vous passez pour aaccéder à internet, vous pouvez vous rendre sur le site [*mon-ip.com*](http://www.mon-ip.com/ip-dynamique.php) .





## Sous-réseaux

_Pour la suite, nous nous bornerons à l'utilisation de l'`IPv4`._

Dans un même réseau, on a très souvent besoin de définir des sous-réseaux.

Chaque sous-réseau à sa propre adresse IP et seules les machines d'un même sous-réseau peuvent communiquer entre elles. 

L'adresse d'une machine à elle seule ne suffit pas. Il manque une donnée : le `masque` de sous-réseau. C'est grâce à lui qu'ont pourra déterminer si deux machines sont  sur le même sous réseau.

### Masque de sous-réseau



En IPv4, le masque est codée de la même manière qu'une adresse IP c'est à dire sur 4 octets. La particularité du masque, c'est qu'il doit être constitué de gauche à droite d'une série continue de 1 puis d'une série continue de 0. 

_Exemples :_

|          |             En binaire              |  En base 10   | notation CIDR               |
| :------: | :---------------------------------: | :-----------: | --------------------------- |
| masque 1 | 11111111.00000000.00000000.00000000 |   255.0.0.0   | /8 (huit `1` consécutifs)   |
| masque 2 | 11111111.11111111.00000000.00000000 |  255.255.0.0  | /16 (seize `1` consécutifs) |
| masque 3 | 11111111.11111111.11111111.00000000 | 255.255.255.0 | /24                         |
| masque 4 | 11111111.11111111.11000000.00000000 | 255.255.196.0 | /26                         |

* Les masques 2 et 3 sont les plus répandus. En reprenant la petite manip faites plus haut, vous pouvez retrouver le masque de votre sous réseau.
* `CIDR` signifie  `Classless Inter-Domain Routing`. Cette notation permet de noter un masque de façon plus concise en comptant simplement le nombre de `1` au début du masque.



**Règles :**

> Deux machines sont sur le même sous-réseau si le résultats du **ET** logique entre leur adresse IP et le masque donne le même résultats. Ce résultat est l'adresse de leur sous-réseau.
>
> La partie de l'adresse IP d'une machine qui fait face aux `0` du masque permet quant à elle d'identifier de façon unqiue une machine sur le sous-réseau.



| Adresse IP en base 10 d'une machine       |   192    |   168    |    1     |    2     |
| :---------------------------------------- | :------: | :------: | :------: | :------: |
| Adresse IP en binaire de cette machine    | 11000000 | 10101000 | 00000001 | 00000010 |
| Masque de sous-réseau                     | 11111111 | 11111111 | 11111111 | 00000000 |
| Adresse du réseau                         |          |          |          |          |
| Adresse de la machine dans ce sous-réseau |          |          |          |          |

_Avez vous compris ?_

> * Une adresse `IP` est : 192.231.5.3/24. Quelle est l'adresse du réseau ?
>
> ```txt
> 
> ```
>
> * Une adresse `IP` d'un réseau est 172.16.0.31/16 . Quelle est l'adresse du réseau ?
>
> ```txt
> ```
>
> * Un sous-réseau a pour adresse 172.16.1.0/24. Donnez deux IP valides pour deux machines de ce réseau.
>
> ```txt
> ```
>
> 



### Adresses réservées

Nous venons de voir que les réseaux ont leur propre adresse IP. Celle-ci ne peut donc pas être attribuée à une autre machine. Ce ne sont pas les seules adresses réservées :

- L'adresse du réseau ne peut être celle d'une machine

- Les adresses dont les bits réservés aux machines sont remplacées par des 1 ne sont pas non plus utilisables. Ce sont des adresses dites de  `broadcast` ou de `diffusion` . Elles servent à envoyer des données à l'ensemble des machines du réseau. 

    

Ainsi, sur le réseau d'adresse IP 192.168.1.0 / 16 :2

* L'adresse de diffusion est 192.168.255.255
* les machines ont une adresse comprise entre 192.168.0.1 et 192.168.255.254 inclus.



### Masque et nombre de machines sur un sous-réseau

Vous avez normalement compris que la partie du masque composée de `1` impose que tous les ordinateurs du même sous réseau aient en commun cette partie de leur adresse. La partie restante, en face des `0` du masque sert à numérotée les machines, en dehors de la plus petite adresse possible qui est celle du réseau lui même et de la plus grande qui est celle de diffusion.

Ainsi :

* plus le masque contient de 1, plus on peut créer de sous-réseau mais moins on peut connecter de machines à ce sous-réseau.
* moins le masque contient de 1, moins on peut créer de sous-réseau mais plus on peut connecter de machines à ce sous-réseau.



_Avez-vous tout compris ?_ 



> Une adresse `IP` d'un réseau est 192.168.12.31/16  
>
> - L'adresse du réseau est :
>
> ```txt
> 
> ```
>
> * L'adresse de `broadcast` est :
>
> ```txt
> 
> ```
>
> - Combien ce réseau peut-il gérer de machines différentes au maximum ?  	
>
> ```
> 
> ```

_Avec un masque non "standard" :_

> L'adresse IP d'une réseau est 172.16.0.0 avec le masque 255.255.128.0
>
> 



____

Par Christophe Mieszczak 

Licence CC BY SA

_source image : production personnelle_








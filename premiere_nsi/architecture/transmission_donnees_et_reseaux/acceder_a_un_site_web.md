# Accéder à un site web



## Site web, DNS et adresse IP.

Lorsque nous saisissons `http://www.ac-lille.fr` dans la barre d'adresse d'un navigateur, nous indiquons au navigateur  que nous souhaitons entrer en communication avec le serveur hébergeant  le site `ac-lille.fr`en utilisant le protocole `HTTP` (**H**yper**T**ext **T**ransfert **P**rotocol) qui est le protocole de communication du web.



_Remarque :_

 Un protocole est  un ensemble de règles qui permettent à deux  machines de communiquer.Il en existe un grand nombre qui ont chacun un utilisation précise.  Le protocole `HTTPS` (**S** pour **S**ecure), par exemple, permet de sécuriser les échanges sur le web.



En fait, en premier lieu, le navigateur va interroger un `serveur DNS` (pour **D**omain **N**ame **S**erver). Celui-ci va renvoyer l'adresse `IP`du serveur hébergeant le site dont le `nom de domaine`est ac-lille.fr . L'adresse IP (pour Internet Protocol)  est le numéro d'identification attribué à ce nom de domaine.  



> Si votre OS est windows ou Linux :
>
> * Ouvrir une fenêtre de commande Windows (windows + R puis cmd).
> * Saisir (par exemple) `nslookup ac-lille.fr` 
> * vont apparaitre l'adresse IP de l'envoyeur et celle du destinataire. 



```txt
Server:		???
Address:	???

Non-authoritative answer:
Name:	ac-lille.fr
Address: 194.199.79.5

```

* Le serveur DNS utilisé à pour adresse  :

* L'adresse IP du serveur qui héberge le site web www.ac-lille.fr est : 



> Trouver les adresses IP des sites:  
>
> - enthdf.fr  
>
>     ```txt
>     91.134.114.119 et 51.178.175.2 et 91.134.168.79 il y a trois serveurs
>     ```
>
>     
>
> - amazon.co.uk  
>
>     ```txt
>     54.239.34.171 ou 54.239.33.58 ou 178.236.7.220
>     ```
>
>     
>
> - google.fr
>
>     ```txt
>     216.58.215.35
>     ```





Il devient alors possible de se connecter au site en tapant l'adresse IP directement dans la barre d'adresse du navigateur.



> Dans la barre d'adresse de votre navigateur, tapez l'adresse IP de notre site.
>
> _Remarque :_
>
>  il se peut que l'adresse qui apparait dans le navigateur soit changée en `www1.ac-lille.fr`. Ceci signifie que l'on a été redirigé vers un autre serveur pour  alléger le serveur principal qui risque d'être saturé. Cela ne change  rien au chargement de la page qui sera la même. 



Il est même possible de localiser géographiquement l'ordinateur qui héberge le site.



> * Allez sur ce site : [localiser un adresse IP](https://trouver-ip.com/index.php)
> * Localisez le serveur utilisée pour héberger www.ac-lille.fr



_Remarque :_

Tout hôte d'un réseau, qu'il soit serveur ou client, possède sa propre adresse IP. Nous détaillerons cela dans un autre cours.



## URL





Quand la communication est établie, la page qui se charge est généralement celle nommée `index`(en php ou html). Pour le site ac-lille.fr, il s'agit de `index.php`.  



> Faire l'essai en saisissant `www.ac-lille.fr/index.php` dans la barre d'adresse de votre navigateur.



La partie `/index.php`s'appelle l'`URL`(**U**niform **R**essource **L**ocator). Elle permet  d'identifier la ressource et  d'indiquer l'endroit où elle se situe sur le serveur. Elle fournit en  fait le chemin d'accès à la ressource, dans l'arborescence du serveur.

Par exemple, en cliquant sur l'onglet `Académie`, rubrique `Le Projet académique 2018-2021`, on charge la page `http://www1.ac-lille.fr/cid133326/projet-academique.html`. L'URL de cette page est donc `/cid133326/projet-academique.html`.



> * Allez, sur le site météofrance.fr, trouver les prévisions météo pour le Pas-de-Calais.
>
> * Quelle est l'URL de cette ressource ?
>
>     ```txt
>     
>     ```
>
>     







___________

Par Christophe Miesczak à partir des documents de Luc Bournonville et David Capitaine

Licence CC BY SA








# Le modèle TCP/IP

## Introduction



* En **1958**, La `DARPA (Defense Advanced Research Projects Agency)` voit le jour. Cette agence gouvernementale américaine a pour but de veiller à la constante suprématie des États unis en  matière technologique et scientifique

*  En **1962** la `DARPA` soutient le  projet du professeur Licklider qui a pour but de mettre en réseau les  ordinateurs des universités américaines afin que ces dernières puissent échanger des informations plus rapidement (même à des  milliers de kilomètres de distance). 

* En **1968**, `ARPANET`,1er réseau informatique à grande échelle de l'histoire voit le jour.  

* Le **29 octobre 1969**, le 1er message (le mot "login") est envoyé depuis l'université de Californie à Los Angeles vers  l'université de Stanford via le réseau `ARPANET` (les 2 universités sont  environ distantes de 500 km). C'est un demi-succès, puisque seules les  lettres "l" et "o" arriveront à bon port. 

* En **1972**, 

    * 23 ordinateurs sont  connectés à `ARPANET` (on trouve même des ordinateurs en dehors des États  unis).
    * En parallèle au projet `ARPANET`, d'autres réseaux voient le jour,  problème, ils utilisent des protocoles de communication hétéroclite et 2 ordinateurs appartenant à 2 réseaux  différents sont incapables de communiquer entre eux puisqu'ils  n'utilisent les mêmes protocoles. 

* En **1974** ,Vint Cerf et Bob Khan vont mettre au point le protocole `TCP`  qui sera très rapidement couplé au protocole `IP` pour donner le modèle `TCP/IP` qui est l'objet de cette leçon.

    * Le modèle `TCP/IP` va très rapidement s'imposer comme un  standard. Les différents réseaux (`ARPANET` et les autres) vont l'adopter. 
    * Cette adoption va permettre d'interconnecter tous ces réseaux (2 machines appartenant à 2 réseaux différents vont pouvoir communiquer  grâce à cette interconnexion). 
    * **Internet était né** (le terme Internet  vient de `internetting` qui signifie Connexion entre plusieurs  réseaux) même si le terme exacte _internet_ n'apparaît qu'en **1983**. 
    



Internet continue de se développer. Il s'est constitué progressivement en reliant entre eux des réseaux utilisant des normes différentes,  au fur et à mesure de l'apparition de technologies : ordinateurs, smartphones, objets connectés... Le modèle `TCP/IP` permet l'inter-connexion de ces systèmes hétérogènes,indépendamment des supports de transmission (câble, onde ...), des normes d'infrastructures réseaux, des systèmes d'exploitation et des applications utilisées.



## Le modèle TCP/IP

### Commençons par une analogie 



Nous sommes il y a très longtemps dans une contrée lointaine, à une époque où les écrits sont gravés dans la pierre. Un écrivain, Pierre, souhaite envoyer un de ses romans.

Tout d'abord, il grave sur une centaines de blocs le roman en entier. Il demande alors à un transporteur d'organiser l'envoie de son œuvre.

De peur de devoir tout recommencer si le roman s'égare sur le trajet vers le palais du roi ou de crééer d'énormes bouchons sur les routes avec un énorme convoi, le transporteur prépare autant de caisses qu'il y a de pages/pierres au roman et place une pierre dans chaque caisse.

De peur qu'on remplace une des pages du roman ou qu'elle ne soit égarée, il place un des ses serviteurs dans chaque caisse. 

Pour permettre de reconstituer le roman dans le bon ordre, il numérote les caisses.

Il écrit le nom de Pierre et celui du roi sur chacune des caisses afin qu'elle arrive à destination. 


_Problème_ : Pierre n'est pas le seul Pierre du royaume. Heureusement le transporteur connaît notre Pierre. Il complète ajoute un emballage à la boite sur lequel il écrit sa propre adresse que tout le royaume connaît, mais aussi celle du roi (ce qui est plus simple puisqu'il n'y a qu'un roi dans le royaume). Il communiquera les informations à Pierre lui même car il le connaît bien. Problème résolu. 

Le transporteur prend la précaution d'envoyer un message aux serviteurs du roi pour être certain que ces derniers sont prêts à recevoir cette livraison et attend de recevoir une réponse positive.

Il missionne un grand nombre de petits colporteurs, capable d'emporter une seule caisse à la fois, sans avoir besoin de savoir ce qu'il y a à l'intérieur. Chaque colporteur est libre de choisir la route qu'il souhaite, pourvu que le paquet arrive à destination. Certains iront par les routes, d'autres par la mer....

Dès qu'un colis arrive à destination: 

* les serviteurs du roi ouvrent la caisse.
* ils vérifient, grâce au pauvre homme enfermé avec elle, qu'il s'agit bien d'une _page_ conforme du roman.
* Ils classent la _page_ reçue selon le numéro trouvé dans la caisse.
* Ils envoient à notre écrivain, grâce à l'adresse écrite sur la caisse, un message pour le prévenir de sa réception.
* Si, au bout d'un temps jugé trop long, le transporteur n'a pas de nouvelle d'une page de son roman, il considère que le transporteur s'est perdu en route, ou que le bloc a été jugé non conforme et il en grave alors une copie sur un nouveau bloc de pierre puis la ré-expédie de la même façon.
* Lorsque le transporteur a reçu des nouvelles de chacun de ses blocs, la transmission est terminée.



> Lister les avantages qu'il y a à envoyer le roman page par page plutôt que d'un seul tenant.
>
> ```txt
> ```
>
> 




##  Le modèle TCP/IP

Le modèle TCP/IP possède 4 _couches_ :

![modèle TCP/IP - source perso](media/modele_tcpip.jpg)



**La couche application**

Une application (votre navigateur, votre boite mail ...) souhaite envoyer des données vers une autre machine ou plus exactement vers un application précise d'une autre machine. Elle structure alors ces données en utilisant un protocole qui lui est propre (HTTP pour un navigateur, SMTP pour un service mail ...). 

  

![couche application - source perso](media/application.jpg)

   Elle passe ensuite ces données à la couche inférieure.



>   
>
> Dans notre petites histoires, quelles passages correspondent à cette couche ?
>
>   

**La couche Transport**



Le protocole TCP :

* Envoie un message au destinataire pour le prévenir de l'envoi des données.

* Attends la réponse du destinataire.

* Découpe les données en petits **paquets** numérotés dans un ordre précis. Ces paquets ont une taille limitée à 1500 octets. En cas de perte de données, il suffit de renvoyer le paquet correspondant et non l'ensemble des données.

* Inclus les données du protocole utilisé par l'application. On dit qu'il **encapsule** ces données :

    

![couche transport - données perso](media/transport.jpg)

* Une fois les données transmises, `TCP` clôt la communication.

_Remarques :_

Le protocole `UDP` peut aussi être utilisé pour le transport. Il ressemble fort au protocole `TCP` mais ne prend aucune précaution :

* Il ne vérifie pas que le destinataire est prêt.
* Il ne vérifie pas si les données envoyées sont bien conformes.
* Il ne vérifie même pas si le destinataire a bien reçu les données.



Ce faisant, ce protocole est plus rapide et est utilisé pour transmettre rapidement de petites quantités de données, depuis un serveur vers de nombreux clients ou  bien dans des cas où la perte éventuelle de données ne serait pas  capitale: visionner des vidéos, écouter de la musique, streamings, jeux  en réseau, visioconférence ou audioconférence . 

L'avantage du découpage en petit paquet est qu'en cas de perte de données :

*  dans le cas de `TCP`,  il suffit de renvoyer un seul petit paquet et non pas l'ensemble des données.

* dans le cas d'`UDP`, la perte de données n'engendrera qu'une gêne passagère, une pixellisation sur un écran par exemple.

  

  
  
  ​    
  
    >    
  >
    > Dans notre petites histoires, quelles passages correspondent à cette couche ?
    >
    >   
  
  ​    

**La couche internet**



Le protocole IP prend alors la suite.

Pour pouvoir trouver une route entre deux machines, il faut utiliser une adresse `IP public` unique au monde. Seules certaines machines peuvent avoir une adresse `IP public`. Chez vous, la box internet en possède. Elle est donc `routable` sur internet. 

La box est également capable de remplacer l'adresse IP privée d'une machine de son réseau par son adresse publique ou de faire l'inverse. On parle alors de `NAT` pour `Network Address Translation`.  Ce procédés permet, par l'intermédiaire de la box qui joue alors le rôle de `routeur NAT`, de rendre en quelque sorte l'IP privée `routable`.

Chaque paquet de la couche supérieure est alors encapsulée dans un paquet _plus grand_ sur lequel est ajouté les IP publiques de l'émetteur et du destinataire.

![couche internet](media/internet.jpg)

Juste avant de passer à la couche inférieure, le protocole `ARP` permet de faire la correspondance entre les adresses `IP` et  `MAC` (pour `Media Access Control`) des machines, ce qui facilitera le travail de la couche inférieure. La sous-couche `MAC` servira d'interface entre la partie logicielle et la partie matérielle.



![couche internet](media/reseau.jpg)

 On peut alors continuer.

>    
>
> Dans notre petites histoires, quelles passages correspondent à cette couche ?
>
>   



**La couche accès au réseau** :

A ce niveau, nous disposons d'un paquet appelé `trame` qui être convertie en signal physique qui va transiter sur des interfaces câblée `Ethernet` (signal électrique), `Wi-fi` (signal électromagnétique) ou fibre optique (ondes lumineuses) , par exemple car il y en a beaucoup d'autres.

>   
>
> Voyez-vous d'autres interfaces ?
>
>   



_Remarques : débits et interfaces_

Selon l'interface utilisée, la bande passante (`BP`) est plus ou moins importante.

| Technologie     |   `BP` descendante    |     `BP` montante     |
| --------------- | :-------------------: | :-------------------: |
| `Modem`         |       56 kbit/s       |       48 kbit/s       |
| `Bluetooth`     |       3 Mbit/s        |       3 Mbit/s        |
| `Ethernet`      |       10 Mbit/s       |       10 Mbit/s       |
| `Wi-Fi`         | 11 Mbit/s à 10 Gbit/s | 11 Mbit/s à 10 Gbit/s |
| `ADSL`          |       13 Mbit/s       |       1 Mbit/s        |
| `4G`            |      100 Mbit/s       |       50 Mbit/s       |
| `Satellite`     |       50 Mbit/s       |       1 Mbit/s        |
| `FastEthernet`  |      100 Mbit/s       |      100 Mbit/s       |
| `Fibre Optique` |       10 Gbit/s       |       10 Gbit/s       |


### Le routage



Chaque routeur établit **une table de routage** grâce à laquelle il sait vers quel routeur diriger la trame pour qu'elle arrive à destination. Il peut y avoir un grand nombre de chemins différents. Cette partie est étudiée en détail en Terminale. 



> Sous Linux :
>
> * Ouvrez un ligne de commande
> * Si ce n'est déjà fait, installer `traceroute` via la commande `sudo apt-get install traceroute`
> * Tapez `nslookup www.google.fr` afin d'obtenir l'adresse IP d'un serveur Google.
> * Tapez la commande `traceroute`  suivie d'un espace et de l'IP précédente : vous obtenez la liste des IP des routeurs traversés avant d'atteindre le site en question.
>
> Sous Windows :
>
> * Ouvrez la ligne de commande en appuyant sur la touche `windows+R` puis en tapant `cmd`
> * Tapez `nslookup www.google.fr` afin d'obtenir l'adresse IP d'un serveur Google.
> * Tapez la commande `tracert` suivie d'un espace et de l'adresse IP précédente: vous obtenez la liste des IP des routeurs traversés avant d'atteindre le site en question. 
>
>  
>
> Via [ce site web](https://www.hostip.fr/) vous pouvez maintenant localiser ces routeurs.
>
> Si vous répétiez l'opération, vous pourriez obtenir un tout autre résultat.
>
> 



### Côté destinataire



Les trames, quand elles arrivent, arrivent donc dans le désordre. On dit que **`TCP` n'a pas de garantie temporelle**.

Elles _remontent_ ensuite le modèle TCP/IP qui les **décapsulent** au fur et à mesure.



**Couche IP**



La couche IP décapsule la couche réseau puis se charger de transmettre le paquet au bon hôte.



**Couche TCP**



Le protocole TCP décapsule la couche IP et :

* Vérifie que le paquet reçu est conforme via divers algorithmes.
* Si le paquet est bien conforme, renvoie un accusé de réception, appelé `acknowledgement`, à l'émetteur. Côté émetteur, si cet accusé n'arrive pas temps un délais déterminé, il renverra tout simplement le paquet une deuxième, un troisième fois ...
* Une fois tous les paquets reçus, il reconstitue les données dans leur entièreté.

_Remarque :_

L'avantage de transmettre des petits paquets est que, si l'un d'entre s'égare ou est erroné à l'arrivée, il suffit de renvoyer à nouveau un **petit** paquets et pas l'ensemble de toutes les données.



**Couche Application**

L'application de destination reçoit les données qui lui sont destinées, entièrement constituées, et va pouvoir les utiliser.



**En résumé**

En résumé, voici ce qui se passe lors de l'envoi de données :



![modèle - source perso](media/aller_retour.jpg)



_Remarque :_

Il existe un autre modèle de couche, le modèle `OSI (Open Systems  Interconnection)`. Il est antérieur au modèle TCP/IP et date des années 1970. Principalement théorique, il a permis de poser les bases des communications réseau. Ce modèle est composé de 7 couches :

![OSI - wikipédia - domaine public](https://upload.wikimedia.org/wikipedia/commons/7/7e/Comparaison_des_mod%C3%A8les_OSI_et_TCP_IP.png)

Ce modèle est donné ici à titre d'information , le principal est de retenir ce qui a été vu sur le modèle TCP/IP. 	

____________

Par Christophe Mieszczak Licence CC BY SA

sources images : productions personnelles sauf :

* [modèle OSI et TCP/IP, wikipédia CC BY SA](https://commons.wikimedia.org/wiki/File:Comparaison_des_mod%C3%A8les_OSI_et_TCP_IP.png)

    
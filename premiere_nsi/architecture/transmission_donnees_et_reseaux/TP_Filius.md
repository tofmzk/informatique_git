# TP Filius



## Késako et Installation 

_Filius_ est un logiciel libre et gratuit conçu pour réaliser des simulations de réseaux informatiques sous Windows ou Linux.

Vous pourrez l'installer à partir de ce lien : [Filius]()

Il s'agit d'un logiciel allemand mais, lors de l'installation, vous pourrez choisir la langue française ou anglaise.



## Partie 1 : un petit LAN



>  
>
> Avant de commencer cette partie, allez consulter les [généralités](generalites.md).
>
> 





C'est fait ? Bien. Lancez Filius.



![filius 1 - perso](media/filius_1.jpg)



* Mettez le logiciel en mode _construction_ en cliquant sur le marteau.
* Placez trois ordinateurs. Notez que _portable_ et _ordinateur_ n'ont de différent que l'apparence.
* Placez un switch en glissant les constituants de la colonnes de gauche.
* sélectionnez le câble et reliez le switch aux ordinateurs.



Nous voilà avec un petit LAN.

![filius 2 - perso](media/filius_2.jpg)

Sélectionnez un des ordinateurs, portables ou non, puis cliquez sur la flèche dans la barre du bas pour ouvrir ses propriétés.

![filius 3 - perso](media/filius_3.jpg)





* Changez son nom en _hôte 1_ ( Vous renommerez de la même façon les deux autres ordinateurs _hôte 2_ et _hôte 3_).
* Sous le nom de l'hôte apparaissent les propriétés de l'ordinateur dans le réseau. 
    * L'adresse MAC
    * L'adresse IP
    * Le masque
    * La passerelle
    * Le serveur DNS



> 
>
> Sauvegardez votre travail sous le nom _réseau_.
>
>  



Vous venez de construire un **réseau physique**  : des machines reliées entre elles par des _câbles_ (ou des ondes si on utilise le wifi par exemple). Il n'est pas encore fonctionnel. Il reste à configurer les machines pour qu'elles puissent communiquer entre elles : nous allons réaliser **un réseau logique**.



## Adresses IP

Une fois le réseau physiquement correctement connecté, on peut créer des sous réseaux logiques grâce à une adresse et un masque qui détermineront quelles machines du même réseau physique peuvent comuniquer entre elles.



> 
>
>  Avant de continuer, allons jeter un oeil au cours [adresses IP](adresses_IP.md) pour apprendre ce qu'est une adresse IP et un masque . 
>
> 



Nous allons maintenant configurer notre réseau :

* Le nom du réseau sera 192.168.0.0 
* Les IP des ordinateurs seront :
    * 192.168.0.1 pour l'hôte 1
    * 192.168.0.2 pour l'hôte 2
    * 192.168.0.3 pour l'hôte 3



> * Quel doit être le masque pour que les 3 premiers octets soient réservés au nom du réseau et le dernier à celui des ordinateurs ?
>
>     ```txt
>             
>     ```
>
>     
>
> * Quel est le nombre maximum d'ordinateurs dans ce réseau ?
>
>     ```txt
>             
>     ```
>
>     



Testons notre réseau :

* Passez en mode simulation en cliquant sur la flèche verte.

* Cliquez sur l'hôte 1 puis sur l'icône _installation de logiciel_.

* Sélectionnez la _ligne de commande_ (c'est à dire un SHELL) puis cliquez sur la flèche gauche et, pour finir, appliquez la modification : un nouvel icône d'application apparaît à côté .

    ![filius4 - perso](media/filius_4.jpg)

* Lancer _ligne de commande_ en cliquant sur l'icône.

* Pour vérifier la possibilité d'atteindre un autre ordinateur, on utilise la commande `ping adresseIP`. Cette dernière essaie de joindre l'adresse IP précisée en lui envoyant un petit _paquet_ puis attend une réponse éventuelle et enfin affiche le temps que l'envoie du message et le retour de l'accusé de réception ont pris.

    * Essayez de joindre les deux autres hôtes.

        

        ![filius 5 - perso](media/filius_5.jpg)
    
        
    
    * Installez _ligne de commande_ sur les autres hôtes et testez les de même.



Notre petit réseau est donc configuré, les hôtes peuvent communiquer entre eux.



> 
>
>  Sauvegardez votre travail sous le nom _réseau_.
>
>  

### Serveur DHCP

Dans la _vraie vie_, ce ne sont pas les humains qui attribuent à la main les adresses IP des machines des réseaux. Un logiciel rend ce service automatiquement : le serveur DHCP.

Sous Filius, ajoutez un ordinateur au réseau, donnez lui une adresse logique compatible avec votre réseau logique puis cliquez sur le bouton `configuration du service DHCP`

![dhcp](/home/t450/Documents/Boulot/informatique_git/snt/internet/media/serveur_dhcp.jpg)

Indiquez le dévut et la fin de la plage d'adressage du réseau et cliquez sur `OK`.

Sur chacune des autres machines du réseau, cliquez sur la _checkbox_ `Adressage automatique par serveur DHCP`.

Passez maintenant en mode exécution et regardez les communications (le fils deviennent vert) : les ordinateurs du réseau interrogent le serveur DHCP qui leur envoie une adresse IP valide dans la plage que vous lui avez autorisée : il S'agit d'un **adressage dynamique** et l'adresse d'une machine peut changer lorsqu'elle quitte le réseau (on l'éteint par exemple) puis y revient.



_Remarque :_

Il arrive que certaines machines ne doivent jamais changer d'adresse. C'est par exemple le cas d'une imprimante réseau : si son adresse change, les ordinateurs ne la trouveront plus !

Pour éviter qu'une adresse dynamique ne change, il faut définir une adresse préférée qui ne changera alors jamais. 

Sous Filius, retourner dans la configuration du serveur DHCP et cliquez sur l'onglet `adressage statique` :

![](/home/t450/Documents/Boulot/informatique_git/snt/internet/media/adresse_preferee.jpg)

Vous pouvez mainteant fixer l'adresse IP d'une machine en précisant :

* son adresse MAC
* l'adresse IP préférée que vous avez choisi

Cette adresse IP sera alors toujours la même pour cette machine précise.





## Clients et serveurs

Nous allons maintenant installer un serveur sur l'un des hôtes, par exemple l'hôte 3.

* Toujours en mode simulation, installer un _serveur web_.

    

    ![filius6 - perso](media/filius_6.jpg)

* Exécutez le serveur web et démarrer le.
* Fermez la boite de dialogue des application de l'hôte 1.
* L'hôte 1 utilise un logiciel serveur. Par extension, on l'appellera serveur lui même. Renommez le donc en _serveur_.



Maintenant qu'on a un serveur, les deux autres hôtes seront ses clients.

* Renommez les en _client 1_ et _client 2_.
* Sur les deux clients, installez un _navigateur web_ 



> 
>
>  Sauvegardez votre travail sous le nom _réseau_.
>
> 





### Se connecter à un site web



> 
>
> Avant de commencer cette partie, aller lire le cours [accéder à un site web](acceder_a_un_site_web.md).
>
> 



Connectons nous au petit site web hébergé par notre serveur. Toujours en mode simulation :

* Cliquez sur le serveur :
    * ouvrez le serveur web
    
    * démarrez le
    
* Cliquez sur les clients :
    * Lancer le navigateur web
    * Tapez l'adresse IP du serveur dans la barre d'adresse ...



_Remarque :_

Filius permet d'ajouter d'autre pages HTML ou de modifier la sienne. mais cela reste limité et ce n'est pas l'objet de ce TP.



Nous allons maintenant ajouter un serveur DNS qui permettra de donner un nom de domaine à notre site web.

* Repassez en mode _construction_.

* Ajouter un ordinateur :

    * Nommez-le _serveur DNS_.
    * Donnez lui l'adresse IP 192.168.1.254 afin de laisser la place pour d'éventuels autres ordinateurs sur ce réseau.
    * Configurez les deux clients en ajoutant l'adresse IP du serveur DNS que nous venons de créer.

* Repassez en mode simulation :

    * installer un serveur DNS sur notre serveur DNS

    * exécutez le serveur et ajoutez le nom de domaine _www.filius.com_ associé à l'adresse IP du serveur web

    * démarrez le serveur DNS

        ![filius7 - perso](media/filius_7.jpg)





* Cliquez sur un client (testez les 2 clients !):
    * Lancer le navigateur web
    * Tapez le nom de domaine dans la barre d'adresse.
    * Regardez bien quels câbles se colorent lors de l'exécution de la requête.



_Remarque :_

* Nous venons d'envoyer une requête `GET` au le serveur qui, en retour, vous a envoyé la page `index.html`

* Allez regarder ce qu'affiche l'application serveur de notre ordinateur serveur :

![filius8 perso](media/filius_8.jpg)

 



> 
>
>  Sauvegardez votre travail sous le nom _réseau_.
>
> 





## Un deuxième réseau



Repassez en mode construction et créez un deuxième petit réseau de deux ordinateurs :

* L'adresse IP de ce réseau sera 192.168.1.0
* le masque 255.255.255.0
* configurez les 2 hôtes.



Pour relier les deux réseaux, on utilise un _routeur_. C'est un composant réseau qui assure le passage de l’information entre deux sous-réseaux logiques distincts en choisissant le meilleur chemin (si plusieurs chemins, il y a…). Comment fait-il ? Nous le verrons dans le cours sur les protocoles de communication

Le routeur n’est pas transparent, il faudra donc adresser (IP) le routeur pour le traverser. Il doit donc disposer en entrée, d’un adressage logique IP compatible avec le LAN1 et le LAN2, d’où les **2 entrées physiques**, c'est à dire deux cartes réseaux et le double adressage logique IP choisit ci-dessus.
       \- l’une avec un adressage IP compatible avec le LAN1
       \- l’autre avec un adressage IP compatible avec le LAN2



Toujours en mode construction :

* Ajouter un routeur entre les deux réseaux 

    * Choisissez deux entrées, puisqu'il y a deux réseaux. Notez qu'un routeur peut faire le lien entre de nombreux réseaux distincts
    * Configurez la carte réseau relié au réseau d'adresse 192.168.0.0 avec une IP compatible avec ce réseau
    * Configurez la carte réseau relié au réseau d'adresse 192.168.1.0 avec une IP compatible avec ce réseau
    * Vous venez de créer une **passerelle** entre nos réseaux !

    ![flius9 - perso](media/filius_9.png)



Oui, il y  un pont mais ça ne suffit pas : il faut que les hôtes des deux réseaux sachent à quelle adresse est cette **passerelle**. 

* Configurez les hôtes des deux réseaux en y indiquant l'adresse IP de la passerelle compatible avec eux.

* Testons tout cela en exécutant un ping d'un hôte d'un réseau vers un autre et inversement (on installera les logiciels nécessaires).

* Essayer d'atteindre le serveur à partir d'un ordinateur du second réseau via son IP.

* Configurez les hôtes du 2ème réseau afin qu'il puisse accéder au site web installé sur un serveur de l'autre réseau via son nom de domaine.

  ​    

  ​    

## Modèle TCP/IP



Avant de poursuivre, aller regarder le cours sur le [modèle TCP/IP](modele_tcpip.md)

Analysez les données échangées entre le client et le serveur à l'aide de l'outil `data exchange` (clique droit sur un hôte).	



![filius10- perso](media/filius_10.jpg)

​	

Comme vous pouvez le constater on retrouve : 



- Le protocole TCP au niveau de la couche "Transport" (`ACK` signifie `acknowledgement`, ce sont les accusés de réception aussi appelés  acquittement) 		
- cliquez sur la ligne du protocole TCP pour accéder au protocole IP.
- Le protocole `HTTP` au niveau de la couche "Application" (en cliquant  sur les requêtes et réponses HTTP on retrouve ce que l'on a vu  précédemment dans le cours "Protocole HTTP") 		
- Le protocole `ARP` qui permet de trouver une adresse MAC à partir  d'une adresse IP (l'échange entre les 2 machines commence d'ailleurs  avec ce protocole `ARP`, sinon, impossible d'envoyer les trames) . 

_____

Par Christophe Mieszczak CC BY SA

_source images : impressions d'écran, production personnelle_ 










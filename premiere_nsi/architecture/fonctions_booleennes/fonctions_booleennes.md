# Booléens et fonctions logiques

## Introduction

Nous avons vu qu'avec des 0 et des 1 nous pouvions coder des entiers relatifs, des caractères et  des nombres décimaux (en valeur approchée). Mais comment faire, à partir de 0 et de 1, pour réaliser des opérations ? Ne serait-ce qu'une addition !

En électronique, les **fonctions booléennes** ou **portes logiques** jouent un rôle majeur : elles sont à la base de tous les systèmes, même complexes. 

Les fonctions utilisent des variables dites **booléennes** qui ne pouvent prendre que deux valeurs : soit 1 soit 0 (soit _True_ soit _False_). Une algèbre particulière a été développée par le mathématicien anglais George Boole qui lui a donné son nom : l'algèbre Booléénne.

### Latex et Markdown

Nous aurons dans ce chapitre besoin d'écrire des formule des formules mathématiques. En _Markdown_, on utilise le _Latex_ :

On place l'expression entre deux balises, de cette façon :   

``` txt
$`expression Latex`$
```



* Pour écrire $`a + b`$ on écrit :

    ```txt
    $`a+b`$
    ```

    

* Pour écrire  $`a \oplus b `$ , on écrit :

    ```txt
    $`a /oplus b`$
    ```

    

* Pour mettre écrire $`\overline a`$ on écrit :

    ```txt
    $`\overline a`$
    ```

    

* Un peu plus compliqué,pour écrire $`\overline {a + \overline b}`$ on code :

    ```txt
    $`\overline {a + \overline b}`$
    ```

    

* Voici quelques syntaxes Latex qui pourraient vous servir :

    ```txt
    a \times b 
    \frac {num} {dénom}
    \sqrt {radicande}
    a ^ {exposant}
    a_{indice}
    ```

>    
>
> Testez quelques formules maintenant !
>
>   





## Les fonctions logiques



### Logisim

Lancez le logiciel libre _Logisim_ afin de simuler les fonctions logiques.

On utilisant les icônes de la barre de tâche, réalisez les circuits ci-dessous.

![et/ou/non, capture d'écran](media/etounon.jpg)



_Fonctionnement de Logisim :_

* Cliquez sur la flèche pour vous mettre en mode _construction_.
* Une fois dans ce mode ,choisissez l'objet à insérer :
    * un carré pour une entrée _a_ ou _b_
    * un des trois opérateurs proposés
    * un rond pour une sortie _s_
    * le _A_ pour insérer du texte, c'est à dire ici _a_, _b_ et _s_ aux entrées et sorties.
* Une fois un circuit terminé, on clique sur le doigt pour passer en mode _exécution_ :
    * Cliquez sur une entrée pour voir l'effet sur la sortie.
    * Compléter les tableaux ci-dessous, appelé table de vérité. 


### Opérateur 1

   

|  a   |  s   |
| :--: | :--: |
|  0   |      |
|  1   |      |



> Quelle est cette fonction ?
>
> En algèbre de Boole, on note cette fonction $`\overline a`$







> Complétez :
>
> * $`\overline{ \bar a}=`$
> * $`\overline { \overline {\bar a}} =`$




### Opérateur _$`\ge`$_

|  b   |  a   |  s   |
| :--: | :--: | :--: |
|  0   |  0   |      |
|  0   |  1   |      |
|  1   |  0   |      |
|  1   |  1   |      |



> Quelle est cette fonction ?
>
> En algèbre de Boole, on note cette fonction $`a + b`$



> Complétez :
>
> * $`a + 0 =`$
> * $`a+1=`$
> * $`a+a=`$
> * $`a + \bar a=`$



### Opérateur &

|  a   |  b   |  s   |
| :--: | :--: | :--: |
|  0   |  0   |      |
|  1   |  0   |      |
|  0   |  1   |      |
|  1   |  1   |      |

  

> Une question difficile : quelle est cette fonction ?				
>
> En algèbre de Boole, on note également cette fonction $`a . b`$





> Complétez :
>
> * $`a.1 =`$
>
> * $`a.a =`$
>
> * $`a.0 =`$
>
> * $`a.\bar a=`$
>
>     






### NAND : NON ET


> Réalisez un circuit _NAND_ et testez le.
> Dresser la table de vérité cette fonction _NAND_ ci-dessous.

|  a   |  b   |  s   |
| :--: | :--: | :--: |
|  0   |  0   |      |
|  1   |  0   |      |
|  0   |  1   |      |
|  1   |  1   |      |

_Remarque :_ 

En algèbre de Boole, le non et entre a et b s'écrit $`\overline {a.b}`$



### NOR : NON OU

> Réalisez un circuit _NOR_ et testez le.
> Dresser la table de vérité cette fonction _NOR_ ci-dessous.

|  a   |  b   |  s   |
| :--: | :--: | :--: |
|  0   |  0   |      |
|  1   |  0   |      |
|  0   |  1   |      |
|  1   |  1   |      |

_Remarque :_ 

En algèbre de Boole, le non ou entre a et b s'écrit $`\overline {a+b}`$



### XOR : OU exclusif



Le _OU exclusif_ doit vérifier la table de vérité ci-dessous :



|  a   |  b   |   s   |
| :--: | :--: | :---: |
|  0   |  0   |   0   |
|  1   |  0   | **1** |
|  0   |  1   | **1** |
|  1   |  1   |   0   |

  

* En algèbre de Boole, on note cette fonction $`a \oplus b`$.
* Regardez bien dans la table, la position des **1** : a **ou exlusif b** est égal à ( a **et ** $`\bar b`$) ou ($`\bar a`$ **et** b).



> * Avec _Logisim_, construisez la fonction **ou exclusif**
> * N'oubliez pas de sauvegarder votre travail dans le répertoire _Logisim_ .



_Remarque :_

On peut écrire cette relation : $` a \oplus b = a . \bar b + \bar a . b`$.



### Petit retour au Python

Nous avons déjà parlé de variables booléennes en Python. Elles prennent également uniquement deux valeurs : `True` ou `False`. 

Nous avons vu que ces variables possèdent des opérateurs `and`, `or` , `not` . Ces derniers sont les mêmes que ceux que nous venons de voir.

```python
>>> a = True
>>> b = False
>>> a and b
???
>>> a or b
???
>>> not a
???
```





## De Morgan



![](media/de_morgan.jpg)



> Réalisez, testez ces fonctions et dressez leur table de vérité.
>
> A quelles fonctions correspondent-t-elle ?



Théorème de De Morgan :



>  $`a + b = \overline{ \bar a . \bar b}`$ 
>
>  $`a . b = \overline{\bar a + \bar b}`$ 



###### On peut donc, avec deux opérateurs seulement, le **non** et un des deux autres, créer le troisième ... et tous les autres.





## Ajoutons deux bits

Pour commencer, on ne rit pas. Maintenant que vous êtes grands, vous en êtes capables. Si si ! 



On souhaite réaliser une fonction capable de donner le résultat obtenu en ajoutant un bit _a_ avec un autre bit _b_. On va obtenir un bit somme _s_ plus une éventuelle retenue _r_ . 

Complétez la table de vérité ci-dessous :

|  a   |  b   |  r   |  s   |
| :--: | :--: | :--: | :--: |
|  0   |  0   |      |      |
|  1   |  0   |      |      |
|  0   |  1   |      |      |
|  1   |  1   |      |      |



  

> En utilisant l'algèbre de Boole et ses opérateurs vus plus haut, écrivez _s_ et _r_ en fonction de _a_ et _b_ .



$`r = `$ 

$`s =`$ 



> * Réalisez une fonction sous _Logisim_ avec deux bits _a_ et _b_ en entrée et deux bits  _r_ et _s_ en sortie.
>
> * Faites une copie d'écran et insérez l'image obtenue ci-dessous après l'avoir sauvegardée au format `jpg`dans le dossier `media`. 
>
>     Pour cela on écrit : `![additionneur incomplet](media/nom_du_fichier_image.jpg)`





Vous avez réalisé un additionneur incomplet, un _demi-additionneur_, car, ainsi conçu, on ne peut enchaîner ces additionneurs pour ajouter plus de deux bits. On va l'améliorer en créant un _additionneur_.



## L'additionneur

L'idée est d'ajouter le bit _a_, le bit _b_ et une retenue _r0_ pour obtenir une somme _s_ et une retenue _r0_. 

<img src="media\additionneur.jpg" alt="addition avec retenue" style="zoom:50%;" />



Compléter la table de vérité :

|  a   |  b   |  r0  |  r   |  s   |
| :--: | :--: | :--: | :--: | :--: |
|  0   |  0   |  0   |      |      |
|  1   |  0   |  0   |      |      |
|  0   |  1   |  0   |      |      |
|  1   |  1   |  0   |      |      |
|  0   |  0   |  1   |      |      |
|  1   |  0   |  1   |      |      |
|  0   |  1   |  1   |      |      |
|  1   |  1   |  1   |      |      |



#### Commençons par regarder _s_



$`s = a . \bar b . \overline {r0} + \bar a . b . \overline {r0} + \bar a . \bar b . r0 + a . b . r0`$



Hou làlà, c'est compliqué et ça risque de faire un sacré circuit !! 



Heureusement les opérateurs logiques `+` et `.` ont des propriétés très voisines des additions et multiplications usuelles (Pour en savoir plus, suivez donc [ce lien](https://fr.wikipedia.org/wiki/Alg%C3%A8bre_de_Boole_%28logique%29)) :

* Commutativité :
    * a . b = b . a
    * a + b = b + a
* Associativité :
    * (a + b ) + c = a + ( b + c) = a + b + c
* Distributivité :
    * a . (b + c) = a . b + a . c



On va pouvoir simplifier notre expressions booléenne :

$`s = a . \bar b . \overline {r0} + \bar a . b . \overline {r0} + \bar a . \bar b . r0 + a . b . r0`$

On commence par factoriser :

$`s = a . ( \bar b . \overline {r0} + b . r0) + \bar a . (b . \overline {r0} + \bar b . r0)`$

On remarque qu'on retrouve le **ou exclusif** :

* $`b . \overline {r0} + \bar b . r0 = b \oplus r0`$
* $` \bar b . \overline {r0} + b . r0 = \overline {b \oplus r0}`$

Ainsi :

$`s = a . (\overline {b \oplus r0}) + \bar a . (b \oplus r0)`$

Il s'agit de nouveau d'un **ou exclusif** entre $`a`$ et $`(b \oplus r0)`$ :

$` s = a \oplus ( b \oplus r0)`$ : Voilà qui va simplifier notre circuit, d'autant que la fonction $`\oplus`$ est présente dans _logisim_



#### Regardons maintenant la retenue r



> Démontrez que $`r = a . b + r_0 . (a \oplus b)`$



#### Logisim



> * Réalisez une fonction additionneur sous _Logisim_ 
>
> * Faites une copie d'écran et insérez l'image obtenue ci-dessous après l'avoir sauvegardée au format `jpg`dans le dossier `media`. 
>
>     Pour cela on écrit : `![additionneur](media/nom_du_fichier_image.jpg)`



_Remarque :_

Il est plus facile d'orienter les entrées côtés sud et les sorties côtés nord pour réaliser des circuits compliqués en sélectionnant le bon _Facing_. Notez au passage que l'opérateur $`\oplus`$ est appelé **XOR** dans _Logisim_ :

![début de l'opérateur](media/debut_operateur.jpg)





## Plus de bits



Dans le répertoire `logisim`, ouvrez le fichier `trois bits plus trois bits.circ`

Ici, on a placé notre opérateur dans une boite qui contient : 

* 3 entrées :
    * le premier bit
    * le deuxième bit
    * la retenue r0
*  et 2 sorties :
    * la somme
    * la nouvelle retenue



Le branchement en cascade permet d'additionner autant de bits qu'on veut.



> * Réalisez un circuit permettant d'additionner 5 bits avec 5 autres bits.
> * Sauvegardez votre travail dans le dossier _Logisim_





## Conclusion



Toutes les opérations réalisées par un ordinateur, même les plus complexes, sont le résultat du traitement de variables booléennes par des fonctions booléennes. 



Au cours de la seconde guerre mondiale, Alan Turing travaillait à Bletchley Park, centre dédié au décryptage des codes secrets utilisés par l’armée allemande. Il obtint un premier succès en 1940 en décryptant le code de la machine Enigma. Le décodage était assisté par des machines spécialisées, appelées « Bombes », réalisées d’après ses plans. Mais les Allemands mirent en service un système de chiffrement plus complexe, le code Lorenz. Celui-ci fut également décrypté en 1942 ; toutefois le processus manuel de transcription était trop long pour un usage opérationnel. Aussi une nouvelle machine, le **Colossus**, fut conçue et réalisée par une équipe dirigée par Thomas Flowers pour un décryptage rapide du code Lorenz. Le premier **Colossus**, construit en onze mois, fut mis en service en janvier 1944 et dix exemplaires fonctionnaient à la fin de la guerre. Le **Colossus**, dans sa dernière version comportait **2400 tubes à vide**. C'est grâce à ces tubes à vides qu'on construisait, à l'époque, les fonctions booléennes.



![tube à vide - wikipédia - domaine public](https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/VacuumTube1.jpg/100px-VacuumTube1.jpg)



Aujourd'hui, on utilise des transistors qu'on grave sur des puces de silicium afin de fabriquer des microprocesseurs.





![quelques transistors - source wikipédia - licence GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Transistors.agr.jpg/715px-Transistors.agr.jpg)





En 1971, le processeur Intel 4004 comptait 2300 transistors. En 2016, le Intel Xeon E5 2699 V4 HLC, en comptait plus de 7 milliards et le nombre augmente chaque année grâce à une gravure de plus en plus _fine_... dont on atteint aujourd'hui les limites ... 

[Processeurs](https://fr.wikipedia.org/wiki/Processeur) grand public :

1972 : [4004](https://fr.wikipedia.org/wiki/Intel_4004) : 2 300
1978 : [8086](https://fr.wikipedia.org/wiki/Intel_8086) : 29 000
1982: [80286](https://fr.wikipedia.org/wiki/Intel_80286) : 134 000
1985 : [80386](https://fr.wikipedia.org/wiki/Intel_80386) : 275 000
1989 : [80486](https://fr.wikipedia.org/wiki/Intel_80486) : 1,16 million
1993 : [Pentium/Pentium MMX](https://fr.wikipedia.org/wiki/Pentium_(microprocesseur)) : 3,1 millions
1995 : [Pentium Pro](https://fr.wikipedia.org/wiki/Pentium_Pro) : 5,5 millions
1997: [Pentium II](https://fr.wikipedia.org/wiki/Pentium_II) : 27 millions
1997 : [K6](https://fr.wikipedia.org/wiki/AMD_K6) : 8,8 millions
1998 : [K6-II](https://fr.wikipedia.org/w/index.php?title=K6-II&action=edit&redlink=1) : 9,3 millions
1999 : [Athlon](https://fr.wikipedia.org/wiki/Athlon) : 37 millions
2001 : [Pentium 4 HT](https://fr.wikipedia.org/wiki/Pentium_4) : 42 millions
2001 : [Athlon XP](https://fr.wikipedia.org/wiki/Athlon)-[Duron](https://fr.wikipedia.org/wiki/Duron) Palomino/Thoroughbred/Thorton/Barton-Spitfire/Morgan/Applebred : 37.2 millions
2003: [Athlon 64](https://fr.wikipedia.org/wiki/Athlon_64) ClawHammer : 105,9 millions
2004 : [Pentium Extreme Edition](https://fr.wikipedia.org/wiki/Pentium_Extreme_Edition) : 169 millions
2004 : [Athlon 64](https://fr.wikipedia.org/wiki/Athlon_64) Newcastle : 68,5 millions
2004 : [Athlon 64](https://fr.wikipedia.org/wiki/Athlon_64) Winchester : 77 millions
2005 : [Athlon 64](https://fr.wikipedia.org/wiki/Athlon_64) Venice : 76 millions
2005 : [Athlon 64](https://fr.wikipedia.org/wiki/Athlon_64)/[Athlon 64 X2](https://fr.wikipedia.org/wiki/Athlon_64_X2) Manchester/Toledo : 233 millions
2006 : [Core 2 Duo](https://fr.wikipedia.org/wiki/Intel_Core_2) : 291 millions
2006 : [Core 2 Quad](https://fr.wikipedia.org/wiki/Intel_Core_2) : 582 millions
2008 : [Core i7](https://fr.wikipedia.org/wiki/Intel_Core_i7) Bloomfield : 730 millions
2008 : [Phenom](https://fr.wikipedia.org/wiki/AMD_Phenom) X4/X3/[Athlon X2](https://fr.wikipedia.org/wiki/Athlon_64_X2) Agena/Toliman/Kuma: 450 millions
2009  : [Intel Core i57/i5](https://fr.wikipedia.org/w/index.php?title=Intel_Core_i57/i5&action=edit&redlink=1) Lynnfield : 774 millions
2010 : [Core i5/i3/Pentium G](https://fr.wikipedia.org/w/index.php?title=Core_i5/i3/Pentium_G&action=edit&redlink=1) Clarkdale : 382 millions
2010 : [Core i7](https://fr.wikipedia.org/wiki/Intel_Core_i7) Gulftown : 1,17 milliard
2010 : [Phenom II](https://fr.wikipedia.org/wiki/Phenom_II) X4/X3/X2-[Athlon II](https://fr.wikipedia.org/wiki/Athlon_II) X4/X3/X2: Deneb/Heka/Callisto-Propus/Rana/Regor : 758 millions
2011 : [Core i7/i5/i3/Pentium G](https://fr.wikipedia.org/w/index.php?title=Core_i7/i5/i3/Pentium_G&action=edit&redlink=1) Sandy bridge : 1,16 milliard (i7 et i5) - 504 millions (i3 et Pentium G)
2012 : [Core i7](https://fr.wikipedia.org/wiki/Core_i7) [Sandy Bridge](https://fr.wikipedia.org/w/index.php?title=Sandy_Bridge-E&action=edit&redlink=1) : 2,27 milliards
2012 : [Core i7/i5/i3/Pentium G](https://fr.wikipedia.org/w/index.php?title=Core_i7/i5/i3/Pentium_G&action=edit&redlink=1) Ivy Bridge : 1,40 milliard
2012 : [FX-4100/6100/8100](https://fr.wikipedia.org/w/index.php?title=FX-4100/6100/8100&action=edit&redlink=1) Zambezï (Buldozer) : 1,20 milliard
2012: [FX-4300/6300/8300](https://fr.wikipedia.org/w/index.php?title=FX-4300/6300/8300&action=edit&redlink=1) Vishera : 1,20 milliard
2013 : FX-9590 Vishera : 1,6 milliard
2014 : [Core i7](https://fr.wikipedia.org/wiki/Core_i7) [Haswell](https://fr.wikipedia.org/wiki/Haswell_(microarchitecture)) : 2,6 milliards

Domaine [graphique](https://fr.wikipedia.org/wiki/Processeur_graphique) :

1997 : [SST-1](https://fr.wikipedia.org/w/index.php?title=SST-1&action=edit&redlink=1) (3Dfx Voodoo 1) : 1 million
1998 : [NV4](https://fr.wikipedia.org/w/index.php?title=NV4&action=edit&redlink=1) (Nvidia TNT) : 7 millions
1998 : [Rage 5](https://fr.wikipedia.org/w/index.php?title=Rage_5&action=edit&redlink=1) (ATI Rage 128) : 8 millions

1999 : [NV10](https://fr.wikipedia.org/w/index.php?title=NV10&action=edit&redlink=1) (Nvidia GeForce256) : 23 millions

2001: [NV20](https://fr.wikipedia.org/w/index.php?title=NV20&action=edit&redlink=1) (Nvidia GeForce3 Ti) : 57 millions

2004 : [R480](https://fr.wikipedia.org/w/index.php?title=R480&action=edit&redlink=1) (ATI Radeon X850) : 160 millions

2006 : [G92](https://fr.wikipedia.org/wiki/G92) (Nvidia GeForce 9800) : 754 millions

2009 : RV870 (ATI Radeon HD5800/5900) : 2,154 milliards

2012 : [GK104](https://fr.wikipedia.org/w/index.php?title=GK104&action=edit&redlink=1) (Nvidia GeForce GTX600) : 3,54 milliards

2019 : [TU102](https://fr.wikipedia.org/w/index.php?title=TU102&action=edit&redlink=1) (Nvidia RTX titan) : 18,6 milliards
2020 : GA102 (Nvidia RTX 3080) : 28,3 milliards

[Serveurs](https://fr.wikipedia.org/wiki/Serveur_informatique) :

- 2019 : AMD Threadripper 3990X (64 coeurs) : 23,25 milliards





_______

Mieszczak Christophe Licence CC-BY-SA

source images : 

* [tube à vide-wikipédia - domaine publique](https://commons.wikimedia.org/wiki/File:6N23P.JPG)
* [transistor - wikipedia- GNU](https://commons.wikimedia.org/wiki/File:Transistors.agr.jpg)
* productions personnelles





 
# Architecture Von Neumann



## Un calculateur universel



Qu'est-ce que signifie le fait de "pouvoir calculer quelque chose" ?



 En **1936**, **[Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing)** veut répondre à cette question  en proposant une machine à calculer universelle. Il s'agit d'une machine théorique et virtuelle qu'on l'appelle [machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing) : Turing ne l'a jamais construite.

En faite, une telle machine a été fabriquée la première fois en 2012 à l'occasion du centenaire de la naissance de Turing, par [des étudiants en master à l'école normale supérieure de Lyon](http://rubens.ens-lyon.fr/fr/) .



Cette machine est constituée :

* D' un **ruban aussi grand que l'on veut** divisé en cases consécutives. Chaque case contient un symbole d'un _alphabet_. L'alphabet contient un symbole spécial appelé « symbole  blanc », et un ou plusieurs autres  symboles. La machine doit toujours avoir  assez de longueur de ruban pour son exécution. On considère que les  cases du ruban contiennent par défaut le « symbole blanc » .

* Une **tête de lecture/écriture**  qui peut lire et écrire les symboles sur le ruban, et se déplacer vers la gauche ou vers la droite du ruban

* Un **registre d'état** qui mémorise l'état courant de la machine de Turing. Le nombre d'états possibles est toujours fini, et il existe  un état spécial appelé « état de départ » qui est l'état initial de la  machine avant son exécution. 

* Une **table d'actions** qui indique à la machine quel symbole écrire sur le ruban, comment déplacer la tête de lecture (vers la droite ou la gauche), et quel est le nouvel état, en fonction  du symbole lu sur le ruban et de l'état courant de la machine. Si aucune action n'existe pour une combinaison donnée d'un symbole lu et d'un  état courant, la machine s'arrête.

    

### Un premier calcul 

Etat de départ : le ruban est vide avec un seul 0 en face de la tête de lecture :

|      |      |      |      |      |      |      |      |      |      |      | V    |      |      |      |      |      |      |      |      |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
|      |      |      |      |      |      |      |      |      |      |      | 0    |      |      |      |      |      |      |      |      |



État 1 :

* si la tête lit 0 alors elle écrit 0, se déplace vers la droite et reste dans l'état 1
* si la tête lit 1 alors elle écrit 1, se déplace vers la droite et reste dans l'état 1
* si la tête lit une case vide, elle n'écrit rien, se déplace vers la à gauche et passe à l'état 2

État 2 :

* si la tête lit 0 alors elle écrit 1, se deplace vers la gauche et passe à l'état 3
* si la tête lit 1 alors elle écrit 0, se déplace vers la gauche et reste à l'état 2
* si la tête lit une case vide, elle écrit 1 et  passe à l'état 4

État 3 :

* si la tête lit 0 alors elle écrit 0, se déplace vers la gauche et reste dans l'état 3
* si la tête lit 1 alors elle écrit 1, se déplace vers la gauche et reste dans l'état 3
* si la tête lit une case vide, elle n'écrit rien, se déplace vers la à droite et passe à l'état 4

État 4 :

* fin du programme



> * Faites _tourner_ ce programme plusieurs fois de suite.
> * Que fait-il ?



### Un autre calcul

On peut résumer les états dans des tableaux où chaque ligne nous dit : si on lit ceci (0, 1 ou rien) alors on écrit cela (0, 1 ou rien) , on se déplace de cette façon (0 à gauche, 1 à droite, ou pas de déplacement) et on passe dans l'état précisé.

| Etat en cours | si on lit | alors on écrit | on se déplace | on passe à l'état |
| :-----------: | :-------: | :------------: | :-----------: | :---------------: |
|       1       |     0     |       1        |       1       |         1         |
|       1       |     1     |       0        |       1       |         1         |
|       1       |           |                |       0       |         2         |
|       2       |     0     |       0        |       0       |         2         |
|       2       |     1     |       1        |       0       |         2         |
|       2       |           |                |       1       |        fin        |



Etat de départ : le ruban est vide avec un nombre écrit en binaire à partir de la tête de lecture :

|      |      |      |      |      |      |      |      |      |      |      | V    |      |      |      |      |      |      |      |      |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
|      |      |      |      |      |      |      |      |      |      |      | 0    | 1    | 1    | 0    |      |      |      |      |      |

> * Faites _tourner_ ce programme plusieurs fois de suite.
> * Que fait-il ?



### Vers la machine universelle

Cette représentation donne l'impression qu'il y a d'un côté un programme (la table d'actions) et de l'autres les données (le ruban et ce qui y est écrit). Il semble alors qu'il soit nécessaire d'avoir une machine spécifique pour chaque calcul différent.

Mais Turing imagine que les différents états et la table d'actions peuvent être eux aussi lus et écrits sur une machine de Turing.

Cette idée parait anodine, elle est pourtant cruciale : **le programme devient lui aussi une donnée** !

Sa machine devient **universelle** et peut réaliser n'importe quel calcul. 

Nous reverrons l'année prochaine que la notion de calculabilité est intimement liée à la machine de Turing. La thèse Church-Turing affirme en effet qu'est calculable toute chose programmable sur une machine de Turing et reciproquement.





# Architecture Von Neumann



En 1945, **[Von Neumann](https://fr.wikipedia.org/wiki/John_von_Neumann)**, pour l'architecture de l'[EDVAC](https://fr.wikipedia.org/wiki/Electronic_Discrete_Variable_Automatic_Computer) ( le premier [ordinateur](https://fr.wikipedia.org/wiki/Ordinateur) entièrement [électronique](https://fr.wikipedia.org/wiki/Électronique) construit pour être [Turing-complet](https://fr.wikipedia.org/wiki/Turing-complet). Il peut être reprogrammé pour résoudre, en principe, tous les problèmes calculatoires.),  devance Alan Turing en donnant un modèle qui utilise une structure de stockage unique pour conserver à la fois  les instructions et les données demandées ou produites par le calcul. De telles machines sont aussi connues sous le nom d’[ordinateur à programme enregistré](https://fr.wikipedia.org/wiki/Ordinateur_à_programme_enregistré)  : c'est la naissance de **l'architecture Von Neumann **qui est encore aujourd'hui celle de nos ordinateurs.

![architecture von Neuman - source wikipédia - GNU ](https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Von_Neumann_architecture_fr.svg/420px-Von_Neumann_architecture_fr.svg.png)





L’**architecture de Von Neumann** décompose l’ordinateur en 4 parties distinctes :

* l’**[unité arithmétique et logique](https://fr.wikipedia.org/wiki/Unité_arithmétique_et_logique)** (UAL ou *ALU* en anglais) ou unité de traitement : son rôle est d’effectuer les opérations de base.
* l’**[unité de contrôle](https://fr.wikipedia.org/wiki/Unité_de_contrôle)**, chargée du « séquençage » des opérations. 
* la **[mémoire](https://fr.wikipedia.org/wiki/Mémoire_(informatique))** qui contient à la fois les données et le programme qui indiquera à  l’unité de contrôle quels sont les calculs à faire sur ces données. La  mémoire se divise entre mémoire volatile (programmes et données en cours de fonctionnement) et mémoire permanente (programmes et données de base de la machine) 
* les dispositifs d’**[entrée-sortie](https://fr.wikipedia.org/wiki/Entrées-sorties)**, qui permettent de communiquer avec le monde extérieur.



_Remarques_ :

Un processeur contient l'unité arithmétique et logique, l'unité de contrôle mais aussi une petite partie de mémoire, appelée **registre** :  il s'agit de la mémoire la plus rapide d'un ordinateur. Il y en a peu car  son coût de fabrication est le plus élevé et la place dans un [processeur](https://fr.wikipedia.org/wiki/Microprocesseur) est limitée.



### Programmes = données

En traitant les programme de la même façon que les données, on peut désormais concevoir des programmes qui utilisent d'autres programmes. C'est grâce à cette caractéristique qu'on peut desormais : 

* coder : un programme peut désormais générer le code d'un autre programme.
* utiliser un système d'exploitation (windows, linux, ios, android ...) qui gère tous les autres programmes
* télécharger des programmes à partir de programmes
* mais aussi _attraper_ des programmes mal intentionnés (virus, malware ...) qui modifient le fonctionnement normal des autres programmes à des fins souvent nuisibles.



Cette caractéristique est tellement essentielle qu'elle donne l'une des définitions de ce qu'est un ordinateur (il y en a d'autres comme bien souvent ...).

### Havard

L’**architecture de type Harvard** est une conception des [processeurs](https://fr.wikipedia.org/wiki/Processeur) qui sépare physiquement la mémoire de données et la mémoire programme. L’accès à chacune des deux mémoires s’effectue via deux [bus](https://fr.wikipedia.org/wiki/Bus_informatique) distincts. 

![](https://upload.wikimedia.org/wikipedia/commons/6/69/Architecture_Harvard.png?20120425180635)



Cette architecture peut se montrer plus rapide et moins honéreuse à produire à technologie identique qu'une architecture Von Neumann mais complique certaine applications. On l'utilise souvent pour les microcontrôleurs, comme la carte :microbit.

Une carte microbit embarque de l'informatique mais nest pas conçue selon l'architecture von Neumann : données et programmes sont dans des mémoires séparées. Il est difficile d'y faire tourner un programme qui utilise un programme et encore plus d'y installer un système d'exploitation. Ce n'est pas pour cela qu'elle est conçue.



# On joue en équipe ?



Pendant des années, pour augmenter les performances des ordinateurs, les constructeurs utilisaient principalement deux stratégie :

* La réduction de la taille des transistors, grâce aux progrès technologiques, a permis d'augmenter le nombre de ces composants sur le microprocesseur.

    * En 1975, Gordon Moore énonce une prévision basée sur l'évolution du nombre de transistors sur une puce : ce nombre est sensé doubler chaque année. Le constructeurs font en sorte de s'aligner sur ce qu'on appelle la **[loi de Moore](https://fr.wikipedia.org/wiki/Loi_de_Moore)** pendant très longtemps.
    * Aujourd'hui on atteint une limite physique : la taille des transistors est de l'ordre de quelques atomes, réduire davantage n'est plus vraiment possible car la miniaturisation est telle que les fabricants commencent à buter sur des effets quantiques. Par conséquent, augmenter leur nombre devient très difficile.

    ![loi de Moore](https://upload.wikimedia.org/wikipedia/commons/a/a4/Loi_de_Moore.png)

* L'augmentation de la fréquence d'horloge des microprocesseurs est également un facteur de performance : elle est liée à sa capacité  d'exécuter un nombre plus ou moins important d'instructions machines par seconde. Plus la fréquence d'horloge du CPU est élevée, plus ce CPU est capable d'exécuter un grand nombre d'instructions machines par seconde (en fait, c'est un peu plus compliqué que cela, mais nous nous  contenterons de cette explication). Cependant ...	

    * L'augmentation des fréquences entraine une augmentation de la consommation électrique : en doublant la fréquence, on multiplis sa consommation par 8. Cela est un important problème à une époque où on cherche à augmenter l'autonomie de materiel embarquant des processeurs (téléphones, objets connectés...)
    * L'augmentation des fréquences entraine également une forte augmentation de la température et donc implique de recourrir à un refroidissement bruyant et consommant lui aussi de l'énergie !

    ![evolution des fréquences - source personnelle](media/evolution_freq.png)



Pour poursuivre la course à la performance il faut donc trouvez d'autres solutions :



* Les microprocesseurs actuels embarquent plusieurs _coeurs_, c'est à dire plusieurs processeurs dans le processeurs. 

    * Aujourd'hui (en 2020) on trouve sur le marché des CPU possédant jusqu'à 18 coeurs ! Même les smartphones possèdent des microprocesseurs  multicoeurs : le Snapdragon 845 possède 8 coeurs. 
    * Le gain semble évident : potentiellement, on calcule deux fois plus vite en doublant _seulement_ la consommation plutôt qu'en la multipliant par 8. 
    * En réalité, le gain n'est obtenu que pour les applications spécifiquement programmé pour utilisés cette architecture et travailler en _parallèle_ , c'est à dire en effectuant plusieurs tâches simultanément en exploitant simultanément plusieurs coeurs. **C'est donc la qualité des algorithmes qui, en faisant travailler les coeurs en équipe,  feront la différence !**

    

* D'autres architectures apparaissent également, même si elles ne sont pas encore tout à fait au point, du moins pour le grand public :

    * l'[ordinateur quantique](https://fr.wikipedia.org/wiki/Calculateur_quantique) que les scientifiques étudient depuis les années 1990 et qui fait des progrès impressionnant depuis quelques années;
    * moins connu, l'[ordinateur ADN](https://fr.wikipedia.org/wiki/Ordinateur_%C3%A0_ADN), un ordinateur non non électronique actuellement explorées pour résoudre des problèmes combinatoires. C'est son immense capacitée de stockage qui fait sa force.



_______

Mieszczak Christophe CC - BY - SA

_sources images :_

* productions personnelles 
* wikipédia (licence GNU  et CC-BY-SA auteur [seofilo](http://seofilo.com/que-es-la-ley-de-moore/))
* [archtexture harvard - wikipédia](https://commons.wikimedia.org/wiki/File:Architecture_Harvard.png)





 
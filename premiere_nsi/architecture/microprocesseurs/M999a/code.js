/////////////////////////////////////////
///// variables globales/////////////////
////////////////////////////////////////

var memoireEnCours=0;


/////////////////////////////////////////
///// abonnements///////////////////////
////////////////////////////////////////


function demarrer () {
// cases mémoires et abonnements///////////////////////
    document.getElementById("memoire").innerHTML=genererTableau(0,11,11);
    for (var i=0;i<99;i++) {
        document.getElementById(i).addEventListener("click",remplirMemoire);
    }

    document.getElementById("0").className="celluleEnCours";
    document.getElementById("annuler").addEventListener("click",annuler);
    document.getElementById("valider").addEventListener("click",valider);
    document.getElementById("caseMemoire").addEventListener("change",valider);
    document.getElementById("caseMemoire").addEventListener("keypress",verifierIntruction);
    //lancer
    document.getElementById("lancer").addEventListener("click",lancerOuStopper);
    //initialiser
        document.getElementById("initialiser").addEventListener("click",initialiser);

    // le menu
    for (var i=1;i<10;i++) {
        document.getElementById("menu"+i).addEventListener("click",choisirMenu);
    }
// crédits
        document.getElementById("fermeCredits").addEventListener("click",fermerFenetres);
        document.getElementById("fermeQuEstCe").addEventListener("click",fermerFenetres);
        document.getElementById("fermePapier").addEventListener("click",fermerFenetres);
}


////////////////////////////////////////
//////////////fonctions/////////////////
////////////////////////////////////////

///// le menu

function choisirMenu(){
    switch (this.id) {
        case "menu1" :
            document.getElementById("sectionMemoire").style.display="none";
            document.getElementById("sectionCredit").style.display="none";
            document.getElementById("sectionPapier").style.display="none";
            document.getElementById("sectionQuEstCe").style.display="flex";

            break;
        case "menu2":
            document.getElementById("sectionMenu").style.display="none";
            document.getElementById("sectionPapier").style.display="none";
            document.getElementById("sectionQuEstCe").style.display="none";
            document.getElementById("sectionCredit").style.display="none";
            document.getElementById("sectionMemoire").style.display="flex";
            document.getElementById("sectionDemo").style.display="flex";

            break
        case "menu3":
                document.getElementById("sectionMemoire").style.display="none";
                document.getElementById("sectionQuEstCe").style.display="none";
                document.getElementById("sectionCredit").style.display="none";
                document.getElementById("sectionPapier").style.display="flex";

            break
        case "menu4" :
                document.getElementById("sectionPapier").style.display="none";
                document.getElementById("sectionMemoire").style.display="none";
                document.getElementById("sectionQuEstCe").style.display="none";
                document.getElementById("sectionCredit").style.display="flex";

            break;
        case "menu5":
            viderMemoire();
            mettre(0,"510");
            mettre(1,"10");
            mettre(10,"001");
            mettre(11,"101");
            mettre(12,"302");
            mettre(13,"420");
            mettre(14,"302");
            mettre(15,"599");
            break

        case "menu6":
            viderMemoire();
            mettre(0,510);
            mettre(1,"5");
            mettre(2,"10");
            mettre(3,"20");
            mettre(10,"001")
            mettre(11,'102');
            mettre(12,'300');
            mettre(13,'420');
            mettre(14,'103');
            mettre(15,'300');
            mettre(16,'599');
            break

        case "menu7":
            viderMemoire();
            mettre(0,"004");
            mettre(1,"402");
            mettre(2,"206");
            mettre(3,"510");
            mettre(4,"10");
            mettre(5,"1");
            mettre(10,"006");
            mettre(11,"105");
            mettre(12,"301");
            mettre(13,"206");
            mettre(14,"106");
            mettre(15,"004");
            mettre(16,"300");
            mettre(17,"204");
            mettre(18,"412");
            mettre(19,"610");
            mettre(20,"402");
            mettre(21,"599");
            break
        case "menu8":
            viderMemoire();
            mettre(0,"510");
            mettre(1,"10");
            mettre(2,"1");
            mettre(10,"001");
            mettre(11,"102");
            mettre(12,"301");
            mettre(13,"203");
            mettre(14,"003");
            mettre(15,"301");
            mettre(16,"204");
            mettre(17,"520");
            mettre(20,"001");
            mettre(21,"103");
            mettre(22,"302");
            mettre(23,"201");
            mettre(24,"003");
            mettre(25,"102");
            mettre(26,"301");
            mettre(27,"203");
            mettre(28,"004");
            mettre(29,"301");
            mettre(30,"204");
            mettre(31,"620");
            mettre(32,"001");
            mettre(33,"402");
            mettre(34,"599");
            break

        case "menu9":
            document.getElementById("sectionDemo").style.display="none";
            document.getElementById("sectionMenu").style.display="flex";
            break
    }
}

function fermerFenetres(){
    document.getElementById("sectionCredit").style.display="none";
    document.getElementById("sectionPapier").style.display="none";
    document.getElementById("sectionQuEstCe").style.display="none";
    document.getElementById("sectionMemoire").style.display="flex";
}

function mettre(memoire,no){
    document.getElementById(memoire).innerHTML=no;
}

function viderMemoire(){
    for (var i=0;i<99;i++) {
        document.getElementById(i).innerHTML="";
    }
    document.getElementById("R").innerHTML="";
    document.getElementById("A").innerHTML="";
    document.getElementById("B").innerHTML="";
    document.getElementById("PC").innerHTML="0";
    document.getElementById("SP").innerHTML="0";

}

function initialiser(){
    if (confirm("Vous désirez vraiment initialiser?")) {
        viderMemoire();
    }
}

//////// generer le tableau des mémoires
function genererTableau(indiceDebut,l,h) {
    var chaine='';
    for (var j=0;j<h;j++) {
        chaine+="<TR>";
        for (var i=0;i<l;i++) {
            if ((i>0) && (j>0) && ((i-1)+(j-1)*(l-1)+indiceDebut<99)) {
                chaine+="<td id='"+((i-1)+(j-1)*(l-1)+indiceDebut)+"' class='celluleMemoire' Title='Mémoire No "+((i-1)+(j-1)*(l-1)+indiceDebut)+"'></td>"
            }
            else if (((i-1)+(j-1)*(l-1)+indiceDebut==99) &&(i>0) && (j>0) ){
                chaine+="<td class='celluleFin' id='99' Title='Stop (Halt)'>HLT</td>"
            }
            else if ((i==0)&&(j!=0)) {
                chaine+="<td  class='colonne1' >"+(10*(j-1))+"</td>"
            }
            else if ((j==0) && (i!=0)){
                chaine+="<td  class='ligne1' >"+(i-1)+"</td>"
            }
            else {
                chaine+="<td  class='coin' ></td>"
            }
        }
        chaine+="<TR>";
    }
    return chaine;
}


function remplirMemoire(){
    document.getElementById("sectionMemoire").style.display="none";
    document.getElementById("sectionRemplir").style.display="flex";
    document.getElementById("noCase").innerHTML="Mémoire No "+this.id;
    memoireEnCours=this.id;
    document.getElementById("caseMemoire").focus();
}

function annuler(){
    document.getElementById("sectionRemplir").style.display="none";
    document.getElementById("sectionMemoire").style.display="flex";
}

function valider(){

        document.getElementById("sectionRemplir").style.display="none";
        document.getElementById("sectionMemoire").style.display="flex";
        document.getElementById(memoireEnCours).innerHTML=document.getElementById("caseMemoire").value;
        document.getElementById("caseMemoire").value="";


}

function verifierIntruction(event){
    document.getElementById("lancer").value="Lancer";
    if (((event.key.charCodeAt(0)<48) || (event.key.charCodeAt(0)>57) ||(document.getElementById("caseMemoire").value.length==3) ) // interdit les touches autres que les chiffres
        &&(event.key.charCodeAt(0)!=65) // autorise les flèches
        && (event.key.charCodeAt(0)!=66) // autorise backspace
        && (event.key.charCodeAt(0)!=68)  // autorise suppr
        && (event.key.charCodeAt(0)!=69)

        // autorise entrée, sinon on ne peut pas valider
        ) { // autorise les flèches, backspace et suppr
        event.preventDefault();
    }
}

function lancerOuStopper(){
    if (this.value=="Lancer") {
        this.value="Stopper";
        if (document.getElementsByClassName("celluleEnCours")[0]) {
            document.getElementsByClassName("celluleEnCours")[0].className="celluleMemoire";
        }
        document.getElementById("99").className="celluleFin";
        document.getElementById("PC").innerHTML="00";
        document.getElementById("A").innerHTML="";
        document.getElementById("B").innerHTML="";
        document.getElementById("R").innerHTML="";
        document.getElementById("0").className="celluleEnCours";
        temporiserDecoder();
    }
    else {
        this.value="Lancer";
        document.getElementById("99").className="celluleFin";
    }
}

function temporiserPC(addr){
    if ((document.getElementById("PC").innerHTML!="99")&&(document.getElementById("lancer").value!="Lancer")) {
        var timer=setTimeout(function () {bougerPC(addr)},(5001-document.getElementById("vitesse").value)/2);
    }
    else {
        document.getElementById("lancer").value="Lancer";
    }
}

function temporiserDecoder(){
/// toutes les cases en noir
    for (var i=0;i<99;i++) {
        document.getElementById(i).style.border="2px solid black";
    }
    document.getElementById("A").style.border="4px solid black";
    document.getElementById("B").style.border="4px solid black";
    document.getElementById("R").style.border="4px solid black";
    if ((document.getElementById("PC").innerHTML!="99")&&(document.getElementById("lancer").value!="Lancer")) {
        var timer=setTimeout(decoder,(5001-document.getElementById("vitesse").value)/2);
    }
    else {
        document.getElementById("lancer").value="Lancer";
    }
}

function decoder(){

/// decodage

    var ok=0; // 0=erreur 1=augmentrPC à la fin 2=decoder à la fin
    code=document.getElementById(eval(document.getElementById("PC").innerHTML)).innerHTML;
    if (code=="") {
        temporiserPC();
        return
    }
    if (code.length!=3) {
        document.getElementById("lancer").value="Lancer";
        alert(code+" n'est pas une instruction valide");
        return
    }
    instruction=code.substring(0,1);
    addr=code.substring(1,3);

    switch (instruction) {

        case "0" : // copier case addr dans A
            try {
                document.getElementById("A").innerHTML=document.getElementById(eval(addr)).innerHTML;
                document.getElementById("A").style.border="4px solid red";
                document.getElementById(eval(addr)).style.border="4px solid red";
                ok=1;
            }
            catch{}
            break
        case "1" : // copier case addr dans B
            try{
                document.getElementById("B").innerHTML=document.getElementById(eval(addr)).innerHTML;
                document.getElementById("B").style.border="4px solid red";
                document.getElementById(eval(addr)).style.border="4px solid red";
                ok=1;
            }
            catch{}
            break
        case "2" : // copier register R dans addr
            try{
                document.getElementById(eval(addr)).innerHTML=document.getElementById("R").innerHTML;
                document.getElementById("R").style.border="4px solid red";
                document.getElementById(eval(addr)).style.border="4px solid red";
                ok=1;
            }
            catch{}
            break
        case "3" :
            try {
                if (addr=="00") { // ajouter A et B et stocker dans R
                    document.getElementById("R").innerHTML=eval(document.getElementById("A").innerHTML)+eval(document.getElementById("B").innerHTML);
                    document.getElementById("A").style.border="4px solid red";
                    document.getElementById("B").style.border="4px solid red";
                    document.getElementById("R").style.border="4px solid red";
                    ok=1;
                }
                if (addr=="01") {// soustraire A et B et stocker dans R
                    document.getElementById("R").innerHTML=eval(document.getElementById("B").innerHTML)-eval(document.getElementById("A").innerHTML);
                    document.getElementById("A").style.border="4px solid red";
                    document.getElementById("B").style.border="4px solid red";
                    document.getElementById("R").style.border="4px solid red";
                    ok=1;
                }
                if (addr=="02") {// multiplier A et B et stocker dans R
                    document.getElementById("R").innerHTML=eval(document.getElementById("A").innerHTML)*eval(document.getElementById("B").innerHTML);
                    document.getElementById("A").style.border="4px solid red";
                    document.getElementById("B").style.border="4px solid red";
                    document.getElementById("R").style.border="4px solid red";
                    ok=1;
                }
            }
            catch {}
            break
        case '4' : // copie le registre r1 dans r2
            try {
                r1=addr.substring(0,1);
                r2=addr.substring(1,2);
                if (((r1=="0") || (r1=="1")||(r1="2"))&&((r2=="0") || (r2=="1") || (r2=="2")) ){
                    nomReg=["A","B","R"];
                    document.getElementById(nomReg[r2]).innerHTML=document.getElementById(nomReg[r1]).innerHTML;
                    document.getElementById(nomReg[r2]).style.border="4px solid red";
                    document.getElementById(nomReg[r1]).style.border="4px solid red";
                    ok=1;
                }
            }
            catch{}
            break


        case '5' : // sauter à la mémoire addr
            try{
                document.getElementById(eval(addr)).style.border="4px solid red";
                ok=2;
            }
            catch{}
            break;
        case "6": // branche à addr si R > 0
            try{
                document.getElementById("R").style.border="4px solid red";
                document.getElementById(eval(addr)).style.border="4px solid red";
                if (eval(document.getElementById("R").innerHTML) > 0) {
                    ok=2;
                }
                else {
                    ok=1;
                }
            }
            catch{}
            break

    }
    if (ok==0) {
        alert(code+" n'est pas une instruction valide");
        document.getElementById("lancer").value="Lancer"
    }
    else if (ok==1){
        temporiserPC(eval(document.getElementById("PC").innerHTML)+1);
    }
    else {
        temporiserPC(addr);
    }
}

function bougerPC(addr){
/// toutes les cases en noir
    for (var i=0;i<100;i++) {
        document.getElementById(i).style.border="2px solid black";
    }
    try {
    document.getElementById("A").style.border="4px solid black";
    document.getElementById("B").style.border="4px solid black";
    document.getElementById("R").style.border="4px solid black";
/// augmentePC et decale la case
    document.getElementsByClassName("celluleEnCours")[0].className="celluleMemoire";
    document.getElementById("PC").innerHTML=addr;
    document.getElementById(eval(document.getElementById("PC").innerHTML)).className="celluleEnCours";
    temporiserDecoder();
    }
    catch{
        alert(addr);
    }
}



////////////////////////////////////////
//////////////CORPS/////////////////////
////////////////////////////////////////



window.addEventListener("load", demarrer); // attends le chargement complet pour démarrer

# Le SHELL

Le SHELL est une interface  entre l'utilisateur est l'OS en mode texte, apparu au début des années  70. Il interprète un certains nombre de commandes qui permettent de  donner des "ordre" au système d'exploitation. Les système d'exploitation modernes (Linux, Windows, OS-X, ) apparus  dans les années 90, on ajouté une couche graphique pour rendre cette  interface plus intuitive ... en masquant une partie du fonctionnement  réel de l'OS. Nous allons utiliser le `SHELL Bash` afin de réaliser un certains nombres de tâches.

N'hésitez pas à visiter [cette page](https://fr.wikipedia.org/wiki/Unix) pour plus d'informations historiques.

![BASH wikipedia GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Linux_command-line._Bash._GNOME_Terminal._screenshot.png/800px-Linux_command-line._Bash._GNOME_Terminal._screenshot.png)



Nous allons utiliser les commandes de base afin de manipuler, en mode console, un système de fichier.



### Où sommes nous ?



- La commande `pwd` (`p`rint `w`ork `d`irectory) renvoie le répertoire courant dans lequel vous êtes actuellement. Testez là.

```
 ~$ pwd
??? 
```

- La commande `ls` (`l`i`s`t) permet de lister, par défaut,  ce qui n'est pas caché "dans" le répertoire courant.


```
~$ ls
???
```

- `ls` peut être utilisé avec différents paramètres :

    - `ls rep` liste les fichiers présents dans le répertoire *rep*.pas ici) que l'utilisateur peut manipuler mais que la couche graphique de l'OS lui masque en donnant l'illusion d'un empilement hiérarchique.

    ```
    ~$ ls Documents
    ???
    ```

    - `ls -a ` liste les fichiers présents dans le répertoire courant **y compris les fichiers cachés**.

    

    - `ls -li ` affiche la liste des _i-numbers_ et les _i-nodes_ correspondants.

    ```txt
    ~$ ls -li
    ???
    ```

    - `ls -l rep` liste les fichiers présents dans le répertoire *rep* au format long, c'est en affichant toutes les caractéristiques des fichiers, notamment leurs droits.

    ```
    ~$ ls -l
    ???
    ```



_Remarque :_

On repère facilement des noms d'utilisateurs, des tailles de fichiers en octets, des dates ... mais  **drwxrwxr...** qu'est ce que c'est que ce charabia ?? Il s'agit des droits accordés, ou non, aux utilisateurs. On étudiera cela un peu plus tard.



Encore une remarque:_

Hou là là, il y en a des commandes ! Comment s'y retrouver ? Heureusement, il y a un manuel intégré. Vous vous souvenez que la commande `ls` est là pour lister mais pas le reste, demander le manuel en utilisant la commande `man` :

```
~$ man ls

```





## Créer et manipuler les fichiers et répertoires



- La commande `mkdir rep`  (`m`a`kd`i`r`ectory) crée le répertoire *rep* dans le dossier courant.

    ```
    ~$ mkdir test
    ~$ ls
    ???
    ```

    

- La commande `cd rep` permet de se placer dans le sous-répertoire *rep*.

- La commande `cd ..` remonte dans le répertoire parent.

- La commande `cd` remonte à la racine

> A l'aide de ces commandes, créez l'arborescence ci-dessous sous le répertoire `test`.
>
> ![tree - perso](media/tree.png)





_Remarque :_

* Les espaces étant utilisés comme séparateur, il vaut mieux éviter de les utiliser pour nommer les fichiers. Dans le cas contraire, il faudra mettre le nom du fichier entre apostrophes à chaque fois qu'on voudra l'utiliser dans une commande. 

* Il est également conseillés d'éviter les accents et majuscules, souvent sources d'erreurs.





- La commande `touch fichier` créer le fichier *fichier* dans le répertoire courant.

> * Utilisez la commande `touch` afin de créer les fichiers et d'obtenir l'arbre ci-dessous. Remontez ensuite à la racine.
>
>     ![tree - perso](media/tree2.png)





- La commande `cp source destination` permet de copier un ficher depuis la _source_ vers la destination.
- La commande `mv source destination` permet de renommer un fichier ou de le déplacer.

> * copier le fichier `readme` du répertoire `jeu_de_la_vie` vers le répertoire `langages_de_prog`.
>
> * renommer ce nouveau fichier `.readme`
>
> * lister les fichiers présents de le répertoire  `langages_de_prog`. 
>
>     * Que constatez-vous ? 
>
>         ```
>                                         
>         ```
>
> 
>
>     * Allez voir un peu plus haut pour trouver comment lister ce fichier
>                     
>         ```
>         ~$ 
>         ```





- La commande `rm fichier` supprime le fichier (si vous en avez le droit... nous verrons cela bientôt)
- La commande `rmdir repertoire` supprime le répertoire (à condition qu'il soit vide et vous ayez le droit)

> Supprimez le répertoire  `langages_de_prog` .



- Il est possible de grouper le démarrage des processus d’une suite de commandes en les entourant de parenthèses.  Par exemple, si un processus à créer en arrière-plan doit comporter plusieurs commandes successives  il suffit de grouper les commandes avec des parenthèses, de séparer les  commandes souhaitées par des points virgule.

    Dans cet exemple, les commandes *commande1* et *commande2* seront exécuter l'une après l'autre.

```
 ( commande1 ; commande2 ) 
```

> Lancer une commande en une seule ligne qui  :
>
> * remonte à la racine, 
> * va dans le répertoire `/test/projets/` 
> * y crée un répertoire `scrabble`
> * va dans le répertoire `scrabble`
> * y crée un fichier `readme`



*Rem :*

En créant une telle commande, on crée un *processus* qui s’exécute en avant plan et on perd temporairement la main sur le  terminal. Dans notre exemple, la commande s’exécute très vite et ce n'est vraiment un problème. Si cela en devenait un, il faudrait demander à ce que le  processus s’exécute en arrière plan. Pour cela, il suffirait d'ajouter un *&* après la parenthèse

```
 ( commande1 ; commande2 )&
```



## La commande `cat`

- la commande `cat` recopie  l'entrée standard (le clavier) sur la sortie standard (l'écran). Pour  quitter cette commande il faut taper CTRL+d au clavier.

> Testez la commande `cat` seule. Ce que  vous validerez sur la première ligne (clavier=entrée standard) sera  recopiée sur la seconde (écran=sortie standard).

```
~$cat
    coucou 
    coucou
```

- La commande `cat fichier` place *fichier* en entrée et permet de visualiser son contenu sur la sortie standard qui est l'écran.

> Retournez dans le répertoire `scrabble` et tapez la commande `cat readme` Rien ne s'affiche ? normal, ce fichier est vide.

- La commande `cat > fichier` redirige l'entrée standard (le clavier) vers *fichier* qui devient la sortie. En clair, ce que vous taperez ensuite écrasera le contenu du *fichier*.

> Toujours dans le répertoire `scrabble` et tapez la commande `cat > readme` et tapez le texte ci-dessous 
>
> ``` 
> ceci est un projet autour du scrabble
> 
> ```



* Pour visualiser le contenu de votre fichier, il faut rediriger le fichier vers la sortie standard qui est l'écran en tapant la commande `cat readme`

> Allez-y !



- La commande `cat >> fichier` fait la même chose que précédemment mais, si *fichier* n'existe pas, elle le crée.

> * Créez un fichier ``plus`` contenant le texte ci-dessous
>
> ```
> il s'agit de trouver les mots que l'on peut écrire avec un certains nbre de lettres
> et de classer la liste obtenue selon différents comparateurs
> ```
>
> * Utilisez la commande `cat` pour le visualiser.



- La commande `cat fichier1 fichier2` concatène le contenu des deux fichiers.

> Testez `cat readme plus` . Quelle est la sortie dans ce cas ?



- Pour redirigez le résultat vers un fichier, il faut utiliser `cat fichier1 fichier2 > fichier3` si *fichier3* existe ou `cat fichier1 fichier2 >> fichier3` si _fichier3_ doit être créé.

> * Créez un fichier`a_propos` obtenu en concaténant les fichiers `readme` et `plus`.
> * Supprimer les fichiers  `readme` et `plus`.





_Remarque 1_ : A propos de `cat`

- La commande `cat` est pratique pour visualiser ou modifier des fichiers courts.

- Pour visualiser des fichiers plus long on peut utiliser la commande `less fichier` qui les affiche page par page le contenu de *fichier* (tapez q pour la quitter).

- Pour éditer des fichiers, on utilise l'éditeur de texte `ed` dont vous trouverez une description [ici](https://translate.google.com/translate?hl=fr&sl=en&u=https://en.wikipedia.org/wiki/Ed_(text_editor)&prev=search)

  ​    

_Remarque 2 :_ au sujet de la redirection



En utilisant `>`,`>>`, `<` ou `<<` vous pouvez rediriger la sortie ou l'entrée standard d'une commande. autre que `cat`. 



>  Redirigez la sortie de la commande `tree` vers un nouveau fichier `arbre` que vous placerai sous le répertoire `test` . Afficher ensuite le contenu du fichier.
>
>  ```
>  ~$ 
>  ```
>
>  





## Gestion des droits.

### Comprendre les droits.

Plusieurs personnes peuvent être amenés à utiliser une même machine. Il faut donc établir des droits pour chaque  utilisateur. Peuvent-ils seulement lire les fichiers ? les modifier ?  les exécuter ?

Imaginons la situation ci-dessous :

```
~/ls -l
drwxrwsr-x 13 tofmzk staff  4096 sept. 1 13:04 test
-rw-rw-r-- 1 luc luc 0 sept. 100 00:47 test1.txt
-rwxr--r-- 3 david david 5 juin 96:30 test2.txt
```

- Il est important de distinguer les dossiers des fichiers (même si un dossier est un fichier particulier) :
    - Le *d* par lequel commence la série *drwxrwx* est là parce que cette ligne concerne le répertoire ( directory) _test_ dont un certain *tofmzk* est propriétaire. Ici, on précise dont les droits de *tofmzk* sur ce répertoire.
    - Le groupe *staff* partage également certains droits sur ce répertoire.
    - *luc* et *david* sont deux utilisateurs ne faisant pas partie du groupe *staff* dont on a défini les droits sur deux fichiers (la série ne commence pas par un *d*)
- Les droits s'appliquent en fonction de 3 identités :
    - le propriétaire du fichier. Ici *tofmzk*
    - le groupe (qui contient un ou plusieurs utilisateurs). Ici *staff*
    - les autres utilisateurs qui ne sont ni propriétaires, ni présents dans le groupe : _luc_ et _david_
- Les droits sont représentés par 3 séries de 3 lettres sous Linux. Ces lettres peuvent prendre comme valeurs :
    - r : droit en lecture (read)
    - w : droit en écriture (write)
    - x : droit à l'execution (eXecute) pour un fichier ou au parcours pour un répertoire.
    - s (ou S) : s'execute avec les droits du propriétaire.
    - t (ou T) : le fichier est placé dans une zone qui lui permet d'être executé plus rapidement (zone de swap)

Reprenons :

```
~/ls -l
drwxrwsr-x 13 tofmzk staff  4096 sept. 1 13:04 test
-rw-rw-r-- 1 luc luc 0 sept. 100 00:47 test1.txt
-rwxr--r-- 3 david david 5 juin 96:30 test2.txt
```

- En ce qui concerne le répertoire (les deux premières lignes qui commencent par *d* )
    - tofmzk, propriétaire, a tous les droits en lecture/écriture et execution (*rwx*)
    - le groupe *staff* possède les mêmes droits (*rwx*)
    - les autres utilisateurs peuvent seulement lire (*r*) et parcourir (*x*) le repértoire
- En ce qui concerne les fichiers :
    - *luc* est propriétaire de *test1.txt* sur lequel il a tous  les droits. Il a accordé ces mêmes droits aux membres de son groupe mais les autres utilisateurs n'ont qu'un droit en lecture.
    - *david* est propriétaire du fichier _test2.txt_ , a tous les droits sur ce fichier et n'accorde que le droit en lecture à son groupe et aucun droit à tous les autres.

### Modifier les droits

Si vous êtes propriétaire d'un fichier ou d'un répertoire, vous pouvez en modifier les droits. Pour cela on utilise la commande `chmod` (`ch`ange `mod`e).

```
chmod (-R) [u g o a][+ - =][r w x s t] fichier(dossier)
```

 avec :

- L'option *-R* rend la modification récursive (elle s'applique à tous les fichiers du dossier).
- Les options *u*, *g*, *o* et *a* spécifient l'attribution des droits pour le propriétaire (*u* : user), le groupe (*g* : group), les autres utilisateurs (*o* : other) ou tout le monde (*a* all).
- Les symboles +, -, = indiquent l'ajout d'un droit, le retrait et enfin l'égalité.

> Expliquez ce que ferait la commande `chmod a-r mon_fichier_secret`
>
> ```txt
> ```
>
> 



Revenons à notre TP :

> * Créez dans le repertoire `test` un fichier `secret` contenant un texte de votre choix.
> * enlever les droit en écriture de ce fichier.
> * essayez maintenant de le modifier en utilisant la commande `cat`
> * essayer de lire le fichier en utilisant la commande `cat` .
> * enlevez le droit en lecture du fichier puis essayez de le lire en utilisant la commande `cat` .



### Modifier le propriétaire ou le groupe



Par défault lorsqu'un utilisateur crée un fichier ou un dossier, il en  devient propriétaire et fait partie du groupe portant le même nom. Si vous avez les droits sur le fichier, vous pouvez en changer le propriétaire.



Mais avant cela, il faut déjà qu'il y ait d'autres utilisateurs. Pour ajouter un nouvel utilisateur, on utilise la commande `adduser` pour, par exemple, ajouter l'utilisateur _toto_ :

```
~$ sudo adduser toto
```



La commande `sudo` permet d'executer la commande en mode _root_, à condition d'avoir le bon mot de passe.



Pour changer le propriétaire d'un fichier, on utilise la commande `chown` (`ch`ange `own`er)  :

```
chown [-R] [utilisateur][:groupe] cible1 [cible2 ..]
```

- Le paramètre _-R_ modifie tous ses sous-répertoires et ses sous-fichiers d'une manière récursive.
- Le paramètre _utilisateur_ fait référence au nom du nouveau propriétaire des fichiers ciblés.
- Le paramètre optionnel _groupe_ (qui doit être préfixé par un double-point `:`) indique à quel groupe les fichiers ciblés doivent être associés.
- Les paramètres _cible_ font référence aux fichiers ou répertoires pour lesquels utilisateur et/ou groupe seront associés.





> Rendez l'utilisateur _toto_ propriétaire du fichier `a_propos`. 

```
~$ 
```

## Automatiser les tâches



Les commandes SHELL constituent un langages de programmation à part entières.  On y trouve les structures conditionnelles, les boucles ...

On peut créer un programme exécutables dans lequel nos tâches seront automatiséé. C'est pourquoi la plupart des administrateurs d'un réseau utilisent le SHELL plutôt que l'interface graphique.



voici un exemple de script simple :

```BASH
#!/bin/bash 
echo "demarrage du script"
mkdir test_script
cd test_script
mkdir images
mkdir musique
cd images
echo "ici on place nos fichiers images" >> readmecd ..
cd ..
cd musique
echo "ici on place nos fichiers musicaux" >> readme
cd ..
cd .. 
```

> * Installez l'éditeur `nano` en tapant `sudo apt-get install nano`.
> * Éditez `script.sh` en tapant la commande `nano script.sh` et collez-y le code complet ci-dessus. Sauvegardez (ctrl + s) puis quittez (ctrl + x)
> * Rendez le script exécutable en tapant la commande `chmod +x script.sh`
> * Éxécutez le en tapant `sh script.sh`
> * Regardez ce que fait ce script. 



______________

Mieszczak Christophe CC BY SA

_source images : wikipédia domaine public, GNU et CC BY SA_


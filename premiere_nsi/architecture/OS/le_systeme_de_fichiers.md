# Le système de fichiers

 # Van Neumann



Nous avons vu que l'architecture Van Neumann permet à la mémoire de contenir à la fois les données et les programmes. La quantité de données à stocker devient vite importante, trop pour être conserver dans les registres des microprocesseurs ou la mémoire vive (RAM) et la nécessité de conserver ces données dans le temps impose la recherche d'un système de stockage permanent et fiable : cartes perforées, disquettes, disques durs, CD et DVD, clefs USB, cloud ...

![cartes perforée - wikipédia - domaine puplic](https://upload.wikimedia.org/wikipedia/commons/8/87/IBM_card_storage.NARA.jpg)

_Archivage de cartons de cartes perforées archivés au service du [NARA](https://fr.wikipedia.org/wiki/National_Archives_and_Records_Administration) en 1959. Chaque carton peut contenir 2 000 cartes (d'une ligne de 80 colonnes chacune)._ 



![disquettes- wikipédia - CCSA](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Diskettes.jpg/439px-Diskettes.jpg)

_Les disquettes ont régné sur le stockage transportable de 1967 (80 ko par disquette ...) au début des années 2000, date d'entrée sur scène des premières clefs USB._



![ancien dd  -wikipdia GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/IBM_old_hdd_mod.jpg/714px-IBM_old_hdd_mod.jpg)

_Le premier disque dur a fait son apparition en 1956 (commercialisation autour de 1960 au prix de 10000$ le mégaoctet ...) et n'a jamais cessé d'évoluer même si, aujourd'hui, les disques SSD occupent une place grandissante sur le marché et tendent à le détrôner._



![CD wikipédia GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/CD-R_Back.jpg/621px-CD-R_Back.jpg)

_Inventé par Philipps en 1987, le CD-ROM offrait la capacité de 500 disquettes, fonctionnait avec un système optique et ne donnait pas d'accès en écriture, du moins dans un premier temps. Ses successeurs DVD puis Blue-Ray font encore, un peu, de résistance face aux autres systèmes de stockages._



![le cloud wikipedia CC SA](https://upload.wikimedia.org/wikipedia/commons/9/93/Nuage33.png)

_Stocker sur le cloud (le nuage) consiste à utiliser des [serveurs informatiques](https://fr.wikipedia.org/wiki/Serveur_informatique) distants par l'intermédiaire d'un réseau, généralement [Internet](https://fr.wikipedia.org/wiki/Internet_Protocol), pour stocker des données ou les exploiter. La fiabilité est très supérieure aux stockages locaux, permet des accès de différents endroits par différentes personnes. La capacité de stockage est énorme, tout autant que la consommation d'énergie_.





# Qu'y-a-t-il sur un disque dur ? 



Non. Pas des fichiers...

Non plus. Pas des 0 et des 1...



![plateau dd - wikipedia CC SA](https://upload.wikimedia.org/wikipedia/commons/1/1d/Hard_disk_platter_reflection.jpg)



Une unité de stockage utilise un moyen **physique** pour coder des suites de 0 et de 1. Ce peut être fait de façon magnétique ([disque dur](https://fr.wikipedia.org/wiki/Disque_dur)), par un système de piégeage d'électrons ([mémoire flash, clé USB](https://fr.wikipedia.org/wiki/M%C3%A9moire_flash)), en traitant une surface réfléchissante ou non  ([CD, DVD ...](https://fr.wikipedia.org/wiki/DVD)) ... 



Ce n'est pas le système d'exploitation qui traduit directement le codage matériel. Ce dernier est livré avec une interface normalisée qui _traduit_ son système personnel de codage en 0 et 1 au système d'exploitation. L'OS _voit_ donc le support de stockage comme une série de 0 et 1 en passant par cet intermédiaire. 



Il faut toutefois organiser cette série de 0 et 1 afin de savoir à quoi elle correspond. S'agit-il :

* de texte ?
* d'une image ?
* d'un son ?
* du code d'un logiciel ?



Pour cela, l'OS utilise un système de fichiers.



# Système de fichiers



### Les fichiers



[Le système de fichiers](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_fichiers) (en abrégé `FS` pour `File System`) utilisé par un OS (il en existe différents type [FAT](https://fr.wikipedia.org/wiki/File_Allocation_Table#Structure), [NTFS](https://fr.wikipedia.org/wiki/NTFS_(Microsoft)) , [ext](https://fr.wikipedia.org/wiki/Extended_file_system)  ainsi que leurs variantes et évolutions par exemple) lui permet de savoir exactement où est stocké un ensemble de données destiné à coder un objet précis. Cet ensemble de donnée est ce qu'on appelle un _fichier_. Évidemment, le `FS` gère tous les fichiers présent sur l'espace de stockage.



Le fichier possède un nom qui peut aujourd'hui utiliser quasiment l'ensemble des caractères du répertoire Unicode, mais, certains caractères spécifiques ayant un sens pour le système d'exploitation peuvent être interdits ou  déconseillés. Ce peut être le cas des accents, des espaces ou de certains caractères comme `\` ou `/` . Dans les environnements graphique, le type du fichier apparaît sous la forme d'un suffixe comme `.txt` pour un fichier texte ou `.jpg` pour une image au format `jpeg`.



Chaque fichier possède un et un seul `noeud d'index`, aussi appelé `i-node`. Il s'agit d'une structure de donnée qui contient les `métadonnées`  du fichier telles que son `type` (texte, image, son ...) , son emplacement(la position de ses données sur le disque) ,  sa `taille` (en ko), son `propriétaire`,  ses `droits d'accès` ... 

* Le `type` du fichier détermine la façon dont l'OS va interprétée les données du fichier. Par exemple, l'octet _01000001_ peut être interprété comme un 'A' s'il s'agit d'un fichier texte ou comme la première ligne d'une image au format `pbm` (`portable bit map`, un format d'image _bicolore_).

* Le propriétaire est celui qui possède les droits sur le fichier en lecture et en écriture. Il peut également allouer ou retirer ces droits aux autres utilisateurs.

    

Chaque `i-node` possède un numéro qui l'identifie appelé `i-number`. Nous verrons plus loin quel est son intérêt.



L'entassement de tous les fichiers sans organisation entraînerait une grande difficulté à les gérer pour l'utilisateur, un peu comme si les pages de nombreux cahiers étaient toutes rangées sans reliures dans un même tiroir. Il faut donc un système de rangement.



### Les répertoires : point de vue de l'utilisateur



**Pour l'utilisateur**, un système de fichiers est vu comme une  arborescence : les fichiers sont regroupés dans des répertoires, aussi appelés dossiers (concept utilisé par la plupart des systèmes d’exploitation). Ces répertoires  contiennent soit des fichiers, soit récursivement d'autres répertoires.  

![arborescence - wikipédia- domaine public](https://upload.wikimedia.org/wikipedia/commons/d/db/AHEADHierarchy.JPG)





Une telle  organisation génère une hiérarchie de répertoires et de fichiers  organisés en arbre :

* il y a un unique répertoire `racine` (`root` noté `/` sous UNIX), le seul qui n'ai pas de parent mais seulement d’éventuels enfants, ses sous-répertoires.
* Tous les autres répertoires ont un unique parent et, éventuellement plusieurs répertoires fils.
* Chaque répertoire peut _contenir_ des fichiers ou d'autres répertoires.



On peut accéder au fichier par _un chemin d'accès_ :

* Il peut être **absolu** s'il démarre de la racine. Il n'y a qu'un seul chemin absolu puisqu'il n'y a qu'une seule racine.
* Il peut être **relatif** s'il démarre d'un autre répertoire. Il peut y avoir autant de chemins relatifs que de répertoires.



Par exemple le fichier _X.java_ aurait pour chemin d'accès absolu :

* Sous UNIX, le séparateur étant `/`,  `/A/Code/X.java`
* Sous Windows, sur un disque nommé `C:` , le séparateur étant `\`,  `c:\A\Code\X.java`.



Si on se place dans le répertoire `A`, le chemin relatif vers _X.java_ serait :

* Sous UNIX et Linux, le séparateur étant `/`,  `Code/X.java`
* Sous Windows, sur un disque nommé `C:` , le séparateur étant `\`,  `Code\X.java`.



Le père d'un repertoire est atteignable par par le chemin  `..`

> Quel est le chemin relatif vers _X.java_ depuis le répertoire `HTM` ?
>
> ```
> 
> ```







### Les répertoires : point de vue du système de fichier.



**Du point de vue du système de fichiers**, un répertoire est traité comme un fichier dont le contenu est la liste des  fichiers référencés. Un répertoire a donc les mêmes métadonnées qu'un fichier comme le nom, la `taille`, la `date`, les `droits d'accès`, son `i-numbers` et les divers autres attributs. 

En réalité, il n'y a donc pas des fichiers et des répertoires mais un ensemble de fichiers qui ont des propriétés différentes.

Un répertoire est donc en réalité un fichier. Mais un fichier spécial qui contient la liste des `i_numbers` des `i-nodes` des fichiers qu'il référence, dont certains peuvent être des dossiers eux même. Voilà, c'est ici qu'on s'en sert :

Lorsqu'on _ouvre_ un répertoire, on lui demande d'afficher les `i-nodes` des fichiers qu'il références.



Par défaut, on se trouve dans la racine et on voit donc les `i-nodes` des fichiers qu'elle référence.





**La hiérarchie n'existe donc pas réellement mais seulement dans la représentation graphique du système**.



La véritable vue d'un système de fichier est un ensemble de liens (qui peuvent être [physiques](https://fr.wikipedia.org/wiki/Lien_physique) ou [symboliques](https://fr.wikipedia.org/wiki/Lien_symbolique#Notes_et_r%C3%A9f%C3%A9rences) mais je ne détaillerai pas ici) que l'utilisateur peut manipuler mais que la couche graphique de l'OS lui masque en donnant l'illusion d'un empilement hiérarchique.



____________

Mieszczak Christophe

licence CC BY SA

source images : wikipédia domaine public, GNU et CC BY S


# **O**perating **S**ystem

## Introduction 



* OS ? Système d'exploitation ? une petite vidéo d'introduction [ICI](https://youtu.be/4OhUDAtmAUo)

	> * Citez les principaux systèmes d'exploitation
	>
	> ```txt
	> 
	> ```
	>
	> * Quel est le rôle général d'un système d'exploitation ?
	>
	> ``` txt
	> 
	> ```
	>
	> * Quels sont les services rendus par un OS ?
	>
	> ``` txt
	> 
	> ```
	>
	> 



* UNIX : le père de famille. Encore une petite vidéo [ICI](https://www.youtube.com/watch?v=Za6vGTLp-wg)

	> * Le système UNIX est un système dit "propriétaire" (certaines personnes  disent "privateur"), c'est-à-dire un système non libre. Mais plus  généralement, qu'est-ce qu'un logiciel libre ? 		
	>
	> ```` txt
	> 
	> ````





* [Microsoft](https://fr.wikipedia.org/wiki/Microsoft) a été créée par Bill Gates et Paul Allen en 1975. Microsoft  est surtout connue pour son système d'exploitation Windows. Windows est  un système d'exploitation "propriétaire", la première version de Windows date 1983, mais à cette date Windows n'est qu'un ajout sur un autre système  d'exploitation nommé MS-DOS. Aujourd'hui Windows reste le système  d'exploitation le plus utilisé au monde sur les ordinateurs grand public, il faut dire que l'achat de Windows est quasiment  imposé lorsque l'on achète un ordinateur dans le commerce, car oui,  quand vous achetez un ordinateur neuf, une partie de la somme que vous  versez termine dans les poches de Microsoft. Il est possible de se	faire rembourser la licence Windows, mais cette opération est  relativement complexe. 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Windows_logo_-_2012.svg/800px-Windows_logo_-_2012.svg.png" alt="img" style="zoom:33%;" />

* En 1991, un étudiant finlandais, [Linus Torvalds](https://fr.wikipedia.org/wiki/Linus_Torvalds), décide de créer un  clone libre d'UNIX en ne partant de rien (on dit "from scratch" en  anglais) puisque le code source d'UNIX n'est pas public. Ce clone d'UNIX va s'appeler Linux (Linus+UNIX). [Cette vidéo](https://youtu.be/IquNF_DXcF8)  raconte l'histoire de Linux en entrant un peu plus dans les détails.

    ![logo - wikipédia - CC0](https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Tux.png/152px-Tux.png)





* Le système d'exploitation des  ordinateurs de marque Apple sont livrés  avec le système d'exploitation mac OS. Il s'agit un système  d'exploitation UNIX, c'est donc un système d'exploitation propriétaire. Il a été commercialisé pour la première fois en 1998.

	  
	
	​	
	
	![MAC OS - wikipédia - domaine public](https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Mac_OS_wordmark.svg/150px-Mac_OS_wordmark.svg.png)



* [Androïd ](https://fr.wikipedia.org/wiki/Android) et [iOS](https://fr.wikipedia.org/wiki/Apple_iOS)  sont les deux systèmes d'exploitation dominants pour les systèmes mobiles.

	* Androïde est basé sur Linux et développé par Google en 2005 sous licence libre GNU. En 2015, Android est le système d'exploitation mobile le plus utilisé dans le monde, devant i OS, avec plus de 80 % de parts de marché dans les smartphones.

	  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Android_logo_2019.png/599px-Android_logo_2019.png" alt="android" style="zoom:33%;" />

	  

	* i OS dérive de Mac OS et d'UNIX et est donc propriétaire.

	

	<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/IOS_wordmark_%282017%29.svg/512px-IOS_wordmark_%282017%29.svg.png" alt="logo source wikipédia - domaine publique" style="zoom:50%;" />

_______

Mieszczak Christophe CC BY SA

_sources images : Wikipédia domaine public et CC BY SA (auteur [Google](http://www.android.com/branding.html))_

* [tux](https://commons.wikimedia.org/wiki/File:Tux,_gray%EF%BC%8Fgrey_background.png)
* [logo windows](https://commons.wikimedia.org/wiki/File:Windows_logo_-_2012.svg)
* [macOS](https://commons.wikimedia.org/wiki/File:Mac_OS_wordmark.svg)
* [android](https://commons.wikimedia.org/wiki/File:Android_logo_2019.png)
* [ios](https://commons.wikimedia.org/wiki/File:IOS_logo_(2013).png)



 
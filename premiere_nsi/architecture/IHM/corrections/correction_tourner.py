from microbit import *
import random

    
def tourner(delais):
    # on éteint tout
    for x in range(5):
        for y in range(5):
            display.set_pixel(x, y, 0)
    # on va faire 4 tours            
    for _ in range(4) :
        for x in range(5):
            display.set_pixel(x, 0 ,9)
            sleep(delais)
            display.set_pixel(x, 0 ,0)
        for y in range(1, 5):
            display.set_pixel(4, y ,9)
            sleep(delais)
            display.set_pixel(4, y ,0)
        for x in range(1, 5):
            display.set_pixel(4 - x, 4 ,9)
            sleep(delais)
            display.set_pixel(4 - x, 4 ,0)
        for y in range(1, 4):
            display.set_pixel(0, 4 - y ,9)
            sleep(delais)
            display.set_pixel(0, 4 - y ,0)
            


tourner(60)
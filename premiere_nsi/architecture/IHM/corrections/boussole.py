from microbit import *

def renvoyer_cap(temps_en_secondes) :
    '''
    calibre la boussole si necessaire puis, pendant le temps imparti,
    affiche le cap en degré.
    '''
    if not compass.is_calibrated :
        compass.calibrate()

    for _ in range(temps_en_secondes) :
        print(compass.heading())
        sleep(1000)
        
renvoyer_cap(30)

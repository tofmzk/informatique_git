# La carte _micro:bit_



## Présentation



La carte _micro:bit_ est une carte embarquée qui peut, mais ce n'est pas vraiment ce pourquoi elle est conçue,  faire office d'objet connecté dans certain cas. Elle a été conçue dans un objectif pédagogique.

![	](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/BBC_Microbit.jpg/713px-BBC_Microbit.jpg)

Pour avoir les informations complètes sur cette carte, aller [visiter cette page du site web micro:bit](https://archive.microbit.org/fr/guide/features/).



> * Quels sont les `capteurs`de cette carte ?
>
> 	````txt
> 	
> 	````
>
> 	
>
> * Quels sont les `actionneurs` de cette carte ?
>
> 	```` txt
> 	
> 	````
>
> * Par quel(s) moyen(s) cette carte peut-elle devenir un objet connecté et faire partie d'un réseau ?
>
> 	````python
> 	
> 	````
>
> 	 	



## micro:bit et micro-Python

La carte _micro:bit_ se programme en Python, ou plutôt une version de Python dédiée au micro-contrôleurs, le microPython. On peut utiliser un [l'editeur micro:bit](https://python.microbit.org/v/2.0) en ligne comme, un éditeur comme _Thonny_ ou même depuis un téléphone. Nous utiliserons la seconde solution. 



* Branchez votre carte sur un port _usb_.
* Lancer _Thonny_. 
* En suivant le menu _Aide/A propos de Thonny_, vérifiez que vous utilisez une version au moins égale à la 3.2 sinon, mettez votre logiciel à jour avant de continuer.
* Suivez le menu _Outils/Options_ puis sélectionnez la languette _Interpréteur_.
* Dans le menu déroulant, choisissez l'interpréteur _MicroPython (BBC micro:bit)_. Remarquez qu'il faudra penser à repasser sur l'interpréteur par défaut pour exécuter un code _normal_.
* Pour terminer, dans le menu déroulant juste en dessous, sélectionnez l'emplacement de la carte : _embed serial port_.



Thonny est prêt, reste à créer un programme. Sauvegardez le sur la carte _micro:bit_ sous le nom `les_bases.py`.

La première chose à faire est d'importer la bibliothèque `microbit` . Pour simplifier l'écriture du code, exceptionnellement, nous allons importer le module ainsi :

````python
from microbit import *
````



**REMARQUE : SIMULATEUR EN LIGNE**

​	SI vous ne disposez pas d'une carte _micro:bit_, vous pouvez utiliser un simulateur en ligne comme celuis 	que vous trouverez [ICI](https://create.withcode.uk/) :

* Importez le module `microbit`.
* Exécutez le code en cliquant sur la flèche en bas à droite de l'écran.





## Utilisons les actionneurs



### Affichage

Il y a plusieurs façon d'afficher des choses sur les 25 LEDS de la cartes en utilisant l'objet `display`qui possède plusieurs méthodes :

* la méthode `clear`eteint toute les LEDs.

  ````python
  display.clear()
  ````

  

* la méthode `scroll`permet de faire défiler du texte, c'est à dire une chaine de caractères. Testez :

  ````python
  from microbit import *
  
  display.scroll('Hello World !')
      
  
  ````



> Réaliser une fonction `compter_a_rebourg` de paramètre _depart_, la valeur entière initiale du décompte, qui affiche le décompte depuis sa valeur de départ jusqu'à 0.
>
> * N'oubliez pas la Docstring.
>
> * on pourra utiliser la méthode `sleep(temps)` où temps est en milisecondes, qui fait une pause dans l'exécution du code durant le temps demandé.
>
> * La fonction permettant de convertir un entier en chaine de caractères est `str`. Vous pouvez tester dans la console :
>
> 	````python
> 	>>> str(10)
> 	?
> 	````
>
> 	



* la méthode `show`permet d'afficher une image. 

	* Certaines sont déjà définies. Vous en trouverez la liste [ICI](https://microbit-micropython.readthedocs.io/en/latest/tutorials/images.html). Testez :

	  ```` python
	  from microbit import *
	  
	  display.show(Image.HAPPY)
	  
	  
	  ````
	
	* Vous pouvez également définir vos images vous même. Chaque LED pouvant être allumée avec une intensité allant de 0 (éteinte) à 9 (intensité maximale). Voici un joli bateau :
	
  ````python
	  from microbit import *
  
	  def afficher_image():
	      '''
	      affiche une image sur l'écran led
	      '''
	      image =  Image("05050:"
	                     "05050:"
	                     "05050:"
	                     "99999:"
	                     "09990")
	      display.show(image)
	  
	  afficher_image()
	      
	````
	
* En utilisant une liste d'images, on peut même créer des animations. Testez ce code pour couler notre bateau :
	
	```Python
		from microbit import *
		
		def animer_images():
		    '''
		    affiche les images de la liste de façon à créer une animation
		    '''
		    bateau1 = Image( "05050:"
		                     "05050:"
		                     "05050:"
		                     "99999:"
		                     "09990")
		
		    bateau2 = Image( "00000:"
		                     "05050:"
		                     "05050:"
		                     "05050:"
		                     "99999")
		
		    bateau3 = Image( "00000:"
		                     "00000:"
		                     "05050:"
		                     "05050:"
		                     "05050")
		
		    bateau4 = Image( "00000:"
		                     "00000:"
		                     "00000:"
		                     "05050:"
		                     "05050")
		
		    bateau5 = Image( "00000:"
		                     "00000:"
		                     "00000:"
		                     "00000:"
		                     "05050")
		
		    bateau6 = Image( "00000:"
		                     "00000:"
		                     "00000:"
		                     "00000:"
		                     "00000")
		    liste_bateaux = [bateau1, bateau2, bateau3, bateau4, bateau5, bateau6]
		    display.show(liste_bateaux, delay = 200)
		
		animer_images()
	```



> Réaliser une fonction `lancer_de` qui simule le lancé d'un dé équilibré en affichant le résultat sur les 25 LEDs de la carte.
>
> * la première version afficher simplement un nombre entre 1 et 6
>
> * la deuxième version afficher la face du dé correspondant au nombre.
>
> 	

* La méthode `display.set_pixel(x, y, intensite)` permet de fixer, entre 0 (éteinte) et 9 (intensité maximale) la luminosité de la LED de coordonnées (x, y). Les LEDs sont repérées de la façon suivante :

  ![micro:bit - wikipédia CC BY SA auteur : [Ugopedia](https://commons.wikimedia.org/w/index.php?title=User:Ugopedia&action=edit&redlink=1)](https://upload.wikimedia.org/wikipedia/commons/c/cb/Micro_bit_position_des_DEL.png)

  Ainsi, pour faire clignoter dix fois la deuxième LED, le code est le suivant. Testez le et complétez la Docstring.

  ```python
  from microbit import *
  
  def faire_clignoter(nbre_de_fois, delais):
      '''
      
      '''
  	for _ in range(nbre_de_fois) :
  	    display.set_pixel(1, 0, 9)
  	    sleep(delais)
  	    display.set_pixel(1, 0, 0)
  	    sleep(delais)
  
  faire_clignoter(10, 100)
  ```


​	

> 
>
> Réaliser une fonction `tourner`, de paramètre _delais_, un entier représentant le délais entre chaque déplacement en millisecondes, qui allume une LED à la fois de façon à donner l'impression que la lumière allumée fait le tour de notre _écran_.
>
> 



> Réaliser une fonction `deplacement_aleatoire`, de paramètre _delais_, un entier représentant le délais entre chaque déplacement en millisecondes, qui affiche une LED se déplaçant aléatoirement sur l'_écran_.







### Broches de connections

Il y a 25 connecteurs externes sur la tranche du _micro:bit_, que l'on nomme _broches_ ou _pins_ en anglais. on peut programmer des moteurs, des LEDs, un haut-parleur ou tout autre composant électrique à l'aide de ces broches. Il est donc possible d'ajouter l'actionneur de son choix en reliant cet actionneur, avec des pinces crocodiles par exemple, à une broche et à la terre (GND) pour fermer le circuit.

![les broches](https://microbit-micropython.readthedocs.io/en/latest/_images/pin0-gnd.png)



Nous n'allons pas développer cette partie d'avantage ici : pour une documentation complète à ce sujet, [allez-ici](https://archive.microbit.org/fr/guide/hardware/pins/). 



## Utilisons les capteurs



### Boutons

Nous disposons de deux boutons : à gauche le bouton _a_ et, à droite, le bouton _b_ modélisé par deux objets en Python, `button_a`et `button_b`.

Pour tester si on appuie sur un de ces boutons, on utilise la méthode `is_pressed()` qui renvoie _True_ ou _False_.

Testez le code ci-dessous  et complétez sa Docstring.

``` python
from microbit import *

def tester_boutons():
    '''
    
    '''
	while True :
    	if button_a.is_pressed() :
        	display.show(Image.HAPPY)
	    elif button_b.is_pressed():
	        break
	    else :
    	    display.show(Image.SAD)

	display.clear()
    
tester_boutons()
```



> Reprenons notre fonction `lance_de`. 
>
> Modifier le code façon à ce que presser sur le bouton _a_ lance un nouveau dé tandis que presser sur le bouton _b_ vide l'_écran_ et stoppe le programme.
>
> 



> * Réaliser une fonction qui allume la LED de coordonnées (x, 4). 
> * Réaliser une fonction `deplace_a_gauche`de paramètre _x_ qui, si _x_ est strictement supérieur à 0, diminue x de 1 et ne le change pas sinon, puis renvoie _x_ .
> * Réaliser une fonction `deplace_a_droite` de paramètre _x_ qui, si _x_ est strictement inférieur à 5, augmente x de 1 et ne le change pas sinon, puis renvoie _x_ .
> * Faites en sorte qu'une pression sur le bouton _a_ décale, si possible, la LED allumée d'un cran vers la gauche.
> * Faites en sorte qu'une pression sur le bouton _b_ décale, si possible, la LED allumée d'un cran vers la droite.
>
> N'oubliez pas les Docstrings !



### Capteur de lumière

Une LEDs peu fonctionner soit comme affichage, soit comme capteur : en les _inversant_ l'écran va devenir un point d'entrée et un capteur de lumière basique, permettant de détecter la luminosité ambiante. Si on utilise les LEDs comme capteurs, on ne peut s'en servir comme affichage. C'est l'un ou l'autre...



Exécutez plusieurs fois le code ci-dessous en laissant plus ou moins de lumière passer sur l'écran et complétez la Docstring.

```` python
from microbit import *

def renvoyer_luminosite(temps_en_secondes)
	'''
	
	'''
	for _ in range(temps_en_secondes):
	    print(display.read_light_level())
        sleep(1000)
        
renvoyer_luminosite(10)
````



> Réalisez une fonction `alarme_lumiere`, sans paramètre, qui mesure l'intensité lumineuse perçue par les LEDS et affiche sur l'écran le message `Alerte !!` (trois fois de suite) si cette intensité dépasse 2.
>
> N'oubliez pas la Docstring !



### Capteur de température

Ce capteur permet à la carte de mesurer la température actuelle de l’appareil, en degrés Celsius.

Testez :

```` Python
>>> from microbit import *
>>> temperature()
???
````



> Réaliser une fonction `afficher_temperature`, de paramètre _temps_en_secondes_, un entier positif, qui affiche la température sur l'écran pendant une durée _temps_en_secondes_. 
>
> N'oubliez pas la Docstring.



### Accéléromètre

La carte _micro:bit_ possède un accéléromètre qui mesure les déplacement selon 3 axes :

- _X_ de gauche à droite
- _Y_ d'avant en arrière
- _Z_ de haut en bas

Les méthode `get_x`, `get_y` et  `get_z` de `accelerometre` permettent de récupérer ces valeurs. Testez les trois fonctions ci-dessous puis complétez les Docstring.

````python
from microbit import *

def tester_x():
    '''

    '''
    while True :
        reading = accelerometer.get_x()
        if reading > 20:
            display.show("D")
        elif reading < -20:
            display.show("G")
        else:
            display.show("-")


def tester_y():
    '''

    '''
    while True :
        reading = accelerometer.get_y()
        if reading > 20:
            display.show("a") #arriere
        elif reading < -20:
            display.show("A") #avant
        else:
            display.show("-")

        
def tester_z():
    '''

    '''
    while True :
        reading = accelerometer.get_z()
        if reading > 20:
            display.show("H")
        elif reading < -20:
            display.show("B")
        else:
            display.show("-")

        
tester_x()
````







### Boussole

La boussole détecte le champ magnétique de la Terre, te  permettant de savoir quelle direction la carte indique. 

* La boussole  doit être étalonnée avant de pouvoir être utilisée. Cela se fait par un petit _jeu_ ou vous devez pencher et tourner la carte dans toutes les directions pour allumer toutes les LEDs de l'écran. Testons un peu cela grâce au programme ci-dessous :

	````python
	>>> compass.calibrate()
	````

* On peut savoir si le compas est bien calibré en utilisation la méthode `is_calibrated()` :

	````python
	>>> compass.is_calibrated()
	???
	````

	

* Nous pouvons maintenant récupérer le _cap_ grâce à la méthode `headling()`de `compass` qui renvoie un entier entre 0 et 359 : 

	* 0 pour une direction vers le Nord

	* 90 pour une direction vers l'Est

	* 180 pour le Sud

	* 270 pour l'Ouest

		

		![rose des vents - wikipédia CC BY SA, auteur : Denelson](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Compass_Card_B%2BW.svg/600px-Compass_Card_B%2BW.svg.png)

	````python
	def renvoyer_cap(temps_en_secondes) :
	    '''
	    calibre la boussole si necessaire puis, pendant le temps imparti,
	    affiche le cap en degré.
		: param temps_en_secondes
		type int
	    '''
	    if not compass.is_calibrated :
	        compass.calibrate()
	    for _ in range(temps_en_secondes) :
	        print(compass.heading())
	        sleep(1000)
	        
	renvoyer_cap(30)
	````



> Réaliser une fonction `indiquer_Nord` de paramètre _temps_en_secondes_, un entier représentant le temps de fonctionnement de notre fonction, qui dessine sur l'écran de la carte une série de 3points démarrant au centre de l'écran et indiquant la direction du Nord à 45° près.
>
> N'oubliez pas la Doctest !
>
> 



### Les broches



Nous avons vu que l'on pouvait utiliser les broches en sortie, comme actionneurs donc, vers un haut-parleur par exemple. Mais on peut également les utiliser en entrée avec un capteur supplémentaire par exemple.



Cette possibilité démultiplie les posibilités de la carte. Cependant, l'objectif n'est pas ici d'exploiter la micro:bit au maximum et nous ne nous étendrons pas là dessus. 



Vous pouvez toutefois vous documentez pour aller un peu plus loin en suivant [ce lien](https://archive.microbit.org/fr/guide/hardware/pins/)



___________

Par Christophe Mieszczak Licence CC BY SA

Images source : wikipédia , CC BY SA,  auteurs par ordre d'apparition : [Ugopedia](https://commons.wikimedia.org/w/index.php?title=User:Ugopedia&action=edit&redlink=1),  [Ravi Kotecha](https://commons.wikimedia.org/wiki/User:Ravi_tt22), [Denelson](https://commons.wikimedia.org/wiki/User:Denelson83)




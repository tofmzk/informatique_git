# GET et POST



## Envoi d'un formulaire à un serveur



La balise `<form>` possède deux attributs importants: 

* _action_ permet de préciser l'URI (Uniform Ressource Identifier) désignant où envoyer les données du formulaire, par le biais de quel programme.

* _method_ permet de définir la méthode HTTP utilisée pour faire transiter les données du client au serveur. 

    

On distingue ainsi deux méthodes:  

* GET : les données sont simplement ajoutées à l'attribut action après un caractère ? . L' URL ainsi constituée est
    envoyée par la barre d'adresse. Les données ne peuvent être constituées que de caractères ASCII et sont visibles
    dans la barre d'adresse et la taille est limitée.
* POST : les données du formulaire ne sont plus liées à l' URL . La taille des données n'est pas limitée.
    La méthode POST est donc plus sûre que la méthode GET mais ne saurait remplacer le protocole HTTPS .



Une fois arrivées sur le serveur, les données sont traitées par un des programmes écrits dans un langage exécutés **côté serveur**, c'est à dire sur le serveur. Un langage couramment répandu est le **PHP** .



## Serveur virtuel

Nous allons mettre en place un serveur virtuel local avec le programme `serveur.py` placé dans le repertoire `get_post` .

* Lorsqu'on éxécute ce programme, l'ordinateur devient serveur avec lequel on communique, via un **port**.

* Quand il reçoit un signal, une page web s'ouvre dont le contenu est modifiable dans le code Python. 



_Remarque à propos des ports:_

De nombreux programmes TCP/IP  peuvent être exécutés simultanément sur Internet. Chacun de ces programmes travaille avec un protocole prècis. Toutefois l'ordinateur doit pouvoir distinguer les différentes sources de données.   

Ainsi, pour faciliter cette identification, chacune des processus se  voit attribuer une adresse unique sur la machine, codée sur 16 bits: **un port**.    

L'adresse IPsert donc à identifier de façon unique un  ordinateur sur le réseau  tandis que le numéro de port indique l'application à laquelle  les  données sont destinées. De cette manière, lorsque l'ordinateur reçoit  des informations  destinées à un port, les données sont envoyées vers  l'application correspondante





> * Chargez le programme sous Thonny 
>
> * Régardez le code et essayez de le comprendre  un peu :
>
>     * Quel est le module utilisé ?
>
>         ```txt
>         le module socket
>         ```
>
>     * Quelle variable de quel type contient la réponse du serveur ?
>
>         ```txt
>         c'est une chaine de caractère encodée en Byte, c'est à dire encodée en série de 0 et 1.
>         ```
>
>     * Quelles lignes doit-on modifier pour changer la page web que le serveur envoie en réponse ?
>
>         ```txt
>         
>             # on lit la page HTML reponse du serveur
>         page_HTML = ("HTTP/1.1 200 Ok host : mon site local Content-Type: text/html\n\n " +
>                        lire_HTML('reponse.html')
>                    )
>         ```
>     
>     * Quel est le numéro du port utilisé ?
>
>         ```txt
>         13450
>         ```
>     
>         



* Nous avons besoin de connaitre l'adresse IP du serveur, c'est à dire celui de notre ordinateur.



>  Pour trouver votre IP :
>
> * Sous windows, dans la ligne de commande, utilisez la commande `ipconfig`.
> * Sous Linux, en ligne de commande, utilisez la commande `ifconfig`.
>
> Notez cette adresse !!



* Exécutez le programme serveur.py avec Thonny. Il se passe rien sauf l'affichage habituel dans la console du message  _%Run serveur.py_  mais le serveur est en service.

    

>  
>
>  Dans la barre d'adresse du navigateur saisir `http:// ` suivi de votre adresse IP puis du port `:13450` .
>
> Que se passe-t-il ?
>
> ```txt
> 
> ```
>
> 
>
> 



## Première méthode

* Avec un editeur de texte (notepad++, sublim text, geany ...) ouvrez le code du fichier `exemple1.html` du repertoire `get_post_fichiers` :

    ```HTML
    <!DOCTYPE html>
    <html lang="fr" >
        <head>
            <meta charset = "utf-8"/>
            <title> exemple5</title>
        </head>
    
        <body>
            <p>  Identifiez-vous </p>
             <form action ="http://192.168.0.12:13450" method = "GET">
              Prénom: <input type="text" name = "premom"  value = "votre prénom" onFocus = "this.value = ''"><br/>
              Mot de passe <input type = "password" name = "motdepasse" value = "mdp" onFocus = "this.value = ''">
              <input  type="submit" value = "Valider" >
            </form>
        </body>
    
    </html>
    
    ```

_Remarques :_

* La zone de texte de type _passaword_ masque ce que l'utilisateur tape.
* Le bouton de type _submit_ est un bouton particulier : lorsqu'on clique dessus les données du formulaire sont envoyées. 



> * Quelle est la méthode utilisée ?
>
>     ```txt
>     méthode GET
>     ```
>
> * A quelle destination les données sont-elles envoyées ?
>
>     ```txt
>     Au serveur 192.168.0.31 via le port 13450. 
>     ```
>
> * Modifier la destination pour qu'elle devienne votre serveur.
>
>     ```HTML
>       <form action ="http://192.168.0.31:13450" method = "GET">
>     ```
>
>     





* Ouvrez maintenant `exemple1.html` avec Firefox. N'oubliez pas, le serveur doit être actif !

    

> 
>
> * Remplissez les formulaires puis soumettez vos données.
>
> * Que renvoie la console python ?
>
>     ```python
>     la chaine de caractère contenant la méthode GET puis la réponse du serveur également au format texte.
>     ```
>
> * La méthode GET est-elle sécurisée ? Pourquoi ?
>
>     ```txt
>     non. Les données des formulaires apparaissent en clair dans la barre de navigation. Elles sont facilement récupérables dans les données envoyées par la méthode GET et visible dans l'onglet "réseau" du debogeur du navigateur.
>     ```
>
>     



_Remarques :_ 



* il est possible d'observer les données envoyées par la barre d'adresse en utilisant le mode développeur du
    navigateur. Pour cela (sur Firefox par exemple):
    * Ouvrir `exemple1.html` avec méthode GET 
    * Presser F12.
    * Choisir le menu réseau.
    * Envoyer le formulaire rempli et observer...



* Il existe des logiciels, comme le célèbre [Wireshark](https://www.wireshark.org/) capable d'écouter des données envoyées par le protocole TCP/IP. Si ces dernières ne sont pas sécurisées et cryptées, on peut les retrouver en clair.

    

    ![wiresharck - perso](media/wireshark.jpg)







## Deuxième méthode



* Avec un éditeur de texte (notepad++, sublim text, geany ...) ouvrez le code du fichier `exemple2.html` du répertoire `get_post_fichiers` :

    ```HTML
    <!DOCTYPE html>
    <html lang="fr" >
        <head>
            <meta charset = "utf-8"/>
            <title> exemple 2</title>
        </head>
    
        <body>
            <p> Identifiez-vous</p>
             <form action ="http://192.168.0.12:13450" method = "POST">
              Prénom: <input type="text" name = "prenom"  value = "prenom" onFocus = "this.value = ''"><br>
              Mot de passe <input type = "password" name = "motdepasse" value = "mdp" onFocus = "this.value = ''">
              <input  type="submit" value = "Valider" >
            </form>
        </body>
    
    </html>
    
    ```



> * Quelle est la méthode utilisée ?
>
>     ```txt
>     POST
>     ```
>
> * A quelle destination les données sont-elles envoyées ?
>
>     ```txt
>     Au serveur 192.168.0.31 via le port 13450. 
>     ```
>
> * Modifier la destination pour qu'elle devienne votre serveur.
>
>     ```HTML
>       <form action ="http://192.168.0.31:13450" method = "POST">
>     ```
>
>     





* Ouvrez maintenant `exemple2.html` avec Firefox. N'oubliez pas, le serveur doit être actif !

  ​    

> 
>
> * Remplissez les formulaires puis soumettez vos données.
>
> * Que renvoie la console python ?
>
>     ```python
>     POST / HTTP/1.1
>     Host: 192.168.0.31:13450
>     User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0
>     Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
>     Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
>     Accept-Encoding: gzip, deflate
>     Content-Type: application/x-www-form-urlencoded
>     Content-Length: 44
>     DNT: 1
>     Connection: keep-alive
>     Upgrade-Insecure-Requests: 1
>     
>     prenom=Christophe&motdepasse=sdk%3Ajcb+bazc+
>     ```
>
> * Quelle différence avec la méthode GET  ?
>
>     ```txt
>     les données du formulaires n'apparaissent ni dans la barre de navigation, ni dans la partie réseau du débogueur et seulement dans la dernière ligne de la requête reçue par le serveur
>     
> ```
>     
>     



_Remarques :_ 



* il est toujours possible d'observer les données envoyées par la barre d'adresse en utilisant le mode développeur du navigateur. Pour cela (sur Firefox par exemple):
    * Ouvrir `exemple2.html` avec methode POST.
    * Presser F12.
    * Choisir le menu réseau.
    * Envoyer le formulaire rempli et observer...



## Conclusion

* La méthode `GET` n'est absolument pas sécurisée. Les données envoyées sont directement lisibles dans la barre d'adresse. Cette méthode n'est utilisée que pour passer des données non confidentielles d'une page web à une autre de façon simple, par exemple pour récupérer un score réalisé dans un jeu sur une page web et affiché dans une autre page.

    

* La méthode `POST` est plus sécurisée car les données n'apparaissent pas dans l'url. Cependant, elles ne sont pas cryptées pour autant. Il faudrait pour cela utiliser le protocole`HTTPS`.





___________

Par Christophe Mieszczak

Licence CC BY SA










 # Mémo HTML et CSS

## Structure d'une page HTML



Voici la  _structure_ classique d'une page HTML :

```HTML

<!doctype html>
<HTML>
	<HEAD>
		<meta charset="utf-8" />
        <TITLE>
			<!-- ici on place le titre qui apparait dans la languette de la page web -->
            
		</TITLE>
        <!-- lien vers la page de style nommee ici style.css -->
		<link href="style.css" rel="stylesheet" type="text/css"  media="screen"/>  
	</HEAD>
	<BODY>
		<!-- corps de la page -->
        
	</BODY>

</HTML>

	
```

* La première balise, `<!DOCTYPE html>`, précise le type du document.
* Tout le code HTML doit être placé entre les balises `<HTML>` et `</HTML>`
* les balises `<TITLE>` permettent de stipuler le titre visible dans la languette de la page.
* Les balises `<HEAD>` servent à indiquer les caractéristiques de la page :
    * le type de codage de caractères. C'est la _méthode_ utilisé pour encoder les caractères. Elle dépend, en gros, de l'alphabet utilisé.  l'UTF_8 est le codage le plus répandu au monde.
    * les liens vers d'autres pages, comme la page _style.css_ 
* les balise `<BODY>` délimite la zone de code dans laquelle on mettra ce que l'internaute verra. C'est entre ces balises que l'on va travailler.



_Remarque :_ 

Notez les tabulations, c'est à dire les décalages : le contenu d'une paire de balises doit être tabulé d'un cran vers la droite par rapport à ces balises. Ces tabulations sont INDISPENSABLES à une bonne lecture du code.





## Quelques balises HTML

Nous allons commencer par placer les balises nécessaires  entre les balises <BODY>. Nous nous occuperons du style (couleur, arrière plan, type de police de caractères ...) seulement ensuite.



* Les balises de titre :

    ```HTML
    <H1>
    	Titre de niveau 1 (gros titre)
    </H1>
    <H2>
    	Titre de niveau 2 (plus petit)
    </H2>
    ```

* La balise _ligne de séparation_ :

    ```HTML
    <HR/>
    ```

* Les balises _paragraphe_ qui ne contiennent que du texte :
	```HTML
    <P>
    	ici j'écris mon texte.
    	<!-- la balise <br/> sert à aller à la ligne dans un paragraphe. -->
    </P>
	```

* La balise image, notez qu'elle est seule : on dit qu'elle est _orpheline_ :

    ```html
    <IMG src="url de l'image" title="commentaire"/>
    ```

    

* Par défaut, les tableaux n'ont pas de bordure. On en ajoutera dans le CSS. Pour les voir, il faut, pour l'instant, utiliser la console. Voici leurs balises :

    ```HTML
    <TABLE> <!-- début du tableau -->
    ²	<TR> <!-- ouverture de la première ligne du tableau -->
    		<TD> 
    			cellule 1 ligne 1
    		</TD>
    		<TD>
    			cellule 2 ligne 1
    		</TD>
    	</TR> <!-- fin de la première ligne du tableau -->
    	<TR> <!-- ouverture de la deuxième ligne du tableau -->
    		<TD> 
    			cellule 1 ligne 2
    		</TD>
    		<TD>
    			cellule 2 ligne 2
    		</TD>
    	</TR> <!-- fin de la deuxième ligne du tableau -->
    </TABLE>	<!-- fin du tableau -->
    ```

* Les balises de liste ( les puces) :

    ```HTML
    <UL> <!-- série de puces non numérotées-->
    	<LI>
    		première puce
    	</LI>
    	<LI>
    		deuxième puce
    	</LI>
    </UL>
    
    <OL> <!-- série de puces numérotées-->
    	<LI>
    		première puce
    	</LI>
    	<LI>
    		deuxième puce
    	</LI>
    </OL>
    
    
    ```

* La balise de lien hypertexte :

    ```HTML
    <A href='url du lien'> description du lien </A>
    ```

    

    > **En avant et amusez vous bien !!**
    >
    > **N'oubliez pas de sauvegarder de temps en temps, cela n'est pas automatique**

    

## Un peu de style : le CSS



Séparer le fond (en HTML) de la forme (en CSS) permet de modifier facilement l'aspect d'une page HTML sans être obligé de la réécrire. Seule la page CSS sera modifiée.



* On peut repérer les objets HTML dans la page css de plusieurs façon :

    * par leur balise. Toute les balises identiques seront impactée.

        ```CSS
        H1 {
        	/*ici on écrira le code du style des balises <H1>*/
        }
        ```

        

    * par un identifiant ajoutée dans la balise en HTML. Seule cette balise seraz concernée, deux balises ne pouvant partager un même identifiant.

        HTML : ajout de l'identifiant _id_

        ```HTML
        <H2 id = "titre_particulier">
        	Ceci est un titre très particulier
        </H2>
        ```

        CSS : on récupère l'identifiant en le nommant après un #

        ```CSS
        #titre_particulier{
        	/*ici on écrira le code du style de la balise qui a cet identifiant*/
        	
        }
        ```

        

    * par une classe ajoutée dans des balises en HTML. Plusieurs balises peuvent partager la même classe. Les balises de même classe seront toutes impactées par le style :

        HTML : ajout des classes _class_

        ```HTML
        <OL>
        	<LI class = "mes_puces">
                puce 1
            </LI>
            <LI class = "mes_puces">
                puce 2
            </LI>
                
        </OL>
        ```

        CSS : on récupère les classes en les nommant après un .

        ```CSS
        .mes_puces{
        	/*ici on écrira le code du style de toutes les balises qui partagent cette classe*/
        	
        }
        ```

* il faut maintenant connaitre quelques fonctionnalités du CSS.

    * En ce qui concerne les couleurs :

        ```css
        color : red ; /* met la couleur en rouge */
        background-color : rgb(230, 230, 230); /*met l'arrière plan dans la couleur définie en rgb*/
        
        ```

    * En ce qui concerne le texte :

        ```CSS
        font-family : tahoma; /* choisit la police tahoma */
        font-size : 18px; /* fixe la taille de la police à 18 pixels*/
        font-weight : bold; /* met le texte en gras */
        font-style : italic; /* met le texte en italic*/
        text-align : center; /* centre le texte dans son conteneur*/
        
        ```

        

    * En ce qui concerne les dimensions, quand l'objet en a :

        ```CSS
        width : 300px; /* fixe la largeur à 300 pixels*/
        height : 350px; /* fixe la hauteur à 350 pixels*/
        
        ```

        

    * En ce qui concerne les bordures :

        ```CSS
        border : 2px solid black; /* une bordure de 2px en trait continue noir*/
        border-radius : 20px; /* arrondi les angles sur 20px*/
        box-shadow : 10px 10px 10px black; /* ajoute une ombre -> effet 3D */
        ```
        
        
        
    * En ce qui concerne les marges extérieures :
    
    ``` css
        margin-left  : 0px; /* une marge de 0px à gauche, pas de marge donc*/
    	margin-right  : 10px; /* une marge de 10px à droite*/
        margin-top  : 0px; /* une marge de 0px en haut, pas de marge donc*/
        margin-bottom : 10px; /* une marge de 10px en bas*/
    ```
    
    * En ce qui concerne les marges intérieures :
    
    ```css
        padding-left  : 10px; /* une marge de 10px à gauche*/
    	padding-right  : 10px; /* une marge de 10px à droite*/
        padding-top  : 10px; /* une marge de 10px en haut*/
        padding-bottom : 10px; /* unemarge de 10px en bas*/
    ```



Par Mieszczak Christophe - Licence CC - BY - SA
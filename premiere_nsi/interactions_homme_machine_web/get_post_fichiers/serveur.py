#!/usr/bin/python3
# -*- coding: utf-8 -*-

from socket import socket, AF_INET, SOCK_STREAM

PORT = 13450
SOCKET = socket(AF_INET, SOCK_STREAM)

def lire_HTML(nom_fichier):
    '''
    renvoie le contenue du fichier HTML de nom passé en paramètre au format str
    : param nom_fichier
    type str
    : return
    type str
    '''
    assert type(nom_fichier) is str,'nom_fichier HTML is str'
    try :
        lecture = open(nom_fichier, 'r', encoding='utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le fichier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    lecture.close()
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n")# enleve les "\n" et convertit en list
    chaine_HTML = ''
    for ligne in toutes_les_lignes :
        chaine_HTML += ligne
    return chaine_HTML
    
def go():
    '''
    met le serveur en marche
    '''
    initialise_connexion()

def initialise_connexion() :
    '''
    ouvre le PORT sur lequel le serveur écoute si celui-ci est disponible
    '''
    # on ouvre le port PORT
    try :
        SOCKET.bind(("", PORT))
        SOCKET.listen(5)
        demarrer_serveur()
    except :
       return 'le socket est encore utilisé, attendez un petit moment svp'
 


def demarrer_serveur() :
    initialise_connexion()
    while True:
        requete = ecouter()

    print("FIN !")
    SOCKET.close()
    
def ecouter() :
    connexion, addresse = SOCKET.accept()
    requete = connexion.recv(1024).decode()
    traiter_requete(connexion, requete)
    connexion.close()
  

def traiter_requete(connexion, requete):
    if requete != '' :
        if requete[:3] == 'GET' :
            traiter_get(connexion, requete)
        elif requete[:4] == 'POST' :
            traiter_post(connexion, requete)

    
def traiter_get(connexion, requete):
    print('Requête GET reçue...')
    print(requete)
    prenom, mdp = valeurs_get(requete)
    envoyer_reponse(connexion, prenom, mdp)
        

    
def traiter_post(connexion, requete):
    print('Requête POST reçue...')
    print(requete)
    prenom, mdp = valeurs_post(requete)
    envoyer_reponse(connexion, prenom, mdp)   
        
     
    
def valeurs_get(requete):
    requete_lignes = requete.split('/n')
    requete_ligne_1 = requete_lignes[0].split('?')[1].split('HTTP')[0]
    prenom = requete_ligne_1.split('=')[1].split('&')[0]
    mdp = requete_ligne_1.split('=')[2]
    return prenom, mdp
    
def valeurs_post(requete):
     requete_lignes = requete.split('\r')
     requete_derniere_ligne = requete_lignes[len(requete_lignes) - 1]
     prenom = requete_derniere_ligne.split('=')[1].split('&')[0]
     mdp = requete_derniere_ligne.split('=')[2]
     return prenom, mdp
    
def envoyer_reponse(connexion, prenom, mdp):
    # on lit lapage HTML reponse du serveur
    page_HTML = ("HTTP/1.1 200 Ok host : mon site local Content-Type: text/html\n\n " +
               lire_HTML('reponse.html')
               )
    # on insère les données à la page
    reponse_split = page_HTML.split('<p>')
    reponse = ( reponse_split[0] +
                '<p> Bienvue ' + prenom + ' ! </p>' +
                '<p> Votre mot de passe est ' + mdp +
                reponse_split[1]
                )
    print('réponse : ')
    print(reponse)
        
    connexion.send(reponse.encode())    
    
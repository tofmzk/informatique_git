 # Client, serveurs et protocole HTTP

# Le modèle client serveur 	



Bill Porte demande à son ami Steve Boulot :

-" Steve, peux tu me ramener l'enveloppe rouge qui est dans la salle B101, dans mon bureau dans le troisième tiroir s'il te plait ?"

-"Bien sûr Bill", répond Steve, "je vais le chercher."

"Le voilà Steve"

-"Merci Bill"





Bill et Steve viennent de créer un modèle _client/serveur_ sous vos yeux ébahis :

* Bill et Steve parle la même langue. Sinon, il auraient bien du mal à se comprendre.
* Bill, _le client_, adresse un _requête_ à Steve : aller lui chercher une ressource dont il a besoin.
* Steve, _le serveur_, est prêt à l'aider mais il a besoin de savoir où est localisée la dite ressource, il n'est pas médium.  Heureusement, Bill lui dit tout ce dont il a besoin :
	* Le _site_ où chercher est la salle B101
	* Le _chemin d'accès_ pour arriver à la ressource, à partir du site de la salle B101,  est _\dans le bureau\dans le troisième tiroir_. C'est un chemin _absolu_ car précisé à partir du site.
	* Il pourrait y avoir plusieurs choses dans ce tiroir, mais Steve a précisé qu'il voulait l'enveloppe rouge : c'est la ressource dont il a besoin !
* Si le document n'est pas à l'endroit précisé, Bill ne pourra que lui dire qu'il ne le trouve pas. Sinon, il pourra le lui ramener. 







Bill, qui est un sacré casse pied en plus d'un grand fainéant, demande à nouveau à Steve :

-" J'aurai besoin du crayon bleu qui est dans le tiroir du dessus"

* Cette fois le chemin est _relatif_ car donné à partir du précédent emplacement et pas à partir du site de la salle B101. Steve devait cependant réussir à le trouver.






>  Sur internet, ce modèle **client/serveur** domine assez largement, même  s'il existe des cas où un ordinateur pourra jouer tour à tour le rôle de  client et le rôle de serveur (exemple le [peer to peer](https://www.journaldunet.fr/web-tech/dictionnaire-du-webmastering/1203399-p2p-peer-to-peer-definition-traduction-et-acteurs/)) très souvent, des ordinateurs (les clients) passeront leur  temps à demander des ressources à d'autres ordinateurs (les serveurs). 

![client - serveur wikipédia CCA](https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Mod%C3%A8le-client-serveur.svg/1200px-Mod%C3%A8le-client-serveur.svg.png)



N'importe quel type d'ordinateur peut jouer le rôle de serveur, mais  dans le monde professionnel les serveurs sont des machines spécialisées  conçues pour fonctionner 24h sur 24h de la façon la plus fiable possible. Ils peuvent aussi avoir une  grosse capacité de stockage afin de stocker un grand nombre de  ressources (vidéos, sons, ...) , utiliser des systèmes de sauvegardes pour éviter les pertes tragiques de données et des moyens de protections très strictes.



​	

 ![media center - wikipedia - CCA](https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/HP_MediaSmart_Server_EX490_006.jpg/800px-HP_MediaSmart_Server_EX490_006.jpg)



Environ 6500 clients se connectent sur Google chaque seconde. Il serait impossible de gérer cela avec un seul serveur. Imaginez un restaurant où 6500 clients commandent leur repas en même temps au même serveur....

Google, Amazon ou encore Facebook possèdent un très grand nombre de  serveurs afin de pouvoir satisfaire les demandes des utilisateurs en  permanence. Ces entreprises possèdent d'immenses salles contenant  chacune des centaines ou des milliers de serveurs selon les besoins de l'entreprise qui les possèdent.

 				

![data center - wikipeda CC BY SA auteur[Hugovanmeijeren](https://commons.wikimedia.org/w/index.php?title=User:Hugovanmeijeren&action=edit&redlink=1)](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Cern_datacenter.jpg/300px-Cern_datacenter.jpg)

 				

Souvent les serveurs sont spécialisés dans certaines tâches, par  exemple, les serveurs qui envoient aux clients des pages au format HTML  sont appelés _serveur web_. 

Il y a quelques années, le web était dit « statique » : le  concepteur de site web écrivait son code HTML et ce code était  simplement envoyé par le serveur web au client. Les personnes qui  consultaient le site avaient toutes le droit à la même page, le web  était purement « consultatif ».

Les choses ont ensuite évolué : les serveurs sont aujourd'hui  capables de générer eux-mêmes du code HTML en utilisant des langages de programmation qui s'éxecute sur le serveur avant qu'il ne vous envoie les données (le PHP par exemple). Les résultats qui  s'afficheront à l'écran dépendront donc des demandes effectuées par  l'utilisateur du site : le web est devenu dynamique : Selon l'endroit où vous habitez, votre âge, votre profil [...] lapage affichée sera différente.

Différents langages de programmation peuvent être utilisés « côté  serveur » afin de permettre au serveur de générer lui-même le code HTML à  envoyer. Le plus utilisé encore aujourd'hui se nomme PHP. D'autres  langages sont utilisables côté serveur (pour permettre la génération  dynamique de code HTML) : Python...





# HTTP et URL 

Pour surfer sur le web, un internaute utilise la plupart du temps un navigateur Internet (Firefox, Chrome, Edge, Opéra, Safari ...).



 En fonction de ses clicks ou de ses saisies, le navigateur va envoyer une demande au sujet de pages stockées sur des serveurs afin d'obtenir des renseignements à leurs sujets, ou de les afficher, ou de les récupérer : on dit qu'il fait une requête au serveur.



De son côté, un  serveur attend les requêtes envoyés par les internautes. Lorsqu'il en reçoit une, il se contente de  répondre à la demande en envoyant du contenu. 



En général, le surf commence par **une adresse**, soit saisie manuellement dans la barre d'adresse, soit obtenue en cliquant sur un lien, soit obtenu suite à une requête dans un moteur de recherche  (qui d’ailleurs est lui aussi lancé par une adresse comme par exemple le célèbre http://www.google.fr ou [https://duckduckgo.com/]([https://duckduckgo.com/) qui promet de ne pas utiliser nos données),  il vous faut toujours une adresse pour aller quelque part. Pour  atteindre un _serveur http_, c’est pareil : on utilise une adresse. 



Une adresse est toujours constituée de la façon suivante : 

**protocole://adresse-du-serveur:port/chemin/ressource** 

Cette adresse est appelée **URL**  (**U**niform **R**esource **L**ocator, système universel de localisation des ressources) :



_Exemple :_

[http://www1.ac-lille.fr/cid93943/le-lycee-leonard-de-vinci-de-calais-s-interroge-sur-le-climat.html](http://www1.ac-lille.fr/cid93943/le-lycee-leonard-de-vinci-de-calais-s-interroge-sur-le-climat.html)



* L'**adresse du serveur** est ... l'adresse où se situe le serveur. Dans notre exemple, c'est _www1.ac-lille.fr_, le serveur de l'académie de Lille. Cela correspond à la salle B101 pour Bill et Steve.
* le **chemin** est l'endroit sur le serveur où est placée la ressource désirée. Il peut être :
    * **absolue** c'est à dire qu'on le donne à partir de la _racine_ , c'est à dire la position de la page de départ, l'index, du site. Dans notre exemple, la ressource est située dans le dossier _cid93943_ situé directement sous la racine à l'image du bureau et du tiroir dont on a parlé plus haut.
    * **Relatif** c'est à dire à partir d'un emplacement donné ailleurs que dans la _racine_, comme dans le tiroir du dessus dans l'exemple de la première partie.
* Et pour terminer le nom de la ressource elle-même, sinon le serveur ne sait pas ce que vous voulez. Ici le client demande de recevoir la page _HTML_ nommée _le-lycee-leonard-de-vinci-de-calais-s-interroge-sur-le-climat.html_

* Si l'adresse n'est pas correcte, le serveur ne trouvera pas la ressource et viendra s'excuser platement... alors qu'il n'y peut rien... Il ne sous-entend même pas que vous vous êtes trompé d'adresse. Quel gentleman ce serveur.



<img src="media\Hum.jpg" alt="hum..." style="zoom:50%;" />





* Pour que Bill et Steve se comprennent, ils doivent parler la même langue. En informatique, c’est la même chose : il faut que navigateur et serveur s'exprime de la même manière. Cette langue est appelée  protocole et le protocole utilisé par navigateur et serveur pour communiquer, c'est le **protocole HTTP** :
	* **HTTP** signifie **H**ypertext **T**ransfert **P**rotocol", un nom des plus _parlant_.
	* Comme vu dans notre introduction, il a été inventé par Tim-Berner Lee dans les années 1990. La version 1.0 du protocole (la plus  utilisée) permet désormais de transférer des messages avec des en-têtes  décrivant le contenu du message en utilisant un codage spécifique (de type [MIME](https://www.commentcamarche.net/contents/175-standard-mime-multipurpose-internet-mail-extensions) pour les curieux, c'est grâce à lui que vous pouvez joindre des fichiers à vos courriers électroniques).  
	* Le but du protocole HTTP est de permettre un transfert de  fichiers (essentiellement au format HTML) localisés grâce à une chaîne de caractères appelée URL entre un navigateur (le client) et un serveur  Web.
	* Ce protocole est un protocole de communication client-serveur et fonctionne sur le principe "requête-réponse".

​	

Toutefois, ils sont robustes les serveurs : ils essaient d'afficher quelques choses même en cas d'erreur. Ils interpréteront correctement certaines adresses erronées où le protocole est manquant. Parfois il s'arrêteront à l'endroit où vous les avez conduits plutôt que de vous renvoyer une erreur.





# HTTPS



Le protocole **HTTPS** ajoute le **S** de **S**ecure au protocole HTTP.

![HTTPS - wikipedia CCA](https://upload.wikimedia.org/wikipedia/commons/e/e5/HTTPS_icon.png)



- Le HTTP n'est pas sécurisé : tout ordinateur présent entre le serveur et votre ordinateur peut lire en clair, c'est à dire sans aucun cryptage, tout ce que vous demandez et tout ce que vous recevez lorsque vous utiliser ce protocole.... C'est **extrêmement dangereux** : tous vos codes et identifiants sont à la merci des pirates !


- le HTTP**S** crypte toutes les requêtes et toutes les réponses en y ajoutant une couche de chiffrement :

  - le client — par exemple le navigateur Web — contacte un serveur  — par exemple Wikipédia — et demande une connexion sécurisée, en lui  présentant un certain nombre de méthodes de chiffrement de la connexion  (des [suites cryptographiques](https://fr.wikipedia.org/wiki/Suite_cryptographique)) .

  - le serveur répond en  confirmant pouvoir dialoguer de manière  sécurisée et en choisissant dans cette liste une méthode de chiffrement  et surtout, en produisant un certificat garantissant qu'il est bien le  serveur en question et pas un serveur pirate déguisé (on parle de  l'homme du milieu). 
    Ces [certificats électroniques](https://fr.wikipedia.org/wiki/Certificat_électronique)  sont délivrés par une autorité tiers dans laquelle tout le monde a  confiance, un peu comme un notaire dans la vie courante, et le client a  pu vérifier auprès de cette autorité que le certificat est authentique  (il y a d'autres variantes, mais c'est le principe général). 
    Le  certificat contient aussi un cadenas en quelque sorte (une clé dite  publique) qui permet de prendre un message et de le mélanger avec ce  cadenas pour le rendre complètement secret et uniquement déchiffrable  par le serveur qui a émis ce cadenas (grâce à une clé dite privée, que  seul le serveur détient, on parle de [chiffrement asymétrique](https://fr.wikipedia.org/wiki/Cryptographie_asymétrique)) .

  - cela permet au client d'envoyer de manière secrète un code (une [clé symétrique](https://fr.wikipedia.org/wiki/Clé_symétrique))  qui sera mélangé à tous les échanges entre le serveur et le client de  façon que tous les contenus de la communication — y compris l'adresse  même du site web, l'[URL](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator) —  soient chiffrées. Pour cela on mélange le contenu avec le code, ce qui  donne un message indéchiffrable et à l'arrivée refaire la même opération  avec ce message redonne le contenu en clair, comme dans un miroir.

    

> En bref : serveur et client se sont reconnus, ont choisi une manière  de chiffrer la communication et se sont passé de manière chiffrée un  code (clé de chiffrement symétrique) pour dialoguer de manière secrète.  



# http 3.0 ?

HTTP/3 est la prochaine version majeure de HTTP



Ce nouveau protocole est différent de tout ce qui l'a précédé. Il s'agit d'une réécriture complète du protocole HTTP qui utilise le protocole QUIC au lieu du protocole TCP, et est également livré avec un support TLS (chiffrement) intégré. Il s'agit d'un amalgame de technologies multiples, toutes conçues pour rendre le chargement des sites Web plus rapide et sur des connexions chiffrées par défaut.



A suivre donc ......



___________

Par Mieszczak Christophe - Licence CC - BY - SA

_source images wikipédia CCA, CC BY SA (auteur précisés dans le lien)_


# Evénements et pages web



Pour chaque partie de ce TP, on partira d'une copie du répertoire `HTML_CSS_JS` qui contient trois fichiers  :

* Un fichier `index.html` contenant le DOM (squelette) d'une page HTML vide reliée au deux fichiers ci-dessous
* Un fichier `style.css` correspondant au style de la page HTML.
* Un fichier `code.js`.



Des rappels HTML et CSS sont disponibles dans [ce mémo](memo_HTML_CSS.md).



## Premières lignes de code et premier événement



La première chose à faire est de _relier_ la page HTML et le code en JS. Pour cela, il faut insérer une ligne de code dans l'entête de la page web, entre les balises `HEAD` :

```HTML
<!-- lien vers la page de code nommée ici code.js -->
<script type="text/javascript" src="code.js"></script>
```

Notez que dans les fichiers fournis, cette ligne est déjà présente.



Le fichier `code.js` contient le code qui s'exécutera au lancement de la page web, dès que le lien entre la page et le code sera lu.



**Le javascript s'exécute côté client** c'est à dire sur l'ordinateur qui utilise le navigateur web. Les informations de la page web (HTML, CSS et JS) sont envoyées depuis le serveur sur lequel le site est hébergé. Cela à une conséquence importante : il y a un temps de latence, plus ou moins important selon la connexion dont on dispose, avant que les données ne nous parviennent depuis le serveur. 

Ainsi, si le code javascript commence à s'exécuter, il pourrait essayer d'interagir avec des éléments de la page qui ne sont pas encore chargés : Il faut les attendre.

Pour cela, on va **écouter un événement** : l'événement `load`. Celui-ci se produit lorsque l'objet qu'on écoute est complétement chargé dans la fenêtre du navigateur. Lorsque cet événement se déclenche, il va alors lancer une fonction, appelée ici `demarrer` qui va initialiser le code javascript : on dit qu'**on abonne la fenêtre à l'événement load**.



 Cela se code ainsi :

```js
////////////////////////////////////////////////////////
//////// ATTENTE DU CHARGEMENT DE LA FENETRE ///////////
////////////////////////////////////////////////////////
// attend que la page web soit chargée pour lancer la fonction demarrer
window.addEventListener('load', demarrer);


////////////////////////////////////////////////////////
//////// DEMARRAGE DU CODE js                ///////////
////////////////////////////////////////////////////////

function demarrer(){
// c'est ici que tout commence dès que la page web est chargée
    
}
```

_Remarques syntaxiques:_

​	En javascript :

* chaque ligne de code se termine par un `;` ou une accolade `{` ou `}`.

* les décalages n'ont pas d'importance pour l'interpréteur de code, ce sont les accolades qui le structurent. Cependant, pour plus de lisibilité, on les fera quand même.

* Comme en Python, il faut faire très attention à la casse (majuscule/minuscule) : écrire `addeventlistener ` au lien de `AddEventListener` déclenchera une erreur de syntaxe !

  

_Remarques sur les événements :_

* Le Javascript est un langage dont les fonctions sont lancées, en partie, en réaction aux interactions de l'utilisateur. On parle d'une programmation **événementielle**. 

* En Python, certaines bibliothèques, comme [pygame](https://openclassrooms.com/fr/courses/1399541-interface-graphique-pygame-pour-python/1399674-presentation-de-pygame) par exemple, permettent de faire de la programmation événementielle. 

    

# Abonner un bouton



Copiez les _fichiers de départ_ dans un répertoire `bouton` puis ouvrez les avec un éditeur de texte.

Dans le fichier `index.html` :

* Dans le corps de la page web, placez un formulaire de type bouton :

    ```html
    <input type = 'button' id = 'bouton' value = 'Cliquez ici'/>
    ```

    * _type_ donne le type du formulaire, ici un bouton
    * _id_ est son identifiant 
    * _value_ est ce qui apparaitra sur le bouton

* Ajoutez le paragraphe ci-dessous :

    ```HTML
    <p id ='paragraphe'>
        Vous n'avez pas encore cliqué sur le bouton    
    </p>
    ```



Dans la feuille de style :

* Pour le bouton, faites en sorte que :
    * ses dimensions soient 300px sur 150 px
    * sa police soit _Comic Sans MS_ en taille 12px
    * son curseur (cursor) soit de type _pointer_
    * au survole, ses dimensions augmentent légèrement.
* Pour le paragraphe, faites en sorte que sa police soit _Comic Sans MS_ en taille 18px et de couleur bleu.



Dans le code javascript :

* Dans la fonction `demarrer`, :

    * Récupérez l'objet HTML identifié par l'id _'bouton'_ en utilisant la fonction `document.getElementById` comme indiqué ci-dessous.
    * Abonnez le bouton à l'événement `click` vers la fonction `cliquez` en utilisant la fonction `addEventListener` comme indiqué ci-dessous.
    * Définissez la fonction `cliquez` comme indiqué ci-dessous.

    ```js
    ////////////////////////////////////////////////////////
    //////// ATTENTE DU CHARGEMENT DE LA FENETRE ///////////
    ////////////////////////////////////////////////////////
    window.addEventListener('load', demarrer);
    
    ////////////////////////////////////////////////////////
    //////// DEMARRAGE DU CODE js                ///////////
    ////////////////////////////////////////////////////////
    
    function demarrer(){
        let mon_bouton = document.getElementById('bouton');
        mon_bouton.addEventListener('click', cliquer);
    
    }
    ////////////////////////////////////////////////////////
    //////// LES FONCTIONS                       ///////////
    ////////////////////////////////////////////////////////
    
    
    
    function cliquer(){
        
    }
    
    ```

    _Remarque :_ Le `let` permet de définir **une variable locale** dans la fonction.

    

* Placez une _alerte_ dans la fonction `cliquer` pour vérifier que notre abonnement marche bien :

    ```js
    function cliquer(){
    	alert('Ca marche !');   
    }
    ```

* Sauvegardez vos trois fichiers puis ouvrez le fichier `index.HTML` avec _Firefox_. cliquez sur le bouton et vérifiez que l'alerte se lance bien !

* Il faut maintenant réagir à ce clic : nous allons écrire dans le paragraphe de texte combien de clics ont été effectués. 

    * Déclarez une variable **globale** `nbre_de_clics` initialisée à 0, hors d'une fonction, traditionnellement tout en haut du code, avec le préfixe `let`.

        ```js
        
        let nbre_de_clics = 0;
        
        ////////////////////////////////////////////////////////
        //////// ATTENTE DU CHARGEMENT DE LA FENETRE ///////////
        ////////////////////////////////////////////////////////
        ```

        

    * Dans la fonction `cliquer` :

        * Enlevez l'alerte.

        * Récupérez l'objet HTML identifié par l'id _'paragraphe'_ en utilisant la fonction `document.getElementById` .

        * Il faut maintenant modifier le contenu qui se trouve entre les balises du paragraphe. Pour cela, on utilise la méthode `innerHTML` . Testez, par exemple, le code ci-dessous :

            ```js
            function cliquer(){
            	let mon_paragraphe = document.getelementById('paragraphe');
                mon_paragraphe.innerHTML = 'Vous avez cliqué !';
            }
            ```

    

    > Modifiez la fonction `cliquer` de façon à ce que le texte du paragraphe affiche le nombre de fois que l'utilisateur a cliqué.
    >
    >  
    >
    > Remarquez pour cela qu'il n'est pas utilise en js, contrairement au Python, de convertir les formats numériques en chaine de caractères pour la concaténation. 
    >
    >   

    

    _Remarque :_ 

    * La méthode `innerHML` est très puissante. On peut, grâce à elle générer une page HTML entière en insérant des éléments entre les balises `<BODY> </BODY>`.

    



* On peut également changer dynamiquement le style du paragraphe, par exemple ,grâce à sa propriété _style_. En ajoutant la ligne ci-dessous, la couleur du texte passera en rouge dès le premier clic. testez là !

    ```js
    mon_paragraphe.style.color = 'red';
    ```
    

    > Modifiez le code de la fonction `cliquer` pour que la couleur du paragraphe soit le bleu lorsque le nombre de clics est pair et le rouge sinon.
    >
    > Vous aurez besoin d'une structure conditionnelle :
    >
    > ```js
    > if (condition) { //  Les parenthèses sont obligatoires   
    > 	// ici on place la conséquence si la condition1 est vraie.
    > }
    > else if (condition2){
    > 	// ici on place la conséquence si la condition2 est vraie.
    > }
    > else {
    >      // ici on place la conséquence si les conditions précédentes sont fausses.    
    > }
    > ```
    >
    > L'opérateur qui renvoie le reste de la division euclidienne est le même qu'en python : `%`.
    >
    > 



_Remarque :_

Un bouton peut provoquer d'autre événements que le clic :

* `mouseover` se déclenche au survole de la souris.
* `mouseout` se déclenche lorsque la souris sort de la surface du bouton.



 ## Lignes de texte

Copiez les _fichiers de départ_ dans un répertoire `ligne_texte` puis ouvrez les avec un éditeur de texte.



En HTML :

* Insérez une balise paragraphe, pour l'instant vide, identifiée par _paragraphe_.

* Ajoutez un formulaire de type _texte_ grâce à les lignes ci-dessous :

    ```html
    <input type = 'text' id ='ligne_texte' value = '' name = 'question'/>
    ```
    
    * _type_ donne le type du formulaire, ici du texte

    * _id_ est son identifiant 

    * _value_ est ce qui apparaitra dans la zone de texte

    * _name_ est le nom de ce formulaire qu'il est important de précisé s'il y en a plusieurs comme nous le verrons dans un prochain cours.

        

Dans la feuille de style :

* Pour la zone de texte, faites en sorte que :
    * ses dimensions soient 300px sur 150 px
    * sa police soit _Comic Sans MS_ en taille 24px
    * son curseur (cursor) soit de type _pointer_
* Pour le paragraphe, faites en sorte que sa police soit _Comic Sans MS_ en taille 18px et de couleur bleu.



En JS, 

* La zone de texte peut réagir à divers événements :
    * `change` se déclenche lorsque le contenu a changé **et** a été validé.

    * `click`, `mouseover`, `mouseout` fonctionnent également.

    * `keypress` se déclenchera dès qu'on appuiera sur une touche dans la zone de texte.

        

    >  
    >
    > Dans la fonction `demarrer` récupérez puis abonnez la zone de texte de façon à ce qu'une modification validée de son contenu déclenche la fonction `valider` que vous définirez également. 
    >
    >  
    >
    > Vérifiez, grâce à une alerte, que votre abonnement est opérant.
    >
    >  

    

* Pour récupérer la _valeur_ contenue dans la zone de texte, on utilise sa propriété `value`. Testez cela :

    ```JS
    ////////////////////////////////////////////////////////
    //////// LES FONCTIONS                       ///////////
    ////////////////////////////////////////////////////////
    
    function valider(){
        let mon_texte = document.getElementById('ligne_texte');
        alert(mon_texte.value);
    }
    ```

    

    > 
    >
    > Modifiez la fonction `valider` pour que le paragraphe affiche 'Vous venez de valider ' suivi du contenu de la zone de texte.
    >
    >  

    

## Les cases à cocher : les checkboxs

Copiez les _fichiers de départ_ dans un répertoire `checkboxs` puis ouvrez les avec un éditeur de texte.



En HTML :

* insérez puis testez les lignes ci-dessous :

    ```html
    <p> Quels véhicules possédez vous ?<p>
    <form>
    	<input type = "checkbox" id = "vehicule1" name = "vehicule1" value = "Bateau">
    	<label for = "vehicule1"> J'ai un bateau </label><br>	
    	<input type = "checkbox" id = "vehicule2" name = "vehicule2" value = "Voiture">
    	<label for="vehicule2"> J'ai une voiture</label><br>
    	<input type = "checkbox" id = "vehicule3" name = "vehicule3" value =  "Trottinette">
    	<label for = "vehicule3"> J'ai une trottinette </label><br>
    </form>
    ```

    _Remarques :_ 

    * Notez que vous pouvez cocher une ou plusieurs cases.

    * Notez qu'il s'agit d'un _formulaire_, d'où les balises `<form>` et `</form>`.

        

* Ajoutez à la suite du formulaire un bouton sur lequel apparait _'Valider'_ et qu'on identifiera par l'id _'bouton'_.

* Ajoutez sous le bouton un paragraphe de texte identifié par l'id _`recapitulatif'._



En JS :

* On ne peut abonner les checkbox car il faut attendre que l'utilisateur est validé ou non une ou plusieurs checkbox. C'est donc le bouton qu'il faut abonner vers une fonction `traiter_reponse`. 

    

    >  
    >
    > Abonnez le bouton vers la fonction `traiter_reponse`, définissez cette fonction et tester l'abonnement avec une alerte.
    >
    >  

* Chaque_checkbox_ possède un attribut _checked_ qui vaut `true` ou `false` (pas de majuscule en js) . Dans la fonction `traiter_reponse` testez le code ci-dessous :

    ```js
    ////////////////////////////////////////////////////////
    //////// LES FONCTIONS                       ///////////
    ////////////////////////////////////////////////////////
    
    function traiter_reponse(){
        alert('ok');
        let checkbox_1 = document.getElementById('vehicule1');
        if (checkbox_1.checked) {
            alert('vous avez coché la première checkbox');
        }
        else {
            alert("vous n'avez pas coché la première checkbox");
        }
    }
    ```
    
    

    >  
>
    > Modifiez `traiter_reponse` de façon à ce que le paragraphe `recapitulatif` affiche les résultats,par exemple, sous la forme-ci dessous :
    >
    > Vous possédez 2 véhicule(s) :
    >
    > * Un bateau
    > * Une trottinette



## Les boutons radio

Copiez les _fichiers de départ_ dans un répertoire `radioboutons` puis ouvrez les.



Les radios boutons ressemblent comme deux gouttes d'eau aux _checkbox_... Mais on ne peut en choisir qu'un seul !



En HTML :

* Insérez puis testez les lignes ci-dessous :

    ```HTML
    <p> Avec quel véhicule allez-vous travailler ?</p>
    <FORM>
    	<INPUT type = "radio" id = 'radio1' name="vehicule" value="voiture"> Avec ma voiture </br>
        <INPUT type = "radio" id = 'radio2' name="vehicule" value = "vélo"> Avec mon vélo </br>
        <INPUT type = "radio" id = 'radio3' name="vehicule" value = "trottinette"> Avec la trottinette </br>
    </FORM>
    ```
    
    _Remarques :_ 
    
    * Notez que vous ne pouvez cocher qu'une seule case.
    
    * Notez qu'il s'agit d'un _formulaire_, d'où les balises `<form>` et `</form>`.
    
        

En JS :

* Pas besoin d'ajouter un bouton ici. On ne peut faire qu'un seul choix, il suffit de déclencher la fonction dès le clic sur ce choix. Dans la fonction `demarrer`, abonnez les trois boutons radio vers la même fonction `valider_choix`.

* Définissez une nouvelle fonction `valider_choix` et testez le code ci-dessous :

    ```JS
    function valider_choix(){
        alert('celui qui a lancé cette fonction est ' + this);
    	alert('id : ' + this.id + '  name : ' + this.name + ' value : ' + this.value);
    }
    ```

    > * Qui est `this` dans cette fonction ?
    > * Ajoutez au code HTML un paragraphe identifié par l'id _paragraphe_.
    > * Modifiez la fonction `valider_choix`  de façon à ce que, entre les balises du _paragraphe_, apparaisse, selon la case cochée, une phrase du type : 'Vous vous rendez sur votre lieu de travail avec votre voiture'.









## Plus loin



* Il existe un grand nombre de _formulaires_ en javascript. Pour en savoir plus sur eux, allez visitez La référence  [w3school](https://www.w3schools.com/html/html_forms.asp).



* Il existe de même de nombreux événements pour gérer les interactions homme-page web. En voici une petite liste :




| Événements |                  Condition de déclenchement                  |
| :--------: | :----------------------------------------------------------: |
|   click    |   Clique sur l'élément (on appuie puis relâche le bouton)    |
|  dblclick  |                 Double-clique sur l'élément                  |
| mousedown  | On appuie sur un bouton de la souris sans avoir besoin de le relâcher. |
| mouseover  |                Le curseur passe sur l'élément                |
|  mouseout  |                 Le curseur sort de l'élément                 |
| mousemove  |               Le curseur bouge  sur l'élément                |
|  keydown   |                   On appuie sur une touche                   |
|   keyup    |                    On relâche une touche                     |
|  keypress  |              On appuie puis relâche une touche               |
|   focus    |     On donne le focus, c'est à dire la main à l'élément      |
|    blur    |             On fait perdre le focus à l'élément              |
|   change   |        La valeur de l'élément est modifiée et validée        |
|   input    |         On tape un caractère dans un champ de texte          |
|   select   |        On sélectionne le contenu d'un champ de texte         |
|    load    | L'élément (page HTML, image...) sur lequel est "posé" l'événement est entièrement chargé sur le client |



















___________

Par Christophe Mieszczak Licence CC BY SA










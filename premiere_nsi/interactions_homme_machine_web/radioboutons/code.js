
////////////////////////////////////////////////////////
//////// ATTENTE DU CHARGEMENT DE LA FENETRE ///////////
////////////////////////////////////////////////////////

window.addEventListener('load', demarrer);

////////////////////////////////////////////////////////
//////// DEMARRAGE DU CODE js                ///////////
////////////////////////////////////////////////////////

function demarrer(){
	for (let i = 1; i < 4; i = i +1) {
		let bouton = document.getElementById('radio' + i);
		bouton.addEventListener('click', valider_choix);	
	}
	
	
	
}
////////////////////////////////////////////////////////
//////// LES FONCTIONS                       ///////////
////////////////////////////////////////////////////////

function valider_choix(){
    // alert('celui qui a lancé cette fonction est ' + this);
	// alert('id : ' + this.id + '  name : ' + this.name + ' value : ' + this.value);
	let paragraphe = document.getElementById('paragraphe');
	paragraphe.innerHTML = 'Vous venez au travail avec votre ' + this.value;
}
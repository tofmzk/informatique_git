

let nbre_de_clics = 0;

////////////////////////////////////////////////////////
//////// ATTENTE DU CHARGEMENT DE LA FENETRE ///////////
////////////////////////////////////////////////////////

window.addEventListener('load', demarrer);

////////////////////////////////////////////////////////
//////// DEMARRAGE DU CODE js                ///////////
////////////////////////////////////////////////////////

function demarrer(){
	let mon_bouton = document.getElementById('bouton'); // let sert à définir une variable locale
	mon_bouton.addEventListener('click', cliquer); // c'est un abonnement à l'événement click vers la fonction cliquer
	
}
////////////////////////////////////////////////////////
//////// LES FONCTIONS                       ///////////
////////////////////////////////////////////////////////


function cliquer(){
	// se déclenche quand on clique sur le bouton
	nbre_de_clics = nbre_de_clics + 1; // compte le nombre de clic(s) sur le bouton
	let mon_paragraphe = document.getElementById('paragraphe'); // récupère l'objet identifié par paragraphe
	mon_paragraphe.innerHTML = 'Vous avez cliqué ' + nbre_de_clics + ' fois !'; // remplace le contenu des balises par la chaine de caractères
	if (nbre_de_clics % 2 == 0){ // si le nombre de clics est pair
		mon_paragraphe.style.color = 'blue'; // on modifie le style de mon_paragraphe en passnt sa couleur en bleu
	}
	else { // sinon
		mon_paragraphe.style.color = 'red'; // on modifie le style de mon_paragraphe en passnt sa couleur en rouge
	}
}

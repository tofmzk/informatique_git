![Logo - source perso](logo.jpg)

# Introduction



*  [Le BO](BO.pdf)
* Le [Markdown](https://fr.wikipedia.org/wiki/Markdown) est le langage de balisage léger utilisé pour ces cours et TP. 
    * Ce langage permet de créer rapidement et efficacement des documents propres ,stylés, très légers et facilement exportables dans de nombreux formats (odt,  pdf, html .... et aussi docx).
    * Vous trouverez sur [openclassroom](https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown) de quoi maîtriser la syntaxe.
    * Pour visualiser ces cours localement, sur votre ordinateur, vous pouvez utiliser des logiciels comme [Typora](https://typora.io/)  (**pensez à aller dans les préférences pour activer la gestion des balises Latex**) et bien d'autres.



## Progression



<table>
    <tr>        
    	<tD align = center rowspan = 3>
            Variables - Fonctions - Conditions - Boucles
        </tD>
    	<td align = center>
        	codage des entiers positifs en base 2
        </td>
    </tr>
    <tr>
    	<tD align = center>
            Hexadécimal
        </tD>
    </tr>
    <tr>
        <td align = center>
        	codage des entiers relatifs en base 2
        </td>
    </tr>
    <tr>
    	<tD align = center rowspan = 2>
            Tableaux - Tuples - Dictionnaires et retour sur les chaînes
        </tD>
		<tD align = center>
        	Les flottants
    	</tD>
	</tr>
	<tr>
		<tD align = center>
	    	Codage des caractères
	    </tD>
	</tr>
    <tr>
        <tD align = center>
            Interface Homme-Machine
        </tD>
        <td align = center>
            Fonctions Booléennes
        </td>
    </tr>
    <tr>
        <tD align = center>
            Les tris 
        </tD>
        <tD align = center>
            Architecture Von Neumann
        </tD>
    </tr>
    <tr>
        <td align = center>
            Recherches dans une liste (parcours, dichotomie)
        </td>
        <tD align = center>
            Langage machine, assembleur et microprocesseurs
        </tD>
    </tr>
    <tr>
        <tD align = center>
            Données en tables
        </tD>
        <td align = center>
            Systèmes d'exploitation
        </td>
    </tr>
    <tr>
        <tD align = center>
            Algorithmes gloutons
        </tD>
        <td align = center>
            Transmission des données dans un réseau
        </td>
    </tr>
    <tr>
        <tD align = center>
            Algorithmes des plus proches voisins
        </tD>
        <td align = center colspam = 2>
            Interaction homme-machine sur le web
        </td>
    </tr>
    <tr>
        <td align = center colspan = 2> Projets </td>
    </tr>
</TABLE>




## Sommaire



### Représentation des données

* Les entiers :
    * [Les Shadocks](representations/representation_des_entiers/shadocks.md)
    * [Représentation des entiers positifs](representations/representation_des_entiers/representation_des_entiers_positifs.md)
    * [Hexadécimal](representations/representation_des_entiers/hexadecimal.md)
    * [Codages des caractères](representations/representation_des_caracteres/codages_des_caracteres.md) (nous en profitons pour faire un peu de HTML/CSS au passage) 
    * [Représentation des entiers relatifs](representations/representation_des_entiers/representation_des_entiers_relatifs.md)
    * [Exercices](representations/representation_des_entiers/exos.md)
* [Représentation des décimaux : les flottants](representations/representation_des_decimaux/les_flottants.md)



### Langages de programmation

* [Variables et types de base](langages_de_programmation/variables.md)

* [Structures conditionnelles](langages_de_programmation/les_conditions.md)

* [Les boucles](langages_de_programmation/les_boucles.md)

* [Prototyper une fonction, DocStrings et Doctests](langages_de_programmation/les_fonctions.md)

* [Retour sur les chaînes de caractères](langages_de_programmation/chaines_de_caracteres.md)

* [Exercices](langages_de_programmation/exercices.md)

    

### Les types construits

* [Tableaux indexés](types_construits/tableaux.md)

* [Exercices sur les tableaux](types_construits/exercices_tab.md)

* [Les tuples](types_construits/tuples.md)

* [Les dictionnaires](types_construits/dictionnaires.md)

* [Exercices sur les dictionnaires](types_construits/exercices_dic.md)

    


### Algorithmique

* Les tris :
    * [TP introduction](algorithmique/tris/tris_tp.md)
    
    * [Tri par sélection](algorithmique/tris/tri_selection.md)
    
    * [Tri par insertion](algorithmique/tris/tri_insertion.md)
    
    * [Exercices](algorithmique/tris/exercices_tris.md)
    
        
    
* [Dichotomie](algorithmique/dichotomie/dichotomie.md)

* [Algorithmes gloutons](algorithmique/algorithmes_gloutons/algo_gloutons.md)

    

* Les k plus proches voisins :
    * [knn, k **n**earest **n**eighbors](algorithmique/k_plus_proches_voisins/k_plus_proches_voisins.md)
    * [Exercices déconnectés](algorithmique/k_plus_proches_voisins/exos_deconnectes.md)



### Données en table

* [Le format csv et les Pokemons](donnees_en_table/pokemon.md)

* [Collecter des donner pour votre quizz](donnees_en_table/quizz.md)

    



### Architecture

* [Fonctions booléennes](architecture/fonctions_booleennes/fonctions_booleennes.md)
* Microprocesseurs :
    * [Architecture Von Neumann](architecture/microprocesseurs/architecture_von_neumann.md)
    * [Assembleur et M10](architecture/microprocesseurs/M_10.md)
    * [Microprocesseur M999](architecture/microprocesseurs/M_999.md)
* Systèmes d'exploitation :
    * [Généralités](architecture/OS/OS.md)
    * [Système de fichiers](architecture/OS/le_systeme_de_fichiers.md)
    * [SHELL et système de fichiers](architecture/OS/shell.md)
* Transmission de données dans un réseau :
    * le [TP avec le logiciel _Filius_](architecture/transmission_donnees_et_reseaux/TP_Filius.md) permet d'aborder les parties suivantes :
        * [Généralités, composants d'un réseau](architecture/transmission_donnees_et_reseaux/generalites.md)
        * [Adresses IP](architecture/transmission_donnees_et_reseaux/adresses_IP.md)
        * [Acceder à un site web](architecture/transmission_donnees_et_reseaux/acceder_a_un_site_web.md)
        * [Le modèle TCP/IP](architecture/transmission_donnees_et_reseaux/modele_tcpip.md)
    * [Protocole du bit alterné](architecture/transmission_donnees_et_reseaux/bit_alterne.md)
* IHM 
    * [Interface Homme-machine](architecture/IHM/IHM.md)
    * [La carte _micro:bit_](architecture/IHM/carte_microbit.md)



### Interaction homme-machine sur le web

* [Petit mémo HTML et CSS](interactions_homme_machine_web/memo_HTML_CSS.md)
* [Evenements et pages web](interactions_homme_machine_web/evenements_et_pages_web.md)
* [Clients et serveurs](interactions_homme_machine_web/clients_et_serveurs.md)
* [Requêtes GET et POST](interactions_homme_machine_web/get_post.md)





### Projets

* La base : découpage en petites fonctions, conditions, boucles
    * [Dessiner avec la tortue](projets/tortue/la_tortue.md)

    * [La conjecture de Syracuse](projets/syracuse/syracuse.md)

    * [Le jeu du Nim](projets/nim/nim.md)

        

* Avec des types construits.

    * [Les envahisseurs](projets/invaders/invaders.md) (tableaux, module _pyxel_)

    * [D'un ami à un autres](projets/les_amis/les_amis.md) (tableaux puis dictionnaires)

    * [Le pendu](projets/pendu/pendu.md) (module, chaînes de caractères)

    * [Quizz](projets/quizz/quizz.md) (module _random_, tableaux de tableaux)

    * [Filtres photographiques](projets/filtres_photos/filtres.md) (tableaux, tuples)

    * [Le SCRABBLE](projets/scrabble/scrabble.md) (utilisation d'un module, tableaux et dictionnaires, tris)

    * [Jeu du Tic Tac Toe (OXO)](projets/oxo/oxo.md) (tableaux de tableaux) 

    * [Le jeu de la vie](projets/jeu_de_la_vie/jeu_de_la_vie.md) (tableaux de tableaux, interface graphique) 

    * [Bataille navale : touché coulé](projets/touche_coule/touche_coule.md) (tableaux de tableaux) 

        

* Données en Tables, tableaux, dictionnaires, NamedTuple, tris 

    * [La course au chicon](projets/chicon/chicon.md) (namedTuple, tris, données en table)
    * [Le voyageur de commerce](projets/voyageur_de_commerce/voyageur_de_commerce.md) (algorithme glouton, module Folium)
    * [Le Choipeau](projets/choipeau/choipeau.md) (algorithme knn)
    * [Gestion de mots de passe](projets/mot_de_passe/mot_de_passe.md) (HTML, serveur Python, requêtes get et post)  



* Informatique embarquée :
    * [Nim et carte _micro:bit_](projets/nim_microbit/nim_microbit.md)
    
        



### QCM

Voici 20 sujets portants sur l'ensemble du programme de première NSI mis à disposition par Laurent Chéno, Inspecteur général.

* [Sujet 1](QCM/Q01-SUJET.pdf) et sa [correction](QCM/Q01-CORRIGE.pdf)

* [Sujet 2](QCM/Q02-SUJET.pdf) et sa [correction](QCM/Q02-CORRIGE.pdf)

* [Sujet 3](QCM/Q03-SUJET.pdf) et sa [correction](QCM/Q03-CORRIGE.pdf)

* [Sujet 4](QCM/Q04-SUJET.pdf) et sa [correction](QCM/Q04-CORRIGE.pdf)

* [Sujet 5](QCM/Q05-SUJET.pdf)  et sa [correction](QCM/Q05-CORRIGE.pdf)

* [Sujet 6](QCM/Q06-SUJET.pdf)  et sa [correction](QCM/Q06-CORRIGE.pdf)

* [Sujet 7](QCM/Q07-SUJET.pdf) et sa [correction](QCM/Q07-CORRIGE.pdf)

* [Sujet 8](QCM/Q08-SUJET.pdf) et sa [correction](QCM/Q08-CORRIGE.pdf)

* [Sujet 9](QCM/Q09-SUJET.pdf) et sa [correction](QCM/Q09-CORRIGE.pdf)

* [Sujet 10](QCM/Q10-SUJET.pdf) et sa [correction](QCM/Q10-CORRIGE.pdf)

* [Sujet 11](QCM/Q11-SUJET.pdf) et sa [correction](QCM/Q11-CORRIGE.pdf)

* [Sujet 12](QCM/Q12-SUJET.pdf) et sa [correction](QCM/Q12-CORRIGE.pdf)

* [Sujet 13](QCM/Q13-SUJET.pdf) et sa [correction](QCM/Q13-CORRIGE.pdf)

* [Sujet 14](QCM/Q14-SUJET.pdf) et sa [correction](QCM/Q14-CORRIGE.pdf)

* [Sujet 15](QCM/Q15-SUJET.pdf) et sa [correction](QCM/Q15-CORRIGE.pdf)

* [Sujet 16](QCM/Q16-SUJET.pdf) et sa [correction](QCM/Q16-CORRIGE.pdf)

* [Sujet 17](QCM/Q17-SUJET.pdf) et sa [correction](QCM/Q17-CORRIGE.pdf)

* [Sujet 18](QCM/Q18-SUJET.pdf) et sa [correction](QCM/Q18-CORRIGE.pdf)

* [Sujet 19](QCM/Q19-SUJET.pdf) et sa [correction](QCM/Q19-CORRIGE.pdf)

* [Sujet 20](QCM/Q20-SUJET.pdf) et sa [correction](QCM/Q20-CORRIGE.pdf)

    



__________

Par Mieszczak Christophe CC BY SA

_source image : production personnelle_






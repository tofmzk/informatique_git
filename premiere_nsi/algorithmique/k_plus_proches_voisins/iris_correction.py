import pylab
import math


def lire_donnees(nom_fichier_CSV, separateur = ';'):
    """
        lit le fichier csv dans le repertoire data et revoie la liste des donnees
        : param nom_fichier_CSV
        type str
        : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
        type str
        return liste des données
        type list
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    try :
        lecture=open(nom_fichier_CSV,'r',encoding='utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le ficier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
    lecture.close()
    convertir(toutes_les_lignes)
    return toutes_les_lignes

def convertir(donnees):
    '''
    convertit les 2 attributs width/length au format float
    : param donnees une liste de données
    type list
    :EFFET DE BORD SUR donnees
    '''
    for i in range(1, len(donnees)):
        donnees[i][0] = float(donnees[i][0])
        donnees[i][1] = float(donnees[i][1])
        
def representer_donnees(donnees):
    '''
    représente graphiquement les données de la liste
    : param donnees
    type list
    '''
    # on enlève la ligne des attributs
    pts = donnees[1:]
    # dictionnaire de couleur selon la classe
    couleur = { 'setosa' : 'green',
                'virginica' : 'red',
                'versicolor' : 'blue'
            }
    #iinitialisation de pylab
    pylab.title('Les iris')
    pylab.xlabel('length')
    pylab.ylabel('width')
    # ajout des points 
    for i in range(len(pts)):
        pylab.scatter(pts[i][0],
                   pts[i][1],
                   color = couleur[pts[i][2]],
                   linewidth = 2.0)
    # trois fleurs inconnues
    pylab.scatter(1.3, 0.4, color = 'black', linewidth = 2.0)
    pylab.scatter(6.5, 2.2, color = 'black', linewidth = 2.0)
    pylab.scatter(2.5, 0.75, color = 'black', linewidth = 2.0)
    # ajouter du texte
    pylab.text(1, 0.7, 'setosa', color = 'green')
    pylab.text(3, 1.5, 'virginica', color = 'red')
    pylab.text(6, 1.5, 'versicolor', color = 'blue')
    # générer l'affichage
    pylab.show()
    
def distance(x1, y1, x2, y2):
    '''
    renvoie la distance euclidienne
    : param x1,y1,x2,y2 les coordonnées
    type float
    : return la distance
    type float
    '''
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

def calculer_distances(x_inconnue, y_inconnue, donnees):
    '''
     renvoie une liste de tuple du genre [(classe1, distance1), ...] où chaque tuple précise la classe de chaque fleur
     présente dans les données et la distance à la fleur de classe inconnue 
    : param x_inconnue, y_inconnue les coordonnees de l'inconnue
    type float
    : param donnees la liste des données d'apprentissage
    type liste
    : return liste de tuple (classe , distance)
    type list
    '''
    liste_distances = []
    for elt in donnees[1:] :
        liste_distances.append(
                                (
                                distance(x_inconnue, y_inconnue, elt[0], elt[1])
                                ,
                                elt[2]
                                )
                               )
                               
    return liste_distances

def renvoyer_k_plus_proches(k, x_inconnu, y_inconnu, donnees):
    '''
    renvoie la liste des tuples correspondant aux _k_ fleurs les plus proches de notre fleur
    : param k le nombre de voisins
    type int
     : param x_inconnue, y_inconnue les coordonnees de l'inconnue
    type float
    : param donnees la liste des données d'apprentissage
    type liste
    : return liste des k plus proches voisins
    '''
    liste_distances = calculer_distances(x_inconnu, y_inconnu, donnees)
    liste_distances.sort( key = lambda clef : clef[0])
    liste_k_plus_proches = []
    for i in range(k):
        liste_k_plus_proches.append(liste_distances[i][1])
    return liste_k_plus_proches
    
def classifier(k, x_inconnu, y_inconnu, donnees):
    '''
    renvoie la classe de l'inconnue
    : param k le nombre de voisins
    type int
     : param x_inconnue, y_inconnue les coordonnees de l'inconnue
    type float
    : param donnees la liste des données d'apprentissage
    type liste
    : return la classe de l'inconnue
    type str
    '''
    liste_k_plus_proches = renvoyer_k_plus_proches(k, x_inconnu, y_inconnu, donnees)
    effectif_classes = {'setosa' : 0, 'virginica':0, 'versicolor' : 0}
    for fleur in liste_k_plus_proches :
            effectif_classes[fleur] += 1
    for cle in effectif_classes :
        if effectif_classes[cle] == max(effectif_classes.values()) :
            return cle

def tester(k, donnees):
    '''
     teste la fonction `classifier` sur chaque fleur présente dans le jeu de données pour k voisins et renvoie le taux d'erreur.
     : param k le nbre de voisins recherchés
     type int
     : param donnees liste des donnees
     type list
     : return taux d'erreur
     type float
    '''
    nbre_erreurs = 0
    nbre_donnees = 0
    for fleur in donnees[1:] :
        nbre_donnees += 1
        if classifier(k, fleur[0], fleur[1], donnees) != fleur[2] :
            nbre_erreurs +=1
    return nbre_erreurs / nbre_donnees
            
import math
import pylab

def f(x) :
    return math.cos(x)

def g(x) :
    return x ** 2 + 2 * x + 1

def afficher(f, x_min, x_max, pas):
    '''
    représente graphiquement f
    : param f
    type function
    '''
    pylab.title('Courbe de f')
    pylab.xlabel('x')
    pylab.ylabel('y')
    # ajout des points 
    x = x_min
    while x <= x_max :
        pylab.scatter(x, f(x), color = 'blue', linewidth = 2.0)
        x += pas      
    
    # générer l'affichage
    pylab.show()
# Les k plus proches voisins (knn)



## Introduction



L’algorithme des k plus proches voisins, appelé **knn** pour **k** **n**earest **n**eighbors, est un algorithme de **classification** qui appartient à la famille des **algorithmes d’apprentissage automatique (machine learning)**.

Le terme de machine learning a été utilisé pour la première fois par l’informaticien américain Arthur Samuel en **1959**, il n'est donc pas des plus récent, même si les algorithmes d’apprentissage automatique ont connu un fort regain d’intérêt au début des années 2000 notamment grâce à la quantité de données disponibles sur internet :  de nombreuses sociétés (comme les **G**oogle**A**pple**F**acebok**A**mazone**M**icrosoft) utilisent les données concernant leurs utilisateurs afin de ”nourrir” des algorithmes de machine learning qui permettront à ces sociétés d’en savoir toujours plus sur nous et ainsi de mieux cerné nos ”besoins” en termes de consommation ... au détriment de la vie privée.



## Principe

Tout d'abord, il nous faut avoir un ensemble de données d'apprentissage. Chaque donnée :

* appartient à une **classe**.
* est caractérisée par un certains nombre d'attributs (ou **labels**). 



Nous avons un individu non identifié, c'est à dire de **classe inconnue**, dont on possède cependant les caractéristiques. L'objectif est de classifier cet individu en regardant ses k plus proches voisins .



_Exemple :_ 

* Voici la représentation de deux classes : les carrés et les disques. En représentant leurs caractéristiques graphiquement, on obtient la répartition ci-dessous. Un individus de classe inconnue possédant ces mêmes caractéristiques, est représenté par le triangle.

<img src="media/knn_ex_perso.jpg" alt="knn exemple - source perso" style="zoom:50%;" />



* En prenant k=3, les 3 plus proches voisins sont 2 disques et un carré : on décidera que l'inconnu est un disque.
* En prenant k=7, les 7 plus proches voisins sont 4 disques et 3 carrés : on décidera encore que l'inconnu est un disque.
* En prenant k=13, les 13 plus proches voisins sont 6 disques et 7 carrés : on décidera alors que l'inconnu est un carré.



_Remarques :_

* L' algorithme *k*-NN est un algorithme de décision qui pourrait être résumé par la phrase "dis moi qui sont des voisins et je te dirai qui tu es". 
* En prenant une décision, on fait un choix éclairé mais on ne peut pas être à 100% certain d'avoir fait le bon :
    * La définition de la _distance_ entre deux classes est particulièrement importante.
    * Le choix du _k_ , c'est à dire du nombre de voisins à étudier, est également primordial.



## Etude florale

### Trois classes d'iris



Nous allons utilisé  le fichier joint `iris.csv`  ([issu de ''The iris dataset''](https://gist.github.com/curran/a08a1080b88344b0c8a7)) qui sera notre jeu de données d'apprentissage . On identifie trois espèces parmi ces fleurs, donc trois **classes**, d'iris :

* les iris setosa 
* les iris virginica
* les iris versicolor 



Les **labels** selon lesquelles les iris sont étudiées sont :

- la largeur _width_ des pétales.	
- la longueur _length_ des pétales.	



### Représentation graphique 



Nous allons ici représenter graphiquement les fleurs étudiées en prenant :

* la largeur des pétales d'un individu en abscisse.
* la longueur des pétales d'un individu en ordonnée.
* Une couleur différente selon la classe de l'individu.



Créez un fichier `iris.py`.



Tout d'abord, il faut importer la base dans une liste en utilisant la fonction `lire_donnees` que nous avons déjà vu dans le chapitre _données en tables_ :

```python
def def lire_donnees(nom_fichier_CSV, separateur = ';'):
    """
    lit le fichier csv dans le repertoire data et revoie la liste des donnees
    : param nom_fichier_CSV (str)
    : param separateur (str) le séparateur par défaut ;
    return tableau des données (list)
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    try :
        lecture=open(nom_fichier_CSV,'r',encoding='utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le ficier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
    lecture.close()
    return toutes_les_lignes
```



> Les attributs `petal_length` et  `petal_width` ayant pour valeurs des nombres, réalisez une fonction `convertir`de paramètre _donnees_, la liste de donnees obtenue en chargeant le fichier `iris.csv`, qui, par effet de bord, convertit les valeurs de ces deux attributs.
>```python
def convertir(donnees):
        '''
        convertit les 2 attributs width/length au format float
        : param donnees une liste de données
        type list
        :EFFET DE BORD SUR donnees
        '''
    ```



Maintenant il faut importer une bibliothèque permettant de réaliser, entre autres, des représentations graphiques, `pylab`, en ajoutant la ligne adéquate en haut du code :

```python
import pylab
```


Voici la fonction qui nous permettra d'afficher nos données. Regardez bien ce code pour en comprendre le fonctionnement :


```python
def representer_donnees(donnees):
    '''
    représente graphiquement les données de la liste
    : param donnees
    type list
    '''
    # on enlève la ligne des attributs
    pts = donnees[1:]
    # dictionnaire de couleur selon la classe
    couleur = { 'setosa' : 'green',
                'virginica' : 'red',
                'versicolor' : 'blue'
            }
    #iinitialisation de pylab
    pylab.title('Les iris')
    pylab.xlabel('length')
    pylab.ylabel('width')
    # ajout des points 
    for i in range(len(pts)):
        pylab.scatter(pts[i][0],
                   pts[i][1],
                   color = couleur[pts[i][2]],
                   linewidth = 2.0)
    # ajouter du texte
    pylab.text(1, 0.7, 'setosa', color = 'green')
    pylab.text(3, 1.5, 'virginica', color = 'red')
    pylab.text(6, 1.5, 'versicolor', color = 'blue')
    # générer l'affichage
    pylab.show()
```



> 
>
> * Quelles données sont ignorées pour l'affichage ?
>
>     ```
>     
>     ```
>
> * Quelle structure de donnée est utilisée pour la couleur ?
>
>     ```txt
>     
>     ```
>
> * Quelle méthode permet de placer un point dans le repère ?
>
>     ```txt
>     
>     ```
>
> * A quoi sert l'instruction `pylab.show()` ?
>
>     ```txt
>         
>     ```



Créons la représentation graphique. Depuis la console :

* Importez les données dans une liste _fleurs_.
* convertissez les données au bon format
* affichez les données.



Vous devriez obtenir cela :

<img src="media\representation_iris.png" alt="représentation des données - perso" style="zoom:50%;" />

On remarque que les trois classes sont plutôt bien différenciées.



## Ajout d'une iris de classe inconnue.

En se promenant, on découvre trois iris dont on mesure les pétales :

* pour la première on obtient 1,3 cm sur 0,4cm

* pour la seconde on obtient 6,5cm sur 2,2 cm
* pour la troisième on obtient 2,5cm sur 0,75cm



> * Modifiez la fonction `representer_donnees` de façon à ajouter ces inconnues avec un point noir. Vous devriez obtenir ceci :
>
>     <img src="media\inconnues.png" alt="inconnues - perso" style="zoom: 50%;" />
>
>     
>
> * Quelle est la classe de la première fleur ?
>
>     ````txt
>         
>     ````
>
>     
>
> * Quelle est la classe de la deuxième fleur ?
>
>     ```txt
>     
>     ```
>
> * Peut-on trancher aussi nettement pour la troisième ?
>
>     ````txt
>         
>     ````
>
>     



Pour la dernière fleur, c'est donc plus délicat. On va devoir regarder à quelle distance elle se situe des autres. 

Pour calculer la distance entre deux points de coordonnées respectives $(x_1; y_1)$ et $(x_2; y_2)$, on applique la formule mathématiques : $d = \sqrt {(x_1 - x_2)² + (y_1 - y_2)²}$.



> Réalisez la fonction `distance` de paramètres _x1_, _y1_, _x2_ et _y2_, quatre flottants, qui renvoie la distance entre les points de coordonnées respectives $(x_1; y_1)$ et $(x_2; y_2)$.
>
> N'oubliez pas d'importer le module `math` pour pouvoir utiliser la fonction racine carrée `sqrt`.
>
> ````python
> def distance(x1, y1, x2, y2):
>     '''
>     renvoie la distance euclidienne
>     : param x1,y1,x2,y2 les coordonnées
>     type float
>     : return la distance
>     type float
>     '''
> ````
>
> 



Il va falloir maintenant regarder qui sont ses _k_ plus proches voisins. 



### Recherche des k plus proches voisins



Commençons par simplement calculer les distances entre notre inconnue et les autres fleurs.

> Créez une fonction `calculer_distances` de paramètres _x_inconnue_, _y_inconnue_ et _donnees_ qui renvoie une liste de tuple du genre [(classe1, distance1), ...] où chaque tuple précise la classe de chaque fleur présente dans les données et la distance à la fleur de classe inconnue de coordonnées  ($x_{inconnue}; y_{inconnue})$.
>
> ````python
> def calculer_distances(x_inconnue, y_inconnue, donnees):
> 	'''
>     renvoie une liste de tuple du genre [(classe1, distance1), ...] où chaque 	tuple précise la classe de chaque fleurprésente dans les données et la distance à la fleur de classe inconnue 
> 	 : param x_inconnue, y_inconnue les coordonnees de l'inconnue
> 	 type float
> 	 : param donnees la liste des données d'apprentissage
> 	 type liste
> 	 : return liste de tuple (classe , distance)
> 	 type list
> 	'''
> ````
>
> 
>
> 



Conservons maintenant les _k_ plus proches voisins

> Créez une fonction `renvoyer_k_plus_proches` de paramètres _k_ ,  _x_inconnue_, _y_inconnue_ et _donnees_ qui renvoie la liste des tuples correspondant aux _k_ fleurs les plus proches de notre fleur inconnue de coordonnées ($`x_{inconnue}; y_{inconnue})`$.
>
> ```python
> def renvoyer_k_plus_proches(k, x_inconnu, y_inconnu, donnees):
>    	'''
>    	renvoie la liste des tuples correspondant aux _k_ fleurs les plus proches e notre fleur
>        : param k le nombre de voisins
>        type int
>        : param x_inconnue, y_inconnue les coordonnees de l'inconnue
>        type float
>        : param donnees la liste des données d'apprentissage
>        type liste
>        : return liste des k plus proches voisins
>        '''
> ```
>
> _Remarque :_ 
>
> Pour trier une liste de tuples selon la valeur d'indice _n_ du tuple,  on utilisera l'instruction suivante qui utilise une fonction `lambda`, c'est à dire une fonction sans nom définie sur une ligne de façon un peu particulière.
>
> ```python
> nom_de_la_liste_de_tuples = sorted(nom_de_la_liste_de_tuples, key = lambda clef : clef[n])
> ```
>
> 



Il reste à décider !

> Créez une fonction `classifier`de paramètres _k_ ,  _x_inconnue_, _y_inconnue_ et _donnees_ qui renvoie la classe la plus représentée parmi les k plus proche fleurs  de notre fleur inconnue de coordonnées ($`x_{inconnue}; y_{inconnue})`$.
>
> ```python
>     def classifier(k, x_inconnu, y_inconnu, donnees):
>  		'''
> 	     renvoie la classe de l'inconnue
> 	     : param k le nombre de voisins
> 	     type int
> 	      : param x_inconnue, y_inconnue les coordonnees de l'inconnue
> 	     type float
> 	     : param donnees la liste des données d'apprentissage
> 	     type liste
> 	     : return la classe de l'inconnue
> 	     type str
> 	    '''
> ```
>
> _Remarques :_ 
>
> * Pour compter l'effectif de chaque classe (espèce) de fleurs, on pourra utiliser un dictionnaire.
>
>     ```python
>     effectif_classes = {'setosa' : 0, 'virginica':0, 'versicolor' : 0}
>     ```
>
>     
>
> * On pourra utiliser la fonction `max`
>
>     ```python
>     max(dic) # renvoie la plus grande clé du dictionnaire
>     max(dic.values()) # renvoie la plus grande valeur du dictionnaire
>     ```
>
>     



### Le cas du _k_



Le choix de la valeur de _k_ est primordial :

* Comme on hésite souvent entre deux catégorie, on prend toujours un _k_ impair de façon à éviter les cas d'égalité. 

* Mais quelle valeur choisir ? 

    

Pour cela nous allons tester l'efficacité de l'algorithme selon les valeurs de _k_ sur le jeu de données d'apprentissage.

> Réalisez une fonction `tester`, de paramètres _k_ et _donnees_, qui teste la fonction `classifier` sur chaque fleur présente dans le jeu de données d'apprentissage pour _k_ voisins, et, renvoie le taux d'erreur.
>
> ```python
> def tester(k, donnees):
>     '''
>      teste la fonction `classifier` sur chaque fleur présente dans le jeu de données pour k voisins et renvoie le taux d'erreur.
>      : param k le nbre de voisins recherchés
>      type int
>      : param donnees liste des donnees
>      type list
>      : return taux d'erreur
>      type float
>     '''
> ```
>
> 



Complétez le tableau ci-dessous à l'aide de la fonction `tester`.

| k             |  3   |  5   |  7   |  10  |
| ------------- | :--: | :--: | :--: | :--: |
| Taux d'erreur |      |      |      |      |



> Quelle valeur choisir pour k ?
>
> ```txt
> 
> ```



_Remarque :_

Si le jeu de données d'apprentissage est trop important, on peut parfois extraire un jeu représentatif plus petit pour effectuer ces tests qu'on appellera alors judicieusement jeu de test.



### Conclusion



Identifiez la troisième fleur, pour laquelle on avait mesuré  2,5 cm sur 0,75 cm.

```txt

```



## Complexité



La `complexité` de l'algorithme `knn`  repose  sur la complexité du tri de la table.  D'après la documentation du langage Python,  le tri de la table dans `sorted`selon la `key` `distance` est en $`O(nlog(n))`$.  Cela veut dire que son coût est comparable à $`nlog(n)`$ ($`n`$ est la taille de la table à trier). Dans ce cas on parle de complexité `quasi-linéaire`.

____

Mieszczak Christophe CC- BY - SA    

_sources images : productions personnelles_
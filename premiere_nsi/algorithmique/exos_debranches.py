def fonction1():
    liste = [0, 0, 0, 0]
    i = 1
    while i < len(liste):
        liste[i] = 3 * liste[i - 1] + 1
        print(liste[i - 1], " ", end = "")
        i += 1
    print(liste[i - 1])
    return liste
    

def fonction2():
    liste = fonction1()
    i = 0
    cumul = 0
    cpt_par = 0
    while cumul < 15:
        cumul += liste[i]
        print(cumul, ' ', end = '')
        if liste[i] % 2 == cumul % 2:
            print('=')
            cpt_par += 1
        else :
            print("!=")
        i += 1
    print(cpt_par, "parités égales")

    
# Exercices débranchés



## Qu'affiche la console ?

Ecrire exactement (en tenant compte des passage à la ligne) ce que le programme ci-dessous va afficher dans la console. On ne veut aucune explication mais seulement ce qui serait affiché si votre feuille était la console.



### Exercice 1

```python
def fonction1():
    liste = [0, 0, 0, 0]
    i = 1
    while i < len(liste):
        liste[i] = 3 * liste[i - 1] + 1
        afficher(liste)
        i += 1
    return liste

def afficher(liste):
    for elt in liste:
        print(elt, end = "")
```

```python
>>> fonction1()
???
```



### Exercice 2

```python
def fonction2(liste):
    cumul = 0
    for elt in liste :
        cumul += elt
        if cumul % 2 == 0 :
            return cumul
    return cumul

```

```python
>>> fonction2([1, 2, 3, 4])
???
>>> fonction2([1, 2, 4, 6])
???
```

### Exercice 3



```python
def fonction3(liste):
    
```


from timeit import timeit
import pylab

#######################
######## calculs de temps
#####################

def mesure_temps_execution(nom_tri,longueur_listes,nbre_repetitions):
    """
    renvoie le temps d'exécution de nbre_repetitions d'un tri de nom nom_tri d'une liste de longueur longueur_liste
    : param nom_tri str
    : longeur_listes  int
    : nbre_repetitions int
    
    
    """
    return (timeit(setup='from tris import '+nom_tri+'; from listes import cree_liste_melangee',
       stmt=nom_tri+'(cree_liste_melangee('+str(longueur_listes)+'))',
       number=nbre_repetitions))


############    

def calculer_temps_un_tri(nom_tri,taille_liste,nbre_essais):
    """
        renvoie la liste des temps d'execution de l'algo de tri pour des tailles de liste à trier allant de 1 à taille_liste
        param nom_tri  nom du tri
        type str
        param taille_liste la taille des listes à trier varie de 1 à taille_liste
        type int
        nbre_essais nbre de répétition de l'algo de tri choisi
        type int
        
    """
    liste_temps=[]
    for i in range(taille_liste):
        liste_temps.append(mesure_temps_execution(nom_tri,i+1,nbre_essais))
    return liste_temps    
          
    
######################
###### afficher les courbes 
#####################
    
def tracer_courbe_un_tri(nom_tri,taille_liste,nbre_essais=100):
    """
        affiche l'évolution du temps d'execution de tri nom_tri pour des liste de taille allant de 1 à taille_liste en répétant nbre_essais tris
        param nom_tri nom du tri à traiter
        type str
        param taille_liste taille max de la liste à trier
        type int
        param nbre_essais nbre de tri à réaliser par défaut 100
        type int
    """
    liste_temps=calculer_temps_un_tri(nom_tri,taille_liste,nbre_essais)
    init_pylab('Temps d\'execution du tri '+nom_tri+' (pour {:d} essais'.format(nbre_essais)+' de taille {:d}'.format(taille_liste)+')',
               'taille des listes',
               'temps en secondes'
               )
    ajouter_pylab(liste_temps,nom_tri,'blue')
    afficher_pylab()
    

def tracer_courbes_tris(taille_liste,nbre_essais=100):
    """
        affiche l'évolution du temps d'execution des 4 tris et du tri sort de python pour des liste de taille allant de 1 à taille_liste en répétant nbre_essais tris
        param nom_tri nom du tri à traiter
        type str
        param taille_liste taille max de la liste à trier
        type int
        param nbre_essais nbre de tri à réaliser par défaut 100
        type int
    """
    noms_tri=['tri_insertion','tri_selection']
    couleur_tri={
            'tri_insertion':'red',
            'tri_selection': 'green',

            }
    init_pylab('Comparatif temps des tris (pour {:d} essais'.format(nbre_essais)+' de taille {:d}'.format(taille_liste)+')',
               'taille des listes',
               'temps en secondes'
               )
  
    for nom in noms_tri:
        for i in range(taille_liste):
            liste_temps=calculer_temps_un_tri(nom,taille_liste,nbre_essais)
        ajouter_pylab(liste_temps,nom,couleur_tri[nom]) 
    afficher_pylab()

#########################
#########PYLAB
#########################    
    
def init_pylab(titre,x_label,y_label):
    """
        initialise la zone graphique
        param titre,x_label_y_label
        type str
    """
    pylab.title(titre)
    pylab.xlabel(x_label)
    pylab.ylabel(y_label)
    pylab.grid()

def ajouter_pylab(liste,label_liste,couleur="black"):
    """
        ajoute la liste aux courbes à tracer
        param liste
        type list
        param label_liste
        type str
        param couleur
        type str
    """
    pylab.plot(liste)    
    pylab.plot(liste,label=label_liste,color=couleur)
    
def afficher_pylab():
    """
        afficher les courbes et leurs légendes
    """
    pylab.legend()
    pylab.show()     
   
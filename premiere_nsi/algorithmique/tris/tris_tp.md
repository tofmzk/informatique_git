# Les Tris: exercices 

Pour classer une liste d'objets, nous avons besoin :

* d'un opérateur capable de comparer deux objets entre eux.
* d'un mode opératoire algorithmique permettant de réaliser le tri en utilisant le comparateur . A chaque étape du tri, on ne peut réaliser qu'une seule comparaison.



### Tris virtuels



Allons visiter cet  [Extraordinaire site sur les tris](http://lwh.free.fr/pages/algo/tri/tri.htm).

Nous allons réaliser quelques tris en utilsant comme opérateur la balance. Celle-ci est capable de comparer deux _tonneaux_ l'un par rapport à l'autre. Comment allons nous procéder pour trier l'ensemble des tonneaux ?



### Tris sur papier



Classer par ordre croissant les éléments des listes ci-dessous.  **Sur papier**, pour chaque exercice :

* Définissez un comparateur selon le type de chose à trier.
* On ne peut réaliser qu'une comparaison par étape. 
* Rédiger en français l'algorithme utilisé pour réaliser le tri.
* Comptez le nombre d'étape et le nombre de comparaisons effectuées.



**Ex 1 :**



|  1   |  2   |  6   |  5   |  9   |  7   |  4   |  3   |
| :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: |
|      |      |      |      |      |      |      |      |





**Ex 2 :**

* Un glacier propose 5 parfums : vanille, fraise, chocolat et pistache et nougât. 
    * Définissez un comparateur
    * Triez les glaces

 ![2 boules](media/vanille.jpg)  ![2 boules](media/chocolat.jpg)  ![2 boules](media/nougat.jpg)  ![2 boules](media/pistache.jpg)  ![2 boules](media/fraise.jpg) 








**Ex 3 : pour cet exercice, trouvez deux comparateurs différents**



BONJOUR, HI,  GUTENTAG, SALAM, BUENOSDIAS,  AYO





# Conclusion :



* ### Le tri est indépendant de l'opérateur utilisé pour comparer deux valeurs. 



* ### Une même liste peut être triée suivant différents opérateurs (ordre alphabétique ou longueur d'un mot par exemple).

    

_____

Par Mieszczak Christophe CC - BY - SA

source images : production personnelle.
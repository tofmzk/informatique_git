# Le tri par insertion 

# Algorithme



- Dans l'algorithme, on parcourt un tableau à trier du début à la fin.

-  Au moment où on considère le *i*-ème élément, les éléments qui le précèdent sont déjà triés.

- L'objectif d'une étape est d'insérer le *i*-ème élément à sa place parmi ceux qui précèdent. Il faut pour cela trouver où l'élément  doit être inséré en le comparant aux autres, puis décaler les éléments  afin de pouvoir effectuer l'insertion. 

	

**Remarque :**

L'algorithme est indépendant du type des éléments que l'on doit trier. Ce type décidera du comparateur à utiliser.





**Exemple : Considérons la liste 6, 5, 3, 1, 8, 7, 2, 4**. 

Le comparateur est  `<`.

<table>
 <tr>
  <td>i = 1&nbsp;:&nbsp;
  <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid red">5<td style="width: 1.3em; border: 2px solid white">3<td style="width: 1.3em; border: 2px solid white">1<td style="width: 1.3em; border: 2px solid white">8<td style="width: 1.3em; border: 2px solid white">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <td>&nbsp;⟶&nbsp;
 <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid red">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid white">3<td style="width: 1.3em; border: 2px solid white">1<td style="width: 1.3em; border: 2px solid white">8<td style="width: 1.3em; border: 2px solid white">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <tr>
  <td>i = 2&nbsp;:&nbsp;
  <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid red">3<td style="width: 1.3em; border: 2px solid white">1<td style="width: 1.3em; border: 2px solid white">8<td style="width: 1.3em; border: 2px solid white">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <td>&nbsp;⟶&nbsp;
 <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid red">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid white">1<td style="width: 1.3em; border: 2px solid white">8<td style="width: 1.3em; border: 2px solid white">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <tr>
  <td>i = 3&nbsp;:&nbsp;
  <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid red">1<td style="width: 1.3em; border: 2px solid white">8<td style="width: 1.3em; border: 2px solid white">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <td>&nbsp;⟶&nbsp;
 <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid red">1<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid white">8<td style="width: 1.3em; border: 2px solid white">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <tr>
  <td>i = 4&nbsp;:&nbsp;
  <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">1<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid red">8<td style="width: 1.3em; border: 2px solid white">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <td>&nbsp;⟶&nbsp;
 <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">1<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid red">8<td style="width: 1.3em; border: 2px solid white">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <tr>
  <td>i = 5&nbsp;:&nbsp;
  <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">1<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid black">8<td style="width: 1.3em; border: 2px solid red">7<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <td>&nbsp;⟶&nbsp;
 <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">1<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid red">7<td style="width: 1.3em; border: 2px solid black">8<td style="width: 1.3em; border: 2px solid white">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <tr>
  <td>i = 6&nbsp;:&nbsp;
  <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">1<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid black">7<td style="width: 1.3em; border: 2px solid black">8<td style="width: 1.3em; border: 2px solid red">2<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <td>&nbsp;⟶&nbsp;
 <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">1<td style="width: 1.3em; border: 2px solid red">2<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid black">7<td style="width: 1.3em; border: 2px solid black">8<td style="width: 1.3em; border: 2px solid white">4
   </table>
 <tr>
  <td>i = 7&nbsp;:&nbsp;
  <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">1<td style="width: 1.3em; border: 2px solid black">2<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid black">7<td style="width: 1.3em; border: 2px solid black">8<td style="width: 1.3em; border: 2px solid red">4
   </table>
 <td>&nbsp;⟶&nbsp;
 <td>
   <table cellspacing=0 style="text-align:center">
    <tr><td style="width: 1.3em; border: 2px solid black">1<td style="width: 1.3em; border: 2px solid black">2<td style="width: 1.3em; border: 2px solid black">3<td style="width: 1.3em; border: 2px solid red">4<td style="width: 1.3em; border: 2px solid black">5<td style="width: 1.3em; border: 2px solid black">6<td style="width: 1.3em; border: 2px solid black">7<td style="width: 1.3em; border: 2px solid black">8
   </table>
</table>



La même chose avec une animation :

![animation - source wikipédia -GNU](https://upload.wikimedia.org/wikipedia/commons/0/0f/Insertion-sort-example-300px.gif)



# Terminaison et correction



* La **terminaison** de l'algorithme est le fait que l'algorithme se termine bien et, donc, ne boucle sans fin.
    *  Ce qui nous permet d'être  certain de la **terminaison**  est appelé **variant de boucle**. Il s'agit d'un nombre entier qui décroit strictement et garantit la sortie de la boucle lorsqu'il atteint 0.
    *  Ici le **variant de boucle** est la longueur de la liste qui reste à trier : cette longueur est un nombre entier qui diminue de 1 à chaque tour et atteindra donc fatalement 0 en sonnant la fin du tri en garantissant la **terminaison** de l'algorithme.





* La **correction** de cette algorithme est l'assurance qu'il réalise bien ce pourquoi il est fait.
*  Ce qui garantit cette correction est appelée **invariant de boucle** : c'est une propriété qui doit rester vraie du début à la fin de l'algorithme
* Ici, notre **invariant de boucle** est le fait que la partie de la liste comprise entre l'élément 0 et l'élément _i_ est toujours correctement triée.





# Coût de l'algorithme



Trier une liste de n éléments nécessite donc n - 1 étape. Mais, chaque étape nécessite un certain nombre de comparaison. C'est ce nombre qui détermine l'efficacité du tri : plus il est faible, plus l'algorithme est efficace.



**Reprenons notre exemple :  le tableau  [6, _5_, 3, 1, 8, 7, 2, 4]** 

*  On commence par comparer 5 avec 6 ce qui conduit à placer 5 à gauche du 6 -> 1 comparaison

	​	**On obtient [5, 6, _3_, 1, 8, 7, 2, 4]**

* On compare 3 à 6 puis 3 à 5 pour placer 3 en tête de liste -> 2 comparaisons

	​	**On obtient [3, 5, 6, _1_, 8, 7, 2, 4]**

* On continue en comparant 1 à 6 puis 1 à 5 puis 1 à 3 -> 3 comparaisons

	​	**On obtient [1, 3, 5, 6,  _8_, 7, 2, 4]**

* On compare 8 à 6 : **8 est à sa place, inutile de continuer !!** -> une seule comparaison et rien ne bouge

	**On obtient [1, 3, 5, 6,  8, _7_, 2, 4]**

* On compare 7 à 8 puis 7 à 6 : **7 est à sa place, inutile de continuer !!** -> deux comparaisons

	**On obtient [1, 3, 5, 6,  7, 8 , _2_, 4]**

* On compare 2 à 8 puis 2 à 7, puis  2 à 6 puis 2 à 5 puis 2 à 3 puis 2 à 1 -> 6 comparaisons

	**On obtient [1, 2, 3, 5, 6, 7, 8, _4_]**

* On compare 4 à 8 puis 4 à 7 puis 4 à 6 puis 4 à 5 puis 4 à 3 : **4 est à sa place !** -> 5 comparaisons

	**On obtient [1,2,3,4,5,6,7,8] : c'est terminé !**

	

	Nous avons effectué 1 + 2 + 3 + 1 + 2 + 6 + 5 = 20 comparaisons 

	

	Lors d'un [tri par sélection](tri par selection.md), nous avons vu que le nombre de comparaisons pour un tableau de taille 8 serait $$ \frac {8 * 7}{2} = 28$$  , c'est à dire **8 de plus que le tri par insertion qui est donc plus performant !!**

	

	Cela s'explique par le fait que le nombre de comparaisons nécessaires avec le tri par sélection est toujours le même alors que, avec le tri par insertion, si un élément est à sa place, c'est à dire si le tableau est déjà un peu trié, le  nombre de comparaisons nécessaires diminue !



> Lors du tri par insertion d'un tableau de n éléments, le nombre de comparaison est au maximum de $$\frac {n ( n-1)}{2}$$ (quand le tableau est à 100% désordonnée) et au minimum de _n - 1_ (si le tableau est déjà totalement ordonné).
>
> Le tri par insertion est donc **au pire** équivalent en terme de performance au tri par sélection mais, la plupart du temps, dès que quelques éléments sont au départ déjà près de leur place, il sera bien meilleur.
> 
> 
> 
> L'ordre de grandeur du nombre de comparaison est au pire  _n²_  et au :ieux _n_: on dit que sa **complexité est au pire quadratique** (aussi noté  **_O(n²)_**) et au mieux **linéaire** (aussi noté **_O(n)_**)
>



# Implémentation en Python



Ouvrez votre fichier _tris.py_ et ajouter le code ci-dessous en le complétant. On utilisera les opérateurs déjà codés.

```PYTHON
#####################################
#### TRI PAR INSERTION
#####################################    
def inserer(tab, i, comp = comp_defaut):
    '''
    insère l'élément tab[i] à sa place dans la tranche tab[0:i+1]
    de sorte que cette tranche reste triée si tab[0:i] l'est auparavant
    :param tab (list)
    :param i (int) indice de l'élément de tab à insérer dans entre les élts d'indice 0 et i	  :param comp (function) une fonction de comparaison
    :Effet de bord sur tab
    >>> t = [1, 2, 4, 5, 3, 7, 6]
    >>> inserer(t, 4)
    >>> t == [1, 2, 3, 4, 5, 7, 6]
    True
    >>> inserer(t, 5)
    >>> t == [1, 2, 3, 4, 5, 7, 6]
    True
    >>> inserer(t, 6)
    >>> t == [1, 2, 3, 4, 5, 6, 7]
    True
    '''
	

def tri_insertion(tab, comp = comp_defaut) :
    '''
    modifie par effet de bord le tableauen la triant grâce au comparateur
    passé en paramètre
    : param tab (list) le tableau à trier
    : param comp (function)
	: effet de bord sur tab
    >>> t = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_insertion(t)
    >>> t == [1, 1, 2, 3, 4, 5, 9]
    True
    '''
    
```



## _Rapidité_ de traitement

Nous avons déjà démontré que le tri par insertion utilise moins de comparaison que le tri par selection. Comment cela se traduit-il en terme de _rapidité ?_



On va se servir du Scrabble pour comparer le tri insertion et le tri selection en terme de temps d'exécution. Il faut avoir conscience que cette méthode n'est pas forcément la meilleure pour mesurer l'efficacité d'un algorithme car elle dépend de la machine sur laquelle il est éxécuté :

* Quelle est la puissance de calcul de cette machine ?
* Quelles tâches executente-t-elle en arrière plan ?



Nous allons importer le module `time` pour comparer les temps d'exécution dans notre mini-projet `scrabble.py`.



Testez la fonction ci-dessous avec nos deux algorithmes de tris `tris.insertion` et `tris.selection` :

```python
impor time # à ajouter en haut du code

def chronometrer(tri):
    '''
    : param tri (function) un tri
    : return (float) le temps en secondes
    '''
    liste_mots = chercher_mots('AEIOUMNPLST') # liste de lettres quelconque
    debut = time.time()
    tri(liste_mots, comp_pt_scrabble)
    fin = time.time()
    return fin - debut

```



Depuis la console :

```python
>>> chronometrer(tris.tri_selection)
???
>>> chronometrer(tris.tri_insertion)
??
```





**Conclusion ?**





_____

Par Mieszczak Christophe CC - BY - SA

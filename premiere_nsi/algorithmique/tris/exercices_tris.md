# Exercices sur les tris



## Dans `tris.py`



Dans cette série d'exercice, on se placera dans le code de notre fichier `tris.py`



**Ex 1** : tab = [2, -4, 5, 6, 7, 12, 19, -3, 12, 27]

* En utilisant l'opérateur `comp_defaut` , triez ce tableau.

	

**Ex 2 :** tab = ['BONJOUR', 'HI',  'GUTENTAG', 'SALAM', 'BUENOSDIAS',  'AYO']

* Testez dans la console les instructions ci-dessous. Qu'en déduisez-vous ?

	````python
	>>> comp_defaut('A','B')
	???
	>>> comp_defaut('BONJOUR', 'BONSOIR')
	???
	>>> comp_defaut('HELLO', 'HALLO')
	???
	>>> comp_defaut('a', 'A')
	???
	````

	

* Triez le tableau.

	

**Ex 3 :** tab = ['BONJOUR', 'HI',  'GUTENTAG', 'SALAM', 'BUENOSDIAS',  'AYO']

* Réalisez un comparateur `comp_longueur_mot` permettant de trier cette liste selon la longueur des mots.

  ``` python
  def comp_longueur_mot(elt1, elt2):
      '''
      si elt1 et elt2 sont rangés par ordre croissant alors renvoie -1 
      sinon si elt1 et elt2 sont rangés par ordre décroissant renvoie 1 
      sinon renvoie 0
      : param elt1, elt2 (str) 
      : return (int)
      >>> comp_longueur_mot('bonjour', 'hi')
      1
      >>> comp_longueur_mot('hello', 'bonjour')
      -1
      >>> comp_longueur_mot('hello', 'hallo')
      0
      '''
  ```

  

* Triez ce tableau !

**Ex 4 :**

 On dispose d'un jeu de carte modélisé par le tableau ci-dessous :

```python
JEU = [ "1♥", "2♥", "3♥", "4♥", "5♥", "6♥","7♥","8♥","9♥","dix♥","V♥","D♥","R♥",
        "1♦", "2♦", "3♦", "4♦", "5♦", "6♦","7♦","8♦","9♦","dix♦","V♦","D♦","R♦",
        "1♣", "2♣", "3♣", "4♣", "5♣", "6♣","7♣","8♣","9♣","dix♣","V♣","D♣","R♣",
        "1♠", "2♠", "3♠", "4♠", "5♠", "6♠","7♠","8♠","9♠","dix♠","V♠","D♠","R♠"]
```

1. On souhaite selon la valeur de la carte sachant qu'un AS vaut 1 point, un 2 vaut deux points ... un roi vaut 13 points.

    * Coder le comparateur adéquate
    * Triez ce JEU !

2. On souhaite selon la valeur de la carte mais en triant également les _couleurs_ dans l'ordre  coeur, carreau, trèfle puis pique.

    * Coder le comparateur adéquate
    * Triez ce JEU !

    

## Retour au SCRABBLE



Nous pouvons maintenant aborder la deuxième partie du projet [SCRABBLE](../../projets/scrabble/scrabble.md)



_____

Par Mieszczak Christophe CC - BY - SA
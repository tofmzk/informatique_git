# Le tri par sélection 

## Algorithme



Sur un tableau de *n* éléments numérotés de 0 à _n - 1_ .

Le principe du tri par sélection est le suivant :

Pour _i_ allant de 0 à  _n_ - 2 :

- Rechercher l'indice _i_min_ du plus petit élément de la partie du tableau allant de l'indice _i_ à l'indice _n_ - 1 en utilisant un comparateur adapté.

- Échanger les éléments d'indice _i_ et _i_min_.

    

**Remarque importante :**

L'algorithme est indépendant de la nature des éléments que l'on doit trier. Cette nature décidera uniquement du comparateur à utiliser.



**Exemple : Considérons le tableau [8, 5, 2, 6, 9, 3, 1, 4, 0, 7] de taille  10.**

* Ses éléments son numérotées de 0 à _n_ - 1 = 9.
* Le comparateur est `<`



_Etape 1 : i_ = 0

* le plus petit élément de la liste entre les indice _i_ = 0 et _n_ - 1 = 9 est 0 , placé au rang 7.

* on échange donc le rang i = 0 avec le rang 7

	**On obtient  [0, 8, 5, 2, 6, 9, 3, 1, 4, 8, 7]**



_Etape 2 : i = 1_

* le plus petit élément de la liste entre les indice _i_ = 1 et _n_ - 1 = 9  est 1, placé au rang 7.

* on échange donc le rang 1 avec le rang 7.

	**On obtient  [0, 1, 5, 2, 6, 9, 3, 8, 4, 8, 7]**



_Etape 3 : i_ = 2

* le plus petit élément de la liste entre les indice _i_ = 2 et _n_ - 1 = 9 est 2, placé au rang 3.

* on échange donc le rang i=2 avec le rang 3 :

	**On obtient  [0, 1, 2, 5, 6, 9, 3, 8, 4, 8, 7]** etc .........

	

Voici une petite animation qui résume tout cela :



![animation - source wikipédia - CC by SA](https://upload.wikimedia.org/wikipedia/commons/9/94/Selection-Sort-Animation.gif)



## Terminaison et correction



* La **terminaison** de l'algorithme est le fait que l'algorithme se termine bien et, donc, ne boucle sans fin.
*  Ce qui nous permet d'être  certain de la **terminaison**  est appelé **variant de boucle**. Il s'agit d'un nombre entier qui décroit strictement et garantit la sortie de la boucle lorsqu'il atteint 0.
*  Ici le **variant de boucle** est la longueur de la liste qu'il reste à trier : cette longueur est un nombre entier qui diminue de 1 à chaque tour et atteindra donc fatalement 0 en sonnant la fin du tri en garantissant la **terminaison** de l'algorithme.

   

   

* La **correction** de cet algorithme est l'assurance qu'il réalise bien ce pourquoi il est fait.
*  Ce qui garantit cette correction est appelée **invariant de boucle** : c'est une propriété qui doit rester vraie du début à la fin de l'algorithme
* Ici, notre **invariant de boucle** est le fait que la partie du tableau comprise entre l'élément 0 et l'élément _i_ est toujours correctement triée.





## Coût de l'algorithme



Trier un tableau de n éléments nécessite donc n - 1 étape. Mais, chaque étape nécessite un certain nombre de comparaisons. C'est ce nombre qui détermine l'efficacité du tri : plus il est faible, plus l'algorithme est efficace.



**Reprenons notre exemple :  le tableau [8, 5, 2, 6, 9, 3, 1, 4, 0, 7]  de taille 10.**

* Lors de l'étape 1, on recherche le plus petit élément entre les indices 0 et 9 : on réalise donc 9 comparaisons.

* Lors de l'étape 2, on recherche le plus petit élément entre les indices 1 et 9 : on réalise 8 comparaisons.

* Lors de l'étape 3, on recherche le plus petit élément entre les indices 2 et 9 : on réalise 7 comparaisons

* [...]

* Lors de l'étape 8, on on recherche le plus petit élément entre les indices 7 et 9 : on réalise 2 comparaisons

* Lors de l'étape 9, on on recherche le plus petit élément entre les indices 8 et 9 : on réalise 1 comparaisons

	

	**Le nombre de comparaisons est donc $`9 + 8 + 7 + 6 + 5 + 4 + 3 + 2 + 1 = \frac {9 * 10} {2} = 45`$ comparaisons**

	



> Lors du tri par sélection d'un tableau de n éléments :
>
> * le premier élément sera comparé à _n_ - 1 éléments
> * le second élément sera comparé à _n_ - 2 éléments
> * le troisième élément sera comparé à _n_ - 3 éléments
> * ..
> * le _n_-1 et dernier élément sera comparé à _n_ - (_n_ - 1) = 1 élément 
>
> **Dans tous les cas**, le nombre de comparaisons est donc  :
>
> $` 1 + 2 + 3 + 4 + ... + (n - 3 ) + ( n - 2) + (n - 1 ) = \frac {n ( n-1)}{2} = \frac {n² - n} {2}`$ 
>
> (Cf [les suites arithmétiques](https://fr.wikipedia.org/wiki/Suite_arithm%C3%A9tique))
>
> L'ordre de grandeur du nombre de comparaison est _n²_ : on dit que sa **complexité est quadratique** ou que sa complexité est **_O(n²)_**.



## Implémentation en Python



Copiez le code ci-dessous avec _Thonny_. Sauvegardez le sous le nom _tris.py_ et complétez le ! 

```PYTHON
#####################################
#### LES COMPARATEURS
#####################################

def comp_defaut(elt1, elt2):
    '''
    si elt1 et elt2 sont rangés par ordre croissant alors renvoie -1 
    sinon si elt1 et elt2 sont rangés par ordre décroissant renvoie 1 
    sinon renvoie 0
    : param elt1, elt 2 (?)
    >>> comp_defaut(2, 3)
    -1
    >>> comp_defaut(3, 2)
    1
    >>> comp_defaut(3,3)
    0
    '''
    if elt1 < elt 2 : # ordre croissant
        return -1
    elif elt 1 > elt2 : # ordre décroissant
        return 1
    else :
        return 0
        
#####################################
#### TRI PAR SELECTION
#####################################    

def minimum(tab, comp = comp_defaut ):
    '''
    renvoie l'indice de l'élement minimum du tableau en utilisant
    le comparateur passé en paramètre
    : param tab (list)
    : param comp (function)
    : return (int) l'indice du minimum
    >>> minimum([4, 5, -7, 8, 0, 2])
    2
    '''
    
    
def echanger(tab, indice1, indice2):
    '''
    echange les élements d'indices indice1 et indice2 du tableau
    : param tab (list)
    : params indice1, indice2 (int)
    : effet de bord sur list
    >>> t = [2, 3, 4, 5, 6]
    >>> echanger(t, 0, 3)
    >>> t == [5, 3, 4, 2, 6]
    True
    '''
    

def tri_selection (tab, comp = comp_defaut) :
    '''
    modifie par effet de bord le tableau en la triant grâce au comparateur 
    passé en paramètre
    : param tab (list)
    : param comp (function)
    : effet de bord sur tab
    >>> t = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_selection(t)
    >>> t == [1, 1, 2, 3, 4, 5, 9]
    True
    '''
     	
    
    
################################################
###### doctest : test la correction desfonctions
################################################
if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = False)
    
```



_____

Par Mieszczak Christophe CC - BY - SA

_source image wikipédia (CC BY SA, auteur : [en:Joestape89](https://en.wikipedia.org/wiki/Joestape89))_


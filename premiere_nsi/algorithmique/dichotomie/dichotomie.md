# Dichotomie

## Trouver un élément dans un tableau



Nous disposons d'un tableau trié (et donc d'un comparateur permettant ce tri). Il s'agit de rechercher une valeur éventuellement présente dans le tableau.



La première technique, que nous avons déjà vu plus tôt en travaillant sur les tableaux indexés,  consiste à simplement parcourir le tableau à partir de son premier élément tant que l'élément parcouru n'est pas celui recherché. 



Créez un fichier `dichotomie.py` puis copiez-y et complétez la fonction suivante :

```python
def recherche_par_parcours(elt, tab):
    '''
    renvoie l'indice de l'elt dans le tableau si l'elt y est présent et -1 sinon
    : param elt (?) un elt présent dans le tableau ou pas.
    : param tab(list)
    : return (int) l'indice de l'elt ou -1
    '''
    i = 0
    # on utilise un while pour limiter le nbre de comparaisons
    while i < len(tab) and tab[i] != elt :

```



 Si le tableau comporte _n_ éléments, le nombre  de comparaisons est d'au plus _n_ et la complexité de cet algorithme est au plus linéaire c'est à dire d'une complexité _o(n)_ . 



Mais cette technique n'utilise pas une caractéristique essentielle du tableau dont nous disposons : **le fait qu'il soit trié !**



# Utilisons l'ordre



Nous disposons d'un comparateur (appelons le `comp`) et d'un tableau (appelons le _tab_) de taille _n_ et trié par ordre croissant.



Pour rappel, `comp(elt1, elt2)` renvoie :

* -1 si _elt1 < elt2_ 
* 1 si _elt1 > elt2_
* 0 sinon.



Soit _elt_a_trouver_ la valeur à rechercher dans _tab_. 



Soit  _i_ un indice quelconque entre 0 et _n_ -1. **Comme _tab_ est trié** :

* si `comp(elt_a_trouver, tab[i])` vaut -1, alors, l'indice de _elt_a_trouver_ est plus petit que _i_ c'est à dire entre 0 et _i_ - 1 inclus.
* sinon, si  `comp(elt_a_trouver, tab[i])` vaut 1, alors, l'indice de _elt_a_trouver_ est plus grand que _i_ c'est à dire entre _i_ + 1 et _n_ - 1 inclus.
* sinon, on a trouvé notre élement et son indice est _i_ .



_Exemple :_

Considérons le tableau numérique trié  _tab_ = [1, 3, 4, 6, 7, 8, 10, 13, 14] . Nous utiliserons simplement le comparateur `<` afin de faciliter la rédaction de cette première recherche dont **le but est de trouver la position de la valeur 4 dans le tableau**. 



| valeurs |  1   |  3   |  4   |  6   |  7   |  8   |  10  |  13  |  14  |
| :-----: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: |
| indices |  0   |  1   |  2   |  3   |  4   |  5   |  6   |  7   |  8   |



* Si on prend, par exemple, _i_ = 6 :
    * _tab[6]_ = 10 : on a 4 < _tab[6]_ et par conséquent l'élement 4 se situe avant tab[6]. Donc l'indice de 4 est compris entre 0 et 6 - 1 = 5 inclus.



* Si on prend, par exemple, _i_ = 1 :
    * _tab[1]_ = 3 : on a _tab[1]_ < 4 et par conséquent l'élément 4 se situe après tab[1]. Ainsi l'indice de 4 est compris entre 1 + 1 = 2 et 5 inclus.



Reste à optimiser le choix de la valeur de _i_.





# Algorithme de recherche dichotomique



Pour pour optimiser la recherche, nous allons choisir l'indice _i_ au milieu de la plage d'indices.



_Reprenons l'exemple précédent, le tableau de taille 9 où nous recherchons la valeur 4:_



![exemple - wikipédia -public domain](https://upload.wikimedia.org/wikipedia/commons/f/f7/Binary_search_into_array.png)



* Étape 1 : on recherche l'élément entre les indices 0 et 8	

    * on prend _i_ = (0 + 8 ) // 2 = 4 (pour rappel `//` est l'opérateurqui donne le quotient de la division entière)

    * _tab[4]_ = 7 

    * 4 < 7 donc l'indice de 4 est compris entre 0 et _i_-1 = 3 inclus.

        

* Étape 2 : on recherche l'élément entre les indices 0 et 3

    * on prend _i_ = (0 + 3) // 2 = 1

    * _tab[1]_ = 3

    * 3 < 4 donc l'indice de 4 est compris entre _i_+1= 2 et 3 inclus.

        

* Étape 3 : on recherche l'élément entre les indices 2 et 3

    * on prend _i_ = (2 + 3) // 2 = 2
    * _tab[2]_ = 4 : on a trouvé notre élément !!



_Exemple d'une liste non numérique_ :

Considérons la liste _Mots_ = ['AYO', 'BONJOUR', 'BUNEOSDIAS', 'GUTENTAG', 'HELLO', 'HI','SALAM'] triée par ordre alphabétique grâce à un comparateur que nous appelerons `comp_alpha`.



Effectuez pas à pas la recherche dichotomique permettant de déterminer l'indice de 'HI' dans la liste.



* Étape 1 : les indices vont de 0 à 6 :
  * _i_ = (0 + 6 ) // 2 = 3 -> 'GUTENTAG'
  * comp_alpha('GUTENTAG', 'HI') renvoie 1 : l'indice de 'HI' est après celui de 'GUTENTAG' cad entre 4 et  6 inclus
* Étape 2 : on recherche entre les indices 4 et 6 inclus
  * _i_ = ( 4 + 6) // 2 = 5   -> ' HI'
  * comp_alpha('HI', 'HI') renvoie 0 -> c'est fin : l'indice de 'HI' est 5



_Remarque :_

* On a trouvé 'HI' en 2 étapes !

* Au maximum, pour cette liste de 6 éléments, il aurait fallu 3 étapes :

  * A la première on a le choix entre 3 indices (3 à gauche et 3  à droite)

  * A la seconde entre 2

  * A la dernière il n'en reste qu'un !

    

    

# Rédigeons l'algorithme



Soit _tab_ une liste de taille _n_ triée par ordre croissant, grâce à un comparateur `comp`, dans laquelle on recherche l'indice de l'élément _elt_. 

Ecrivez l'algorithme de recherche dichotomique en langage courant :

```txt

```





# Terminaison et correction

* Pour prouver la terminaison de l'algorithme, il nous faut trouver un **variant de boucle**, c'est à dire un entier positif strictement décroissant qui garantit la fin de la boucle lorsqu'il atteint 0. Quel est ce variant ?

    

```txt

```





* Pour garantir la correction de l'algorithme, il nous faut trouver un **invariant de boucle** c'est à dire une propriété qui est vraie à l'initialisation de la boucle est reste vraie à chaque étape. Quel est cet invariant ?

    

```txt

```







# Complexité



* Pour étudier la complexité, nous allons nous intéresser au nombre d'itérations maximum nécessaires pour un tableau de taille _n_ dans le cas le plus défavorable (la valeur recherchée n'est pas dans le tableau).  

* Sachant qu'à chaque itération de la boucle on divise la zone de recherche par 2, cela revient donc à se demander combien de fois faut-il diviser la liste en 2 pour obtenir, à la fin, un unique élement. Autrement dit, combien de fois faut-il diviser _n_ par 2 pour  obtenir 1 ? 		

    Mathématiquement cela se traduit par l'équation $`\frac {n}{2 ^ a} = 1`$ où a est le nombre inconnu de fois qu'il faut diviser _n_ par 2 pour obtenir 1. Il faut donc trouver a ! 		

    

* Il est nécessaire d'introduire une nouvelle notion mathématique : le "logarithme base 2", noté $`log_2`$. Par définition, pour tout réel $`x`$, : $`log_2 (2^x) = x`$. Ainsi : 
	$`\frac {n}{2^a} = 1 \Leftrightarrow 2^a = n \Leftrightarrow a = log_2(n)`$			



>  Nous pouvons donc dire que la complexité en nombre de comparaison dans le pire des cas de l'algorithme de recherche dichotomique est _O_($`log_2(n)`$)



_Remarque :_

Afin de visualiser les différentes complexités en fonction de _n_, voici un graphique représentant le nombre de comparaisons pour une complexité _o(n²)_ (en rouge), celle du tri par sélection par exemple, une complexité au _o(n)_ (en vert), celle du parcours complet d'une liste par exemple, et la complexité _O($`log_2(n)`$)_ (en bleu) de la dichotomie.



<img src="media\complexité.jpg" alt="complexité - source perso" style="zoom:50%;" />



# Implémentation en Python



Voici un code contenant la recherche par parcours, deux comparateurs et la définition de la fonction `dichotomie`. PLacez le tout à la suite de votre fichier `dichotomie.py` et complétez la fonction `dichotomie` .

```python
#####################################
### RECHERCHE PAR PARCOURS
#####################################
def recherche_par_parcours(elt, tab):
    '''
    renvoie l'indice de l'elt dans la liste si l'elt y est présent et -1 sinon
    : param elt : un elt présent dans le tableau ou pas.
    : param tab(list)
    : return (int) l'indice de l'elt ou -1
    '''
    i = 0
    # on utilise un while pour limiter le nbre de comparaisons
    while i < len(tab) and tab[i] != elt :
		i = i + 1
    if i < len(liste) : 
        reponse = i
    else : 
        reponse = -1
    return reponse

#####################################
#### LES COMPARATEURS
#####################################

def comp_defaut(elt1, elt2):
    '''
    si elt1 et elt2 sont rangés par ordre croissant alors renvoie -1 
    sinon si elt1 et elt2 sont rangés par ordre décroissant renvoie 1 
    sinon renvoie 0
    param elt1, elt 2 deux élément de la liste
    type int ou float
    >>> comp_defaut(2, 3)
    -1
    >>> comp_defaut(3, 2)
    1
    >>> comp_defaut(3,3)
    0
    '''
    if elt1 < elt2 : # ordre croissant
        return -1
    elif elt1 > elt2 : # ordre décroissant
        return 1
    else :
        return 0
    
def comp_longueur_mot(elt1, elt2):
    '''
    si elt1 et elt2 sont rangés par ordre croissant alors renvoie -1 
    sinon si elt1 et elt2 sont rangés par ordre décroissant renvoie 1 
    sinon renvoie 0
    param elt1, elt 2 deux éléments de type str
    >>> comp_longueur_mot('bonjour', 'hi')
    1
    >>> comp_longueur_mot('hello', 'bonjour')
    -1
    >>> comp_longueur_mot('hello', 'hallo')
    0
    '''
    if  len(elt1) < len(elt2): # ordre croissant
        return -1
    elif len(elt1) > len(elt2) : # ordre décroissant
        return 1
    else :
        return 0
    
################################################
###### dichotomie
################################################

def dichotomie(elt, tab, comp = comp_defaut):
    '''
    renvoie l'indice de l'elt dans le tableau, si l'elt y est présent, en procédant par dichotomie et -1 sinon
    : param tab (list) nu tableau trié
    : param elt (?) un elt de la liste
    : return (int) un indice ou -1
    >>> tab = [1, 3, 4, 6, 7, 8, 10, 13, 14]
    >>> dichotomie(3, tab)
    1
    >>> dichotomie(1, tab)
    0
    >>> dichotomie(14, tab)
    8
    >>> dichotomie (0, tab)
    -1
    >>> mots = ['AYO', 'BONJOUR', 'BUNEOSDIAS', 'GUTENTAG', 'HELLO', 'HI','SALAM']
    >>> dichotomie('AYO', mots)
    0
    >>> dichotomie('SALAM', mots)
    6
    >>> dichotomie ('HALLO', mots)
    -1
    '''
    
    

    
################################################
###### doctest : test la correction desfonctions
################################################
if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = False) 

```



# Chronométrage



On rappelle qu'il faut avoir conscience que cette méthode n'est pas forcément la meilleure pour mesurer l'efficacité d'un algorithme car elle dépend de la machine sur laquelle il est exécuté :

* Quelle est la puissance de calcul de cette machine ?
* Quelles tâches exécute-t-elle en arrière plan ?



Retournons un moment dans notre projet `scrabble.py`  dans lequel nous dispsons d'un tableau `LISTE_COMPLETE` contenant une très longue liste de mots.

Après avoir importer `dichotomie` et le module `time`, ajoutons la fonction ci-dessous :

```python
def chrono_dicho(nbre_repetitions):
    '''
    compare le temps mis par la recherche par parcours et la recherche par dichotomie
    pour nbre_repetitions recherche
    : param nbre_repetitions(int)
    : return (tuple) le tuple des temps 
    '''
    LISTE_COMPLETE.sort() #tri LISTE_COMPLETE
    temps_total1 = 0 # temps total par parcours
    temps_total2 = 0 # temps total par dicho
    for i in range(nbre_repetitions):
        mot = random.choice(LISTE_COMPLETE) # choisi au hasard le mot à rechercher
        debut1 = time.time()        
        indice = recherche_par_parcours(mot, LISTE_COMPLETE)
        fin1 = time.time()
        temps_total1 += fin1 - debut1
        debut2 = time.time()
        indice = dichotomie.dichotomie(mot, LISTE_COMPLETE)
        fin2 = time.time()
        temps_total2 += fin2 - debut2
    return temps_total1 , temps_total2
```



* Que fait cette fonction ? 
* Testez la fonction :

```python
>>> chrono(100)
???
>>> chrono(1000)
???
```









___

Par Mieszczak Christophe CC- BY - SA

sources images :

* [dichotomie : wikipedia, domaine public](https://commons.wikimedia.org/wiki/File:Binary_search_into_array.png)
* courbes  : production personnelle
# Exercices : les bases

### Exercice : types de base

Ecrivez dans la console les permettant de répondre aux problèmes ci-dessous et donner les résultats obtenus et leur type :

1. $`2 \times (3 + 4) ^5 = ???`$

2. $`\frac {12} 4 = ???`$

3. $` 257 = 12 \times q + r`$  que valent $`q`$ et $`r`$ ?

3. Quel est le résultat de la concaténation de 'Hello' avec 'World' ?

5. Que vaut '234' + '6' ?

4. Est-ce que le reste de la division entière de 2356 par 3 vaut 1 ?

5. Est-ce que la variable _a_ est paire ?

6. $`\Delta = b² - 4ac`$. Est-ce que  $`\Delta>0`$ lorsque $`a=4,~b=3,~c=-2`$ ?

7. Que vaut Vrai et Faux ?

10. Que vaut Vrai ou Faux ?

11. Que vaut Faux et (Vrai ou Faux) ?

    

### Exercice : entrée/sortie et chaînes de caratères

Écrivez le code d'un programme qui :

* demande "Comment vous appelez-vous ?"
* affiche 'Bonjour' suivi du nom répondu dans la console

### Exercice  : entrée/sortie et types

Écrivez le code d'un programme qui :

* demande "Comment vous appelez-vous ?"
* demande "Quel âge avez vous ?"
* affiche 'Bonjour' suivi du nom répondu dans la console et, sur la même ligne, "vous avez ... ans"
* affiche "En 2050 tu auras " suivi de l'âge qu'il aura en 2050

### Exercice : structures conditionnelles

Écrivez le code d'un programme qui :

* demande "Quel âge avez vous ?"
* affiche :
    * "vous êtes un enfant." si cet âge est inférieur ou égal à 13
    * "vous êtes un adolescent."  si cet âge est supérieur strictement à 13 inférieur strictement à 18
    * "vous êtes majeur" sinon.



Modifiez ce code en prenant en demandant au début du code "Êtes-vous un garçon ou une fille ?" 

### Exercice : discriminant

Écrivez le code d'un programme qui :

* demande "Quelle est la valeur entière de a ?"
* demande "Quelle est la valeur entière de b ?"
* demande "Quelle est la valeur entière de c ?"
* Calcule $`\Delta = b²-4ac`$
* Affiche "$`\Delta = b²-4ac = ...`$ puis sa valeur.
* affiche :
    * "il y a deux solutions" puis, à la ligne,  les valeurs de ces solutions , si $`\Delta \gt 0`$
    * "Il y a une seule solution." puis, à la ligne, cette solution, si $`\Delta = 0`$ 
    * "Il n'y a pas de solution" sinon

_Remarque :_ 

Python utilise le codage UTF-8 qui compte de nombreux caractères spéciaux comme  Δ que vous pouvez afficher directement dans la console grâce à un `print('Δ').

### Exercice : and et or

Une année est bissextile si elle est multiple de 4 et pas de 100 ou si elle est multiple de 400.

Écrire un programme qui demande une année à l'utilisateur et affiche _elle est bissextile_ ou _elle n'est pas bissextile_.

### Exercice : boucles

1. Écrivez le code d'un programme qui affiche tous les entiers de 1 à 100 inclus.
2. Écrivez le code d'un programme qui affiche tous les entiers pairs de 1 à 100 inclus.
3. Écrivez le code d'un programme qui affiche tous les entiers de 100 à 1 inclus.

### Exercice  : boucles  et table de 5

1. Écrivez un programme qui affiche la table de multiplication par 5.
2. Modifiez le code pour demander à l'utilisateur quelle table il veut afficher.

### Exercice 7 : boucle et tables de multiplication

1. Écrivez un code qui, tant que la réponse fournie est fausse, demande le résultat de la multiplication de 8 par 7 puis, lorsque la réponse est correcte, affiche 'Bravo'
2. Modifiez le code précédent pour afficher 'Bravo, vous avez réussi en ... essais'
3. Modifiez le code précédent pour que la multiplication soit le résultat de _a_ par _b_ où _a_ et _b_ sont des variables de type _int_ fixées au début du programme.
4. Modifiez le code précédent pour que les valeurs de _a_ et _b_ soit demandées à l'utilisateur au début du programme.
5. Modifiez le code précédent pour que :
    * "vous êtes très fort" si la bonne réponse est donnée du premier coup
    * "Pas mal" si la bonne réponse est donnée en 2 ou 3 coups
    * "entrainez-vous" sinon

### Exercice : boucle et chaînes

1. Écrivez un code qui :

    * demande un mot à l'utilisateur

    * écrit dans la console : "Ce mot s'épelle :"

    * puis écrit chaque lettre du mot 

2. Même question mais en épellant le mot à rebours (de la fin au début)

### Exercice : boucle et chaînes, encore

Écrivez un code qui :

* demande un mot à l'utilisateur
* crée un nouvelle chaîne de caractère vide nomme _rebours_ contenant les même caractères que le mot choisi par l'utilisateur mais à l'envers puis l'affiche dans la console.

### Exercice : boucle et structures conditionnelles

Écrivez le code d'un programme qui :

* choisit un nombre entier au hasard entre 0 et 100 inclus.
* Tant que l'utilisateur n'a pas touvé ce nombre :
    * lui demande de proposer un nombre
    * Annonce si sa proposition est trop petite ou trop grande
* Le félicite en cas de victoire en annonçant le nombre d'essais qui a été nécessaire.

_Remarque :_

On utilisera le module `random` de la façon suivante :

```python
import random # importe le module random 
nombre_a_trouver = random.randint(0, 100) #utilse la méthode randint du module random pour choisir un nombre entier au hasard entre 0 et 100 de façon équiprobable
```



### Exercice : tant que et tortue

Nous allons faire quelques exercices autour du module `turtle` de Python afin de travailler tout cela. Le module `turtle` comporte un ensemble de fonctions permettant de dessiner.



* Créez un fichier _spirale1.py_



* Importer le module `turtle` en tapant en première ligne de code la ligne ci-dessous :


 ````
import turtle
 ````



*  Voici une petite liste des fonctions importantes de la tortue de Python :
    * `turtle.forward(distance)`: Avancer d'une distance donnée  
    * `turtle.left(angle) `Tourner à gauche d'un angle donné (exprimé en degré) 
    * `turtle.right(angle)` Tourner à droite d'un angle donné (exprimé en degré) 
    * `turtle

  

  <img src="media/spirale1.jpg" alt="spirale no1" style="zoom:150%;" />

  * L'algorithme pour tracer la spirale ci-dessu est le suivant :

  

  ```
  Tant que longueur > 0 
  	la tortue avance sur une distance de longueur
  	la tortue tourne de 90° à droite
  	longueur diminue de 10
  Fin du tant que
  ```

  

1. Codez cet algorithme en Python.

2. Êtes vous certain que la boucle s'arrête à un moment donné ? Pourquoi ?

3. Lancez l'algorithme de la console.



Modifions légèrement notre algorithme :


   ```
Tant que longueur > 0 
  	la tortue avance sur une distance de longueur
  	la tortue tourne de 90° à droite
  	longueur est multipliée par 0.9
  Fin du tant que
   ```



  Créez un fichier _spirale2.py_ et importez le module _turtle_.

4. Codez cet algorithme en Python.
5. Lancez l'algorithme de la console.
6. Que se passe-t-il ? Pourquoi ? Modifiez le code pour y remédier.



### Exercice : boucle pour, tortue 



1. Créez un programme _triangles.py_. Importez le module _turtle_.

2. Écrivez un code permettant de tracer un triangle équilatéral de côté 100.

3. Modifiez ce code pour que le triangle précédent soit dessiné 36 fois de suite en tournant de 10° après chaque triangle.

4. Créez maintenant un programme _carrés.py_ et coder l'algorithme permetant de réaliser le dessin ci-dessous :

    ![carrés](media/carres.jpg)

En réalité, il serait judicieux d'utiliser ici des structures de fonctions : on fait ça juste après !



### Exercice : Tortue et fonctions

1. Reprenez votre programme _triangles.py_.

2. Insérez le code permettant de tracer un triangle dans la fonction ci-dessous :

    ```python
    def tracer_triangle():
        '''
        dessine un triangle de longueur 100
        '''
    ```

3. Testez votre code.

4. Ajouter un paramètre _longueur_ de type _int_ à cette fonction lui permettant de tracer un triangle de longueur choisie par l'utilisateur.

5. En utilisant la fonction `tracer_triangle`  et le morceau de code qui vous reste, compléter la fonction ci-dessous :

    ```python
    def effectuer_rotation():
        '''
        trace un triangle puis tourne de 10° et recommence 36 fois
        '''
    ```

6. ajouter un paramètre _nbre_de_rotations_ de type _int_ permettant à l'utilisateur de choisir l'angle de rotation et d'effectuer toujouer un tour complet (donc 360° en tout)

7. Coder, en utilisant le programme _carrés.py_, la fonction `tracer_carre()`  qui dessine un carré de côté 100.



### Exercice : Binaire vers décimal

Considérons la fonction ci-dessous :

```python
def bin_vers_dec(nbre_bin):
    '''
    renvoie le nombre entier positif en base 10 correspondant au nombre binaire passé en paramère
    : param nbre_bin (str)
    : return (int) 
    >>> bin_vers_dec('100')
    4
    >>> bin_vers_dec('0')
    ...
    >>> bin_vers_dec('11111111')
    ...
    >>> bin_vers_dec(...)
    ...
```

1. Qu'attend-t-on de cette fonction ?
2. Quel est le type du paramètre ?
3. Que vérifie le premier test ?
4. Créer un fichier _conversions.py_ et coller-y ce code
5. Complétez les trois tests manquants
6. Commencer par coder une assertion
7. Coder cet algorithme

### Exercice : Décimal vers binaire par soustraction

Considérons le code ci-dessous :

```python
def renvoie_puissance(n) :
    '''
    renvoie le plus grand entier p tel que 2^p <= n
    : param n (int) n >= 0
    : return p (int)
    >>> renvoie_puissance(9)
    3
    >>> renvoie_puissance(255)
    ...
    >>> renvoie_puissance(1)
    ...
    '''


def dec_vers_bin_soustraction(n):
    '''
    renvoie une chaine contenant la conversion en binaire de l'entier positif n
    utilise la méthode par soustraction
    : param n (int) >= 0
    : return (str)
    >>> dec_vers_bin(4)
    '100'
    >>> dec_vers_bin(255)
    ...
    >>> dec_vers_bin(0)
    ...
    >>> dec_vers_bin(...)
    ...
    '''
    
```

1. Qu'attend-t-on de ces fonction ?
2. Quel est le type du paramètre ?
3. Ouvrir le fichier _conversions.py_ et coller-y ce code à la suite.
5. Compléter les tests manquants
6. Commencer par coder les assertions (une par fonction)
7. Coder ces fonctions (attention au cas n = 0).

### Exercice : Décimal vers binaire par division

Considérons le code ci-dessous :

```python
def dec_vers_bin(n):
    '''
    renvoie une chaine contenant la conversion en binaire de l'entier positif n
    utilise la méthode par division
    : param n (int) >= 0
    : return (str)
    >>> dec_vers_bin(17)
    '10001'
    >>> dec_vers_bin(255)
    ...
    >>> dec_vers_bin(0)
    ...
    >>> dec_vers_bin(...)
    ...
    '''
    
```

1. Qu'attend-t-on de cette fonction ?
2. Quel est le type du paramètre ?
3. Que vérifie le premier test ?
4. Ouvrir le fichier _conversions.py_ et coller-y ce code à la suite.
5. Compléter les trois tests manquants
6. Commencer par coder une assertion
7. Coder cet algorithme





 

_________

Par Mieszczak Christophe

Licence CC BY SA


























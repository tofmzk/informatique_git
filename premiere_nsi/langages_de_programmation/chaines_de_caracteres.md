 # Les chaînes de caractères



Nous avons déjà parlé des chaînes de caractères, le format _str_ :

```python
>>> chaine = 'Hello World'
>>> type(chaine)
???
```



Chaque caractère de la chaîne est repéré son indice, un peu comme s'il s'agissait d'un tableau :

```python
>>> chaine[0]
???
>>> chaine[1]
??]
>>> chaine [1:]
???
>>> chaine [:3]
???
>>> chaine [2:5]
???
>>> chaine [??? : ???] ----------> les slices, pas au programme mais très pratiques
'o Wor'
```



On peut parcourir une chaîne, comme un tableau, grâce à son itérateur :

``` python
>>> for caractere in chaine :
    	print(caractere)
???

```



Une chaîne de caractère est non modifiable et non mutable, non pas comme un tableau mais comme un tuple :

```python
>>> chaine[0] = 'a'
???
```



Comme les tableaux, les chaînes de caractères possèdent [énormément de  méthodes](https://www.w3schools.com/python/python_ref_string.asp) . En voici quelques unes :

*  `lower()` et `upper()` font respectivement passer la chaîne en minuscule ou en majuscule :

    ```python
    >>> chaine.lower()
    ???
    >>> chaine.upper()
    ???
    ```

    

* `len(chain)` renvoie la longueur de la chaîne.

    ```python
    >>> len(chaine)
    ???
    ```

    

* La méthode `count(chaine_2)` renvoie le nombre d'occurrence(s) de la chaine_2 dans la chaine de caractère.

    ```python
    >>> chaine.count('d')
    ???
    >>> chaine.count('o')
    ???
    >>> chaine.count('z')
    ???
    ```

    

* La méthode `find(chaine_2)` renvoie  :

    * l'indice de départ de la première occurrence de _chaine_2_ dans la chaine, si _chaine_2_ est présent dans _chaine_
    * -1 sinon

    ```python
    >>> chaine.find('o')
    ???
    >>> chaine.find('world')
    ???
    >>> chaine.find('z')
    ???
    ```

    

* La méthode `replace(chaine_2, chaine_3)` renvoie une chaîne dont toutes les occurrences de chaine_2 dans la chaine de départ ont été remplacées par la chaine_3.

    ```python
    >>> chaine.replace('o','a')
    ???
    >>> chaine
    ???
    ```

    

* La méthode `split(separateur)` coupe la chaîne en sous chaînes selon le séparateur choisit et renvoie la liste des sous-chaînes

    ```python
    >>> chaine.split(' ')
    ???
    >>> chaine.split('o')
    ???
    >>> chaine.split('i')
    ???
    ```

    

* La méthode `strip()`, et ses acolytes `lstrip()` (l pour left) et `rstrip()`  (r pour right) , ne retirent pas la chaîne de caractères aux extrémités d’une autre chaîne de caractères. Elles retirent des extrémités toutes lettres qui sont dans la chaînes passée en paramètre. 

    ```python
    >>> chaine = 'aaooiioouueepp'
    >>> chain.strip('a')
    ???
    >>> chaine.strip('ap')
    ???
    >>> chaine.lstrip('ap')
    ???
    >>> chaine.rstrip('ap')
    ???
    ```





________________

Par Mieszczak Christophe CC BY SA
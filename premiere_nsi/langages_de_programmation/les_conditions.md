# Structures conditionnelles 



## Prototypage d'une structure conditionnelle

En Python, les structures conditionnelles s'écrivent ainsi :

```Python
if condition1 : # condition1 doit valoir True ou False c'est à dire vrai ou faux
	consequence2 # après une tabulation, on code la conséquence d'une condition vérifiée
elif condition2 : # on enlève la tabulation pour un sinon si avec une autre condition
	consequence2 # après une tabulation, on code la conséquence d'une condition vérifiée
else : # on enlève la tabulation pour un sinon, dernière possibilité de l'instruction 
	consequence3 # après une tabulation, on code la conséquence d'une condition vérifiée
	
```

_Remarques :_

* Notez les deux points `:` en fin de ligne. Ils jouent le rôle du `alors`
* Notez les tabulations, indispensables pour la structure.
* Les _conditions_ sont des booléens qui valent donc `True`ou `False`.
* `elif` est une contraction de `else if`. Il peut y en avoir plusieurs si besoin.
* `else` n' a pas besoin de condition : il sera vrai si les toutes les conditions précédentes sont fausses. On l'utilise en fin de structure.



Créez dans le répertoire de ce cours un fichier `test.py` . Collez-y le code ci-dessous et testez le en changeant les valeurs de _a_ et _b_.

````Python
a= 10 
b = 5
if a < b :
	print('a est inférieur à b')
elif a > b:
	print('a est supérieur à b')
else :
	print('a est égal à b')
		
````



_Encore une remarque :_

On retrouve à peu de chose prêt la même façon de faire dans d'autres langages de programmation :

- En Javascript :

	```javascript
	if (condition1){
	    consequence1; // la tabulation est facultative
	}
	else if (condition2){ 
	    consequence2;// la tabulation est facultative
	}
	else {
	    consequence3;// la tabulation est facultative
	}
	```

	

- En C++

	```c++
	if (condition1){
	    consequence1;// la tabulation est facultative
	}
	else if (condition2){ 
	    consequence2;// la tabulation est facultative
	}
	else {
	    consequence3;// la tabulation est facultative
	}
	```

	



> Relevez les points communs et les différences entre les trois prototypage Python/Javascript/C++





## Structures conditionnelles et booléens

Les conditions des structures conditionnelles sont du type booléen : elles valent chaque fois `True`ou `False`. On peut donc, entre conditions, utiliser les opérateurs des booléens 

### Opérateurs de comparaison

Afin de pouvoir effectuer des tests renvoyant une valeur vraie ou  fausse on dispose d'un certain nombre d'opérateurs de comparaison.

| Opérateur    |                         Description                          |
| ------------ | :----------------------------------------------------------: |
| a == b       | Opérateur d'égalité. Renvoie *True* si a et b contiennent des valeurs égales. |
| a != b       | Renvoie *True* si a et b contiennent des valeurs différentes. |
| a > b        | Renvoie *True* si la valeur de a est supérieure à la valeur de b. |
| a < b        | Renvoie *True* si la valeur de a est inférieure à la valeur de b. |
| a >= b       | Renvoie *True* si la valeur de a est supérieure ou égale à la valeur de b. |
| a <= b       | Renvoie *True* si la valeur de a est inférieure ou égale à la valeur de b. |
| c <  a < b   | Renvoie *True* si la valeur de a est supérieure ou égale à c et inférieure ou égale à b. |
| c <=  a <= b | Renvoie *True* si la valeur de a est strictement supérieure à c et inférieure à b. |



### Opérateurs logiques

On a fréquemment besoin de combiner plusieurs tests. Pour réaliser des opérations de comparaison on utilisera des opérateurs logiques.

| Opérateur |                         Description                          | Exemple         |
| --------- | :----------------------------------------------------------: | --------------- |
| and       | L'opérateur logique ET. Renvoie True si les deux conditions sont vraies (sinon False). | a < b and c > d |
| or        | L'opérateur logique OR. Renvoie True si l'une des deux conditions est vraie (sinon False). | a < b or c > d  |
| not       | L'opérateur de négation logique, NON. Renvoie True si son argument est faux et False si il est vrai. | not a < b       |



> Écrivez un code demandant trois entiers _a_ et _b_ et _c_ puis affichant dans la console selon le cas de figure, 'ordre croissant' ou 'ordre décroissant' ou 'ni croissant ni décroissant'
>
> 

#### Simplifier des expressions booléennes



Maîtriser les booléens permet bien souvent de rendre le code plus clair et plus concis. 

Regardons le code ci-dessous :

```python
if a < b :
    reponse = True
else :
    reponse =  False
print(reponse)
```

* Que fait-il ?
* Quel est le type de `a < b ` ? 
* Comment pourriez-vous réécrire ce code de façon aussi concise que possible ?









________

Par Mieszczak Christophe CC BY SA


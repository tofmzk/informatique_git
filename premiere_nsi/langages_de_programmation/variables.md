# Les variables de base et leur Type 



En Python, pour déclarer une variable, on lui donne un nom (son _identificateur_) et on lui _affecte_ une valeur qui lui confère son _type_.  

Cette façon de faire parait naturel et est utilisé dans de nombreux langages comme le _Javascript_.... Mais pas par tous. Certains langages de programmation demandent que l'on donne un nom et un type pour déclarer une variable. Cette dernière ne peut alors plus changer de type par la suite.



### Noms des variables et bonnes pratiques

Dès qu'un algorithme se complexifie, il peut utiliser de nombreuses variables.  Pour éviter les conflits de noms et rendre le code compréhensibles, il y a de bonnes pratiques : 

* Le nom s'écrit en minuscule et on utilise le tiret bas `_`  s'il est en plusieurs parties comme par exemple pour la variable `nombre_de_vies`
* On donne aux variables un nom significatif. Par exemple, une variable indiquant le nom d'un joueur s'appellera `nom_joueur`plutôt que `j`. Inutile d'avoir peur des noms longs : l'auto-complétion est là pour nous aider (touche tabulation).
* Si on utilise des constantes, on les nommera en majuscule. 



### Les entiers

Dans la console, Tapez le code ci-dessous pour déclarer la variable _a_ et demander son type.  Remplacez les `???` par la réponse obtenue.

```python
>>> nbre1 = 11
>>> type(nbre1)
>>> int
```

* Cela signifie que la variable _nbre_ est du type _int_ c'est à dire entier (_integer_ en Anglais). 

* Python sait parfaitement coder les entiers. Il peut effectuer très rapidement toute opération mathématiques entre eux, même avec de très grands nombres.

	

Dans la console, à la suite du code précédent, poursuivez :

````python
>>> nbre2 = 31
>>> nbre1 + nbre2
>>> 
>>> nbre1 * nbre2
>>> 
>>> nbre1 - nbre2
>>> 
````

Continuons avec l'opérateur puissance entière `**` . Au passage, vous aurez un aperçu de la capacité de calcul de Python avec les nombres entiers.

````Python
>>> nbre1 ** nbre2
>>> 
>>> ( nbre1 ** nbre2) ** 4
>>>
````

Vous avez remarqué, car vous êtes très perspicaces, si si, que je n'ai pas encore effectué de division. La cause en est que l'opérateur `/` , la division, donne un résultat de type différent. Testons cela :

````Python
>>> nbre1 / nbre2
>>> 
>>> type (nbre1 / nbre2)
>>> 
````

* Cela signifie que _nbre1 / nbre2_ est un nombre _flottant_ (_float_ en Anglais). Ce n'est plus un entier. 
* Les nombres _flottants_ sont des **approximations**  de nombres décimaux. En effet, le codage en binaire d'un nombre décimal entraîne le plus souvent une erreur. Une erreur très petite mais lourde de conséquence : dès qu'il calcule avec des nombres flottants, Python (comme presque tout langage de programmation) fait des approximations et il faut s'en méfier ! Nous y reviendrons un peu plus bas. Pour l'instant retournons chez les entiers.



Nous allons effectuer maintenant une division entière.  Il y a deux opérateurs importants :

*  ``//`` donne le quotient de la division entière.

* ``%`` donne le reste de la division entière.

	

Pour mémoire, voici un division entière :



![division entière](media/division_entiere.png) 



Allons-y :

````Python
>>> nbre2 // nbre1
???
>>> type(nbre2 // nbre1)
???
>>> nbre2 % nbre1
???
>>> type(nbre2 % nbre1)
???
````



Complétez : 

* le dividende est `31`.

*  le diviseur est  `11`

*  le quotient est `2` 

*  le reste `9`

	

Les opérations avec des entiers étant parfaitement exécutées, on peut les comparer sans risque d'erreur. 

Les opérateurs de comparaisons sont :

* ``==`` : est-ce égal ? _Remarquez qu'il s'agit bien d'un double égal. Le simple égal est une affectation !!_
* `>` : est-ce supérieur ?
* `>= `: est_ce supérieur ou égal ?
* `<` : est-ce inférieur ? 
* `<=`: est-ce inférieur ou égal ?



Voyons un peu cela :

````python
>>> nbre1 == nbre2
>>>
>>> nbre1 > nbre2
>>>
>>> nbre1 <= nbre2
>>> 
>>> type (nbre1 < nbre2)
>>> 
````

* `True` signifie _vrai_. 
* `False`signifie _faux_
* Une variable qui prend une de ces deux valeurs est appelée _booléenne_ (type _bool_ en Python). Nous y reviendrons plus loin.



### Retour vers les flottants



Nous avons vu plus haut que les flottants étaient des **approximations** des nombres décimaux. Regardons quelles en sont les conséquences.



````Python
>>> nbre = 1.0
>>> type(nbre)
>>> 
>>> nbre = 0.1
>>> nbre
>>>
>>> nbre + nbre + nbre 
>>> 

````

Et oui, il y a une toute petite erreur, mais elle à une conséquence importante :

````Python
>>> nbre == 0.1
>>>
>>> nbre + nbre + nbre == 0.3
>>> 
````

Et oui, _nbre + nbre + nbre_ ne fait pas exactement 0.3 et la comparaison renvoie donc `False` :



**La comparaison stricte entre deux flottants n'a aucun sens**

**On ne peut les comparer qu'approximativement **

**Heureusement, l'erreur est faible** ... 



Utilisons la fonction `round( flottant, nombre de décimales)` qui arrondit un flottant avec le nombre de décimales souhaitées entre 0 et 16. Vous en trouverez la description détaillée [ICI](https://www.w3schools.com/python/ref_func_round.asp)

````Python
>>> round (nbre + nbre + nbre , 10)
>>>
>>> round (nbre + nbre + nbre , 10) == 0.3
>>>
````



**Il faudra se souvenir de la particularité des flottants chaque fois qu'il faudra s'en servir et ne s'en servir que quand ils sont nécessaires.**



### Les chaînes de caractères



Une chaîne de caractères contient... des caractères. On la déclare en insérant sa valeur soit entre deux apostrophes `' '` soit entre deux guillemets `" "` :

````Python
>>> chaine1 = 'Hello'
>>> type(chaine1)
>>> 
>>> chaine2 = "World"
>>> type(chaine2)
>>> 
````

* Une chaîne de caractère est du type _string_ (_str_).  _String_ est la traduction littérale de _chaîne_ en Anglais. 

* pour afficher un guillemet , on met la chaîne entre apostrophe et inversement 

	````Python
	>>> chaine1 = "C'est moi !"
	>>> chaine1
	>>> 
	>>> chaine2='il me dit "Bonjour !"'
	>>> chaine2
	>>> 
	````

	

Il est possible d'ajouter deux chaînes entre elles. On fait alors une **concaténation**. L'opérateur est le `+`. Attention, contrairement à l'addition entre entiers ou flottants, l'ordre a son importance !

````Python
>>> chaine1 = ' je suis ' 
>>> chaine2 = ' yoda '
>>> chaine1 + chaine2
>>>
>>> chaine2 + chaine1
>>> 
````



On **ne** peut **pas** ajouter une chaîne et un nombre !

````Python
>>> nbre = 4
>>> 'A' + nbre
>>> 
````



En revanche on peut convertir un nombre en chaîne en utilisant la fonction `str(nombre)` détaillée [ICI](https://www.w3schools.com/python/ref_func_str.asp)

```Python
>>> chaine='1'
>>> str(4)
>>> 
>>> chaine + str(4)
>>> 
```



Quand c'est possible, on peut aussi convertir une chaîne en entier ou en flottant en utilisant soit la fonction ``int(chaine)`` détaillée [ICI](https://www.w3schools.com/python/ref_func_int.asp) soit la fonction `float(chaine)`détaillée [LA](https://www.w3schools.com/python/ref_func_float.asp).



Tapez le code ci-dessous et réfléchissez aux différentes erreurs rencontrées.

```Python
>>> nom = 'toto'
>>> age = 18
>>> nom + ' a ' + age + 'ans'
???
>>> nom +' a '+ str(age)+' ans'
???
>>> mon_age='70'
>>> "L'année prochaine, j'aurai " + mon_age + 1 + 'ans'
???
>>> "L'année prochaine, j'aurai " + int(mon_age) + 1 + 'ans'
???
>>> "L'année prochaine, j'aurai " + ??? + ' ans'
>>> "l'année prichaine, j'aurai 71 ans"
>>> ma_note = '18.5'
>>> 20 - ma_note
???
>>> 20- int(ma_note)
??? 
>>> 20 - float(ma_note)
???
```



Il reste bien des choses à dire au sujet des chaînes. Nous les verrons en détails plus tard.



_Remarque :_

La multiplication est une répétition d'addition :

* En mathématiques, 2 + 2 + 2 + 2 = 2 * 4 = 4 * 2 
* Avec les chaînes aussi : 'A' + 'A' + 'A' + 'A'  = 'A' * 4 = 4 * 'A' = 'AAAA'



Testez :

```python
>>> 'A' * 4
???
>>> 4 * 'A'
???
```

Deux fonctions sont régulièrement utilisées pour réaliser des interfaces homme-machines :

* `print(chaîne)` interprête la chaîne et l'affiche dans la console.
* `rep = input(chaîne)` pose une question à l'utilisateur et stocke la réponse dans la variable _rep_ qui sera toujours du type _str_.

```python
>>> rep = input('Quel âge avez-vous ?')
12
>>> rep
???
>>> type(rep)
???
>>> print(rep)
???
```

Le `print` interprête la chaîne :

* Le `\` (anti-slash) est un caractère dit `d'échappement` : lorsqu'on le place dans une chaîne de caractère, on indique que le caractère suivant ne doit pas être interpréter mais seulement afficher. 
* le `\n` permet un renvoi à la ligne
* le `\t` effectue une tabulation.

```python
>>> print('J\'ai faim')
???
>>> print('J\'ai faim.\n Quand est-ce qu\'on mange ?')
???
>>> print('\t -Hello ! \n \t -Goodbye')
???
```



Si on veut que la chaîne soit affichée telle quelle, sans interprétation, li faut placer un _r_ devant la chaîne lors de sa définition :

```python
>>> c = r'Hello \n Comment va ?'
>>> c
???
>>> print(c)
???
```

_Remarque:_

Le `print` peut afficher autre chose que des chaînes et même afficher plusieurs variables de types différents dans la même ligne en les séparant par des `,`:

```python
>>> age = 18
>>> taille = 1.85
>>> nom = 'Ada'
>>> print('Hello ' + nom + '.Tu as', age, 'ans et mesures', taille, 'm')
???
```



### Les booléens.

Une variable booléenne ne peut prendre que deux valeurs distinctes :

* `True`
* `False`

````Python
>>> reponse = 2 < 3
>>> reponse

````



On peut effectuer des opérations logiques entre booléens : `and` ,`or` et `not`:

* Voici la table de vérité du `and` :

	

|    Expression     | Résultat |
| :---------------: | :------: |
|  True `and` True  |   True   |
| True `and` False  |  False   |
| False `and` True  |  False   |
| False `and` False |  False   |

* Voici la table de vérité du `or` :

|    Expression    | Résultat |
| :--------------: | :------: |
|  True `or` True  |   True   |
| True `or` False  |   True   |
| False `or` True  |   True   |
| False `or` False |  False   |

* Voici la table de vérité du `not` :

| Expression  | Résultat |
| :---------: | :------: |
| `not` True  |  False   |
| `not` False |   True   |



Essayez d'anticiper la réponse avant de la demander :

```python
>>> 2 > 3 and 3 < 4
???
>>> 2 > 3 or 1 > 2 
??? 
>>> not 5 // 3 == 1
???
>>> 5 % 3 == 2
???
```



_Remarques importantes :_

**Python est paresseux : ** lorsqu'il doit évaluer une expression booléenne, il le fait de gauche à droite et, s'il n'est pas nécessaire d'évaluer cette expression en entier pour obtenir le résultat, il ne le fera pas.



Ainsi, s'il doit évaluer l'expression `1 == 0 and 2 == 3` , il procède ainsi :

* `1 == 0` est évalué à `False`
* L'opérateur suivant est `and` donc le résultat final sera obligatoirement `False` : Pas la peine de regarder la suite !



La preuve, testez et expliquez pourquoi aucune erreur ne se déclenche dans la grande expression :

```python
>>> a = 2
>>> a == 2
???
>>> 2 / 0 == 2
???
>>> a == 2 or 2 / 0 == 2
???
```





Les booléens sont très importants car beaucoup utilisés, dans les conditions `si/alors/sinon` , mais aussi dans les boucles `pour` et `tant que` en Python.





_Multiplicité des langages :_



- En Javascript et en C++ on utilisera une syntaxe un peu différente mais le principe reste exactement le même :
	-  `&&` pour le `and` 
	-  `||` pour le `ou`
	-  `!` pour le `not`





___________

Par Mieszczak Christophe CC BY SA

_sources images : productions personnelles_



  
# Les boucles : _pour_ et _tant que_

En Python, comme dans la plupart des langages de programmation (Javascript, Java, C++ ..), il y a principalement deux types de boucles  :

* Les boucles :

	* Tant que.
	* Pour. 

	



## Boucles _Tant que_ 

### Prototypage



En Python, les boucles _Tant que_ s'écrivent ainsi :

```Python
while condition :
	Corps de la boucle # ici on place le code qui s'execute en boucle
# en enlevant la tabulation, on marque le début du code qui suit la boucle	
```



_Remarques :_

- Notez les deux points `:` en fin de ligne. 
- Notez la tabulation lorsqu'on code le corps de la boucle
- le corps de la boucle s'exécute tant que la _condition_ reste vraie. 
- Comme pour les structures conditionnelles, la condition est un booléen qui peut être le résultat d'opérations entre booléens.



_Exemples : Dans la console, testez les boucles ci-dessous_				

```python
>>> compteur = 0
>>> while compteur < 5 :
    	compteur = compteur + 1
>>> compteur
>>> ???
>>> chaine=''
>>> while len(chaine) < 10 :
    	chaine = chaine + "!"
>>> chaine
>>> ???
>>> ca_tourne = True
>>> while ca_tourne :
    	print("houlala j'ai mal à la tête")
```



_Remarque :_

Les boucles _Tant que_ peuvent ne pas se terminer : on dit qu'elles sont `non bornées`. Il faut bien réfléchir à sa condition de fin pour être certain qu'on y arrive à un moment ou un autre !!



_Encore une remarque :_

On retrouve à peu de chose prêt la même façon de faire dans d'autres langages de programmation :

- En Javascript :

	```javascript
	while (condition) {
	    corps de la condition; // la tabulation est facultative
	}	
	```
	
	
	
- En C++

	```c++
	while (/* Condition */){
		corps de la condition; // la tabulation est facultative
	}
	```
	
	

> **Exercice :**
>
> Relevez les points communs et les différences entre les trois prototypages Python/Javascript/C++





## Boucles _Pour_ 

### Prototypage



En Python, les boucles _Pour_ s'écrivent ainsi :

```Python
for i in sequence :
    corps de la boucle # ici on place le code qui s'execute en boucle    
	
```



_Remarques :_

- Notez les deux points `:` en fin de ligne. 

- Notez la tabulation lorsqu'on code le corps de la boucle

- _i_ parcourt une séquence, un _objet itérable_ de sa première valeur à sa dernière. _Késako ???_ Il s'agit d'un objet qui peut être parcouru de son premier élément à son dernier.  Il y en a beaucoup en Python. En voici deux exemples, il y en aura d'autres plus tard :

	- Les plages d'entiers `range(a, b)` qui contiennent les entiers ordonnées de _a_ compris à _b_ exclu.
	- les chaînes de caractères.

- la variable de boucle (_i_  ci-dessus) peut être nommée de la même façon que n'importe quelle variable. Si on ne l'utilise pas dans la boucle, on peut même la nommer `_`.

- la variable de boucle parcourt donc la séquence depuis l'élément de départ jusqu'au dernier :  : on dit que les boucles `for` sont `bornées` et on est donc certain qu'elle se termine !

	

_Exemples : Dans la console, testez les boucles ci-dessous_				

````python
>>> for nbre in range(2,5):
		print(nbre)
???
# On peut parcourir la plage selon nu pas particulier en le précisant en troisième position dans le range
>>> for nbre in range(5, 1, -1) :
    	print(nbre)
>>> for lettre in "Hello World"
		print(lettre)
???
>>> nbre = 0
>>> for _ in "Hello World" :
    	nbre = nbre + 1
>>> nbre
>>> ???
````



_Encore une remarque :_

On retrouve à peu de chose prêt la même façon de faire dans d'autres langages de programmation :



- En Javascript :

	```javascript
	for (var i = 0; i < 10; i = i + 1) { // ici i varie de 0 à 9 avec un pas de 1 
	    corps de la boucle; // la tabulation est facultative
	}	
	```
	
	
	
- En C++

	```c++
	for (int i = 0; i < 10; i = i + 1) { // ici i varie de 0 à 9 avec un pas de 1 
	    corps de la boucle; // la tabulation est facultative
	}
	```

	

> **Exercice :**
>
> Relevez les points communs et les différences entre les trois prototypages Python/Javascript/C++







____________

Par Mieszczak Christophe

_sources images :  créations personnelles_






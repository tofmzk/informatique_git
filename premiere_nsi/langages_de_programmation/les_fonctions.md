# Les fonctions

Nous allons ici introduire les fonctions afin de pouvoir :

* structurer le code et le rendre plus lisible
* coder plusieurs algorithmes dans le même programme
* pouvoir réutiliser plusieurs fois le même code sans être obligé de le réécrire (on dit qu'on **factorise** ce code)
* découper le problème global en sous problèmes aussi petits que possibles afin de rendre chaque petit problème plus facile à résoudre et à déboguer.

## Prototyper une fonction en Python



En Python, on prototype,  c'est à dire définit la structure, d'un fonction  de la façon suivante :

```python
def nom_de_la_fonction(parametre1, parametre2, ...) :
  	corps de la fonction # ici on place le code
	return valeur # La fonction renvoie la valeur attendue
```



_Remarques :_

* Le caractère `#` permet de mettre une annotation qui ne sera pas prise en compte lors de l'exécution
* Le nom de la fonction s'écrit en minuscule, sans accent. Si il est composé, on sépare les différentes partie par un `_` . Le nom doit être en rapport avec ce que fait la fonction.
* Notez les deux points `:` 
* La tabulation à la suite suivante qui permet de voir qu'on est DANS la fonction. L'enlever signifie sortir de la fonction.
* la commande `return`  renvoie la (ou les) valeur(s) qui la sui(ven)t et provoque la sortie de la fonction. Il ne doit y avoir qu'un seul `return` au maximum dans une fonction. Certaines fonctions, souvent utilisée pour réaliser des interfaces, ne renvoient rien. Ce sont alors des _procédures_ plutôt que des fonctions.



  

>  Créez un programme `operations.py` dans lequel on placera tous les exercices de ce cours.

  

Voici par exemple une fonction toute bête qui prend en paramètre deux nombres entiers et renvoie leur somme :

```python
def ajouter(a, b) :
    somme = a + b
    return somme
```



On appelle la fonction poliment, par son nom, en précisant les paramètres. On l'appeler depuis la console de la façon suivante :

```python
>>> ajouter(2, 3)
???
>>> ajouter(5, 7)
???
```





> * Définissez et codez la fonction `soustraire(a, b)` qui renvoie la différence  `a - b` et testez la depuis la console.
> * Définissez et codez la fonction `multiplier(a, b)` qui renvoie le produit de _a_ par _b_ et testez la depuis la console.
> * Définissez et codez la fonction `diviser(a, b)` qui renvoie la division de _a_ par _b_ et testez la depuis la console.



## Autres langages de programmation 



​	On retrouve à peu de chose prêt la même façon de faire dans d'autres langages de programmation :

- En Javascript :

	```javascript
	function nom_de_la_fonction(parametre1, parametre2,...) {
	    corps de la fonction ; // ici on place le code la tabulation est facultative
		return valeur ; // La fonction renvoie la valeur attendue
	}
	```

	

- En C++

	```c++
	void nom_de_la_fonction (type parametre1, type parametre2, ...) {
	    corps de la fonction ; // ici on place le code la tabulation est facultative
		return valeur ; // La fonction renvoie la valeur attendue
	}
	```

	

> **Exercice :**
>
> Relevez les points communs et les différences entre les trois prototypage Python/Javascript/C++



## Documenter une fonction



Nous avons déjà dit que la fonction devait avoir un nom qui explicite ce qu'elle fait. par exemple, notre fonction `ajouter` ... ajoute.

Oui mais elle ajoute quoi ? Testons un peu :

```python
>>> ajouter('a', 'b')
???
>>> ajouter(2, 3)
???
>>> ajouter('a', 2)
???
```

Pour expliquer clairement ce que fait la fonction et ce qu'elle attend comme paramètre(s), on doit rédiger une `documentation` aussi appelée `spécification` ou  `Docstring` dans chaque fonction. Cette dernière doit :

* Expliquer clairement ce que fait la fonction.
* Détailler les paramètres attendus et leur type. 
* Si la fonction renvoie quelque chose, elle doit préciser le type de l'élément renvoyé.



Par exemple, reprenons la fonction `ajouter` :

```python
def ajouter(a, b) :
    '''
    renvoie la somme des entiers a et b
    : param a (int) un entier
    : param b (int) un entier
    : return (int) la somme a + b
    '''
    somme = a + b
    return somme
```

Maintenant, l'utilisateur ne peut plus se tromper sur ce que fait la fonction ni sur les paramètres qu'il peut lui _passer_.



La façon dont est organisé une Docstring est très précise. Respectez toujours cette syntaxe !



> De la même façon, documentez les fonctions `soustraire`, `multiplier` et `diviser`. Attention, on ne peut diviser par 0 ...





Une fois la fonction documentée, on peut, depuis la console, demander des informations sur cette fonction via la commande `help(nom_de_la_fonction)`:

```Python
>>> help(ajouter)
???
```

## S'assurer que les paramètres sont corrects

On vient de documenter la fonction. L'utilisateur va l'appeler. Comment s'assurer que les paramètres utilisés seront bien conformes à votre documentation ?



C'est très important car nous avons vu que des paramètres inadéquates pouvaient entrainer des exceptions ou des _plantages_ !



Nous allons utiliser des **assertions** sous la forme :

```python
assert (ici on place une condition qu on souhaite être vraie), "ici on place un message d'erreur qui se déclenche si la condition précédente est fausse"
```

Dans notre fonction `ajouter` cela donne :

```python
def ajouter(a, b) :
    '''
    renvoie la somme des entiers a et b
    : param a (int) un entier
    : param b (int) un entier
    : return (int) la somme a + b
    '''
    assert type(a) == int, 'on veut ajouter des entiers'
    assert type(b) == int, 'on veut ajouter des entiers'
    somme = a + b
    return somme
```



Testez dans la console :

```python
>>> ajouter('a', 'b')
???
>>> ajouter(2, 3)
???
>>> ajouter('a', 2)
???
```

>   
>
> Ajouter les assertions dans les autres fonctions que vous avez codées.
>
>   





## Vérifier la correction de la fonction



Il est impératif d'être certain de la correction de la fonction. Il faut pour cela qu'elle renvoie, sans erreur, les résultats attendus.



Pour nous rassurer, nous allons ajouter quelques tests, **les DocTests**, à l'intérieur de nos DocStrings.



* Première Etape : à la fin du code, en bas de page, on ajoute les lignes de codes suivantes afin que les tests s'exécutent automatiquement lors de l'interprétation du code :

	```python
	################################################
	###### doctest : test la correction des fonctions
	################################################
	
	if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
	    import doctest
	    doctest.testmod(verbose = True) # si on passe verbose à False, seules les erreurs seront détaillées, s'il y en a.
	```

* Deuxième étape : on fabrique des tests. Par exemple, pour `ajouter` nous savons que `ajouter(2, 3)` doit renvoyer 5. 


* Pour terminer, on ajouter les tests à la fin de la DocString de la façon suivante :

```python
def ajouter(a, b) :
    '''
    renvoie la somme des entiers a et b
    : param a (int) un entier
    : param b (int) un entier
    : return (int) la somme a + b
    >>> ajouter(2, 3)
    5
    '''
    assert type(a) == int, 'on veut ajouter des entiers'
    assert type(b) == int, 'on veut ajouter des entiers'
    somme = a + b
    return somme
```

_Remarque :_

* Les trois chevrons `>>>` doivent être suivi d'un espace puis de l'instruction à exécuter.
* La ligne suivante indique le résultat exact attendu. 


​	

Testez en exécutant à nouveau le code.

```
Trying:
    ajouter(2, 3)
Expecting:
    5
ok
1 items had no tests:
    __main__
1 items passed all tests:
   1 tests in __main__.calculer_discriminant
1 tests in 2 items.
1 passed and 0 failed.
Test passed.
```



​	Ici tout va bien, le test a réussi !



>  **Exercice :**
>
> * Ajouter un test dans chacune des fonctions que vous avez écrites. 
>
> * Vérifier que vos fonctions passent correctement nos DocTests.



_Remarques :_

* Vous devez être certains que vos Doctests sont corrects !!
* Si un seul des tests est faux, l'algorithme ne donne pas satisfaction, il faut revoir le code.
* Si tout les tests sont vrais, ce n'est pas une preuve absolu de correction mais un indice d'autant plus valable que :
	* les tests sont pertinents. 
	* les tests sont nombreux.



_Remarque :_

Pour ne voir que les tests ratés il suffit de modifier dernière la ligne du programme : en passant le paramètre _verbose_ à False.

````Python
doctest.testmod(verbose = False)
````



## Appeler une fonction depuis une fonction

On peut également appeler une fonction depuis une autre, exactement de la même façon que nous l'avons fait depuis la console.

On va realiser une fonction de paramètres a,b et c des flottants avec c non nul qui renvoie le résultat de l'opération $`\frac {a + b} {d}`$ en utilisant les fonctions ajouter et diviser.

Voici la définition de la fonction. 

```python
def calculer(a, b, c):
    '''
    
    '''
    assert
    assert
    assert
    
    
    return resultat
```

> * Documentez et créez un jeu de test pour cette fonction.
> * Complétez les assertions
> * Utiliser les fonctions `ajouter` et `diviser` uniquement pour déterminer le résultat.



## Variables locales et globales 

Un point important  :  une variable définie dans une fonction est une variable LOCALE : elle n'existe que dans cette fonction et on ne peut donc l'utiliser ailleurs... sauf si on précise qu'on veut qu'elle soit GLOBALE (mais on l'évite autant que possible) et donc utilisable partout dans le programme. 



Sous la fonction `ajouter`, insérez la ligne `print(somme)` ainsi :

````python
def ajouter(a, b) :
    '''
    renvoie la somme des entiers a et b
    : param a (int) un entier
    : param b (int) un entier
    : return (int) la somme a + b
    >>> ajouter(2, 3)
    5
    '''
    somme = a + b
    return somme

print(somme)
````



>  Exécutez le programme. Que se passe-t-il  ? Pourquoi ?



 



______________

Par Mieszczak Christophe 

licence CC BY SA








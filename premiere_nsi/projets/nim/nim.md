# Le Nim

![allumettes - perso](media/allumettes.jpg)



* Le jeu se joue à deux joueurs. Pour nous, ce sera  un humain et l'ordinateur
* Un certain nombre (au minimum 4) d'allumettes sont alignées.
* Chaque joueur, à son tour, doit enlever au minimum 1 et au maximum 3 allumettes.
* Celui qui ramasse la dernière allumette a gagné.

  

Le déroulement du jeu, pour _nun certain nombre d'allumettes, est le suivant :

* On décide de qui commence.
* Tant qu'il reste des allumettes
    * on affiche la situation en cours (le nbre d'allumettes et qui doit jouer)
    * si c'est à l'humain de jouer, on lui demande combien d'allumettes il souhaite enlever, sinon, on demande l'ordinateur.
    * on annonce le coup du joueur (humain ou ordinateur)
    * on retire ce nombre d'allumette(s) au nombre d'allumette(s) avec lesquelles on joue.
    * s'il reste encore des allumettes, on change de joueur (sinon je joueur en cours a gagné ...)
* On annonce la victoire du dernier joueur à avoir joué.



Nous allons implémenter ce jeu en Python le décomposant  en plusieurs petites fonctions, chacune exécutant une des tâche listées ci-dessus.



## D'abord, faisons quelques essais.



* Simuler  plusieurs parties avec seulement 4 allumettes.
* Faites de même avec 5 puis 8 allumettes



> * Dans quelle situation un joueur peut-il être sûr de gagner ?
>
>     ```txt
>                                 
>     ```
>
>     
>
> * Quand un joueur a-t-il intérêt à commencer la partie ?
>
>     ```txt
>                                 
>     ```
>
>     



## Python

Il faudra proposer des tests dès que cela est possible et ne pas oublier les assertions lorsqu'elles sont nécessaires. Il n'y aura,au maximum, qu'un seul `return` dans une fonciton.

### Qui commence ?





> * Réaliser la fonction `choisir_depart()` qui renvoie 'Humain' si l'humain commence et 'Ordi' sinon.
>
>     ```python
>     def choisir_depart():
>         '''
>         Renvoie 'Humain' si l'humain commence et 'Ordi' sinon
>         : return (str) 'Humain' ou 'Ordi'
>         '''
>
> 	```
>
> * Un peu d'aide :
>
>     * On pose une question à l'humain : ' _Voulez-vous commencer (o/n) ? '_  en stockant la réponse dans une variable qu'on nommera _reponse_ en utilisant l'instruction  `input`:
>
>       ```python
>         reponse = input('Voulez-vous commencer (o/n) ? ')
> 		```
>
>     * _Tant que_ _reponse_ n'est pas 'o' **et** n'est pas 'n', on pose à nouveau cette question. 
>
>     * Lorsqu'on sort de la boucle _tant que_, l'utilisateur a donc répondu 'o' ou 'n'. Il ne reste qu'à renvoyer la variable _reponse_ en utilisant l'instruction `return`.
>
> * Testez votre fonction dans la console
>
>     ```python
>     >>> choisir_depart()
>     Voulez-vous commencer (o/n) ? coucou
>     Voulez-vous commencer (o/n) ? o
>     'Humain'
>     >>> choisir_depart()
>     Voulez-vous commencer (o/n) ? n
>     'Ordi'
>
> 



### Choix de l'humain



> Définissez la fonction `choix_humain(nbres_allumettes_restantes)` où la variable _nbre_allumettes_ passée en paramètre correspond au nombre d'allumettes non encore enlevées. Cette fonction renvoie le nombre d'allumettes que l'humain choisit d'enlever. 
>
> ```python
> def choix_humain(nbre_allumettes):
> 	'''
> 	demande à l'humain le nbre d'allumettes qu'il souhaite enlever entre 1 et 3
> 	dans la limite du nombre d'allumettes disponibles
> 	: param nbre_allumettes (int)> 0
> 	: return (int) le nbre d'allumettes à enlever	(1 2 ou 3)
> 	'''
> ```
> 
> Testez cette fonction via la console.
>
> ```python
>>>> choix_joueur(2)
> combien d'allumettes souhaitez vous enlever ? 3
> combien d'allumettes souhaitez vous enlever ? 5
> combien d'allumettes souhaitez vous enlever ? 2
> 
> ```
> 
> 



### Choix de l'ordinateur



Nous avons vu plus haut, en jouant un peu nous même, qu'il y a une situation perdante : si le nombre d'allumette restante est un multiple de quatre alors le joueur dont s'est le tour de jouer perdra si son adversaire ne fait aucune erreur.

Aussi l'ordinateur va essayer de laisser la main à l'humain en lui laissant un nombre d'allumettes multiple de 4. S'il ne peut pas, il jouera au hasard entre 1 et 3 allumettes.

Par exemple, si c'est à l'ordinateur de jouer :

* s'il reste 15 allumettes alors l'ordinateur va en enlever 3 afin de laisser 12 allumettes à l'humain car 12 est un multiple de 4 : l'ordi est en position gagnante.
* s'il reste 8 allumettes alors l'ordinateur est dans une situation perdante et ne peut pas enlever suffisamment d'allumettes pour retourner la situation (il en faudrait 4 ou 0 pour retomber sur un multiple de 4). Il enlève dans un nombre aléatoire d'allumettes entre 1 et 3.



Pour vérifier combien d'allumette(s) l'ordinateur doit enlever, on utilise l'opérateur `%` qui,  calcule le reste de la division entière.

Testez dans la console :

```python
>>> 5 % 4
???
```

S'il reste 5 allumettes, l'ordinateur en enlèvera une seule pour laisser à l'humain une situation perdante.



```python
>>> 6 % 4
???
```

S'il reste 6 allumettes, l'ordinateur en enlèvera deux pour laisser à l'humain une situation perdante.



```python
>>> 7 % 4
???
```

S'il reste 7 allumettes, l'ordinateur en enlèvera 3 pour laisser à l'humain une situation perdante.



```python
>>> 8 % 4
???
```

S'il reste 8 allumettes, il faudrait qu'il enlève 0 allumette (ou 4) ce qui n'est pas possible. Il va donc choisir au hasard un nombre entre 1 e t 3 inclus.

_Remarque :_

Nous allons devoir, pour ce cas de figure, importer le module `random` en ajoutant en haut du code :

```python
import random
```



> * Réalisez la fonction `choix_ordinateur(nbre_allumettes)`où la variable _nbre_allumettes_ passée en paramètre correspond au nombre d'allumettes non encore enlevées, qui renvoie le nombre d'allumettes que l'ordinateur choisit d'enlever pour essayer de laisser à l'humain une situation perdante. 
>
>     ```python
>     def choix_ordi(nbre_allumettes):
>         '''
>         Renvoie le nbre d'allumettes qu'enlève l'ordinateur.
>         : param nbre_allumettes (int) > 0
>         : return le nbre d'allumettes à enlever(int)
>         >>> choix_ordi(5)
>         ???
>         >>> choix_ordi(6)
>         ???
>         >>> choix_ordi(7)
>         ???
>         '''
>     ```
>     
>     * Testez votre fonction via la console
> 
>    ```python
>     >>> choix_ordi(9)
>    ???
>     >>> choix_ordi(10)
>     ???
>     >>> choix_ordi(11)
>     ???
>     >>> choix_ordi(12)
>     ???
>     >>> choix_ordi(12)
>     ???
>    ```
> 
> 

### Au suivant



> * Réalisez une fonction `changer_joueur(joueur_en_cours)` où _joueur_en_cours_ est soit 'Humain' soir 'Ordi', qui renvoie le joueur suivant, c'est à dire soit 'Humain' soir 'Ordi'. Complétez les deux Doctests.
>
>     ```python
>     def changer_joueur(joueur_en_cours):
>         '''
>         renvoie le joueur suivant
>         : param joueur_en_cours (str) 'Ordi' ou 'Humain'
>         : return (str) le joueur suivant 'Humain' ou 'Ordi'
>         >>> changer_joueur('Ordi')
>       	???
>         >>> changer_joueur('Humain')
>         ???
>        '''
>    ```
>
> 



### Les affichages



Il faut toujours séparer la partie affichage du reste. Si on souhaite par la suite améliorer le rendu , par exemple en faisant une interface graphique, il ne faudra ainsi refaire que la partie affichage.

Par exemple, la fonction ci-dessous permet d'afficher les conditions de départ d'un tour du jeu :

```python
def afficher_situation(joueur_en_cours, nbre_allumettes):
    '''
    affiche le joueur qui commence et le nombre d'allumettes restantes
    : param joueur_en_cours (str)
    : param nbre_allumettes(int) > 0
	: pas de return
    '''
    print('>>>' joueur_en_cours, ' joue avec ', nbre_allumettes, ' allumette(s)')
    print('.............')
```





> Sur le même modèle, réalisez les  fonctions  :
>
> * `afficher_coup(joueur_en_cours, nbre_allumettes)` qui affiche qui vient de jouer et ce qu'il a choisi de jouer, par exemple sous la forme : 'Humain a retiré 3 allumettes'
>
> * `afficher_victoire(joueur_en_cours)` qui affiche la victoire du joueur en cours, par exemple sous la forme : 'Ordi a gagné la partie !!'
>
> * Testez vos fonctions depuis la console :
>
>     ```python
>     >>> afficher_coup('Humain', 3)
>     'Humain a retiré 3 allumettes'
>     >>> afficher_victoire('Ordi')
>     'Ordi a gagné la partie !!'
>     ```
>
>     * Vous pouvez enjoliver vos affichages en utilisant des caractères spéciaux en UTF-8 : 😀, 🤖  ...



## Déroulement du jeu

Récapitulons : 

* On va lancer la partie avec un certain nombre d'allumettes.
* Tout d'abord on va demander à l'humain s'il veut commencer ou pas : la variable _joueur_ sera initialisée avec la réponse de la fonction dont s'est la tâche.
* Tant que le nombre d'allumettes est strictement supérieur à 0
    * on affiche la situation en cours.
    * si le joueur est 'Humain' alors on lui demande le nombre d'allumettes qu'il souhaite enlever, sinon on demande à l'ordinateur
    * on enlève le nombre choisi au nombre total d'allumette(s)
    * on affiche le coup du joueur
    * **s'il reste encore des allumettes**, on passe au joueur suivant
* A la suite de cette boucle, on annonce la victoire du _joueur_.



> * Définissez la fonction `jouer(nbre_allumettes)` où le paramètre _nbre_allumettes_ est le nombre d'allumettes total choisi pour la partie, qui réalise le déroulement du jeu. Utilisez des assertions.
>
>     Un peu d'aide :
>
>     ```python
>     def jouer(nbre_allumettes):
>         '''
>         lance et déroule la partie avec nbre_allumettes à enlever au début
>         : param nbre_allumettes (int) > 3
>     	: pas de return
>         '''
>         joueur = choisir_depart()
>         while nbre_allumettes > 0:
>
> 
>
> * Jouez dans la console !
>
>    ```python
>     >>> jouer(10)
>    ???
>    ```
>
> 

____________

Par Mieszczak Christophe CC - BY - SA

image : production personnelle.
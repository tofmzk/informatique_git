# Changement de base 

.



# De la base 10 à la base 2



![binaire - source wikipédia -CCA ](https://upload.wikimedia.org/wikipedia/commons/1/1a/Information_binaire.png)



> Définissez une fonction `base10_vers_base2`, de paramètre _nombre_, un entier compris entre 0 et 255, qui renvoie une chaine de caractère correspondant à l'écriture binaire de _nombre_ sur un octet sans complément à 2.
>
> Avant même de commencer à coder :
>
> * documentez votre fonction
> * créez 4 doctests
> * aller relir la partie **convertir en base 2** du document `representation_des_entiers.md`
>
> 
>
> Précaution d'usage :
>
> * Si le paramètre n'est pas un entier compris entre 0 et 255, la fonction retournera la chaine _'paramètre non valide'_.







# De la base 10 aux Shadocks (base 4).

![shadocks - source flickr - CC BY-NC-ND ](https://live.staticflickr.com/829/41450028734_c45b9db457_b.jpg)



Pour l'algorithme ci-dessous, on utilisera les symboles shadocks :

* **0**  on écrit O
* **1** on écrit −
* **2**  on écrit ⨼
* **3 ** et on écrit ◿



> Définissez une fonction `base10_vers_shadock`, de paramètre _nombre_, un entier compris entre 0 et 255, qui renvoie une chaine de caractère correspondant à l'écriture shadock de _nombre_ .
>
> Avant même de commencer à coder :
>
> - documentez votre fonction
>
> - créez 4 doctests
>
>   
>
> 
>
> Précaution d'usage :
>
> - Si le paramètre n'est pas un entier compris entre 0 et 255, la fonction retournera la chaine _'paramètre non valide'_.



## De la base 2 à la base 10



> Réalisez une fonction base2_vers_base10, de paramètre _bin_, une liste contenant des 0 et des 1 et représentant une série de bits, qui renvoie la valeur décimal de l'octet.
>
> * Documentez la fonction
> * Ajoutez au minimum 3 doctests

_Pour bien commencer:_

Voici le début du code

```Python
def base2_vers_base10(bin):
    '''
    renvoie la valeur décimal de la série de bits codé dans la liste bin
    pram bin
    type liste de 0 ou 1
    return la conversion en base 10
    type int
    doctests :
    >>> base2_vers_base10([1,1,1,1,1,1,1,1])
    255
    >>> base2_vers_base10([1,0,0])
    4
    >>> base2_vers_base10([1,1,0,1])
    13
    >>> base2_vers_base10([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])
    65535
    '''
```

______

Par Mieszczak Christophe CC BY SA
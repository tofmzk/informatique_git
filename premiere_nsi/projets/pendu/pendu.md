# Le pendu 

![le pendu - source wikipédia -  **GNU Free Documentation License**](https://upload.wikimedia.org/wikipedia/commons/d/d6/Hangman-6.png)



## Le principe

L'ordinateur choisit aléatoirement un mot parmi une banque de mots disponible. Il s'agit de la deviner.

Pour cela le joueur a droit à 7 tentatives :

```markdown
L'ordinateur choisit un mot
il crée un 'mot masqué' correspondant au mot choisi en cachant chaque lettre du mot choisi.
Tant qu'il reste des tentatives et que le mot n'est pas découvert
	* l'ordinateur affiche le mot masqué
	* le joueur propose une lettre
	* si la lettre est dans le mot, alors l'ordinateur démasque la lettre dans le mot masqué.
	* Sinon, le joueur perd une tentative et on affiche le pendu 
	
Si le mot est découvert alors le joueur à gagné
Sinon il est pendu haut et court !
```



Créez un fichier `pendu.py` qui contiendra notre code.



## Démarrons

Nous allons utiliser deux modules :

* Le modules `random` que nous avons déjà utilisé
* Le module `module_pendu` qui est fourni avec le fichier texte qui contient les mots dont nous allons nous servir.



Dans la console tapez le code ci_dessous :

````Python
>>> import module_pendu
>>> help(module_pendu)
???
````



Testons la fonction `afficher_pendu` :

````Python
>>> module_pendu.creer_liste_mots()
??? (seulement un extrait ...)
>>> module_pendu.afficher_pendu(1)
???
>>> module_pendu.afficher_pendu(8)
???
````



> 
>
>  Importez ces deux modules correctement dans notre programme.
>
> 



_Remarque :_



Ajoutez le code ci-dessous en bas de  notre fichier Python. Il servira à valider, ou non, nos tests :



````Python
################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = True) 
````



## Choisir un mot au hasard



>  Créez et codez la fonction `choisir_mot` qui renvoie au hasard un mot de 7 lettres maximum parmi la liste de mots obtenue grâce au `module_pendu` et sa fonction `creer_liste_mot`.
>
>  Pour vous aider :
>
>  ```python
>  def choisir_un_mot():
>  '''
>      renvoie au hasard une mot de 7 lettres maximum, choisi dans
>      la liste de mot créée par module_pendu
>      : return (str)
>    '''
>  
>  
>  ```



## Création d'un masque

Une fois le mot choisi par l'ordinateur, nous allons générer un masque de ce mot c'est à dire une chaîne de caractères de même longueur que le mot à deviner mais dans laquelle on remplace chaque lettre par un _'?'_  en conservant un éventuel tiret  '-'.



Au fil des essais du joueur, le masque évoluera en dévoilant les lettres présentes dans le mot à deviner.





> Définissez une fonction `generer_masque(mot_a_masquer)` de paramètre une chaîne de caractères, qui renvoie une chaîne de caractères de même longueur que _mot_a_masquer_ mais ne contenant que des _'?'_  et, éventuellement, des '-'.
>
> Vous trouverez ci-dessous le début de cette fonction et des Doctests.
>
> ````Python
> def generer_masque(mot_a_masquer):
>        '''
>        renvoie une chainede meme longueur que le mot qui ne contient que des '?'
>        : param mot_a_masquer (str)
>        : return (str)
>        >>> generer_masque('HELLO')
>        '?????'
>        >>> generer_masque('ALIBABA')
>        '???????'
>        >>> generer_masque('CHAUFFE-EAU')
>        '???????-???'
>        '''
>    ````
>    
> 



Depuis la console, testez votre fonction avec différents mots.



## Démasquer une lettre



Le joueur va proposer une _lettre_ . Si cette dernière est présente dans le mot à deviner alors, aux emplacements de cette lettre, le masque doit être modifié de façon à remplacer les _'?'_ par la lettre correspondante.



> Définissez une fonction `modifier_masque(mot_a_deviner, mot_masque, lettre_joueur)` de paramètres trois chaînes de caractères, qui renvoie un mot masqué modifié de façon à laisser apparaître la lettre aux emplacements adéquates, si elle est présente dans le mot à deviner.
>
> 
>
> ````Python
> def modifier_masque(mot_a_deviner, mot_masque, lettre_joueur):
>      '''
>         renvoie le mot masqué modifié de façon à laisser apparaitre la lettre
>         aux emplacements adéquates si elle est présente dans le mot à deviner.
>         : params
>            mot_a_deviner(str)
>            mot_masque(str)
>            lettre_joueur(str)
>         : return str
>         >>> modifier_masque('HELLO','?????','E')
>         '?E???'
>         >>> modifier_masque('HELLO','?????','L')
>         '??LL?'
>         >>> modifier_masque('ALIBABA','?L?????','A')
>         'AL??A?A'
>     '''
>      masque_modifie = ''
> ````
>
> 



Depuis la console, testez votre fonction avec différentes chaînes.



## Question et affichage

On pourrait se contenter d'utiliser `input` et `print` directement dans notre fonction principale. Mais cela est une mauvaise idée : il faut toujours séparer les algorithmes de l'interface afin de pouvoir facilement, si on le souhaite, modifier l'interface sans toucher aux algorithmes.



Ainsi, nous allons coder trois fonctions.



> Codez la fonction `demander_lettre()` qui demande au joueur une lettre majuscule et une seule, puis renvoie la lettre proposée.
>
> Documentez votre fonction et testez là depuis la console.
>
> _Remarque :_
>
> On pourra utiliser la fonction `ord(caractere)` qui renvoie le code ASCII du caractère. Testez dans a console :
>
> ```python
> >>> ord('A') 
> ???
> >>> ord('B')
> ???
> ```
>
> 





> Codez la fonction `afficher_texte(chaine)` qui affiche, dans la console, la chaîne de caractères passée en paramètre.
>
> Documentez votre fonction et testez là depuis la console.



> Codez la fonction `afficher_pendu(n)` qui, grâce au `module_pendu`, affiche le pendu à l'étape _n_ dans la console.
>
> Documentez votre fonction et testez là depuis la console.





## Jouons 








> Définissez la fonction `jouer` sans paramètre qui réalise l'algorithme ci-dessous :
>
> * L'ordinateur choisit un mot que l'on stocke dans la variable _mot_a_deviner_.
> * On génère le masque _masque_ du mot à deviner.
> * On initialise la variable _nombre_erreurs_ à 0 .
> * Tant que _masque_ et _mot_a_deviner_ sont différents et que _nombre_erreurs_ est strictement inférieur à 8  :
> 	* on afficher le masque.
> 	* on demande une lettre au joueur.
> 	* si la lettre est présente dans le mot à deviner alors on modifie le masque
> 	* sinon, _nombre_erreurs_ augmente de 1 et on affiche le pendu dans l'état adéquate.
> * si  _masque_ et _mot_a_deviner_ sont identiques alors le joueur à gagné et on le félicite.
> * sinon il est pendu haut et court et on affiche le mot qu'il fallait deviner.



_______

Par Mieszczak Christophe 

licence CC BY SA

source image :  [wikipédia licence CC BY SA](https://commons.wikimedia.org/wiki/File:Hangman-6.png)


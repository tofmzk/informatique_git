'''
    :author: Mieszczak C
    :date: 23     sept 2019
'''       

def creer_liste_mots():
    """
    : Après lecture d'un fichier txt, renvoie une liste de mots en majuscule sans accent
    : return : list
    """
    fichier = open('complet.txt', 'r', encoding = 'UTF8')
    chaine_de_mots = fichier.read()
    fichier.close()
    liste_de_mots=chaine_de_mots.split('\n')
    for mot in liste_de_mots :
        if len(mot)<3 :
            liste_de_mots.remove(mot)
    return liste_de_mots

def afficher_pendu(nombre_erreurs):
    '''
    affiche l'état du pendu selon le nombre d'erreurs fait entre 1 et 8
    param nombre_erreurs
    type int
    '''
           
    DESSIN_PENDU = [
"""
  __________
 |/   |
 |
 |
 |
 |
 |""",
 """
  __________
 |/    |
 |     O
 |
 |
 |
 |""",
 """
  __________
 |/    |
 |     O
 |    -+-
 |
 |
 |""",
 """
  __________
 |/    |
 |     O
 |   /-+-
 |
 |
 |""",
 """
  __________
 |/    |
 |     O
 |   /-+-\\
 |
 |
 |""",
 """
  __________
 |/    |
 |     O
 |   /-+-\\
 |     +
 |
 |""",
 """
  __________
 |/    |
 |     O
 |   /-+-\\
 |     +
 |    /
 |""",
 """
  __________
 |/    |
 |     O
 |   /-+-\\
 |     +
 |    / \\
 |   R I P
 |"""]
    print(DESSIN_PENDU[nombre_erreurs - 1])
    

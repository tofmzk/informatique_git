# La fourmi de Langton

On nomme fourmi de Langton un automate cellulaire (voir machine de Turing) bidimensionnel comportant un jeu de règles très simples. On lui a donné le nom de Christopher Langton, son inventeur.

Elle constitue l'un des systèmes les plus simples permettant de mettre en évidence un exemple de comportement émergent. 

<img src='media\exemple.jpg' width='250px' height='250px'/>

## Les régles

* La grille de "jeu" est composée de **NB_LIGNES** et **NB_COLONNES**. 
    * Chaque cellule a deux état : elle est soit blanche soit noire.
    * Au départ la grille peut, au choix, ne contenir que des cellules vides ou un certains nombre de cellules pleines placée aléatoirement.

* La fourmi, au départ, se place au centre de la grille. 

* La fourmi peut se déplacer à gauche, à droite, en haut ou en bas d'une case à chaque fois selon les règles suivantes :
    * Si la fourmi est sur une case noire, elle tourne de 90° vers la gauche, change la couleur de la case en blanc et avance d'une case.
    * Si la fourmi est sur une case blanche, elle tourne de 90° vers la droite, change la couleur de la case en noir et avance d'une case.

     

<img src='media\premieres_etapes.jpg'  width='350px' height='250px'/>

>Ces règles simples conduisent à un comportement étonnant de la fourmi : 
>après une période initiale apparemment chaotique, la fourmi finit par construire une « route » formée par 104 étapes qui se répètent indéfiniment.
>On **conjecture** que ce comportement reste vrai pour n'importe quel motif initial fini dessiné sur la grille. C'est bel et bien une **conjecture** : ce n'est pas démontré à ce jour.

<img src='media/autoroute.jpg' width='400px' height='400px'/>






from random import randint,choice
from time import sleep
from random import randint
from tkinter import *
#### Constantes entieres

NB_LIGNES=20  #nbre de lignes
NB_COLONNES=20 #nbre de colonnes
LONG_CANVAS=500 #longueur du canvas en pixels
HAUT_CANVAS=500 #hauteur du canvas en pixels
TEMPO=0.5#temporisation entre les déplacement
VIDE=True#grille vide par défaut
SIMULATION_EN_COURS=False #par défaut la simulation n'est pas lancé.



##################################
#### grille
##################################

def generer_grille_vide():
    """
    Renvoie une liste de liste de NB_LIGNES et NB_COLONNES ne contenant que des 0
    >>> grille=generer_grille_vide()
    >>> len(grille[0])==NB_COLONNES
    True
    >>> len (grille)==NB_LIGNES
    True
    
    """
    grille=[]
    for j in range(0,NB_LIGNES):
        grille.append([])
        for _ in range(0,NB_COLONNES):
            grille[j].append(0)
    return grille


def generer_grille_alea():
    """
    renvoie une liste de liste de NB_LIGNES et NB_COLONNES contenant soit des 0 soit des 1 placés aléatoiremnt
    >>> grille=generer_grille_alea()
    >>> len(grille[0])==NB_COLONNES
    True
    >>> len (grille)==NB_LIGNES
    True
    
    """
    grille=[]
    for j in range(0,NB_LIGNES):
        grille.append([])
        for _ in range(0,NB_COLONNES):
            if randint(0,10)==0:
                grille[j].append(1)
            else :
                grille[j].append(0)
    return grille

##################################
#### affichage
##################################
def initialiser_canvas(grille):
    """
    dessine le canvas en position initiale  (quadrillage vide)
    """
    toile.create_rectangle(0,0,LONG_CANVAS,HAUT_CANVAS,fill='white')
    #global NB_COLONNES,NB_LIGNES,LONG_CANVAS,HAUT_CANVAS
    largeur_case=LONG_CANVAS//NB_COLONNES
    hauteur_case=HAUT_CANVAS//NB_LIGNES
    for x in range(NB_COLONNES):
        for y in range(NB_LIGNES):
            if grille[y][x]==0:
                couleur='white'
            else :
                couleur="black"
            toile.create_rectangle(x*largeur_case,y*hauteur_case,(x+1)*largeur_case,(y+1)*hauteur_case,fill=couleur)
    toile.update()

def modifier_case(x,y,couleur):
    """
        affiche un rectangle de la couleur choisi dans la case (x,y)
    """
    global NB_COLONNES,NB_LIGNES,LONG_CANVAS,HAUT_CANVAS
    largeur_case=LONG_CANVAS//NB_COLONNES
    hauteur_case=HAUT_CANVAS//NB_LIGNES
    toile.create_rectangle(x*largeur_case,y*hauteur_case,
                           (x+1)*largeur_case,(y+1)*hauteur_case,fill=couleur)
    toile.update()
    
def afficher_fourmi(x,y):
    """
        affiche la fourmi dans la case (x,y)
    """
    global NB_COLONNES,NB_LIGNES,LONG_CANVAS,HAUT_CANVAS
    largeur_case=LONG_CANVAS//NB_COLONNES
    hauteur_case=HAUT_CANVAS//NB_LIGNES
    toile.create_rectangle(x*largeur_case+largeur_case//4,y*hauteur_case+hauteur_case//4,
                           (x+1)*largeur_case-largeur_case//4,(y+1)*hauteur_case-hauteur_case//4,fill="red")
    toile.update()

###################################
########déplacements
##################################

def changement_direction(grille,direction,x,y):
    """
        renvoie la direction selon les règles du jeu et affiche les ases modifiées dans le canvas
        effet de bord sur grille
        1 -> haut
        2 -> droite
        3 -> bas
        4 -> gauche
    """
    if grille[y][x]==0 : # cellule vide...
        direction=(direction+1)%4 # tourne à droite
        grille[y][x]=1 # remplir cellule précédente
        modifier_case(x,y,"black") # représenter la cellule précédent pleine
    else :
        grille[y][x]=0 # vide case précédent
        modifier_case(x,y,"white") # représente la case vide
        direction-=1 # tourne à gauche
        if direction<0:
            direction=3
    return direction


def deplacer_fourmi(grille,direction,x,y):
    """
        renvoie les coordonnées et la direction courante de la fourmi
    """
    direction=changement_direction(grille,direction,x,y)
    dx=[0,1,0,-1]
    dy=[-1,0,1,0]
    return x+dx[direction],y+dy[direction],direction






################################################
###### simulation
################################################

def simuler():
    """
        simule le déplacement de la fourmi tant qu'elle ne sort pas du canvas ou qu'on appuie sur le bouton action
    """
    global SIMULATION_EN_COURS
    SIMULATION_EN_COURS=not SIMULATION_EN_COURS
    if SIMULATION_EN_COURS :
        
        nb_deplacements=0
        x_fourmi=NB_COLONNES//2
        y_fourmi=NB_LIGNES//2
        direction=0
        if VIDE :
            grille=generer_grille_vide();
        else :
            grille=generer_grille_alea()
        initialiser_canvas(grille)
        while x_fourmi>=0 and x_fourmi<NB_COLONNES and y_fourmi>=0 and y_fourmi<NB_LIGNES and SIMULATION_EN_COURS :
            afficher_fourmi(x_fourmi,y_fourmi)
            sleep(TEMPO)
            x_fourmi,y_fourmi,direction=deplacer_fourmi(grille,direction,x_fourmi,y_fourmi)
            nb_deplacements+=1
            valeur_tempo.set('Nombre de déplacements : '+str(nb_deplacements))
    SIMULATION_EN_COURS=False

#####################################
#################interface Tkinter
#####################################

def recuperer_les_constantes():
    """
        recupere les constante dans l'interface puis lance simuler
    """
    global NB_COLONNES,NB_LIGNES,TEMPO,VIDE
    NB_LIGNES=int(nb_lig.get())
    NB_COLONNES=int(nb_col.get())
    TEMPO=float(tempo.get())
    VIDE=grille_vide.get()
    simuler()



#####################################################
fenetre = Tk()
fenetre.title("La fourmi de Langton")
########canvas
toile=Canvas(fenetre, width=LONG_CANVAS, height=HAUT_CANVAS, bg='white')
toile.grid(row=1,column=1,columnspan=5,padx=10,pady=10)
##########ligne 1
Label(fenetre,text="Nbre de lignes:").grid(row=2,column=1)
nb_lig=Entry(fenetre)
nb_lig.grid(row=2,column=2)
nb_lig.insert(0,NB_LIGNES)
Label(fenetre,text="Nbre de colonnes:").grid(row=2,column=3)
nb_col=Entry(fenetre)
nb_col.grid(row=2,column=4)
nb_col.insert(0,NB_COLONNES)
###########ligne 2
Label(fenetre,text="temporisation:").grid(row=3,column=1)
tempo=Entry(fenetre)
tempo.grid(row=3,column=2)
tempo.insert(0,0)

grille_vide = BooleanVar()
grille_vide.set(True)
check_vide  = Checkbutton(fenetre, text = "Grille de départ vide ", variable = grille_vide).grid(row = 3, column = 3)
################################ligne 3
valeur_tempo=v = StringVar()
valeur_tempo.set("Nombre de déplacements : 0")
nb_dep=Label(fenetre,textvariable=valeur_tempo).grid(row=4,column=2)
 ################ ligne 4
go=Button(fenetre, text ='ACTION !',cursor="hand1",command=recuperer_les_constantes)
go.grid(row=3, rowspan=2, column=4,columnspan=2)

fenetre.mainloop()



################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose=True)
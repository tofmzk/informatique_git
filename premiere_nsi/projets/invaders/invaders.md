# Invaders

## Pyxel

Nous allons utiliser un _module_, c'est à dire une bibliothèque de fonctions, dédié à la création de jeu en 2D : pyxel.

1. Ouvrez Thonny.
2. Enregistrer un nouveau fichier sous le nom _invaders.py_ dans le répertoire où se trouve ce cours.



Avec pyxel, la structure du code est toujours la même. Collez le code ci-dessous dans la partie programme (en haut) de Thonny.

```python
# importation des modules nécessaires
import pyxel

#########VARIABLES GLOBALE
#ici on définit toutes les variables globales


################### On placera les fonctions annexes ci-dessous


###################Calculs indispensable au jeu
def calculer() :
    '''
    ici on effectue tous les calculs nécessaires au jeu
    '''
    

###################Affichage des objets du jeu
def afficher():
    '''
    ici on dessine tous les objets du jeu
    '''


##############lancement du programme
pyxel.init(256, 256, 'Invaders')
pyxel.run(calculer, afficher)
```

Le seul code qui se lance à l'exécution et celui qui se trouve en bas :

* `pyxel.init(256, 256, 'Invaders')` affiche la fenêtre graphique de 256 pixels sur 256 pixels nommées _Invaders_
* `pyxel.run(calculer, afficher)` lance en réalité une boucle qui appelle 30 fois par seconde la fonction `calculer` (spoiler, elle va servir à réaliser les calculs)  et la fonction `afficher ` (besoin d'un spoiler ou ça ira ?)

Exécutez le code et regardez la fenêtre graphique qui s'affiche :

* Son origine et en haut à gauche.
* l'axe des abscisses et traditionnellement orienté vers la droite.
* l'axe des ordonnées est orienté vers le bas : plus en descend plus l'ordonnée est grande.

![](media/fenetre.png)

## Les Envahisseurs

### Ajouter des envahisseurs ... carrés

Dans la partie tout en haut dédiées aux variables globales, déclarez `tab_invaders`, un tableau vide (pour l'instant ...).

Un envahisseur sera simplement défini par ses coordonnées. A l'initialisation d'un envahisseur, il se trouvera tout en haut, à l'ordonnée 0 et à une position aléatoire en abscisse en 16 et 240. Ainsi, un envahisseur _e_ de coordonnées (23, 0) sera représenté par le tableau _e = [23, 0]_ :

* _e[0]_ est l'abscisse de l'envahisseur;
* _e[1]_ est son ordonnée.

 

> Codez, dans la partie _fonctions annexes_, une fonction `ajouter_envahisseur` qui ajoute au tableau _tab_invaders_  un envahisseurs.
>
> Vous penserez bien à importer (tout en haut du code) le module `random` qui permet de générer un entier aléatoire compris entre a et b inclus grâce à l'instruction `random.randint(a, b)`.

  

Pour gérer le temps, on va utiliser la variable `pyxel.frame_count`. Cette dernière a pour valeur le nombre de fois que la fenêtre a été raffraichie. Toutes les 10 frames, c'est à dire chaque fois que `pyxel.frame_count` est divisible par 10, on ajoutera un envahisseur au tableau d'envahisseurs.



Pour afficher un envahisseur, nous allons pour l'instant nous contenter d'un rectangle de 16 pixels sur 16 pixels en utilisant l'instruction `pyxel.rect(x, y, longueur, hauteur, couleur)`.

* x est l'abscisse du coin en haut à gauche.
* y est l'ordonnée du coin en haut à gauche.
* longueur et hauteur sont les dimensions du rectangle.
* La couleur est un entier entre 0 et 16 au choix :

![](media/couleurs.jpg)





> * Dans la fonction `calculer`, si `pyxel.frame_count % 10 == 0` alors, appelez la fonction `ajouter_envahisseurs` .
> * Dans la fonction `afficher`, pour chaque envahisseur du tableau d'envahisseurs, afficher un rectangle.



### Déplacer les envahisseurs



Pour déplacer les envahisseurs, il suffit d'augmenter leur ordonnée (vous vous rappelez bien que l'axe des ordonnées pointe vers le bas ...). Mais il faudra retirer l'envahisseur du tableau si cette ordonnée dépasse 256 car il sortira alors de la fenêtre graphique.

> Dans la fonction `calculer` , pour chaque envahisseur du tableau d'envahisseurs :
>
> * déplacez le de 2 pixels vers le bas
> * s'il sort de la fenêtre enlevez le du tableau.



## Le vaisseau

### Initialisation et affichage

Le vaisseau va être lui aussi défini par des coordoonées et son score.

>  Dans la partie variable globale définissez le tableau `vaisseau = [116, 224, 0]` :
>
>  * `vaisseau[0]` est l'abscisse du vaisseau;
>  * ` vaisseau[1]` est son ordonnée;
>  * ` vaisseau[2]` est le score du vaisseau.



Pour afficher le vaisseau, on va à nouveau dessiner un rectangle, mais il sera un peu plus grand et fera 24 pixels sur 24 pixels.



> Dans la fonction `dessiner` :
>
> *  affichez le rectangle qui représente le vaisseau;
> * affichez le score en haut à gauche en utilisant l'instruction `pyxel.text(x, y, chaine_de_caratère, couleur)`.
>
> Dans la fonction `calculer` :
>
> * si un envahisseur sort de la fenêtre, le score diminue de 50.



### Déplacements

Pour gérer les événements clavier, c'est à dire les interactions entre le joueur et le clavier, pyxel utilise l'instruction `pyxel.btn(touche)` qui renvoir `True` si la touche a été pressée et `False` sinon.



> * Compléter la fonction annexe ci-dessous pour gérer les déplacements du vaisseau :
>
> ```python
> def deplacer():
>     '''
>     modifie les coordonnées du tableau du vaisseau
>     : pas de return, effet de bord sur vaiseau
>     '''
>     if pyxel.btn(pyxel.KEY_Q):
>         pyxel.quit()
>     if pyxel.btn(pyxel.KEY_RIGHT) and vaisseau[0] < 256 - 24 :
>         vaisseau[0] += 4
>     if pyxel.btn(pyxel.KEY_LEFT) and vaisseau[0] > 0 :
>         vaisseau[0] -= 4
> ```
>
> _Remarque :_ le vaisseau ne doit pas pouvoir sortir de la fenêtre !
>
> * Dans la fonction `calculer`, appelez la fonction `deplacer` .
>
> Evidemment, on teste !!

## Les tirs

Voilà donc comment le vaisseau tire :

* Nous aurons besoin d'une variable globale `tab_tirs` initialialement vide.
* Chaque fois que le joueur presse la touche `ESPACE`, on ajoute un tir au tableau de tirs, à condition qu'il n'y en ait pas déjà trop. 
* Un tir sera modélisé par un tableau contenant ses coordonnées.
* Au départ, un tir est initialisé juste au dessus du vaisseau et en son milieu.
*  A chaque appel de la fonction `calculer`, l'ordonnée de chaque tir diminuera de 8, ce qui le fera monter vite.
* On représentera,dans la fonction `afficher`, chaque tir par un rectangle de 8 pixels de haut et de 1 de large.

> * Codez la fonction `ajouter_tir` qui ajoute un nouveau tir au tableau `tab_tirs` avec les coordonnées adéquates.
> * Modifiez la fonction `deplacer` pour appeler la fonction `ajouter_tir` lorsque la touche `KEY_SPACE` est pressée par le joueur.
> * Dans la fonction `dessiner`, pour chaque tir du tableau de tirs, dessinez le.
> * Dans la fonction `calculer`, pour chaque tir du tableau de tirs, faites le monter. Si l'ordonnée du tir devient négative, retirez le du tableau de tir.
>
> Evidemment, on teste au fur et à mesure !



## Destructions ...

### ... des envahisseurs

Si un des tirs touchent un envahisseur, il est détruit.

![](media/detruire_invader.png)

> * A quelle condition sur tir[0], tir[1], invader[0] et invader[1] l'envahisseur sera touché par le tir ?
> * Codez la fonction annexe ci-dessous `est_touche(invader, tir)` qui renvoie `True` si l'invader est touché par le `tir` et `False` Sinon. 
> * Dans la fonction `calculer` , pour chaque envahisseur, pour chaque tir, si un envahisseur est touché par un tir alors, le tir est retiré du tableau de tirs et l'envahisseur est retiré du tableau d'envahisseurs.
>
> Testez au fur et à mesure

### ... du vaisseau

Le vaisseau est détruit lorsqu'un envahisseur le percute. 

![](media/detruire_vaisseau.png)

> * A quelle condition sur invader[0], invader[1], vaisseau[0] et vaisseau[1] l'envahisseur touchera le vaisseau ?
> * Codez la fonction annexe ci-dessous `est_perdu(invader, vaisseau)` qui renvoie `True` si l'invader touche le `vaisseau` et `False` Sinon. 
> * Dans la fonction `calculer` , pour chaque envahisseur si un envahisseur touche le vaisseau alors :
>     * l'envahisseur est enlevé du tableau d'envahisseurs
>     * les coordonnées de vaisseau passe à [300, 300] afin de sortir le vaisseau de la fenêtre et de l'empêcher d'être déplacé
> * Dans la fonction `afficher` , si les coordonnées du vaisseau sont [300, 300], alors on affiche le message `Game Over` grâce à l'instruction `pyxel.text(x, y, chaine_de_caratère, couleur)`.
>
> Testez au fur et à mesure

### piou piou

Ici, on va ajouter un peu de son.

AJouter les deux lignes de code ci-dessous après avoir initialiser la fenêtre graphique (tout en bas) : 

```python
##############lancement du programme
pyxel.init(256, 256, 'Invaders')
pyxel.sound(0).set("a3a2c1a1", "p", "7", "s", 5) # défini le son d'indice 0
pyxel.sound(1).set("a3a2c2c2", "n", "7742", "s", 10) # défini le son d'indice 1
pyxel.run(calculer, afficher)
```

Pour jouer nu son, on utilise la commande `pyxel.play(1, [n], loop = False)` pour jouer le son d'indice n.



> Jouer le son d'indice 0 lorsque le vaisseau tir et le son d'indice 1 lorsque quelque chose explose.



## Un peu de graphisme

### Editeur graphique



Pyxel dispose d'un editeur pour les graphisme (et le son aussi d'ailleurs)

Pour l'ouvrir, dans le menu de Thonny, suivez `outils/console système`.

Tapez maintenant la commande `pyxel edit invaders` pour ouvrir l'éditeur et créer un fichier nommé _invaders.pyxres_.



Vous pouvez maintenant éditer un envahisseur de 16 pixels sur 16 pyxels comme ci-dessous :

![](media/editeur1.png)

_Remarques :_

* **N'oubliez pas de sauvegarder en utilisant CTRL + S et vérifiez que le fichier invaders.pyxelres est bien présent dans votre dossier, à côté du programme invaders.py !!**
* Notez en bas à gauche que vous dessiner l'image no 0. On appelle cela la planche no 0. Vous pouvez utiliser 3 planches numérotez de 0 à 2. 

Vous pouvez ainsi dessiner plusieurs envahisseurs ce qui permettra de faire une petite animation.

![](media/editeur2.png)

### Revenons au code

Pour charger votre fichier `invaders.pyxres` dans votre programme Python, juste après avoir initialisé la fenêtre graphique, on insère l'instruction : `pyxel.load("invaders.pyxres")`.



Ensuite, pour dessiner l'envahisseur à la place du rectangle qui le représentait, on utilise l'instruction 

`pyxel.blt(x, y, no_planche, position_image_x, position_image_y , longueur, hauteur, couleur tranparente)` qui : 

* se place dans la planche _no_planche_
* copie la zone graphique de longueur et hauteur précisée à partir des coordonnées (position_image_x, position_image_y) aux coordonnées (x, y) de la zone de jeu. 



Ainsi `pyxel.blt(invader[0], invader[1],0, 0, 0, 16, 16, 0)` affichera l'invader de 16px sur 16px dessiné à partir de (0, 0) dans la planche 0, aux coordonnées lui correspondant avec le noir (0) comme couleur transparente.

De même,  `pyxel.blt(invader[0], invader[1], 0, 16, 0, 16, 16, 0)` affichera l'invader de 16px sur 16px dessiné à partir de (16, 0) dans la planche 0 aux coordonnées lui correspondant avec le noir (0) comme couleur transparente.

Pour réaliser l'animation il suffit d'afficher le premier invaders si `pyxel.frame_count % 10 < 5` et l'autre sinon.  On peut bien entendu utiliser plus de trois dessins pour peaufiner l'animation...



> * Codez la fonction`afficher_invader(invader)` qui affiche une des images représentant un envahisseur en selon la valeur de `pyxel.frame_count % 10` .
> * Modifiez la fonction `affichage` pour appeler `afficher_invader` au lieu d'afficher un carré.
> * Faites la même chose pour le vaisseau : dessinez un vaisseau (ou plusieurs) de 24px sur 24px dans votre fichier _invader.pyxres_ et remplacez le carré qui le représentait.



## Des boss



Toutes les 500 frames (on utilisera `frame_count % 500`) , on va ajouter un boss à un tableau de boss.

Un boss : 

* est initialisé en haut à gauche de l'écran.
* mesure 16px sur 16px et a un look différent des autres envahisseurs.
* explose si un tir le touche en faisant marquer 1000 points au joueur.
* se déplace vers la droite et, lorsqu'il atteint l'extrémité droite de l'écran, réapparait à l'etrémité gauche de l'écran, un peu plus bas.
* s'il percute le vaisseau ou arrive en bas de l'écran , la partie est perdue.

* Un boss tire toutes les 100 frames. Son tir se dirige vers le bas. S'il percute le vaisseau, la partie est perdue. 





___



Par Mieszczak Christophe

Licence CC BY SA














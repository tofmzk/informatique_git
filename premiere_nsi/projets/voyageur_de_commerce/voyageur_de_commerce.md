# Voyageur de commerce



## Position du problème



Le problème du voyageur de commerce - *Traveling Salesman Problem* TSP -, étudié depuis le 19e siècle, est l’un des plus connus dans le  domaine de la recherche opérationnelle. William Rowan Hamilton a posé  pour la première fois ce problème sous forme de jeu dès 1859.

Le problème du TSP sous sa forme la plus classique est le suivant : « Un voyageur de commerce doit visiter une et une seule fois un nombre  fini de villes et revenir à son point d’origine. Trouvez l’ordre de  visite des villes qui minimise la distance totale parcourue par le  voyageur ». 





Les domaines revenant à poser le même problème sont nombreux :  problèmes de logistique, de transport aussi bien de marchandises que de  personnes, et plus largement toutes sortes de problèmes  d’ordonnancement. Certains problèmes rencontrés dans l’industrie se  modélisent sous la forme d’un problème de voyageur de commerce, comme  l’optimisation de trajectoires de machines outils : comment percer  plusieurs points sur une carte électronique le plus vite possible ?



Pour un ensemble de `n` points, il y a $`\frac {(n - 1) !} 2`$ chemins candidats à considérer.  

| Nombre de villes |             Nombre de chemins candidats             |
| :--------------: | :-------------------------------------------------: |
|        3         |                          1                          |
|        4         |                          3                          |
|        5         |                         12                          |
|        6         |                         60                          |
|        7         |                         360                         |
|        8         |                        2 520                        |
|        9         |                       20 160                        |
|        10        |                       181 440                       |
|        15        |                   43 589 145 600                    |
|        20        |                    beaucoup trop                    |
|        71        | c'est environ le nombre d'atomes dans l'univers ... |

([Page wikipedia *Problème du voyageur de commerce*](https://fr.wikipedia.org/wiki/Problème_du_voyageur_de_commerce)).  



Il n'existe pas de méthode pour déterminer le meilleur trajet sans trouver et comparer tous les trajets... ce qui est impossible. On va donc utiliser une méthode gloutonne pour le résoudre.



## Ça c'est cadeau.

Pour mener à bien ce projet sont fournis :

* deux fichiers au format `csv` (un format texte que vous pouvez ouvrir avec un éditeur de texte ou un tableur et qui offre des données sous forme d'un tableau) :
    * `4_villes.csv` contient 4 villes est leurs coordonnées géographique. Ouvrez le avec un éditeur de texte puis un tableur pour voir son contenu. Ce fichier sera utilisé pour les Doctests.
    *  `tournee_complete.csv`  contient les villes que doit visiter le voyageur. Ouvrez le avec un éditeur de texte.
* un module `module_voyageur` qui contient les fonctions :
    * `lire_donnees` qui renvoie un dictionnaire des villes contenues dans le fichier `csv` lu où les clés sont les noms de villes et les valeurs sont les coordonnées géographiques convertis en flottant. Allez voir comment est codée cette fonction : elle deviendra vite un grand classique !
    * `distance` qui renvoie la distance entre deux villes à partir de leurs coordonnées géographiques.
    * `creer_carte` crée une carte au format HTML en utilisant le module `folium` sur laquelle il trace le circuit passé en paramètre. **Il faudra penser à installer ce module via le menu `outils/gérer les paquets` de Thonny.**



> * Créez un fichier `voyageur.py`
>
> * importez le module `module_voyageur`
>
> * exécutez le code de `voyageur.py`
>
> * Depuis la console, testez :
>
>     ```python
>     >>> import module_voyageur
>     >>> tournee = module_voyageur.lire_donnees('4_villes.csv')
>     >>> tournee
>     ???
>     >>> tournee['Annecy']
>     ???
>     >>> module_voyageur.distance(tournee, 'Annecy', 'Sedan')
>     ???
>     >>> module_voyageur.creer_carte(tournee, ['Annecy', 'Sedan', 'Lens', 'Bordeaux', 'Annecy'])
>     ---> allez ouvrir le fichier carte.html créé dans le répertoire courant !!
> ```
> 
> 

## Résolution du problème

Nous allons procéder de façon gloutonne :

* On choisira une ville de départ
* A chaque étape, on choisira pour ville suivante la ville la plus proche de la ville courante
* Pour optimiser le résultat, on comparera les itinéraires obtenus en choisissant toutes les villes, les une après les autres,  comme ville de départ.



### Ville la plus proche

On appelle `tournee` le dictionnaire des villes que doit visiter le voyager de commerce.
On suppose que celui-ci démarre d'une des villes, on pourra dans un  premier temps considérer qu'il part de la première ville de la liste. 

Pour les essais et tests, on privilégiera le fichier `4_villes.csv`. 



>  Réaliser une fonction `trouver_ville_plus_proche(tournee, ville)`, de paramètre `tournee` et `ville` qui renvoie le nom de la ville la plus proche du nom de ville passé en paramètre, figurant elle-même dans la tournée.  
>
> ```python
> def trouver_ville_plus_proche(tournee, ville):
>        '''
>        renvoie le nom de la ville présente dans la tournée la plus proche de la ville pasée en paramètre.
>        : param tournee un dictionnaire de villes (dict)
>    	: param ville le nom d'une ville (str)
>    	: return le nom de la ville la plus proche (str)
>    	>>> v = module_voyageur.lire_donnees('4_villes.csv')
>    	>>> trouver_ville_plus_proche(v, 'Annecy')
>    	'Sedan'
>     	'''
> ```
> 
>    On n'oubliera pas les lignes habituelles pour les Doctest :
> 
>```python
> ################################################
>###### doctest : test la correction des fonctions
> ################################################
> 
> if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
>  import doctest
>  doctest.testmod(verbose=False)
> ```
>    
>    
> 
>Testez dans la console :
> 
>```python
> >>> v = module_voyageur.lire_donnees('4_villes.csv')
>>>> trouver_ville_plus_proche(v, 'Bordeaux')
> ???
> 
> ```
> 







### Génération d'un circuit glouton



> Créer la fonction `determiner_circuit` ,de paramètres `tournee` et `ville_depart` , qui renvoie la liste des noms des villes à parcourir en choisissant à chaque étape la ville la plus proche non encore visitée grâce à la fonction `trouver_ville_plus_proche`. Le circuit **commence et se termine par la ville de départ**.
>
> ```python
> def determiner_circuit(tournee, ville_depart):
>       	'''
>    	renvoie le circuit commençant par la ville de départ en faisant le choix de la ville la plus proche non encore visitée à chaque étape
>    	: param tournee le dictionnaire des villes à visiter (dict)
>    	: param ville_depart le nom de la ville de départ (str)
>    	return la liste des nms des villes (list)
>    	>>> villes = module_voyageur.lire_donnees('4_villes.csv')
>    	>>> determiner_circuit(villes, 'Annecy')
>    	['Annecy', 'Sedan', 'Lens', 'Bordeaux', 'Annecy']
>    	'''
> ```
> 
> 

_Remarque :_

Pensez à un moyen de ne pas calculer les distances avec des villes déjà visitées. Peut-être générer à chaque étape un dictionnaire de villes disponibles ?



> * Utilisez la fonction `creer_carte` du module `module_voyageur` pour générer une carte au format HTML de notre circuit (Allez voir la documentation de la fonction pour l'utiliser correctement).
> * Ouvrez la carte 
> * Testez plusieurs circuits avec le fichier `tournee_complete.csv` en changeant la ville de départ.



### Longueur d'un circuit



> Définissez une fonction `calculer_longueur_circuit`, de paramètres `circuit`, une liste de nom de villes, qui renvoie la longueur totale du circuit.
>
> ```python
> def calculer_longueur_circuit(tournee, circuit):
>        '''
>        renvoie la longueur totale du circuit
>        : param circuit une liste de ville à visiter
>        type list
>        : return la longueur du circuit
>        type float
>        >>> villes = module_voyageur.lire_donnees('4_villes.csv')
>        >>> circuit = determiner_circuit(villes, 'Annecy')
>        >>> calculer_longueur_circuit(villes, circuit)
>        2176.285692573849
>     	'''
> 
> ```



### Choisir le bon départ

> Créez une fonction `chercher_circuits, de paramètre `tournee qui renvoie un tableau de tous  les circuits possibles en prenant tour à tour chaque villes de la `tournee` comme ville de départ.
>
> Testez depuis la console :
>
> * ```python
>    >>> v = module_voyageur.lire_donnees('4_villes.csv')
>     >>> choisir_circuit(v)
>         ???
>    ```
>
> 

### Le plus court

Il reste à déterminer le plus court des circuits grâce à la fonction `renvoyer_plus_court_circuit(tournee)` qui prend en paramètre un dictionnaire de villes et renvoie le circuit le plus court obtenu avec notre méthode gloutonne.



> Définissez, documentez et codez cette fonction !







## Conclusion



> * Déterminer la ville de départ idéale de la  tournée complète (avec le fichier `tournee_complete.csv`), sa longueur et afficher le circuit sur une carte.
>
>     ```txt
>                 
>     ```
>
>     
>
> 
>
> * Quelle particularité à ce circuit ?
>
>     ```txt
>                 
>     ```
>
>     





____

Mieszczak Christophe CC- BY - SA

A partir des documents de Luc Bournonville et de la formation NSI de l'université de Lille 1
# coding: UTF-8
import math
import folium

def lire_donnees(nom_fichier_CSV, separateur = ';'):
    """
        lit le fichier csv dans le repertoire data et revoie la liste des donnees
        : param nom_fichier_CSV
        type str
        : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
        type str
        return liste des données
        type liste
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    try :
        lecture = open(nom_fichier_CSV, 'r', encoding='utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le ficier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
    lecture.close()
    dict_villes = {}
    for ligne in toutes_les_lignes:
        dict_villes[ligne[0]] = (float(ligne[1]), float(ligne[2]))
    return dict_villes
    



def distance (dico_villes, depart, arrivee) :
    """
    Calcul la distance en km entre depart et arrivee
    à partir des angles de coordonnées GPS
    : param dico_villes: dictionnaire de villes
    : param : dict
    : param depart: nom de la ville de départ
    : param arrivee: numero de la ville d arrivee
    : return: float
    CU: depart et arrivee  clés présentes dans  tour
    """
    lat1, lat2 = math.radians(dico_villes [depart][0]), math.radians(dico_villes [arrivee][0])
    long1, long2 =math.radians( dico_villes [depart][1]), math.radians(dico_villes [arrivee][1])
    d = math.acos(math.sin(lat1)*math.sin(lat2)+math.cos(lat1)*math.cos(lat2)*math.cos(long2-long1))*6371

    return d

def creer_carte(dico_villes, circuit, nom_page_html = 'carte'):
    """
    place des villes sur un carte au sein du page html, sauvegardée ensuite. 
    :param dico_villes: (dict) dictionnaire de villes
    :param circuit: (list) liste de villes
    :param nom_page_html:(str) nom de la page html (sans extension)
    :CU: les villes de circuit sont des clés de dico_villes
    """
    gps = []
    for ville in circuit :
        gps.append((dico_villes[ville][1], dico_villes[ville][0]))
    carte = folium.Map(location = gps[0],zoom_start=6, popup = circuit[0])
    folium.PolyLine(gps, color="blue", weight=2.5, opacity=0.8).add_to(carte)
    carte.save(nom_page_html + '.html')
# coding: utf-8
'''
    :author: Mieszczak C
    :date: 23     sept 2019
'''       

def creer_liste_mots():
    """
    : Après lecture d'un fichier txt, renvoie une liste de mots en majuscule sans accent
    : return : list
    """
    fichier = open('complet.txt', 'r', encoding = 'UTF8')
    chaine_de_mots = fichier.read()
    fichier.close()
    liste_de_mots=chaine_de_mots.split('\n')
    for mot in liste_de_mots :
        if len(mot)<2 :
            liste_de_mots.remove(mot)
    liste_de_mots.sort()
    return liste_de_mots

def passer_en_majuscule(chaine):
    """
     passe la chaine en majuscule sans caractère accentué
     param chaine
     type str
     return
     type str
    """
    chaine=chaine.upper()
    long = len(chaine)
    maj={'Â':'A', 'Ê':'E', 'Î':'I', 'Ô':'O', 'Û':'U', 'Ä':'A', 'Ë':'E', 'Ï':'I', 'Ö':'O', 'Ü':'U',
             'À':'A', 'Æ':'AE',  'Ç':'C', 'É':'E', 'È':'E', 'Œ':'OE', 'Ù':'U',
             '"':''}
    for i in range(0,long):
        if chaine[i] in maj :
            chaine = chaine[:i] + maj[chaine[i]] + chaine[i+1:]
    return chaine


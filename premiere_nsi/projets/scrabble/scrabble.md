 # Scrabble

Pour ce mini-projet, nous allons utiliser le _module_mots_ qui gère une looooongue liste de mots. Tout ce trouve déjà prêt dans le répertoire _SCRABBLE_. 



Créez un nouveau fichier `scrabble.py` .

Remarquez la liste constante (nommée en majuscule donc) en haut de votre code : elle contient la liste complète des mots que nous allons utiliser :

```python
import module_mots
LISTE_COMPLETE = module_mots.creer_liste_mots()
```



Pour rappel, voici ce que vous avez codée dans le cours sur les _dictionnaires_. Cela nous sera très utile.

```python
def calculer_score(mot) :
    '''
    renvoie le score du mot selon le dictionnaire du scrabble
    : param mot(str)
    : return (int) le score
    '''
    valeur_lettre = { 'A' : 1,
                'B' : 2,
                'C' : 2,
                'D' : 2,
                'E' : 1,
                'F' : 4,
                'G' : 2,
                'H' : 4,
                'I' : 1,
                'J' : 8,
                'K' : 10,
                'L' : 1, 
                'M' : 2,
                'N' : 1,
                'O' : 1,
                'P' : 3,
                'Q' : 8,
                'R' : 1, 
                'S' : 1,
                'T' : 1,
                'U' : 1,
                'V' : 4,
                'W' : 10,
                'X' : 10,
                'Y' : 10,
                'Z' : 10
                }
    score = 0
    for lettre in mot :
        score += valeur_lettre[lettre]
    return score
```





> Pour chaque question ci-dessous, en plus de répondre, indiquez les lignes de code, qui vous ont permis de trouver cette réponse.
>
> * Combien y-a-t-il de mot dans cette liste ?
>
>     ```python
>     >>>
>     ```
>
>     
>
> * Le mot 'RECAPITULATION' fait-il partie de la liste ?
>
>     ```python
>     >>>
>     ```
>     
> * Combien de mots commencent par un 'Z' ?
>
>     ```python
>                 
>     ```
>
>     
>
> * Quels mots de la liste sont écrits à la fois avec un 'X' et un 'Z' ?
>
>     ```python
>     
>     ```
>
> * Quels mots de la liste comportent plus de cinq 'E' ?
>
>     ```python
>                 
>     ```
>
>     
>
> * Quel est le mot le plus long de la liste ?
>
>     ```python
>     
>     ```
>     
> * Quel mot de la liste rapporte le plus de points au SCRABBLE ?
>
>     ```python
>                 
>     ```
>     
>     
>     
> * Quel est le dernier mot de la liste par ordre alphabétique ? Tester les lignes ci-dessous avant de le chercher en vous inspirant de la question précédente.
>
>     ```python
>     >>> 'A' < 'B'
>     ???
>     >>> 'B' > 'C'
>     ???
>     >>> 'ARTHUR' < 'ALBERT'
>     ???
>     ```
>
>     





## On commence doucement

> Codez la fonction `existe(mot, tab)` qui renvoie _True_ si le mot est présent dans _tab_ et _False_ sinon.
>
> Cette fonction n'utilisera pas l'opérateur Pytonh `in` mais une dichotomie.



## C'est d'écrire ça ?

> Réalisez une fonction `est_possible(mot, lettres_disponibles)`, de paramètres _mot_ et _lettres_disponibles_ deux chaînes de caractères qui renvoie `True` s'il est possible d'écrire le _mot_ en utilisant les lettres disponibles dans la chaîne _lettres_disponibles_ et `False` sinon.
>
> * On ne prendra pas en compte le joker.
>
> * Documentez votre fonction.
>
> * On utilisera le Doctest ci-dessous et vous en ajouterez deux autres.
>
>     ```python
>     >>> est_possible('ASILE','AEIPLMNS')
>     True
>     >>> est_possible('EMPLOIS','AEIPLMNS')
>     False
>     ```

​     

​     


## Mode triche ON



> Réaliser une fonction `chercher_mots(lettres_disponibles)`, de paramètre  _lettres_disponibles_, la chaîne de caractères contenant les ... lettres disponibles, qui renvoie une liste de  tous les mots de notre liste complète de mots qu'il est possible d'écrire avec ces lettres.
>
>  
> 
>  * On ne prendra pas en compte le joker.
> 
>  * Documentez votre fonction.
> 
>  * On utilisera le Doctest ci-dessous.
> 
>      ```python
>      >>> chercher_mots('AEPLM')
>      ['ALE', 'LA', 'PE', 'LE', 'AE', 'MAL', 'PALE', 'AME', 'ALPE', 'EM', 'ME', 'MEL', 'AMPLE', 'MALE', 'PALME', 'PA', 'LAMPE', 'LM', 'AP', 'PAL', 'LAME', 'MA', 'AL']
>      ```
> 
> ​     



## Affichage

> Créer une fonction `afficher_mot(mot)` de paramètre _mot_, une chaîne de caractères, qui affiche dans la console le mot, le nombre de caractères qui le compose et son score au SCRABBLE.
>
> Documentez votre fonction, comme toujours.
>
> Voici un exemple qui pourra servir de Doctest :
>
> ```python
> >>> afficher_mot('XYLOPHONE')
> XYLOPHONE --> 9 caractères, 32 points.
> ```






>  Créer une fonction `afficher_mots(liste_de_mots)`, de paramètre _liste_de_mots_, une liste de chaînes de caractères, qui affiche dans la console tous les mots de la liste de mots, leur longueur et leur score.
>
> Documentez votre fonction, comme toujours.





## Un peu de tri



**On ne peut aborder cette partie qu'après le chapitre sur les tris.**

Nous allons nous servir `tris.py` comme d'un module :  Importez `tris` (il faudra copier_coller le fichier `tris.py` dans le même répertoire que `scrabble.py`)





**Ex 1:** tab = ['BONJOUR', 'HI',  'GUTENTAG', 'SALAM', 'BUENOSDIAS',  'AYO']

* Réalisez un comparateur `comp_pt_scrabble` permettant de trier ce tableau selon la valeur du mot au SCRABBLE. 

```python
def comp_pt_scrabble(mot1, mot2):
    '''
    compare les mots selon leurs scores au scrabble
    : Params  
    	mot1 (str)
        mot2 (str)
    : return -1 (si croissant),1 (décroissant)ou 0 sinon
    '''

```



* Triez !

    

**Ex 2**: 

* Réalisez une fonction `score_maximum`, de paramètre _liste_ , une liste de mots, qui renvoie la liste trier selon l'opérateur `comp_pt_scrabble.`



* Réalisez une fonction `longueur_maximale`, de paramètre _liste_ , une liste de mots, qui renvoie la liste trier selon le comparateur `comp_longueur_mot`.

    

_Remarque_ : 

Attention, si le comparateur utilisé est codé dans le module `tris.py` , il faudra l'appeler en précisant qu'il est codé dans le module :`tris.comparateur` ...





______

Par Mieszczak Christophe CC BY SA

Licence CC BY SA








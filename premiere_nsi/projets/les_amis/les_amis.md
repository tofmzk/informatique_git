 # Les amis

Voici un graphe représentant les liens d'amitié réciproques dans un petit groupe de personnes sur un réseau social.

![graphe non orienté](media/graph_amitie.jpg)

>  Créez un programme `graphe.py`  : Nous allons coder un algorithme pour se déplacer sur le chemin le plus court d'un sommet à un autre.



##  Modélisons les relations

Nous allons définir les variable globales, écrites en majuscules, dont nous auront besoin dans notre programme.



* On modélise chaque membre du groupe par son prenom, sans majuscule.

```python
MEMBRES = ['etta', 'ella', 'mick', 'joe', 'jim', 'ray']
```



* On modélise les amis d'un membre par un tableau de leurs noms, toujours en majuscule. Par exemple, les amis de 'ray' seront donc modélisés par le tableau AMI_RAY = ['etta', 'ella', 'mick'].



> En Python, déclarez les constantes, chaînes de caractères, représentant les amis de chaque membre.
>
> _Remarque :_ les bonnes pratiques préconisent de noter les constantes en majuscule.
>
> ````Python
> AMI_RAY = ['etta', 'ella', 'mick']
> AMI_MICK = 
> AMI_JOE = 
> AMI_JIM = 
> etc ... 
> ````
> 
>



## Une fonction de vérification

> Ecrivez une fonction `est_membre` de paramètre _tab_amis_, un tableau d'amis, qui renvoie _True_ si tous les amis font partis des membres et _False_ sinon. 



```python
def est_membre(tab_amis):
    '''
    renvoie True si tous les amis font partis des membres et False sinon.
    : param tab_amis (list)
    : return (boolean)
    >>> est_membre(AMI_RAY)
    True
    >>> est_membre(AMI_MICK)
    True
    >>> est_membre(AMI_JOE)
    True
    >>> est_membre(AMI_JIM)
    True
    >>> est_membre(AMI_ELLA)
    True
    >>> est_membre(AMI_ETTA)
    True
    >>> est_membre(['ella', 'james', 'joe'])
    False
    '''
```





## Choisissons un ami au hasard



Nous allons créer une fonction permettant de choisir au hasard un ami dans un tableau d'amis.  Pour cela, il faut importer la bibliothèque `random` et utiliser la fonction `random.choice(tableau)`.



> Définissez la fonction `choisir_ami` de paramètre _membre_,  une chaîne de caractères  représentant un membre, et renvoyant un ami choisi au hasard parmi ses amis. 
>
> * Documentez votre fonction.
>
> * Testez votre fonction via la console :
>
> ````Python
> >>> choisir_ami('ray')
> ???
> >>> choisir_ami('etta')
> ???
> >>> ...
> ````
> 
>

​	

## Cherchons un chemin d'un ami à un autre, au hasard



Nous allons maintenant chercher un chemin **au hasard** pour relier un membre de départ à un membre de destination. 

* Le nombre de protagonistes étant faible et tout le monde étant joignable, on considère qu'un chemin est toujours possible.

	

* L'algorithme est le suivant :

	````markdown
	On part d'un membre de depart 
	On souhaite arriver à un membre de destination
	
	Le chemin contient pour l'instant uniquement le membre de départ
	Tant que le membre de départ n'est pas celui de destination 
	    On remplace le membre de départ par un de ses amis choisi au hasard
	    On ajoute le membre de départ au chemin
	On renvoie le chemin    
	```
	
	
	

 > * Définissez la fonction `chercher_chemin`de paramètres _depart_ et _destination_ , deux chaînes de caractères différentes représentant deux membres, qui renvoie un tableau contenant des membres par qui passer pour les joindre en choisissant au hasard.
 >
 > 
 >
 > * Testez votre fonction via la console :
 >
 > ```` Python
 > >>> chercher_chemin('jim','eta') # à faire plusieurs fois
 >  ???
 > >>> ....
 > ````
 >
 > Le chemin se faisant au hasard, vous n'obtiendrez pas toujours le même tableau !



## Cherchons le chemin le plus court



* Pour trouver le chemin le plus court, on suppose que si on cherche au hasard un chemin un grand nombre de fois, l'un de ces chemins sera le plus court. **Une telle stratégie s'appelle une marche aléatoire**.



* On applique donc l'algorithme suivant :

````markdown
On veut rechercher le plus court chemin en réalisant nbre_XP recherches d'un chemin au hasard

On choisit un chemin de référence en effectuant une première recherche
Pour i dans la plage allant de 0 inclus et nbre_XP exclue
    On cherche un nouveau chemin au hasard
    Si la longueur de ce chemin est inférieure à celle du chemin de reférence alors 
       ce chemin devient le chemin de référence
On renvoie le chemin de référence pour terminer

````





> Définissez la fonction `rechercher_chemin_court`de paramètres _depart_  et _destination_ , deux membres différents, et _nbre_XP_ un entier décidant du nombre de recherches de chemins au hasard à effectuer.
>
> * Cette fonction renvoie un tableau représentant le plus court chemin trouvé parmi les _nbre_XP_ chemins trouvés au hasard entre _depart_ et _destination_.
>
>   * Documentez votre fonction.
>
> * Testez votre fonction via la console :
>
>   ````Python
>   >>> chercher_chemin_plus_court('mick','ella',5)
>    ???
>    >>> chercher_chemin_plus_court('mick','ella',100)
>    ???
>   ````
> 
> 



## Evolution : changer de structure de données



Nous reviendrons ici après avoir découvert les dictionnaires.



Nous utiliserons donc un dictionnaire pour modéliser les relations d'amitiés. Complétez le dictionnaire ci-dessous :

```python
amis = {'ray' :['etta', 'ella', 'mick'],
			'ella' : ['etta', 'ray'],
            
			}
			

```



Testez dans la console :

```python
>>> amis['ray']
???
>>> amis['mick']
???
```



>  
>
> Quelles fonctions seront impactées par ce changement de structure ?
>
> ```txt
> 
> ```
>
> 



> Modifier les fonction suivantes pour les adapter à cette nouvelle structure :
>
> * `verification`
>
> * `choisir_amis(membre)` qui prendra en paramètre un _membre_ et en renverra un ami. Ca se fait en une seule ligne de code !
>
>     

__________

Par Mieszczak Christophe CC BY SA

_source images personnelle_
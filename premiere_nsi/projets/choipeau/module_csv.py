

def lire_donnees(nom_fichier_CSV, separateur = ';'):
    """
        lit le fichier csv dans le repertoire data et revoie la liste des donnees
        : param nom_fichier_CSV
        type str
        : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
        type str
        return liste des données
        type liste
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    try :
        lecture=open(nom_fichier_CSV,'r',encoding='utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le ficier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n").split(separateur) # enleve les "\n" et convertit en list
    lecture.close()
    return toutes_les_lignes


def convertir_csv(liste, separateur  = ';'):
    '''
    renvoie une liste de chaine compatible avec l'écriture d'un fichier en csv
    : param : liste_villes une liste de villes
    type list
    : param  :separateur par défaut ';'
    type str
    return une liste de chaines
    type list
    '''
    liste_chaines = []
    for elt in liste :
        chaine = ''
        for i in range(len(elt)) :
            chaine += elt[i]
            if i < len(elt) - 1 :
                chaine += separateur
        chaine += '\n'
        liste_chaines.append(chaine)
    return liste_chaines

def sauver(liste, nom_fichier_CSV, separateur = ';'):
    """
    écrit dans le fichier csv la liste des chaines passées en pramètre
    : param nom_fichier_CSV
    type str
    : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
    type str
    : ne renvoie rien mais crée un fichier ou écrit dan sun fichier existant
    """
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    liste = convertir_csv(liste, separateur)
    try :
        ecriture=open(nom_fichier_CSV,'w',encoding='utf-8') # ouvre un canal en écriture vers fichier2
        ecriture.writelines(liste)
        ecriture.close()
    except:
        print("!!!!!!!!!! impossible d'ecrire dans " +nom_fichier)

def convertir_csv(liste, separateur):
    '''
    convertie au format str la liste passée en parametre
    : param liste
    type list
    : return chaine de caractère
    type str
    '''
    chaine = ''
    for i in range(len(liste)):
        for j in range(len(liste[i])):
            chaine += liste[i][j]
            if j < len(liste[i]) - 1 :
                chaine += separateur
        if i < len(liste) - 1 :
            chaine += '\n'
    return chaine

def afficher(donnees, nbre = 10):
    '''
    affiche nbre lignes dans les données
    : param donnees
    type liste
    : param nbre
    type int
    '''
    try :
        for elt in donnees[1:nbre + 1]:
            for i in range(len(elt)):
                print(donnees[0][i],' : ',elt[i], '  ', end = '')
            print('')
    except :
        pass
    
        
################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose=False)

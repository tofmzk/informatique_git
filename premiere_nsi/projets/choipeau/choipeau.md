# Le Choipeau  



## Le rôle du Choipeau



Lorsque les nouveaux sorciers entrent à Poudlard en première année, le Choipeau décide à quelle maison il appartiendra. Cette maison peut être Serpentar, Pouffsoufle, Gryffondor ou Serdaigle.



Pour attribuer la maison, le Choipeau se base sur son expérience. Il connaît les anciens élèves de chaque maison et les caractéristiques qui lui permettent de fait le chois de la maison idéale :

* Nom
* Courage
* Loyauté
* Sagesse
* Malice

Aussi, lorsqu'un nouvel arrivant se place devant lui, il lit les caractéristiques de ce nouvel élève, calcule les k plus proches voisins de cet élève dans sa base de donnée personnelle puis lui attribue la maison la plus présente parmi ses voisins. En case de doute, il élargit la recherche à plus de voisins.



## Modélisation



* Chaque élève est modélisé par une liste du type [Nom, Courage, Loyauté, Sagesse, Malice, Maison] où :

    * Nom est le nom de l'élève, de type _str_.
    * Courage, Loyauté, Sagesse et Malice sont 4 entiers (des _int_ donc) entre 0 et 10.
    * Maison est la maison de l'élève si elle est connue et '?' sinon. Maison est donc du type _str_.

* Nous disposons des données du Choipeau sauvegardées dans le fichier `choipeau.csv` joint : ouvrez ce fichier  pour y jeter un coup d'oeil. Combien d'élèves y sont présents ?

    

    Cette année, quatre élèves se présentent devant le Choipeau. Quelle sera leurs maisons respectives ?

    *  hermione = ['Hermione', 8, 6, 6,  6, '?']
    *  drago = ['Drago', 6, 5, 5, 8, '?']
    * cho = ['Cho', 7, 6, 9, 6, '?']
    * cedric = ['Cédric', 7, 10, 5, 5, '?']

    

## Module_csv



Vous disposez du module `module_csv` qui propose ici la fonction`lire_donnees` qui renvoie la liste des données présentes dans un fichier csv. Toutes les valeurs sont au format _str_ par défaut.



## Implentation



### Importer la base



> Définissez et codez une fonction `importer_base` , sans paramètre, qui renvoie la liste des valeurs de la base après avoir convertit les valeurs numériques au format _int_.
>
> ```python
> def importer_base():
>        '''
>        renvoie la liste des donnees après avoir convertit les valeurs numériques au     format _int_.
>        : return (list)
>        '''
>    ```
> 
>



### Calculer une distance

Pour calculer la distance entre deux élèves, on utilisera la distance euclidienne que nous avons déjà vu en dimension 2, mais en dimension 4 puisque nous avons 4 attributs numériques :

$` d = \sqrt { (x_1 - x_2) ² + (y_1 - y_2)² + (z_1 - z_2)² + (t_1- t_2) ²}`$ où :

* $`x_i`$ est la valeur du _Courage_
* $`y_i`$ est la valeur de la _Loyauté_
* $`z_i`$ est la valeur de la _Sagesse_
* $`t_i`$ est la valeur de la _Malice_



> Réalisez une fonction `distance` de paramètres _eleve1_ et _eleve2_, deux liste d'élèves, qui renvoie la distance entre ce deux élèves.
>
> ```python
> def distance(eleve1, eleve2):
>        '''
>        renvoie la distance entre deux élèves
>        : param elve1, eleve2 deu listes d'éléve (list)
>        : return la distance (float)
>        >>> round(distance(['Hermione', 8, 6, 6,  6, '?'], ['Drago', 6, 5, 5, 8, '?']),2)
>        3.16
>        '''
>    
>    ```
> 
> 



### Lister toutes les distances

> Définissez la fonction `lister_distances`, de paramètres _base_ et _eleve_, qui renvoie une liste de tuple du genre [(maison1, distance1), (maison2, distance2) ...] où les maisons sont celles présentes dans la table d'apprentissage et les distances celles entre les élèves de la table d'apprentissage et celui passé en paramètre.
>
> ```python
> def lister_distances(base, eleve):
>        '''
>        renvoie une liste de tuple du genre (maison, distance) 
>     	: param eleve une liste de caractéristique d'un élève (list)
>     	: param base la base complète connue (list)
>     	: return (list of tuple)
>     	'''
>    ```
>    
>    

### Trouver les k plus proches voisins

> Codez la fonction `envoyer_k_plus_proches_voisins` , de paramètres _base_, _eleve_ et _k_ , qui renvoie un dictionnaire des effectifs des maisons des _k_ plus proches élèves de l'_eleve_.
>
> ```python
> def renvoyer_k_plus_proches_voisins(base, eleve, k):
>     '''
>     renvoie un dictionniaire des maisons des k plus proches voisins de l'élèves
>     : param eleve une liste de caractéristique d'un élève
>     type list
>     : param base la base complète connue
>     type list
>     : param k le nbre de voisins souhaité
>     type int
>     : return un dictionnaire avec pour clé les maisons et pour valeurs leur effectif parmi les k voisins
>     type list
>     >>> base = importer_base()
>     >>> renvoyer_k_plus_proches_voisins(base, ['Hermione', 8, 6, 6,  6, '?'], 5)
> 	{'Serpentar': 1, 'Poufsouffle': 0, 'Gryffondor': 4, 'Serdaigle': 0}
> 
>     '''
>     k_proches_voisins = {'Serpentar' : 0,
>                       'Poufsouffle' : 0,
>                       'Gryffondor' : 0,
>                       'Serdaigle' : 0}
> ```
>



_Remarque :_ 

Pour comprendre comment trier une liste de tuples, testez les lignes ci-dessous :

```python
>>> liste = [('a', 3), ('b', 2), ('c', 1)]
>>> liste = sorted(liste, key = lambda x : x[0])
>>> liste
???
>>> liste = sorted(liste, key = lambda x : x[1])
>>> liste
???
```





### Déterminer la maison d'un élève

> Définissez une fonction `renvoyer_maison` , de paramètres _base_, _eleve_ et _k_, qui renvoie la maison d'effectif maximum parmi les k plus proches voisins de l'_eleve_ s'il y en a une. En cas d'égalité entre plusieurs maisons, on renverra une chaîne de caractère contenant les maisons séparées d'une virgule.
>
> ```python
> def renvoyer_maison(base, eleve, k):
>        '''
>        renvoie la maisond'effectif maximum parmi les k plus proches voisins de   l'_eleve_ s'il y en a une. En cas d'agalité entre plusieurs maisons, on renverra une chaine de caractère contenant les maisons séparées d'une virgule.
>        : param base
>        type liste
>        : param eleve
>        type list
>        : param k
>        type int
>        : return la maison
>        type str
>        >>> base = importer_base()
>        >>> dean = ['Dean', 9, 8, 4, 7, '?']
>        >>> renvoyer_maison(base, dean, 3) == 'Gryffondor'
>        True
>        >>> maleo = ['Maleo', 8, 7, 6, 9, '?']
>        >>> renvoyer_maison(base, maleo, 5) == 'Serpentar,Gryffondor'
>        True 
>        '''
> ```
>
> 

### Tester la base

Pour déterminer quelle valeur de _k_ est la meilleure pour commencer nos tests, nous allons déterminer le taux d'erreur en utilisant la table d'apprentissage.



> Réaliser une fonction `tester_k`, de paramètre _base_ et _k_, qui teste la fonction `renvoyer_maison` avec _k_ voisins sur tous les les élèves de la _base_ puis renvoie le taux d'erreur.
>
> ````python 
> def tester_k(base, k):
>     '''
>     renvoie le taux d'erreur dans la base en recherchant avec k voisins
>     : param base
>     type list
>     : param k
>     type int
>     : return le taux d'erreur*
>     type float
>     '''
>     nb_erreurs = 0
>     nb_eleves = 0
> ````
>
> 



* Complétez le tableau ci-dessous :

| valeurs de k  |  3   |  5   |  7   |  9   |
| ------------- | :--: | :--: | :--: | :--: |
| taux d'erreur |      |      |      |      |



* Quelle valeur semble la meilleure ?



### Retrouvons nos quatre petits nouveaux

Il nous reste à déterminer la maison de nos quatre nouveaux. On va commencer avec _k = 3_ et augmenter la valeur de _k_ en cas de litige.

*  hermione = ['Hermione', 8, 6, 6,  6, '?'] 
*  drago = ['Drago', 6, 5, 5, 8, '?']
* cho = ['Cho', 7, 6, 9, 6, '?']
* cedric = ['Cédric', 7, 10, 5, 5, '?']





__________________________



Mieszczak Christophe CC- BY - SA


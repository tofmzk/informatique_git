# Touché - coulé 


## Règle du jeu



![touché coulé - wikipédia CC BY SA auteur [Starex Novelty Co](https://fr.wikipedia.org/w/index.php?title=Starex_Novelty_Co&action=edit&redlink=1)](https://tse4.mm.bing.net/th?id=OIP.QcPehchfIFmVX0_OegaEDwAAAA&pid=Api)



Dans une grille de 10 cases sur 1à cases, numérotée de 1 à 10 verticalement et de A à J horizontalement, un joueur (ce sera l'ordinateur pour nous) cache des bateaux sans les faire se chevaucher :

* 1 sous-marins qui occupent chacun une case.
* 1 torpilleur qui occupe 2 cases.
* 1 cuirassé qui occupe 3 cases.
* 1 portre-avions qui occupe 4 cases.



Le deuxième joueur doit découvrir l'amplacement des bateaux. Pour cela :

* Il propose une case de la grille, par exemple "A2".
* La case proposée est alors découverte et il y a deux possibilités :
    * Soit un bateau est touché et le joueur 1 annonce de quel type de bateau il s'agit et, si c'est le cas, qu'il est coulé.
    * Soit c'est un coup dans l'eau et le joueur 2 perd un essais

* Tant que le joueur 2 dispose d'essais, il peut proposer une case.
* La partie s'arrête lorsque tous les essais sont épuisés (dans ce cas le joueur 2 a perdu) ou que tous les bateaux sont coulés (dans ce cas le joueur 2 a gagné).



## Modélisons l'océan.



Il n'y a pas qu'une façon de faire. 



Nous allons utilisé une structure de liste de listes pour modéliser la grille de jeu. La liste ci-dessous correspond à un océan de 10 cases sur 10 cases, sans aucun bateau.



```Python
>>> grille = [ 
    		   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
             ]
              
    
```



> A quel élément du tableau correspondent les cases :
>
> * A2 : grille\[?][?]
> * F4 : grille\[?][?]
> * E8 : grille\[?][?] 



> Créez une fonction `generer_grille` qui renvoie la liste ci-dessus en utilisant une expression régulière et la méthode `append`



Pour gagner un peu de temps :

```python
# coding: utf-8
import random

def generer_grille():
    '''
    renvoie une grille de 10 cases sur 10  cases
    return grille
    type list
    >>> grille = generer_grille()
    >>> grille
    [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    >>> grille[1][2] = 1
    >>> grille
    [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    '''
    grille = []
    

```



Nous utiliserons des entiers pour coder ce que peut contenir une case :

* 0  pour une case vide non dévoilée.
* 5 pour une case vide dévoilée.
* 1 pour une case non dévoilée contenant un sous-marin 
* 2 pour une case non dévoilée contenant une partie d'un torpilleur 
* 3 pour une case non dévoilée contenant une partie d'un croiseur 
* 4 pour une case non dévoilée contenant une partie d'un porte-avions
* Une case jouée contenant un bateau augmentera de 5 de façon à conserver une information sur le batiment touché tout en la différenciant d'une case identique non encore dévoilée.





## Accesseurs



> Réalisez une fonction `set_cell` de paramètre _grille_, une liste représentant le jeu,  _x_, _y_ et _valeur_, trois entiers, qui affecte _valeur_ à la case de coordonnées (_x_;_y_) du jeu grâce à un effet de bord sur grille.



> Réalisez une fonction `get_cell` de paramètre _grille_, une liste représentant le jeu,  _x_, et  _y_, deux entiers, qui renvoie le contenu de la case de coordonnées (_x_;_y_).



_Remarque :_

ATTENTION : le première élément d'une liste a pour indice 0 mais ici, les cases sont numérotées de 1 à 10.



Je suis trop gentil :

```Python
def set_cell(grille, x, y, valeur):
    '''
    affecte _valeur_ à la case de coordonnées (_x_;_y_) du jeu.
    param grille
    type list
    param x, y, valeur
    int
    effet de bord sur grille
    >>> grille = generer_grille()
    >>> set_cell(grille, 2, 1, 1)
    >>> grille
    [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    '''
    
    

def get_cell(grille, x, y):
    '''
    renvoie la valeur contenue dans la case de coordonnées (_x_;_y_) du jeu.
    param grille
    type list
    param x, y
    int
    return la valeur de la case
    type int
    >>> grille = generer_grille()
    >>> set_cell(grille, 2, 1, 1)
    >>> get_cell(grille, 2, 1)
    1
    >>> get_cell(grille, 1, 2)
    0
    '''
    


```



Nous venons de créer deux fonctions :

* `get_cel`: renvoie le contenue d'une case de notre océan
* `set_cel` : modifie le contenue d'une case de l'océan.

`get_cel` et `set_cel` sont des _assesseurs_ : il donne accès à la grille en lecture et en écriture et rendent le code indépendant de la structure utilisée pour modéliser le jeu : si on décide de modéliser autrement notre océan, seules ces fonctions seront à modifier, pas la suite du code.



**On utilisera donc ces accesseurs dans toute la suite du programme et plus jamais les listes !!**



## Interface Homme - Machine



Nous utiliserons la console pour afficher l'océan en mode texte, via la commande `print`.

Pour nous aider un peu, la fonction d'affichage aura une option permettant de tricher, c'est à dire de choisir entre :

* Le mode _normal_ qui va afficher :
    * '_'  pour les cases non choisies
    *  'S', 'T', 'C' et 'P' respectivement pour les Sous-marins, Torpilleurs, Croiseur et Porte_avions **déjà touchés**.
    * 'X' pour les coups dans l'eau.
* Le mode _triche_ qui va afficher :
    * '_'  pour les cases non choisies
    *  'S', 'T', 'C' et 'P' respectivement pour les Sous-marins, Torpilleurs, Croiseur et Porte_avions qu'ils soient touchés ou pas, de manière à les rendre toujours visibles.
    * 'X' pour les coups dans l'eau.



Pour cela nous allons définir notre fonction`afficher` de la façon suivante :

```python
def afficher(grille, triche =0):
    '''
    affiche la grille de jeu.
    param grille
    type list
    param triche : facultatif vallant 0 par défaut
    type int    
    * Le mode normal, mode par défaut, c'est à dire lorsque triche vaut 0, va afficher :
        * '_'  pour les cases non choisies
        *  'S', 'T', 'C' et 'P' respectivement pour les Sous-marins, Torpilleurs, Croiseur et Porte_avion déjà touchés
        * 'X' pour les coups dans l'eau.
    * Le mode triche, activé lorsque le paramètre triche vaut 1, va afficher :
        * '_'  pour les cases non choisies
        *  'S', 'T', 'C' et 'P' respectivement pour les Sous-marins, Torpilleurs, Croiseur et Porte_avion qu'ils soient touchés ou pas, de manière à les rendre toujours visibles.
        * 'X' pour les coups dans l'eau.
    '''
    if triche == 0:
        cases =['_', '_', '_', '_', '_','X','S','T','C','P']
    else :
        cases =['_', 'S', 'T', 'C', 'P','X','S','T','C','P']
    print('   A B C D E F G H I J')
```



Le rendu attendu pour un océan vide est celui-ci :

```txt
   A B C D E F G H I J
1 |_|_|_|_|_|_|_|_|_|_|
2 |_|_|_|_|_|_|_|_|_|_|
3 |_|_|_|_|_|_|_|_|_|_|
4 |_|_|_|_|_|_|_|_|_|_|
5 |_|_|_|_|_|_|_|_|_|_|
6 |_|_|_|_|_|_|_|_|_|_|
7 |_|_|_|_|_|_|_|_|_|_|
8 |_|_|_|_|_|_|_|_|_|_|
9 |_|_|_|_|_|_|_|_|_|_|
10|_|_|_|_|_|_|_|_|_|_|

```



## Placer les sous-marin

> Réaliser une fonction `placer_sous_marin` ,de paramètre _grille_, une liste représentant le jeu, qui place aléatoirement un sous-marin **sur une case vide** de la  _grille_.
>
> La fonction ne renvoie rien mais modifie la grille passée en paramètre par effet de bord.
>
> * Documentez votre code !
> * Utiliser la fonction `afficher` pour le tester via la console.
>
> 



Le principe de cette fonction est simple :

* on choisit une valeur _x_ au hasard entre 1 et 10
* on choisit une valeur _y_ au hasard entre 1 et 10
* Tant que la case de coordonnées (_x_; _y_) n'est pas vide, on choisit à nouveau une valeur de _x_ et une valeur de _y_ au hasard
* Une fois _x_ et _y_ choisis, on affecte 1, qui représente un sous-marin, à la case correspondante.



# Placer un torpilleur

> Réaliser une fonction `placer_torpilleur` ,de paramètre _grille_, une liste représentant le jeu, qui place aléatoirement un sous-marin **sur deux cases vides** de la  _grille_ soit verticalement soit horizontalement.
>
> La fonction ne renvoie rien mais modifie la grille passée en paramètre par effet de bord.
>
> Documentez votre code !
>
> Testez votre fonction en utilisant l'affichage en mode triche.



Le principe est celui-ci : 

* On va placer le torpilleur depuis sa case de départ vers la droite ou vers le bas.
    * on choisit une valeur _x_ au hasard entre 1 et 9 (pour laisserune case à droite)
    * on choisit une valeur _y_ au hasard entre 1 et 10 (pour laisser une case en bas)
    * on choisit une direction (vers à droite ou vers le bas) et donc une seconde case de coordonnées (_x2_, _y2_).
* Tant que les cases de coordonnées (_x_; _y_) et (_x2_, _y2_) ne sont pas vides, on recommence.



## Même chose pour les autres bateaux



> Sur le même principe, réalisez une fonction `placer_croiseur` et `placer_porte_avion` placçant respectivement un croiseur (de trois cases) et un porte_avion (de quatre case) dans notre grille.
>
> La fonction ne renvoie rien mais modifie la grille passée en paramètre par effet de bord.
>
> Documentez votre code !
>
> Testez votre fonction en utilisant l'affichage en mode triche.



## Placer et surveiller les bateaux



> Réalisez une fonction `placer_bateaux`, de paramètre _grille_, la grille de jeu, qui place dans la grille :
>
> 
>
> * 1 sous-marins qui occupent chacun une case.
>
> * 1 torpilleur qui occupe 2 cases.
>
> * 1 cuirassé qui occupe 3 cases.
>
> * 1 portre-avions qui occupe 4 cases. 
>
>     
>
>     La fonction ne renvoie rien mais modifie la grille passée en paramètre par effet de bord.
>
>     Documentez votre code !
>
>     Testez votre fonction en utilisant l'afichage en mode triche.
>



> Créez une fonction `est_present`, de paramètre _bateau_, une chaine de caractère pouvant être 'S', 'T', 'C' ou 'P', et _grille_, la grille de jeu, qui renvoie `True` s'il reste des cases dans la grille représentant le bateau et `False` sinon.



## Allez, on joue



> Réaliser une fonction `jouer` de paramètres _x_ et _y_ , les coordonnées de deux cases, et _grille_, la grille de jeu, qui :
>
> * renvoie le msg 'Raté !' si le coup et dans l'eau.
>
> * renvoie le msg 'Touché !' en précisant quel type de bateau l'a été si c'est le cas
>
> * modifie par effet de bord la grille en cas de bateau touché.
>
> * précise 'coulé' si le bateau n'est plus présent dans la grille en précisant quel type de bateau l'a été.
>
> * renvoie 'Gagné !' s'il n'y a plus du tout de bateau dans la grille.
>
>      
>
>      Testez votre fonction en mode triche.



> Réaliser une fonction `lancer_partie`  de paramètre _nb_coups_, le nombre maximum de coups autorisé, qui :
>
> * génére une grille de jeu
>
> * tant que le nombre de coups maximum n'a pa été atteint et que le joueur n'a pas gagné :
>
>     * affiche la grille
>     * demande le coup du joueur
>
> * En cas de victoire, afiche 'bravo vous avez gagné en ... coups`
>
> * En cas de défaite, affiche la grille en mode triche.
>
>     



___

Par Mieszczak Christophe licence CC - BY - SA

_image source wikipédia CC BY SA (auteur précisé dans le lien_
# coding: utf-8
import random

def generer_grille():
    '''
    renvoie une grille de 10 cases sur 10  cases
    return grille
    type list
    >>> grille = generer_grille()
    >>> grille
    [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    >>> grille[1][2] = 1
    >>> grille
    [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    '''
    grille = []
    for _ in range(10):
        grille.append([0] * 10)
    return grille
    
def set_cell(grille, x, y, valeur):
    '''
    affecte _valeur_ à la case de coordonnées (_x_;_y_) du jeu.
    param grille
    type list
    param x, y, valeur
    int
    effet de bord sur grille
    >>> grille = generer_grille()
    >>> set_cell(grille, 2, 1, 1)
    >>> grille
    [[0, 1, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    '''
    grille[y - 1][x - 1] = valeur
    

def get_cell(grille, x, y):
    '''
    renvoie la valeur contenue dans la case de coordonnées (_x_;_y_) du jeu.
    param grille
    type list
    param x, y
    int
    return la valeur de la case
    type int
    >>> grille = generer_grille()
    >>> set_cell(grille, 2, 1, 1)
    >>> get_cell(grille, 2, 1)
    1
    >>> get_cell(grille, 1, 2)
    0
    '''
    return grille[y - 1][x - 1] 

def afficher(grille, triche = 0):
    '''
    affiche la grille de jeu.
    param grille
    type list
    param triche : facultatif vallant 0 par défaut
    type int
    
    * Le mode normal, mode par défaut, c'est à dire lorsque triche vaut 0, va afficher :
        * '_'  pour les cases non choisies
        *  'S', 'T', 'C' et 'P' respectivement pour les Sous-marins, Torpilleurs, Croiseur et Porte_avion déjà touchés
        * 'X' pour les coups dans l'eau.
    * Le mode triche, activer lorsque le paramètre triche vaut 1, va afficher :
        * '_'  pour les cases non choisies
        *  'S', 'T', 'C' et 'P' respectivement pour les Sous-marins, Torpilleurs, Croiseur et Porte_avion qu'ils soient touchés ou pas, de manière à les rendre toujours visibles.
        * 'X' pour les coups dans l'eau.
    '''
    if triche == 0:
        cases =['_', '_', '_', '_', '_','X','S','T','C','P']
    else :
        cases =['_', 'S', 'T', 'C', 'P','X','S','T','C','P']
    print('   A B C D E F G H I J')
    for y in range (10):
        chaine = str(y + 1)
        if y < 9 :
            chaine +=' '
        chaine +='|'
        for x in range(10):
            chaine += cases[get_cell(grille, x, y)]+'|'
        print(chaine)

def placer_sous_marin(grille):
    '''
        place aléatoirement un sous-marin sur un case vide
        param grille
        type list
        effet de bord sur grille
    '''
    x = random.randint(1,10)
    y = random.randint(1,10)
    while get_cell(grille, x, y) != 0 :
        x = random.randint(1,10)
        y = random.randint(1,10)
    set_cell(grille, x, y, 1)

def chercher_cases_vides(grille, x, y):
    '''
    renvoie la liste des coordonnées des cases vides autour de la cases (x,y)
    param grille
    type list
    param x,y
    type int
    return la liste des cases vides
    type list
    '''
    cases_vides = []
    dx=[-1,0,1,-1,1,-1,0,1]
    dy=[-1,-1,-1,0,0,1,1,1]
    chaine=[]
    for i in range(8):
       if x + dx[i] > 0 and x + dx[i] < 11 and y + dy[i] >0 and y + dy[i] < 11:
        if get_cell(grille, x, y) == 0 :
            cases_vides.append([x + dx[i], y + dy[i]])
    return cases_vides
    

def placer_torpilleur():
    '''
    
    '''
    while True :
        x1 = random.randint(1,10)
        y1 = random.randint(1,10)
        cases_vides = chercher_cases_vides(grille, x1, x2)
        if cases_vides != [] :
            x2, y2 = random.choice(cases_vides)
            
    


################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose=False)

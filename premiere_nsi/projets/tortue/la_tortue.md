# La tortue 



## Le module turtle

Nous allons faire quelques exercices autour du module `turtle` de Python afin de travailler tout cela. Le module `turtle` est un ensemble d'outils permettant de dessiner à l'aide d'instructions simples.
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Turtle_Graphics_Spiral.svg/600px-Turtle_Graphics_Spiral.svg.png" alt="module turtle - source wikipédia - creative commons" style="zoom:50%;" />

[Vous trouverez une description complète du module ICI](https://docs.python.org/fr/3.6/library/turtle.html)



- Pour commencer, ouvrez _Thonny_, créez un fichier _tortue.py_ et sauvegardez le dans votre répertoire _La tortue_.
- importer le module `turtle`

```Python
import turtle
```



* On règle la vitesse de la tortue par `turtle.speed(vitesse)` où vitesse peut prendre la valeur :
  
  * “fastest” 
    
  * “fast” 
  * “normal” 
    
  * “slow”
  * “slowest” 
  
  
  
* Il est possible de commander le paramétrage du crayon par :
  
  * `turtle.down()` qui abaisse le stylo
    
  * `turtle.up()` qui relève le stylo
    
  * `turtle.pensize(width)` qui change l’épaisseur du trait
    
  * `turtle.pencolor(color)` qui change la couleur (`"red"`, `"green"`, `"blue"`… ou un triplet de paramètres `(r, g, b)`)
  
  
  
* On déplace la tortue avec :

  * `turtle.forward(length)` qui avance d’un nombre de pas donné
  * `turtle.backward(length)` qui recule
  * `turtle.right(angle)` qui tourne vers la droite d’un angle donné (en degrés)
  * `turtle.left(angle)` qui tourne vers la gauche.

  

* On peut également déplacer la tortue à un point donné ou modifier son orientation avec :

  * `turtle.goto(x,y)` qui déplace la tortue jusqu’au point (*x*, *y*)
  * `turtle.setheading(angle)` qui oriente la tortue à l’angle donné en degrés, le 0° étant à l’est, le 90° au nord, etc.

  

* Au départ :

  *  la tortue est en (0, 0), orientée à 0°.
  * La fenêtre par défaut est 950 = 2 × 475 pixels de large et 800 = 2 × 400 pixels de haut. Le point (0, 0) est au centre de l’écran.

  

* Il est possible de modifier cette fenêtre avec

  * `turtle.setup(width, height)` qui définit les tailles en pixels de la largeur et hauteur de la fenêtre
  * `turtle.clear()` qui efface tout ce qui a été tracé ou écrit dans la fenêtre.

  

* On peut  écrire des chaines de caractères avec:

  * `turtle.write(str)` qui écrit la chaîne de caractères donnée à la position courante
  * `turtle.write(str, True)` qui érit et déplace la tortue à la fin du texte écrit.

  

* Pour terminer proprement le dessin on utilise `turtle.Terminator`



# Exercice 1



> **Etape 1:**
>
> Définissez et réalisez une fonction `tracer_motif` de paramètre _longueur_, un entier positif, qui trace le motif ci-dessous où :
>
> * le segment de départ mesure _longueur_
> * le carré a pour côté _longueur_  / 2
>
> 
>
> ![motif](media/motif.jpg)
>
> Documentez votre fonction.
>
> N'oubliez pas les assertions.



> **Etape 2:**
>
> Réalisez une fonction `effectuer_rotation` de paramètres _longueur_, un entier positif, et _nbre_rotations_, un entier positif, qui réalise _nbre_rotations_ rotations du motif de base de façon à obtenir le dessins ci-dessous :
>
> <img src="media/fig1_rendu_final.jpg" alt="dessin final" style="zoom:50%;" />
>
> Faites en sorte qu'un carré sur deux soit noir et l'autre rouge.
>
> N'oubliez pas les assertions.





#   Exercice 2 :



Observez les différentes étapes ci-dessous :



* Etape 1: tracé du motif 

  ![etape 1](media/fig2_etape 1.jpg)

* Etape 2 : motif suivant

![etape 2](media/fig2_etape 2.jpg)

* Rendu final

![final](media/fig2_rendu_final.jpg)



> **Un peu de Python**:
>
> - Réalisez une fonction, de paramètre _longueur_ (un flottant positif) qui trace le motif de base dont le côté mesure _longueur_. Documentez cette fonction. N'oubliez pas les assertions.
>
> _Remarques :_
>
> * Le motif de base est celui qui est répété à chaque itération. Regardez bien l'étape 1 et la position de la flèche qui est importante !







> **Un peu de Mathématiques :**
>
>  Si le carré ABCD est un carré de côté L, quel sera la longueur du carré EFGH obtenue de façon à ce que AE = BF = CG = DH = a où a appartient à [0,L] ?
>
>  <img src="media\maths.jpg" alt="source perso" style="zoom: 50%;" />



> **Un peu de Python**:
>
> * Réalisez une deuxième fonction, de paramètres _longueur (un entier) et nbre_repetitions_ (un entier), utilisant la fonction précédente et permettant de tracer les étapes suivantes.
> * Faites en sorte qu'un carré sur deux soit noir et l'autre rouge.



_Remarque :_

* Il vous faudra importer le module `math`dont nous utiliserons deux fonctions :
  * `math.sqrt(x)` renvoie la racine carré, de type flottant,  du flottant _x_ positif.
  * `math.atan(x) * 180 / math.pi` renvoie un angle en degré, du type flottant, dont la tangente est _x_.





___________

Par Mieszczak Christophe CC BY SA

_source images personnelle_
# QUIZZ

L'objectif de ce projet est de réaliser un jeu de Quizz. 

Chaque question posée propose quatre réponses A, B C ou D dont seulement une est la bonne.

* On doit poser 5 questions à l'utilisateur. 
* A chaque réponse de l'utilisateur, on doit lui dire s'il a eu bon et, sinon, lui donner la bonne réponse.

  

Pour réaliser ce projet, on va le découper en fonctions aussi petites et simples que possible.



ATTENTION :

* chaque fonction sera documentée et, si possible, testée.
* On utilisera des assertions pour vérifier le domaine des paramètres.



## Structures de données

Il faut maintenant choisir et utiliser des structures de données adaptées.

### Un tableau

On va utiliser un tableau de tableaux de chaînes de caractères dans lequel **la première réponse proposée est toujours la bonne**  :

```python
QUIZZ = [
            ['En quelle année a été inventée le web ?', '1990', '1970', '2000', '1980'],  
            ['Pour quelle raison Ada Lovelace est-elle connue ?', 'Elle a réalisé le premier programme informatique de l\'histoire', 'Elle a crée le premier réseau social de rencontre', 'Elle avait un prénom étrange', 'Elle a joué dans Docteur Who'],
             'On tape 2 ** 3 dans la console. Quel est le résultat retourné ?', '8', '6', 'une erreur', '3.14']
        ]
```

 

> *  Créer un tableau d'au minimum 10 questions en rapport avec la NSI.
>
> * Récupérer les questions de votre binôme pour en avoir au moins 20.
>
> _La pertinence et l'exactitude des questions et réponses seront prises en compte dans l'évaluation._



_Remarque :_

Le nom de ce tableau est écrit en majuscule car il s'agit d'une constante ( une variable qui une fois définie ne varie pas) globale qu'on va utiliser dans tout le programme.

### Un second tableau

On ne va pas poser toutes les questions mais seulement 5. 



> Codez une fonction `choisir_cinq(tab)` où _tab_ est un tableau de plus de 5 questions/réponses, qui renvoie un tableau composé de 5 éléments de _tab_ choisis au hasard sans doublon. 
>
> **Attention : pas d'effet de bord sur tab !!**



### Des dictionnaires

On va maintenant construire deux dictionnaires dont les clés sont des questions :

* pour le premier, les valeurs seront un tableau contenant les propositions associées mélangées.
* pour le second, les valeurs seront la bonne réponse.



> * Codez la fonction `generer_dic_propositions(tab)` qui renvoie un dictionnaire dont les clés sont les questions du tableau passé en paramètre et les valeurs un tableau contenant les 4 propositions correspondantes à cette question dont on changera l'ordre aléatoirement.
>
> Voici un exemple  : 
>
> ```python
> >>> dic = generer_dic_propositions(QUESTIONS)
> >>> dic['En quelle année a été inventée le web ?']
> ['1970', '1980', '1990', '2000']
> ```
>
> 
>
> * Codez de même la fonction `generer_dic_bonnes_reponses(tab)`qui renvoie un dictionnaire dont les clés sont les questions du tableau passé en paramètre et les valeurs la bonne réponse correspondante.
>
> Voici un exemple qui pourrait servir de test : 
>
> ```python
> >>> dic = `generer_dic_bonnes_reponses(QUESTIONS)
> >>> dic['En quelle année a été inventée le web ?']
> '1990'
> ```
>
> 

## Fonctions annexes

On va avoir besoin de quelques fonctions annexes :

* une pour afficher la question et les propositions.
* une pour demander à l'utilisateur de répondre par 'A', 'B', 'C' ou 'D' en l'empêchant de répondre autre chose.
* une pour vérifier si la réponse donnée est la bonne ou pas
* une pour féliciter l'utilisateur ou le corriger s'il s'est trompé.
* une pour commenter le score final



### Afficher une question et ses propositions

On commence très doucement

> Coder une fonction `afficher_question(question, dic_propositions)` où la _question_ est du type _str_ et _dic_propositions_ est le dictionnaire permettant de relier questions et propositions, qui :
>
> * affiche la question posée.
> * affiche les 4 propositions de réponse correspondantes en les numérotant A, B C et D.



### Demander la réponse de l'utilisateur

> Coder une fonction `demander_reponse()` qui renvoie 'A', 'B', 'C' ou 'D' après avoir demandé la réponse de l'utilisateur. On posera la question tant que ce dernier répond autre chose.



### Vérifier la réponse

>  Codez la fonction `verifier_reponse(question,reponse, dic_propositions, dic_bonnes_reponses)` où :
>
>  * _question_ est une chaîne correspondant à une question posée.
>
>  * _reponse_ est un caractère parmi 'A', 'B', 'C' ou 'D'.
>
>  * _dic_propositions_ est le dictionnaire permettant de relier questions et propositions.
>
>  * _dic_bonnes_reponses_ est le dictionnaire permettant de relier questions et leur bonne réponse.
>
>    
>
>  Cette fonction renverra _True_ si la réponse est correcte et False sinon.



### Afficher un commentaire

> Coder une fonction `commenter_reponse_correcte()` qui affiche aléatoirement un message choisi au hasard parmi plusieurs messages possibles pour féliciter un joueur qui a bien répondu.



> Coder une fonction `commenter_reponse_fausse(bonne_reponse)` qui prend en paramètre la chaîne de caractères qui correspond à la bonne réponse attendue et qui notifie le joueur qu'il s'est trompé puis lui donne la _bonne réponse_. La notification sera choisie au hasard parmi plusieurs messages possibles comme pour la fonction précédente.



## Poser une question

Coder la fonction `poser_question(question, dic_propositions, dic_bonnes_reponses)` qui prend en paramètre une _question (str)_ à poser et les deux dictionnaires indispensables.

Cette fonction, en utilisant les fonctions annexes, doit :

* afficher la _question_ passée en paramètre et les propositions associées.
* demande une réponse à l'utilisateur.
* si la réponse donnée est correcte, afficher un message de félicitation.
* si la réponse est incorrecte, affiche un message le notifiant et la réponse correcte attendue. 
* pour finir, cette fonction renverra 1 si la réponse et correcte et 0 sinon.



Voici un exemple pour vous aider un peu :

```python
>>> dic = generer_dic(QUIZZ)
>>> poser_question('Quelle est la couleur du cheval blanc d\'Henry IV ?', dic_propositions, dic_bonnes_reponses)
Quelle est la couleur du cheval blanc d`Henry IV ?
A.gris
B.blanc
C.Ça dépend de la météo
D.jaune fluo
Votre réponse : crotte
Vous devez répondre A, B, C ou D : A
Et non.... la bonne réponse était blanc.
0
```



### Commenter le score

> Ecrire la fonction `commenter(score)`qui commente le score en choisissant ses commentaires au hasard parmi plusieurs possibles pour chaque score obtenu.



## Quizz

Il faut maintenant coder une fonction `quizz()`  qui :

* initialise un tableau de 5 questions différentes (sans proposition, uniquement les questions) dans un ordre aléatoire à partir du tableau QUIZZ en utilisant une fonction codée plus haut.
* génère les dictionnaires qui associe question et propositions ou questions et bonne réponse à partir de ce tableau en utilisant une fonction codée plus haut.
* Pose chaque question.
* calcule le score
* A l'issu du quizz, commente le score final obtenu.





## A bientôt ....

Tout tourne ? Super !

Mais on n'a pas fini. Le Quizz reviendra bientôt avec les données en table...





___________

Par Mieszczak Christophe

Licence CC BY SA






 # Jeu de la vie

## Qu'est-ce que c'est que ce truc ??



Le jeu de la vie est une _simulation_ de l'évolution d'une population de cellules modélisées par une grille de taille fixée au départ de l'expérience.



-------------------------------    Allez donc visionner [cette petite vidéo](https://www.youtube.com/watch?v=S-W0NX97DB0) !



Chaque case de la grille d'une génération donnée, peut prendre deux états :

* Soit la case est pleine : la cellule qu'elle représente est _vivante_

* Soit la case est vide : la cellule qu'elle représente est _morte_

    

Voici, par exemple, la représentation une génération d'une population dans  une grille 3x3 contenant 3 cellules vivantes (les 'O') et 6 mortes (les '_') :

```markdown

_O_
_O_
_O_
```



Chaque case de la grille possède un nombre de cases voisines compris entre 3 (si c'est un coin) et 8 (si elle n'est pas sur le bord de la grille). Chaque voisine peut être vivante ou morte.



D'une génération à l'autre, la population évolue en suivant deux règles très simples :

* **Une cellule vivante reste vivante si et seulement si elle a 2 ou 3 voisines vivantes.**
* **Une cellule morte devient vivante si et seulement si elle a exactement 3 voisines vivantes.**



Reprenons l'exemple précédent. Réfléchissez au passage de la _génération no 1_ à la  _génération no 2_ puis trouvez la _génération no 3_.

```txt
Génération no 1 :
_O_
_O_
_O_


Génération no 2
___
OOO
___

Génération no 3
???




```



Le **[jeu de la vie](https://fr.wikipedia.org/wiki/Jeu_de_la_vie)** est un [automate cellulaire](https://fr.wikipedia.org/wiki/Automate_cellulaire) imaginé par [John Horton Conway](https://fr.wikipedia.org/wiki/John_Horton_Conway) en 1970 et qui est probablement le plus connu de tous les automates cellulaires.

Le jeu de la vie est un jeu au sens [mathématique](https://fr.wikipedia.org/wiki/Jeu_mathématique) plutôt que ludique. Bien que n'étant pas décrit par la [théorie des jeux](https://fr.wikipedia.org/wiki/Théorie_des_jeux), certains le décrivent comme un « jeu à zéro joueur ». 



## Modéliser une grille de jeu

Nous allons modéliser la grille du jeu de la vie par un tableau de tableaux  dans lequel :

* un "0" représente une case vide.
* un "1" représente une case vivante.



Par exemple, pour modéliser la grille 3*3 vide de l'exemple précédent, il nous faudrait la le tableau ci-dessous :

```python
>>> grille = [ [0, 1, 0],
               [0, 1, 0],
               [0, 1, 0]
             ]
>>> grille[0][0]
???
>>> grille[0][1]
???
>>> grille[1][0]
???
>>> grille[2][1]
???

```



### Générer une grille bien précise



> Créer une fonction `generer_grille1`, sans paramètre, qui renvoie le tableau correspondant à la situation ci-dessous où est représentée une grille 4*4 contenant 6 cellules vivantes (les 'O') et 10 mortes (les '_') :
>
> ```
> ____
> _OOO
> OOO_
> ____
> ```
>
> _Remarque :_ dans la liste qui modélise la grille, on ne place pas des 'O' ou des '_' mais des 1 ou des 0. 



### Générer une grille vide



> Écrivez une fonction `generer_grille(larg, haut)` de paramètres _hauteur_ et _largeur_, deux entiers strictement supérieurs à 2, qui renvoie un tableau représentant une grille vide du jeu de la vie de dimension _largeur_ sur _hauteur_ .  



Pour vous aider :

```python
def generer_grille(larg, haut):
    '''
        renvoie un tableau de tableaux représentant une grille vide de dimension largeur * hauteur
        : params
        	larg (int) > 2
        	haut (int) > 2
        : return (list of list of 0)
        >>> grille = generer_grille(4,3)
        >>> grille
        [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
        >>> grille[0][1] = 1
        >>> grille
        [[0, 1, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    '''
    grille= []
		
```



### Générer une grille au remplissage aléatoire



> Écrire une fonction `remplir(larg, haut)`, qui renvoie un tableau de dimensions précisées en paramètre et qui contient environ un tiers de 1 et deux tiers de 0.

 

Pour vous aider :

 ```python
 def remplir(larg, haut):
 	'''
     renvoie une grille de dimensions précisées en paramètre et qui contient environ un tiers de 1 et deux tiers de 0.
     : params
     	haut (int) > 2
     	larg (int) > 2
     : return (list)
     '''         
 ```

  

 _Remarque :_

 On utilisera le module `random` (qu'il faudra donc importer) ,et la onction `random.randint(1,3)`  pour décider si on place un "1" ou un "0" dans la grille.

  

> Testez votre fonction dans la console  !!
>



## Interface Homme - Machine

### Dimension d'une grille



> * Ecrivez une fonction `largeur`, de paramètre _grille_, un tableau représentant une grille de jeu, qui renvoie la largeur de la grille.
> * Ecrivez une fonction `hauteur`, de paramètre _grille_, un tableau représentant une grille de jeu, qui renvoie la hauteur de la grille.
>



Pour vous aider :

 ```python
def largeur(grille):
    '''
    renvoie la largeur de la grille
    :param grille (list of list)
    :return (int)
    >>> grille = generer_grille(3, 4)
    >>> largeur(grille)
    3
    '''
 ```





### Afficher une grille



> Ecrivez une fonction `afficher` de paramètre _grille_, un tableau représentant une grille du jeu, qui affiche une représentation de la grille dans la console en utilisant la fonction `print`.



 Le rendu pourrait être le suivant, pour une grille 4 sur 3 :

 ```txt
_O_O                     O O
_O__                 ou  O
_OO_                     OO
 ```

  On pourra construire, pour chaque ligne, une chaîne de caractères contenant les cellules, vivantes ou mortes, à afficher dans la ligne.

  

 Pour bien démarrer :

 ```python
def afficher(grille):
	'''
    affiche la grille de jeu dans la console
    : param grille(list)
    '''
    
 ```

 

> Testez votre fonction dans la console :
>
> ```python
>  >>> grille = genere_grille_1()
>  >>> afficher(grille)
> ???
> 
> ```
>
> 



## J'ai toujours préféré aux voisins les voisines vivantes



> Ecrivez une fonction `compter_voisines_vivantes`, de paramètre _grille_, un tableau représentant une grille de jeu, et _x_, _y_ les coordonnées d'une case de la _grille_, qui renvoie le nombre de voisines vivantes qu'à la case située à la ligne _y_ et à la colonne _x_ de la _grille_ .

  


Voici de quoi vous aider à démarrer :

 ```python
def compter_voisines_vivantes(grille, x, y):
    '''
    renvoie le nombre de voisines vivantes qu'à la case située à la ligne y et à la colonne x de la grille .
    : params 
    	grille (list of list)
        x (int) largeur(grille) > x >=0
        y (int) hauteur(grille) > y >= 0
    : return (int) nombre de voisines vivantes
    >>> grille = generer_grille1()
    >>> compter_voisines_vivantes(grille, 0, 0)
    1
    >>> compter_voisines_vivantes(grille, 2, 1)
    4
    >>> compter_voisines_vivantes(grille, 3, 3)
    1
    >>> compter_voisines_vivantes(grille, 0, 3)
    2
    >>> compter_voisines_vivantes(grille, 3, 0)
    2
    '''
	nbre_voisines = 0 

 ```



  ## Génération suivante

### Dura lex ced lex



> Coder la fonction `appliquer_regle(contenu, nb_voisines)` qui prend en paramètre le _contenu_ d'une cellule de la grille (0 ou 1), son nombre de voisines vivantes (int >0) puis renvoie 1 si la case contiendra une cellule vivante à la prochaine génération et 0 sinon.
>
> Elle applique la loi suivante : 
>
> * **Une cellule vivante reste vivante si et seulement si elle a 2 ou 3 voisines vivantes.**
>
> * **Une cellule morte devient vivante si et seulement si elle a exactement 3 voisines vivantes.** 
>



### Next Gén

> Créer une fonction `generation_suivante(grille)`, de paramètre _grille_, un tableau représentant une grille de jeu, qui renvoie un tableau correspondant à la grille de la génération suivante, en utilisant les règles du jeu de la vie.
>
>     ATTENTION : IL NE DOIT PAS MODIFIER LA GRILLE grille MAIS CRÉER UNE DEUXIÈME GRILLE INITIALEMENT VIDE QUI REPRÉSENTERA LA FUTURE GÉNÉRATION !!
>    On va donc commencer par créer un tableau _next_grille_ correspondant à une grille vide de mêmes dimensions que la grille de départ, dans laquelle on va placer, ou pas,  des 1 en parcourant la grille courante.
> 
>
> 
>

Comme d'habitude, un peu d'aide :

```python
def generation_suivante(grille):
    '''
    renvoie la grille de prochaine génération.
    rem : Utilise une deuxième grille pour éviter tout effet de bord sur grille.
    : param grille (list of list)
    : return (list)
    : PAS D EFFET DE BORD SUR grille !!
    >>> grille = generer_grille1()
    >>> generation_suivante(grille)
    [[0, 0, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 0, 0]]
    
    '''
    next_grille = generer_grille(largeur(grille), hauteur(grille))

```



## Animer 



> Définissez une fonction `animer` de paramètres _grille_ et _n_, un entier positif représentant le nombre de générations à calculer.
>
> Cette fonction devra : 
>
> * afficher l'étape courante
> * remplacer la génération courante par la génération suivante
> * recommencer le processus n fois.
>
>  
>
> Comme d'habitude :
>
> * Documentez votre fonction
>
> * Testez là dans le console :
>
>     ```python
>     >>> grille = generer_gille1()
>     >>> animer(grille, 5)
>     ???
>     ```
>
>     



_Remarque :_

* La situation de départ modélisée par cette dernière grille est appelé _oscillateur_, on comprend pourquoi.

* Il existe de nombreuses situations particulières. En voici quelques exemples :

    ```txt
    Un oscillateur 4*4 :
    
    __OO
    __OO
    OO__
    OO__
    
    Un vaisseau 20*20 qui va se déplacer vers la droite:
    __O__O______________
    ______O_____________
    __O___O_____________
    ___OOOO_____________
    ____________________
    ____________________
    ____________________
    ____________________
    ____________________
    ____________________
    
    
    
    ```

     
    
    

> Définissez les fonctions :
>
> * `generer_grille2`, sans paramètre, qui renvoie une liste correspondant à l'oscillateur 4 * 4
> * `generer_grille3`, sans paramètre, qui renvoie une liste correspondant au vaisseau 20 * 20
> * Testez l'évolution de ces systèmes 



_Remarque :_ 

vous trouverez d'autres structures [en suivant ce lien](https://fr.wikipedia.org/wiki/Vaisseau_(automate_cellulaire)).



## Pygame



### Objectif

Nous allons utiliser le module `pygame` pou réaliser une interface graphique en remplaçant la fonction `afficher` codée plus haut.

Voici le résultat attendu, par exemple, pour la `grille3` définie plus haut.

<img src="media/apercu.jpg" alt="aperçu" style="zoom:33%;" />





> * Renommer `afficher` en `afficher_old`
>
> * importez le module `pygame` en haut du code
>
> * Insérez la nouvelle fonction `afficher` ci-dessous :
>
>     ```python
>     def afficher(grille) :
>         '''
>         affiche la grille avec pygame
>         : param grille (list of list)
>         '''
>     ```
> 
>



_Remarque :_

On voit là tout l'intérêt d'avoir réaliser une fonction d'affichage à part et non pas de `print` un peu partout dans le code : seule cette partie est à modifier !



### Initialisation

Nous avons besoin de donner une taille à nos cellules, que l'on choisira carrée, de façon à ce que le dessin complet n'excède pas 1024 pixels sur 768 pixels. Pour cela, on déclare la variable `taille` ci-dessous :

```python
taille = min (1024 //largeur(grille), 768 // hauteur(grille))
```

Voici comment initialiser une fenêtre graphique :

```python
import pygame as py #on utilise py plutôt que pygame dans la suite
py.init() # initialise une fenêtre
py.display.set_caption("titre") # affiche le texte dans la barre de titre
window = py.display.set_mode((largeur, hauteur)) # fixe les dimension en pixel
window.fill((r, g, b)) # remplit l'arrière plan dans la couleur (r, g, b)
py.display.update() # met à jour les modifications affichées dans la fenêtre
```



> Complétez la fonction `afficher` de façon à initialiser la fenêtre graphique vide sur un fond blanc.



### Sprite 

Pour afficher une cellule, on affichera un carré de côté `taille`. Pour cela, on a besoin d'une image (un sprite) .

> Avec Gimp, créez une image carrée de côté 10 pixels et sauvegardez la sous le nom `carre.jpg` au côté de votre programme Python.



Voici les méthodes utilisées pour utiliser ces sprites :

```python
carre = py.image.load('carre.jpg').convert_alpha() # charge l'image spécifiée dans la variable carre
carre = py.transform.scale(carre,(longueur, largeur))#redimensionnel'image aux dimensions précisées
window.blit(carre, (x, y)) # affiche l'image à partir du pixel de coordonnées (x, y)
py.display.update() # met à jour les modifications affichées dans la fenêtre
```



> En vous inspirant de  `afficher_old`, complétez la fonction `afficher` afin de faire apparaître les cellules vivantes présentes dans la grille.



_Remarque :_

* Pour permettre un petit délais entre deux affichages, en vue d'une animation, on utiliser la méthode `py.time.wait(temps_en_miliseconde)` à la fin de l'affichage.
* Pour quitter, il faudra cliquer sur l'icône `stop` de Thonny. Nous verrons plus tard comment gérer des événements.



> Testez :
>
> ```python
> >>> g = generer_grille3()
> >>> animer(g, 80)
> 
> ```
>
> Testez encore :
>
> ```python
> >>> g = generer_grille(40, 40)
> >>> remplir(g)
> >>> afficher(g)
> >>> animer(g,100)
> ```
>
> 

_________

Par Mieszczak Christophe CC BY SA

source images : production personnelle.




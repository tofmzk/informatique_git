# La course au chicon





## Introduction

Chaque année, en otobre à Baisieux, commune du Nord frontalière de la Belgique, a lieu la [course du chicon](http://courirabaisieux.fr/la-course-du-chicon/).

Cette course se décline sur plusieurs distances de 15km à 0.5km. Nous ne nous intéresserons dans ce TP qu'à la version 15km.

Vous avez la responsabilité du traitement informatique des données : gestion des inscriptions, récolte des performances des concurrents, publication des résultats.



## Pour bien débuter



Sont fournis :

* `inscrits.csv` : la table complète des inscrits.
* `small_inscrits.csv` : une petite table d'inscrits qui pourra être utilisée pour les tests.
* `performances.csv` : la table complète des résultats.
* `small_performances.csv` : une petite table de résultats qui pourra être utilisée pour les tests.



> * Dans la table `inscrits.csv` quels sont les attributs ?
>
>     ```
>     
>     ```
>
> * Dans la table `performances.csv` quels sont les attributs ?
>
>     ```
>     
>     ```
>
>     



Créez un fichier *course_au_chicon.py* dans lequel on codera tout le projet.

On n'oublie pas le code pour les doctest à placer tout en bas :

```python
################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = False)
```





## Lire les donnees





> Réaliser une fonciton `lire_donnees` de paramètres _nom_fichier_csv, un chemin vers un fichier de format `csv` , et _separateur_, le séparateur utilisé (par défaut `;`), qui renvoie une liste dont chaque élement est lui même une liste correspondant à une ligne de la table.
>
> ```python
> def lire_donnees(nom_fichier_CSV, separateur = ';'):
>     	"""
>        lit le fichier csv dans le repertoire data et revoie la liste des donnees
>        : param nom_fichier_CSV
>        type str
>        : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
>        type str
>        return liste des données
>        type list
>     	"""
>     	assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
> ```
>
> Testez votre fonction en utilisant `small_inscrits.csv` et `small_performances.csv` dans la console :
>
> ```python
> >>> l = lire_donnees('small_inscrits')
> >>> l 
> ???
> >>> p = lire_donnees('small_performances')
> >>> p 
> ???
> ```
>



## Année de naissance 



Testez :

```python
>>> l = lire_donnees('small_inscrits.csv')
>>> l[1]
???
```



Par défaut l'année de naissance est une chaine de caractères. Il faut y remédier !



> Créez une fonction `convertir_annee` de paramètre _liste_inscrits_, une liste d'inscrits, qui convertit, avec un effet de bord, l'année de naissance au type `int`.
>
> ```python
> def convertir_annee(liste_inscrits):
>        '''
>  	convertir l'année de naiss au format int
>  	: param liste_inscrits
> 	 type liste
>  	: EFFET DE BORD SUR LISTE_INSCRITS
>  	>>> l = lire_donnees('small_inscrits.csv')
>     	>>> convertir_annee(l)
>    	>>> l == [['Prenom', 'Nom', 'Sexe', 'Annee_naiss'], ['Sidney', 'Robert', 'M', 1970], ['Paien', 'Gilbert', 'M', 1953], ['Vincent', 'Riquier', 'M', 1980], ['Saville', 'Marier', 'M', 1969], ['Namo', 'Lereau', 'M', 1980], ['Romaine', 'Hughes', 'F', 1943], ['Archard', 'Rivard', 'M', 1950], ['Cheney', 'Chassé', 'M', 1949], ['Avelaine', 'CinqMars', 'F', 1983], ['Sidney', 'Charest', 'M', 1981]]
>    	True
> 	'''
>     for i in range(1, len(listes_inscrits)):
> ```
>
> 



## Assigner une numéro de dossard



> Créez une fonction `assigner_dossards`, de paramètre _liste_inscrits, une liste d'inscrit qui ajoute, par effet de bord, un attribut `Dossard` à la liste des inscrits et un dossards entre 1 et le nombre d'inscrits à chaque compétiteur.
>
> Pour vous aider, un petit début :
>
> ```python
> def assigner_dossards(liste_inscrits):
> 	'''
>  	ajoute un no de dossard assigné dans l'ordre des inscriptionsà chaque compétiteur de la liste
>  	le premier dossard a pour no 1
>  	: param liste_inscrits
>  	type liste
>  	: EFFET DE BORD SUR LISTE_INSCRITS
>     	>>> l = lire_donnees('small_inscrits.csv')
>    	>>> convertir_annee(l)
>    	>>> assigner_dossards(l)
>    	>>> l == [['Prenom', 'Nom', 'Sexe', 'Annee_naiss', 'Dossard'], ['Sidney', 'Robert', 'M', 1970, 1], ['Paien', 'Gilbert', 'M', 1953, 2], ['Vincent', 'Riquier', 'M', 1980, 3], ['Saville', 'Marier', 'M', 1969, 4], ['Namo', 'Lereau', 'M', 1980, 5], ['Romaine', 'Hughes', 'F', 1943, 6], ['Archard', 'Rivard', 'M', 1950, 7], ['Cheney', 'Chassé', 'M', 1949, 8], ['Avelaine', 'CinqMars', 'F', 1983, 9], ['Sidney', 'Charest', 'M', 1981, 10]]
>    	True
>  	'''
>  	liste_inscrits[0].append('Dossard') # on ajoute l'attribut 'Dossards'
>  	for i in range(1, len(liste_inscrits)): 
>      
> ```
>
> 



## Catégories

 

Selon l'âge du participant, l'organisation de la course lui assigne une catégorie. Voici la liste des catégories selon l'année de naissance :



| Année de naissance | catégorie |
| :----------------: | :-------: |
|   [2000 ; 2010[    |  junior   |
|   [1990 ; 2000[    |  senior   |
|   [1980 ; 1990[    |   elite   |
|    [1970; 1980[    |  master   |
|    [1960; 1970[    |  master2  |
|    [1950; 1960[    |  master3  |
|    [1940; 1950[    |  master4  |
|    [1930; 1940[    |  master5  |
|    [1920; 1930[    |  master6  |



> 
>
> Réaliser une fonction `creer_categories` de paramètre _liste_inscrits_, une liste d'inscrits qui :
>
> * convertit la liste l'attribut `Annee_nais` de la liste  _liste_inscrits_ au type `int`.
> * remplace l'attribut `Annee_nais` par l'attribut `Categorie`.
> * remplace les valeurs de de l'attribut `Annee_nais` par la catégorie correspondante.
> * ne renvoie rien mais modifie la liste _liste_inscrits_ par effet de bord.
>
> ```python
> def creer_categories(liste_inscrits):
>     '''
>     remplace l'attribut `Annee_naiss` par l'attribut `Categorie`.
>     remplace les valeurs de de l'attribut `Annee_nais` par la catégorie correspondante.
>     : param liste_inscrits
>     type list
>     : EFFET DE BORD ne renvoie rien mais modifie la liste _liste_inscrits_ par effet de bord.
>     >>> l = lire_donnees('small_inscrits.csv')
>     >>> creer_categories(l)
>     >>> l == [['Prenom', 'Nom', 'Sexe', 'Categorie'], ['Sidney', 'Robert', 'M', 'master'], ['Paien', 'Gilbert', 'M', 'master3'], ['Vincent', 'Riquier', 'M', 'elite'], ['Saville', 'Marier', 'M', 'master2'], ['Namo', 'Lereau', 'M', 'elite'], ['Romaine', 'Hughes', 'F', 'master4'], ['Archard', 'Rivard', 'M', 'master3'], ['Cheney', 'Chassé', 'M', 'master4'], ['Avelaine', 'CinqMars', 'F', 'elite'], ['Sidney', 'Charest', 'M', 'elite']]
>     True
>     '''
>     convertir_annee(liste_inscrits)
>     categories = { 1930 : 'master6',
>                    1940 : 'master5',
>                    1950 : 'master4',
>                    1960 : 'master3',
>                    1970 : 'master2',
>                    1980 : 'master',
>                    1990 : 'elite',
>                    2000 : 'senior',
>                    2010 : 'junior'
>                    }                  
> ```
>
> 





## Performances et temps



> Réalisez une fonction `convertir_performances`, de paramètre _liste_performances_, une liste de performances, qui convertit les attributs `Dossard`, `Heure`, `Min` et `Sec` au type `int`, ne renvoie rien mais modifie la liste par effet de bord.
>
> Pour vous aider, un petit début :
>
> ```python
> def convertir_performances(liste_performances):
> 	'''
>  	convertit les attributs 'Dossards' 'Heure', 'Min' et `Sec` au type `int`. 
>  	: param liste_performance
>  	type liste
>  	: EFFET DE BORD SUR liste_performances
>    	>>> p = lire_donnees('small_performances.csv')
>     	>>> convertir_performances(p)
>     	>>> p == [['Dossard', 'Heure', 'Min', 'Sec'], [1, 1, 8, 55], [2, 1, 5, 45], [3, 1, 21, 23], [4, 0, 56, 29], [5, 1, 6, 20], [6, 1, 17, 8], [7, 0, 46, 31], [8, 0, 48, 10], [9, 0, 59, 57], [10, 1, 6, 38]]
>     True 	
>  	'''
>  	for i in range(1, len(liste_performances)):
>                 
>         
> 
> ```
>
> 



### Retrouver un competiteur

On a besoin de retrouver un competiteur à partir de son numéro de dossard, dans une liste de performances. On va pour cela utiliser la méthode par parcours car la liste n'est pas triée par défaut.



Si le dossard recherché n'est pas présent dans la liste de performances, c'est que le courreur correspondant n'a pas fini. Dans ce cas, la fonction renverra `None`.



> Réaliser une fonction `trouver_competiteur`, de paramètre _liste_, une liste de performances,  et _dossard_, un no de dossard, qui renvoie la liste de toutes les valeurs du competiteur correspondant au dossard passé en paramètre ou `None` si le dossard n'est pas attribué. 
>
> ```python
> def trouver_competiteur(liste_performances, dossard):
>    	'''
>    	trouve le competiteur correspondant au dossard puis renvoie la liste de ses 	performance par parcours
>    	: param liste_performances
>    	type list
>    	: param dossard
>    	type int
>    	: return la liste de perf
>    	type list
>    	>>> perf = lire_donnees('small_performances.csv')
> 	>>> convertir_performances(perf)
> 	>>> trouver_competiteur(perf, 1) ==	[1, 1, 8, 55]
> 	True
> 	>>> trouver_competiteur(perf, 10)==	[10, 1, 6, 38]
> 	True
> 	>>> trouver_competiteur(perf, 3) == [3, 1, 21, 23]
> 	True
> 	>>> trouver_competiteur(perf, 20)
>    	'''
> 
> ```
>
> 





## Joindre les inscriptions et les résultats

On veut maintenant joindre les deux listes _liste_inscrits_ et _liste_performances_. Quelle clé primaire peut-on utiliser ?

```txt

```

Le principe est le suivant :

* On commence par initialiser une liste _liste_complete = [['Prenom', 'Nom', 'Sexe', 'Categorie', 'Dossards','Heure', 'Min', 'Sec']]_ 
* Pour _i_ allant de 1 (on ne lit pas la ligne des attributs) à _len(liste_inscrits)_ exclus :
    * On initialise une liste _ligne = [ ]_ qui contiendra toutes les valeurs de tous les attributs des deux listes, sans doublon.
    * Pour tout élément de _liste_inscrits[i]_, c'est à dire de l'inscrit no _i_, on ajoute l'élément à la liste _ligne_.
    * On trouve le _competiteur_ ayant, dans  la _liste_performances_, le dossard de l'inscrit no _i_. 
    * Si aucun _competiteur_ ne correspond au dossard no _i_, c'est que le courreur en question a abandonné : on lui attribue alors la liste `[liste_inscrits[i][4], 10 , 0, 0]` c'est à dire le no de dossard recherché et un temps l'envoyant en fin de classement de 10h0'0''.
    * pour tout élement de la liste _competiteur_, sauf le dossard pour éviter un doublon, on ajoute l'élement à la liste _ligne_.
    * On ajoute la ligne à la liste _liste_complete_.
* On renvoie la _liste_complete_



> 
>
> Créez une fonction `joindre_tables` de paramètres _liste_inscrits_, une liste d'inscrits, et _liste_performances_, une liste de performances, qui en revoie une liste résultat d'une jointure entre les deux tables. 
>
> 
>
> Un petit coup de main :
>
> ```python
> def joindre_tables(liste_inscrits, liste_performances):
> 	'''
>  	renvoie une liste obtenue par jointure des deux tables
>  	: param liste_inscrits, liste_performances
>  	type liste
>  	: return la jointure
>  	type list
>  	>>> l = lire_donnees('small_inscrits.csv')
>     	>>> creer_categories(l)
>    	>>> assigner_dossards(l)
>        >>> p = lire_donnees('small_performances.csv')
>    	>>> convertir_performances(p)
>    	>>> table_complete = joindre_tables(l, p)
>    	>>> table_complete == [['Prenom', 'Nom', 'Sexe', 'Categorie', 'Dossard', 'Heure', 'Min', 'Sec'], ['Sidney', 'Robert', 'M', 'master', 1, 1, 8, 55], ['Paien', 'Gilbert', 'M', 'master3', 2, 1, 5, 45], ['Vincent', 'Riquier', 'M', 'elite', 3, 1, 21, 23], ['Saville', 'Marier', 'M', 'master2', 4, 0, 56, 29], ['Namo', 'Lereau', 'M', 'elite', 5, 1, 6, 20], ['Romaine', 'Hughes', 'F', 'master4', 6, 1, 17, 8], ['Archard', 'Rivard', 'M', 'master3', 7, 0, 46, 31], ['Cheney', 'Chassé', 'M', 'master4', 8, 0, 48, 10], ['Avelaine', 'CinqMars', 'F', 'elite', 9, 0, 59, 57], ['Sidney', 'Charest', 'M', 'elite', 10, 1, 6, 38]]
>     True
>  	'''
>  	liste_complete = [['Prenom', 'Nom', 'Sexe', 'Categorie', 'Dossard','Heure', 'Min', 'Sec']]
>  	for i in range(1, len(liste_inscrits)):
> 	    ligne = [] 
> ```
>
> 
>
> 



## NamedTuple



Nous allons créer une collection de competiteurs avec les attributs `Prenom`, `Nom`, `Sexe`, `Categorie`, `Dossard` , `Heure`, `Min` et `Sec`.

Cela nous permettra de réaliser des tris sur notre collection.

**Pour commencer, n'oubliez pas d'importer le module `collections` !! **



> Créez une fonction `creer_collection` de paramètre _liste_, une liste complète de compétiteurs avec inscriptions et performances, qui renvoie une collection de namedtuples avec les attributs souhaités.
>
> Pour bien commencer :
>
> ```python
> def creer_collection(liste):
>  '''
>  créer une collection de tupples nommés à partir de la liste passée en paramètre.
>  : param liste liste de données chargées avec la fonction liste_donnees
>  type list
>  : return une collection de namedtuples
>  type list of namedtuples
>  
>  '''
>  # creons un tuple nommé 'villes' avec les attributs adéquats
>  competiteur = collections.namedtuple('competiteur',
>                                       ['Prenom', 'Nom', 'Sexe', 'Categorie', 'Dossard', 'Heure', 'Min', 'Sec']
>                                       )
>  collection = [] # on initialise la liste qui va contenir la collection de competiteurs
>     
> ```



_Remarque :_

On commence à avoir beaucoup de choses à écrire dans la console pour le moindre test. Pour se simplifier la vie, on utilisera la fonction ci-dessous :

```python
def test():
    '''
    Effectue un test du code
    '''
    l = lire_donnees('small_inscrits.csv')
    assigner_dossards(l)
    p = lire_donnees('small_performances.csv')
    convertir_performances(p)
    table_complete = joindre_tables(l,p)
    collection = creer_collection(table_complete)
    return collection
```



## Afficher une collection

> Réaliser une fonction `afficher` de paramètres _collection_, une collection, et _nbre_, un entier valant par défaut 10, qui affiche les _nbre_ premiers éléments de la collection sous le format suivant :
>
> Nom Prenom ( Sexe / Categorie/ Dossard ) Heure : Min : Sec
>
>  
>
> Documentez votre fonction.
>
> On pourra  uiliser les méthode  `try` et `except` pour gérer une éventuelle erreur.





Testez votre affichage via la fonction `test` :

```python
def test():
    '''
    Effectue un test du code
    '''
    l = lire_donnees('small_inscrits.csv')
    assigner_dossards(l)
    p = lire_donnees('small_performances.csv')
    convertir_performances(p)
    table_complete = joindre_tables(l,p)
    collection = creer_collection(table_complete)
    afficher(collection)
```





## Sauvegarde : ça, c'est cadeau



On utiliser la fonction `sauver` ci-dessous sans se préoccupper de son code ni des deux fonctions qu'elle utilise.

```python
def convertir_en_liste(collection):
    '''
    renvoie une liste correspondant à la collection
    : param collection une collection de namedtuples
    type list
    : return la liste correspondante
    type list
    '''
    liste = [['Prenom', 'Nom', 'Sexe', 'Categorie', 'Dossard', 'Heure', 'Min', 'Sec']]
    for comp in collection :
        liste.append([comp.Prenom, 
                      comp.Nom,
                      comp.Sexe,
                      str(comp.Categorie),
                      str(comp.Dossard),
                      str(comp.Heure),
                      str(comp.Min),
                      str(comp.Sec)]
                    )
    return liste

def convertir_csv(liste, separateur  = ';'):
    '''
    renvoie une liste de chaine compatible avec l'écriture d'un fichier en csv
    : param : liste_villes une liste de villes
    type list
    : param  :separateur par défaut ';'
    type str
    return une liste de chaines
    type list
    '''
    liste_chaines = []
    for elt in liste :
        chaine = ''
        for i in range(len(elt)) :
            chaine += elt[i]
            if i < len(elt) - 1 :
                chaine += separateur
        chaine += '\n'
        liste_chaines.append(chaine)
    return liste_chaines

def sauver(collection, nom_fichier_CSV, separateur = ';'):
    '''
    écrit dans le fichier csv la liste des chaines passées en pramètre
    : param nom_fichier_CSV
    type str
    : param separateur , le séparateur utilisé dans le fichier csv. par défaut ;
    type str
    : ne renvoie rien mais crée un fichier ou écrit dan sun fichier existant
    '''
    assert type(nom_fichier_CSV) is str,'nom_fichier_CSV is str'
    liste = convertir_en_liste(collection)
    liste = convertir_csv(liste, separateur)
    try :
        ecriture=open(nom_fichier_CSV,'w',encoding='utf-8') # ouvre un canal en écriture vers fichier2
        ecriture.writelines(liste)
        ecriture.close()
    except:
        print("!!!!!!!!!! impossible d'ecrire dans " +nom_fichier)
```



> Sauvegardez la collection obtenue via la fonction `test` dans le fichier`test.csv`.
>
> Ouvrez ensuite `test.csv` avec _Libreoffice_ pour vérifier s'il est correct.



### Tris en tout genre



**Pour commencer, n'oubliez pas d'insérer la ligne `from operator import attrgetter` en haut du code! **

Pour trier un Namedtuple, nommé par exemple _ma_collection_, selon un ( ou plusieurs) attributs on utilise la méthode ci-dessous. 

* Le tri s'effectue selon le premier attribut et, en cas d'égalite, selon le suivant (s'il y en a) et ainsi de suite.
* Le paramètre _reverse_  détermine si le tri est croissant (_reverse = False_) ou décroisant (_reverse = True_).

```python
>>> sorted(ma_collection, key = attrgetter(attribut1, attribut2, ..., reverse = False)
```

_Remarque :_

Un NamedTuple est un Tuple donc n'est pas modifiable. Aussi cette fonction de tri ne marche pas par effet de bord. Elle renvoie un nouveau namedTuple trié.



**Classement par Dossard**



> Créez une fonction `tri_dossard` de paramètre _collection_, une collection de compétiteurs, qui renvoie la collection triée uniquement selon l'attribut `Dossard` .
>
> Vous sauvegarderez la collection obtenue sous le nom `classement_par_dossard.csv`
>
> Vous joindrez le fichier à votre code.





**Classement scratch**



> Créez une fonction `tri_scratch` de paramètre _collection_, une collection de compétiteurs, qui renvoie la collection triée de façon à obtenir le classement scratch.
>
> Vous sauvegarderez la collection obtenue sous le nom sous le nom `classement_scratch.csv`. 
>
> Vous joindrez le fichier à votre code.



**Classement scratch par sexe**



> Créez une fonction `tri_sexes` de paramètre _collection_, une collection de compétiteurs, qui renvoie la collection triée de façon à obtenir le classement scratch par sexe.
>
> Vous sauvegarderez la collection obtenue sous le nom sous le nom `classement_scratch_sexes.csv`.
>
> Vous joindrez le fichier à votre code.



**Classement par sexe et catégorie**



> Créez une fonction `tri_annee` de paramètre _collection_, une collection de compétiteurs, qui sauvegarde la collection triée de façon à obtenir le classement par sexe et catégorie.
>
> Vous sauvegarderez la collection obtenue sous le nom sous le nom `classement_categories_sexes.csv`.
>
> Vous joindrez le fichier à votre code.









___

Par Mieszczak Christophe CC- BY - SA	

A partir des documents de la formation NSI de l'université de Lille 1
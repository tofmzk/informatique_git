# OXO (Tic Tac Toe) 

## Règle du jeu



![Tic tac Toe - source wikipedia - GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Tic-tac-toe-game-1.svg/479px-Tic-tac-toe-game-1.svg.png)



Dans une grille de 3 cases sur 3 cases, numérotée de 1 à 9, deux joueurs s'affrontent. Le jeu se déroule de la façon suivante :

* On initialise une grille vide
* Tant que cette grille n'est pas pleine et  que le joueur en cours n'a pas gagné :
    * Le joueur en cours choisit une case valide
    * On place le symbole qui lui correspond dans la case
    * On change le joueur en cours.
* Si l'un des joueurs gagne, on annonce sa victoire sinon,  le match nul est déclaré.



## Modélisons la grille de jeu



Il n'y a pas qu'une façon de faire ou qu'une structure à utiliser. Il faut faire un choix.

Pour modéliser la grille de jeu, la structure que nous allons utiliser est une liste de 3 listes où chaque ligne de la grille sera représentée par une liste contenant 3 éléments tous égaux à 0.


> Créez une fonction `generer_grille` qui renvoie le tableau ci-dessus en utilisant une expression régulière ou la méthode `append`. 
>
> Pour gagner un peu de temps :
>
> ```python
> # coding: utf-8
> import random
> 
> def generer_grille():
>    	'''
>     	renvoie une grille de 3 lignes contenant chacune 3 elements
>     	: return grille (list)
>     	>>> grille = generer_grille()
>     	>>> grille
>     	[[0, 0, 0], [0, 0, 0], [0, 0, 0]]
>     	>>> grille[1][2] = 1
>     	>>> grille == [[0, 0, 0], [0, 0, 1], [0, 0, 0]]
>     	True
>     	'''
>    
>     
> ```
> 
>
> 



Dans la suite, nous utiliserons des entiers pour coder ce que peut contenir une case dans le tableau qui modélise la grille de jeu :

* 0  pour une case vide.
* 1 pour une case cochée par le joueur 1.
* 2 pour une case cochée par le joueur 2.



Les cases, pour les utilisateurs, seront numérotées de 0 à 8 et affichées comme ci-dessous :

```txt
|0|1|2|
|3|4|5|
|6|7|8|
```



> Réaliser une fonction `convertir` de paramètre _n_, un entier, qui renvoie un tuple de coordonnées (x, y) dans la grille de la case No _n_. On pourra utiliser les opérateurs `//` et `%` , très utiles ici ...
>
> Pour vous aider :
>
> ```python
> def convertir(n):
>    	'''
>    	renvoie un tuple de coordonnées correspondant à la case no n. la première case est à (0, 0).
>     	:param n (int) entre 0 et 8
>     	:return tuple
>     	>>> convertir(0) == (0, 0)
>     	True
>     	>>> convertir(5) == (2, 1)
>     	True
>     	'''
>    ```



## Prédicat

Un prédicat est une fonction qui renvoie `True` or `False`.



> Réalisez une fonction `est_vide(grille, n)` de paramètre _grille_, un tableau représentant le jeu,  et _n_, un entier, qui renvoie `True` si la case _n_ de la grille est vide et `False` sinon.
>
> ```python
> def est_vide(grille, n):
> 	'''
> 	renvoie True si la case n de la grille est vide et False sinon
> 	: params
> 	grille (list)
>   n (int) entre 0 et 8 inclus
> 	: return (boolean)
> 	>>> grille = generer_grille()
> 	>>> est_vide(grille, 2)
> 	True
> 	>>> grille[0][2] = 1
> 	>>> est_vide(grille, 2)
> 	False
> 	'''
>     x, y = convertir(n)
> ```
>
> 



## Interface Homme - Machine



Nous utiliserons la console pour afficher la grille en mode texte, via la commande `print`.

Le rendu attendu est celui-ci où :

* la croix correspond au joueur 1;
* le rond au joueur 2;
*  Les cases vides affichent leur No.

```python
>>> grille = [[0, 1, 2],[0, 1, 2],[0, 2, 0]]
>>> afficher(grille)
|0|X|O|
|3|X|O|
|6|O|8|
```

Un peu comme pour la structure que nous utilisons, nous pourrions décider plus tard de changer la façon dont s'affiche notre grille en utilisant des modules Python dédiés à cela comme [pygame](https://www.cours-gratuit.com/tutoriel-python/tutoriel-python-comment-crer-un-jeu-avec-pygame) (spécialisé dans les jeux 2D) ou [Tkinter](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/234859-creez-des-interfaces-graphiques-avec-tkinter) (parfait pour réaliser des interfaces graphiques).

En séparant l'affichage du reste du code, il suffira alors de changer la fonction ci-dessous sans toucher au reste. C'est pour cette raison qu'on réalise toujours à part la fonction dédiée à l'affichage.



Pour cela nous allons définir notre fonction`afficher` de la façon suivante :

```python
def afficher(grille):
    '''
    affiche la grille de jeu.
    : param grille (list)
    : pas de return, pas d'effet de bord    
    '''
    cases =[' ', 'X', 'O'] 
    chaine = '' # cette chaîne de caractère contiendra les caractères permettant d'afficher la grille.
    # Remarquez qu'inserer le caractre "\n" permettra d'aller à la ligne lors de l'affichage.
    # cette fonction se terminera donc par : print(chaine).
    
```

_Remarque :_

Une deuxième fonction d'affichage est nécessaire : celle qui annoncera le résultat final. Mais cette dernière a besoin de fonction dont on ne dispose pas encore (qui a gagné ?) et sera donc réalisée plus loin.



## On joue



Un tour de jeu se déroule en trois étape : 

* On demande un coup valide au joueur courant.
* On place le pion correspondant 
* On change de joueur courant ... s'il n'a pas encore gagné. D'ailleurs, en raison du besoin de savoir s'il a gagné ou pas, on ne fera cette fonction que plus tard.



> Créez une fonction `demander_coup`, de paramètre _grille_ , un tableau représentant le jeu, qui demande une case au _joueur_ tant que la case proposée n'est pas vide ou pas valide , puis renvoie le numéro de case choisi.
>



```python
def demander_coup(grille):
    '''
    renvoie un coup valide choisi par le joueur
    : param grille (list)
    : return (int) no d'une case entre 0 et 8 inclus
    '''
```

_Remarque :_

En fin de projet, on demandera de coder une fonction similaire pour faire jouer l'ordinateur... Une mini IA en perspective...





>  
>
> Réaliser une fonction `placer_pion(grille, n, joueur)` qui place la symbole correspondant au _joueur_ (X ou O) dans la case _n_ de la _grille_.
>
>  N'oubliez pas la Docstring et un Doctest.
>
> 



## Alignements de 3 pions



Nous allons procéder en deux étapes :

* On va construire un tableau contenant les numéros présents horizontalement, verticalement et en diagonale dans le tableau représentant la grille de jeu.
* Une série de 3 numéros est appelé un motif.
* Si le numéro correspondant à un joueur (1 ou 2) répété trois fois de suite (111 ou 222) est dans le tableau, c'est que le joueur a gagné !



> Réalisez une fonction `lister_motifs` de paramètre _grille_, un tableau représentant le jeu , qui renvoie un tableau des 8 motifs présents dans la grille.
>
> On déclare un tableau vide _motifs_ qui va, à terme, contenir 8 chaînes de 3 caractères correspondants :
>
> * Aux 3 entiers affichés dans la première, deuxième et troisième ligne, horizontalement.
> 
> * Aux 3 entiers affichés dans la première, deuxième et troisième colonne, verticalement
>
> * Aux 3 entiers affichés dans les 2 diagonales.
>
>     
>
> voici le début de la fonction :
>
> ```python
>def lister_motifs(grille):
>     '''
>     revoie le tableau des motifs présents dans la grille
>     : param grille (list)
>     : return motifs (list)
>     >>> grille = [[0, 1, 2], [0, 2, 2],[1, 1, 2]]
> 	>>> lister_motifs(grille) == ['012', '022', '112','001', '121', '222','022', '122'] 
> 	True
>     '''
>     motifs =[]
> 
> 
> 
> ```
>     
> 







> Réaliser le prédicat `a_gagne(grille, joueur)`, de paramètre _grille_, un tableau représentant le jeu, et _joueur_, un entier (1 ou 2) représentant le joueur, qui renvoie `True` si le joueur en question a gagné et  `False` sinon.
>
> ```python
> def a_gagne(grille, joueur):
> 	'''
>  	renvoie vraie si le tableau des motifs issu de la grille contient '111' ou '222' c'est à dire str(joueur) * 3
>  	: params
>     		grille (list)
> 		joueur (int) 1 ou 2
>  	: return (boolean)
>  	>>> grille =[[0, 1, 2],[0, 2, 2],[1, 1, 2]]
>  	>>> a_gagne(grille, 1)
>  	False
>  	>>> a_gagne(grille, 2)
>  	True
>  	'''
>  	motifs = lister_motifs(grille)
> 
> 
> ```
>
> 



Maintenant que l'on est capable de savoir si un joueur a gagné ou pas, on va pouvoir coder la fonction qui renvoie le joueur suivant si le joueur courant n'a pas gagné et, sinon, renvoie le joueur courant sans en changer.



> Définissez et codez la fonction `changer_joueur(grille, joueur_courant)` qui effectue la tâche indiquée au dessus et renvoie donc le futur joueur (qui peut ne pas changer).
>
> Documentez votre fonction mais aucun Doctest n'est demandé.



Pour la même raison, nous sommes maintenant capable de réaliser la fonction qui affichera l'issu du match en précisant soit le joueur vainqueur soit le match nul.



> Définissez et codez la fonction `afficher_resultat(grille)` qui précisera qui a gagné ou déclarera un match nul.
>
> Documentez votre fonction mais aucun Doctest n'est demandé.

_Remarque :_

On peut déjà joueur depuis la console. Testez :

```python
>>> grille = generer_grille()
>>> joueur_courant = 1
>>> afficher(grille)
|0|1|2|
|3|4|5|
|6|7|8|
>>> demander_coup(grille, joueur)
>>> a_gagne(joueur)
False 
>>> joueur_courant = changer(grille, joueur_courant)
>>> afficher(grille)
## etc .....

```



## Joueurs humains

> Définissez une fonction `h_vs_h` sans paramètre, qui déroule une partie à deux joueurs humains jusqu'à son dénouement sur le modèle du déroulement dans la console joué plus haut.
>
> ```python
> def h_vs_h():
>  	'''
>  	déroule une partie humain contre humain jusqu'à la victoire d'un des joueurs ou un match nul
>  	'''
>  	grille = generer_grille()
>  	joueur_courant = 2
>  	nbre_coups = 0
>  	while nbre_coups < 8 and not a_gagne(grille, joueur_courant):
>      
> ```
>
> 
>
>  Testez différentes parties !!
>
> 







## Plus loin, en option, pour les très costauds : Humain VS Ordinateur



L'humain sera le joueur 1, l'ordinateur sera toujours le joueur 2.

Il joue  selon la stratégie hiérarchisée ci-dessous :

1. s'il peut gagner alors il gagne.
2. sinon s'il risque de perdre alors il bloque la case dangereuse.
3. sinon si le centre est libre alors il joue au centre.
4. sinon si un coin au moins est libre alors il choisi un coin au hasard.
5. sinon il choisit une case libre restante au hasard.



Un peu d'aide car ce n'est pas _facile_ tout ça. On découpe le gros problème en plusieurs petits problèmes moins difficiles à régler. On utilisera, évidemment, les fonctions codées plus haut : pas question de ré-écrire un code déjà écrit !

```python
def ordi_gagne(grille, n):
    '''
    renvoie True si l'ordi gagne en jouant dans la case n
    : params
    	grille (list)
    	n (int) un numéro de case entre 0 et 8 inclus
    : return  (boolean)
    PAS D'EFFET DE BORD SUR grille, ne pas oublier de retirer le pion joué !!
    '''

    
    

def humain_gagne(grille, n):
    '''
    renvoie True si l'humain gagne en jouant dans la case n
	: params
    	grille (list)
    	n (int) un numéro de case entre 0 et 8 inclus			
    : return (boolean)
    : PAS D'EFFET DE BORD SUR grille !!
    '''
    
    
def coins_libres(grille):
    '''
    renvoie un tableau contenant les no des coins vides
    : param grille (list)
    : return (list) la liste des coins vides
    '''
    
def cases_libres(grille):
    '''
    renvoie un tableau contenant les no des cases vides
    : param grille (list)
    : return (list) la liste des cases vides
    '''

def demander_coup_ordi(grille):
    '''
    l'ordinateur, qui prend la place du joueur2, choisi une case selon le principe hiérarchisé suivant :
        1. s'il peut gagner alors renvoie la case où il gagne
        2. sinon, s'il risque de perdre alors il renvoie la case risquée.
        3. sinon, si le centre est libre alors il renvoie le centre
        4. sinon, si un coin au moins est libre, alors il renvoie un coin au hasard
        5. sinon il renvoie une case libre au hasard.
    : param  grille(list)
    : return (int) la case choisie
    '''
    
           
        
 
```



>  Il n'y a _plus qu'à_ réaliser une fonction `h_VS_o` sans paramètre, qui déroule une partie à deux joueurs, humain (joueur 1) contre ordinateur (joueur 2), jusqu'à son dénouement :
>
>  * soit la victoire d'un des joueurs
>  * soit le match nul
>
>  Cette fonction s'inspire fortement de `h_VS_h` ...
>
>  



____

Par Mieszczak Christophe CC BY SA

source image  : [ tic tac toe - wikipedia  CC BY SA](https://commons.wikimedia.org/wiki/File:Tic-tac-toe-game-1.svg)




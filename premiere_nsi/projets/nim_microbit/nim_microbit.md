# NIM et _micro:bit_

![allumettes - perso](../nim/media/allumettes.jpg)



Nous allons reprendre le projet du [jeu de Nim](../nim/nim.md) pour l'adapter à la carte _micro:bit_ :

* Le nombre d'allumettes est maintenant compris entre 4 et 25.
* Chaque LED allumée représente une allumette. A chaque allumette enlevée, une LED s'éteint. 
* Lorsque l'humain joue :
    * La pression sur le bouton _A_ enlève une allumette (avec un maximum de trois)
    * La pression dur le bouton _B_, ou le fait d'avoir enlever 3 allumettes, donne la main à l'ordinateur.
* L'ordinateur joue toujours de la même façon.
* Celui qui éteint la dernière LED gagne.



## Qui commence ?

* Créez un fichier `nim_microbit.py`

* Commencer par importer toute le module `microbit` ainsi que le module `random`.

    ```python
    from microbit import *
    import random
    ```

_Remarque :_

* MicroPython n'a pas de module `doctest`

* Allez consulter le cours sur la carte [micro:bit](architecture/IHM/carte_microbit.md) dès que vous en avez besoin !

    

> Créez une fonction `depart`, sans paramètre, qui renvoie 'H' si l'Humain commence et 'O' sinon.
>
> * Une pression sur le bouton de gauche fait passer le choix de 'H', pour Humain, à 'O', pour 'Ordi, en tant que joueur de départ. 'H' ou 'O' s'affiche alors sur l'écran.
> * Une pression sur le bouton de droit valide le choix du joueur qui commence.
>
> Documentez cette fonction





## Gestion des LEDs





> Réalisez une fonction `afficher`, de paramètre _nbre_allumettes_, un entier entre 4 et 25, qui allume les _nbre_allumettes_ premières LEDs de l'écran et éteint les autres.
>
> On utilisera la méthode `set_pixel` de `display`.
>
> Documentez cette fonction



## Changement de joueur



> Réalisez une petite fonction `changer` , de paramètre _joueur_, le joueur en cours 'H' ou 'O', qui renvoie le prochaine joueur, soit l'humain, 'H', soit l'ordinateur, 'O'.
>
> Documentez cette fonction



## Choix de l'humain



> Réliser une fonction `humain`, de paramètre _nbre_allumettes_, le nombre d'allumettes restantes à enlever qui :
>
> * Utilise les deux boutons de la carte ainsi :
>
>     * une pression sur le bouton _A_ enlève une allumette,dans la limite de 3 et du nombre d'allumettes restantes. S'il ne reste plus d'allumettes ou si l'humain en a enlevé 3, il perd la main. A chaque pression sur le bouton _A_, l'affichage et modifié en conséquence. On pensera à utiliser la fonction `sleep` après l'affichage pour permettre au joueur d'avoir le temps de le voir.
>     * Une pression sur le bouton _B_, si au moins une allumette a déjà été enlevée, donne la main à l'ordinateur.
>
> * renvoie au final le nombre d'allumettes à enlever
>
>      
>
> Documentez cette fonction



## Choix de l'ordinateur



_Petit rappel sur la stratégie de l'ordinateur :_

* Si il reste _n_ allumettes lorsque c'est au tour de l'ordinateur de joueur :
    * si __nbre_allumettes_ % 4 est compris entre 1 et 3, l'ordinateur choisit d'enlever _nbre_allumettes_ % 4 allumettes
    * sinon il enlève, au hasard, un nombre d'allumettes compris entre 1 et 3.





> Réalisez la fonction `ordi` ,  de paramètre _nbre_allumettes_, le nombre d'allumettes restantes à enlever qui :
>
> * détermine le nombre _n_ d'allumettes que l'ordinateur choisit d'enlever
> * Actualise l'affichage en conséquence. On pensera à utiliser la fonction `sleep` après l'affichage pour permettre au joueur d'avoir le temps de le voir.
> * renvoie _n_
>
> 





## Déroulement du jeu

* Le nombre total d'allumettes est choisi au hasard entre 4 et 25 inclus.
* Ensuite, on va demander à l'humain s'il veut commencer ou pas :
    *  S'il commence alors la variable _joueur_ sera initialisée avec la valeur 'H'
    *  sinon elle sera initialisée avec la valeur 'O'
* Tant que le nombre d'allumettes est strictement supérieur à 0
    * on affiche le joueur en cours.
    * on affiche la situation de jeu.
    * si le joueur est 'H' alors on lui demande le nombre d'allumettes qu'il souhaite enlever.
    * sinon on demande à l'ordinateur.
    * on enlève le nombre choisi au nombre total d'allumettes
    * s'il reste encore des allumettes, on passe au joueur suivant
* A la suite de cette boucle, on annonce la victoire du _joueur_ en faisant défiler un message.



> * Définissez la fonction `jouer()` , qui réalise le déroulement du jeu.
>
>     Un peu d'aide :
>
>     ```python
>     def jouer():
>         '''
>         lance et déroule la partie avec nbre_allumettes à enlever au début
>         : param nbre_allumettes
>         type int
>         '''
>         nbre_allumettes = random.randint(4, 25)
>         joueur = depart()
>         while nbre_allumettes > 0:
>             display.scroll(joueur)
>             afficher(nbre_allumettes)
>             
>         
>     ```
>
>     
>




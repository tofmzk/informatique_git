# La conjecture de Syracuse 

## La conjecture

Commençons par choisir un entier _n_ non nul quelconque. Appliquons ensuite l'algorithme ci-dessous :



```

- Tant que n est supérieur strictement à 1 :
  - Si n est pair, c'est à dire divisible par 2, alors il prend la valeur n // 2
  - Sinon il prend la valeur 3 * n + 1

```



_Exemple :_

Prenons _n_ =3

* _n_ est impair donc il prend la valeur n * 3 + 1 = 10
* _n_ est pair maintenant, donc il prend la valeur _n_ // 2 = 5
* _n_ est de nouveau impair, il prend la valeur n * 3 + 1 = 16
* _n_ est pair maintenant, donc il prend la valeur _n_ // 2 = 8
* _n_ est pair maintenant, donc il prend la valeur _n_ // 2 = 4
* _n_ est pair maintenant, donc il prend la valeur _n_ // 2 = 2
* _n_ est pair maintenant, donc il prend la valeur _n_ // 2 = 1

C'est terminé, _n_ est 'retombé' à 1.



La conjecture de Syracuse affirme que, quelque soit la valeur de départ de _n_, il prendra au bout d'un certain nombre d'itérations, la valeur 1. 

* On appelle _temps de vol_ le nombre d'itérations nécessaires pour que _n_ prenne la valeur 1

* On appelle altitude maximale la plus grande valeur prise par _n_ avant de retomber à 1.

  

_Exemple :_

En reprenant l'exemple précédent :

* _n_ retombe bien à 1.
* Il faut 7 itérations pour retomber à 1 donc le temps de vol vaut 7.
* L'altitude maximale est la plus grande valeur de _n_ atteinte : c'est 16



**A ce jour la conjecture de Syracuse n'est toujours pas démontrée !!**



>  **Petite question : ** 
>
> Que se passe-t-il si la valeur de départ de _n_ est 0 ?



_Remarque :_

[Pour en savoir plus, suivez ce lien ...](https://fr.wikipedia.org/wiki/Conjecture_de_Syracuse)



## Calcul du prochain _n_



> * Définissez une fonction _terme_suivant_, de paramètre _n_, un entier positif non nul, dont le but sera de renvoyer la prochaine valeur de _n_
> * Documentez votre fonction
> * Préparez 3 Doctests 
> * Faites les assertions nécessaires.
> * Depuis la console, appelez votre fonction  avec diverses valeurs de _n_.





## Temps de vol

> * Définissez une fonction temps_de_vol, de paramètre _n_ , un entier positif non nul, dont le but sera de renvoyer le nombre d'itération(s) qu'il faut à _n_ pour 'retomber' à 1 en suivant l'algorithme de Syracuse.
> * Documentez votre fonction.
> * Préparez 3 Doctests .
> * Faites les assertions nécessaires.
> * Depuis la console, appelez votre fonction  avec diverses valeurs de _n._







## Altitude maximale

> - Définissez une fonction altitude_maximale, de paramètre _n_ , un entier positif non nul, dont le but sera de renvoyer l'altitude maximale atteinte par _n_ en suivant l'algorithme de Syracuse.
> - Documentez votre fonction.
> - Préparez 3 Doctests .
> - Faites les assertions nécessaires.
> - Depuis la console, appelez votre fonction  avec diverses valeurs de _n._







## Testons la conjecture 

> - Définissez une fonction _tester_conjecture_, de paramètre _n1_  et _n2_, deux entiers positifs non nuls tels que _n1<n2_, dont le but sera tester la conjecture de Syracuse pour tous les entiers entre _n1_ et _n2_. Elle renverra le message "test validé" à la fin du test  s'il est concluant et bouclera indéfiniment sinon.
> - Documentez votre fonction.
> - Faites les assertions nécessaires.
> - Depuis la console, appelez votre fonction  avec diverses valeurs de  _n1_ et _n2_.





________

Par Mieszczak Christophe 

licence CC BY SA




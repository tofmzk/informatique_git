#!/usr/bin/python3
# -*- coding: utf-8 -*-

import module_csv

from socket import socket, AF_INET, SOCK_STREAM

PORT = 13450
SOCKET = socket(AF_INET, SOCK_STREAM)


def lire_HTML(nom_fichier):
    '''
    renvoie le contenue du fichier HTML de nom passé en paramètre au format str
    : param nom_fichier
    type str
    : return
    type str
    '''
    assert type(nom_fichier) is str,'nom_fichier HTML is str'
    try :
        lecture = open(nom_fichier, 'r', encoding='utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le fichier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    lecture.close()
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n")# enleve les "\n" et convertit en list
    chaine_HTML = ''
    for ligne in toutes_les_lignes :
        chaine_HTML += ligne
    return chaine_HTML
    

def demarrer() :
    '''
    ouvre le PORT sur lequel le serveur écoute.
    si celui-ci est disponible, on demarre le serveur    
    '''
    # on ouvre le port PORT
    SOCKET.bind(("", PORT))
    SOCKET.listen(5)
    demarrer_serveur()
    #except :
     #  return 'le port ' + str(PORT) + ' est encore occupé, attendez un petit moment svp'
 


def demarrer_serveur() :
    while True:
        requete = ecouter()
    print("FIN !")
    SOCKET.close()
    
def ecouter() :
    connexion, addresse = SOCKET.accept()
    requete = connexion.recv(1024).decode()
    traiter_requete(connexion, requete)
    connexion.close()
  

def traiter_requete(connexion, requete):
    if requete != '' :
        print(requete)
        if requete[:3] == 'GET' :
            traiter_get(connexion)
        if requete[:4] == 'POST' :
            traiter_post(connexion, requete)

def traiter_get(connexion):
    '''
    renvoie la page d'identification
    '''
    envoyer_page(connexion, 'identification.html')
    
def traiter_post(connexion, requete):
    '''
    détermine de quelle page est issue la requête et
    lance la fonction correspondant à son traitemenet
    : param connexion
    type socket
    : param requete
    type str    
    '''
    if 'nom' in requete  :
        traiter_inscription(connexion, requete)
    else :
        traiter_identification(connexion, requete)
        

def traiter_identification(connexion, requete):
    '''
    vérifie si l'utilisateur est dans la base et renvoie la page ok.html ou inscription.html
    : param connexion
    type socket
    : param requete
    type str 
    '''
    identifiant, mdp = parametres_identification(requete)
    if est_dans_base(identifiant, mdp):
        envoyer_page(connexion, 'ok.html')
    else :
        envoyer_page(connexion, 'inscription.html')
        

def est_dans_base( identifiant, mdp):
    '''
    renvoie True si identifiant et mdp sont dans la base
    : param identifiant, mdp
    type str
    : return True or False
    type booleen
    '''
    base = module_csv.lire_donnees('base.csv')
    for i in range(1, len(base)):
        if base[i][2] == identifiant and base[i][3] == mdp :
            return True
    return False
    
    
def parametres_identification(requete):
    '''
    renvoie les paramètres de l'identification
    ; param requete
    type str
    : return les paramètres
    type tuple
    '''
    requete_lignes = requete.split('\r')
    requete_derniere_ligne = requete_lignes[len(requete_lignes) - 1]
    identifiant = requete_derniere_ligne.split('=')[1].split('&')[0]
    mdp = requete_derniere_ligne.split('=')[2]
    return identifiant, mdp


def traiter_inscription(connexion, requete):
    '''
    ajoute les données d'inscription au fichier base.csv puis renvoie la page d'inscription
    : param connexion
    type socket
    : param requete
    type str  
    '''
    nom, prenom, identifiant, mdp = parametres_inscription(requete)
    if est_valide(nom, prenom, identifiant, mdp) :
        base = module_csv.lire_donnees('base.csv')
        base.append([nom, prenom, identifiant, mdp])
        module_csv.sauver(base, 'base.csv')
        envoyer_page(connexion, 'identification.html')
    else :
        envoyer_page(connexion, 'inscription.html')    

def est_valide(nom, prenom, identifiant, mdp):
    '''
    renvoie `True` si les paramètres sont valides et `False` sinon.
    : param nom, prenom, identifiant, mdp
    type str
    : return True ou False
    boolean
    '''
    if est_dans_base(identifiant, mdp) or nom == '' or prenom == '' or identifiant == '' or mdp == '' :
        return False
    chiffre_present = False
    for chiffre in range(10) :
        if str(chiffre) in mdp :
            chiffre_present = True
    if len(mdp) < 5 or not chiffre_present :
        return False
    return True
    
    
def parametres_inscription(requete):
    '''
    renvoie les paramètres de la requete
    : param requete
    type str
    : return les paramètres
    type tuple
    
    '''
    requete_lignes = requete.split('\r')
    requete_derniere_ligne = requete_lignes[len(requete_lignes) - 1]
    nom = requete_derniere_ligne.split('=')[1].split('&')[0]
    prenom = requete_derniere_ligne.split('=')[2].split('&')[0]
    identifiant = requete_derniere_ligne.split('=')[3].split('&')[0]
    mdp = requete_derniere_ligne.split('=')[4].split('&')[0]
    return nom, prenom, identifiant, mdp
    

def envoyer_page(connexion, nom_page):
    '''
    Renvoie le contenu de la pahe HTML nommée nom_page au client
    : param connexion
    type socket
    : param nom_page nom de la page html nom.html
    type str
    '''
    reponse = "HTTP/1.1 200 Ok host : mon site local Content-Type: text/html\n\n " + lire_HTML(nom_page)
    connexion.send(reponse.encode())    
    


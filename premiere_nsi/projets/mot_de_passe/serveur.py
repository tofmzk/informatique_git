#!/usr/bin/python3
# -*- coding: utf-8 -*-

import module_csv
from socket import socket, AF_INET, SOCK_STREAM

PORT = 13450
SOCKET = socket(AF_INET, SOCK_STREAM)


def lire_HTML(nom_fichier):
    '''
    renvoie le contenue du fichier HTML de nom passé en paramètre au format str
    : param nom_fichier (str)
    : return (str)
    '''
    assert type(nom_fichier) is str,'nom_fichier HTML is str'
    try :
        lecture = open(nom_fichier, 'r', encoding='utf_8') # ouvre un canal en lecture vers text.txt
    except FileNotFoundError :
        raise # renvoie une erreur si le fichier n'existe pas
    toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
    lecture.close()
    for i in range(0,len(toutes_les_lignes)): 
        toutes_les_lignes[i] = toutes_les_lignes[i].rstrip("\n")# enleve les "\n" et convertit en list
    chaine_HTML = ''
    for ligne in toutes_les_lignes :
        chaine_HTML += ligne
    return chaine_HTML
    

def demarrer() :
    '''
    ouvre le PORT sur lequel le serveur écoute.
    si celui-ci est disponible, on demarre le serveur    
    '''
    SOCKET.bind(("", PORT))
    SOCKET.listen(5)
    demarrer_serveur()
   
 


def demarrer_serveur() :
    '''
    demarre le serveur et écoute les requêtes
    '''
    while True:
        requete = ecouter()
    print("FIN !")
    SOCKET.close()
    
def ecouter() :
    '''
    ecoute et traite les requêtes reçues
    '''
    connexion, addresse = SOCKET.accept()
    requete = connexion.recv(1024).decode()
    traiter_requete(connexion, requete)
    connexion.close()
  

def traiter_requete(connexion, requete):
    '''
    lance le traitement des requêtes selon qu'elles soient du type GET ou POST
    : params
        connexion(socket)
        requete (str)
    '''
    if requete != '' :
        print(requete)
        if requete[:3] == 'GET' :
            traiter_get(connexion)
        if requete[:4] == 'POST' :
            traiter_post(connexion, requete)
            
def envoyer_page(connexion, nom_page):
    '''
    Renvoie le contenu de la page HTML nommée nom_page au client
    : params
        connexion (socket)
        nom_page (str) nom de la page html nom.html
    : pas de return mais un renvoie via connexion.send(..)
    '''

def traiter_get(connexion):
    '''
    renvoie la page d'identification
    : param connexion (socket)
    : pas de return utilise envoyer_page
    '''
    
    
def traiter_post(connexion, requete):
    '''
    détermine de quelle page est issue la requête et
    lance la fonction correspondant à son traitement
    : params
        connexion (socket)
        requete (str)
    '''
    
           

   
def parametres_identification(requete):
    '''
    renvoie les paramètres de l'identification
    : param requete (str)
    : return les paramètres (tuple)
    '''

def parametres_inscription(requete):
    '''
    renvoie les paramètres de la requete
    : param requete (str)
    : return les paramètres (tuple)    
    '''

def est_dans_base( identifiant, mdp):
    '''
    renvoie True si identifiant et mdp sont dans la base
    : params
        identifiant (str)
        mdp (str)
    : return (bool)
    '''
    

def traiter_identification(connexion, requete):
    '''
    vérifie si l'utilisateur est dans la base et renvoie la page ok.html ou inscription.html
    : params
        connexion (socket)
        requete (str)
    : pas de return , utilise envoyer_page        
    '''

def est_valide(nom, prenom, identifiant, mdp):
    '''
    renvoie `True` si les paramètres sont valides et `False` sinon.
    : params
        nom, prenom, identifiant, mdp (str)
    : return (bool) 
    '''    

def traiter_inscription(connexion, requete):
    '''
    ajoute les données d'inscription au fichier base.csv puis renvoie la page d'inscription
    : params
        connexion (socket)
        requete (str)
    : pas de return, modifie base.csv
    '''
   


    
    


    


   
    


# Inscription et mot de passe 





## Objectif



Nous allons réaliser une interface web de gestion de nom d'utilisateurs et mots de passe simplifiée.

* Lors de la demande d'accès à un site web, ce dernier vous demande votre nom d'utilisateur et mot de passe.
* Il consulte sa table de donnée afin de savoir s'ils sont corrects
* Si ils sont sont incorrects, il vous renvoie une page permettant de vous inscrire.
    * si l'inscription est valide :
        *  il enregistre les nouvelles données dans sa table.
        * il charge la page d'identification à nouveau.
    * sinon, il redemande de vous inscrire.
* Si ils sont corrects, il renvoie une page stipulant qu'il vous a identifié.



Il s'agit d'un système très simplifié et sans sécurité. Il serait simple d'ouvrir le tableau des identifiants et des mots de passe. 



## Pages HTML

Allez relire le cours sur [les requêtes GET et POST](../../interactions_homme_machine_web/get_post.md).

### S'inscrire



> * Réalisez un page HTML nommée `inscription.html`, sans javascript ni feuille de style, permettant d'afficher le formulaire ci-dessous :
>
> ![inscription - source sperso](media/inscription.jpg)
>
> * Le formulaire utilisera la méthode `POST` vers le serveur Python `serveur.py` dont vous préciserez l'IP et le port.



### S'identifier



> * Réalisez un page HTML nommée `identification.html`, sans feuille de style ni page js, permettant d'afficher le formulaire ci-dessous :
>
> ![inscription - source sperso](media/identification.jpg)
>
> * Le formulaire utilisera la méthode `POST` vers le serveur Python `serveur.py` dont vous préciserez l'IP et le port.



### Identification vérifiée !



> * Réalisez un page HTML nommée `ok.html`, sans feuille de style ni javascript, permettant d'afficher le texte ci-dessous 
>
>     ![ok - source perso](media/ok.jpg)





## Côté serveur



### serveur.py



Ouvrez le fichier `serveur.py` fourni.

* Comme déjà vu lors du cours sur les méthodes `GET` et `POST` :

    * la fonction `demarrer` lance le serveur qui _écoute_ le port 13450.

    *  les fonction `traiter_get` ou  `traiter_post` se lancent selon le type de la requête.

    * La fonction `lire_HTML` récupère le code de la page HTML passée en paramètre dans une chaine de caractère.

       

### Format de la réponse du serveur

Lorsque le serveur démarre, une _connexion_ est crée. S'il doit renvoyer une réponse au client, cette réponse doit avoir un format précis.

* Tout d'abord, elle possède une entête au format _str_ :

    ```python
    entete = "HTTP/1.1 200 Ok host : mon site local Content-Type: text/html\n\n "
    ```

* Cette entête est suivie du code HTML de la page à renvoyer. Si cette page est nommée `page.html`, par exemple,  on récupère le code au format _str_ en utilisant la fonction `lire_HTML` .

    ```python
    code_HTML = lire_HTML('page.html')
    ```

* On peut alors définir la réponse au format _str_ :

    ```python
    reponse = entete + code_HTML
    ```

* Il suffit alors de renvoyer cette réponse. Mais cette dernière doit être au format _Bytes_ , le format binaire. Pour cela, il suffit de convertir notre chaine _reponse_ avec la méthode `encode` :

    ```python
    connexion.send(reponse.encode())
    ```



> Réalisez la fonction `envoyer_page`, de paramètres _connexion_ et _nom_page_, qui renvoie au client une réponse correctement formatée avec le code de la page HTML.
>
> ```python
> def envoyer_page(connexion, nom_page):
>     '''
>     Renvoie le contenu de la page HTML nommée nom_page au client
>     : params
>     	connexion(socket)
>     	nom_page nom de la page html nom.html (str)
>     : pas de return mais un renvoie via connexion.send(..)
>     '''
> ```
>
> 





### Requête GET



Considérons que le serveur a été démarré. 

Si on ouvre un navigateur et qu'on tape l'IP du serveur suivi du port utilisé (192.168.0.12:13450 par exemple), on envoie alors une requête `GET` au serveur. Ce sera la seule requête de ce genre qu'il gérera : il réagit en revoyant la page `identification.html`.



> Réalisez la fonction `traiter_get`, de paramètre _connexion_, qui renvoie la page `identification.html`.
>
> ```python
> def traiter_get(connexion):
>  '''
>  renvoie la page d'identification
>  : pas de return utilise envoyer_page
>  '''
> ```
>
> 



Pour testez cela :

* démarrez le serveur
* ouvrez un navigateur
* tapez dans la barre de navigation IP:Port (192.168.0.12:13450 par exemple) : la page d'identification doit être envoyée au client, c'est à dire au navigateur.



### Requêtes POST



Deux pages peuvent envoyer une requête `POST` au serveur :

* la page `inscription.html` qui retournera les paramètres _nom_, _prenom_, _identifiant_ et _mdp_.
* la page `identification.html` qui retournera les paramètres _identifiant_ et _mdp_.



> Codez la fonction `traiter_post` de paramètres _connexion_ et _requete_, qui lancera la fonction `traiter_identification` si les paramètres envoyés sont ceux de la page `identification.html` et `traiter_inscription` sinon. Les deux fonctions  `traiter_identification`  et  `traiter_inscription` seront codées plus tard.
>
> ```python
> def traiter_post(connexion, requete):
> 	'''
>  	détermine de quelle page est issue la requête et	
> 	lance la fonction correspondant à son traitement
>  	: params
>     	connexion (socket)
>  		requete (str)
>  	: pas de return 
>  	'''
> 
> 
> 
> def traiter_identification(connexion, requete):
>     '''
>     vérifie si l'utilisateur est dans la base et renvoie la page ok.html ou inscription.html selon le cas
>     : params
>     	connexion(socket)
>         requete (str)
>     : pas de return utilise envoyer_page
>     '''
> 
> 
> 
> 
> def traiter_inscription(connexion, requete):
>     '''
>     si les paramètres sont corrects, ajoute les données d'inscription au fichier base.csv puis renvoie la page d'inscription
>     sinon, renvoie la page d'incription
>     : params 
>     	connexion(socket)
>     	requete (str)
>     : pas de return utilise envoyer_page
>     '''
> 
> ```
>
> 

### Les paramètres



Tout d'abord, i lfaut comprendre où se trouve les paramètres dans la requête.

* Démarrez le serveur

* Demandez la page des inscriptions comme vu plus haut.

* Cliquez sur le bouton `valider`.

* Retourner voir la console du serveur. Elle affiche ceci :

    ```txt
    POST / HTTP/1.1
    Host: 192.168.0.12:13450
    User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
    Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 38
    Origin: http://192.168.0.12:13450
    Connection: keep-alive
    Referer: http://192.168.0.12:13450/
    Upgrade-Insecure-Requests: 1
    
    identifiant=identifiant&motdepasse=mdp
    ```

    Les identifiants et le mot de passe se situent dans la dernière ligne, entre les caractères `=` et `&`.
    
    Le passage à la ligne utilise ici le caractère d'échappement `\r`



 * Dans la console copiez et validez :

    ```python
     >>> requete = 'POST / HTTP/1.1\rHost: 192.168.0.12:13450\rUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\rAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\rAccept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3\rAccept-Encoding: gzip, deflate\rContent-Type: application/x-www-form-urlencoded\rContent-Length: 38\rOrigin: http://192.168.0.12:13450\rConnection: keep-alive\rReferer: http://192.168.0.12:13450/\rUpgrade-Insecure-Requests: 1\r\ridentifiant=identifiant&motdepasse=mdp'
    ```

    

 * Commençons par séparer les lignes en créant une liste :

     ```python
     >>> lignes = requete.split('\r')
     >>> lignes
     ?
     ```

* Les paramètres sont dans la dernière ligne : Récupérons là !

     ```python
     >>> derniere_ligne = lignes[len(lignes) - 1]
     >>> derniere_ligne
     ???
     
     ```

* Séparons maintenant les paramètres en coupant cette chaine selon les `=` et les `&`

     ```python
     >>> parametres = derniere_ligne.split('=')[1].split('&')
     >>> parametres
     ???
     ```



> * Réalisez la fonction `parametres_identification` , de paramètres _connexion_ et _requete_ qui renvoie les paramètres du formulaire placé dans la variable _requete_.
>
>     ```python
>     def parametres_identification(requete):
>         '''
>         renvoie les paramètres de l'identification
>         : params
>         	requete(str)
>         : return les paramètres (tuple)
>         '''
>     ```
>
> 
>
> * Sans démarrer le serveur, testez cette fonction avec la requête déjà vue plus haut
>
>     ```python
>      >>> requete = """'POST / HTTP/1.1\rHost: 192.168.0.12:13450\rUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\rAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\rAccept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3\rAccept-Encoding: gzip, deflate\rContent-Type: application/x-www-form-urlencoded\rContent-Length: 38\rOrigin: http://192.168.0.12:13450\rConnection: keep-alive\rReferer: http://192.168.0.12:13450/\rUpgrade-Insecure-Requests: 1\r\ridentifiant=identifiant&motdepasse=mdp'
>     >>> parametres_identification(requete)
>     ???
>     ```
>
> 



Il va falloir maintenant adapter cette technique pour récupérer les paramètres d'une requête envoyée par `inscription.html` . Cette requête sera de la forme :

```python
POST / HTTP/1.1
Host: 192.168.0.12:13450
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 38
Origin: http://192.168.0.12:13450
Connection: keep-alive
Referer: http://192.168.0.12:13450/
Upgrade-Insecure-Requests: 1

identifiant=identifiant&motdepasse=mdp
POST / HTTP/1.1
Host: 192.168.0.12:13450
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 60
Origin: http://192.168.0.12:13450
Connection: keep-alive
Referer: http://192.168.0.12:13450/
Upgrade-Insecure-Requests: 1

nom=nom&prenom=prenom&identifiant=identifiant&motdepasse=mdp
```





> * Réalisez la fonction `parametres_identification` , de paramètres _connexion_ et _requete_ qui renvoie les paramètres du formaulaire placée dans la variable _requete_.
>
>     ```python
>     def parametres_inscription(requete):
>         '''
>         renvoie les paramètres de l'inscription
>         param requete (str)
>         : return les paramètres (tuple)
>         '''
>     ```
>
> 
>
>     * Sans démarrer le serveur, testez cette fonction avec la requête déjà vue plus haut
>     
>     ```python
>    >>> requete = 'POST / HTTP/1.1\rHost: 192.168.0.12:13450\rUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\rAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\rAccept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3\rAccept-Encoding: gzip, deflate\rContent-Type: application/x-www-form-urlencoded\rContent-Length: 38\rOrigin: http://192.168.0.12:13450\rConnection: keep-alive\rReferer: http://192.168.0.12:13450/\rUpgrade-Insecure-Requests: 1\r\ridentifiant=identifiant&motdepasse=mdp\rPOST / HTTP/1.1\rHost: 192.168.0.12:13450\rUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0\rAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\rAccept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3\rAccept-Encoding: gzip, deflate\rContent-Type: application/x-www-form-urlencoded\rContent-Length: 60\rOrigin: http://192.168.0.12:13450\rConnection: keep-alive\rReferer: http://192.168.0.12:13450/\rUpgrade-Insecure-Requests:1\r\rnom=nom&prenom=prenom&identifiant=identifiant&motdepasse=mdp'
>     >>> parametres_inscription(requete)
>     ???
>     
>     ```
>
> 



### Identification



Il aut nous falloir gérer des données en table.

* Ouvrez le fichier `base.csv` fourni. Pour l'instant il y a un seul inscrit dont nous nous servirons pour tester l'identification

    
  
* Vous disposez du module `module_csv` qui vous propose :

    * La fonction `lire_donnee` pour récuperer les valeurs du tableau dans une liste

    * La fonction `sauver` pour sauvegarder les données dans un fichier 

        

        

> Réaliser la fonction `est_dans_base` de parametres _identifiant_ et _mdp_ qui renvoie `True` si l'utilisateur est présent dans `base.csv` et `False` sinon
>
> ```python
> def est_dans_base( identifiant, mdp):
>  '''
>  renvoie True si identifiant et mdp sont dans la base
>  : params 
>  	identifiant (str)
>  	mdp (str)
>  : return (bool)
>  '''
> ```
>
> * Testez cette fonction avec le seul présent pour l'instant puis avec un mauvais utilisateur.



> Reprenez et codez la fonction `traiter_identification` déjà définie plus haut. Cette fonction, récupére l'identifiant et le mot de passe. S'ils sont présents dans `base.csv`, alors le serveur renvoie la page `ok.html`sinon il renvoie la page `inscription.html`
>
> ```python
> def traiter_identification(connexion, requete):
> 	'''
>     vérifie si l'utilisateur est dans la base et renvoie la page ok.html ou inscription.html
>     : params
>        connexion(socket)
>        requete (str)
>     : pas de return utilise envoyer_page
> 	'''
> ```
>
> 



### Inscription

On considère que l'inscription est valide si et seulement si :

* les identifiants et mots de passe ne sont pas déjà dans la base
* aucun champ n'est vide
* le mot de passe possède au minimum 5 caractères dont au moins un chiffre.



> Réalisez la fonction `est_valide` de paramères _nom_, _prenom_, _identifiant_  et _mdp_ qui renvoie `True` si les paramètres sont valides et `False` sinon.
>
> ```python
> def est_valide(nom, prenom, identifiant, mdp):
>     '''
>     renvoie `True` si les paramètres sont valides et `False` sinon.
>     : params
>     	nom (str)
>     	prenom (str)
>     	identifiant(str)
>    		mdp(str)
> 	: return (bool)
>     '''
> ```
>
> 



> Reprenez le code de la fonction `traiter_inscription` déjà définie plus haut qui récupère les paramètres de la requête d'`inscription.html` et :
>
> * si ils sont corrects, ajoute l'utilisateur à la base puis renvoie la page `identification.htm`
> * si ils sont incorrects, renvoie la page `inscription.html`
>
> ```python
> def traiter_inscription(connexion, requete):
>     '''
>     ajoute les données d'inscription au fichier base.csv puis renvoie la page d'inscription
>     : params
>     	connexion (socket)
>     	requete (str)
>     : pas de return mais modification de base.csv
>     '''
> ```
>
> Testez notre serveur en inscrivant de nouveaux utilisateurs.







__________________________

Mieszczak Christophe CC- BY - SA

_source images productions personnelles_
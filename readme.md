# L'informatique au lycée

  * ### [La SNT en seconde](snt/readme.md)
  
  * ### [La NSI en première](premiere_nsi/readme.md)
  
  * ### [La NSI en terminale](terminale_nsi/readme.md)

  ​    

______

  Par Mieszczak Christophe

  Licence CC BY SA


# Bac blanc  NSI - v 1.0 _ correction

## Exercice 1

_Dans cet exercice, on parle des bases de données relationnelles._

On pourra utiliser les mots clés suivants : 

`SELECT, FROM, WHERE, INSERT, INTO, VALUES, COUNT, OR, AND, ORDER BY, JOIN, ON`.

Des acteurs ayant joué dans des pièces de théâtre sont recensés dans une base de données nommée `Theatre` dont le schéma relationnnel est :

* piece (id_piece (INT), titre (TEXT), langue (TEXT), PK (id_piece))
* acteur(id_acteur (INT), nom (TEXT) , prenom (TEXT), annee_naiss (DATE), PK(id_acteur))
* role(id_piece (INT), id_acteur (INT), nom_role (TEXT), PK(id_piece),FK(id_acteur) REF(acteur(id_acteur)), FK(id_piece) REF(piece(id_piece)))

### Partie A **1,75 pt**s.

1. Expliquer ce que signifient les mots `relation`, `enregistrement`, `attribut`, `domaine` et `valeur` pour une base de donnée relationnelle **0, 5**

    > * relation : tableau contenant des données relatives à un sujet
    > * attribut : titre des colonnes du tableau concernant une caractéristique du sujet.
    > * domaine : ensemble dans laquel un attribut prend ses valeurs.
    > * enregistrement : une ligne du tableau
    > * valeur : une case du tableau cad une valeur d'un attribut.

2. Expliquer ce qu'est une clé primaire et donner celles des trois relations de  `Theatre`. **0,25**

    > * clé primaire : attribut ou ensemble d'attributs qui identifi(ent) un unique enregistrement.
    > * piece a pour clé primaire id_piece
    > * acteur a pour clé primaire id_acteur
    > * role a pour clé primaire id_role

3. Expliquer ce qu'est une clé étrangère et donner son utilité. **0,5**

    > Une clé étrangère est un attribut ou un ensemble d'attributs qui prend ses valeurs parmi celles d'une clé primaire d'une autre relation. On dit qu'elle référence une clé primaire. Son rôle est de faire le lien entre deux relation.

4. Un nouvel acteur non référencé dans la base est embauché pour jouer un rôle non référencé dans la base dans une nouvelle pièce non référencée dans la base. Expliquer, sans donner les requêtes SQL, dans quel ordre les modifications de `Theatre` doivent être faites pour y inclure ces nouvelles données.**0,5**

>    * On peut indiféremment ajouter la pièce ou l'acteur dans n'importe quel ordre car les deux relations de sont pas liées par une clé étrangère.
>    * Ensuite seulement, on ajoute le rôle dans la relation role. L'ordre est imposé par l'existence de la clé étrangère de la relation role qui référence la clé primaire id_acteur et la clé étrangère id_piece qui référence la clé primaire de la relation piece.

​    

### Partie B **2,25**pts

5. Dans la pièce `Tartuffe`, l'acteur `Micha Lescot` a joué le rôle de `Tartuffe`. On attribut à l'acteur l'identifiant `389761` et à la pièce l'identifiant `46721`. 

    Écrire la requête SQL permettant d'ajouter ce nouveau rôle à la relation `role` , l'acteur et la pièce étant déjà présents dans la base `Theatre`. **0,25**

    ```sql
    insert into role
    (id_piece, id_acteur, nom_role)
    values
    (46721, 389761, 'Tartuffe');
    ```

    

6. Expliquer ce que fait la requête ci-dessous : **0,25**

    ```sql
    UPDATE piece
    SET langue = "Anglais"
    WHERE langue = "Américaine" OR langue = "Britannique";
    ```

    >  Elle met à jour la reltion piece en remplaçant les langues 'Américaine' et 'Britannique' par 'Anglais'.

7. Pour chacune des questions ci-dessous, écrire une requête SQL permettant d'extraire les données demandées :

    a. Le titre des pièces écrites en anglais ou en allemand. **0,25**

    ```SQL
    Select titre from piece
    where langue = 'Anglais' or langue = 'Allemand';
    ```
    
    b. Les noms rangés par ordre alphabétique et prenoms des acteurs.**0,25**
    
    ```sql
    select nom, prenom from acteur
    order by nom;
    ```
    
    c. Le nombre de pièces dans la base.**0,25**
    
    ```sql
    select count(id_piece) from piece;
    ```
    
    d. Les noms des rôles qu'a tenu l'acteur `Richard Auteuil`. **0,5**
    
    ```sql
    select nom_role from role 
    join acteur
    on acteur.id_acteur = role.id_acteur
    where acteur.nom = 'Auteuil' and acteur.prenom = 'Richard';
    ```
    
    e. Le nom des rôles qui sont joués en anglais.**0,5**
    
    ```sql
    select nom_role from role
    join piece
    on piece.id_piece = role.id_piece
    where piece.langue = 'Anglais';
    ```
    
    

## Exercice 2

_Cet exercice traite des processus et du routage dans un réseau informatique._

Les parties A et B sont indépendantes.

### Partie A : processus 2,5pts

La commande UNIX `ps -eo pid,ppid,stat` affiche, pour chaque fichier du répertoire en cours, son PID, son  PPID, son statut (R : en cours d'exécution, S : stoppé) et son URL.

1. Définir le PID et le PPID ? **0,25**

    > * PID : no d'identification du processus
    > * PPID : no d'identification du parent du processus.

2. Donner et expliquer les différents statuts (ou états) par lesquels passe un processus (on pourra faire un schéma). **0,5**

    ![](media/ordonnancement.jpg)

3. Expliquer ce qu'est un interblocage (on pourra faire un schéma). **0,5**

    > ​	Un interblocage intervient lorsque deux processus prêts ont chacun besoin d'une ressource que l'autre processus utilise :
    >
    > * Le premier processus utilise la ressource A et a besoin du B : il est bloqué
    > * Le second processus utilise la ressource B et a besoin de A : il est bloqué
    >
    > Pour débloqué la situation, il faut tuer un des deux processus.

4. La commande `ps -eo pid,ppid,stat` renvoie le résultat suivant :

    ```bash
    PID    PPID    STAT    COMMAND
    1              Ss      /sbin/init
    ...	   ....    ..      ...
    1912   1908    S       Bash
    1920   1912    S       Thonny
    2091   1908    R       /usr/lib/firefox/firefox
    5437   1920    R       python programm prog1.py
    5440   1908    S       python programm prog2.py
    5450   1912    R       ps -eo pid,ppid,stat
    ```

    a. Donner le nom de la première application exécutée par le système d'exploitation au démarrage.**0,25**

    > _init_ a pour PID 1

    b. Donner l'application depuis laquelle a été lancé `prog1.py`.**0,25**

    > c'est Thonny 

    c. Donner l'ordre dans lequel `prog1.py` et `prog2.py` ont été lancés.**0,25**
    
    > Dans l'ordre de leur PID : prog1 puis prog2
    
    d. Donner et expliquer les conséquences de la commande `kill -kill 1912` sur chacune des applications listées plus-haut.**0,5**
    
    > Elle tue le processus de PID 1912 et ses processus fils :
    >
    > * Le Bash (PID 1912)
    > * son enfant Thonny et l'enfant de Thonny _prog1.py_ 
    > * la commande `ps -eo pid,ppid,stat` qui est déjà terminée à l'issu de l'affichage des processus.
    
    

### Partie B : routage 1,5pts

On considère le réseau modélisé par le schéma ci-dessous où les routeurs sont identifiés par des lettres de A à F et les débits sont indiqués sur les liaisons.    modifie la pile de façon à ce qu'aucun élément n'y soit en doubl

![reseau](media/reseauABCDEF.jpg)

5. a. Dans un réseau informatique, expliquer comment chaque routeur construit sa table de routage. **0,25**

    > * Il connait ses voisins directs qui eux même connaissent leurs voisins directs.
    > * chaque routeur envoie à ses voisins sa table toutes les 30s.
    > * Dès qu'un routeur reçoit une destination inconnue il l'ajoute à sa table
    > * Dès qu'un routeur reçoit une destination connue et de coût inférieure, il efface l'ancienne destination et ajoute la nouvelle à sa place.

    b. Expliquer comment les routeurs modifient leur table de routage lorsque l'un d'eux tombe en panne puis lorsque, une fois réparé, il fonctionne à nouveau correctement.**0,25**

    > * Lorsqu'un routeur tombe en panne, i lcesse d'envoyer sa table à ses voisins. Ces derniers, au bout d'un certains temps, l'efface de leur table. Cette dernière ce remet alors à jour en appliquant le principe précédent. 
    > * lorsqu'il est réparé, il se remet à emettre et met à jour les tables des autres routeurs.

6. Dans cette question, on utilise le protocole RIP. Le tableau ci-dessous montre une ligne de la table de routage des routeurs du réseau qui leur permet d'atteindre le routeur F. Recopier et compléter ce tableau :**0,5**


| Routeur | Passerelle | Métrique |
| :-----: | :--------: | :------: |
|    F    |     F      |    0     |
|    A    |     D      |    3     |
|    B    |     C      |    3     |
|    C    |     E      |    2     |
|    D    |     E      |    2     |
|    E    |     F      |    1     |



7. Dans cette question, on utilise le protocole OSPF. Le coût d'une liaison est calculé par la formule $coût = \frac{10^8}{débit}$ où le débit s'exprime en bit/s. 

    Le tableau ci-dessous montre une ligne de la table de routage des routeurs du réseau qui leur permet d'atteindre le routeur F. Recopier et compléter ce tableau :**0,5**

    | Routeur | Passerelle | Coût |
    | :-----: | :--------: | :--: |
    |    F    |     F      |  0   |
    |    A    |     B      |  4   |
    |    B    |     C      |  3   |
    |    C    |     E      |  2   |
    |    D    |     E      |  11  |
    |    E    |     F      |  1   |



## Exercice 3

_Dans cet exercice, il est question de piles et de programmation orientée objet_.

On dispose d'une classe `Pile` dont les seules méthodes sont  `est_vide()`, `empiler(element)` et `depiler()`. 

1. Expliquer pourquoi on appelle une telle structure, structure LIFO. **0,25**

    > Le dernier arrivé et le premier à sortFKir : Last In First Out

2. Dans la console, on tape les instructions ci-dessous :

    ```python
    >>> p = Pile()
    >>> for caractere in "Hello" :
        	p.empiler(caractere)
    >>> p.depiler()
    ???
    ```

    a. Donner le résultat renvoyé par la dernière instruction.**0,25**

    > 'o', le dernier empilé

    b. Écrire, à la suite des instructions précédentes, une boucle permettant d'obtenir les uns après les autres les éléments présents dans la pile du premier au dernier sans déclencher une exception.**0,5**

    ```python
    while not p.est_vide() :
        p.depiler()
    ```

3. On souhaite ajouter une méthode `nb_elements` à la classe `Pile` de façon à ce qu'elle renvoie le nombre d'élements présents dans la Pile. Écrire le code des zones _à compléter_ en précisant leur no (1, 2 ou 3) dans la **méthode** ci-dessous :**1**

    ```python
    	def nb_elements(self) :
            '''
            renvoie le nombre d'élements dans la pile sans la modifier.
            : return (int)
            : pas d'effet de bord sur l'objet !
            '''
            pile_intermediaire = Pile()
            nb_elts = 0
            while not self.est_vide() :
                pile_intermediaire.empiler(self.depiler)
                nb_elts +=1
            while not pile_intermediare.est_vide() :
                self.empiler(pile_inbtermediaire.depiler())
            return nb_elts
    ```

2.   Écrire le code des zones _à compléter_ en précisant leur no (1, 2 ou 3) dans la **fonction** ci-dessous :**1**

    ```python
    def copier(pile_depart, pile_destination) :
        '''
        Empile les élements de pile_depart sur la pile_destination
        dans leur même ordre que leur ordre dans pile_depart. 
        : params 
        	pile_depart(Pile)
        	pile_destination(Pile)
        : pas de return
        : effet de bord sur pile_destination
        : pas d'effet de bord sur pile_depart qui ne doit pas être modifiée
        '''
        assert isintance(pile_depart, Pile), 'on attend une pile'
        assert isintance(pile_destination, Pile), 'on attend une pile'
        pile_intermediaire = Pile()
        while not pile_depart.est_vide() :
            pile_intermediaire.empiler(pile_depart.depiler())
        while not pile_intermediaire.est_vide() :
            elt = pile_intermediaire.depiler()
            pile_depart.empiler(elt)
            pile_destination.empiler(elt)    
    ```


5.   Écrire le code des zones _à compléter_ en précisant leur no (1 ou 2) dans la fonction ci-dessous :**1**

    ```python
    def retirer_doublon(pile):
        '''
        modifie la pile de façon à ce qu'aucun élément n'y soit en double sans changer l'ordre des éléments de la pile.
        : param pile (Pile)
        : pas de return
        : effet de bord sur pile
        '''
        assert isinstance(pile, Pile), 'on attend une pile'
        # on utilise le tableau ci-dessous pour stocker les éléments de la pile, s'ils n'y sont pas déjà :
        tab_elts = []    
        while not pile.est_vide() :
            elt = pile.depiler()
            if not elt in tab_elts :
                tab_elts.append(elt)
        tab_elts.reverse()
        for elt in tab_elts :
            pile.empiler(elt)
    ```

## Exercice 4 

_Cet exercice traite de la structure d'arbre, de poo et de récursivité_.

Les organisateur d'un tournois utilisent un arbre binaire. Les compétiteurs sont divisés en quatre poules de quatre compétiteurs. 

Voici un arbre  représentant la poule no1 du tournois :



![](media/arbre_competition.jpg)

* Chaque feuille est un compétiteur.
* le symbole $\empty$ désigne un arbre vide.
* chaque noeud interne a :
    * un sous-arbre droit dont l'étiquette est un compétiteur. 
    * un sous-arbre gauche dont l'étiquette est un compétiteur.
    * une racine dont l'étiquette est le vainqueur du match entre les deux compétiteurs précédents.

On dispose d'une classe `Arbre` .

Son seul attribut public est `etiquette` , l'étiquette de la racine de l'arbre.

Ses méthodes sont : 

* `est_vide(self)` : renvoie `True` si l'arbre est vide et `False` sinon.
* `renvoyer_sad(self)` :  renvoie le sous-arbre droit de l'instance `self`.
* `renvoyer_sag(self)` :  renvoie le sous-arbre gauche de l'instance `self`.

1. On veut construire l'arbre de la poule no1. Pour cela on écrit les instructions suivantes  :

    ```python
    >>> f1 = Arbre("Joris")
    >>> f2 = Arbre("Kamel")
    >>> a1 = Arbre("Kamel", f1, f2)
    ```

    Donner la suite des instructions nécessaires à la construction de l'arbre entier qu'on appelera `poule_1`.**0,75**

    ```python
    >>> f3 = Arbre("Carine")
    >>> f4 = Arbre("Abdou")
    >>> a2 = Arbre("Carine", f3, f4)
    >>> poule_1 = Arbre("Kamel", a1, a2)               
    ```

2. Pour chaque parcours de l'arbre `poule_1`, donner les étiquettes obtenues dans l'ordre.**4 fois 0,25 = 1**

    a. parcours préfixe 

    > "Kamel", "Kamel", "Joris", "Kamel", "Carine","Carine", "Abdou"

    b. parcours infixe.

    > "Joris", "Kamel", "Kamel", "Kamel", "Carine", "Carine", "Abdou"

    c. parcours suffixe (ou postfixe)

    > "Joris", "Kamel", "Kamel", "Carine", "Abdou", "Carine", "Kamel"

    d. parcours en largeur.

    > "Kamel", "Kamel", "Carine", "Joris", "Kamel", "Carine", "Abdou"

3. On souhaite implémenter le parcours suffixe via la méthode suivante de la classe `Arbre` :**1**

    ```python
        def suffixe(self) :
            '''
            renvoie un tableau des étiquettes obtenues lors du parcours suffixe de        l'arbre.
            : return (list)
            '''
            if self.est_vide() :
            	return []
            else :
            	return (  self.renvoyer_sag().suffixe()
                        + self.renvoyer_sad().suffixe()
                        +  [self.etiquette]
                       )        
    ```
    
      Écrire le code des zones _à compléter_ ci-dessus en précisant leur no (1, 2, 3, 4 ou 5 ).
    
4. On souhaite coder une fonction qui renvoie un dictionnaire dont les clés sont les compétiteurs et les valeurs le nombre de matchs qu'ils ont remporté dans un arbre. Pour cela, on dispose d'un tableau d'étiquettes obtenu en utilisant l'un des parcours possibles, qu'importe lequel.

    a. Donner une relation entre le nombre d'occurence(s) d'un compétiteur dans le tableau et son nombre de victoire(s).

    >  Le nombre de victoire(s) est égal au nbre d'occurence(s) - 1**0,25**

    b.  Écrire le code de la zone _à compléter_ de la fonction ci-dessous.**1**
    
    ```python
    def renvoyer_dic_victoires(tab) :
        '''
        renvoie un dictionnaire dont les clés sont les compétiteurs du tableau tab est les valeurs leur nombre de victoire(s).
        : param tab (list) une liste d'étiquettes d'un arbre
        : return (dict)
        >>> renvoyer_dic_victoires("Kamel', "Kamel", "Joris" , "Kamel", "Carine", "Carine", "Abdou")
        {"Kamel" : 2, "Carine" : 1, "Joris" : 0, "Abdou" : 0}
        '''
        assert isinstance(tab,list), 'on attend un tableau'
        dic = {}
        for competiteur in tab :
            if competiteur in dic :
                dic[competiteur] += 1
            else :
            	dic[competiteur] = -1
        return dic
    ```

## Exercice 5

_Cet exercice implémente et utilise une file de différentes façons via différents paradigmes de programmation_.

### Partie A 0,25pts

1. Pourquoi appelle-t-on la structure de File structure FIFO ?**0,25**

   >  Premier entré premier sortie : First In Firse Out

### Partie B 2pts

On va utiliser une structure de tableau et la programmation fonctionnelle dans cette  partie.

La fonction ci-dessous renvoie une file vide :

```python
def creer_file():
    '''
    renvoie une file vide
    : return (list)
    '''
    return []
```

On construit une file avec l'instruction suivante :

```python
>>> file = creer_file()
```

2.  Écrire le code des zones _à compléter_ , en précisant leur no (de 1 à 7), pour chacune des fonctions ci-dessous : 

    **0,5**

```python
def enfiler(file, element) :
    '''
    enfile l'élément en tête de file
    : params
    	file (list)
    	element (?) type variable selon ce qu'on veut mettre dans la file
    : pas de return
    : effet de bord sur la file
    >>> file = creer_file()
    >>> enfiler(file, 3)
    >>> enfiler(file, 2)
    >>> file
    [3, 2]
    '''
    assert isinstance(file, list) , 'on attend un tableau'
    file.append(element)
```
​	**0,5**

```python
def est_vide(file):
    '''
    renvoie True si la file est vide et False sinon
    : param file(list)
    : return (boolean)
    >>> f = creer_file()
    >>> est_vide(f)
    True
    >>> enfiler(f, 2)
    >>> est_vide(f)
    False
    '''
    return len(file) == 0  
```

**0,5**

```python
def defiler(file) :
    '''
    renvoie, si la file n'est pas vide, l'élément en tête de file et le supprime de la file. Déclenche une exception sinon.
    : param file (list)
    : return (?) type variable selon ce qu'on a mis dans la file
    : effet de bord sur file
    >>> file = creer_file()
    >>> enfiler(file, 3)
    >>> enfiler(file, 2)
    >>> defiler(file)
    3
    >>> file
    [2]
    '''
    assert isinstance(file, list), 'on attend un tableau'
    assert not est_vide(file), 'on de peut defiler une file vide'
    elt = file[0]
    file = file[1:]
    return elt
    
```



3. En utilisant cette implémentation, écrire une fonction documentée et testée de paramètre _n_, un entier positif, qui renvoie une file contenant les entiers de 0 à _n_ inclus rangés dans l'ordre (0 en tête et _n_ en queue).**0,5**

    ```python
    def enfiler_entier(n):
        '''
        renvoie une file contenant les entiers de 0 (en tête) à n (en queue)
        : param n (int) 
        : return (list)
        >>> enfiler_entier(3)
        [0, 1, 2, 3]
        '''
        file = creer_file()
        for i in range(n + 1):
            enfiler(file, n)
        return file
    ```

    

### Partie C **1,75pt**

On utilise ici le paradigme objet pour implémenter la structure de file.

Voici le début de l'implémentation :

```python
class Maillon() :
    def __init__(self, valeur = None, suivant = None):
        '''
        initialise un Maillon de valeur et de maillon suivant précisés en paramètre
        '''
        self.valeur = valeur
        self.suivant = suivant
        
        

        
        
        
class File() :
    def __init__(self) :
        '''
        initialise une file vide
        '''
        self.tete = None
        self.queue = None
```

4. Une file est vide si son attribut `tete` est égal à `None`. Écrire la méthode documentée et testée `est_vide(self)` . **0,5**

    ```python
    	def est_vide(self):
            '''
            renvoie True si la file est vide et False sinon
            : param file(File)
            : return (boolean)
            >>> f = File()
            >>> f.est_vide()
            True
            >>> f.enfiler(2)
            >>> est_vide(file)
            False
            '''
            return self.tete == None
    ```
    
    
    
5. Lorsqu'on défile une file non vide, la tête de la file devient le deuxième maillon et on renvoie la valeur de l'ancien maillon de tête. Si on défile une file vide, une exception est déclenchée. Écrire la méthode documentée et testée `defiler(self)`. **0,75**

    ```python
    	def defiler(self):
            '''
            renvoie, si la file n'est pas vide, l'élément en tête de file et le supprime de la file. Déclenche une exception sinon.
            : param file (File)
            : return (?) type variable selon ce qu'on a mis dans la file
            : effet de bord sur file
            >>> file = File()
            >>> file.enfiler(3)
            >>> file.enfiler(2)
            >>> file.defiler()
            3
            >>> file.defiler()
            2
            '''
            assert not self.est_vide(), 'on ne défile pas une file vide'
            if not self.est_vide() :
            	elt = self.tete.valeur
            	self.tete = self.tete.suivant
            	return elt
            
                
    ```
    
    

​	

4. En utilisant la classe `File`, écrire une fonction documentée et testée de paramètre _f_, une file, qui renvoie une liste de tous les éléments de la file. **0,5**

    ```python
    def lister_elt(file) :
        '''
        renvoie un tableau de tous les éléments de la file. 
        : param file (File)
        : return (list)
        >>> file = File()
        >>> file.enfiler(2)
        >>> file.enfiler(1)
        >>> file.enfiler(3)
        >>> lister_elt(file)
        [2, 1, 3]
        '''
        assert isinstance(file, File), "on attend une File"
        tab = []
        while not file.est_vide() :
            elt = file.defiler()
            tab.append(elt)
        for elt in tab :
            file.enfiler(elt)
        return tab
    ```

    

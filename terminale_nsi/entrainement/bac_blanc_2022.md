# Bac blanc  NSI - v 1.0

Vous choisirez trois exercices complets parmi les cinq proposés et taiterez chacun d'eux en entier.

Chaque exercice est noté sur quatre points.

Entamer un exercice supplémentaire ne rapporte pas de point.

La calculatrice n'est pas autorisée.

## Exercice 1

_Dans cet exercice, on parle des bases de données relationnelles._

On pourra utiliser les mots clés suivants : 

`SELECT, FROM, WHERE, INSERT, INTO, VALUES, COUNT, OR, AND, ORDER BY, JOIN, ON`.

Des acteurs ayant joué dans des pièces de théâtre sont recensés dans une base de données nommée `Theatre` dont le schéma relationnnel est :

* piece (id_piece (INT), titre (TEXT), langue (TEXT), PK (id_piece))
* acteur(id_acteur (INT), nom (TEXT) , prenom (TEXT), annee_naiss (DATE), PK(id_acteur))
* role(id_piece (INT), id_acteur (INT), nom_role (TEXT), PK(id_piece), FK(id_acteur) REF(acteur(id_acteur)), FK(id_piece) REF(piece(id_piece)))

### Partie A

1. Expliquer ce que signifient les mots `relation`, `enregistrement`, `attribut`, `domaine` et `valeur` pour une base de donnée relationnelle.
2. Expliquer ce qu'est une clé primaire et donner celles des trois relations de  `Theatre`.
3. Expliquer ce qu'est une clé étrangère et donner son utilité.
4. Un nouvel acteur non référencé dans la base est embauché pour jouer un rôle non référencé dans la base dans une nouvelle pièce non référencée dans la base. Expliquer, sans donner les requêtes SQL, dans quel ordre les modifications de `Theatre` doivent être faites pour y inclure ces nouvelles données.

### Partie B

5. Dans la pièce `Tartuffe`, l'acteur `Micha Lescot` a joué le rôle de `Tartuffe`. On attribut à l'acteur l'identifiant `389761` et à la pièce l'identifiant `46721`. 

    Écrire la requête SQL permettant d'ajouter ce nouveau rôle à la relation `role` , l'acteur et la pièce étant déjà présents dans la base `Theatre`.

6. Expliquer ce que fait la requête ci-dessous :

    ```sql
    UPDATE piece
    SET langue = "Anglais"
    WHERE langue = "Américaine" OR langue = "Britannique"
    ```

7. Pour chacune des questions ci-dessous, écrire une requête SQL permettant d'extraire les données demandées :

    a. Le titre des pièces écrites en anglais ou en allemand.

    b. Les noms rangés par ordre alphabétique et prenoms des acteurs.

    c. Le nombre de pièces dans la base.

    d. Les noms des rôles qu'a tenu l'acteur `Richard Auteuil`

    e. Le nom des rôles qui sont joués en anglais.

    
    
    

## Exercice 2

_Cet exercice traite des processus et du routage dans un réseau informatique._

Les parties A et B sont indépendantes.

### Partie A : processus

La commande UNIX `ps -eo pid,ppid,stat` affiche, pour chaque fichier du répertoire en cours, son PID, son  PPID, son statut (R : en cours d'exécution, S : stoppé) et son URL.

1. Définir le PID et le PPID ?

2. Donner et expliquer les différents statuts (ou états) par lesquels passe un processus (on pourra faire un schéma).

3. Expliquer ce qu'est un interblocage (on pourra faire un schéma).

4. La commande `ps -eo pid,ppid,stat` renvoie le résultat suivant :

    ```bash
    PID    PPID    STAT    COMMAND
    1      0       Ss      /sbin/init
    ...	   ....    ..      ...
    1912   1908    S       Bash
    1920   1912    S       Thonny
    2091   1908    R       /usr/lib/firefox/firefox
    5437   1920    R       python programm prog1.py
    5440   1908    S       python programm prog2.py
    5450   1912    R       ps -eo pid,ppid,stat
    ```

    a. Donner le nom de la première application exécutée par le système d'exploitation au démarrage.

    b. Donner l'application depuis laquelle a été lancé `prog1.py`.

    c. Donner l'ordre dans lequel `prog1.py` et `prog2.py` ont été lancés.

    d. Donner et expliquer les conséquences de la commande `kill -kill 1912` sur chacune des applications listées plus-haut.

    

### Partie B : routage

On considère le réseau modélisé par le schéma ci-dessous où les routeurs sont identifiés par des lettres de A à F et les débits sont indiqués sur les liaisons.

![reseau](media/reseauABCDEF.jpg)

5. a. Dans un réseau informatique, expliquer comment chaque routeur construit sa table de routage.

    b. Expliquer comment les routeurs modifient leur table de routage lorsque l'un d'eux tombe en panne puis lorsque, une fois réparé, il fonctionne à nouveau correctement.

6. Dans cette question, on utilise le protocole RIP. Le tableau ci-dessous montre une ligne de la table de routage des routeurs du réseau qui leur permet d'atteindre le routeur F. Recopier et compléter ce tableau :


| Routeur | Passerelle | Métrique |
| :-----: | :--------: | :------: |
|    F    |     F      |    0     |
|    A    |            |          |
|    B    |            |          |
|    C    |            |          |
|    D    |            |          |
|    E    |            |          |



7. Dans cette question, on utilise le protocole OSPF. Le coût d'une liaison est calculé par la formule $coût = \frac{10^8}{débit}$ où le débit s'exprime en bit/s. 

    Le tableau ci-dessous montre une ligne de la table de routage des routeurs du réseau qui leur permet d'atteindre le routeur F. Recopier et compléter ce tableau :

    | Routeur | Passerelle | Coût |
    | :-----: | :--------: | :--: |
    |    F    |     F      |  0   |
    |    A    |            |      |
    |    B    |            |      |
    |    C    |            |      |
    |    D    |            |      |
    |    E    |            |      |



## Exercice 3

_Dans cet exercice, il est question de piles et de programmation orientée objet_.

On dispose d'une classe `Pile` dont les seules méthodes sont  `est_vide()`, `empiler(element)` et `depiler()`. 

1. Expliquer pourquoi on appelle une telle structure, structure LIFO.

1. Dans la console, on tape les instructions ci-dessous :

    ```python
    >>> p = Pile()
    >>> for caractere in "Hello" :
        	p.empiler(caractere)
    >>> p.depiler()
    ???
    ```

    a. Donner le résultat renvoyé par la dernière instruction.

    b. Écrire, à la suite des instructions précédentes, une boucle permettant d'obtenir les uns après les autres les éléments présents dans la pile du premier au dernier sans déclencher une exception.

3. On souhaite ajouter une méthode `nb_elements` à la classe `Pile` de façon à ce qu'elle renvoie le nombre d'élements présents dans la Pile. Écrire le code des zones _à compléter_ en précisant leur no (1, 2 ou 3) dans la **méthode** ci-dessous :

    ```python
    	def nb_elements(#à compléter 1) :
            '''
            renvoie le nombre d'élements dans la pile sans la modifier.
            : return (#à compléter 2)
            : pas d'effet de bord sur l'objet !
            '''
            pile_intermediaire = Pile()
            nb_elts = 0
            #à compléter 3
    ```

2.   Écrire le code des zones _à compléter_ en précisant leur no (1, 2 ou 3) dans la **fonction** ci-dessous :

    ```python
    def copier(pile_depart, pile_destination) :
        '''
        Empile les élements de pile_depart sur la pile_destination
        dans leur même ordre que leur ordre dans pile_depart. 
        : params 
        	pile_depart(Pile)
        	pile_destination(Pile)
        : pas de return
        : effet de bord sur pile_destination
        : pas d'effet de bord sur pile_depart qui ne doit pas être modifiée
        '''
        assert #à compléter 1
        assert #à compléter 2
        pile_intermediaire = Pile()
        # à compléter 3
        
    ```


5.   Écrire le code des zones _à compléter_ en précisant leur no (1 ou 2) dans la fonction ci-dessous :

    ```python
    def retirer_doublon(pile):
        '''
        modifie la pile de façon à ce qu'aucun élément n'y soit en double sans changer l'ordre des éléments de la pile.
        : param pile (Pile)
        : pas de return
        : effet de bord sur pile
        '''
        assert #à compléter 1
        # on utilise le tableau ci-dessous pour stocker les éléments de la pile, s'ils n'y sont pas déjà :
        tab_elts = []    
        #à compléter 2
    ```

## Exercice 4 

_Cet exercice traite de la structure d'arbre, de poo et de récursivité_.

Les organisateur d'un tournois utilisent un arbre binaire. Les compétiteurs sont divisés en quatre poules de quatre compétiteurs. 

Voici un arbre  représentant la poule no1 du tournois :



![](media/arbre_competition.jpg)

* Chaque feuille est un compétiteur.
* le symbole $\empty$ désigne un arbre vide.
* chaque noeud interne a :
    * un sous-arbre droit dont l'étiquette est un compétiteur. 
    * un sous-arbre gauche dont l'étiquette est un compétiteur.
    * une racine dont l'étiquette est le vainqueur du match entre les deux compétiteurs précédents.

On dispose d'une classe `Arbre` .

Son seul attribut public est `etiquette` , l'étiquette de la racine de l'arbre.

Ses méthodes sont : 

* `est_vide(self)` : renvoie `True` si l'arbre est vide et `False` sinon.
* `renvoyer_sad(self)` :  renvoie le sous-arbre droit de l'instance `self`.
* `renvoyer_sag(self)` :  renvoie le sous-arbre gauche de l'instance `self`.





1. On veut construire l'arbre de la poule no1. Pour cela on écrit les instructions suivantes  :

    ```python
    >>> f1 = Arbre("Joris")
    >>> f2 = Arbre("Kamel")
    >>> a1 = Arbre("Kamel", f1, f2)
    ```

    Donner la suite des instructions nécessaires à la construction de l'arbre entier qu'on appelera `poule_1`.

2. Pour chaque parcours de l'arbre `poule_1`, donner les étiquettes obtenues dans l'ordre.

    a. parcours préfixe.

    b. parcours infixe.

    c. parcours suffixe (ou postfixe)

    d. parcours en largeur.

3. On souhaite implémenter le parcours suffixe via la méthode suivante de la classe `Arbre` :

    ```python
        def suffixe(#à compléter 1) :
            '''
            renvoie un tableau des étiquettes obtenues lors du parcours suffixe de        l'arbre.
            : return (list)
            '''
            if self.est_vide() :
            	return # à compléter 2
            else :
            	return (#à compléter 3 (éventuellement rien) 
                        + [self.etiquette]
                        #à compléter 4 (éventuellement rien) 
                       )        
    ```
    
      Écrire le code des zones _à compléter_ ci-dessus en précisant leur no (1, 2, 3 ou 4 ).
    
4. On souhaite coder une fonction qui renvoie un dictionnaire dont les clés sont les compétiteurs et les valeurs le nombre de matchs qu'ils ont remporté dans un arbre. Pour cela, on dispose d'un tableau d'étiquettes obtenu en utilisant l'un des parcours possibles, qu'importe lequel.

    a. Donner une relation entre le nombre d'occurence(s) d'un compétiteur dans une poule et son nombre de victoire(s) dans cette poule.

    b.  Écrire le code de la zone _à compléter_ de la fonction ci-dessous.

    ```python
    def renvoyer_dic_victoires(tab) :
        '''
        renvoie un dictionnaire dont les clés sont les compétiteurs du tableau tab est les valeurs leur nombre de victoire(s).
        : param tab (list) une liste d'étiquettes d'un arbre
        : return (dict)
        >>> renvoyer_dic_victoires("Kamel', "Kamel", "Joris" , "Kamel", "Carine", "Carine", "Abdou")
        {"Kamel" : 2, "Carine" : 1, "Joris" : 0, "Abdou" : 0}
        '''
        #à compléter    
    ```

## Exercice 5

_Cet exercice implémente et utilise une file de différentes façons via différents paradigmes de programmation_.

### Partie A 

1. Pourquoi appelle-t-on la structure de File structure FIFO ?

    

### Partie B

On va utiliser une structure de tableau et la programmation fonctionnelle dans cette  partie.

La fonction ci-dessous renvoie une file vide :

```python
def creer_file():
    '''
    renvoie une file vide
    : return (list)
    '''
    return []
```

On construit une file avec l'instruction suivante :

```python
>>> file = creer_file()
```

2.  Écrire le code des zones _à compléter_ , en précisant leur no (de 1 à 7), pour chacune des fonctions ci-dessous :

    

```python
def enfiler(file, element) :
    '''
    enfile l'élément en queue de file
    : params
    	file (list)
    	element (?) type variable selon ce qu'on veut mettre dans la file
    : pas de return
    : effet de bord sur la file
    >>> file = creer_file()
    >>> enfiler(file, 3)
    >>> enfiler(file, 2)
    >>> file
    [3, 2]
    '''
    assert #à compléter 1
    #à compléter 2
```
​	

```python
def est_vide(file):
    '''
    renvoie True si la file est vide et False sinon
    : param file(list)
    : return (#à compléter 3)
    >>> f = creer_file()
    >>> #à compléter 4
    True
    >>> #à compléter 5
    >>> est_vide(file)
    False
    '''
    #à compléter 6    
```


```python
def defiler(file) :
    '''
    renvoie, si la file n'est pas vide, l'élément en tête de file et le supprime de la file. Déclenche une exception sinon.
    : param file (list)
    : return (?) type variable selon ce qu'on a mis dans la file
    : effet de bord sur file
    >>> file = creer_file()
    >>> enfiler(file, 3)
    >>> enfiler(file, 2)
    >>> defiler(file)
    3
    >>> file
    [2]
    '''
    #à compléter 7
```



3.  En utilisant cette implémentation, écrire une fonction documentée et testée de paramètre _n_, un entier positif, qui renvoie une file contenant les entiers de 0 à _n_ inclus rangés dans l'ordre (0 en tête et _n_ en queue).

### Partie C

 On utilise ici le paradigme objet pour implémenter la structure de file.

Voici le début de l'implémentation :

```python
class Maillon() :
    def __init__(self, valeur = None, suivant = None):
        '''
        initialise un Maillon de valeur et de maillon suivant précisés en paramètre
        '''
        self.valeur = valeur
        self.suivant = suivant
        
        

        
        
        
class File() :
    def __init__(self) :
        '''
        initialise une file vide
        '''
        self.tete = None
        self.queue = None
```

4. Une file est vide si son attribut `tete` est égal à `None`. Écrire la méthode documentée et testée `est_vide(self)` .

5. On considère la méthode `enfiler` connue. Lorsqu'on défile une file non vide, la tête de la file devient le deuxième maillon et on renvoie la valeur de l'ancien maillon de tête. Si on défile une file vide, une exception est déclenchée. Écrire la méthode documentée et testée `defiler(self)`.

6. En utilisant la classe `File`, écrire une fonction documentée et testée de paramètre _f_, une file, qui renvoie un tableau de tous les éléments de la file sans effet de bord sur la file.







 

# Sécurisation : exercices

## Code César

Comme vu dans le cours, le [code César](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage) , codage utilisé par Jules César dans ses correspondances, utilise un simple décalage des lettres de l'alphabet tandis que les autres caractères (espaces, ponctuation ...) ne changent pas.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Caesar3.svg/800px-Caesar3.svg.png" alt="code César" style="zoom:50%;" />



 Les lettres de l'alphabet dont numérotées de 0(A) à 25(Z). 

| A    | B    | C    | D    | E    | F    | G    | H    | I    | J    | K    | L    | M    | N    | O    | P    | Q    | R    | S    | ...  |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| 0    | 1    | 2    | 3    | 4    | 5    | 6    | 7    | 8    | 9    | 10   | 11   | 12   | 13   | 14   | 15   | 16   | 17   | 18   | ...  |

1. Quel serait le chiffré de "BONJOUR" avec un décalage de 2 ?
2. Codez la fonction `generer_dic` qui renvoie un dictionnaire dont les clés sont les lettres majuscules de l'alphabet et les valeurs leur rang dans l'alphabet. Ainsi 'A' correspond à 0 et 'Z' à 26.
2. Codez la fonction `code_cesar(clair, decalage)` qui renvoie la chaîne de caractères correspond à la chaîne _clair_ avec le décalage entier entre 1 et 25 passé en paramètre. 

4. Quel est le lien entre le décalage utilisé pour coder et celui qui sera utilisé pour décoder ?

5. Chiffrez un clair de votre choix en utilisant la fonction précédente. Décodez le chiffré obtenu en utilisant cette même fonction.

6. Réalisez une fonction `decodage_cesar(chiffre)` qui renvoie tous les clairs possibles en testant tous les décalages. 

 

_Remarque :_

* La fonction `decodage` utilise la `force brute` à laquelle le codage César est très vulnérable. 
* Une autre attaque repose sur le fait que, dans la langue française, la lettre `E` est la plus utilisée. Il suffit alors de déterminer le symbole qui revient le plus souvent dans le chiffré, de considérer qu'il correspond à un `E`, de calculer le décalage correspondant puis de déchiffré en utilisant ce décalage. On appelle cette méthode, _méthode statistique_. Vous pouvez trouver de quoi la mettre en place en suivant [ce lien](https://fr.wikipedia.org/wiki/Fr%C3%A9quence_d%27apparition_des_lettres). Cependant, cette méthode ne fonctionne que sur des textes très très longs.





## Attaque à clair choisi et chiffre de Vigenère

Une attaque _à clair choisi_ consiste à faire décrypter un texte dont une partie du clair est connu. Cela parait irréalisable mis, en réalité, cela est faisable très facilement : C'est ainsi qu'a été craqué le célèbre code ENIGMA qui commençait par la météo et terminait par 'Heil Hitler'.

Si l'attaquant a connaissance d'une partie du clair et du chiffré et qu'il possède suffisamment de données, il peut tenter d'extraire la clé de chiffrement.



2. Réalisez la fonction `decodage_vigenere(chiffre, cle)` où `chiffre`  et `cle` sont des chaînes de caractères majuscules. Cette fonction renvoie le clair obtenu en décodant le chiffré grâce à la clé. Attention : on aligne la clé avec les lettres, pas avec les espaces et la ponctuation !



2. Voici un texte chiffré dont l'auteur est [Michel Quint](https://fr.wikipedia.org/wiki/Michel_Quint).

```txt
"JUJVQ YXIE TCR SE GXQFBE IHBBDREHZ, NDX VSWDDEJ RC WN PRVANRS VQKBAE JRCF MESRCG BOLV TRB TRETRB, AMDVG VEDH LR BAMRQE ZU ZOA RCAZHVG MEJWQAN A WDQEN RZUM, YNS TOWJWS D RVG METOMALHV OM PQAXUQA. MEJ GMFRRJ GM YJRDHA RC DV GMPQIIDVGB EJSWVAS, UH KHRSRQBRB DFXTRDRJ, HB QNS YRVGNS UH XNAIR. PQPQEC TCVWT."
```

Il y a toutes les  chances pour que les deux derniers mots soient sa signature... Essayons à partir de là 

de retrouver la clé :

|       clair        |  M   |  I   | C    | H    | E    |L|Q|U|I|N|T|
| :----------------: | :--: | :--: | ---- | ---- | ---- | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ |
|     chiffré     | P | Q | P | Q | E |C||||||
|    code clair    | 12 | 8 | 2 |      |      |      |      |      |      |||
| code chiffré | 15 | 16 | 15 |      |      |      |      |      |      |||
|      décalage      | 3 | 8 | 13 |      |      |||||||
| clé correspondante | D | I | N |      |      |      |      |      |      |||



4. Utilisez votre fonction pour décoder le texte complet.



## `XOR`

[RC4](https://fr.wikipedia.org/wiki/RC4) est un chiffrement dont le principe est assez simple : on réalise une opération simple (`Xor` par exemple) entre les données à coder est une _clés_ 256 octets. Ce chiffrement, très rapide, permet de chiffrer des flots de données et est utilisé, par exemple, pour le chiffrement [WPA](https://fr.wikipedia.org/wiki/Wi-Fi_Protected_Access) du `wi-fi`  ou les protocoles [TLS, aussi appelés SSL,](https://fr.wikipedia.org/wiki/Transport_Layer_Security) permettant de sécuriser la couche transport du modèle `TCP/IP` et qui, couplé au protocole `HTTP`, à engendré le `HTTPS`.

1. Choisissez une clé de 8 bits.

2. complétez le tableau ci-dessous :

    <table>
    	<TR>
    		<TD>
    			Mot :
    		</TD>
    		<TD  text-align = 'center' colspan = 8>
    			A
    		</TD>
    	</TR>
    	<TR>
    		<TD> 
    			code ASCII
    		</TD>
    		<TD>
    		0
    		</TD>
    		<TD>
    		1
    		</TD>
    		<TD>
    		0
    		</TD>
    		<TD>
    		0
    		</TD>
    		<TD>
    		0
    		</TD>
    		<TD>
    		0
    		</TD>
    		<TD>
    		0
    		</TD>
    		<TD>
    		1
    		</TD>
    	</TR>
        <TR>
    		<TD> 
    			votre clé
    		</TD>
            <TD>
      		</TD>
    		<TD>
      		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
      		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    	</TR>
    	<TR>
    		<TD> 
    			chiffré par XOR
    		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
      		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    	</TR>
</Table>

3. Pour décodé le chiffré , il faut connaître la clé et réaliser de nouveau un ou exclusif entre elle et le chiffré. Complétez :

    <table>
    	<TR>
    		<TD> 
    			code binaire du chiffré
    		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    	</TR>
    	<TR>
    		<TD> 
    			votre clé
    		</TD>
    	    <TD>
      		</TD>
    		<TD>
      		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
      		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    	</TR>
    	<TR>
    		<TD> 
    			déchiffré par XOR
    		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
       		</TD>
    		<TD>
      		</TD>
    		<TD>
    		</TD>
    		<TD>
    		</TD>
    	</TR>
        <TR>
    		<TD>
    			Mot :
    		</TD>
    		<TD  text-align = 'center' colspan = 8>    
    		</TD>
    	</TR>
    </Table>

4. Comment rendre ce chiffrement résistant aux attaques de force brute et aux attaques à clair choisi ?



## Générer une clé symétrique et crypter des données

Lancer linux et ouvrez le SHELL.

1. En utilisant les commande `mkdir` et `cd` créez un répertoire `crypto` et allez dedans.

2. En utilisant la commande `cat > clair` (qui dirige l'entrée standard, c'est à dire le clavier, vers le fichier _clair_ en sortie), créez le fichier _clair_ en y mettant le texte en clair.

3. Utilisez la commande `ls` pour vérifier la présence de votre fichier.

4. Editez le fichier _clair_ en utilisant la commande `cat clair` qui dirige le fichier _clair_ vers la sortie standard c'est à dire l'écran.

5. a. Pour crypter notre _clair_, on va utiliser la commande suivante : 

    `openssl enc -aes-256-cbc -pbkdf2  -in clair - out chiffre`:

    * `-aes-256-cbc -pbkdf2` est la méthode de codage utilisée, il y en a de nombreuses;

    * `-in` est suivi de la source à chiffrer;

        `-out` est suivi du nom du fichier codé à générer;

    * Cette commande vous demandera un mot de passe 'deux fois le même) : c'est la clé de chiffrement. Plus elle est longue, plus il sera compliqué de la deviner.

    b. Vérifier la présence du fichier _chiffre_ avec la commande `ls`.

    c. En utilisant la commande `cat` afficher le contenu du fichier _chiffre_.

    d. On peut également utiliser la commande `diff clair chiffre` pour afficher les différences entre les deux fichiers

6. a. Pour déchiffrer le message codé, on utilise la commande suivante :

    `openssl enc -d -aes-256-cbc -pbkdf2 -in chiffre - out dechiffre`:

    * `-d` demande est décryptage;

    * `-aes-256-cbc -pbkdf2` est la méthode de codage utilisée;

    * `-in` est suivi de la source à déchiffrer;

        `-out` est suivi du nom du fichier décrypter à générer;

    * Cette commande vous demandera un mot de passe : c'est la clé de chiffrement que bien entendu vous n'avez pas oubliée !

    b. Vérifier la présence du fichier _dechiffre_ avec la commande `ls`.

    c. En utilisant la commande `cat` afficher le contenu du fichier _dechiffre_.

    d. On peut également utiliser la commande `diff clair dechiffre` pour afficher les différences entre les deux fichiers



## Utiliser une clé asymétrique

Lancer linux et ouvrez le SHELL.

Un problème de la Virtual box est qu'il s'agit d'une boite fermée. Il n'y a pas possibilité d'envoyer quelque chose à un voisin. On va donc simuler deux personnes sur la même machine :

* Alice va générer une clé asymétrique, extraire la clé publique et donner cette clé publique à Bob.

* Bob va  : 

    * décider d'une une clé symétrique (un message secret);
    * écrire un message en clair et le chiffrer avec cette clé symétrique;
    * crypter cette clé symétrique avec la clé publique d'Alice;
    * envoyer à Alice son message chiffré et la clé symétrique cryptée avec la clé publique d'Alice.

* Alice va décoder la clé chiffrée de Bob et l'utilisée pour déchiffrer le message de Bob.

    

> **Avant de commencer quelques petits rappels : **
>
> * commande `mkdir nom_rep` permet de créer un répertoire nommé `nom_rep`;
>   	* pour _se promener_ dans l'arborescence du système de fichiers, on utilise la commande `cd` :    
>   	* `cd ..` remonte au répertoire parent;   
>   	* `cd rep` ouvre le répertoire `rep`;
> 	* l'auto-complétion se fait en appuyant sur la touche `tab` du clavier : pas besoin de taper le nom des répertoires en entier;
> 	* Si vous êtes perdu, la command `pwd` vous indiquera le répertoire courant et la commande `ls` en listera le contenu;
> 	*  la commande SHELL permettant de copier un fichier est `cp chemin/fichier1 chemin/fichier2`.

​     

1. Créez deux répertoires `crypto_bob` et `crypto_alice` . Nous agirons comme s'il s'agissait de deux machines différentes.

2. Dans `crypto_bob`, avec la commande `cat` :

    *  créez un fichier _cle_sym_ contenant un mot de passe servant à crypter des données comme dans l'exercice précédent;
    * créez un fichier _clair_ contenant le message à chiffrer;
    * Créez le fichier _chiffre_ en cryptant le fichier _clair_ avec la clé écrite dans _cle_sym_ : c'est ce message qu'Alice doit déchiffrer;
    * copiez le fichier _chiffre_ dans `crypto_alice`.

    

3. Dans `crypto_alice`, on va générer une clé asymétrique.

    Afin de générez une clé asymétrique, nommée ici `macleasym` , on utilise la commande suivante : `openssl genrsa -out macleasym 2048` . Cette commande utilise l'algorithme `rsa` et génère une clé de 2048 bits qui  contient la **clé publique ET la clé privée**. 

    a. Générez votre clé asymétrique dans le répertoire `crypto_alice`  et  visualisez son contenu.
    
    Pour extraire la clé publique, que l'on nommera  `alicepub` , de votre clé asymétrique qu'on a appelée `macleasym` , on utilise la commande suivante `openssl rsa -in macleasym -pubout -out alicepub`.

    b. Extrayez votre clé publique dans  dans le répertoire `crypto_alice` et visualisez son contenu.
c. Il faut maintenant pouvoir diffuser votre clé publique : copier `alicepub` dans le répertoire de Bob



4. Dans `crypto_bob`, pour chiffrer le fichier `cle_sym` avec la clé publique d'Alice, on utilise la commande suivante : `openssl pkeyutl -encrypt -in cle_sym -inkey nomclepub -pubin -out cle_sym_chiffre`.

    * **ATTENTION**, la dernière lettre de `pkeyutl` est un `L` minuscule.
    * `-in` précise l'entrée
    * `-inkey` la clé de cryptage
    * `-pubin` précise qu'il s'agit d'une clé publique.
    * `-out` précise la sortie.
    
    a. Chiffrez la clé `cle_sym`  de Bob avec la clé publique d'Alice. Vous nommerez le fichier chiffré `cle_sym_chiffre`
    b. Copiez `cle_sym_chiffre` dans le répertoire `crypto_alice` .
    
8. Dans `crypto_alice`, pour déchiffrer `cle_sym_chiffre`  de votre voisin grâce à votre clé privée `macleasym`, on utilise la commande suivante : `openssl pkeyutl -decrypt -in cle_sym_chiffre -inkey macleasym -out cle_sym_dechiffre` 

   a. Décryptez `cle_sym_chiffre` dans un fichier `cle_sym_dechiffre`.

   b. Visualisez le contenu de `cle_sym_dechiffre`.

   c. Déchiffrez maintenant le fichier _chiffre_ que Bob vous a envoyé au départ.

​				

##  RSA : un chiffrement asymétrique 



Le chiffrement [RSA](https://fr.wikipedia.org/wiki/Chiffrement_RSA) repose sur quatre concepts mathématiques :

* les nombres premiers : les nombres qui ont exactement deux diviseurs qui sont eux même et 1. La suite des nombres premiers a été démontrée comme étant infinie.
* les nombres premiers entre eux sont les nombres qui n'ont que 1 pour seul diviseur commun.
* la fonction _modulo_ (`%` en Python) : le reste de la division euclidienne.
* la décomposition binaire (que nous ne ferons pas ici).



### Génération des clés

Voici les étapes nécessaires à la création du couple de clé :

* On choisit deux nombres premiers _p_ et _q_ aussi grands que possibles. Ce couple va engendrer d'autres nombres qui vont constituer la clé. Pour expliquer ce procéder, on va choisir deux petits nombres premiers pour simplifier les calculs. En conséquence, la clé obtenue ne sera pas très solide.
    * Prenons donc _p_ = 11 et _q_ = 23.
    * Le nombre _n_ qui résulte de leur produit est une partie de la clé publique : ici $`n = p \times q = 11 \times 23 =`$ ...
    
    _Remarque :_
    
    _p_ et _q_ étant petits dans notre exemple, il est aisé de les retrouver à partir de 253... En réalité les nombres premiers choisis s'écrivent chacun avec plus de 100 chiffres et donnent donc un nombre _n_ comptant plus de deux cents chiffres dans son écriture. 



* A partir de _p_ et _q_, on génère un autre nombre grâce à la fonction indicatrice d'Euler $`\phi(n) = (p-1)(q-1)`$. Dans notre exemple, $` `\phi(253)=`$...

    

* On choisit maintenant arbitrairement un nombre _e_ entier, appelé **exposant de chiffrement**, qui doit être supérieur à 3 et premier avec $`\phi(n)`$.  Dans notre exemple, _e_ =17. convient. 

  _Remarques :_

  * _e_ = 17 n'est pas la seule valeur possible. Toute autre valeur répondant aux critères conviendrait tout autant.  Plus _n_ est grand, plus il existe de valeurs différentes possibles pour _e_. 
  * _e_, comme son nom l'indique, sera utilisé pour chiffrer les données
  
  > Donnez d'autres valeurs de _e_ possibles.
  >
  > ```txt
  > 
  > ```
  >
  > 
  
    
  
  

* **Le couple $`(\phi(n), e)`$ c'est à dire (253, 17 ) constituera notre clé publique**. N'importe qui, connaissant ce couple, est capable d'envoyer un message chiffré.

  

* Nous allons maintenant déterminer un **exposant de décryptage** que nous utiliserons pour déchiffrer les données. Cet exposant se déduit de _e_ et de $`\phi(n)`$ : _n_ étant secret, on ne peut le retrouver à partir de _e_ seul. 

    Il s'agit d'un nombre _d_ tel que $`(d \times e)~ mod(\phi(n)) = 1`$. Ça pique un peu... 

    Dans notre exemple, cela signifie : 

    * qu'on cherche un nombre _e_ tel que le reste de la division de $`d \times e$ = 17 d`$ par 220 doit être 1. 
    * Autrement dit, 17 _d_ doit être égal à un multiple de 220 auquel on ajoute 1. 
    * On recherche donc un entier positif _k_ tel que : 17 _d_ = 220 _k_ +1. Cette expression à deux conséquences :
        *  _d_ doit être très proche de $`\frac {220 k}{17} \approx 13k`$ : _d_ est donc très proche de 13 ou 26 ou 39 ou 42 ...
        * 220 _k_ +1 se termine forcément par un 1. Or pour que 17 _d_ se termine par un 1, il faut que _d_ se termine par un 3 ($`3 \times 7 = 21`$).
    * En recoupant les deux conséquences ci-dessous, on en déduit que _d_ = 13.

    

* **Le couple $`(\phi(n), d)`$ , c'est à dire (253, 13), constituera la clé privée.** 

    >  Imaginons que nous ayons choisi _e_ = 21.
    >
    > Retrouvez la valeur de _d_ correspondante.
    >
    > En quoi est-ce gênant ?



_Remarque :_

Avec des nombres très grands, trouver _d_ à partir de _e_ est beaucoup plus compliqué. Cela se fait en utilisant l'[algorithme d'Euclide étendu](https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide_%C3%A9tendu).

  



### Cryptage

Le cryptage permet de transformer un nombre $`x`$ strictement plus petit que _n_ en un autre nombre _y_, lui aussi strictement plus petit que _n_. Pour imposer la règle `strictement plus petit que n` l'algorithme utilise la fonction `modulo n` ou `mod n` ou `% n` en Python, c'est-à-dire le reste de la division entière par _n_.  

En effet, ce reste est forcément un nombre compris entre 0 et _n-1_.

A chaque nombre $`x`$ correspond $`(x^e) mod~ n`$

Dans le cas de notre exemple, nous pouvons coder 253 valeurs différentes soit pratiquement tous les codes ASCII. 

Codons, par exemple, la lettre A à coder avec la clé publique (253, 17).

* Son code ASCII est 65

* Il sera donc transformé en $`y=(65 ^ {17}) mod~253`$



Remarque :_

Il existe des outils mathématiques pour simplifier le calcul de $`65^{17}`$ en arithmétique. Mais Python, qui n'a pas peur des grands entiers, fait ça très bien !  



> * Utiliser Thonny pour déterminer le résultat de ce calcul.
> * Avec quel caractère sera codé A ?
> * De la même façon, déterminer le codage des lettres R et S avec la clé publique (253, 17).



### Décryptage

Pour décrypter, c'est à dire retrouver le nombre de départ $`x`$ à partir du nombre chiffré $`y`$ on utilise la relation suivante : $`x = (y ^ d) mod~n`$.

_Remarque :_

* Cette relation découle de la façon dont on a déterminer la valeur de _d_.

* Notez que la clé publique, qui ignore _d_ ne peut réaliser ce calcul et donc ne peut pas déchiffrer.



>Utilisez Thonny pour déchiffrer les nombres 21, 250 et 190 grâce à la clé privée (253, 13).







_______
Par Mieszczak Christophe

sources images :

* [Code César wikipédia domaine publique](https://commons.wikimedia.org/wiki/File:Caesar3.svg)

    


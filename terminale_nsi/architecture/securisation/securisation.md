Sécurisation des communications

## Introduction

### Cryptographie

Lorsqu'on communique et qu'on ne souhaite pas que le message soit entendu par tout le monde il  existe plusieurs stratégies :

- se réunir en un endroit secret entre personnes concernées par le message. Seules les personnes présentes entendront le message.

- ne pas se soucier d'être écouter mais faire en sorte que le message ne soit pas compréhensible pour les personnes auxquels il n'est pas destiné. Tout le monde peut entendre le message, mais seuls ses destinataires peuvent le comprendre.

    

La [cryptographie](https://fr.wikipedia.org/wiki/Cryptographie) est la science du **chiffrement**, opération qui permet à deux individus d'échanger des messages de manière confidentielle à travers un canal **non sécurisé** : on opte donc ici pour la seconde solution.

L'[histoire de la cryptographie](https://fr.wikipedia.org/wiki/Histoire_de_la_cryptologie) est très riche. Sa plus célèbre illustration est certainement celle de la machine [Enigma](https://fr.wikipedia.org/wiki/Enigma_%28machine%29), la célèbre machine de chiffrement/déchiffrement utilisée par les Allemands pendant la seconde guerre mondiale et mise en scène dans l'excellent film [Imitation Game](https://fr.wikipedia.org/wiki/Imitation_Game) que je vous conseille au passage de dévorer.

![Enigma](https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Enigma-IMG_0484-black.jpg/600px-Enigma-IMG_0484-black.jpg)





Nous allons donc _chiffrer_ les données à transmettre. On découvrira vite que le chiffrement n'est pas le seul problème à résoudre. Il nous faudra également vérifier :

* que les données reçues sont bien celles qui ont été envoyées : c'est un problèmes d'`intégrité de données`.

* que celui qui les a envoyées est bien celui qu'on croit : c'est un problème d'`authentification d'entité` et de `signature` de documents.

    


### Clair et chiffré

Deux individus souhaitent donc communiquer de façon à ce que leur communication soit comprise par eux uniquement.

Pour réaliser cet objectif, ils conviennent d'un `secret`, aussi appelé `clé`,  permettant de transformer le message non crypté, appelé `clair`, en un message inintelligible, appelé `chiffré`, afin que l'attaquant éventuel ne puisse en prendre connaissance. 

Cette transformation doit être `réversible` : si on connaît le secret et le `chiffré`, alors on peut reconstituer le `clair`.

  

### Code César 

Le [code César](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage) , codage utilisé par Jules César dans ses correspondances, utilise un simple décalage des lettres de l'alphabet. 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Caesar3.svg/800px-Caesar3.svg.png" alt="code César" style="zoom:50%;" />

Tant que le secret (le principe du décalage) n'est pas connu des _espions_ qui essaient de décrypter le chiffré, les messages sont incompréhensibles. Mais si ces derniers ont vent de la méthode employée pour le codage, il devient très facile de retrouver le message initial car les nombre de décalages est limité :



>  Combien y-en-a-t-il ?
>



Rendez-vous dans les [exercices](securisation _exercices.md) pour en savoir plus.

_Conséquence :_

> * Le faible nombre de possibilités rend ce codage très vulnérable aux attaques de force brute. 
> * En fait, dès que le principe de codage est connu, la clé ne permet plus de protéger le message codé.



### Code de Vigenère

Pour rendre le chiffrage moins vulnérable à ce type d'attaque, il en existe des évolutions, comme le chiffre de Vigenère qui utilise une clé. 

On écrit sous chaque lettre du clair la clé en la répétant suffisemment de fois pour couvrir tout le clair.

Comme pour le code César, chaque lettre correspond à son indice dans l'alphabet : on décale alors les lettres du clair de l'indice de la lettre de la clé qui lui correspond.

Par exemple, pour chiffrer 'BONJOUR' avec la clé 'HI' on écrirait :

| clair                        | B    | O    | N    | J    | O    | U    | R    |
| ---------------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| code lettre                  | 1    | 14   |      |      |      |      |      |
| clé                          | H    | I    | H    | I    | H    | I    | H    |
| code lettre                  | 7    | 8    | 7    | 8    | 7    | 8    | 7    |
| somme des codes              | 8    | 22   |      |      |      |      |      |
| modulo 26 (`% 26` en Python) | 8    | 22   |      |      |      |      |      |
| chiffré                      | I    | W    |      |      |      |      |      |



Pour déchiffrer, il suffit de faire l'opération inverse.... ce qui est impossible sans la clé.

Pour cela, ce chiffrement est bien meilleur que le code César : on a beau connaitre la technique de chiffrement, il est impossible de déchiffrer sans la clé !



Encore une fois, rendez-vous dans les [exercices](securisation _exercices.md) pour en savoir plus !



_Conséquence :_

> * Connaitre la méthode de chiffrement ne permet pas de déchiffrer le message : sans la clé c'es timpossible
> * Ce chiffrement est également sensible à un type d'attaque : l' `attaque à clair choisi`. Pour le contrer, une seule solution : prendre une clé suffisamment grande et sans signification pour rendre difficile cette attaque.





### Les bases de la cryptographie

Les deux exemples précédents permettent de poser les bases de la cryptographie :

* Un bon système de chiffrement ne doit et ne peut pas reposer uniquement sur le caractère secret de la méthode employée. Il faut partir du principe que l'attaquant connait parfaitement le principe de chiffrement : c'est le [Principe de Kerckhoffs](https://fr.wikipedia.org/wiki/Principe_de_Kerckhoffs) ;
* La clé utilisée doit être suffisamment complexe pour ne pas pouvoir être _devinée_ par un attaquant en un temps raisonnable.



Ainsi :

La méthode de chiffrement explique :

* comment à partir des `clés` et du `clair`, on produit le `chiffré`.
* comment, à partir du `chiffré` et des `clés`, on produit le `clair`.

​    


La sécurité d'un système de chiffrement doit reposer sur :

* La difficulté à calculer le `clair` à partir du `chiffré` sans la connaissance de la `clé`.

* La difficulté à retrouver la `clé`. 



## Chiffrement symétrique


### Principe

[Alice et Bob](https://fr.wikipedia.org/wiki/Alice_et_Bob) souhaitent communiquer des données confidentielles.

Dans les méthodes de [chiffrement symétrique](https://fr.wikipedia.org/wiki/Cryptographie_sym%C3%A9trique), la clé est **unique** : elle sert à la fois au **chiffrement et au déchiffrement**.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Chiffrement_sym%C3%A9trique.png/800px-Chiffrement_sym%C3%A9trique.png?20230212141726)    

* Si Alice veut envoyer un message chiffré à Bob, elle chiffre le message en utilisant une clé partagée entre eux deux
* Pour déchiffrer le message, Bob utilise la même clé.

### Exemples de chiffrements symétriques

Il existe de nombreuses façon de générer une clé de chiffrement symétrique complexe :

* Chiffrements par flot :
    * [RC4](https://fr.wikipedia.org/wiki/RC4) est un chiffrement dont le principe est assez simple : on réalise une opération simple (`Xor` par exemple) entre les données à coder est une _clés_ 256 octets. Ce chiffrement, très rapide, permet de chiffrer des flots de données et est utilisé, par exemple, pour le chiffrement [WPA](https://fr.wikipedia.org/wiki/Wi-Fi_Protected_Access) du `wi-fi`  ou les protocoles [TLS, aussi appelés SSL,](https://fr.wikipedia.org/wiki/Transport_Layer_Security) permettant de sécuriser la couche transport du modèle `TCP/IP` et qui, couplé au protocole `HTTP`, à engendré le `HTTPS`( nous en reparlerons plus loin).
* Chiffrements par bloc :
    * [Blowfish](https://fr.wikipedia.org/wiki/Blowfish) : un système relativement ancien (1993) mais toujours solide et efficace. Utilisé par exemple par [openSSH](https://fr.wikipedia.org/wiki/OpenSSH) qui permet de communications sécurisées sur un réseau informatique en utilisant le protocole [SSH](https://fr.wikipedia.org/wiki/Secure_Shell), un protocole qui ajoute un chiffrement à la couche TCP.
    * [AES](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard) : actuellement l'algorithme le plus utilisé et le plus sûr.



Aller hop, on retourne dans la partie  [exercices](securisation _exercices.md)  pour pratiquer un peu



### Ben oui mais il reste des _petits_ problèmes...

Deux problèmes en fait :

*  La clé doit d'abord être échangée entre les protagonistes, au  moyen d'un **canal sûr**.  Si ce dernier est compromis, la clé risque d'être transmise à un tiers ... qui pourra déchiffrer tous les messages ! Comment faire en sorte d'échanger cette clé sans risquer de la compromettre ?

* Si _n_ personnes souhaitent communiquer ensemble de façon secrète, il faut que chacune d'entre elle échange une clé symétrique différents avec chacune des autres personnes... 

    

> * Trouvez une idée pour échanger une clé sans risque de la compromettre.
> * Si 5 personnes s'échangent des clés, combien de clé différentes faudra-t-il ?
> * même question avec _n_ personnes




## Chiffrement asymétrique


### Le principe

Dans les méthodes de chiffrement asymétrique, chaque participant possède une **paire de clés** :

* une **clé publique**, que (comme son nom l'indique) tout le  monde peut obtenir.
* une **clé privée**,  propriété exclusive de son propriétaire.



Il doit être impossible de déduire une clé à partir de l'autre.



Ainsi, si Alice souhaite communiquer en secret avec Bob, elle génère deux clé, une privée (rose) et une publique (verte), et envoie la clé publique à Bob en conservant sa clé privée pour elle seule.



<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Asymmetric_cryptography_-_step_1.svg/730px-Asymmetric_cryptography_-_step_1.svg.png" style="zoom:50%;" />




Ces deux clés sont **liées** : un message chiffré avec la clé publique ne pourra être déchiffré  qu'avec la clé privée correspondante (et inversement même si cela n'aura d'intérêt que plus tard). Ainsi, lorsque Bob veut envoyer un message de manière confidentielle à  Alice, il utilise la **clé publique d'Alice** : Seule Alice pourra  déchiffrer le message.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Asymmetric_cryptography_-_step_2.svg/800px-Asymmetric_cryptography_-_step_2.svg.png" style="zoom:50%;" />



On peut faire une analogie avec un coffre fort qui possède deux serrures s'ouvrant avec deux clés différentes avec comme propriété que si on ferme le coffre en utilisant l'une des serrure (donc l'une des clés, la clé publique ici), on ne peut l'ouvrir qu'en utilisant l'autre serrure (donc l'autre clé, la clé privée ici)

<img src="media/deux_serrures.jpg" alt="analogie" style="zoom:50%;" />



Alice peut de la même façon envoyer sa clé publique à toutes les personnes avec qui elle souhaite converser en secret.

Remarquez que si Alice code un message avec sa clé privée (ou ferme le coffre avec sa clé privée) alors toutes les clés publiques seront capables de le déchiffrer (ou d'ouvrir le coffre) : ce n'est pas exactement ce qu'on souhaite lorsqu'on veut échanger en secret mais cela sera utile dans la suite.



> * Que doit faire Bob s'il veut qu'Alice puisse lui répondre de façon à ce que lui seul puisse déchiffrer le message ?
> * Que doit faire Alice si elle souhaite échanger avec un certain nombre _n_ de personnes dans le monde de façon sécurisé ?

### Exemples de chiffrements asymétriques

Il existent différents systèmes de chiffrement asymétrique.  Ils reposent sur des problèmes mathématiques plus ou moins complexes.

- [RSA](https://fr.wikipedia.org/wiki/Chiffrement_RSA) ( `Ronald Shamir Adleman`), 1977

- [Mc Eliece](https://fr.wikipedia.org/wiki/Cryptosyst%C3%A8me_de_McEliece), 1978

- [Goldwasser-Micali](https://fr.wikipedia.org/wiki/Cryptosyst%C3%A8me_de_Goldwasser-Micali) , 1982

- [El Gamal](https://fr.wikipedia.org/wiki/Cryptosyst%C3%A8me_de_ElGamal) :  1984

    ​    

Dans Firefox, vous pouvez voir tous les types de systèmes de codage supportés par le navigateur : 

* Tapez `about:config` dans la barre d'adresse
* tapez ensuite  `security.ssl` dans la barre de recherche



Rendez-vous dans les [exercices](securisation _exercices.md).



###  Inconvénient



Ces systèmes ont un inconvénients majeur : une grande complexité algorithmique. Il est long et lourd (donc énergivore) de crypter et décrypter de cette façon.



 En conséquences :

-   Les système asymétriques sont utilisés pour communiquer une clé d'un chiffrement symétrique.
-   Cette dernière clé est appelée **clé de session**.

  

Rendez-vous dans les [exercices](securisation _exercices.md).



### Le HTTPS

Le protocole `HTTP` est celui qu'utilise les navigateurs web pour permettre aux clients d'envoyer une requête à un serveur et au serveur de lui répondre.

Son problème est la sécurité car les données transitent en clair : quelqu’un pourrait les  intercepter et les lire  sans problème.

C’est pourquoi la version sécurisée de `HTTP` a été créée : c'est le protocole `HTTPS`(`HyperText Transfer Protocol Secure`). Ce protocole s’appuie sur le protocole  `TSL`(`Transport Layer Security`), anciennement `SSL` (`Secure Socket Layer`).



Les échanges vont se faire grâce à une clef symétrique. Pour l'échanger sans risque entre client et serveur, un chiffrement asymétrique va être nécessaire.

Pour chaque nouveau client effectuant une requête à un serveur  :

- Le serveur génère une clé publique et une clé privée pour un chiffrement asymétrique.
-  Il envoie sa clé publique au client.
- Le client génère une clé de session pour un chiffrement symétrique. 
- Le client chiffre la clé partagée avec la clé publique du serveur et l’envoie au serveur.
- Le serveur reçoit la version chiffrée de la clé publique envoyée au client et la déchiffre avec sa clé  privée.
- Le client et le serveur sont donc en possession de la clé de session et peuvent dorénavant échanger des données en les chiffrant et les déchiffrant avec elle.



>  La méthode parfaite ??? ET ben non... il demeure quelques problèmes, comme nous allons le voir ci-dessous, et la description du protocole HTTPS n'est pas tout à fait complète. On y reviendra plus loin.



## Signatures électroniques

### Encore des problèmes

Alice et Bob souhaitent communiquer : ils génèrent leur paire de clés et chacun envoie à l'autre sa clé publique.



Le problème et que les clé publique sont publiques et qu'il peut donc y avoir beaucoup de monde qui possèdent les clés publiques d'Alice et Bob : n'importe qui pourrait donc leur envoyer un message en se faisant passé pour eux !



 On souhaite pouvoir garantir :

- l'authentification un document : provient-il bien de son auteur ? 

- La certification du document : est-on certain de recevoir les données que l'auteur du message voulait nous envoyer ? Ce dernier point implique que toute modification ultérieure à l'envoie du message invaliderait la signature. 

  

> *  **Il s'agit du problème de la signature** , 
> * **La signature électronique doit être fonction du document et de son auteur.**



### Signer ses données

Nous avons vu lus haut qu'un chiffrement réalisé par une des clés, publique ou privée, ne pouvait être déchiffrée par l'autre clé, privée ou publique.  Nous allons utiliser cette symétrie pour signer nos données. 

Supposons qu'Alice souhaite envoyer un message secret à Bob :

* elle doit coder ce message avec la clé publique de Bob.

* Avant de chiffrer ce message :

    * Grace à une fonction dites de [hachage](https://fr.wikipedia.org/wiki/Fonction_de_hachage) elle va générer une _empreinte numérique_ de son message. Un _condensé_ en quelque sorte. Ce condensé pourra être comparer au message original pour certifier qu'il n'a pas changé entre l'envoi et la réception (voir plus bas pour une explication).

    * elle va chiffrer son condensé avec sa clé privée, que seule sa clé publique peut déchiffrer : **le condensé chiffré constitue sa signature**.
    * elle ajoute sa signature aux données à envoyer à Bob.

* Il ne reste plus à Alice qu'à chiffrer l'ensemble de ces données (message et signature) avec la clé publique de Bob et de les lui transmettre.

    

Voyons ce qui se passe lorsque Bob reçoit le message d'Alice :

* grâce à sa clé privée, il décode l'ensemble des données.

* il trouve la signature :

    * grâce à la clé publique d'Alice dont il dispose, il déchiffre cette signature et trouve alors le condensé et du coup est certain que le message provient d'Alice.
    * il peut comparer ce condensé aux données effectivement reçues pour être certain que les données sont bien conformes à celles envoyées initialement par Alice.

    <img src="media/signature.png" alt="signature" style="zoom:80%;" />
    
    



_Remarque : condensé_

Il n'existe aucune fonction de hachage parfaite, capable de _résumer_ n'importe quelles quantité de données en utilisant moins de données de façon à pouvoir, à partir du résumé, retrouver les données initiales. Sinon, on pourrait tout résumer à un seul octet... 

En revanche, il est possible de fabriquer un empreinte beaucoup plus légère que l'ensemble des données de façon à ce que deux données différentes aient deux empreintes différentes. 

Voici un exemple à partir de deux tableaux :

![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Pedagogical_illustration_of_the_principle_of_hashing_functions.png/800px-Pedagogical_illustration_of_the_principle_of_hashing_functions.png)

On considère ici une fonction de hachage consistant à convertir  une image haute résolution en une empreinte très basse résolution.  L'empreinte est beaucoup plus légère en mémoire. Elle perd une grande  partie de l'information mais elle reste suffisante pour distinguer  rapidement deux images.



_Remarques :_

* La signature électronique possède la même valeur juridique qu'une signature manuscrite. 
* [Loi du 13 mars 2000](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000399095/2020-12-04/) portant adaptation du droit de la preuve aux technologies de l’information et relative à la signature électronique. 
* [Décret d'application du 31 mars 2001](https://www.legifrance.gouv.fr/loda/id/LEGITEXT000005630796/2008-02-25/) sur la signature électronique.
* [Site de la Commission européenne](http://europa.eu.int/information_society/eeurope/2005/all_about/security/index_en.htm#Toward a Holistic Approach) sur la signature électronique.



Rendez-vous dans les [exercices](securisation _exercices.md) !



> Bon cette fois c'est bon ? 
>
> Toujours pas ! 
>
> Il reste encore un dernier problème ... et une dernière solution !



## Certificats électroniques

### L'attaque de l'homme du milieu

-   Alice et Bob souhaitent encore communiquer : ils génèrent leur paire de clés et chacun envoie à l'autre sa clé publique.
-   L'attaquant [Mallory](https://fr.wikipedia.org/wiki/Alice_et_Bob) parvient, par un moyen ou un autre, à intercepter la diffusion des clés publiques. A la place, il envoie à Bob et à Alice sa propre clé publique en se faisant passer pour Alice et Bob.
-   Alice reçoit la clé publique de Mallory pensant avoir celle de Bob.
-   De même Bob reçoit la clé publique de Mallory pensant avoir celle d'Alice.

![](media/alice-bob-mallory.jpg)

> Comment Mallory peut-il utiliser cette situation pour intercepter, déchiffrer et modifier les données que s'échangent Alice et Bob à leur insu ?



Il faut trouver une parade pour éviter que cette situation puisse se produire.



### Encore un problème à régler

Comment l'empêcher d'intercepter les clés publiques qui ne lui sont pas destinées et d'envoyer sa clé publique en se faisant passer pour un autre ?

Lorsqu'une entité (personne ou entreprise) veut communiquer avec un large public, il lui suffit de diffuser sa clé publique. C'est la diffusion de cette clé qui pose problème car elle se fait au travers d'un canal non sécurisé.



Pour régler ce problème, on utilise des certificats.



### Les certificats



Nous allons utiliser un _tiers de confiance_ selon le principe `: quelqu'un en qui j'ai confiance me dit de faire confiance à quelqu'un d'autre, alors je fais confiance à ce quelqu'un d'autre.`



Il faut savoir qu'il existe des **autorités de certification**. Il s'agit d'organismes dûment enregistrés et certifiés auprès des autorités publiques. Les systèmes d'exploitation et navigateurs web incluent nativement les  listes des clés publiques de ces autorités de certification. 



Ces autorités produisent des **certificats électroniques**. Il s'agit d'un ensemble de données contenant :

- des informations d'identification (nom, adresse mail ...)

- une clé publique

- une signature des données précédentes obtenue avec la clé privée de l'autorité de certification. 

    

Ainsi, lorsque Alice veut pouvoir diffuser sa clé publique de façon sécurisée :

- Elle effectue une demande auprès d'une autorité de certification. Celle ci reçoit la clé publique et l'identité d'Alice. 

-   Après avoir vérifié la clé publique et l'identité d'Alice par des moyens conventionnels, l'autorité crée un certificat contenant les données qu'elle a recueillie au sujet d'Alice, la clé publique d'Alice et la signature de l'ensemble de ces données crytpée avec sa clé privée (celle de l'autorité).
    
    

Bob souhaite communiquer avec Alice et il a donc besoin de sa clé publique :

*  Alice lui envoie son certificat.

* Bob peut alors vérifier l'intégrité du certificat en utilisant la clé publique de l'autorité de certification.
* S'il authentifie le certificat, alors il peut utiliser la clé publique d'Alice qu'il contient pour échanger avec elle.



> 
>
> Que se passe-t-il si l'homme du milieu, Mallory, veut envoyer à Bob sa clé publique ?
>
> 



### Certification d'un site web

Nous avons parlé plus haut du `HTTPS`. L'échange de clés publiques par les navigateurs utilisent également des certifications.



Vous êtes certainement déjà tombés sur ce genre de mise en garde en surfant sur le web :

![](media/pb_certification.jpg)

Le site web vous a envoyé sa clé publique sans utiliser un certificat ou alors ce certificat est caduque  ou non valide (trop vieux par exemple ou décerné par une autorité non reconnue par le navigateur). Ainsi, le navigateur vous prévient qu'il ne peut s'appuyer sur aucun organisme de confiance pour certifier l'origine de la clé publique qu'il a reçue. Il y a donc un risque à échanger des données avec ce site web. 



### Revenons au HTTPS



>  Retourner lire le déroulement de l'établissement d'une connexion HTTPS. 
>
> A quel moment y-a-t-il un problème de sécurité ?
>
> Comment y remédier en utilisant certificat et signature ?



_____________

Par Mieszczak Christophe

Licence CC BY SA



sources images :

* [Enigma - wikipedia - CC BY SA](https://commons.wikimedia.org/wiki/File:Enigma-IMG_0484-black.jpg)
* [Code César wikipédia domaine publique](https://commons.wikimedia.org/wiki/File:Caesar3.svg)
* [clé symétrique wikipédia CC BY SA](https://commons.wikimedia.org/wiki/File:Chiffrement_sym%C3%A9trique.png)
* [Alice et Bob 1 - wikipedia - CC BY SA](https://commons.wikimedia.org/wiki/File:Asymmetric_cryptography_-_step_1.svg)
* [Alice et Bob 2 - wikipedia - CC BY SA](https://commons.wikimedia.org/wiki/File:Asymmetric_cryptography_-_step_2.svg)
* coffre à deux serrures : production personnelle.
* [attaque de l'homme du milieu - wikipedia - CC BY SA](https://commons.wikimedia.org/wiki/File:Alice-bob-mallory.jpg)
* signature : production personnelle à partir de l'image précédente.
* [condensé et tableaux - wikipédia - CC BY SA](https://commons.wikimedia.org/wiki/File:Pedagogical_illustration_of_the_principle_of_hashing_functions.png)
* problème de certificat : production personnelle
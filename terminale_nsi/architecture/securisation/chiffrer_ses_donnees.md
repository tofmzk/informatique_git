# Chiffrer ses données 



## Préparons le terrain

### Clé usb

Insérez votre clé usb puis lancer un BASH linux.

L'URL de votre clé usb est `../../media/nom_utilisateur/nom_de_la_cle_usb`.

On rappelle que pour se promener dans l'arborescence du système de fichiers, on utilise la commande `cd` :

* `cd ..` remonte au répertoire parent.
* `cd rep` ouvre le répertoire `rep`.

_Remarque :_

* Notez que l'autocomplétion se fait en appuyant sur la touche `tab` du clavier : pas besoin de taper le nom des répertoires en entier.

    

Si vous êtes perdu, la command `pwd` vous indiquera le répertoire courant et la commande `ls` en listera le contenu.

![](media/shell_cle_usb.jpg)

  

> Placez vous dans le répertoire de votre clé USB.

 

Nous allons maintenant créer le répertoire `cryptographie`. Pour cela on utilise la commande `mkdir rep` qui crée le répertoire `rep`. 

  

> Créez le répertoire `cryptographie` et placez-vous dedans.



### Fichier en clair

La commande `cat entree > sortie`  envoie l'entrée vers la sortie.

Par défaut, si ce n'est pas précisé,  l'entrée standard est le clavier et la sortie standard est l'écran.



> Testez la commande `cat` seule. pour en sortir, taper CTRL + D.



Pour créer une fichier nommé `clair ` contenant un text entré au clavier, il suffit de ne pas préciser l'entrée (qui sera donc le clavier) et de mettre en sortie le fichier.



> * Testez la commande `cat > clair` : tapez un petit texte que vous enverrez vers ce fichier.
> * Quelle commande permet de visualiser le contenu du fichier `clair` à 'écran ?





### Clé symétrique

Le protocole SSL (Secure Socket Layer) a été développé par la société Netscape Communications Corporation pour permettre aux applications client/serveur de communiquer de façon sécurisée. 

`openssl` est une _boite à outils_ permettant de chiffrer/déchiffrer un fichier selon un grand nombre d'algorithmes différents. 



La commande ci-dessous permet de chiffrer `fichier_clair` en etrée (`-in`) avec la méthode [Blowfish](https://fr.wikipedia.org/wiki/Blowfish) (`-bf-cbf` tout collé) avec `fichier_chiffre` en sortie (`-out`):

```txt
openssl enc -bf-cbc -in fichier_clair -out fichier_chiffre
```



Une _phrase_ qui servira de clé d'encodage vous sera demandée (deux fois). Évidemment, il ne faut surtout pas oublier cette _phrase_ !



> * Chiffrez votre fichier `clair` avec cette méthode en appelant le fichier de sortie `chiffre`
> * Grâce à la commande `cat` , visualisez le contenu du fichier `chiffre`.



Pour décoder on utilise la commande ci-dessous où le paramètre `-d` et les noms des fichiers d'entrée et de sortie diffèrent de la commande précédente :

```txt
openssl enc -bf-cbc -d -in fichier_chiffre -out fichier_dechiffre
```



> * Déchiffrez votre fichier `chiffre` en appelant le fichier de sortie `dechiffre`
>
> * Visualisez le contenu du fichier `dechiffre`.



On peut comparer le contenu de deux fichiers avec la commande `diff fichier1 fichier2` qui renvoie dans la sortie standard (l'écran) les différences entre les deux fichiers.



> Comparez les deux fichiers `chiffre` et `dechiffre`.



### Objectif ?



> * Supprimez les fichiers `clair` et `dechiffre` en utilisant la commande `rm fichier` qui supprimer le `fichier`.
> * Ouvrez un second Bash (sans fermer le premier). 
>     * sur le bureau créez un repertoire `temp`.
>     * dans ce répertoire, créez un fichier `cle_sym` contenant la _phrase_ de déchifrement secrète que vous avez choisie.
>     * ne fermez pas ce Bash, il va servir à nouveau.



Il faudrait pouvoir crypter `cle_sym` pour pouvoir le communiquer avec votre voisin de façon à ce que lui seul puisse déchiffrer la phrase. Une fois qu'il aura `cle_sym` en clair, il pourra alors décrypter votre fichier `chiffre`. Bien entendu, vous devrez décrypter le sien. 

Pour cela on va avoir besoin de **clés asymétriques.**



## Clé asymétrique

### Générer une clé asymétrique

Afin de générez une clé asymétrique, nommée ici `macleasym` , on utilise la commande suivante :

```txt
openssl genrsa -out macleasym 2048
```

Cette commande utilise l'algorithme `rsa` et génère une clé de 2048 bits.

Cette clé contient la **clé publique ET la clé privée**. Il faudrait la crypter car laisser une telle clé sans protection est dangereux ... Mais dans ce TP, nous serons imprudents par soucis de simplification.

> * Générez votre clé asymétrique dans le répertoire `temp` de votre bureau,
> * Visualisez son contenu.

Pour extraire la clé publique, que l'on nommera  `maclepublique`, de votre clé asymétrique qu'on a appelée `macleasym` , on utilise la commande suivante :

```txt
openssl rsa -in macleasym -pubout -out maclepublique
```

> * Extrayez votre clé publique
> * Visualisez son contenu

  

Pour copier la clé publique sur votre clé usb, on utilise la commande `cp source destination` où `source` et `destination` sont des URL.



> * Placez vous dans le répertoire `temp` du bureau et copiez votre clé publique vers le repertoire `cryptographie` de votre clé usb.
> * Passez maintenant votre clé à votre voisin et prenez la sienne. Aidez le à terminer son travail s'il est en retard sur vous.



Pour copier la clé publique de votre voisin dans votre répertoire `temp`, on utilise la commande `cp` en renommant le fichier pour qu'il n'écrase pas le votre. Pour cela, dans l'URL de destination, on indique à la fin de l'URL un autre nom de fichier, par exemple `saclepublique` .

 

> * Copiez la clé publique de votre voisin dans le répertoire `temp` du bureau en le renommant `saclepublique`.
> * Ne lui rendez pas encore sa clé usb !



### Chiffrons 

Pour chiffrer le fichier `cle_sym` avec la clé publique de votre voisin, on utilise la commande suivante :

```txt
openssl pkey -encrypt -in cle_sym -inkey saclepublique -pubin -out cle_sym_chiffre
```

* **ATTENTION**, la dernière lettre de `rsautl` est un `L` minuscule.
* `-in` prècise l'entrée
* `-inkey` la clé de cryptage
* `-pubin` prècise qu'il s'agit d'une clé publique.
* `-out` précise la sortie.

> * Depuis le répertoire `temp` du bureau, chiffrez votre clé `cle_sym` avec la clé publique du voisin. Vous nommerez le fichier chiffré `cle_sym_chiffre`
> * Copiez le fichier chiffré obtenu dans le répertoire `cryptographie` de la clé USB de votre voisin.
> * Echangez vos clés à nouveau. Aidez votre voisin à terminer son travail s'il est en retard sur vous.



### Déchiffrons

A ce stade, vous avez récupéré votre clé USB. Dans le répertoire `cryprtographie`, de cette clé doivent se trouver les fichiers `chiffre`  et `cle_sym_chiffre`.

Pour déchiffrer `cle_sym_chiffre` grâce à votre clé privée `macleasym`, on utilise la commande suivante :

```txt
openssl rsautl -decrypt -in cle_sym_chiffre -inkey macleasym -out cle_sym_dechiffre
```



> * Copiez les deux fichiers  `chiffre`  et `cle_sym_chiffre` dans le répertoire `temp` de votre bureau
> * Décryptez `cle_sym_chiffre` dans un fichier `cle_sym_dechiffre`.
> * Visualisez le contenu de `cle_sym`.
> * Décryptez le fichier `chiffre` de votre voisin.







__________

Par Mieszczak Christophe

Licence CC BY SA
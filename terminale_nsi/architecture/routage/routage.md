# Routage sur un réseau

## Rappels



L'année dernière, au travers d'un  [TP avec le logiciel _Filius_](../../../premiere_nsi/architecture/transmission_donnees_et_reseaux/TP_Filius.md) nous avons étudié la façon dont les données étaient transmises sur un réseau. N'hésitez pas à aller y jeter un coup d’œil, ou plus, si besoin.



En voici un résumé ...

### Le modèle `TCP/IP`



### La couche application

Une application (votre navigateur, votre boite mail ...) souhaite envoyer des données vers une autre machine ou plus exactement vers un application précise d'une autre machine. Elle structure alors ces données en utilisant un protocole qui lui est propre (HTTP pour un navigateur, SMTP pour un service mail ...). 

Elle passe ensuite ces données à la couche inférieure.

### La couche transport

La couche transport découpe les données en petits paquets numérotées. Elle a la charge du transport de ces paquets vers le destinataire. 

* Si le protocole `TCP` est utilisé, il s'occupe de vérifier que le destinataire est prêt à recevoir les données, il contrôle la bonne transmission de ces données et de leur _reconstruction_ complète lors de leur réception et préviens l'émetteur de la réussite de la réception. Notez que les paquet n'arrivent pas forcément dans l'ordre.
* S'il s'agit du protocole `UDP`, il se _contente_ d'envoyer les paquets, gagnant ainsi en rapidité ce qu'il perd en sécurité.

L'avantage du découpage en petit paquet est qu'en cas de perte de données :

*  dans le cas de `TCP`,  il suffit de renvoyer un seul petit paquet et non pas l'ensemble des données.
* dans le cas d'`UDP`, la perte de données n'engendrera qu'une gêne passagère, une pixellisation sur un écran par exemple.



Pour envoyer ces données au destinataire, elle a besoin de localiser l'émetteur et le destinataire pour pouvoir trouver un chemin de l'un vers l'autre. C'est la couche `internet` qui s'en charge.

### La couche internet

Dans un réseau local, les machines possèdent une adresse `IP privée` qui, elle même, contient l'adresse du réseau sur lequel elles se trouvent, celle-ci se déduisant de l'adresse IP en réalisant un `ET logique`  entre cette adresse et le masque de sous-réseau.  Deux machines de deux réseaux locaux différents peuvent avoir la même adresse IP privée. 

Cela un avantage et un inconvénient :

* Avantage : Il n'est pas nécessaire d'assigner une adresse unique à toutes les machines donc il n'est pas nécessaire d'avoir un nombre d'adresses uniques gigantesque. Malgré cela, la norme `IPv4` sera tôt ou tard dépassée et remplacée par la norme `IPv6` qui permet un (beaucoup) plus grand nombre d'adresses.
* Inconvénient : On ne peut donc pas trouver une route vers ou depuis une machine d'un autre réseau uniquement grâce à cette adresse : on dit qu'une IP privée seule n'est pas `routable`. 

Pour pouvoir trouver une route entre deux machines, il faut utiliser une adresse `IP public` unique au monde. Seules certaines machines peuvent avoir une adresse `IP public`. Chez vous, la box internet en possède. Elle est donc `routable` sur internet. 

Pour ne pas mélanger les adresses `IP privée` et `IP publique`, des plages leurs sont allouées. Dans un réseau local, les adresses à utiliser sont :

* 10.0.0.0/8
* 172.16.0.0/12
* 192.168.0.0/16 
* 192.168.0.0/24

>   
>
>   * A quoi correspondent les `/8`, `/12` et `/16` ?
>   * combien de sous-réseaux peut-on définir dans chaque cas ?
>   * combien de machines peut-on identifier sur un réseau dans chaque cas ?
>
>   





La box est également capable de remplacer l'adresse IP privée d'une machine de son réseau par son adresse publique ou de faire l'inverse. On parle alors de `NAT` pour `Network Address Translation`.  Ce procédés permet, par l'intermédiaire de la box qui joue alors le rôle de `routeur NAT`, de rendre l'IP privée `routable`.

Chaque paquet de la couche supérieure est alors encapsulée dans un paquet _plus grand_ sur lequel est ajouté les IP publiques de l'émetteur et du destinataire.

Juste avant de passer à la couche inférieure, le protocole `ARP` permet de faire la correspondance entre les adresses `IP` et  `MAC`(pour `Media Access Control`) des machines, ce qui facilitera le travail de la couche inférieure. La sous-couche `MAC` servira d'interface entre la partie logicielle et la partie matérielle.

 On peut alors continuer.

### Couche accès au réseau 

A ce niveau, nous disposons d'une trame qui être convertie en signal physique qui va transiter sur des interfaces câblée `Ethernet` (signal électrique), `Wi-fi` (signal électromagnétique) ou fibre optique (ondes lumineuses) , par exemple car il y en a beaucoup d'autres.

>   
>
> Voyez-vous d'autres interfaces ?
>
>   

### Le routage



Pour envoyer les données d'un émetteur à un destinataire hors du réseau local, il faut passer par des `routeurs` , qui servent de `passerelles` entre les réseaux ou vers d'autres routeurs. 

Un routeur possède autant de cartes réseaux qu'il relie de réseaux ou de routeurs entre eux, de façon à pouvoir communiquer avec chacun d'entre eux avec des adresses IP compatibles avec toutes ces machines.

 Il existe deux type de routeurs :

* Les routeurs `d'accès` donnent directement accès à un ou plusieurs réseaux. Ils utilisent des interfaces `Wi-Fi` ou `Ethernet`.
* Les routeurs `internes` relaient les paquets vers des routeurs d'accès. Ces routeurs, souvent connectés sur de plus grandes distances, utilisent des fibres optiques, liaisons satellite ou câbles téléphoniques.

![avec routeur](media/routeurs.jpg) 

>   
>
>   * Quel(s)s routeur(s) sont des routeurs internes ? Des routeurs d'accès ?
>
>   * Combien d'interface(s) réseau possèdent les routeurs `Routeur1` , `Routeur A`, `Routeur C` ?
>
>     ​    

_Remarque :_

Une _box_ internet est à la fois :

* Switch : elle permet de connecter entre eux les machines du réseau local.
* Serveur DHCP : elle attribue une adresse IP privée à chaque nouvelle machine connectée sur son réseau local.
* Serveur DNS : elle fait le lien entre le nom de domaine du site que vous voulez atteindre et l'adresse IP du serveur qui l'héberge. Si elle ne connait pas le site, elle interrogera le serveur DNS de vote fournisseur d'accès.
* Routeur `NAT` : elle fait la correspondance entre son adresse privée et l'adresse publique de l'émetteur.
* Routeur : elle offre un accès au réseau global qu'est internet.



### Réception



Une fois arrivée à destination, les paquets remontent les couches du protocoles `TCP/IP` à l'envers :



![modèle - source perso](media/aller_retour.jpg)




**L'objet de cette leçon est de comprendre comment se passe ce routage.**



## Routage

Reprenons notre petit réseau :

![avec routeur](media/routeurs.jpg)

* Si `192.168.0.1`, veut envoyer un message à `192.168.0.2`, le message arrive au `switch1` qui sait que ces machines sont sur le même réseau grâce à leurs adresses IP. Le message est alors directement transmis. 

* Si `192.168.0.1` veut envoyer un message à `192.168.2.1` : le message arrive au `switch1` qui sait que le destinataire n'est pas sur son réseau. Il envoie alors le message  à son routeur d'accès `routeur1` qui lui même le transmettra au routeur interne `routeur A` qui transmettra :

    * soit à `Routeur B` qui le transmettra au routeur d'accès `routeur 3` qui transmettra au `switch 3` qui connaît alors la machine de destination.
    * soit à `Routeur C` qui le transmettra à `Routeur D` qui poursuivra l'acheminement vers le `routeur 3` puis le `switch 3`.

    

_Remarques :_

* Le chemin qui utilise le moins de routeur n'est pas forcément le meilleur : le débit de l'interface utilisée par le routeur (fibre optique plutôt que câble téléphonique en cuivre par exemple) peut rendre un chemin plus rapide qu'un autre, même en passant par un plus grand nombre de routeurs.

* Le nombre de routeurs sur le réseaux est généralement trop grand pour qu'on puisse les configurer à la main. De plus, cette configuration doit être actualisée en permanence au cas ou un routeur tombe en panne (il faut alors trouver un autre chemin) ou que sa topologie est modifiée (nouveaux routeurs installés, nouveaux réseaux ajoutés, type d'interface modifiée ..).  

  

**La question est : comment font les routeurs pour savoir quel chemin prendre et les actualiser en permanence ?** 

### Les tables de routage

#### Qu'est-ce que c'est ? A quoi ça sert ?

Une table de routage peut être vue comme un tableau contenant les informations nécessaires au routeur pour savoir à quelle machine transmettre les données qu'il reçoit pour qu'elles puissent atteindre leur destination. Chaque routeur possède sa table de routage. 

![avec routeur](media/routeurs.jpg)

Voici un exemple de table de routage du `Routeur 1` :

| IP destination |    Masque     | Passerelle |     Interface     | Métrique |
| :------------: | :-----------: | :--------: | :---------------: | :------: |
|  192.168.2.0   | 255.255.255.0 | 172.0.0.2  | 172.0.0.1(`eth0`) |    2     |
|  192.168.3.0   | 255.255.255.0 | 127.0.0.2  | 172.0.0.1(`eth0`) |    2     |

* L'IP de destination est la destination finale visée.
* Le masque est le masque de sous-réseau.
* la passerelle est la machine suivante (ou son interface) à joindre pour atteindre la destination finale.
* L'interface correspondant au moyen utilisé pour le transport des données (câble, wi-fi ...)
* Le vecteur `métrique` correspond au `coût` de l'utilisation de la route choisie. Selon la façon dont le routage est effectué, ce coût peut s'exprimer de différentes façons comme nous le verrons plus loin. Ici il exprime le nombre minimum de routeurs restant pour atteindre la destination finale.

Cette table peut également être écrite avec la norme `CIDR` qui _simplifie_ l'écriture des masques :

Reprenons la table de routage du `Routeur 1` :

| IP destination |  Passerelle  |      Interface       | Métrique |
| :------------: | :----------: | :------------------: | :------: |
| 192.168.2.0/24 | 172.0.0.2/24 | 172.0.0.1/24(`eth0`) |    2     |
| 192.168.3.0/24 | 127.0.0.2/24 | 172.0.0.1/24(`eth0`) |    2     |



> ​    
>
> Dresser une table de routage possible pour le `Routeur A` et le `Routeur 2`.
>
>    



#### Comment se construit une table de routage ?

Au départ, chaque table de routage d'un routeur ne contient que les machines, les réseaux ou les routeurs qui lui sont directement connectés. Ensuite, chaque routeur envoie périodiquement à tous ses voisins des messages contenant la liste de tous les réseaux et chemins qu'il connaît déjà. De même chaque routeur reçoit des messages de ses voisins :

* S'il découvre une nouvelle route vers un réseau qui lui était inconnu, il l'ajoute simplement à sa table de routage.
* S'il découvre une nouvelle route vers un réseau qui lui était connu :
    * Si sa métrique est supérieure à la route qu'il connaissait, cela veut dire qu'un problème est apparu sur son ancienne route et que celle-ci soit faire un _détour_ : il met alors à jour sa table avec cette nouvelle route.
    * Si la métrique de cette nouvelle route est inférieure à celle qu'il connaissait alors il supprime l'ancienne route de sa table et la remplace par la nouvelle.



Chaque routeur diffuse sa table de routage aux routeurs voisins toutes les 30 secondes. Si un routeur ne donne plus de nouvelle pendant une période trop longue (3 minutes), ses voisins considèrent qu'il est en panne :

* Ils suppriment alors le routeur en panne de leur table de routage
* puisque les routeurs diffusent leur tables à leurs voisins, le réseau entier est rapidement prévenu de la panne et les routes s'actualisent en passant par de nouveaux chemins.
* Lorsque le routeur en panne est finalement réparé, il transmet de nouveau sa table de routage et ses voisins son alors avertis de son retour.

### Le routage en lui même

Un réseau modélisé par un graphe dont les sommets sont les routeurs et les arêtes les interfaces. Pour trouver un chemin dans le réseau, on recherche donc un chemin dans le graphe en utilisant les algorithmes adéquates. Il en existe de nombreux, nous allons en étudier deux : les protocoles `RIP` et `OSPF`.

_Remarque :_

- Ce routage automatique est appelé `routage dynamique` : on  utilise des protocoles qui vont permettre de "découvrir" les différentes routes automatiquement afin de pouvoir remplir la table de routage tout aussi automatiquement. 							
- On peut également utiliser `le routage statique` : chaque ligne de la table doit être renseignée "à la  main". Cette solution est seulement envisageable pour des très petits  réseaux de réseaux.					



#### Le protocole `RIP`

Le `Routing Information Protocol` est un protocole de routage des informations qui minimise le nombre de routeur(s) à traverser pour atteindre le destinataire. Il utilise l'algorithme de `Bellman et Ford`,une optimisation de celui qui permet de trouver le [plus court chemin](../../structures_de_donnees/graphes/recherche_chemins.md) d'un sommet à un autre dans un graphe.

Pour cette algorithme, la `métrique` est le nombre de routeurs par lesquels il faut passer afin d'atteindre la destination. Le nombre de saut maximum autorisé est de 15 afin d'éviter qu'un paquet ne se déplace en boucle. Au delà, la route est supprimée de la table.



> Reprenons notre réseau :
>
> ![avec routeur](media/routeurs.jpg)
>
> 
>
> Activité débranchée :
>
> * Déterminez le chemin reliant chaque réseau à chaque réseau selon cet algorithme.
> * Avec Filius :
> * Construisez le réseau ci-dessous et configurez le.
>
> * Installez une application `ligne de commande` sur 192.168.0.1
>
> * Testez :
>
>     * la commande `ipconfig`
>
>     * la commande `ping` vers une `IP` distante.
>
>     * la commande `traceroute` vers une `IP` distante.
>
>         

#### Le protocole `OSPF`

Le protocole `OSPF` (`Open Shortest Path First`) a été développé pour remplacer le protocole `RIP` en prenant en compte le débit des interfaces. 

Pour pénaliser les liaisons lentes, la `métrique` utilisée est le coût d'une liaison est obtenu par le calcul $`\frac {10⁸}{débit}`$ où _débit_  est la bande passante de la liaison en bit/s. Le coût n'a pas d'unité.

>  Voici quelques débits théoriques. Calculer leur coût.
>
>  | Technologie              |    Bande passante     | Coût |
>  | ------------------------ | :-------------------: | :--: |
>  | `Modem`                  |       56 kbit/s       |      |
>  | `Bluetooth`              |       3 Mbit/s        |      |
>  | Ethernet                 |       10 Mbit/s       |      |
>  | `Wi-Fi`                  | 11 Mbit/s à 10 Gbit/s |      |
>  | `ADSL`                   |       13 Mbit/s       |      |
>  | `4G`                     |      100 Mbit/s       |      |
>  | `Satellite`              |       50 Mbit/s       |      |
>  | `FastEthernet`           |      100 Mbit/s       |      |
>  | `Fibre optique` (`FFTH`) |       10 Gbit/s       |      |
>
>  

Le protocole `OSPF` recherche donc des routes en minimisant cette métrique. Il utilise un graphe donc les arêtes sont pondérés et l'algorithme de [Dijkstra](https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra) que nous étudierons en projet.

> Reprenons notre petit réseau :
>
> ![avec routeur](media/routeurs.jpg)
>
> * Calculer le coût du chemin `Routeur 1` $`\rightarrow`$ `Routeur A` $`\rightarrow`$ `Routeur 3`.
> * En utilisant le protocole `OSPF` :
>     * déterminer la route entre le `routeur A` et le `routeur 3`. Donner son coût total.
>     *  donner la table de routage des `Routeur A`.





_________

Par Mieszczak Christophe

licence CC BY SA

source images : productions personnelles,  à partir de Filius pour le réseau.
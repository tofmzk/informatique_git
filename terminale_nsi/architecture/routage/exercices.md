# Exercices

### Connaissez-vous le cours ?

1. Qu'est-ce qu'un serveur DNS ?
2. Qu'est-ce qu'un serveur DHCP ?
3. Qu'est-ce qu'un serveur web ? 
4. Qu'est-ce qu'un client web ? Citez en quelques uns.
5. Quelles différences y-a-t-il entre une adresse IP privée et publique ?
6. Avec la norme IPv4, de combien d'adresses différentes dispose-on ?
7. Avec la norme IPv6, de combien d'adresses différentes dispose-on ?

### Masques et IP

1. Dans un réseau, un hôte est configuré de la façon suivante :
   
   ```txt
   IP : 172.16.1.21
   masque : 255.255.0.0
   ```
   
    a. Quelle est l'adresse du réseau ?
   
    b. Quelle est l'adresse de broadcast ?
   
    c. Combien de machines ce réseau peut-il gérer ?

2. Mêmes questions pour la machine suivante :
   
   ```txt
   IP : 192.168.1.23/24
   ```

3. Trois machines ont pour adresses IP `90.8.220.5`, `90.8.220.20` et `90.8.220.37`. Appartiennent-elles au réseau `90.8.220.0/27` ?



### Configurer un réseau

1. Configurez les hôtes ci-dessous pour que le réseau fonctionne.
    ![](media/reseau_a_completer.jpg)

_________





### Protocole RIP



Voici un exemple de réseau :

![réseau](media/ex_routage.jpg)

1. Suivant le protocole RIP, comment calcule-t-on le coût d'une route ?

2. Compléter, selon ce protocole la table de routage du routeur A ci-dessous :

    | Destination | Passerelle | distance |
    | :---------: | :--------: | :------: |
    |      B      |     B      |    1     |
    |      C      |            |          |
    |      D      |            |          |
    |      E      |            |          |
    |      F      |            |          |
    |      G      |            |          |

    

3. Compléter, selon ce protocole, le tableau ci-dessous qui indique, pour chaque routeur de départ, la portion de sa table de routage vers le routeur G.

    | Départ | Destintation | Passerelle | distance |
    | :----: | :----------: | :--------: | :------: |
    |   A    |      G       |            |          |
    |   B    |      G       |            |          |
    |   C    |      G       |            |          |
    |   D    |      G       |            |          |
    |   E    |      G       |            |          |
    |   F    |      G       |            |          |

    

4. Le routeur E tombe en panne. Quelle(s) ligne(s) de la table de routage du routeur B va (vont) être modifiée(s) ? 

    



### Protocole OSPF



Voici un exemple de réseau :

![réseau](media/ex_routage.jpg)

1. On rappelle que le coût, qui n'a pas d'unité, se calcule en appliquant la formule $`coût = \frac {10 ^ 8 } {débit}`$ où le _débit_ est exprimé bit/s. Complétez le tableau ci-dessous :

    |   interface   |            Débit             | Coût |
    | :-----------: | :--------------------------: | :--: |
    |     Modem     |          56 kbit /s          |      |
    |   Bluetooth   |           3 Mbit/s           |      |
    |   Ethernet    |                              |  1   |
    |     Wi-Fi     | entre 11 Mbit/s et 10 Gbit/s |      |
    |     ADSL      |          13 Mbit/s           |      |
    |      4G       |          100 Mbit/s          |      |
    | FastEthernet  |          100 Mbit/s          |      |
    | FFTH ( fibre) |          10 Gbit/s           |      |
    |               |                              |      |

2. Suivant le protocole OSPF, comment calcule-t-on le coût d'une route ?

3. Compléter, selon ce protocole la table de routage du routeur A ci-dessous :

    | Destination | Passerelle | coût |
    | :---------: | :--------: | :--: |
    |      B      |     B      |      |
    |      C      |            |      |
    |      D      |            |      |
    |      E      |            |      |
    |      F      |            |      |
    |      G      |            |      |

    

4. Compléter, toujours selon ce protocole, le tableau ci-dessous qui indique, pour chaque routeur de départ, la portion de sa table de routage vers le routeur G.

    | Départ | Destination | Passerelle | distance |
    | :----: | :---------: | :--------: | :------: |
    |   A    |      G      |            |          |
    |   B    |      G      |            |          |
    |   C    |      G      |            |          |
    |   D    |      G      |            |          |
    |   E    |      G      |            |          |
    |   F    |      G      |            |          |

    

5. Le routeur E tombe en panne. Quelle(s) ligne(s) de la table de routage du routeur B va (vont) être modifiée(s) ? 



### A l'envers

Voici les tables de routages de 4 routeurs A, B, C, D qui utilisent le protocole RIP.

Table de A :

| Destination | Passerelle | Distance |
| :---------: | :--------: | :------: |
|      B      |     B      |    1     |
|      C      |     B      |    2     |
|      D      |     D      |    1     |
|      E      |     D      |    2     |

Table de B
| Destination | Passerelle | Distance |
| :---------: | :--------: | :------: |
|      A      |     A      |    1     |
|      C      |     C      |    1     |
|      D      |     A      |    2     |
|      E      |     A      |    3     |

Table de C
| Destination | Passerelle | Distance |
| :----------: | :--------: | :------: |
| A            | B          | 2        |
| B            | B          | 1        |
| D            | B          | 3        |
| E            | B          | 4        |

Table de D


| Destination | Passerelle | Distance |
| :----------: | :--------: | :------: |
| A            | A          | 1        |
| B            | A          | 2        |
| C            | A          | 3        |
| E            | E          | 1        |

Table de E
| Destination | Passerelle | Distance |
| :----------: | :--------: | :------: |
| A            | D          | 2        |
| B            | D          | 3        |
| C            | D          | 4        |
| D           | D          | 1       |

Dessinez un réseau compatible avec ces tables de routage.





_______________

Par Mieszczak Christophe

licence CC BY SA

source images : productions personnelles,  à partir de Filius pour le réseau.
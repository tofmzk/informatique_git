# Processus

### Exercice 1

Dans un bureau d’architectes, on dispose de certaines ressources qui ne peuvent être utilisées
simultanément par plus d’un processus, comme l’imprimante, la table traçante, le modem.
Chaque programme, lorsqu’il s’exécute, demande l’allocation des ressources qui lui sont
nécessaires. Lorsqu’il a fini de s’exécuter, il libère ses ressources.

|        Programme 1        |      Programme 2      |        Programme 3        |
| :-----------------------: | :-------------------: | :-----------------------: |
| demander (table traçante) |   demander (modem)    |   demander (imprimante)   |
|     demander (modem)      | demander (imprimante) | demander (table traçante) |
|         exécution         |       exécution       |         exécution         |
|      libérer (modem)      | libérer (imprimante)  | libérer (table traçante)  |
| libérer (table traçante)  |    libérer (modem)    |   libérer (imprimante)    |

1. Supposons que le processus p1 demande la table traçante alors qu'elle est en cours
    d'utilisation par le processus p3. Parmi les états suivants, quel sera l'état du processus p1
    tant que la table traçante n'est pas disponible :
    a) élu    b) bloqué    c) prêt    d) terminé

2. Les processus s'exécutent de manière concurrente (on alterne chacun à leur tour les processus prêts à être exécutés).  Justifier qu'une situation d'interblocage peut se produire.

3. Modifier l'ordre des instructions du programme 3 pour qu'une telle situation ne puisse pas se
    produire. Aucune justification n'est attendue.

  

### Exercice 2

Une commande UNIX  affiche, pour chaque fichier du répertoire en cours, son PID, son  PPID, son statut (R : en cours d'exécution, S : stoppé) et son URL.

1. Définir le PID et le PPID ?

2. Donner et expliquer les différents statuts (ou états) par lesquels passe un processus (on pourra faire un schéma).

3. Expliquer ce qu'est un inter-blocage (on pourra faire un schéma).

4. Une commande renvoie le résultat suivant :

    ```bash
    PID    PPID    STAT    COMMAND
    1      0       Ss      /sbin/init
    ...	   ....    ..      ...
    1912   1908    S       Bash
    1920   1912    S       Thonny
    2091   1908    R       /usr/lib/firefox/firefox
    5437   1920    R       python programm prog1.py
    5440   1908    S       python programm prog2.py
    5450   1912    R       ps -ef 
    ```

    a. Indiquer, parmi les commandes ci-dessous, celle qui donne ce résultat :

    ​            A. ps -ef         B. cd ..         C. ls -a             D. chmod +r 1920

    b. Donner le nom du premier programme exécuté par le système d'exploitation au démarrage.

    d. Donner l'application depuis laquelle a été lancé `prog1.py`.

    e. Donner l'ordre dans lequel `prog1.py` et `prog2.py` ont été lancés.
    
    f. Donner et expliquer les conséquences de la commande `kill -kill 1912` sur chacun des processus listé plus-haut.
    
    

### Exercice 3

Cet exercice porte sur la gestion des processus et des ressources par un système
d’exploitation.

#### Partie 1

1. Les états possibles d’un processus sont : prêt, élu, terminé et bloqué.
    a. Expliquer à quoi correspond l’état élu.
    b. Proposer un schéma illustrant les passages entre les différents états.

2. On suppose que quatre processus C₁, C₂, C₃ et C₄ sont créés sur un ordinateur, et qu’aucun autre processus n’est lancé sur celui-ci, ni préalablement ni pendant l’exécution des quatre processus. L’ordonnanceur, pour exécuter les différents processus prêts, les place dans une structure de données de type file. Un processus prêt est enfilé et un processus élu est défilé.
a. Parmi les propositions suivantes, recopier celle qui décrit le fonctionnement
des entrées/sorties dans une file :
    i. Premier entré, dernier sorti  ii. Premier entré, premier sorti  iii. Dernier entré, premier sorti
b. On suppose que les quatre processus arrivent dans la file et y sont placés
dans l’ordre C₁, C₂, C₃ et C₄.

3. On considère toujours les quatre processus C₁, C₂, C₃ et C₄ qui sont tels que  : 

  - Les temps d’exécution totaux de C₁, C₂, C₃ et C₄ sont respectivement 100 ms, 150 ms, 80 ms et 60 ms.

  - Après 40 ms d’exécution, le processus C₁ demande une opération d’écriture disque, opération qui dure 200 ms. Pendant cette opération d’écriture, le processus C₁ passe à l’état bloqué.

  - Après 20 ms d’exécution, le processus C₃ demande une opération d’écriture disque, opération qui dure 10 ms. Pendant cette opération d’écriture, le processus C₃ passe à l’état bloqué.

​	Sur la frise chronologique ci-dessous, les états du processus C₂ sont donnés. 

![frise](media/frise_processus.png)

​	Compléter la frise avec les états des processus C1, C3 et C4.

#### Partie 2

 On trouvera ci- dessous deux programmes rédigés en pseudo-code.

Verrouiller un fichier signifie que le programme demande un accès exclusif au fichier et l’obtient si le fichier est disponible.

| Programme 1             | Programme 2             |
| ----------------------- | ----------------------- |
| Verrouiller fichier_1   | Verrouiller fichier_2   |
| Calculs sur fichier_1   | Verrouiller fichier_1   |
| Verrouiller fichier_2   | Calculs sur fichier_1   |
| Calculs sur fichier_1   | Calculs sur fichier_2   |
| Calculs sur fichier_2   | Déverrouiller fichier_1 |
| Calculs sur fichier_1   | Déverrouiller fichier_2 |
| Déverrouiller fichier_2 |                         |
| Déverrouiller fichier_1 |                         |


a. En supposant que les processus correspondant à ces programmes s’exécutent simultanément (exécution concurrente), expliquer le problème qui peut être rencontré.
b. Proposer une modification du programme 2 permettant d’éviter ce problème.



___

Par Mieszczak Christophe

Licence CC BY SA

d'après les sujets de Bac 2021 - 2022


# Les Processus

## Rappels

### Rôle de l'OS

Nous avons étudié les différentes tâches d'un [système d'exploitation](../../../premiere_nsi/architecture/OS/OS.md) en première.

Le schéma ci-dessous illustre le fonctionnement d'un OS :

<img src="media/os.jpg" alt="OS" style="zoom:50%;" />



* L'utilisateur agit sur les applications (Firefox, Gimp, LibreOffice ...) en utilisant soit une interface graphique soit directement en ligne de commande dans une console (SHELL). 
* Les applications utilisent des bibliothèques de fonctions qui s'appuient directement sur le noyau du système d'exploitation. Ce noyau gère la mémoire, le système de fichiers, les protocoles réseaux et **l'ordonnancement des processus, objet de ce chapitre**.
* La gestion de la mémoire, du système de fichiers et des protocoles réseaux utilisent des périphériques (disque dur, RAM, cartes réseau ...) en passant par leur pilote respectif, c'est à dire le programme dédié au fonctionnement du périphérique.
* La gestion des processus est une affaire qui se règle entre l'OS et le microprocesseur.



### Les commandes SHELL

Dans ce chapitre, nous allons utiliser des commandes SHELL. Nous en avons parlé en première, lors du chapitre sur le [système de fichiers](../../../premiere_nsi/architecture/OS/le_systeme_de_fichiers.md). 

N'hésitez pas à aller y jeter un œil si besoin ...



## Qu'est-ce qu'un processus ?

Un processus est un programme en cours d’exécution sur un ordinateur. Il peut s'agir de fichiers liés au fonctionnement du système ou d'une application. Notez que si on exécute deux fois le même programme, on crée alors deux processus différents. 

Pour s’exécuter, chaque processus à besoin d'utiliser des ressources : Une certaines quantité de mémoires, des entrées comme le clavier, la souris  ou des sorties comme l'écran ou l'imprimante ... 

L'OS gère les ressources et l’exécution des processus. On dit que le système d'exploitation gère `l'ordonnancement` des processus.



Un processus a plusieurs états :

* L' `initialisation`) est l'état dans lequel se trouve le processus lors de sa création, au lancement d'une application par exemple.
* Il est ensuite dans un état `prêt`, c'est à dire dans l'attente d'être choisi par l'ordonnanceur pour être exécuté.
* Lorsqu'il est `élu` , c'est à dire choisi par l'OS pour être exécuté,  Il peut alors demander d'accéder à une ressource. On dit que le processus est `actif`. L'OS décide combien de _temps_ il reste élu puis le processus repassera à l'état `prêt`.
* Lorsqu'il est `bloqué` , il attend que les ressources dont il a besoin soient disponibles et que l'ordonnanceur l'élise c'est à dire le repasse à l'état `prêt`.
* Enfin, il passe à l'état `terminé` lorsqu'il a achevé son travail ou qu'il a été forcé de s'arrêter.



L'OS choisit un processus parmi les processus `prêts`:

* si les ressources dont il a besoin sont disponibles, un _temps_,  lui est attribué pendant lequel il est exécuté puis remis en attente.
* sinon, il passe à l'état `bloqué` et l'OS élit un autre processus.

![ordonnancement](media/ordonnancement.png)

## Ordonnancement des processus.

### Au démarrage



Sous un système d'exploitation comme Linux, au moment du  démarrage de l'ordinateur un tout premier processus (appelé `processus 0`  ou encore `Swapper`) est crée.

Ensuite, ce `processus 0` crée un processus souvent appelé  `init`. Vous l'avez compris, un processus peut créer un ou plusieurs processus. Il est alors le père de ces processus qui, eux, sont ses fils.

À partir de `init`,  les processus fils nécessaires au bon fonctionnement du système sont créés, puis d'autres processus sont créés à partir des ces fils ...  On peut modéliser ces  relations père/fils par une structure arborescente. Sous Linux, les processus fils de `init` sont appelés `démons`. 



Dès leur création, chaque processus se voit attribuer un numéro unique qui l'identifie : son `PID`. Tous les processus, en dehors du `processus 0`, ont également un `PPID` qui indique le `PID` de leur parent. 

​	

Voici, par exemple, une représentation de cette arborescence pour un démarrage de Linux :

<img src="media/demarrage.jpg" alt="création des procesus" style="zoom:50%;" />

* [inetd](https://fr.wikipedia.org/wiki/Inetd) permet de gérer les connexions à des  services réseau.
* [cron](https://fr.wikipedia.org/wiki/Cron) permet aux utilisateurs d’exécuter automatiquement des scripts
* [getty](https://fr.wikipedia.org/wiki/Getty_%28Unix%29) , lorsqu'il détecte une connexion, crée le processus `login` qui lui permet de s'identifier.
* [login](https://fr.wikipedia.org/wiki/Login_(informatique)) vérifie l'authenticité des identifiants et poursuit le création de processus s'il sont valides.
* [shell](https://fr.wikipedia.org/wiki/Shell_Unix) est le processus correspondant au SHELL UNIX. Il sera le parent des applications que l'utilisateur lancera en ligne de commande, par exemple Firefox, Gimp ou Xeyes.



Sous Linux, les processus qui tournent ne permanence en arrière plan sont appelés `démons` ou `daemons`.



### Algorithmes d'ordonnancement

Considérons trois processus, chacun comportement plusieurs lignes d'instruction. 

<img src="media/trois_processus.jpg" alt="trois processus" style="zoom:80%;" />

Dans un microprocesseur, un cœur exécute un seul processus à la fois.  Aujourd'hui, les micro-processeurs étant multi-cœur, plusieurs processus peuvent être exécutés simultanément en parallèle... si l'OS est programmé de façon à pouvoir le faire.



 **La performance du système dépend donc énormément de la façon dont l'OS gère ces processus**.



Voici quelques exemples d'algorithmes d'ordonnancement.

#### En parallèle

Si le microprocesseur possède plusieurs coeurs et que l'OS sait les gérer, il peut ordonner les processus en parallèle. Avec trois processus pour trois coeur, cela donne alors 3 piles d'instructions, une par coeur :

<img src="media/trois_processus.jpg" alt="trois processus" style="zoom:80%;" />



Etant donné qu'il y a énormément de processus qui tournent simultanénment, même un processeur multi-coeurs bien géré par un OS devra ordonner plusieurs processus simultanément pour chaque coeur. Il faut alors décider de la stratégie à adopter pour chaque coeur .... Voyons cela.



#### Le tourniquet / la concurrence

Pour donner l'illusion que les différents programmes tournent en même temps, un temps très court est alloué à tour de rôle aux processus. C'est un algorithme simple et facile à mettre en œuvre. La rapidité avec laquelle on passe d'un processus au suivant donne l'illusion d'une exécution simultanée.

<img src="media/tourniquet.png" alt="tourniquet" style="zoom:80%;" />



#### Le système de priorités

Lors de l'élection d'un processus, l'OS utilise un système de priorités. Il peut y en avoir plusieurs :

* **premier entré, premier servi** : chaque processus créé et placé dans une file et les processus s'exécutent les uns après les autres. C'est celui qu'on utilise, par exemple, pour gérer la file d'impressions d'une imprimante. 

    <img src="media/file_processus.jpg" alt="file de processus" style="zoom:80%;" />

* **Le plus court d'abord** : le processus qui se terminera le plus vite passera en priorité. c'est un algorithme très efficace pour satisfaire l'utilisateur, mais il est compliquer de déterminer si un processus sera rapidement terminé ou pas avant qu'il ne s'exécute. 

    <img src="media/plus_court.jpg" alt="plus court d'abord" style="zoom:80%;" />





### Les Interblocages

Les interblocages sont des problèmes qui peuvent survenir dans l'ordonnancement des processus.

Un tel blocage survient lorsque deux processus (ou plus de deux processus), s'attendent mutuellement : le premier utilise une ressource nécessaire au second qui , lui même, utilise une ressource nécessaire au premier. Les deux processus sont donc placés dans un état `bloqué`  sans fin, tant qu'aucun des deux n'est détruit et ne libère la ressource qui débloquera l'autre.

L'ordonnancement est alors dans une impasse.



Deux solutions sont mises en œuvre par l'OS devant ce problème :

* essayer d'éviter un interblocage... mais il n'est pas simple de les prévoir à l'avance, et même souvent impossible.
* détecter les interblocages quand ils surviennent et les supprimer, par exemple en détruisant les processus concerné. C'est la solution la plus fréquemment utilisée.



## Shell et  processus



### Les fils du SHELL



L'exécution d'une commande  par le shell donne lieu à un nouveau processus. Chaque processus est  repéré par un identifiant système unique (PID). La commande `ps` permet de lister les processus en cours d’exécution dans le SHELL.

> Tester la commande `ps` et copier le résultat ci-dessous. Commentez le !
>
> ```txt
> 
> ```
>

   

Si on veut visualiser tous les processus en cours, on utilise `ps -e`. Allez-y vous alle voir qu'il y en a beaucoup ...



>  
>
>  Trouvez le nom du processus dont le PID est 1
>
>  Comme il y a énormément de processus, on peut demander de lister les résultats avec possibilité de les parcourir grâce aux flèches du clavier. La commande à taper est alors : `ps -e | less`.
>
>  

Si on veut tous les détails sur nos processus, il faut ajouter l'option `-f` ou `-aux`.



>  
>
>  Testez la commande `ps -f` , repérez les différentez colonnes et indiquez ci-dessous à quoi elles correspondent :
>
>  | UID  | PID  | PPID |  C   | STIME | TTY  | Time | CMD  |
>  | :--: | :--: | :--: | :--: | :---: | :--: | :--: | :--: |
>  |      |      |      |      |       |      |      |      |
>
>  

On peut également de lister qu'un seul processus dont on connait le nom. Pour cela la commande est `ps -C nom_processus` éventuellement suivi de l'option  `-f`  si on souhaite voir tous les détails.

>   
>
> Afficher uniquement le processus de PID 1 dont vous avez trouvé le nom plus haut avec tous les détails le concernant.
>
>   



Installons maintenant un petit jeu qui s'execute en console : le snake. Pour cela, tapez la commande suivante :

```shell
sudo apt install nsnake
```

* _sudo_ vous place en mode administrateur et, pour cela, vous demande son mot de passe
* _apt_ est une abréviation de _application_
* _install_ se passe de commentaire
* _nsnake_ est le nom de l'application.

Pour lancer le jeu, tapez directement _nsnake_ dans la console.

On va mettre le jeu en pause en tapant CTRL + z.

>  
>
> Tapez à nouveau la commande ps et :
>
> * trouvez le PID de nsnake 
> * Trouver son PPID : à quoi correspond-t-il ?
>
> 




La commande `kill -parametre pid` envoie un signal au processus repérer par son PID :

* si le paramètre est `kill alors le processus est détruit.	

* si le paramètre est `stop` alors le processus est bloqué.

* si le paramètre est  `cont` alors le processus préalablement bloqué repasse à l'état prêt. 

    

>     * Débloquez _nsnake_ en utilisant la commande `kill`
>     * tapez à nouceau ctrl + Z
>     * Détruisez le processus _nsnake_.





## Sous Windows



Sous Windows, on peut visualiser les processus avec le gestionnaire de tâches qu'on lance en appuyant simultanément sur les touches `Ctrl` + `Shift` + `Esc`.



* Dans l'onglet `processus` :

    *  vous pouvez arrêter un processus, après un clic droit sur son nom.

    * vous visualisez :

        * le nom du programme qui correspond au processus
        * le taux d'occupation du processeur pour ce processus à un moment donné.

        <img src="media/processus_windows.jpg" alt="processus" style="zoom:80%;" />

* Dans l'onglet `Détails`, vous retrouverez le `PID` et des processus en cours et d'autres informations :

    <img src="media/details.jpg" alt="details" style="zoom:80%;" />

* Sous Windows, les processus qui tournent en arrière plan en permanence, comme les anti-virus, sont appelés `services`. Sous l'onglet `services`, vous pouvez les visualiser :

    <img src="media/services.jpg" alt="services" style="zoom:80%;" />



______________

Mieszczak Christophe	licence CC BY SA

sources images : production personnelle
# Recherche textuelle

On s'interesse ici à la recherche des occurences d'une chaine de caractères dans une autre. Ce problème d'apparence anodin est en réalité d'une importance cruciale : lorsque vous recherchez un mot précis dans un document, un nom dans un annuaire,  une donnée dans une base de donnés ou envoyez une requête à un moteur de recherche, il es tlà, juste derrière !



On va ici s'intéresser à la recherche d'une séquence d'ADN, qu'on appelera motif, dans un brin d'ADN. Un brin d'ADN est constitué d'acides aminés notés A, C, G ou T et peut être représenté par une chaine de caractères du genre `CTATGTGCTGCAACCTTGC...`. Repérer la position d'un motif comme `TGCT` dans la chaine ADN complète sera le fil rouge de cette leçon.

## Algorithme naïf

### Principe

Pour rechercher notre motif dans nu brin d'ADN, l'algorithme de recherche le plus _naturel_ est celui-ci :

* On place les deux chaines l'une au dessus de l'autre :

| T     | G    | C    | T    |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| **C** | T    | A    | T    | G    | T    | G    | C    | T    | G    | C    | A    | A    | C    | C    | T    | T    | G    | C    | ...  |

* **T** et **C**  ne correspondent pas donc on décale d'un cran à droite  le motif sur le brin :

|      | T     | G    | C    | T    |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| ---- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| C    | **T** | A    | T    | G    | T    | G    | C    | T    | G    | C    | A    | A    | C    | C    | T    | T    | G    | C    | ...  |

Cette fois ci, on a bien aligné **T** avec **T** donc on passe à la seconde lettre du motif :

|      | T    | G     | C    | G    |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| ---- | ---- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| C    | T    | **A** | T    | G    | T    | G    | C    | T    | G    | C    | A    | A    | C    | C    | T    | T    | G    | C    | ...  |

Ça ne colle pas : on déclae
|      |      | T     | G    | C    | G    |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
| ---- | ---- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| C    | T    | **A** | T    | G    | T    | G    | C    | T    | G    | C    | A    | A    | C    | C    | T    | T    | G    | C    | ...  |

Ça ne colle pas donc on décalle le motif à droite sur le brin et on recommence !


|      |      |      | T    | G    | C    | G    |      |      |      |      |      |      |      |      |      |      |      |      |      |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| C    | T    | A    | T    | G    | T    | G    | C    | T    | G    | C    | A    | A    | C    | C    | T    | T    | G    | C    | ...  |

* et ainsi de suite jusqu'à trouver une occurence ou qu'il ne reste plus assez de caractères dans la chaine pour décaler le motif.



### Implémentation



> Recopier et compléter la fonction de recherche ci-dessous en suivant le procédé décrit juste au dessus :
>
> ```python
> def naif(motif, chaine) :
>  '''
>  renvoie la position de la première occurence du motif dans la chaine, s'il y en a une et -1 sinon.
>  : params
>  	motif (str)
>  	chaine (str)
>  : return (int) 
>  >>> recherche_naive('AB', 'CBCABABC')
>  5
>  >>> recherche_naive('AD', 'CBCABABC')
>  -1
>  '''
> 
> ```



### Complexité

Soit N le nombre de caractères de la chaine et n le nombre de caractères du motif. 

* Le nombre maximum de fois qu'on peut répéter le motif dans la chaine sera _N  - n_ car ensuite, il n'y a plus assez de caractères dans la chaine  pour y inclure le motif.
* A chaque position possible du motif, il faudra, au maximum, vérifier les _n_ caractères du motif. 

Ainsi, le nombre maximum de comparaisons est _(N - n) x n_ : la compléxité est de l'ordre de l'ordre de _Nn_.

Ce n'est pas si mal... 

Cependant, lorsqu'on s'aperçoit que wikipedia peut être vue comme une chaine de 2,7 milliards de caractères et que ce n'est qu'une petite partie de ce qu'on trouve sur le web, on se dit que ça vaut le coup d'améliorer cela car, tout de même, 27 miliards de comparaisons pour rechercher un motif de 10 caractères, ça fait tout de même beaucoup !





## Algorithme de Boyer-Moore (1980)

### Principes

L'algorithme de Boyer-Moore repose sur deux idées principales :

* La première consiste à comparer le motif est la chaine de gauche à droite, c'est à dire depuis la fin du motif et non le début
* La seconde consiste à opérer des décalages dont la taille varie  selon l'endroit où une non correspondance est remarquée.



Pour calculer le décalage a effectuer on procède ainsi :

* Si le caractère de la chaine n'est pas présent dans le motif, on décale le motif à droite de cette lettre.
* Sinon, si possible,on décale le motif de façon à faire correspondre la dernière lettre de la chaine comparée à la prochaine lettre identique dans le motif. Si ce n'est pas possible, on décale de 1.



Voici un exemple  pour rechercher e motif “TTAGT" dans une grande chaine d'ADN.

* On place les deux chaines l'une au dessus de l'autre mais on commence par la fin du motif :

![](media/tab1.png)

* le C et le T ne correspondent pas  et il n'y a pas de C dans le motif : on déclae le motif en le plaçant après le C	

![](media/tab2.png)

* Le G et le T ne correspondent pas : on décale le motif pour aligner les G

![](media/tab3.png)

* T et G correspondent mais pas G et A. On ne peut pas aligner le motif sur un G donc on décale de 1.

![](media/tab4.png)

* Le T et le A ne correspondent pas donc on décale le motif pour aligner les A

![](media/tab5.png)

* Les T correspondent mais pas T et G : on décale le motif pour aligner les T

![](media/tab6.png)

* G et T ne correspondent pas : on décale lemotif pour aligner les G

![](media/tab7.png)

* motif et chaine correpondent : on a trouvé une occurence du motif dans la chaine. C'était moins une : on ne pouvait plus décaler ensuite.



Ca va plus vite puisqu'on ne décale plus forcément de 1 en 1.

Mais , pour pouvoir calculer au mieux le décalage à effectuer, il faut pré-traiter le motif.

### Pré-traitement du motif

On va construire un tableau appelé **table des sauts**.

Ce tableau donne, pour chaque caractère du motif, l’écart minimal entre ce caractère et la fin du motif, c'est à dire le nombre de lettre après lui.

Attention : La dernière lettre du mot est traitée à part, elle renvoie un écart maximal si elle n’est pas présente ailleurs dans le mot.

Pour TTAGT, le T final est ignoré et ce tableau sera :

| Caractère |  T   |  G   |  A   | autres caractères |
| --------- | :--: | :--: | :--: | :---------------: |
| Écart     |  3   |  1   |  2   |         5         |

Un fois la table des sauts connues, lorsqu'une correspondance est mauvaise,on effectue la soustraction "valeur de la table de sauts correspondante à la lettre lue de la chaine" moins "l'indice du caractère du motif en cours en partant de la gauche" et :

* si le résultat est positif, il s'agit du décalage

* sinon on décale de 1

    

Reprenons l'exemple :

* On place les deux chaines l'une au dessus de l'autre mais on commence par la fin du motif :

![](media/tab1.png)

* le C de la chaine correspond à la valeur 5 et on lit le caractère d'indice 0 (en partant de la gauche) : on décale de 5 - 0 = 5

![](media/tab2.png)

* Le G de la chaine correspond à 1 et on lit le caractère d'indice 0 (en partant de la gauche) : on décale de 1 - 0 = 0

![](media/tab3.png)

* Le G de la chaine correspond à 1 et on lit le caractère d'indice  2 (en partant de la gauche) : on décale de 1 - 2 = -1 : IMPOSSIBLE donc on décale de 1.

![](media/tab4.png)

* Le A de la chaine correspond à 2 et on lit le caractère d'indice 0 (en partant de la gauche) : on décale de 2 - 0 = 2

![](media/tab5.png)

* Le T de la chaine correspond à 3 et on lit le caractère d'indice 1 (en partant de la gauche) : on décale de 3 - 1 = 2

![](media/tab6.png)

* Le G de la chaine correspond à 1 et on lit le caractère d'indice 0 (en partant de la gauche) : on décale de 1 - 0 = 1

![](media/tab7.png)

* motif et chaine correpondent : on a trouvé une occurence du motif dans la chaine. C'était moins une : on ne pouvait plus décaler ensuite.



### Impémentation

Recopier et compléter le code ci-dessous :

```python
def table(motif):
    '''
    renvoie la table de saut du motif
    : param motif (str)
    : return (dic)
    >>> table('TTAGT') = {'T' : 3, 'G' : 1, 'A' : 1}
    True
    >>> table('TTAGC') = {'T' : 3, 'G' : 1, 'A' : 1}
    '''
    dico = {}
    
    
def boyer_moore(motif, chaine):
    '''
    renvoie la première occurence du motif dans la chaine s'il y est présent et -1 sinon
    params :
    	motif (str)
    	chaine(str)
    return (int)
    >>> boyer_moore('TTAGC', 'TAGTCCCCGGTATTAGT')
    13
    >>> boyer_moore('TTAGA', 'TAGTCCCCGGTATTAGT')
	-1
	'''
    
```



____________

Par Mieszczak Christophe

Licence CC BY SA

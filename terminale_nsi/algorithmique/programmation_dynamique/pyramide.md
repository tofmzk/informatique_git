# Le problème de la pyramide

## Le principe



Une pyramide de hauteur _n_ est un graphe particulier dont les noeud contiennent des entiers.

Voici une pyramide de hauteur 1 :

<img src="media/pyramide1.png" style="zoom:50%;" />

Voici une pyramide de hauteur 2 :

<img src="media/pyramide2.png" style="zoom:50%;" />



Le jeu consiste la plus grande somme obtenable en partant de la racine vers une feuille et en ajoutant tous les valeurs des noeuds traversés.

## Méthode exhaustive

On peu imaginer une solution naïve pour résoudre ce problème : parcourir tous les chemins possibles et choisir la plus grande somme obtenue.

Avec une pyramide de hauteur 1, c'est facile :Il n'y a que deux chemins : l'un a une somme de 10, l'autre de 14 : ce dernier est le vainqueur !

<img src="media/pyramide1.png" style="zoom:50%;" />

Avec une pyramide de hauteur 2, c'est plus long... Allez-y !

<img src="media/pyramide2.png" style="zoom:50%;" />

Avec une pyramide plus grande cela devient vite compliquée :

<img src="media/pyramide3.png" style="zoom:50%;" />

 

> * combien y-a-t-il de chemin avec la pyramide précédente ?
> * Si la pyramide avait une hauteur _n_, quel serait le nombre de chemins à explorer ?  
>
>   



Le problème de la pyramide a une complexité exponentielle : on ne peut pas le résoudre ainsi dès que _n_ devient grand.



## Méthode Gloutonne



Nous avons étudié en première la méthode gloutone qui à chaque étape de la résolution, choisit la meilleure valeur possible pour cette étape, c'est à dire un maximum local.

En appliquant une méthode gloutonne, trouver une solution à la pyramide de hauteur 4 :

<img src="media/pyramide3.png" style="zoom:50%;" />

Trouve-t-on forcément la meilleure réponse ?



SPOILER plus bas

|

|

|

|

|

Ben non : 

![](https://upload.wikimedia.org/wikipedia/commons/8/8c/Greedy-search-path-example.gif)

## Formule récursive

Nous allons appliquer ici la méthode de programmation dynamique : traitér de petits problèmes en mémorisant les résultats et s'en servir pour résoudre les problèmes plus grand sans redondance de calcul.



Nous allons modéliser les deux pyramides de taille 2 et 4 vue plus haut ainsi :

```python
pyr2 = [   [8], [2, 6],  [3, 4, 6] ]
pyr4 = [   [8], [2, 6],  [3, 4, 6], [9, 6, 4, 4], [1, 5, 9, 5, 7] ]
```

Nous allons coder une fonction récursive qui calcule la somme maximum calculable dans la pyramide aux coordonnées (i,j)

Pour cela il y a deux cas :

* si on est au bas de la pyramide, la somme est la valeur du noeud
* sinon on ajoute la valeur du noeud avec ceux des deux noeuds du dessous.

La formule récursive est celle-ci :

$`somme(pyr, i,j) = \left\{ \begin{array}{ll}     pyr[j][i] {~si~} j = len(pyr) - 1\\      pyr[j][i] + max(somme(pyr, i, j + 1), somme(pyr, i + 1, j + 1){~sinon.} \end{array}\right.`$



> Codez la fonction récurive `somme(pyr, i, j)` correspondant à la définition ci-dessus et testez la avec les pyramides _pyr2_ et _pyr4_.



## Méthode dynamique

Le problème de notre formule récursive est que l'on va calculer plusieurs fois les mêmes choses à l'image de la suite de Fibonacci.

on va donc utiliser une structure pour stocker les valeurs des problèmes intermédiaires. Ici un dictionnaire est bien adapté : on va faire correspondre à chaque coordonnée sa somme maximale.

Copiez et complétez le code ci-dessous  :

```python
def somme_dyna(pyr, i, j , mem = {}):
    if j == len(pyr) - 1 : # si on est en bas de la pyramide
        mem[(i,j)] = ... 
        return mem[(i,j)]
    elif (i,j) in mem : #si on a déjà calculé la somme
        return ...
    else :
        mem[(i,j)] = pyr[j][i] + max(..., ...) # on mémorise la nouvelle somme
        return ...
```



Pour se convaincre de l'amélioration qu'apporte la mémoïsation, on va compter le nombre d'appels récursifs pour chacune des fonctions :



```python
n1 = 0
n2 = 0

def somme(pyr, i, j) :
    global n1
    n1+=1
    ...
   
def somme_dyna(pyr, i, j, mem = {}) :
    global n2
    n2+=1
    ...
```

> Comparez _n1_ et _n2_ avec les pyramides _pyr2_ et _pyr4_.



______

Par Mieszczak Christophe

Licence CC BY SA


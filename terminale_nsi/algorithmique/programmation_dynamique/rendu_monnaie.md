# Rendu de la monnaie



## Méthode gloutonne

Ce grand classique de l'algorithmique a déjà été étudié en première dans le chapitre des algorithmes gloutons : Il s'agit de rembourser une somme d'argent à l'aide d'un certain nombre de montants à disposition.

Nous avons utiliser une méthode gloutonne qui opte toujours pour le plus grand montant possible à chaque étape car le nombre de possibilités est trop important pour toutes les étudier.



**Exemple** :

Vous disposez des montants 20€, 10€, 5€, 2€ et 1€ pour rembourser 34€ comment procédez vous ?



**Exemple ** :

Vous disposez cette fois des montants 60£, 30£, 24£, 12£, 6£, 2£ et 1£ et vous devez payer 48£.

* Appliquez à nouveau la méthode gloutonne : qule sera le paiement proposé ?
* Est-ce optimal ?



## Algorithme récursif _naïf_

Considérons une somme entière _s_ à rembourser en utilisant un système monaitaire contennt les _pièces_ $p_0, p _1, ..., p_{n-1}$ avec $p_0= 1$ afin d'être certain que le rendu exact soit possible.

L'algorithme récursif _naïf_ permettant de calculer le nombre minimum de _pièces_ nécessaires au rendu de la monnaie est le suivant :

$`nb\_piece(s) = \left\{ \begin{array}{ll}     0 ~si~ s = 0    \\ 1 + \underset{p: 0 \rightarrow n -1}{min}(nb\_pieces(s - p)) ~sinon\end{array}\right.`$



Regardons et étudions la fonction ci-dessous :

 ```python
 def nb_pieces(somme, systeme):
     '''
     renvoie le nbre minimum de pièces à rendre pour rembourser la somme
     : params 
         somme : int >=0
         system (list) un tableau des pièces diposibles avec systeme[0] = 
     : return (int) le nbre de pièces à rendre au minimum>     '''
     if somme == 0 :# cs d'arrêt
         return 0
     else : #cas récursif
         nb = somme #au pire en rembourse 1 par 1 
         for piece in systeme : # pour chaque pièce
             if piece <= somme : #inférieure à la somme à rendre
                 nb = min(nb ,1+ nb_pieces(somme - piece, systeme))
         return nb
 ```

 Testez :

```python
>>> nb_pieces(48,[60, 30, 24, 12, 6])
```

On obtient bien le nombre de pièces minimales à rendre, contrairement à l'algorithme glouton qui en choissit 3 (cf plus haut)... **Mais le coût de cet algorithme est très supérieur à celui de notre algorithme glouton !"**

Testez :

```python
>>> nb_pieces(100,[1, 2])
```

Le résultat devrait être trivial miais ça coince ... Regardons cela de plus près :

* pour rendre 100, l'algo va prendre le minimum entre le nombre de pièces pour rendre **99** et pour rendre **98** :
    * Pour rendre **98** l'algo va prendre le minimum entre le nombre de pièces pour rendre **97** et **96**
        * pour rendre **97**  l'algo va prendre le minimum entre le nombre de pièces pour rendre **96** et **95**
        * pour rendre **96**  l'algo va prendre le minimum entre le nombre de pièces pour rendre **95** et **95**
    * Pour rendre **99** l'algo va prendre le minimum entre le nombre de pièces pour rendre **98** et **97**
        * pour rendre **98**  l'algo va prendre le minimum entre le nombre de pièces pour rendre **97** et **96**
        * pour rendre **97**  l'algo va prendre le minimum entre le nombre de pièces pour rendre **96** et **95**



Nous ne sommes qu'au début et on voit déjà que l'algorithme va de nombreuses fois calculer la même chose : ces redondances conduisent à un coût trop important pour que la fonction puisse se terminer en un temps raisonnable.

Pour la mếmé raison, l'appel `nb_pieces(48,[60, 30, 24, 12, 6, 2, 1])` ne se terminera pas !

## Mémoïsation

Comme pour le problème des Pyramides et la suite de Fibonacci, on a un algorithme récursif qui décomposele gros problème en sous-problèmes redondants : on va utiliser une structure de mémoïsation pour garder en mémoire la résolution des sous_problèmes au fur et à mesure de leur calcul.

Un tableau est parfait : 

* on sait qu'il y aura au maximum _somme _ pièces de 1 dont _somme_ - 1 élement au tableau
* pour rendre 0 on a besoin de 0 pièces

Le tableau `nb = [0] * (somme + 1)` contient les éléments d'indices 0 à _somme_ :

	* `nb[0]` correspond à la bonne réponse pour _somme_ = 0 
	*  `nb[somme]` sera le nombre de pièces à rendre pour la _somme_.

Copiiez-collez puis omplétez la fonction `nb_pièces_dyna` ci-dessous :

```python
def nb_pieces_dyna(somme, systeme):
    nb = [0] * (somme + 1) # on prépare la structure de mémoïsation
    return dyna(somme, systeme,nb) # appel de la fonction récursive

def dyna(somme, systeme, nb):
    '''
    renvoie le nbre minimum de pièces à rendre pour rembourser la somme
    : params 
        somme : int >=0
        system (list) un tableau des pièces diposibles avec systeme[0]
        nb (list) tableau de (somme + 1) elt valant 0 lors de l'appel initial
    : return (int) le nbre de pièces à rendre au minimum
    '''
    if somme <= 1 : #si on doit rendre 0 ou 1
        nb[somme] = ...
        return ...
    elif nb[somme] != 0 : # si on a déjà calculé nb[somme]
        return ...
    else : # sinon on va le calculer
        nb[somme] = ... # au pire, on utilise somme pièces de 1
        for piece in systeme : # pour chaque pièce possible
            if piece <= somme : #si la pice est inférieure à la somme à rendre
                nb[somme] = min(nb[somme], 1 + dyna(...) # on calcule récursivement nb[somme] est on le mémoïse
        return ... # on renvoie le résultat
```

Testez à nouveau :

```python
>>> nb_pieces_dyna(100,[1, 2])
???
>>> nb_pieces_dyna(48,[60, 30, 24, 12, 6, 2, 1])
???
```



## Impératif



En regardant bien notre algorithme, on s'aperçoit qu'on peut le coder de façon impérative. Il suffit pour cela de faire une boucle qui calcule dans l'ordre le nombre minimum de pièces nécessaires pour rendre tous les montants de 0 à _somme_ en utili sant les sommes déjà calculées avant dansla boucle.

```python
def nb_pieces_dyna_imperatif(somme, systeme) :
    '''
    renvoie le nbre minimum de pièces à rendre pour rembourser la somme
    : params 
        somme : int >=0
        system (list) un tableau des pièces diposibles avec systeme[0]
    : return (list) le tableau du nbre de pièces à rendre au minimum pour tous les montants de 0 à somme inclus
    '''
    nb = [0] * (somme + 1)
    for montant in range(0, somme + 1) :
        if montant <= 1 :
            nb[montant] = ...
        else :
            nb[montant] = montant
            for piece in systeme :
                if piece <= montant and nb[montant - piece] < nb[montant] :
                    nb[montant] = ...
    return nb
```



## Solution minimale

Il faut maintenant obtenir le tableau des pièces à rendre en plus de leur nombre.

Pour cela on va ajouter une structure de mémoïsation _tab_, un tableau de tableaux :

* tab[0] sera un tableau du minimum de pièces nécessaires pour rendre 0 
* tab[1] sera un tableau du minimum de pièces nécessaires pour rendre 1
* tab[2] sera un tableau du minimum de pièces nécessaires pour rendre 2
* etc ...

**Au départ,_tab_ doit donc contenir _somme +1_ tableaux vide**



lorsqu'une pièce est choisie dans la boucle de la fonction précédente pour payer un _montant_, il faut alors que _tab_ contienne tous les élément de _tab[montant - piece]_ puis y ajouter cette dernière _piece_.



copiez et complétez le code ci-dessous :

```python
def rendu(somme, systeme) :
    '''
    renvoie le nbre minimum de pièces à rendre pour rembourser la somme
    : params 
        somme : int >=0
        system (list) un tableau des pièces diposibles avec systeme[0]
    : return (tuple) (le tableau du nbre de pièces à rendre au minimum pour tous les montants de 0 à somme inclus, le tableau des pièces utilisées)
    '''
    nb = [0] * (somme +1)
    tab = ... #initialisation du tableau de pièces utilisées
    for montant in range(0, somme + 1) :
        nb[montant] = montant
        tab[montant] = ... # calcule des pièces utilisées au pire (montant pieces de 1)
        for piece in systeme :
            if piece <= montant and nb[montant - piece] < nb[montant]:
                nb[montant] = 1 + nb[montant - piece]
                tab[montant] = tab[montant - piece][:] #on copie tab[montant-piece]
                tab[montant]. append(piece)#on y ajoute la dernière piece
	return nb, tab
```



Testez :

```python
>>> rendu(36, [1, 2, 5, 10, 20])
???
>>> rendu(48,[60, 30, 24, 12, 6, 2, 1])
???
```



______

Par Mieszczak Christophe licence CC BY SA






# Introduction : la suite de Fibonacci

## Une fonction récursive

Nous avons déjà parlé de cette suite dont voici la définition récursive : 

$`U_n = \left\{ \begin{array}{ll}     1 ~si~ n = 0    \\ 1 ~si~n=1 \\ U_{n-1} + U_{n-2} ~sinon \end{array}\right.`$

>   
>
> * Codez la fonction `fibo(n)` qui renvoie la valeur du terme de rang _n_ en suivant cette définition
>
> * Calculer les termes de rang 10, 20, 40, 60.
>
> 

## Des calculs redondants

Lenteur de cette fonction s'explique par les calculs redondants, c'est à dire des calculs qu'on effectue plusieurs fois inutilement.

Prenons, par exemple le calcul du terme de rang 5 :

<img src="media/fibo.png" style="zoom: 67%;" />

on calcule ici plusieurs fois la même chose : 

<img src="media/fibo2.png" style="zoom:50%;" />

Ces redondances vont rapidement rendre le calcul impossible à réaliser dans un temps raisonnable. Il nous faut une solution !

## La mémoïsation

La programmatoin dynamique a été introduite au début des années 1950 par [Richard  Bellman](https://fr.wikipedia.org/wiki/Richard_Bellman) dans le cadre de travaux en mathématiques appliquées. Il  est important de bien comprendre que ”programmation” dans ”programmation dynamique”,  ne doit pas s’entendre comme "utilisation d’un langage  de programmation", mais comme synonyme de planification et ordonnancement.



AU départ, on  a tout un algorithme récursif dont le problème est la redondance de calculs. On va alors procéder ainsi :

* on va calculer les sous-problèmes et les mémoriser sans une structure de données adéquate (souvent un tableau) : c'est la **mémoïsation**, terme qui vient d'un anglicisme.

* On va calculer les problèmes de plus en plus grands en utilisant les valeurs _mémoïsées_ ce qui va nous permettre d'en _mémoïser_ de nouvelles.

    

Ainsi, on évite d'effectuer plusieurs fois les mêmes calculs mais, en contrepartie, on augmente la quantité de mémoire nécessaire au fonctionnement de l'algorithme. Est-ce intéressant ? SPOLER --> OUI !

## Programmons dynamiquement

Appliquons la méthode à notre suite de Fibonacci.

Recopier et complétez ce code :

```python
def fibo(n) :
    '''
    renvoie la valeur du terme de rang n
    : param (n) int >=0
    : return (int)
    '''
    assert isinstance(n, int) and n >=0, 'le paramètre doit être entier positif'
    mem = [1, 1] # prépare la structure de mémoïsation
    return fibo_dynamique(n, mem)

def fibo_dynamique(n, mem):
    '''
    renvoie la valeur du terme de rang n en utilisant la programmation dynamique
    : param (n) int >=0
    : return (int)
    '''
    if n < len(mem): #si Un a déjà été calculée
        return ...
    else :
        mem.append(...) #calcule la nouvelle valeur et la mémoïse
        return ...
    
```



Est-ce efficace ?

Calculez les termes de rang 50, 60, 100...



Ca va mieux non ??



Cette technique est employée dans de nombreux problèmes d'optimisation comme nous le verrons bientôt.

___________

Par Mieszczak Christophe

Licence CC BY SA




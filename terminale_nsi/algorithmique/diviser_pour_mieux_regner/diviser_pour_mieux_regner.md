# Diviser pour mieux régner



Le principe du diviser pour mieux régner est de découper un problème en sous-problèmes plus simples que l'on résout séparément, puis que l'on recombine afin de résoudre finalement le problème global.



Cette technique fournit des techniques très efficaces pour de nombreux problèmes comme la recherche dichotomique ou le tri fusion ou les tour de Hanoï que nous allons étudier ici.



## Dichotomie





### Recherche dichotomique



Nous avons étudié en première la recherche dichotomique d'un élément ans une liste ordonnée afin de savoir si l'élément est présent dans la liste ou non. 

* rappelez-en le principe.

* écrivez une définition récursive de la dichotomie dont le résultat attendu est la présence ou non de l'élément dans la liste.

* implantez cette définition en Python.  Nous utiliserons ici une liste d'entiers ou de chaînes de caractères.

  ​    

```python
def comp_defaut(elt1, elt2):
    '''
    compare les elt1 et elt2
    pram elt1, elt2 deux elements de type int ou str et renvoie -1 si elt1<elt2, 1 si elt1>elt2 et 0 sinon
    type int ou str
    : return
    type int 
    '''
    if elt1 < elt2 :
        return -1
    elif elt1 > elt2 :
        return 1
    else :
        return 0

def dichotomie(element, liste, comp = comp_defaut) :
    '''
    renvoie True si elt est dans la liste et false sinon
    : param element
    type depend de la liste
    : param liste une liste ordonnée
    type liste
    : param comp
    type function
    : return 
    type boolean
    
    '''
    assert(isinstance(liste, list),'liste est une liste !'
    
```



## Le tri rapide



Le **tri rapide** ou **tri pivot** (en anglais *quicksort*) est un algorithme de tri inventé par [C.A.R. Hoare](https://fr.wikipedia.org/wiki/C.A.R._Hoare) en 1961 et fondé sur la méthode de conception diviser pour régner. 

La méthode consiste à placer un élément du tableau (appelé pivot) à  sa place définitive, en permutant tous les éléments de telle sorte que  tous ceux qui sont inférieurs au pivot soient à sa gauche et que tous  ceux qui sont supérieurs au pivot soient à sa droite.





Cette opération s'appelle le partitionnement. Pour chacun des  sous-tableaux, on définit un nouveau pivot et on répète l'opération de  partitionnement. Ce processus est répété récursivement, jusqu'à ce que  l'ensemble des éléments soit trié.

![tri rapide (quicksort)](https://upload.wikimedia.org/wikipedia/commons/6/6a/Sorting_quicksort_anim.gif)









## Le tri fusion











_________________

Par Mieszczak Christophe, licence CC-BY-SA



sources images wikipédia:

* [Tri rapide - CC BY SA par RolandH](https://commons.wikimedia.org/wiki/File:Sorting_quicksort_anim.gif)

    
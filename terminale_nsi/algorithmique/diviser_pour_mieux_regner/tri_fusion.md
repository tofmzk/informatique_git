# Divisier pour mieur régner : le tri Fusion



## Diviser pour mieux régner

On utilise la méthode [diviser pour mieux régner](https://fr.wikipedia.org/wiki/Diviser_pour_r%C3%A9gner_(informatique)) qui consiste en trois points :

1. **Diviser** : découper un problème initial en sous-problèmes ;
2. **Régner** : résoudre les sous-problèmes (récursivement ou directement s'ils sont assez petits) ;
3. **Combiner** : calculer une solution au problème initial à partir des solutions des sous-problèmes.



Le tri fusion est récursif et se déroule ainsi :

1. Si le tableau n'a qu'un élément, il est déjà trié.
2. Sinon, séparer le tableau en deux parties à peu près égales : c'est la phase **diviser**.
3. Trier récursivement les deux parties avec l'algorithme du tri fusion : c'est la phase **régner**
4. Fusionner les deux tableaux triés en un seul tableau trié : c'est la phase **fusionner**.



Voici un exemple statique du tri fusion où on peut repérer chaque étape :

![statique](https://upload.wikimedia.org/wikipedia/commons/6/60/Mergesort_algorithm_diagram.png)



En voici un petite animation en bonus :

![](https://upload.wikimedia.org/wikipedia/commons/c/cc/Merge-sort-example-300px.gif)

## Pratiquons un peu

On distribue quelques cartes d'un jeu de cartes et on applique le tri fusion.



## Implémentation

Comme pour les autres tris, le tri fusion utilise un comparateur.

Il utilise deux fonctions :

* `fusion(tab1, tab2)` qui fusionne les deux tableaux ordonnés en un seul tableau ordonné qu'il renvoie.

* `trifusion(tab, comp = comp_defaut)` qui est récursive et suit la formule suivante :

    $`trifusion(tab) = \left\{ \begin{array}{ll}     tab {~si~} len(tab) <2\\     fusion(trifusion(moitiée~gauche~de~tab), trifusion(moitiée~droite~de~tab))~sinon \end{array}\right.`$

    



Complétez ces deux fonctions ci-dessous en commençant par `fusion` qui est plus abordable :

```python
def comp_defaut(elt1, elt2):
    '''
    renvoie -1 si elt1 < elt2, 1 si elt1 > elt 2 et 0 si elt1 == elt2
    : params elt1, elt2 (??)
    : return (int)   
    >>> comp_defaut(1, 3)
    -1
    >>> comp_defaut(3, 1)
    1
    >>> comp_defaut(1, 1)
    0
    '''
    if elt1 < elt2 :
        rep = -1
    elif elt1 > elt2 :
        rep = 1
    else:
        rep = 0
    return rep


def fusion(tab_gauche, tab_droit, comp = comp_defaut):
    '''
    renvoie un tableau résultant de la fusion des deux tabs triés   
    : params
        tab_gauche (list) un tableau trié
        tab_droit (list) un tableau trié
    : return (list) le tableau fusionné trié    
    >>> fusion([4], [3])
    [3, 4]
    >>> fusion([1, 3], [2, 7])
    [1, 2, 3, 7]
    >>> fusion([1, 3], [2, 4, 9])
    [1, 2, 3, 4, 9]
    '''
    tab_fusion = []
    i_gauche = 0
    i_droit = 0
    while i_gauche < len(tab_gauche) and i_droit < len(tab_droit) :
    
    
    
def tri_fusion(tab,comp = comp_defaut):
    '''
    renvoie le tableau trié
    : param tab(list)
    : resturn (list)
    >>> tri_fusion([1, 4, 7, 8, 2, 0])
    [0, 1, 2, 4, 7, 8]
    '''
    
    


if __name__ == '__main__' :    
    import doctest
    doctest.testmod(verbose = True)

```

## Complexité

Côté complexité, nous avons déjà étudié en première.

* le tri selection dont la complexité est quadratique (_o(n²)_) dans tous les cas
* le tri insertion dont la complexité est linéaire (_o(n)_) au mieux et quadratique au pire.
* La dichotomie dont la compléxité est logarithmique (_o(log(n)_).

Le tri fusion a une complexité quasi-linéaire, c'est à dire o(_nlog(n)_) dans le meilleur cas, le pire des cas et en moyenne.

![](media/complexite.png)







______________

Par Mieszczak Christophe

Licence CC BY SA

sources : 

* [arbre - wikipédia](https://commons.wikimedia.org/wiki/File:Mergesort_algorithm_diagram.png)
* [animation - wikipédia](https://commons.wikimedia.org/wiki/File:Merge-sort-example-300px.gif)
* graphe : production personnelle
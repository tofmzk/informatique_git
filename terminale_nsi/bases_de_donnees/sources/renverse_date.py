def lecture(nom_fichier):
    filin = open(nom_fichier, "r")
    entete=filin.readline()    
    lignes=[ s.rstrip() for s in filin.readlines()]
    filin.close()
    for i in range(len(lignes)) :
        lignes[i] = lignes[i].split(',')
    return lignes

def sauvegarder(nom_fichier, lignes):
    filin = open(nom_fichier, 'w', encoding = 'utf-8')
    lignes_bis = ['id, prenom, nom, classe, date_naissance\n']
    for i in range(len(lignes)):
        ligne = ''
        for elt in lignes[i]:
            ligne = ligne + elt + ','
        lignes_bis.append(ligne[:-1])

 
    filin.writelines(lignes_bis)
    filin.close()

def renverser_date(nom_fichier):
    lignes = lecture(nom_fichier)
    for i in range(len(lignes)):
        jour = lignes[i][4][:2]
        mois = lignes[i][4][3:5]
        annee = lignes[i][4][6:]
        lignes[i][4] = annee + ':' + mois + ':' + jour +'\n'
    sauvegarder('result.csv', lignes)
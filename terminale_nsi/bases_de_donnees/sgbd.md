# SGBD



## Introduction

En première, nous avons travailler sur des données structurées stockées dans des fichiers au format CSV.  Cette technique a l'avantage d'être simple et pratique mais présente des limites :

*  sécurité et confidentialité inexistantes
*  difficultés pour partager le fichier entre plusieurs utilisateurs en lecture et écriture.
*  difficultés à lier plusieurs table _csv_ entre elles.



L'apparition de systèmes de stockage performant, avec l'arrivée des disques durs à la fin des années 50, a permis l'accumulation et la manipulation de données toujours plus nombreuses. Le terme _base de données_ fait alors son apparition dans les années 60.

En 1970, [Franck Codd](https://fr.wikipedia.org/wiki/Edgar_Frank_Codd), qui travaille chez IBM, propose un modèle logique pour gérer le partage de gros volumes de données ... mais le premier système ne verra le jour que 10 ans plus tard avec la société Oracle.

Depuis l'apparition du web, en 1990, des réseaux sociaux (2002 - 2003), des objets connectés [...] , la quantité de données à stocker, traiter et sécuriser n'a cessé de s'accroître et de jouer un rôle de plus en plus important dans un monde de plus en plus numérisé ... et réglementé : des textes de loi, comme le célèbre [RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es), imposant confidentialité et sécurisation des données.





## `S`ystème de `G`estion de `B`ases de `D`onnées



Dans une base de données, l'information est stockée dans des  fichiers, mais à la différence de ceux au format _csv_, il n'est pas  possible de travailler sur ces données avec un simple éditeur de texte. 

Pour manipuler les données présentes dans une base de données  (écrire, lire ou encore modifier), il est nécessaire d'utiliser un type de logiciel appelé  "**S**ystème de **G**estion de **B**ase de **D**onnées" très souvent abrégé en **SGBD**.

 Il existe une multitude de SGBD : des gratuites, des payantes, des  libres ou bien encore des propriétaires. Par exemple, [SQLite](https://sqlite.org/) que nous allons utiliser, est un SGBD dont le code source est dans le domaine public.



Voici les différents services accomplis par les SGBD :	

- **Gestion de la lecture, l'écriture ou la modification des informations** contenues dans une base de données.	

- **Gestion des autorisations d'accès à une base de données**. Il est en effet souvent nécessaire de contrôler les accès par  exemple en permettant à l'utilisateur A de lire et d'écrire dans la base de  données alors que l'utilisateur B aura uniquement la possibilité de lire les informations contenues dans cette même base de données. 	

- **Gestion d'accès multiples et simultanés**. Plusieurs personnes peuvent avoir besoin d'accéder aux informations  contenues dans une base données en même temps. Cela peut parfois poser  problème, notamment si les 2 personnes désirent modifier la même donnée au même moment  (on parle d'accès concurrent). 	

- **Maintenance des données**. Les fichiers des bases de données sont stockés sur des disques durs  dans des ordinateurs, ces ordinateurs peuvent subir des pannes. Il est  souvent nécessaire que l'accès aux informations contenues dans une base de données soit maintenu, même en cas de panne matérielle.  Les bases de données sont donc dupliquées sur plusieurs ordinateurs afin qu'en cas de panne d'un ordinateur A, un ordinateur B contenant une copie de la base de données présente dans A, puisse  prendre le relais. Tout cela est très complexe à gérer, en effet toute  modification de la base de données présente sur l'ordinateur A doit  entraîner la même modification  de la base de données présente sur l'ordinateur B. Cette  synchronisation entre A et B doit se faire le plus rapidement possible,  il est fondamental d'avoir des copies parfaitement identiques en  permanence. 

  ​    

Les SGBD jouent un rôle fondamental.  Leur utilisation explique en partie la supériorité de  l'utilisation des bases de données sur des solutions plus simples à  mettre en oeuvre, mais aussi beaucoup plus limitées comme les fichiers au format _csv_. 



________

Par Mieszczak Christophe

Licence CC BY SA
# Créer une base de données



## SQlite

![SQlite](https://sqlite.org/images/sqlite370_banner.gif)

Nous allons utiliser  [DB browser for SQLite](https://sqlite.org/) , un SGBD libre, gratuit et très répandu.



Il va nous permettre de créer, gérer et interroger une base de donnée. C'est la création qui nous intéresse ici. Nous allons créer une base de données contenant les relations _classe_, _profs_ et _eleves_ vues dans le cours _bases de données relationnels_.

Le langage utilisé est le **S**tructuel **Q**uery **L**angage (langage de requête structuré) . C'est un langage déclaratif :

- Il décrit le résultat voulu sans décrire la manière de l'obtenir
- Les SGBD déterminent automatiquement la manière optimale d'effectuer les opérations nécessaires à l'obtention du résultat



## "A la main"



### Créer une nouvelle base



Commençons par lancer notre SGBD : _DB Browser for SQLite_.



Cliquez sur `Nouvelle base de données` dans l'onglet du haut. :

* choisissez un nom pour votre base et sauvegardez là.

* Vous devriez maintenant voir la fenêtre suivante : 	

    ![create_table](media/create_table.jpg)



`CREATE TABLE` est une `requête` SQL pour créer une relation. Cliquer sur `Annuler` , oui c'est _un peu_ étrange, pour accéder à cette nouvelle base en fait, on annule la requête et on crée un base vide :

![base vide](media/base_vide.jpg)



Cliquez sur l'onglet `Exécuter SQL` pour accéder  à l'éditeur de SQL. Grâce à la requête`CREATE TABLE` , on va créer une relation correspondant à la relation _classes_.

Tapez la **requête** ci-dessous puis exécutez la en cliquant sur la flèche bleue à droite de l'icône _imprimante_.

```SQL
CREATE TABLE salles
(classe TEXT, salle INT);
```

_Remarque :_

* Notez bien les `domaines` de chaque attribut : 
    * _classe_ a pour domaine TEXT, c'est une chaine de caractères;
    * _salle_ a pour domaine INT : c'est un entier.
* nomtez le `;` à la fin de la requête.



Revenez dans l'onglet `Structure de la Base de Données` et retrouvez la relation _salles_ .

![relation salles](media/relation_salles.png)

### Insérer, supprimer ou modifier des données



Nous allons maintenant insérer les données ci-dessous dans la base :

| classe | salle |
| :----: | :---: |
|  TG1   |  201  |
|  TG2   |  204  |
|  TG3   |  108  |
|  TG4   |  21   |



Pour cela, retournons dans l'onglet `Exécuter le SQL`  et utilisons la requête `INSERT` (n'oubliez pas d'exécuter la requête en cliquant sur la flèche verte) :

```SQL
INSERT INTO salles
(classe, salle)
VALUES
('TG1', 201),
('TG2', 204),
('TG3', 108),
('TG4', 21);
```

_Remarques :_

* Notez la structure de la requête :

     `INSERT INTO` _nom de la table_

    _tuple d'attributs_

    `VALUES`

    _tuple  correspondant au premier enregistrement_ **,**

    _tuple  correspondant au deuxième enregistrement_ **,**

    ...

    _tuple  correspondant au dernier enregistrement_ **;**

    

* Les majuscules aux noms de requêtes et les tabulations sont facultatifs : ils sont seulement là pour une meilleure lecture du code.

* le `;` marque la fin de la requête

    

Allons maintenant dans l'onglet `Parcourir les données` pour visualiser cela :

![Parcourir les données](media/parcourir_donnees.png)



En cas d'erreur, on peut supprimer un enregistrement grâce à la requête :

```SQL
DELETE FROM nom_table
WHERE condition ;
```



>  
>
> Supprimer de la relation _salles_ l'enregistrement dont l'attribut `salle` prend la valeur  21.
>
>  



Retournez dans `Parcourir les données` pour constater la suppression. 



>  
>
> Bon, c'était juste pour l'exemple mais elle existe bel et bien la TG4 et elle a bien cours en salle 21.
>
> En utilisant la requête `INSERT` déjà vue plus haut, ajoutez l'enregistrement qui lui correspond dans notre table _salles_.
>
> 

La requête `UPDATE` permet de mettre à jour une table en modidiant certaines valeurs de ses attributs :

```SQL
UPDATE nom_table
SET nom_attribut = valeur
WHERE condition;
```



> 
>
> Changement de salle : la TG4 aura cours en 121.
>
> Modifiez l'attribut _salle_ de notre relation _salles_, pour que cette salle devienne 121 où elle est 21 .
>
> 



**ATTENTION : ** N'oublions pas d'enregistrer nos modifications en passant par le menu _Fichier/Enregistrer les modification_. 



Reste maintenant à insérer dans notre base les relations _profs_ et _eleves_. Comment ça vous avez la flemme ? OK, moi aussi. On va utiliser les tables _csv_ que vous avez dans le répertoire _sources_.





## A partir de tableau au format _csv_



### Importation



Dans le répertoire _sources_ vous trouverez les deux tables au format _csv_ qui vont nous servir à compléter notre base de données relationnelle.



Pour cela, suivez le menu _Fichiers/Importer/Table depuis un fichier CSV_  (vérifiez bien que _tous les fichiers_/_alle files_ est sélectionné en bas à droite) et choisissez _eleves.csv_

![choix du fichier csv](media/choix_fichier_csv.jpg)



Cochez la case `Nom des Col. en première ligne` ,pour nommer correctement les colonnes, notez le séparateur (ici une `,` mais ce n'est pas toujours le cas), l'encodage (`UTF-8` la plupart du temps, parfois `ISO`, faites attention !) et cliquez sur `OK`.

![importer une table csv](media/importer_csv.png)



La structure de la base de données a changée (notez les schémas relationnels) :

![, ](media/nouvelle_structure.png)

> 
>
> De la même façon, importez la table CSV _profs_.
>
> 



N'oubliez pas d'enregistrer nos modifications !!



### Domaines et Clés



Nous savons que les attributs ont un domaine particulier (TEXT, NUMERIC, INT, BLOB). Par défaut, le SGBD importe les tables avec tous les domaines en TEXT : il va nous falloir préciser tout cela.

De plus, chaque relation doit avoir une clé primaire et certaines relations peuvent avoir une clé étrangère qui référence une clé primaire d'une autre table.



Nous avons vu que :

* la relation _salles_ admet pour clé primaire l'attribut _classe_
  
* la relation _profs_ admet pour clé primaire l'es attribut _matiere_.
  
* la relation _eleves_ :
    * admet pour clé primaire l'attribut _id_ des élèves.
    * a deux clés étrangères :
        *  _classe_ qui référence la clé primaire _classe_ de la relation _classes_.
        * _specialite_ qui réfrence la clé primaire _matiere_ de la relation _profs_
    
    



Commençons par ajouter une clé primaire à la relation _salles_. Allez dans l'onglet `Structure de la Base de Données` et cliquez droit sur la relation puis suivez le menu _modifier_ .

Il suffit maintenant de cocher la case CP (clé primaire) de l'attribut _classe_.

![clé primaire de _classes_](media/cle_primaire_salles.png)



> Ajouter maintenant la clé primaire de la relation _profs_.



Allez de nouveau dans l'onglet `Structure de la Base de Données` et cliquez droit sur la table _eleves_. Il suffit alors de modifier ce qui doit l'être :

* ajout d'une clé primaire (**C**lé **P**rimaire ou **P**rimary**K**ey)
* ajout les deux clés étrangères(**C**lé **E**trangère ou **F**oreign**K**ey)
* modification du domaine de l'attribut _date_naissance_ en NUMERIC

![modification de la table eleves](media/modification_eleves.png)





Validez en cliquant sur `OK` puis retournez voir le schéma de relation de la relation _eleves_ qui intègre bien une clé primaire (PK) et une clé étrangère (FK).



**N'oubliez pas d'enregistrer les modifications de temps en temps !!**



## Contraintes liées aux clés

### Unicité



Nous avons vu qu'une clé primaire imposait une contrainte d'unicité : impossible d'insérer dans une table un élément déjà présent !

> 
>
> * Testons cela en essayant d'insérer une classe déjà présente dans la relation _classes_ 
>
>	 ```sql
>	INSERT INTO salles
> 	(classe, salle)
> 	VALUES
> 	('TG1', 214);
> 	```
>
> 
>
> * Quel est le message d'erreur renvoyé ?
>
> 	```sql
> 			
> 	```
>
> 



>  
>
>  * Quelle serait la contrainte d'unicité de la relation _eleves_ ? 
>
>      



### Intégrité



Nous avons vu que la clé étrangère était une contrainte d'intégrité : impossible d'utiliser, dans la clé étrangère, une valeur non présente dans la clé primaire qu'elle référence.



> * Testons cela en essayant d'insérer un éléve d'id 44 nommé Alain Posteur en 'TG5' et suivant la spécialité NSI :
>
>     ```sql
>                 
>     ```
>
>     
>
> * Quel est le message d'erreur renvoyé ?
>
>     ```sql
>                 
>     ```
>
>     
>
> 



## Autres contraintes



On décide qu'aucun attribut de peut-être nul : c'est la `contrainte de non nullité`. Pour cela, on retourne dans la fenêtre qui nous a permis de déclarer les clés primaires et étrangères et on coche les cases `NN` (NOT NULL ou NON NUL) qui correspondent à cette contrainte.





L'attribut `salle` de la relation _salles_ n'est pas utilisé comme clé primaire mais se doit d'être unique. Pour cela, on retourne dans la fenêtre qui nous a permis de déclarer les clés primaires et étrangères et on coche les cases `U` (UNIQUE ...)



> Rendez unique l'attribut `salle` de la relation _salles_ 









_______

Par Mieszczak Christophe

licence CC BY SA

source images : production personnelle


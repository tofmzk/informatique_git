# SQL et Python



Dans ce TP, vous allez découvrir comment interroger une base de données depuis un programme écrit en python. Dans ce cas, le SGBD est le serveur et le programme est le client. La plupart des langages (le cobol, le langage C, le langage java...) permettent d'utiliser des requêtes SQL pour importer des données enregistrées de façon persistante dans une base de données. On parle de *embedded SQL*. Dans notre cas, nous allons utiliser le langage **Python** et le SGBD **SQLite**.

Vous pouvez si nécessaire consulter la documentation en ligne de SQLite https://docs.python.org/3/library/sqlite3.html.



Vous pourrez tester les instructions en les programmant à l'aide de Thonny ou de l'éditeur de code de votre choix, et vérifier leur exécution en utilisant par exemple *DB Browser*.



## Connexion à la base depuis un programme

Pour interagir avec notre base de données, nous utiliserons le module  `sqlite3`.

Pour ce cours, nous allons créer un petit carnet d'adresse, croisé avec des données sur les villes de France.



- La méthode `connect` prend en paramètre le chemin vers la base de données et retourne un objet de type `Connection`. Si la base n'existe pas, cette instruction crée une base vide.

```python
>>> import sqlite3
>>> fichierDonnees = 'carnet_adresse.db'
>>> conn = sqlite3.connect(fichierDonnees)
   
```

- Il ne faut pas oublier de fermer la connexion à la **fin du programme**

```python
>>>	conn.close()
```

- Une fois la connexion établie, on peut créer un curseur (un objet de type `Cursor`)  par la méthode `cursor()` de `Connection`.

```python
>>> cur=conn.cursor()
```



## Instructions `create`, `update`, `delete`

- Un curseur dispose d'une méthode `execute()` qui prend en paramètre une chaîne de caractères représentant une requête SQL. Exemple : `CREATE`, `INSERT`, `UPDATE`.

```python
>>> cur.execute("CREATE TABLE personne (nom text primary key, prenom text, age int, ville text);")
>>> nom = 'Jesapél'
>>> prenom='Groot'
>>> age = 118
>>> ville = 'planetX'
>>> cur.execute("INSERT INTO personne VALUES (?, ?, ?, ?);", (nom, prenom, age, ville))
```

- La méthode  `commit()` de la classe `Connection` met à jour la base de données pour les autres utilisateurs. Sans appel à cette méthode, une autre connexion à la base de donnée ne verra pas vos modifications. Par exemple, les modifications ne seront pas visibles dans DB Browser.

```python
>>> conn.commit()
```

- L'instruction suivante est possible, mais déconseillée ([risque d'injection SQL](https://fr.wikipedia.org/wiki/Injection_SQL))

```python
>>> cur.execute("INSERT INTO personne VALUES ('Jesapél', 'Groot', 118, 'PanetX');")    
```

- On peut aussi insérer plusieurs enregistrements

```python
>>> liste_personnes = [('Yoda', 'Master', 645,'Dagobah'), ('Maurane', 'Bob', 77, 'Paris'), ('Beurk', 'James', 60 , 'USS_entreprise') ]
>>> cur.executemany("INSERT INTO personne VALUES (?, ?, ?, ?);", liste_personnes)   
```

- Modifier avec `UPDATE` .

```python
>>> nom1='Kirk'
>>> cur.execute("UPDATE personne SET nom = ? where nom = 'Beurk';", (nom1,))
```

 **Attention** : Un tuple d'un unique élément doit être écrit avec une virgule après cet élément.

* Pour supprimer avec `DELETE` :

```python
>>> cur.execute("DELETE FROM personne WHERE nom = ?;", (nom1,))
```

>  
>
> Remettez donc James Kirk dans notre base de données. 
>
>  



## Consultation des données : le `SELECT`

La méthode `execute()` du curseur permet également de faire des sélections de données. Dans ce cas, il faut récupérer le résultat de la requête pour pouvoir le traiter dans le programme. Chaque ligne de la table ou de la combinaison de tables interrogée(s) est représentée sous la forme d'un tuple.

-  On peut récupérer la "première" ligne (un seul tuple) avec la méthode `fetchone`. Une fois qu'un premier tuple est extrait, cette méthode renvoie le tuple suivant. Si toutes les lignes ont été lues ou que la requête ne renvoie pas de ligne, `None` est renvoyé.

```python
 >>>cur.execute("SELECT * FROM personne;")
>>> first_personne = cur.fetchone() # récupère la première personne
>>> first_personne
???
>>> first_personne[0], first_personne[1]
???
>>> second_personne = cur.fetchone() # récupère la personne suivante
>>> second_personne
???
```

* La requête peut bien sûr être plus complexe.

```python
>>> cur.execute("SELECT * FROM personne WHERE prenom=?;", ('Bob',))
>>> first_personne = cur.fetchone() # récupère la première personne dont le prénom est Bob
>>> first_personne
???
# ou en utilisant une variable
>>> prenom='Yoda'
>>> cur.execute("SELECT * FROM personne where prenom=?;", (prenom,))
>>> first_personne = cur.fetchone() # récupère la première personne dont le prénom est prenom
>>> first_personne
???
```

* On peut récupérer un nombre défini de résultats avec la méthode `fetchmany`. Le résultat est une liste de tuples, de taille correspondant au paramètre précisé lors de l'appel à la fonction (si suffisamment de résultats existent) et commençant par la première ligne obtenue par la requête.

```python
>>> cur.execute("SELECT * FROM personne;")
>>> trois_personnes = cur.fetchmany(3) # récupère les 3 premières lignes de personne
>>> trois_personnes
???
# le nom et le prénom de ces personnes :
>>> for personne in trois_personnes:
    	print(personne[0], personne[1])
        
```

* On peut récupérer tous les résultats avec la méthode `fetchall`. Le résultat est une liste de tuples, qui peut être vide si aucun résultat n'est retourné.

```python
>>> cur.execute("SELECT * FROM personne;")
>>> personnes = cur.fetchall() # récupère toutes les personnes
>>> personnes
>>> for personne in personnes:
    	print(personne[0], personne[1])
```



## Plus loin ?

La commande `cure.execute("requete")` exécute la requête indiquée entre apostrophe, si cette requête est valide, bien entendu. 

A partir de là, vous pouvez, en Python, exécuter toutes les requêtes SQL vues et testée avec BDBrowser en SQLite :

* `SELECT`
* `ORDER BY`
* les agrégats (`AVS`, `MIN`, `MAX`, `COUNT`, `AVG` ...)
* Les jointures (`JOIN` ... `ON`...)



Yapuka !





__________

Par Mieszczak Christophe

Licence CC BY SA

A partir des documents du DIU, université de Lille.
# Les bases de données relationnelles



## Le modèle relationnel



### Relation, attributs et domaine 



On appelle `relation` un tableau à deux dimensions (un peu comme une table _csv_).  

Il s'agit donc d'un groupe d'informations relatives à un sujet :

- Les colonnes sont les `attributs`
    - Les lignes, ou `n-uplets`, sont appelées  `enregistrements`. Chaque `enregistrement` doit être unique : il est interdit d'avoir, dans une relation, deux fois la même ligne.
- Chaque cases est une `entrée` , aussi appelée `valeurs`.



_Exemple :_ Voici une petite relation que nous appellerons _salles_, qui précise pour cchaque classe la salle dans laquelle ont lieu les cours :

| classe | salle |     genre     |
| :----: | :---: | :-----------: |
|  TG1   |  201  |   générale    |
|  TG2   |  204  |   générale    |
|  TG3   |  108  |   générale    |
|  TG4   |  21   | technologique |





> * Quels sont les attributs ?
>
>     ```txt
>                                 
>     ```
>
>     
>
> * Donnez un enregistrement.
>
>     ```txt
>                                 
>     ```
>
>     
>
> * Donnez une entrée.
>
>     ```txt
>                                 
>     ```
>
>     



_Remarque :_



Le tableau de la relation a une particularité : on peut intervertir n'importe quelle ligne avec une autre ligne ou n'importe quelle colonne avec une autre colonne sans changer quoi que ce soit à la relation.





Chaque attribut prend ses valeurs dans un ensemble appelé `domaine`. 

Un `domaine`  peut être :

* L'ensemble des nombres entiers ou une partie de l'ensemble. Il y a d'ailleurs différents type d'entiers : INT, SMALLINT ,LONGINT ...
    * L'ensemble des des nombres flottants ou une partie d'entre eux qui peuvent être du type FLOAT ou REAL ; 

* Les champs de type DATE ou TIME pour mesure du temps.
* les chaînes de caractères (TEXT, STR ou STRING) ou une structure particulière de chaîne de caractères (pour le nom d'un email par exemple)
* uniquement vrai ou faux, un booléen donc (BOOL).



Dans notre exemple :

* Le `domaine` de la classe est une chaîne de trois caractères commençant par `TG` et finissant par un numéro.
* Le `domaine`de la salle est un entier de trois chiffres.



Pour le SGBD que nous utiliserons, `DB Browser for SQLite`, les domaines sont "simplifiés" :

* INT pour les entiers.
* TEXT pour les chaînes de caractères.
* REAL pour les nombres flottants.
* BLOB (Binary large objet) pour les gros objets en format binaire.



Pour coder un booléen, SQlite utilisera un entier prenant pour valeur 0 ou 1.



_Remarque :_

L'intérêt d'être très précis dans le _type_ du domaine est l'optimisation de la place qu'occupent les données en mémoire. Par exemple, si toutes les salles sont codées par un entier à 3 chiffres, 3 octets suffiront à sauvegarder leurs valeurs tandis qu'un type _entier_ en utilisera beaucoup plus pour pouvoir coder de entiers beaucoup plus grands.

Cela a peu d'importance pour une petite base comme la notre mais devient crucial au fur et à mesure que la base grossit.



### Clé primaire (CP) ou Primary Key (PK)



Dans une relation, une `clé primaire`  est un `attribut` , ou un groupe d'attribut, qui identifie de manière unique une ligne. Elle ne doit pas être `NULL` (vide), peut être composée d'une ou plusieurs colonnes (On peut parfois ajouter une colonne dédiée si besoin).



> * Dans la relation vue plus haut, quels `attribut` peuvent être une `clé primaire` à eux seuls et pourquoi ?
>
>     ```txt
>                 
>     ```
>
>     
>
>



L'existence d'une clé primaire est une `contrainte d'unicité` qui permet de vérifier s'il n'existe pas deux enregistrements identiques dans la base, ce qui est interdit comme nous l'avons déjà dit plus haut. Cette contrainte permet de s'assurer de la cohérence de la base de donnée.



### D'autres relations



Une base de données n'est pas constituée d'une unique `relation` mais d'un ensemble de sujets connexes (c'est à dire relatifs à un même domaine) repartis en plusieurs tables.



En reprenant notre exemple, on peux ajouter à la base la `relation` suivante, que nous appellerons _eleves_ , concernant un groupe d'élèves du même lycée.



|   prenom   |    nom     | classe | date_naiss |  specialite   |
| :--------: | :--------: | :----: | :--------: | :-----------: |
|   Ernest   |  Legrand   |  TG2   |  02:02:12  | Mathématiques |
| Jean-yves  |   Petit    |  TG1   |  01:03:13  | Mathématiques |
|   Sophie   | Allexandre |  TG2   |  02:05:21  | Mathématiques |
|  Jocelin   |  Lamirand  |  TG2   |  01:10:16  | Mathématiques |
|    Eve     | Lawniczak  |  TG1   |  02:06:23  | Mathématiques |
|   Gaëlle   | Prudhomme  |  TG3   |  02:04:12  |    Anglais    |
| Christophe |  Scherer   |  TG4   |  02:03:13  |    Anglais    |
|  Corentin  |    Sten    |  TG3   |  01:06:21  | Mathématiques |
|  Condette  |   Sophie   |  TG4   |  01:12:06  | Mathématiques |
|  Emeline   | Vanherzek  |  TG4   |  02:01:03  | Mathématiques |
|   Louise   | Vandhamme  |  TG1   |  00:12:24  | Mathématiques |
|   Pierre   |   Techec   |  TG3   |  02:11:22  |      SES      |
|   Abdel    |   Arabah   |  TG1   |  02:09:17  |      SES      |
|   Matéo    | Diterlizi  |  TG4   |  02:08:13  |      SES      |
|   Pierre   |   Perrin   |  TG1   |  02:07:04  |      SES      |
|    Léo     |  Lawnisak  |  TG2   |  02:11:05  | Mathématiques |
|  Perrine   |   Lannoy   |  TG1   |  01:12:07  | Mathématiques |
|   Youcef   |  Ourcenar  |  TG3   |  02:04:27  | Mathématiques |
|   Ursula   |  Andrews   |  TG4   |  02:02:13  |      EPS      |
|   Céline   |    Dyon    |  TG1   |  02:02:12  |      EPS      |
|  François  |  Fledman   |  TG1   |  01:12:12  | Mathématiques |
| Hypollite  |  Legentil  |  TG2   |  00:12:24  | Mathématiques |
|  Franklin  |  Latortue  |  TG2   |  01:10:13  | Mathématiques |
|  Théodore  | Grosvelte  |  TG4   |  02:13:01  |     HGGSP     |



> * Quels sont les attributs ?
>
>     ```txt
>                                 
>     ```
>
>     
>
> * Donnez un enregistrement.
>
>     ```txt
>                                 
>     ```
>
>     
>
> * Donnez une entrée.
>
>     ```txt
>     
>     ```
>
> * Donnez un attribut ou un groupe d'attributs pouvant constituer une clé primaire pour cette relation  :
>
>     ```txt
>                                 
>     ```
>
>     

_Remarque :_

Une clé primaire qui ne contiendrait que le nom et le prénom des élèves risque de poser un problème en cas de présence d'homonymes.  On ajoutera alors à la relation une colonne dédiée qui sera la clé primaire avec un simple numéro, un identifiant souvent appelé `id`  qui fera une clé primaire simple et fiable.



|  id  |   prenom   |    nom     | classe | date_naiss |  Specialite   |
| :--: | :--------: | :--------: | :----: | :--------: | :-----------: |
|  0   |   Ernest   |  Legrand   |  TG2   |  02:02:12  | Mathématiques |
|  1   | Jean-yves  |   Petit    |  TG1   |  01:03:13  | Mathématiques |
|  2   |   Sophie   | Allexandre |  TG2   |  02:05:21  | Mathématiques |
|  3   |  Jocelin   |  Lamirand  |  TG2   |  01:10:16  | Mathématiques |
|  4   |    Eve     | Lawniczak  |  TG1   |  02:06:23  | Mathématiques |
|  5   |   Gaëlle   | Prudhomme  |  TG3   |  02:04:12  |    Anglais    |
|  6   | Christophe |  Scherer   |  TG4   |  02:03:13  |    Anglais    |
|  7   |  Corentin  |    Sten    |  TG3   |  01:06:21  | Mathématiques |
|  8   |  Condette  |   Sophie   |  TG4   |  01:12:06  | Mathématiques |
|  9   |  Emeline   | Vanherzek  |  TG4   |  02:01:03  | Mathématiques |
|  10  |   Louise   | Vandhamme  |  TG1   |  00:12:24  | Mathématiques |
|  11  |   Pierre   |   Techec   |  TG3   |  02:11:22  |      SES      |
|  12  |   Abdel    |   Arabah   |  TG1   |  02:09:17  |      SES      |
|  13  |   Matéo    | Diterlizi  |  TG4   |  02:08:13  |      SES      |
|  14  |   Pierre   |   Perrin   |  TG1   |  02:07:04  |      SES      |
|  15  |    Léo     |  Lawnisak  |  TG2   |  02:11:05  | Mathématiques |
|  16  |  Perrine   |   Lannoy   |  TG1   |  01:12:07  | Mathématiques |
|  17  |   Youcef   |  Ourcenar  |  TG3   |  02:04:27  | Mathématiques |
|  18  |   Ursula   |  Andrews   |  TG4   |  02:02:13  |      EPS      |
|  19  |   Céline   |    Dyon    |  TG1   |  02:02:12  |      EPS      |
|  20  |  François  |  Fledman   |  TG1   |  01:12:12  | Mathématiques |
|  21  | Hypollite  |  Legentil  |  TG2   |  00:12:24  | Mathématiques |
|  22  |  Franklin  |  Latortue  |  TG2   |  01:10:13  | Mathématiques |
|  23  |  Théodore  | Grosvelte  |  TG4   |  02:13:01  |     HGGSP     |





La base peut contenir plus de deux relations bien entendu. Voici, par exemple,  une autre relation que nous appellerons _profs_ :

|    nom     |     prenom     |    matiere    |
| :--------: | :------------: | :-----------: |
|  Dickens   |    Charles     |    Anglais    |
| Mirzakhani |     Maryam     | Mathématiques |
|  Lovelace  |      Ada       |      NSI      |
|    Hugo    |     Victor     |   Français    |
|   Irving   |     Fisher     |      SES      |
|   Eiffel   |    Gustave     |      SI       |
|   Arouet   | François-Marie |     HGGSP     |
|   Reeves   |     Hubert     | Sc. Physique  |
|    Cruz    |    Pénélope    |   Espagnol    |
|  Devinci   |    Léonard     |    Italien    |
|   Mayer    |     Kévin      |      EPS      |

> Donner plusieurs clés primaires possibles pour cette relation :
>
> ```txt
> ```
>
> 

### Optimisation de la base 

On pourrait créer une énorme mais unique relation contenant les données de nos trois relations...

 Mais :

* C'est le fait d'avoir plusieurs tables qui nous rend inutile de stipuler, pour chaque élève, quels sont ses professeurs et dans quelle salle il se trouve, par exemple. **Définir plusieurs tables évite donc la redondance des valeurs et des colonnes et optimise ainsi la base de données.**
* On pourrait avoir envie d'indiquer, pour chaque classe, le nombre d'élèves. Cependant, cela ajouterait un attribut supplémentaire alors que ce dernier peut être calculé grâce à des fonctions du SGBD. **On évitera toujours de déclarer des attributs supplémentaires s'ils peuvent être déduits d'autres attributs**



### Clé étrangère (CE) ou Foreign Key (FK)

Il faut maintenant créer un _lien_ entre nos  `relations`  afin de pouvoir _recouper_ leurs données : c'est le rôle de la clé étrangère ou foreign key.



Un attribut d'une relation peut prendre pour valeurs les valeurs d'un attribut, ou d'un groupe d'attributs, qui constitue une clé primaire d'une autre relation. Un tel attribut est appelé `clé étrangère`  . Une clé étrangère `référence`, c'est à dire _fait le lien avec_,  une clé primaire d'une autre table : ses valeurs doivent pré-exister dans la table qu'elle référence.



Dans la relation _eleves_, la clé `classe` est une clé étrangère qui référence la clé primaire `classe` de la relation _salles_.



Une clé étrangère est une `contrainte d'intégrité` qui permet de garder une cohérence entre les deux relations :

* ses valeurs doivent pré-exister dans la relation à laquelle elle fait référence : on ne peut pas ajouter à la clé étrangère une valeur qui n'est pas présente parmi les valeurs de la clé primaire de l'autre relation. 
* On ne peut pas supprimer une valeur d'une clé primaire qui est référencée dans une clé étrangère.



Dans notre exemple, impossible de placer un élève en TG5 ou de supprimer la classe TG1 de la relation _classes_.



> 
>
> Comment _relier_ les relations _profs_ et _élèves_ ?
>
>  ```TXT
>```
> 
>



### Autres contraintes



Nous avons vu que la clé primaire imposait une `contrainte d'unicité` et la clé étrangère une `contrainte d'intégrité`. Il existe d'autre type de contraintes, par exemple :

* une bonne pratique consiste à interdire qu'un attribut ne pas pas être nul (NULL). Cette contrainte de non nullité se fait au moyen du mot clé `NOT NULL`.
* il arrive parfois qu'un attribut, bien qu'il ne soit pas une clé primaire, doivent être unique. Il est possible de forcer cette unicité sans utiliser l'attribut comme clé primaire.
* on peut définir des `contraintes de domaine` en réduisant le domaine. Par exemple, un attribut entier peut voir sa plage réduite à un chiffre. Cela se fait lors de la requête pour créer la table ou en modifiant une table déjà existante. 



. Nous en reparlerons dans le cours [créer une base de donnée](creer_une_base.md).





### Schéma relationnel



Un`schéma de relation`  d'une relation en précise le nom, les attributs et leurs domaines, la clé primaire et, éventuellement , la clé étrangère.



Ainsi, le `schéma de relation` de _classes_ pourrait être :

 `salles(classe (PK) : "TG" suivi d'un entier, salle : entier à 3 chiffres)`



Avec SQlite, il est : 

 `salles(classe (PK) : TEXT, salle : INT`



> * Donnez le schéma de relation de _eleves_ :
>
>     ```txt
>                                 
>     ```
>
>     
>
> * Donnez le schéma de _profs_ :
>
>     ```txt
>                                 
>     ```
>
>     





On appelle `schéma relationnel` l'ensemble de tous les schémas de relation d'une base de donnée.



> Donner le schéma relationnel de notre base constituées des relations _classes_, _eleves_ et _profs_ :
>
> ```txt
> 
> ```
>
> 



Graphiquement, on peut représenter le schéma relationnel de notre base ainsi :

![schéma relationnel](media/schema_relationnel.png)





## D'autres modèles de base de données



Le modèle relationnel que nous étudierons cette année est le modèle le plus couramment utilisé, mais il n'est pas le seul. Il en existe même beaucoup d'autres.

### Modèle NO-SQL

Le modèle [NoSQL](https://fr.wikipedia.org/wiki/NoSQL) n'utilise pas la structure tabulaire typique d'une base de documents  relationnelle. Il  offre une évolutivité rapide pour gérer des ensembles de données  volumineux et généralement non structurés. 

NoSQL est également un type de base de données distribuée, ce qui  signifie que les informations sont copiées et stockées sur différents  serveurs, qui peuvent être distants ou locaux. Cela garantit la  disponibilité et la fiabilité des données. Si certaines données sont  hors ligne, le reste de la base de données peut continuer à s'exécuter.  

### Modèle de données *entité-association*

Ce type de modèle est le plus couramment utilisé pour la conception de modèles de données *logiques*. Selon ce type de modèle, une base de données est un lot d'entités et d'associations. Une *entité* est un sujet concret, un objet, une idée, pour laquelle il existe des informations. Un *attribut* est un renseignement concernant ce sujet — exemple le nom d'une personne. À chaque attribut correspond un *domaine* : un ensemble de valeurs possibles. Une *association* désigne un lien entre deux entités — par exemple, un élève et une école.



### Modèle de données *objet*

Ce type de modèle est fondé sur la notion d'objet de la programmation orientée objet. Selon ce type de modèle, une base de données est un lot d´*objets* de différentes *classes*. Chaque objet possède des *propriétés* — des caractéristiques propres, et des *méthodes* qui sont des opérations en rapport avec l'objet. 



### Modèle de données *hiérarchique*

Ce type de modèle de données a été créé dans les années 1960 ; c'est le plus ancien modèle de données. Selon ce type de modèle, les  informations sont groupées dans des *enregistrements*, chaque enregistrement comporte des *champs*. Les enregistrements sont reliés entre eux de manière hiérarchique : à  chaque enregistrement correspond un enregistrement parent.



### Modèle de données *réseau*

Ce type de modèle de données est semblable au modèle hiérarchique. Les informations sont groupées dans des *enregistrements*, chaque enregistrement possède des *champs*. Les enregistrements sont reliés entre eux par des pointeurs.  Contrairement aux modèles hiérarchiques, l'organisation des liens n'est pas obligatoirement hiérarchique, ce qui rend ces modèles plus  polyvalents.







_________

Par Mieszczak Christophe

Licence CC BY SA

source image : production personnelle.


# Requêtes SQL



Nous en avons déjà vu lors de la [création d'une base](creer_une_base.md), nous allons ici voir les principales requêtes utilisées en **S**_tructured_ **Q**_uery_ **L**_angage_.

Le cours s'appuie d'ailleurs sur la base de données que nous avons justement créer précédemment au sujet d'un lycée.



## `SELECT`ion d'attribut dans une relation



> * Testez la commande SQL suivante et insérez le résultat de la console en dessous par copier-coller:
>
>     ```sql
>     SELECT 
>     nom, prenom
>     FROM
>     profs;
>     ```
>     
> * Quelle requête permet de lister les matières enseignées par les professeurs ?
>
>     ```sql
>                   
>     ```
> 
>     



Nous avons imposer une contrainte d'unicité pour nos attributs qui se devaient d'être uniques et n'étaient pas des clés primaires. Ce n'est pas le cas, par exemple, de l'attribut _prenom_ de la relation _eleves_. Si on veut lister les prénoms différents, il faut utiliser l'opérateur `DISTINCT`



> * Testez cette commande : combien y-a-t-il d'enregistrements ?
>
>     ```sql
>     SELECT 
>     prenom
>     FROM
>     eleves;
>     ```
>     
>* Testez maintenant cette commande  : que constatez-vous ?
> 
>    ```sql
>     SELECT DISTINCT
>     prenom
>     FROM
>     eleves
>    ```
>    
> * Comment savoir si plusieurs élèves ont la même date de naissance ?
>
>     ```sql
>                   
>     ```
> 
>     

_Remarque :_

On peut renommer la colonne obtenue lors de l'affichage d'un résultats en utilisant `AS` . C'est utile lorque plusieurs relations utilisent des attributs de même nom.

Testez :

```sql
SELECT nom as nom_prof FROM profs;
```





## La clause `WHERE`



On peut sélectionner uniquement les lignes qui respectent une condition grâce à la clause `WHERE` :

```sql
SELECT 
noms_colonnes_séparés_par_virgules
FROM
nom_table
WHERE
condition;
```



La condition porte sur les valeurs des colonnes :

- Utilisation d'opérateurs de comparaison (op_comp) : `=`, `<>`, `!=`, `>`, `>=`, `<`, `<=`
- Utilisation d'opérateurs booléens (op_bool) : 
    - `AND` : combinaisons de conditions sur des colonnes différentes
    - `OR` : plusieurs valeurs possibles pour une même colonne



_Remarque : les dates et SQLite_

Dans la plupart des SGBD, il existe de [nombreux domaines pour les dates](https://www.sqlfacile.com/apprendre_bases_de_donnees/les_types_sql_date).

Mais SQLite, n'en compte ... aucun.

Pour pouvoir classer par date, il faut alors utiliser l'astuce suivante : écrire la date sous la forme année/mois/date et choisir le domaine `TEXT` pour cet attribut.  L'ordre _lexicographique_ est alors la relation d'ordre de nos dates.



> * Testez la requête suivante (à quelle date correspond-t-elle ?)
>
>     ```sql
>     SELECT 
>     nom, prenom,classe,date_naiss
>     FROM
>     eleves
>     WHERE
>     classe = "TG1" ;
>     ```
>
> * Déterminez la liste des noms des élèves de "TG2" ou de "TG3" :
>
>     ```sql
>     
>     ```
>
> * Déterminez la liste des noms des élèves  nés entre le 10 janvier 2000 et le 12 février 2001
>
>     ```sql
>     ```
>
>     



_

## `ORDER BY` et `AS`: ordonner les résultats

La commande `ORDER BY` permet de changer l'ordre dans lequel s'affichent les résultats..

La commande `AS` , facultative, permet de renommer certains attributs, pour les rendre plus parlant par exemple.

```sqlite
SELECT nom_colonne AS nom_affiché
FROM nom_table AS abrev
ORDER BY nom_colonne [DESC];
```

_Remarque :_

* Par défaut, le tri est dans l'ordre croissant, `DESC` permet d'obtenir l'ordre décroissant.
* Attention au domaine et à la relation d'ordre qui permet de la trier ! 



> * Testez :
>
>     ```sql
>     SELECT nom as noms_eleves, prenom as prenoms_eleves FROM eleves ORDER BY nom;
>     ```
>
> 
>
> * Déterminer la liste des noms et prénoms les élèves de TG1 par ordre alphabétique croissant de l'attribut _nom_. Indiquez la requête et le résultat ci-dessous.
>
>     ```sql
>         
>     ```
>
> 
>
> * Même question en ordonnant du plus jeune au plus vieux.  Indiquez la requête et le résultat ci-dessous.
>
>     ```sql
>     
>     ```
>
> * Déterminez la liste des nom, prenom et matière des professeurs par ordre alphabétiques des matières enseignées.
>
>     



### Recherche _floue_



Il est parfois utile de sélectionner des enregistrements dont un attribut commence, se termine ou contient un caractère particulier. On utilise pour cela l'opérateur `LIKE` .

> * Testez :
>
>     ```sql
>     SELECT nom AS 'D...', prenom 
>     FROM eleves
>     WHERE nom 
>     LIKE'D%'
>     ```
>
> 
>
> * Quelle commande permettrait de trouver et afficher par ordre alphabétique les noms et prénoms des élèves nés en 2002 ?
>
>     ```sql
>     
>     ```
>
> * Comment retrouver la spécialité d'un élève dont le nom s'écrit _Lawnizak_ ou _Lawniczak ou _lawnizak_ ?
>
>     ```sql
>     ```
>
>     



## Fonctions d'agrégation.



Les fonctions d'agrégation permettent d'effectuer des opérations statistiques sur une colonne. En voici la syntaxe :

```
SELECT FONCTION(nom_colonne)
FROM nom_table;
```

Applique une `FONCTION` sur les valeurs d'une colonne

- `COUNT` : compte le nombre de lignes sélectionnées.
- `MIN`, `MAX` : renvoie la valeur minimum ou maximum de la colonne, parmi les lignes sélectionnées
- `SUM`, `AVG` : calcule la somme ou la moyenne des valeurs **numériques** de la colonne, parmi les lignes sélectionnées





> * Testez cette commande. Que fait-elle ?:
>
>     ```sql
>     SELECT COUNT(nom) FROM eleves
>     WHERE date_naiss LIKE '02%'
>     ```
>
> * Quelle commande renverra le nombre d'élèves des classes TG1 ?
>
>     ```sql
>         
>     ```
>
>
> * Quelle commande renvoie le nombre d'élèves qui suivent la spécialité NSI ?
>
>     ```sql
>     ```
>
>     
>
> * Quelle commande renverra la date de naissance de l'élève le plus jeune de relation `eleves` ? 
>
>     ```sql
>
> 	```
>
> * Quelle commande renverra le nom, le prénom et la date de naissance de l'élève le plus vieux de la TG3 ?
>
>     ```sql
>         
>     ```
>
> 
>
> 





## Jointure

On appelle  `jointure` l'opération consistant à rapprocher, selon une condition, des tuples de deux (ou plusieurs) relations d'une base afin d'en créer une nouvelle qui contient l'ensemble des tuples obtenus en concaténant ceux des différentes relations vérifiant la condition de rapprochement.



OUCH, ça a l'air de piquer un peu ... un petit exemple vite



Voici une requête qui joint les deu relations profs et élèves en alignant ces deux relations grâce à la clé étrangère _specialite_ d'_eleves_ et la clé primaire _matière_ de _profs_ :
```sql
SELECT eleves.nom as nom_eleve, eleves.prenom as prenom_eleve, profs.nom as nom_prof_de_spe FROM eleves, profs
ON profs.matiere = eleves.specialite;
```

_Remarques :_

* Les attributs sont précédés du nom de la relation à laquelle ils appartiennent : on dit qu'ils sont `préfixés` par le nom de leur relation.

    * Cela permet de différencier deux attributs de deux relations différentes qui portent le même nom, comme ici les attributs _nom_ et _prenom_ communs aux relations _eleves_ et _profs_.

    * Si le nom de l'attribut est unique, il est inutile de préciser sa relation. Dans notre exemple, l'attribut _salle_ n'a pas besoin de son préfixe `profs`

* On peut très bien joindre plus de deux relations.

    

Éxécutez cette requête :

```sql
```





> Pour chaque question, indiquez la requête utilisée et le résultat obtenu.
>
> * Lister les noms et prénoms de tous les élèves ainsi que la salle où ils travailleront.
>
>     ```sql
>     
>     ```
>
> 
>
> * Comment s'appelle le professeur qui enseigne la spécialité de Franklin Latortue ?
>
>     ```sql
>     
>     ```
>
> 
>
> * Combien y-aura -t-il d'élèves dans la salle 121 ?
>
>     ```sql
>     
>     ```
>
> 
>
> * Quelles sont la date de naissance, le nom et le prénom de l'élève le plus âgé dont le professeur est Charles Dickens ?
>
>     ```sql
>     
>     ```
>
> * Dans quelles classes intervient M Eiffel ? (il faudra une double jointure ...)
>
>     ```sql
>     ```
>
> * Quels professeurs font cours dans la salle 121 ? 
>
>     ```sql
>     ```
>
>     







_____

Par Mieszczak Christophe

Licence CC BY SA
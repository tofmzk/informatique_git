# Agence de voyage



Une agence de voyage utilise une base de données dont voici le schéma relationnel.



![schéma relationnel](media/agence_voyage.jpg)

### A propos des clés

1. Donnez des domaines possibles pour chaque attribut de chaque relation.

2. Donnez les clés primaires de chaque relation.

3. Donnez les clés étrangères des relations qui en ont une, en précisant quel attribut de quelle relation elle référence.

4. Quelles clés garantissent l'unicité des enregistrements dans chaque relation ? Expliquez.

5. Quelles clés garantissent l'intégrité des enregistrements entre les relations ? Expliquez.

6. Une nouvelle croisière va être créée. Elle passe par une nouvelle ville. Précisez dans quel ordre les relations devront-être modifiées pour ajouter cette nouvelle croisière.

   

    
   
    



### Requêtes

1. Donnez une requête permettant de lister les villes par laquelle passe la croisière dont l'identifiant est 13.
2. Donnez une requête permettant de connaître le nombre de clients dans la base de donnée.
3. Donnez une requête permettant de connaître le nom et l'identifiant et la date de naissance du plus vieux client de la base de donnée.
4. Donnez une requête permettant de connaître l'identifiant de(s) croisière(s) réservée(s) par un client dont l'identifiant est 123.
5. Donnez une requête permettant de lister les noms et prénoms des clients qui ont réservé la croisière dont l'identifiant est 21.
6. Donnez une requête permettant de lister les villes que visitera le client dont l'identifiant est 32.
7. Un terrible virus frappe la ville de _Puerto corona_ par laquelle passent certaines croisières. Donnez une requête permettant de lister les noms et prénoms des clients qui passent par cette ville. 







_____

Par Mieszczak Christophe

Licence CC BY SA

source image : production personnelle.
import module_lineaire

class Arbre():
    '''
    une classe pour implémenter des arbres de façon récursive
    '''
    def __init__(self, etq = None, sag = None, sad = None):
        '''
        construit un arbre vide avec un sous_arbre gauche et un sous arbre droit vides
        par défaut
        : params 
            etq (?) l'étiquette du noeud
            sag (Arbre) le sous arbre gauche
            sad (Arbre) le sous-arbre droit
        '''
        self.etiquette = etq
        self.sous_arbre_gauche = sag
        self.sous_arbre_droit = sad
    
    def est_vide(self) :
        '''
        renvoie True si l'arbre est vide est False sinon
        : return (bool)
        >>> a = Arbre()
        >>> a.est_vide()
        True
        >>> a1.est_vide()
        False
        '''
        return self.etiquette == None
    
    def __repr__(self) :
        '''
        renvoie une chaine qui représente la classe
        '''
        if self.est_vide() :
            chaine = "Instance d'Arbre vide"
        else :
            chaine = "Instance d'Arbre d'étiquette " + str(self.etiquette)
        return chaine
    
    def sag(self):
        '''
         renvoie le fils gauche de l'arbre.
         Si ce fils est None, renvoie un Abre vide
         : return (Arbre)
         >>> a = Arbre()
         >>> a.sag()
         Instance d'Arbre vide
         >>> a8.sag()
         Instance d'Arbre d'étiquette 3
        '''
        if self.sous_arbre_gauche == None :
            self.sous_arbre_gauche = Arbre()
        return self.sous_arbre_gauche

    def sad(self):
        '''
        renvoie le fils droit de l'arbre Si ce fils est None, renvoie un Abre vide
        : return (Arbre)
        >>> a = Arbre()
        >>> a.sad()
        Instance d'Arbre vide
        >>> a8.sad()
        Instance d'Arbre d'étiquette 10
        '''
        if self.sous_arbre_droit == None :
            self.sous_arbre_droit = Arbre()
        return self.sous_arbre_droit
        
        
    def etq(self):
     '''
     renvoie l'étiquette de l'arbre
     : return (??)
     '''
     return self.etiquette
    
    def mut_etq(self, valeur):
        '''
        remplace l'etiquette de l'Arbre par la valeur
        : param valeur (???)
        : pas de return
        : effet de bord sur l'Arbre
        '''
        self.etiquette = valeur
        
    def __str__(self):
        '''
        renvoie une chaine qui correspond à l'arbre
        : return (str)
        >>> a = Arbre()
        >>> print(a)
        ∆
        >>> print(a6)
        (6, (4, ∆, ∆), (7, ∆, ∆))
        >>> print(a8)
        (8, (3, (1, ∆, ∆), (6, (4, ∆, ∆), (7, ∆, ∆))), (10, ∆, (14, (13, ∆, ∆), ∆)))
        '''
        if self.est_vide() :
            return '∆'
        else :
            return ( '(' + str(self.etq()) + ', '
                     + self.sag().__str__() + ', '
                     + self.sad().__str__() + ')'
                     )
    
    def taille(self) :
        '''
        renvoie la taille de l'Arbre
        : return(int)
        >>> a = Arbre()
        >>> a.taille()
        0
        >>> a8.taille()
        9
        '''
        if self.est_vide():
            return 0
        else:
            return(1
                   + self.sag().taille()
                   + self.sad().taille()
                   )
        
    def hauteur(self) :
        '''
        renvoie la hauteur de l'Arbre
        : return (int)
        >>> a = Arbre()
        >>> a.hauteur()
        -1
        >>> a3.hauteur()
        2
        >>> a8.hauteur()
        3
        '''
        if self.est_vide():
            return -1
        else :
            return 1 + max(self.sag().hauteur(), self.sad().hauteur())
    

    def prefixe(self):
        '''
        renvoie un tableau des étiquettes de l'arbre
        dans l'ordre prefixe
        : return (list)
        >>> a8.prefixe()
        [8, 3, 1, 6, 4, 7, 10, 14, 13]
        '''
        if self.est_vide() :
            return []
        else :
            return ([self.etq()] +
                    self.sag().prefixe() +
                    self.sad().prefixe()
                    )
    def infixe(self):
        '''
        renvoie un tableau des étiquettes de l'arbre
        dans l'ordre infixe
        : return (list)
        >>> a8.infixe()
        [1, 3, 4, 6, 7, 8, 10, 13, 14]
        '''
        if self.est_vide() :
            return []
        else :
            return (self.sag().infixe() +
                    [self.etq()] +
                    self.sad().infixe()
                    )
        
    def suffixe(self):
        '''
        renvoie un tableau des étiquettes de l'arbre
        dans l'ordre suffixe
        : return (list)
        >>> a8.suffixe()
        [1, 4, 7, 6, 3, 13, 14, 10, 8]
        '''
        if self.est_vide() :
            return []
        else :
            return (self.sag().suffixe() +
                    self.sad().suffixe() +
                    [self.etq()] 
                    )
        
    def largeur(self):
        '''
        renvoie un tableau des etqselon le parcours en largeur d'abord
        : return (list)
        >>> a8.largeur()
        [8, 3, 10, 1, 6, 14, 4, 7, 13]
        '''
        #File
        f = module_lineaire.File()
        f.enfiler(self)
        #tableau
        tab = []
        while not f.est_vide():
            a = f.defiler()
            if not a.est_vide():
                tab.append(a.etq())
                f.enfiler(a.sag())
                f.enfiler(a.sad())
        return tab
        
    def sauvegarder(self, nom_fichier):
        '''
        sauvegarde l'arbre en mode texte dans mon_fichier
        : param mon_fichier (str)
        : pas de return
        '''
        assert type(nom_fichier) is str,'nom_fichier doit être du type str'
        chaine = self.__str__()
        chaine = chaine.replace(', ' , ',')
        try :
            ecriture = open(nom_fichier,'w',encoding='utf_8')
            ecriture.write(chaine)
            ecriture.close()
        except FileNotFoundError :
            raise 
        

        
        
    ### charger un arbre
    def extraire_racine(chaine) :
        '''
        renvoie la racine de la chaine représentant un arbre
        et la chaine privée de cette racine
        : param (str)
        : return (tuple of str)
        >>> Arbre.extraire_racine('8,(3,(1, ∆, ∆),(6,∆, ∆)),(2,(7, ∆, ∆),∆)')
        ('8', '(3,(1, ∆, ∆),(6,∆, ∆)),(2,(7, ∆, ∆),∆)')
        '''
        racine = ''
        i = 0
        while chaine[i] != ',' :
            i = i + 1
        return chaine [:i], chaine [i + 1:]
        
    def extraire_sous_arbre(chaine) :
        '''
        renvoie le premier sous-arbre contenu dans la chaine
        et la chaine privée de ce sous-arbre
        :param chaine (str)
        : return (tuple of str)
        >>> Arbre.extraire_sous_arbre('(3,(1, ∆, ∆),(6,∆, ∆)),(2,(7, ∆, ∆),∆)')
        ('(3,(1, ∆, ∆),(6,∆, ∆))', '(2,(7, ∆, ∆),∆)')
        '''
        if chaine[0] == '∆' :
            return '∆', chaine[2:]
        
        else :
            i = 1
            sous_arbre = '('
            parenthese = module_lineaire.Pile()
            parenthese.empiler('(')
            while not parenthese.est_vide() :
                sous_arbre += chaine[i]
                if chaine[i] == '(' :
                    parenthese.empiler('(')
                elif chaine[i] == ')' :
                    parenthese.depiler()
                i = i + 1
            return sous_arbre, chaine [i + 1:]
    
    def construire(self, chaine):
        '''
        construit un arbre correspondant à la chaine passée en paramètre
        de façon récursive.
        :param chaine(str)
        : pas de return
        : effet de bord sur self
        >>> a = Arbre()
        >>> a.construire('(8,(3,(1,∆,∆),(6,(4,∆,∆),(7,∆,∆))),(10,∆,(14,(13,∆,∆),∆)))')
        >>> print(a)
        (8, (3, (1, ∆, ∆), (6, (4, ∆, ∆), (7, ∆, ∆))), (10, ∆, (14, (13, ∆, ∆), ∆)))
        '''
        # prépare la syntaxe de la chaine
        if chaine[0] == '(' :
            chaine = chaine[1:]
        if chaine[-1] == ')' :
            chaine = chaine[: -1]
        # extraire racine, sag et sad
        racine, chaine = Arbre.extraire_racine(chaine)
        sag, chaine = Arbre.extraire_sous_arbre(chaine)
        sad, chaine = Arbre.extraire_sous_arbre(chaine)
        self.etiquette = racine
        if sag != '∆' :
            arbre_gauche = Arbre()
            arbre_gauche.construire(sag)
            self.sous_arbre_gauche = arbre_gauche
        else :
            self.sous_arbre_gauche = Arbre()
        if sad != '∆':
            arbre_droit = Arbre()
            arbre_droit.construire(sad)
            self.sous_arbre_droit = arbre_droit
        else:
            self.sous_arbre_droit = Arbre()
            
    def charger(nom_fichier):
        '''
        renvoie un arbre construit à partir des données du fichier txt nom_ficher
        : param nom_fichier (str)
        : return (Arbre)
        '''
        assert type(nom_fichier) is str,'nom_fichier doit être du type str'
        try :
            lecture = open(nom_fichier,'r',encoding='utf_8')
            chaine = lecture.read()
            lecture.close()
        except FileNotFoundError :
            raise 

        a = Arbre()
        if len(chaine) > 1 :
            a.construire(chaine)
        return a


  
    

    
            
           
                
     
     
        
        
#############################
if __name__ == '__main__' :
    a1 = Arbre(1)
    a4 = Arbre(4)
    a7 = Arbre(7)
    a13= Arbre(13)
    a6 = Arbre(6, a4, a7)
    a14 = Arbre(14, a13)
    a3 = Arbre(3, a1, a6)
    a10 = Arbre(10, None, a14)
    a8 = Arbre(8, a3, a10)
    import doctest
    doctest.testmod(verbose = True)
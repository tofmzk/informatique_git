# Maître Hibou

## Apprendre sur un arbre 

Maître Hibou peut tenter de retrouver l'animal auquel vous pensez en vous posant des questions et, s'il ne le connaît pas, apprendre le nom et une caractéristique de cet animal.


Au départ, le hibou ne sait rien du tout. Il dispose d'une structure d' Arbre binaire qui est vide.

```txt
        ∆
       / \
      ∆   ∆ 
```

Il suit alors l'algorithme décrit ci-dessous.

**Si l'arbre est vide**,  Il vous demande :

* de proposer une question dont la réponse est oui ou non et qui décrit un animal. Par exemple :  _"Est-ce que c'est un animal de compagnie ?"_
* de lui donner l'animal auquel vous pensez et qui correspond à cette question. Par exemple, **un chien**.

Il construit ensuite l'arbre ci-dessous et l'insère à la place de l'arbre vide :

```txt
                   Est-ce que c'est un animal de compagnie ?
                             /oui                  \non
                           un chien                 ∆
                          /oui    \non
                         ∆         ∆
```
Sinon si l'arbre n'est pas vide :

* si le sous arbre gauche est vide, l'étiquette est le nom d'un animal (ci dessus, _un chien_). Le hibou vous propose alors ce nom et vous demande s'il est le bon :
    * si oui il a trouvé !
    * si non, il recommence son algorithme dans le sous-arbre droit.
* sinon, l'étiquette est une question et le hibou la pose  :
    * si la réponse est oui,  il recommence son algorithme dans le sous-arbre gauche.
    * si non,  il  recommence son algorithme dans le sous-arbre droit.


Petit à petit, Maître Hibou augmente ses connaissances et sera capable de deviner de plus en plus d'animaux.

```txt
          Est-ce  un animal de compagnie ?
     /oui                                   \non
   un chien  	                      Est-ce que il vit dans la jungle ?
 /oui       \non                           /oui               \non
∆    A-t-il a de grandes oreilles ?     un lion                 etc ....
         /oui                \non
       un lapin               etc ...
       /oui
      ∆
```

D'ailleurs, avant de poser une question à l'utilisateur, il ne manque pas de lui préciser combien il connait d'animaux qui correspondent à la description que l'utilisateur lui a faite jusque là.

## Ça c'est cadeau

Je vous fournis une évolution de notre `module_arbre` avec deux _petites_ choses intéressantes :

* la fonction `charger(nom_fichier)` de la classe _Arbre_ renvoie un arbre à partir des données du fichier texte passé en paramètre. 
* la méthode `sauvegarder(self, nom_fichier)` de la classe _Arbre_ sauvegarde l'_Arbre_ dans le fichier `nom_fichier`.

Vous pouvez toujours y jeter un coup d'oeil ... on y utilise une pile de parenthèses.



Pour terminer, un petit hibou en ASCII art histoire de rendre l'interface agréable :

```python
HIBOU = r'''
  _^_____^__
 / ___  ___ \     
/ / @ \/ @ \ \  
\ \___/\___/ / 
 \____\/____/ 
 /     /\\\\\
 |     |\\\\\
 \     \\\\\\\
  \______/\\\\\
 ___||_||___  \\
 ---00-00--- '''
```
* le `r` devant la chaîne de caractères permet de ne pas interpréter les caractères d'échappements `\`. Sans lui, le hibou perdra des plumes.
* le triple `'''` permet de définir une chaîne sur plusieurs lignes sans utiliser le classique `\n` qui ne serait pas interprété en raison du `r`.

Testez donc un `print(HIBOU)` dans la console.



## Les tâches 

Pour mener à bien ce projet vous aurez besoin, au minimum (on peut/doit réaliser de petites fonctions annexes), des fonctions :

* `jouer()` : 
    * Charge l'arbre à partir d'un fichier associé ou, s'il n'y pas de fichier crée un _Arbre_ vide.
    * Lance le déroulement du jeu en se basant sur l'_Arbre_ précédent (fonction `derouler` à voir plus bas).
    * Sauvegarde l'Arbre à l'issue de la partie dans le fichier associé.
    * Propose de rejouer .. ou pas et éventuellement relance une partie.
* `compter_animaux(arbre)` , une fonction récursive, renvoie le nombre d'animaux présents dans l'arbre.
* `derouler(arbre)` , une fonction récursive : déroule le jeu en se basant sur l'_arbre_ passé en paramètre :
    * on affiche le hibou.
    * le hibou dit à l'utilisateur combien il connait d'animaux dans son arbre.
    * le hibou applique sa méthode de recherche (vue tout en haut).

* `ajouter_dans_arbre(arbre, question, animal)` qui ajoutera dans l'_arbre_ la `question` (str) et l'`animal` (str) passés en paramètre aux bons endroits.
* De fonction**S** d'interface pour toutes les interactions entre lui et le hibou (poser une question, proposer un animal , afficher le nombre d'animaux présents dans l'arbre ...)



On respectera les principes de programmation habituels, entre autres  :

* Docstrings détaillées et précises (difficile de faire des tests dans ce projet ...).
* Une fonction réalise une et une seule chose. Si elle en fait plusieurs, c'est qu'on a besoin de sous-fonctions.
* Le même code ne doit pas apparaître à des endroits différents : on fait une fonction avec ce code et on l'appelle plusieurs fois.
* Les lignes de codes sont commentées.
* Séparation des algorithmes et de l'interface utilisateur  : aucun `print` et aucun `input` ailleurs que dans une fonction d'interface !
* On nomme fonctions et variables avec des noms désignant ce à quoi elles servent.



## Evolution possible (Bonus)

Le Hibou n'est pas une andouille. Si vous lui proposez un animal qu'il connait déjà, il vous dira que vous avez fait une erreur quelque part dans vos réponses et vous donnera la série de question qui conduit à lui









__________

Mieszczak Christophe

licence CC BY SA


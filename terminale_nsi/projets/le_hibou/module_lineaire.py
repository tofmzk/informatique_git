# -*- coding: utf-8 -*-
'''
   un module pour les classes des structures linéaires
    : Auteur(s) c mzk
'''

###################################################
########### Un MAILLON #############################
###################################################

class Maillon :
    '''
    une class epour un maillon
    '''
    def __init__(self, valeur = None, suivant = None) :
        '''
        initialise un maillon
        : params
            valeur (type inconnu)
            suivant (Maillon)
        '''
        self.valeur = valeur
        self.suivant = suivant
        
    def est_vide(self):
        '''
        renvoie True si la maillon est vide et False sinon
        : return (bool)
        '''
        return self.valeur == None
    
    def __repr__(self) :
        '''
        décrit l'instance
        '''
        return 'Maillon de valeur ' + str(self.valeur) +' et de suivant ' 
    
    def acc_valeur(self):
        '''
        accesseur de l'attribut valeur
        : return (?) valeur
        '''
        return self.valeur
    
   
    def acc_suivant(self):
        '''
        accesseur de l'attribut suivant
        : return (Maillon)
        '''
        return self.suivant
    
    def mut_valeur(self, val):
        '''
        modifie l'attribut valeur
        : param val (??)
        '''
        self.valeur = val
        
    def mut_suivant(self, maillon):
        '''
        modifie l'attribut suivant
        : param maillon (Maillon)
        '''
        self.suivant = maillon
        
###################################################
########### LES LISTES CHAINEES####################
###################################################   
        
class ListeChainee() :
    '''
    une classe pour implémenter une liste chainee
    '''
    def __init__(self):
        '''
        construit une liste chainee vide de longueur 0
        '''
        self.tete = None
        self.longueur = 0
        
    def __repr__(self):
        '''
        donne la classe de l'objet et sa taille
        : return (str)
        '''
        return 'Class Liste_chainées de taille ' + str(self.longueur)
    
    def ajouter(self, valeur):
        '''
        ajoute un maillon de valeur précisé en tête de liste
        : param valeur (???)
        : pas de return
        >>> l = ListeChainee()
        >>> l.ajouter(2)
        >>> l.tete.acc_valeur()
        2
        >>> l.ajouter(3)
        >>> l.tete.acc_valeur()
        3
        >>> l.tete.acc_suivant().acc_valeur()
        2
        >>> l.longueur
        2
        '''
        nv_maillon = Maillon(valeur, self.tete)
        self.tete = nv_maillon
        self.longueur += 1
        
    
    def est_vide(self):
        '''
        renvoie True si la liste est vide et False sinon
        : return (boolean)
        >>> l = ListeChainee()
        >>> l.est_vide()
        True
        >>> l.ajouter(1)
        >>> l.est_vide()
        False
        '''
        return self.tete == None
    
    def retirer(self):
        '''
        enlève si possible la tête de la liste chainee
        >>> l = ListeChainee()
        >>> l.ajouter(1)
        >>> l.retirer()
        >>> l.est_vide()
        True
        '''
        if not self.est_vide() :
            self.tete = self.tete.acc_suivant()
            self.longueur = self.longueur - 1
            
    def __str__(self):
        '''
        renvoie une chaine pour visualiser la liste
        : return (str)
        >>> l = ListeChainee()
        >>> print(l)
        ()
        >>> l.ajouter(1)
        >>> print(l)
        (1, ())
        >>> l.ajouter(2)
        >>> print(l)
        (2, (1, ()))
        '''
        chaine = '('
        # on s place sur le maillon de tete
        maillon = self.tete
        while maillon != None :
            # on ajoute la valeur du maillon à la chaine puis ', ('   
            chaine = chaine + str(maillon.acc_valeur()) + ', ('
            # on passe au maillon suivant
            maillon = maillon.acc_suivant()
        chaine = chaine + ')' * (self.longueur + 1)
        return chaine
    
    def acceder(self, n) :
        '''
        renvoie le maillon d'indice n s'il existe
        : param n (int) n < longeur de la liste
        : return (Maillon)
        >>> l =ListeChainee()
        >>> l.ajouter(4)
        >>> l.ajouter(3)
        >>> l.ajouter(2)
        >>> l.ajouter(1)
        >>> l.acceder(0).acc_valeur()
        1
        >>> l.acceder(2).acc_valeur()
        3
        '''
        assert isinstance(n, int) and n < self.longueur, 'index out of range'
        maillon = self.tete
        for i in range(n):
            maillon = maillon.acc_suivant()
        return maillon
    
    
    def inserer(self, n, valeur):
        '''
        insérer un maillon de valeur passée en paramètre
        à l'indice n
        : params
            n (int) 0 <= int <= self.longueur
            valeur (??)
                >>> l = ListeChainee()
        >>> l.ajouter('A')
        >>> l.inserer(1, 'B')
        >>> l.inserer(2, 'C')
        >>> print(l)
        (A, (B, (C, ())))
        '''
        assert isinstance(n, int) and 0<=n<=self.longueur, 'out of range'
        if n == 0 :
            self.ajouter(valeur)
        else :
            maillon_precedent = self.acceder(n - 1)
            nouveau_maillon = Maillon(valeur, maillon_precedent.acc_suivant())
            maillon_precedent.mut_suivant(nouveau_maillon) 
            self.longueur += 1
            
    def supprimer(self, n):
        '''
        supprime le maillon d'indice n
        : params
            n (int) 0 <= int < self.longueur
        >>> l = ListeChainee()
        >>> l.ajouter('A')
        >>> l.inserer(1, 'B')
        >>> l.inserer(2, 'C')
        >>> l.supprimer(1)
        >>> print(l)
        (A, (C, ()))
        >>> l.supprimer(1)
        >>> print(l)
        (A, ())
        '''
        assert isinstance(n, int) and 0<= n <self.longueur, 'out of range'
        if n == 0 :
            self.retirer()
        else :
            maillon_precedent = self.acceder(n - 1)
            maillon_precedent.mut_suivant(maillon_precedent.acc_suivant().acc_suivant()) 
            self.longueur -= 1
            
    def __getitem__(self, n):
        '''
        renvoie la valeur du maillon d'indice n
        : param n (int) 0<= n < longueur de la liste
        : return (???)
        >>> l = ListeChainee()
        >>> l.ajouter('A')
        >>> l.inserer(1, 'B')
        >>> l.inserer(2, 'C')
        >>> l[0]
        'A'
        >>> l[1]
        'B'
        >>> l[2]
        'C'
        '''
        assert isinstance(n, int) and 0<= n <= self.longueur, 'out of range'
        maillon = self.acceder(n)
        return maillon.acc_valeur()
        
    def __setitem__(self, n, valeur):
        '''
        remplace la valeur du maillon d'indice n par valeur
        : params
            n (int) 0<= n < longueur de la liste
            valeur (???)
        >>> l = ListeChainee()
        >>> l.ajouter('A')
        >>> l.inserer(1, 'B')
        >>> l.inserer(2, 'C')
        >>> l[0] = 1
        >>> print(l)
        (1, (B, (C, ())))
        >>> l[1] = 2 
        >>> print(l)
        (1, (2, (C, ())))
        >>> l[2] = 3
        >>> print(l)
        (1, (2, (3, ())))
        '''
        assert isinstance(n, int) and 0<= n <= self.longueur, 'out of range'
        maillon = self.acceder(n)
        maillon.mut_valeur(valeur)    
        
    def __len__(self) :
        '''
        renvoie la longueur de la liste
        : return (int)
        >>> l = ListeChainee()
        >>> len(l)
        0
        >>> l.ajouter(1)
        >>> len(l)
        1
        '''
        return self.longueur

###################################################
########### LES PILES #############################
###################################################
    

class Pile :
    '''
    implémentation d'une pile avec des maillons
    '''
    def __init__(self):
        '''
        construit une pile vide
        '''
        self.sommet = None

    def empiler(self, valeur):
        '''
        ajoute la valeur en tête de pile
        '''
        maillon = Maillon(valeur, self.sommet)
        self.sommet = maillon

    def est_vide(self):
        '''
        renvoie True si la pile est vide et False sinon
        : return
        boolean
        >>> p = Pile()
        >>> p.est_vide()
        True
        >>> p.empiler(1)
        >>> p.est_vide()
        False
        '''
        return self.sommet == None



    def depiler(self):
        '''
        enlève si possiblela valeur en tête de pile et la renvoie ou déclenche un message d'erreur si la pile est vide.
        : return
        type ?
        >>> p = Pile()
        >>> p.empiler(1)
        >>> p.empiler(2)
        >>> p.empiler('a')
        >>> p.depiler()
        'a'
        >>> p.depiler()
        2
        '''
        assert not self.est_vide(), 'On ne peut pas dépiler une Pile vide'
        valeur = self.sommet.acc_valeur()
        self.sommet = self.sommet.acc_suivant()
        return valeur

    def __str__(self):
        '''
        renvoie une chaine pour visualiser la pile
        : return
        type str
        >>> l = Pile()
        >>> l.empiler(1)
        >>> print(l)
        1
        _
        >>> l.empiler(2)
        >>> print(l)
        2
        1
        _
        '''
        chaine = ''
        maillon = self.sommet
        while maillon != None :
            chaine = chaine + str(maillon.acc_valeur()) + '\n'
            maillon = maillon.acc_suivant()
        chaine = chaine + '_'
        return chaine


    def __repr__(self):
        '''
        renvoie une chaine pour décrire la classe
        '''
        return 'Classe Pile'
    
###################################################
########### LES FILES #############################
###################################################
         
class File:
    '''
    une classe pour les Files
    '''
    def __init__(self):
        '''
        contruit une file vide de longueur 0 possédant une tete et une queue
        '''
        self.tete = None # premier maillon de la queue
        self.queue = None # dernier maillon de la queue

    
    def est_vide(self):
        '''
        renvoie True si la file est vide et False sinon
        >>> f = File()
        >>> f.est_vide()
        True
        '''
        return self.tete == None

    
    def enfiler(self, valeur):
        '''
         ajoute un élement à la file. On distinguera 2
         - le cas où la file est vide
         - le cas où la file n'est pas vide 
         - les autres cas.
        : param valeur (?) la valeur a ajouter
        >>> f = File()
        >>> f.enfiler(1)
        >>> f.enfiler(2)
        >>> f.est_vide()
        False
        '''
        nveau_maillon = Maillon(valeur, None)
        if self.est_vide() :
            self.tete = nveau_maillon
            self.queue = nveau_maillon            
        else :
            self.queue.mut_suivant(nveau_maillon)
            self.queue = nveau_maillon
            
            
            
        
    def defiler(self):
        '''
        renvoie la valeur en tête de file et la retire de la file si la file n'est pas vide. déclenche un message d'erreur sinon.
        : return (?)
        >>> f = File()
        >>> f.enfiler(1)
        >>> f.enfiler(2)
        >>> f.defiler()
        1
        >>> f.defiler()
        2
        >>> f.est_vide()
        True
        '''
        assert not self.est_vide(), 'On ne défile pas une file vide'
        valeur = self.tete.acc_valeur()
        self.tete = self.tete.acc_suivant()
        return valeur
        
        

    
    def __str__(self):
        '''
        renvoie la chaine permettant d'afficher la file
        : return (str)
        >>> f = File()
        >>> f.enfiler(3)
        >>> f.enfiler(2)
        >>> f.enfiler('a')
        >>> print(f)
        (3, (2, (a, ())))
        '''
        maillon = self.tete
        chaine = '('
        nb_parentheses = 1
        while maillon != None : 
            valeur = maillon.acc_valeur()
            chaine = chaine + str(valeur) + ', ('
            maillon = maillon.acc_suivant()
            nb_parentheses += 1
        chaine += ')' * nb_parentheses
        return chaine
            
        
            
        
    
    def __repr__(self):
        '''
        renvoie une représentation de la file
        : return (str)
        '''
        return 'Classe File'


###########################################
if __name__ == '__main__' :
    import doctest
    doctest.testmod(verbose = True)
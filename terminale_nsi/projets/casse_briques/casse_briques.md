# Le casse-briques

En cours, nous avons découvert la POO en codant un pong. On va poursuivre le projet ici on y ajoutant un mur de briques.

<img src="media/casse_briques.png" alt="le jeu" style="zoom:33%;" />

## La classe `Brique`

Dans un programme _module_briques.py_, nous allons coder la classe `Brique` :

### Initialisation

La classe `Brique` possède les attributs suivants :

* son _genre_ un entier entre 1 et 5;

* la position _x_ en abscisse du coin supérieur gauche (entre 10 et 260);

* la position _y_ en ordonnée du coin supérieur gauche (entre 10 et 200);

    

> * Coder la méthode `__init__` de paramètres _genre_, _x_, et _y_, qui initialise une instance de la classe `Brique`.
> * Coder la méthode `__repr__` qui en renvoie une description précisant le genre et la position de la brique.

### Affichage

Une brique est représentée par un rectangle positionné selon les coordonnées de la brique, de largeur 30 px, de hauteur 15px et dont la couleur dépend du type de la brique.

* la couleur _jaune_ (no 10) est réservée.
* La brique de type 3 sera invisible.

* La brique de type 4 est indestructible et sera de la même couleur que les bordures du jeu.

> * Coder la méthode `afficher` qui affiche une brique.
> * dans le _module_jeu_, ajouter temporairement, juste pour le test, quelques briques de types différents et affichez les via la méthode `afficher` de ce module.

Pour rappel :

![les couleurs](media/couleurs.jpg)

### Accesseur

On aura besoin de connaitre les coordonnées de chaque brique pour gérer les collisions avec la balle.

> Coder les accesseurs `acc_x` et `acc_y` qui renvoie l'attribut de la brique qui lui correspond.

### Touchée

Lorsqu'une brique est touchée par la balle, elle adopte les comportement suivants :

* ellse s'affiche en jaune (c'est pour cela que cette couleur était interdite plus haut);
* une brique de genre 2 devient une brique de type 1;
* Une brique de genre 5 va initialiser une nouvelle balle, aux coordonnées de la balle qui l'a touchée avec des attributs _dx_ et _dy_ non nuls et aléatoirement comprises entre -4 et 4. 

la méthode `touchee` est susceptible d'ajouter une balle à l'attribut _balles_ de la classe `Jeu` :

> Dans la classe `Jeu`, coder un mutateur `mut_balles(self, balle)` qui ajoute une nouvelle _balle_ au tableau _balles_.

On peut maintenant modifier depuis la classe `Brique` l'attribut _balles_ de la classe `Jeu` :

> Coder la méthode `touchee` qui donne aux briques le comportement souhaité.

## La classe `Niveau`

La classe `Niveau` sera codée dans `module_niveau.py`.

Elle s'initialise avec un paramètre _progression_ un entier et possède pour attribut unique un tableau _mur_ tout d'abord vide.

Selon la valeur du paramètre _progression_, le tableau _mur_ se remplira de briques en appelant la méthode adéquate.

> * Déclarer et initialiser la classe `Niveau`;
> * Coder la méthode `niveau_0` qui construit le _mur_ façon à ce qu'il contienne cinq lignes de huit briques de type 1. On l'appelera depuis la méthode `__init__` si le paramètre _progression_ vaut 0.



Lorsque le mur n'a plus de brique, le niveau est terminé.

> Coder l'accesseur `est_vide` qui renvoie _True_ si le mur n'a plus de brique et _False_ sinon.



Il faut pouvoir enlever une brique du mur du niveau en cas de collision avec une balle :

> Coder le mutateur `mut_mur`(self, balle) qui enlève du mur l'intance de `Balle` passée en paramètre.



## Revenons à la classe `Jeu`

Dans la classe `Jeu`, effectuer les modification ci-dessous :

* Dans `__init__`, un attribut `niveau`, instance de la classe `Niveau` initialisée en passant au paramètre _progression_ l'argument 0.

* dans `afficher`,  afficher chaque brique du niveau et préparer un affichage special en cas de victoire finale.

* dans `calculer` si le niveau est vide, l'attribut progression augmente de 1 :

    * Si on terminé le dernier niveau, la partie est gagnée et un affichage s'en suit.
    * sinon l'attribut _niveau_ est initialisée avec une nouvelle instance de `Niveau` en lui passant en paramètre la valeur de _progression_ et la raquette et la balle sont replacées en position initiale afin de passer au niveau suivant.

    

## Revenons à la classe `Balle`

Un balle peut toucher une brique :

* par le bas : dans ce cas elle _rebondit_ vers le bas.
* par le haut : dans ce cas elle rebondit vers le haut.
* par la gauche : dans ce cas elle rebondit à gauche
* par la droite : dans ce cas elle rebondit à droite.



>  Dans la classe `Balle`, coder la méthode `collision_mur(self, niveau)`  qui prend en paramètre une instance de la classe `Niveau` et qui vérifie pour chaque brique du niveau si la balle la touche. Si c'est le cas :
>
> * la méthode `touchee` de la brique est appelée.
> * la brique est retirée du _mur_ du niveau.

## Evolutions 

### Obligatoires 

* créer plusieurs niveaux en utilisant divers types de briques.

### Facultatives

* ajouter du son;

* ajouter un type de brique qui, lorsque la balla la touche, la fait accélérer;

* ajouter un type de brique qui, lorsque la balle la touche, diminue la taille de la raquette;

* ajouter un type de brique qui, lorsque la balle la touche, augmente la taille  de la raquette;

* D'autres idées ?? N'hésite pas !

    



___________

Par Mieszczak Christophe

Licence CC BY SA

Source images : production personnelle
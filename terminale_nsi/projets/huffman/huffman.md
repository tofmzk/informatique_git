# Le codage de Huffman

## La compression de données



La compression de données est un problème crucial que l'on retrouve pour de nombreux type de fichiers :



* les fichiers musicaux ont leur formats compressés :

    * [mp3](https://fr.wikipedia.org/wiki/MP3) propriétaire avec perte et donc une baisse de qualité.
    * [ogg](https://fr.wikipedia.org/wiki/Ogg) moins répandu mais pourtant libre et sans perte

* les images bien entendu :

    * [jpg](https://fr.wikipedia.org/wiki/JPEG) avec perte de données et donc baisse de la qualité
    * [png](https://fr.wikipedia.org/wiki/Portable_Network_Graphics) sans perte mais avec des fichiers nu peu plus lourds

* Les vidéos sont plus que jamais compressée. Un film en 4K de 1h30 à 20 images/seconde représente 7,5To de données. Aucun support actuel ne permet d'utiliser ce format sans compression, ces données étant réduite à quelques dizaines de Go ... avec pertes.

* Les documents de texte ont aussi leur importance : archivage de mails, wikipédia et ses 70 go de textes ... Différents algorithmes sont utilisés par divers logiciels pour en réduire la taille, sans perte de données qui rendrait leur lecture impossible :

    * [gzip](https://fr.wikipedia.org/wiki/Gzip) utilise le codage de Huffman, étudié dans ce TP, et l'algorithme [LZ77](https://fr.wikipedia.org/wiki/LZ77_et_LZ78)
    * [bzip2](https://fr.wikipedia.org/wiki/Bzip2) utilise également le codage de Huffman associé à la [transformée de Burroxs-Wheeler](https://fr.wikipedia.org/wiki/Transform%C3%A9e_de_Burrows-Wheeler)
    * [7-zip](https://fr.wikipedia.org/wiki/7-Zip) qui utilise l'algorithme [lzma](https://fr.wikipedia.org/wiki/LZMA) développée en 2001

    



## Le principe du codage de Huffman (1952)



### Exemple

On va coder chaque caractère en binaire avec un nombre de bits plus ou moins élevé selon sa fréquence d'apparition en utilisant un arbre.



Prenons par exemple la chaîne "aabbbcccc".

Que l'on utilise le code ASCII, une nome ISO ou l'UTF-8, cette chaîne de 9 caractères sera codée sur 9 octets soit 72 bits.

 

Complétez le tableau ci-dessous

| Lettre | Nombre d'occurrence(s) |
| :----: | :--------------------: |
|   a    |                        |
|   b    |                        |
|   c    |                        |



A partir de ce tableau, nous allons construire un arbre en utilisant une méthode gloutonne :

* Etape 1 : on construit des feuilles dont les étiquettes seront les tuples (lettre, nombre d'occurrences) et on les classes par ordre croisant de nombre d'occurrences. En reprenant les notations pour les arbres cela nous donnera la liste d'arbres ci-dessous :

    (('a', 2), Δ , Δ), (('b', 3), Δ,Δ), (('c', 4), Δ, Δ)

    

* Etape 2 : on regroupe les deux feuilles de moindre occurrences pour former un arbre dont la racine sera ('', somme des occurrences des sous-arbres), on retire les 2 feuilles de la liste et on insère le nouvel arbre dans notre liste de façon à ce qu'elle reste ordonnée.

    Le nouvel arbre est (('', 5), (('a', 2), Δ,Δ), (('b', 3), Δ, Δ))) : le nombre d'occurrences total de cet arbre est 5 = 2 + 3 . C'est cet arbre :

    ![aabbb](media/aabbb.jpg)

    La liste d'arbres ordonnée devient : (('c', 4), Δ, Δ), (('', 5), (('a', 2), Δ,Δ), (('b', 3), Δ, Δ)))

    

* Etapes suivantes : on recommence jusqu'à ce qu'il n'y ait plus qu'un seul arbre dans la liste. Ici, tout se passe rapidement puisque, à l'étape suivante nous n'aurons plus que l'arbre ci-dessous :

    en mode texte : (('', 9) , (('', 5), (('a', 2), Δ,Δ), (('b', 3), Δ, Δ))),   (('c', 4), Δ, Δ))

    en mode graphique :

    ![arbre de Huffman](media/aabbbcccc_non_pondere.jpg)

    

Ce dernier arbre restant est l'arbre de Huffman : il va nous permettre de coder et décoder nos données. Si cet exemple ne vous suffit pas, allez donc regarder cette petite vidéo sur la [construction d'un arbre de Huffman](https://www.youtube.com/watch?v=L9sblTQmaSo).



_Remarque :_ 

Sans pour autant commettre d'erreur, on aurait pu, selon la façon dont vous construisez vos arbres, selon que vous placiez un sous-arbre à droite ou à gauche, obtenir un arbre différent. Cela n'est en aucun cas un problème. Une fois l'algorithme écrit, il donnera toujours le même arbre de Huffman.



Pour coder, on recherche  la feuille correspondant au caractère à coder dans l'arbre.
* chaque fois qu'on va à gauche on ajoute un bit 0
* chaque fois qu'on va à droite, on ajoute un bit 1
* si on arrive à une feuille, on a trouvé le codage le caractère, et on passe au suivant.

L'arbre utilisé est alors celui-ci :

![ponderé](media/aabbbcccc.jpg)

Pour décoder, on lit les bits de gauche à droite :
* si on lit un 0, on va à gauche dans l'arbre.
* si on lit un 1, on va à droite dans l'arbre.
* si on est dans une feuille, on a trouvé le caractère à décoder et on revient à la racine de l'arbre.





> Donner un dictionnaire ayant pour clé les caractères de la chaîne et pour valeur leur codage binaire.
>
> ```python
> dic = {
> ```
>
> 
>
> Quel est le code binaire de la chaîne "aabbbccc" ? 
>
> ```txt
> 
> ```
>
> Quel est le taux de compression ?
>
> ```txt
> 
> ```
>
> _Remarque :_ en réalité, il faut sauvegarder le dictionnaire en plus des données afin de pouvoir coder et décoder le texte. Donc la compression n'est pas si bonne que cela, surtout si le texte est court.
>
> A quelle chaîne, en utilisant le même arbre, correspond le codage 1011001 ?
>
> ```txt
> 
> ```
>
> 



_Remarques importantes :_

Le codage est **réversible et non équivoque** : si on code, on peut toujours décoder,  et on ne peut que retrouver le texte initial.



### Principales tâches à accomplir 

Nous allons devoir :

* créer une fonction permettant d'importer le contenu d'un fichier texte dans une variable de type chaîne de caractères.
* créer une fonction qui renvoie le dictionnaire des occurrences dont les clés seront les différents caractères de la chaîne à compresser, et les valeurs, leur nombre d'occurrence(s).
* créer une fonction qui, à partir de ce dictionnaire, renvoie l'arbre de Huffman.
* créer une fonction qui, à partir de l'arbre de Huffman, renvoie un dictionnaire dont les clés sont les caractères et les valeurs leur codage binaire.
* créer une fonction qui renvoie une chaîne de caractères représentant les bits correspondant à notre chaîne compressée grâce à l'arbre.
* créer une fonction qui décode la chaîne de bits et renvoie la chaîne originale en se servant de l'arbre.



_Remarques :_

* on pourrait aller plus loin en ajoutant de quoi sauvegarder nos données compressées et le dictionnaire d'occurrence... mais on va se contenter de cela dans ce TP.
* Pour aller encore plus loin, on pourrait compresser des fichiers binaires en créant un arbre de Huffman à partir de chaque octet qui compose ce fichier et non pas à partir de caractères. Le principe est le même mais le codage permettra de compresser n'importe quel type de fichier.



## Implémentation

Nous allons utiliser notre module `module_arbres` qui définit la classe `Arbre`.

```python
import module_arbres
```

Toutes vos fonctions, sauf indications contraire, devront comporter :

* une spécification précise (Docstring)

* au moins un test

* des assertions pour vérifier les pré-conditions (type des paramètres et leur domaine précis)

* si nécessaire, un gestion des erreurs potentielles avec `try ... except`

    ​	

### Importer un texte

> Définissez et codez une fonction `importer_chaine(nom_fichier)` qui renvoie une chaîne des caractères contenu dans le fichier texte dont le nom est passé en paramètre.
>
> Ne réalisez pas de test pour cette fonction.



### Générer le dictionnaire



> Définissez et réaliser la fonction `generer_dic_occurrences(chaine)` qui renvoie le dictionnaire des occurrences qui correspond à la chaîne passée en paramètre.
>
> On pourra utiliser l'exemple vu plus haut comme test.
>
> ```python
> def generer_dic_occurrences(chaine):
>    	'''
>     	renvoie le dictionnaire pour le codage de Huffman correspondant à la chaine
>     	: param nom_fichier (str)
>     	: return (dict)
>     	>>> generer_dic_occurrence('aabbbcccc') == {...}
>     	True
>     	'''
>    ```
>    
>     



### l'Arbre de Huffman



Testez le code ci-dessous dans la console :

```python
>>> dic_occurrences = generer_dic_occurrences('aabbbccc')
>>> liste_tuples = sorted(dic_occurrences.items(), key = lambda x : x[1])
>>> liste_tuples
???
```

Vous avez donc à disposition une liste de tuples du genre (caractère, nombre d'occurrences).



> En utilisant la classe `Arbre` de `module_arbres`, créer la fonction `renvoyer_foret(dic_occurrences)` qui renvoie une _forêt_, c'est à dire une liste triée par ordre croissant d'occurrences des arbres/feuilles dont la racine a pour étiquette les tuples (caractère, nombre d'occurrences).
>
> On pourra utiliser l'exemple vu plus haut comme Doctest.
>
> ```python
> def renvoyer_foret(dic_occurrences):
>    	'''
>      	renvoie une liste triée par ordre croissant d'occurrences des arbres/feuilles dont la racine
>      	a pour étiquette les tuples (caractère, nombre d'occurrences).
>      	: param dic_occurrences(dict)
>      	: return une foret d'arbre (list of Arbre)
>      	>>> dic = generer_dic_occurrences('aabbbcccc')
>      	>>> foret = renvoyer_foret(dic)
>      	>>> foret
>      	[(('a', 2), ∆, ∆), (('b', 3), ∆, ∆), (('c', 4), ∆, ∆)]
> 	'''
>  ```
>  
> 



Nous disposons donc d'une forêt de feuilles. On va appliquer l'algorithme glouton déjà vu plus haut :

* tant que la forêt comporte plus d'un arbre :
    * on prend les deux premiers arbres de la forêt, donc les deux plus _légers_ puisque la forêt est triée.
    * on supprime ces deux arbres choisis de la forêt.
    * on crée un nouvel arbre dont les sous-arbres sont les deux arbres choisis au dessus, et pour lequel la racine a pour étiquette le tuple ('', la somme des occurrences des sous-arbres).
    * on insère le nouvel arbre dans la forêt de façon à ce qu'elle reste triée selon le nombre d'occurrences.

  

Insérer le nouvel arbre n'est pas si simple. Nous allons donc dédier une fonction à cette insertion.

> Codez la fonction `inserer(nouvel_arbre, foret)` qui insère le nouvel arbre à sa place dans la forêt.
>
> ```python
> def inserer(nouvel_arbre, foret):
> 	'''
> 	insére le nouvel arbre dans une forêt trié par poids croissant
> 	de façon à ce que la foret reste triée
> 	: param nouvel_arbre (Arbre)
> 	: param foret (list of Arbre)
> 	: return une foret (list of Arbre)
> 	>>> dic = generer_dic_occurrences('aabbbbccccc')
> 	>>> foret = renvoyer_foret(dic)
>  	>>> arbre1 = module_arbre.Arbre(('d',3))
>  	>>> arbre2 = module_arbre.Arbre(('e',6))
>  	>>> foret = inserer(arbre1, foret)
>  	>>> foret
>  	[(('a', 2), ∆, ∆), (('d', 3), ∆, ∆), (('b', 4), ∆, ∆), (('c', 5), ∆, ∆)]
>  	>>> foret = inserer(arbre2, foret)
>  	>>> foret
>  	[(('a', 2), ∆, ∆), (('d', 3), ∆, ∆), (('b', 4), ∆, ∆), (('c', 5), ∆, ∆), (('e', 6), ∆, ∆) ] 
>  	'''
>  ```
> 
> Remarque : regardez bien les Doctests. Il y a un cas général et un cas particulier à prendre en compte.

  

On peut maintenant coder notre algorithme glouton.



> Codez la fonction `generer_arbre(dic_occurrences)` qui renvoie l'arbre de Huffman.
>
> ```python
>def generer_arbre(dic_occurrences) :
>     	'''
>      	renvoie l'arbre de Huffman de la chaine,
>        obtenu en utilisant le dictionnaire passé en paramètre
>        : params
>        	chaine (str)
>        	dic_occurrence(dict)
>        : return un arbre de huffman (Arbre)
>    	'''
>    ```
>    
>    Testez ce code avec l'arbre obtenue pour la chaîne 'aabbbccc' pour laquelle on a construit l'arbre _à la main_ au début du TP. L'arbre que vous obtenez peut être différent en raison des symétries possibles entre les sous-arbres droit et gauche.



### Coder

Pour commencer, nous allons générer, grâce à l'arbre un dictionnaire de codage. Pour notre exemple précédent, ce serait `{'a' : '00', 'b' : '01', 'c' : '1'}`.



> Codez la fonction `generer_dic_codage(arbre, dic_codage, chemin = '')` qui modifiera par effet de bord le dictionnaire `dic_codage` passé en paramètre et initialement vide.
>
> Cette fonction est récursive : le `chemin` , vide par défaut, se construit au fur et à mesure.
>
> ```python
> def generer_dic_codage(arbre, dic_codage, chemin = '') :
>        '''
>        construit récursivement un dictionnaire prenant pour clé les feuilles de l'arbre et pour valeur la chaine
>        binaire obtenue selon le chemin emprunté pour atteindre la feuille
>        : param arbe(Arbre)
>        : param dic_codage(dict)
>        : param chemin(str) (vide à l'appel, utilisé dans la récursion pour construire le chemin)
>        : pas de return, EFFET DE BORD SUR dic_codage
>    	'''
>    ```
>    
>    



Il n'y a plus qu'à compresser !



> Codez la fonction `coder(chaine, dic_codage)` qui renvoie une chaîne de '0' et de '1' correspondant à la chaîne passée en paramètre compressée par la méthode de Huffman.
>
> On utilisera l'exemple du départ comme Doctest.
>
> ```python
> def coder(chaine, dic_codage):
>    	'''
>        renvoie une chaine de caractères, comportant des 0 ou des 1,
>        correspondant au codage de huffman de la chaine
>        : param chaine (str)
>        : return ( str)
>        '''
>    ```



### Taux de compression



> * Choisissez un long fichier texte (ce document par exemple) : quel est sa taille en bits ?
>
>     ```txt
>                             
>     ```
>
>     
>
> * Compressez le. indiquez les commandes tapez dans la console :
>
>     ```python
>                             
>     ```
>
>     
>
> * Quel est la taille en bits après compression ? 
>
>     ```txt
>                             
>     ```
>
>     
>
> * Quel est le taux de compression ?
>
>     ```txt
>                             
>     ```
>
>     

_Remarque :_

* Pour coder puis décoder, il faut pouvoir reconstituer notre arbre. Il faudra donc sauvegarder, en plus du fichier compresser, de quoi le retrouver, c'est à dire le dictionnaire des occurrences. Le codage de Huffman est donc très mauvais pour coder les chaînes très courtes, le poids du dictionnaire étant alors prépondérant.



### Décoder

A partir d'une chaîne de bits générer par codage d'un fichier texte, on doit maintenant retrouver le fichier texte.

Comme vu plus haut, pour chercher une feuille, on commence à la racine de l'arbre, puis, on lit la chaîne de bits de gauche à droite :

* si on lit un 0, on cherche dans le sous-arbre de gauche.
* si on lit un 1, on cherche dans le sous-arbre de droite.
* si on est sur une feuille, on a trouvé le caractère et on le renvoie.





> Codez la fonction `chercher_feuille(arbre, bin_str)` qui renvoie un tuple constitué : 
>
> * du caractère présent sur la feuille sur laquelle on arrive en suivant le chemin indiqué par la liste de bits de la chaîne `bin_str`. 
> * de la chaîne représentant le chemin parcouru depuis la racine jusqu'à cette feuille.
>
> Cette fonction est récursive : 
>
> * Quel est son cas d'arrêt ?
> * Quelle est la formule récursive ?
>
> On utilisera l'exemple du départ comme Doctest.
>
> ```python
> def chercher_feuille(arbre, bin_str) :
>     '''
>     renvoie la feuille atteinte en suivant le chemin indiqué par la chaine bin_str
>     en allant à gauche lorsqu'on lit un 0 et à droite sinon
>     : param arbre(Arbre)
>     : param bin_str(str)
>     : return (tuple) 
>     '''
> ```
> 
> 



Il n'y a plus qu'à faire la dernière fonction !



> Codez la fonction `decoder(bin_str, arbre)` qui renvoie la chaîne de caractère correspondant à la chaîne de bits passée en paramètre.
>
> N'oubliez pas d'ajouter un Doctest.
>
> ```python
> def decode_bin_str(bin_str, arbre):
        '''
        renvoie la chaine de carctère obtenue en décodant bin_str à l'aide de l'arbre
        : param bin_str (str)
        : param arbre(Arbre)
        : return (str)
        '''
    ```
>    
>    



### Test final



> * reprenez la chaîne de bits obtenue lors du calcul du taux de compression.
> * Décodez là !





___________

Par Mieszczak Christophe

licence CC BY SA

sources images : production personnelle.














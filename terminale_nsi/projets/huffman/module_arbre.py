import module_lineaire

class Arbre():
    '''
    une classe pour implémenter des arbres de façon récursive
    '''
    def __init__(self, etiquette = None, gauche = None, droit = None):
        '''
        construit un arbre vide avec un sous_arbre gauche et un sous arbre droit vides
        '''
        self.etiquette = etiquette
        self.sag = gauche
        self.sad = droit
        
    def est_vide(self):
        '''
        renvoie True si l'arbre est vide et False sinon
        : return (boolean)
        >>> a = Arbre()
        >>> a.est_vide()
        True
        '''
        return self.etiquette == None
        
    def renvoyer_sag(self):
        '''
        renvoie le fils gauche de l'arbre
        : return 
        type Arbre
        '''
        if self.sag == None :
            return Arbre()
        else :
            return self.sag

    def renvoyer_sad(self):
        '''
        renvoie le fils droit de l'arbre
        : return 
        type Arbre
        '''
        if self.sad == None :
            return Arbre()
        else :
            return self.sad
    
    def __str__(self):
        '''
        renvoie une chaîne de caractère représentant l'arbre
        '''
        if self.est_vide() :
            return '∆'
        else :
            return ( '('
                     + str(self.etiquette)
                     + ', '
                     + self.renvoyer_sag().__str__()        
                     + ', '
                     + self.renvoyer_sad().__str__()
                     +')'
                    )
    def taille(self) :
        '''
        renvoie la taille de l'Arbre
        : return (int)
        >>> a1.taille()
        5
        >>> f1.taille()
        1
        >>> a3.taille()
        3
        '''
        if self.est_vide() :
            return 0
        else :
            return 1 + self.renvoyer_sag().taille() + self.renvoyer_sad().taille()
    
    def hauteur(self) :
        '''
        renvoie la hauteur de l'arbre. l'arbre vide mesure -1 de haut
        : return (int)
        >>> a3.hauteur()
        1
        >>> f1.hauteur()
        0
        >>> a1.hauteur()
        2
        >>> Arbre().hauteur()
        -1
        '''
        if self.est_vide() :
            return -1
        else :
            return 1 + max(self.renvoyer_sag().hauteur(), self.renvoyer_sad().hauteur())
            
    def __repr__(self) :
        return self.__str__()
        
    def prefixe(self):
        '''
        renvoie une liste des étiquettes selon le parcours préfixe
        : return (list)
        >>> a1.prefixe()
        [1, 7, 3, 2, 1]
        '''
        if self.est_vide() :
            return []
        else :
            return ( [self.etiquette] 
                    + self.renvoyer_sag().prefixe()
                     + self.renvoyer_sad().prefixe()
                    )
    def infixe(self):
        '''
        renvoie une liste des étiquettes selon le parcours infixe
        : return (list)
        >>> a1.infixe()
        [7, 1, 2, 3, 1]
        '''
        if self.est_vide() :
            return []
        else :
            return ( self.renvoyer_sag().infixe()
                    + [self.etiquette]
                     + self.renvoyer_sad().infixe()
                    )
    def suffixe(self):
        '''
        renvoie une liste des étiquettes selon le parcours suffixe
        : return (list)
        >>> a1.suffixe()
        [7, 2, 1, 3, 1]
        '''
        if self.est_vide() :
            return []
        else :
            return ( self.renvoyer_sag().suffixe()
                    + self.renvoyer_sad().suffixe()
                    + [self.etiquette]
                   
                    )
        
    def largeur(self) :
        '''
        renvoie une liste des étiquettes parcourues en largeur
        : return (list)
        >>> a1.largeur()
        [1, 7, 3, 2, 1]
        '''
        file_arbres = module_lineaire.File()
        liste_etiq = []
        file_arbres.enfiler(self)
        while not file_arbres.est_vide() :
            arbre = file_arbres.defiler()
            if not arbre.est_vide() :
                liste_etiq.append(arbre.etiquette)
                file_arbres.enfiler(arbre.renvoyer_sag())
                file_arbres.enfiler(arbre.renvoyer_sad())
        return liste_etiq
            
            
class ABR(Arbre) :
    '''
    une classe pour implémenter des arbres binaire de recherche de façon récursive
    '''
    def __init__(self):
        '''
        construit un arbre vide avec un sous_arbre gauche et un sous arbre droit vides
        '''
        # on fait appelle au constructeur de la classe parente, la classe Arbre pour construire un ABR vide :
        super().__init__()
        
    def __repr__(self) :
        return ('Un ABR de taille '
                + str(self.taille())
                + ' est de hauteur '
                + str(self.hauteur())
                )
    
    def comp_defaut(a, b):
        '''
        renvoie -1 si a < b, 1 si a > b et 0 si a == b.
        : param a et b
        type comparables avec l'opérateur < ou >
        : return 0, -1 ou 1
        type int
        '''
        if a < b :
            return -1
        elif a > b :
            return 1
        else :
            return 0
        
    def inserer(self, cles, comp = comp_defaut):
        '''
        insérer une clé ou une liste de clés à sa place dans l'ABR si celle-ci n'y est pas déjà
        : param 
           cles (? ou list de clé)
           comp (function)le comparateur utilisé
        : pas de return mais EFFET DE BORD SUR self
        >>> a = ABR()
        >>> a.inserer(8)
        >>> print(a)
        (8, ∆, ∆)
        >>> a.inserer([3, 1, 6, 4, 7, 10, 14, 13])
        >>> print(a)
        (8, (3, (1, ∆, ∆), (6, (4, ∆, ∆), (7, ∆, ∆))), (10, ∆, (14, (13, ∆, ∆), ∆)))
        '''
        if type(cles) != list :
            cles = [cles]
        for cle in cles : # pour chaque clé de la liste de clés
            if self.est_vide() :
                self.etiquette = cle
                self.sag = ABR()
                self.sad = ABR()
            elif comp(cle, self.etiquette) == -1 :                
                self.renvoyer_sag().inserer(cle, comp)
            elif comp(cle, self.etiquette) == 1 :
                self.renvoyer_sad().inserer(cle, comp)
            else :
                raise "clé déjà présente dans l'ABR"
        
    def rechercher(self, cle, comp = comp_defaut) :
        '''
        Renvoie True si la clé est presente et False sinon
        : param cle (?)
        : return (boolean)
        >>> a = ABR()
        >>> a.inserer(8)
        >>> a.inserer([3, 1, 6, 4, 7, 10, 14, 13])
        >>> a.rechercher(6)
        True
        >>> a.rechercher(13)
        True
        >>> a.rechercher(3)
        True
        >>> a.rechercher(9)
        False
        '''
        if self.etiquette == cle :
            return True
        elif self.est_vide() :
            return False
        elif comp(cle, self.etiquette) == -1 :
            return self.renvoyer_sag().rechercher(cle)
        else :
            return self.renvoyer_sad().rechercher(cle)


    
    
    
        
    
        
        
    
            
           
        
################DOCTEST
        
if __name__ == '__main__' :
    f2 = Arbre(2)
    f1 = Arbre(1)
    f7 = Arbre(7)
    a3 = Arbre(3, f2, f1)
    a1 = Arbre(1, f7, a3)    
    import doctest
    doctest.testmod(verbose = False)
    
########################################################
###### doctest : test la correction des fonctions ######
########################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = True) 
        
        
    

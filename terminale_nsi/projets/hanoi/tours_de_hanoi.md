# Tours de Hanoï



## Règle du jeu



Les tours de Hanoï est un _casse_tête_ qui consiste à déplacer tous les disques empilés sur une tour de départ vers une tour de destination, en se servant d'une tour intermédiaire avec pour seule règle le fait de ne pouvoir poser un disque que sur une tour vide ou un disque plus grand.



Une petite photo d'un jeu en bois avec 8 disques sur la tour de départ :

![8 disques](https://upload.wikimedia.org/wikipedia/commons/0/07/Tower_of_Hanoi.jpeg)





Voici, pour 4 disques,  les étapes de la résolution :



![4 disques](https://upload.wikimedia.org/wikipedia/commons/6/60/Tower_of_Hanoi_4.gif)



>
>
>Résolvez _à la main_ les tours de Hanoï lorsque la première tour ne comporte que trois disques
>
>



## Définition récursive



Il existe différentes méthodes pour résoudre ce problème. Nous allons utiliser une définition récursive.



Considérons une situation où la tour de départ compte _n > 1_ disques_ à déplacer vers la tour de destination :![situation de départ](media/etape1.png)





Pour déplacer les _n_ disques d'une tour de départ vers une tour de destination, via une tour intermédiaire, il y a deux cas de figure.



**Soit n = 1** : Dans ce cas, la tour de départ ne comporte qu'un seul disque qu'il suffit de déplacer directement vers la tour de destination.



**Soit n > 1**:  Dans ce cas, il _suffit_, de réaliser les mouvements suivants :



* Déplacer les _n - 1_ premier(s) disque(s) de la tour de départ vers la tour intermédiaire

    ![déplacer n - 1 disques](media/etape2.png)

* Déplacer le dernier disque de la tour de départ vers la tour de destination :

    ![déplacer dernier disques](media/etape3.png)

    

* Déplacer les _n - 1_ disques de la tour intermédiaire vers la tour de destination.![déplacer n - 1 disques vers la destination](media/etape4.png)







A chaque étape, nous avons une tour de `départ`, une tour de `destination` et une tour `intermédiaire`. Il ne s'agit pas des mêmes tours à chaque fois. 



>
>
>Donner une définition récursive pour déplacer _n_  disques de la tour de _départ_ vers la tour de _destination_, via la tour _intermédiaire_ si nécessaire :
>
>$`deplacer( n, depart, destination, intermediaire) = \left\lbrace \begin{array}{}  cas~d'arrêt~si~ n = 1 \\ \left\lbrace \begin{array}{}  Etape~1  \\ Etape~2  \\ Etape~3 \end{array} \right. \end{array} \right. sinon`$
>
>



## Implémentation

### Structure de données



* Chaque tour est un empilement de disque(s) : la structure de pile est donc incontournable ! Nous aurons besoin de notre `module_lineaire`.

* Le jeu comporte trois tours : nous allons utiliser 3 piles qui seront _depart_, _intermediaire_ et _destination_.

* Pour chaque pile, un disque sera représenté par un entier correspondant à la taille du disque. Ainsi ,pour une pile de 3 disques cela donnera :

    ```python
    1 
    2
    3
    ```

    

### La classe `Hanoi`



Nous coderons la _classe_ `Hanoi` dans un module que l'on appellera `module_hanoi.py`.



> Définissez et construisez la classe `Hanoi` qui possède quatre attributs :
>
> * la _hauteur_, passée en paramètre dans l'initialisateur, correspond au nombre total de disques sur la tour de départ, numérotés de 1 (pour le plus petit disque tout en haut) à _hauteur_ (pour le plus grand tout en bas).
> * les trois piles  _depart_, _intermediaire_ et _destination_ : 
>
> On empilera dans la pile _depart_  les anneaux correspondants à la hauteur tandis que les deux autres piles seront vides.



Les  méthodes de cette classe sont :

* `depiler(self, depart, destination)` qui dépile la tour de départ dans la tour de destination.

* `est_termine(self)` qui renvoie _True_ si le jeu est terminé et _False_ sinon.

* `__repr__(self)` qui renvoie simplement la chaîne "Class Tour de hauteur ....".

* ` __str__(self)` qui renvoie une chaîne de caractères pour afficher les 3 tours, par exemple, sous la forme suivante où chaque entier représente la taille du disque :

    ```txt
     Tour no 0 : 
    1 
    4 
    
     Tour no 1 : 
    2 
    3 
    
     Tour no 2 : 
    5 
    6 
    7 
    8 
    ```




_Remarque :_

On pourra utiliser la méthode `__str__` des piles, par exemple :

```python
	chaine_pile_depart = self.depart.__str__()
```





### Résolution du problème

Nous avons besoin de trois méthode

* `resoudre(self)` qui replace le jeu en position initiale puis lance la résolution en utilisant les fonctions ci-dessous :
    * `deplacer(self, nbre_disques, depart, destination, intermediaire)` qui déplace _nbres_disques_ du  départ jusqu'à la destination, via l'_intermédiaire_ si nécessaire,  en utilisant la définition récursive.
    * `afficher_coup(self, depart, destination)` qui affiche la situation après un déplacement de départ vers destination en précisant le coup qui vient d'être joué.




Voici, par exemple, le déroulé de la résolution du jeu avec 3 disques au départ :

```python
>>> t = Hanoi(3)
>>> t.resoudre()
____________________départ________________________
 
 Tour no 0 : 
1 
2 
3 

 Tour no 1 : 

 Tour no 2 : 

________de 0 vers 2 _________
 
 Tour no 0 : 
2 
3 

 Tour no 1 : 

 Tour no 2 : 
1 

_________de 0 vers 1 _________
 
 Tour no 0 : 
3 

 Tour no 1 : 
2 

 Tour no 2 : 
1 

________de 2 vers 1 _________
 
 Tour no 0 : 
3 

 Tour no 1 : 
1 
2 

 Tour no 2 : 

________de 0 vers 2 _________
 
 Tour no 0 : 

 Tour no 1 : 
1 
2 

 Tour no 2 : 
3 

________de 1 vers 0 _________
 
 Tour no 0 : 
1 

 Tour no 1 : 
2 

 Tour no 2 : 
3 

________de 1 vers 2 _________
 
 Tour no 0 : 
1 

 Tour no 1 : 

 Tour no 2 : 
2 
3 

________de 0 vers 2 _________
 
 Tour no 0 : 

 Tour no 1 : 

 Tour no 2 : 
1 
2 
3 


```



## On joue 

Créez un fichier _Hanoi.py_ afin de jouer aux tours de Hanoï.

Le programme permet à un utilisateur de lancer le jeu avec un certain nombre d'anneaux.

Tant que le jeu n'est pas terminé et que le joueur n'a pas abandonné :

* La situation en cours est affichée
* L'utilisateur demande un déplacement d'une tour vers une autre ou abandonne :
    * Si le coup est valide, on le joue.
    * Si le joueur abandonne, on lance la résolution du jeu.

Si le joueur gagne, on le félicite en affichant le nombre de coups qui ont été nécessaires.



**ATTENTION : On respecte les bons principes !**

On utilise des fonctions d'_interface utilisateur_ pour demander ou afficher : Pas de _print_ ou de _input_ en dehors de ces fonctions.



### Evolution ?

A faire éventuellement : réaliser une interface graphique avec _pygame_ ou _tkinter_.



____________

Mieszczak Christophe

Licence CC BY SA

sources images :

* [Tours en bois, wikipédia, CC BY SA ,auteur Ævar Arnfjörð Bjarmason ](https://commons.wikimedia.org/wiki/File:Tower_of_Hanoi.jpeg)

* [animation Hanoï, wikipédia, CC BY SA, auteur Aka](https://commons.wikimedia.org/wiki/File:Tower_of_Hanoi_4.gif)

* autres images : production personnelle

    
# Calculs et fractions

Ceci est un projet à mener à deux. Il demande de maîtriser les opérations de base sur les fractions apprises au collège.





## La classe`Fraction`



L'objectif ici est de construire la classe `Fraction` dans un module _module_fraction.py_

Chaque instance de cette classe aura deux attributs entiers :

* `num` le numérateur.
* `den` le dénominateur qui doit être non nul



Voici l'interface demandée pour ce du module :

```python
class Fraction(builtins.object)
 |  Fraction(numerateur, denominateur = 1)
 |  
 |  Methods defined here:
 |  
 |  __add__(self, frac)
 |      renvoie une Fraction égale à la somme de self et frac.
 |      : param frac (Fraction)
 |      : return (Fraction)
 |      : Pas d effet de bord.
 |  
 |  __eq__(self, frac)
 |      renvoie True si self et frac sont égales. 
 |      : param frac (Fraction)
 |      : return (boolean)
 |      : Pas d effet de bord.
 |  
 |  __gt__(self, frac)
 |      renvoie True si self > frac. 
 |      : param frac (Fraction)
 |      : return (boolean)
 |      : Pas d effet de bord.
 |  
 |  __init__(self, numerateur, denominateur = 1)
 |      Initialise l'objet
 |      : param numerateur (int)
        : param denominateur (int) non nul, vaut 1 par défaut
 |      
 |  
 |  __lt__(self, frac)
 |      renvoie True si self < frac et False sinon.
 |      : param frac (Fraction)
 |      : return (boolean)
 |      : Pas d effet de bord.
 |  
 |  __mul__(self, frac)
 |      renvoie une Fraction égale au produit de self et frac. 
 |      : param frac (Fraction)
 |      : return (Fraction)
 |      : Pas d effet de bord.
 |  
 |  __pow__(self, n)
 |      renvoie self ** n. 
 |      : param n (int) positif ou negatif
 |      : return (Fraction)
 |      : Pas d effet de bord.
 |  
 |  __sub__(self, frac)
 |      renvoie une Fraction égale à la différence de self et frac.
 |      : param frac (Fraction)
 |      : return (Fraction)
 |      : Pas d effet de bord.
 |  
 |  __truediv__(self, frac)
 |      renvoie une Fraction égale au quotient de self par frac. Déclenche une exception  |      si frac est nul.
 |      : param frac (Fraction)
 |      : return (Fraction)
 |      : Pas d effet de bord.
 |  
 |  evaluer(self, n = 10)
 |      renvoie une évaluation de la fraction à n chiffres après la virgule
 |      : param n (int) entier entre 1 et 10 qui vaut 10 par défaut
 |      : return (float)
 |  
 |  inverser(self)
 |      Renvoie la fraction inversée de self. Déclenche une exception si
 |      self est nul.
 |      : return (Fraction)
 |      : Pas d effet de bord. 
 |  
 |  simplifier(self)
 |      renvoie une fraction égale à self simplifiée au maximum.
 |      : return (Fraction)
 |      : Pas d effet de bord sur self    
 |
 |  __repr__(self)
 |      renvoie une chaine représentant la fraction et une valeur approchée 
 |      : return (str)
 |
 |  __str__(self)
 |      renvoie une chaine pour la commande print sur la fraction
 |      : return (str)
 |  
```



* Une autre méthode est également indispensable à la simplification des fractions : le calcul du `pgcd` de deux entiers positifs. Vous trouverez [ICI](https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide) des explications sur les algorithmes permettant de le calculer. Cette méthode n'étant pas dans l'interface, il faudra la rendre privée une fois le projet terminé.



* Pour pouvoir faire des Doctests, la méthode  `__eq__` est à faire en priorité. Elle  correspond au test d'égalité `==` entre deux fractions. 

    

* Pour avoir une représentation d'une instance de  `Fraction`, la méthode `__repr__` est également prioritaire.



Pour vous aider, voici un petit exemple avec la méthode réservée `__add__(self, frac)`

```python
    def __add__(self, frac):
        '''
        renvoie une Fraction égale à la somme de self 
        : param frac
        type Fraction ou int
        : return
        Fraction
        >>> Fraction(1, 3) + Fraction(1, 2) == Fraction(5, 6)
        True
        '''
        resultat = Fraction(self.__num * frac.__den + frac.__num * self.__den,
                            self.__den * frac.__den) # on construit le résultat de l'opération qui est une fraction
        return resultat # on renvoie le résultat 
```



Dans la console, cette méthode serait utilisée de la façon suivante, une fois la méthode `__repr__` réalisée :

```python
>>> f = Fraction(1, 3)
>>> g = Fraction(1, 2)
>>> f + g
5 / 6 ~ 0.8333333333333334
```



## En avant !



Voici les méthodes classées par difficultés :

* Difficulté 1 : 
    * `__init__`
    * `__repr__`
    * `evaluer`
    * `simplifier` (à condition d'avoir codé le pgcd)
    * `inverser`
    * `__gt__` et `__lt__`
    * `__sub__`
* Difficulté 2 : attention à ne pas faire d'effet de bord !
    * `__mul__`
    * `__truediv__`
    * `__eq__`
* Difficulté 3 : c'est pas super difficile non plus ...
    * `pgcd(a, b)`
    * `__pow__` 



Avant de commencer à coder :

* faites une liste des différentes tâches à accomplir.
* jugez la difficulté de chaque tâche.
* répartissez vous les tâches équitablement 



Dans les docstrings des méthodes, précisez l'auteur de la méthode.

N'oubliez pas les doctests et les assertions !!

Commentez chaque ligne de votre code !

_________________________________________

_Par Mieszczak Christophe, licence CC-BY-SA_





​    
# Labyrinthe partie 3

## Pygame

Nous allons utiliser le module [Pygame](https://www.pygame.org/wiki/tutorials) pour réaliser une jolie interface pour notre labyrinthe.

Voici un rendu finale possible :

![aperçu](media/apercu.jpg)

L'utilisateur déplacera le personnage (ici une araignée) avec les flèches du clavier et la touche `ESC` pour demander la solution.

Nous allons pou cela faire évoluer le code de notre module `module_laby`.



## Interface Pygame

Le module `module_laby` propose la méthode `__str__(self)` pour afficher le labyrinthe dans la console. Nous allons lui ajouter la méthode `py_play(self)` qui donnera un rendu du labyrinthe en mode graphique, dans une fenêtre `pygame`.

### Dimension de la fenêtre

Une cellule du labyrinthe peut être :

* un mur
* le personnage
* la sortie

Toutes les cellules sont carrées et de même dimension. Le labyrinthe complet ne doit pas dépasser  1024 pixels de large sur 768 pixels de haut.

> * Complétez la méthode ci_dessous :
>
> ```python
>     def py_dimensions(self):
>         '''
>         renvoie la taille d'une cellule de façon à ce que le labyrinthe entier
>         ait pour dimensions maximales (1024, 768)
>         : return (tuple of int)
>         '''
> ```
> *  Testez dans la console :
>
>     ```python
>     >>> lab = Labyrinthe('lab1.txt')
>     >>> lab.py_dimension()
>     >>> lab = Labyrinthe('lab3.txt')
>     >>> lab.py_dimensions()
>     ```
> 



### Initialiser une fenêtre

Voici comment initialiser une fenêtre graphique :

```python
import pygame as py #on utilise py plutôt que pygame dans la suite

py.init() # initialise une fenêtre
py.display.set_caption("titre") # affiche le texte dans la barre de titre
window = py.display.set_mode((largeur, hauteur)) # fixe les dimension en pixel
window.fill((r, g, b)) # remplit l'arrière plan dans la couleur (r, g, b)
```

> * importer `pygame` dans notre module.
>
> * Compléter la fonction ci-dessous afin d'initialiser une fenêtre aux dimensions souhaitées.
>
> ```python
>     def py_print(self):
>         '''
>         affiche le labyrithe dans une fenêtre pygame
>         '''
> ```



### Les sprites



Nous aurons besoin de  trois images, de dimension 100 pixels sur 100 pixels :

* une pour le personnage à déplacer.
* une qui servira pour chaque mur.
* une pour représenter la sortie.



> Choisissez trois images libres de droit ou dessinez les et sauvegardez les sous les noms :
>
> * `mur.jpg`
> * `sortie.jpg`
> * `personnage.jpg`



Voici les méthodes utilisées pour utiliser ces sprites :

```python
mur = py.image.load('mur.jpg').convert_alpha() # charge l'image spécifiée dans la variable mur
mur = py.transform.scale(mur,(longueur, largeur))#redimensionnel'image
window.blit(mur, (x, y)) # affiche l'image à partir du pixel de coordonnées (x, y)
py.display.update() # met à jour les modifications affichées dans la fenêtre
```



> * Complétez la méthode `py_print` de façon à ce qu'elle affiche les murs du labyrinthe, le personnage et la sortie.
>
> * Testez dans la console avec des labyrinthes de dimensions différentes.
>
>     ```python
>     >>> lab = Labyrinthe('lab1.txt')
>     >>> lab.py_print()
>     >>> lab = Labyrinthe('lab3.txt')
>     >>> lab.py_print()
>     ```
>     



### Gestion des événements

Nous allons maintenant utiliser le paradigme événementiel en réagissant à des événements : l'enfoncement d'une touche du clavier.

> Définissons une nouvelle méthode `jouer(self)` pour dérouler le jeu. 
>
> Voici le début du code :
>
> ```python
>     def jouer(self):
>         '''
>         déroule un jeu lors duquel le joueur propose des direction pour
>         se déplacer dans le but de sortir du labyrinthe
>         '''
>         self.py_print()
>         while not self.est_gagne():
>             # ici on gérera les événements
> ```
>
> 

voici comment on gère les événements avec `pygame` :

```python
for event in py.event.get(): # pour tout événement déclenché
	if event.type == py.QUIT:  # s'il est du type QUIT alors ...
    	raise SystemExit # on quitte 
    elif event.type == py.KEYDOWN: # s'il est du type KEYDOWN ...
    	print(event.key) # on affiche le code de la touche enfoncée.
```

> * Déterminez le code des touches correspondant aux flèches et à `ESC`.
>
> * Complétez `jouer(self)` de façon à ce qu'enfoncer une flèche déclenche un mouvement du joueur et un rafraîchissement de l'affichage. 
>
>     

_Remarque :_

`pygame` gère de nombreux événements : presser ou relâcher une touche du clavier, bouger la souris, cliquer sur un bouton de la souris et même l'utilisation de joystick ... Une petite recherche sur le web vous permettra de compléter vos connaissances.



### Dérouler la solution



Pour dérouler la solution, on procède exactement comme on le faisait en mode texte. Mais :

* au lieu d'un `print(self)`, on utilisera `self.py_print()`
* On utilisera la méthode `py.time.wait(temps_en_ms)` pour ralentir le déplacement du personnage.



> * Complétez la méthode ci-dessous :
>
>     ```python
>          def py_derouler_solution(self):
>             '''
>             déroule la solution dans la fenêtre pygame
>             '''
>             parent = self.sortir()
>     ```
>
> * Modifiez la méthode `jouer(self)` de façon à réagir à l'événement "presser la touche `ESC`" en déclenchant la solution.



_______

Par Mieszczak Christophe licence CC BY SA

source image : production personnelle


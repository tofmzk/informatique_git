# Labyrinthe partie 2 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Die_Gartenlaube_%281885%29_b_787_1.jpg/600px-Die_Gartenlaube_%281885%29_b_787_1.jpg" alt="labyrinthe" style="zoom:50%;" />

Nous avons codé un objet labyrinthe en début d'année. Reprenons en le code pour y ajouter les méthodes permettant de trouver le plus court chemin vers la sortie de ce labyrinthe. 



Ce type de problème est un problème courant dans les jeux vidéos :

* un des fantôme du Pacman court après le pacman : il cherche le chemin le plus court pour le rejoindre.
* dans un jeu lorsque vous cliquez à un endroit de la carte pour que vos personnages le rejoignent, ils doivent d'abord calculer un chemin pour s'y rendre.



## Le principe



Nous allons modéliser notre labyrinthe par un graphe : 

* chaque sommet est une position possible du joueur dans le labyrinthe
* chaque sommet et lié aux sommets correspondants à des positions qu'il est possible d'atteindre à partir de la position que représente le sommet. Les positions atteignables sont les voisins de la position du joueur.
* pour sortir du labyrinthe, on va chercher un chemin dans le graphe depuis le sommet correspondant à la position du joueur jusqu'à celui correspondant à la sortie.



Concrètement, nous n'avons pas besoin de recourir aux classes que nous avons implémenter. Nous allons procéder en trois grandes étapes :

* la recherche d'un chemin. 

* la création d'une pile de positions à prendre.

* le déroulement de la solution.

    

Pour trouver le chemin le plus court vers la sortie :

* On définit un dictionnaire _parent_ qu'on initialise avec pour seul clé la position de départ du joueur qui n'a aucun parent :

    ```python
    parent = {self.joueur : None}
    ```

* On construit une file vide et on y enfile la position initiale du joueur dans le labyrinthe.

* Tant que la file n'est pas vide et qu'on a pas gagné :

    * on place le joueur à la première position de la file.

    * on génère la liste des voisins de cet élément.

    * pour chaque voisin de la liste de voisins :

    * si ce voisin n'a pas été déjà visité (n'est donc pas dans `parent`)  alors :

        * le parent de ce voisin est la position du joueur.
        
        

* Une fois le chemin déterminé, grâce au dictionnaire `parent`, nous allons pouvoir construire la pile des positions à prendre pour faire sortir le joueur du labyrinthe :

    * à cet endroit du programme, la position du joueur est sur la sortie puisque l'étape précédente a été effectué avec succès.
    * on construit une pile vide.
    * on ajoute la position de la sortie à cette pile.
    * Tant que le parent du joueur n'est pas vide (dans ce cas on serait arrivé à son position initial) :
        * on empile le parent de la position du joueur dans notre pile
        * on place le joueur sur la position parent.
    * A la fin de cette boucle, on obtient une pile de position. Le haut de la pile est le dernier parent non `None`, c'est à dire la position initiale du joueur. Le bas de la pile est la sortie. En dépilant, nous allons pouvoir déplacer le joueur vers la sortie.
    
* Il nous reste maintenant à dépiler la solution :
  
    * Tant que la pile n'est pas vide :
        * on la dépile
        * on place le joueur sur la position dépilée
        * on affiche le labyrinthe
        * on marque une pause (grâce à un `input` par exemple)



## Questions centrales



* Où ajouter les voisins dans la liste `positions_possibles`  pour obtenir le plus court chemin ? En tête ? En queue ? Argumentez !
* Pourquoi utiliser une pile, plutôt qu'une file ou une liste, pour remonter les parents depuis la sortie jusqu'à la position initiale du joueur ?





## Implémentation



Voici le squelette de notre implémentation. Complétez le :



```python
# -*- coding: utf-8 -*-
'''
    module labyrinthe
    : Auteurs mieszczak christophe
'''
import module_lineaire

class Labyrinthe():
    '''
    Définit la classe Labyrinthe
    '''
    def __lire(nom_fichier):
        '''
        lit le fichier txt passé eb paramètre et renvoie la structure du labyrinthe
        : param nom_fichier
        type str
        return laby
        type list of list
        '''
        assert type(nom_fichier) is str,'nom_fichier is str'
        try :
            lecture = open(nom_fichier, 'r',encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
        except FileNotFoundError :
            raise # renvoie une erreur si le fichier n'existe pas
        toutes_les_lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
        lecture.close()
        laby = []
        for i in range(0, len(toutes_les_lignes)):
            laby.append([])
            for elt in toutes_les_lignes[i] :
                if elt != '\n':
                    laby[i].append(elt)
        return laby

    def __position_joueur(self):
        '''
        renvoie la position du joueur dans le labyrinthe
        '''
        for y in range(self.__hauteur) :
            for x in range(self.__largeur):
                if self.__laby[y][x] == 'X' :
                    self.__laby[y][x] = ' '
                    return  x, y 
        return None, None
    
    def __position_sortie(self):
        '''
        renvoie la position de la sortie du labyrinthe
        '''
        for y in range(self.__hauteur) :
            for x in range(self.__largeur):
                if self.__laby[y][x] == 'S' :                    
                    return  x, y 
        return None, None
    
    def __init__(self, nom_fichier):
        '''
        construit un objet labyrinthe à partir du fichier txt passé en paramètre
        : param nom_fichier
        type str
        '''
        self.__laby = Labyrinthe.__lire(nom_fichier)
        self.__hauteur = len(self.__laby)
        self.__largeur = len(self.__laby[0])
        self.__joueur = self.__position_joueur()
        self.__sortie = self.__position_sortie()
    
        
    def __futur_position(self, direction):
        '''
        renvoie la prochaine position du joueur s'il se déplace dans la direction précisée
        '''
        directions = { 'n' : (0, -1),
                       's' : (0, 1),
                       'e' : (1, 0),
                       'o' : (-1, 0)
                     }
        return (self.__joueur[0]+directions[direction][0],
                self.__joueur[1]+directions[direction][1]
                )
        
    
    def __est_vide(self, position):
        '''
        renvoie vrai si la position est vide
        : param position
        type tuple
        '''
        return (self.__laby[position[1]][position[0]] == ' '
                or self.__laby[position[1]][position[0]] == 'S'
                )
    
    def est_gagne(self):
        '''
        renvoie vrai si la partie est finie
        type str
        : return
        type boolean
        '''
        return self.__joueur == self.__sortie

    
    def est_possible(self, direction):
        '''
        renvoie vraie si le déplacement dans la direction est possible
        : param direction 'n', 's' , 'e' ou 'o'
        type str
        return boolean
        '''
        j_pos = self.__futur_position(direction)
        return ( j_pos[0] > -1
                 and j_pos[0] < self.__largeur
                 and j_pos[1] > -1
                 and j_pos[1] < self.__hauteur
                 and self.__est_vide(j_pos) 
                )
                
    
    def deplacer(self, direction):
        '''
        déplace le joueur dans la direction souhaitée si possible
         : param direction 'n', 's' , 'e' ou 'o'
        type str
        EFFET DE BORD sur le labyrinthe
        '''
        if direction in 'nseo' and len(direction) == 1 :
            if self.est_possible(direction):
                self.__joueur = self.__futur_position(direction)     
            
        
    def __repr__(self):
        '''
        renvoie une représentation du labyrinthe
        '''
        return self.__str__()
    
    def __str__(self):
        '''
        renvoie une représentation du labyrinthe via un print
        '''
        chaine = ''
        for y in range(self.__hauteur) :
            for x in range(self.__largeur):
                if (x, y) == self.__joueur :
                    chaine += 'X'
                elif (x, y) == self.__sortie :
                    chaine += 'S'
                else :
                    chaine += self.__laby[y][x]
            chaine += '\n'
        return chaine
    
    def renvoyer_voisins(self):
        '''
        renvoie une liste de tuple des positions voisines de la position et la direction empruntée pour y arriver
        : param position
        type tuple
        : return
        type tuple
        '''

    
    
    def sortir(self):
        '''
        renvoie le dictionnaire de parents pour sortir du labyrinthe ou None s'il n'y a pas
        de sortie possible.
        : return (dict)
        '''

            
        
    
    def renvoyer_pile(self, parent) :
        '''
        renvoie une pile de positions depuis la sortie (en bas de la pile) jusqu'au départ(en haut)
        grâce au dictionnaire parent
        : param parent
        type dict
        : return
        type Pile
        '''
            
    
    def derouler_solution(self):
        '''
        déroule la solution et affiche les labyrinthes successif en faisant passer le joueur
        de la position de départ à la sortie.
        : param parent
        type dict
        '''

            
```

Testons  :

```python
>>> l = Labyrinthe('lab1.txt')
>>> l.derouler_solution()
???
```





### Jouer 



`module_labyrinthe` est complété. Il nous reste à modifier le programme `laby.py` afin que le joueur puisse, à tout moment, demander la solution pour sortir du labyrinthe. Il suffit pour cela d'accorder une possibilité supplémentaire lorsqu'on lui demande un déplacement : il peut répondre 'n', 's', 'e' ou 'o' mais aussi, par exemple 'solution'. Dans ce dernier cas, on lance la résolution du labyrinthe.



Pour rappel,  le code de la version précédente qu'il faut modifier si le coup est 'solution'  :

```python
# -*- coding: utf-8 -*-
'''
    jeu du labyrinthe
    : Auteurs mieszczak christophe
'''

import module_laby

def jouer(nom_fichier):
    '''
    déroule un jeu lors duquel le joueur propose des direction pour
    se déplacer dans le but de sortir du labyrinthe codé dans le fichier texte nom_fichier.
    : param nom_fichier
    type str
    '''
    laby = module_laby.Labyrinthe(nom_fichier)
    while not laby.est_gagne():
        print(laby)
        coup = input('direction (n,s,e,o) ou solution ?')
        laby.deplacer(coup)
    print(laby)
    print('VICTOIRE')
```



___________

Par Mieszczak Christophe

source image : [labyrinthe de la cathédrale de St Omer, wikipedia, domaine public](https://commons.wikimedia.org/wiki/File:Die_Gartenlaube_(1885)_b_787_1.jpg)

licence CC BY SA


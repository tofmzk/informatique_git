# Générer un labyrinthe

Nous allons utiliser un parcours de graphe en profondeur pour construire un labyrinthe.



## Principe

Voici, ci-dessous, un labyrinthe dans lequel on a placé des 5 * 3 salles toutes emmurées.

```txt
###########
# # # # # #
###########
# # # # # #
###########
# # # # # #
###########
```

On peut repérer les salles par leurs coordonnées :

```txt
###############################
#(1,1)#(3,1)#(5,1)#(7,1)#(9,1)#
###############################
#(1,3)#(3,3)#(5,3)#(7,3)#(9,3)#
###############################
#(1,5)#(3,5)#(5,5)#(7,5)#(9,5)#
###############################
```

>   
>
> Déterminer le lien entre le nombre de salles et les dimensions du labyrinthe.
>
>   

On va modéliser ce labyrinthe grâce à un graphe non orienté, les coordonnées de chaque salle étant les sommets reliés par une arête aux sommets représentant les salles adjacentes. 

```txt
(1,1)-(3,1)-(5,1)-(7,1)-(9,1)
  |     |     |     |     |
(1,3)-(3,3)-(5,3)-(7,3)-(9,3)
  |     |     |     |     |
(1,5)-(3,5)-(5,5)-(7,5)-(9,5)

```



Un parcours en profondeur, légèrement modifier comme nous le verrons plus loin, nous donnera une liste des sommets de ce graphe. On pourrait, par exemple, obtenir la liste de sommets suivante

```python
[(1, 1), (3, 1), (5,1), (5, 3), (7, 3),(9, 3),(9, 1), (7,1), (9, 5),(7, 5), (5, 5), (3, 5), (3, 3), (1, 3), (1, 5)]
```



 Il suffira alors d'enlever, pour chaque sommet de la liste de parcours en profondeur :

* le mur sur le sommet.
* le mur entre le sommet et le sommet dont il est issu, c'est à dire son `parent`, s'il y en a un.

Il faudra donc modifier l'algorithme du parcours en profondeur pour renvoyer en plus de la liste de sommets un dictionnaire `parent`, comme on le verra plus loin.

```txt
De (1, 1) à (7, 1) :

###############################
#(1,1) (3,1) (5,1)#(7,1) (9,1)#
#############     #######     #
#(1,3)#(3,3)#(5,3) (7,3) (9,3)#
###############################
#(1,5)#(3,5)#(5,5)#(7,5) (9,5)#
###############################

Arrivé à (7, 1), on est dans un cul-de-sac. On repart lors au dernier sommet qui avait encore des voisins non visités, c'est à dire (9, 3), le parent de (9,5). Et on continue :

###############################
#(1,1) (3,1) (5,1)#(7,1) (9,1)#
#############     #######     #
#(1,3) (3,3)#(5,3) (7,3) (9,3)#
#     #     ##############    #
#(1,5)#(3,5) (5,5) (7,5) (9,5)#
###############################

```

Maintenant que le principe est clair, on va implémenter !

## Modélisation par un graphe



> * Réalisez la fonction `modeliser_laby(largeur, hauteur)` qui renvoie une instance de la classe `Graphe_non_oriente_dic` modélisant un labyrinthe. 
>
> ```python
> def modeliser_laby(largeur, hauteur) :
>     '''
>     renvoie un graphe non orienté modélisant un labyrinthe comportant largeur salles sur hauteurs salles
>     : param largeur, hauteur (int)
>     : return (Graphe_non_oriente_dic)
>     '''
> ```
>
> * Testez en utilisant le labyrinthe de la partie précédente.



## Parcours en profondeur

Comme expliqué plus haut, on va reprendre le parcours en profondeur déjà vu en cours en y ajoutant un dictionnaire `parent` dont les clefs seront des sommets et les valeurs le parent dont sont issus ces sommets.

>   
>
> Y-a-t-il un sommet préférentiel pour commencer le parcours en profondeur ?
>
>   



> Définissez la fonction `parcours_laby(graphe)` qui renvoie un tuple composé d'une liste de sommets correspondants à un parcours en profondeur et d'un dictionnaire `parent` permettant de savoir de qui sont voisins les voisins.
>
> ```python
> def parcourir_profondeur(graphe) :
>     '''
>     renvoie un tuple composé d'une liste de sommets correspondants à un parcours en profondeur du graphe et d'un dictionnaire parent permettant de savoir de qui sont voisins les voisins.
>     : param graphe (Graphe_non_oriente_dic)
>     : return tuple (liste de sommets, dictionnaires de parent)
>     '''
> ```
>
> 



## Labyrinthe et tableau de tableaux

Nous allons maintenant utiliser un tableau de tableaux pour construire notre labyrinthe.

Le tableau de tableaux modélise un labyrinthe entièrement muré, sans salle :

```python
Soit le labyrinthe ci dessous :
    
###########
# # # # # #
###########
# # # # # #
###########
# # # # # #
###########

Il est modélisé par e tableau de tableaux suivant :
[
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]
```

> Réalisez la fonction `generer_tab(largeur, hauteur)` qui renvoie un tableau de tabeaux représentant un labyrinthe de largeur salles sur hauteurs salles.
>
> Documentez votre fonction. Ajoutez-y deux Doctests.



Nous allons parcourir la liste des sommets obtenues grâce à la fonction précédente.

* Si le sommet a un parent, on va enlever le mur entre lui et son parent.

En reprenant la liste de sommet `[(1, 1), (3, 1), (5,1), (5, 3), (7, 3),(9, 3),(9, 5),(7, 5), (5, 5), (3, 5), (3, 3), (1, 3), (1, 5)]` vu à l'exemple précédent, et sachant que le parent du sommet (9, 5) est le sommet (9, 3),  cela donnerait, pas à pas :

```txt
Etape 1 : on se place en (1, 1) qui n'a pas de parent
[
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]
Etape 2 : on se place en (3, 1) dont le parent est (1, 1) et on enlève le mur entre eux :
[
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]
Etape 3 : on se place en (5, 1) dont le parent est (3, 1) et on enlève le mur entre eux :
[
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
 [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]


Poursuivez !
```

>  * Réalisez la fonction `milieu(coord1, coord2)` qui renvoie les coordonnées **entières** de la salle entre les salles de coordonnées précisées en paramètre. On utilisera un assertion pour certifier l'adjacence des salles.
>  * Documentez votre fonction, proposez deux Doctest.
>  * Question subsidiaire : pourquoi doit -elle renvoyer des coordonnées **entières** ?

Il n'y a plus qu'à coder l'algorithme principale !

>  * `construire_tab_laby(largeur, hauteur)` qui renvoie le tableau de tableaux représentant un labyrinthe de largeur salles sur hauteurs salles, où les 1 représentent les murs et les 0 les salles. Cette fonction devra :
>    * construire le graphe qui modélise le labyrinthe.
>      * obtenir le tableau du parcours en profondeur du graphe et le dictionnaire de parents des sommets.
>      * initialiser un tableau de tableaux représentant le labyrinthe non parcouru.
>      * ouvrir les murs du labyrinthe, en remplaçant les 0 par des 1 aux bons endroits dans le tableau de tableaux.
>  




## Création d'une chaîne de caractères

Il faut maintenant générer une chaîne de caractères pour pouvoir l'utiliser dans notre projet Labyrinthe, c'est à dire pouvoir l'écrire sous la forme suivante :

```txt
###############################
#   # #       #         #     #
### # # # ##### # ##### ##### #
# # # # #       # #   #     # #
# # # # ######### # # ##### # #
# # # # #       #   #   # # # #
# # # # # # ######### # # # # #
# # #   # # #       # # #     #
# # # ### ### ##### # # ##### #
#   #     #   #     # #   # # #
# ### ##### ####### # ### # # #
# #   #   #       # #   # # # #
# # ### # # ##### # # ### # # #
# # #   # #   #   # # #   # # #
# ### ### ##### ### # # ### # #
#   #   #     # # # # #   #   #
### ### ##### # # # ##### #####
# #     #   # # #   #   #     #
# ######### # # # ### # ##### #
#           #   #     # #     #
###############################
```

> * Codez la fonction `generer_str(tab_laby)` qui renvoie la chaîne de caractères correspondante au tableau modélisant un labyrinthe passée en paramètre.
> * Générez plusieurs labyrinthe à l'aide de cette fonction, copier la chaîne obtenue et collez là dans un bloc note, placez-y un `X` et un `S`, sauvegardez dans le répertoire de votre projet `Labyrinthe` et vous avez un labyrinthe jouable. Testez le !
> * Question subsidiaire : peut-on atteindre n'importe quelle salle depuis n'importe quelle salle ? 



## Allez plus loin ?



Plutôt que de générer un certain nombre de labyrinthes et de les sauvegarder dans un fichier texte, on pourrait reprendre le code de notre objet `Labyrinthe` et modifier sa méthode  `__init__` afin de construire un labyrinthe à partie d'un nombre de salles ..

__________

Par Mieszczak Christophe

licence CC BY SA










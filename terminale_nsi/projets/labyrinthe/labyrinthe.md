# Labyrinthe : Partie 1



Ceci est un projet à mener à deux.

Avant de commencer à coder :

* faites une liste des différentes tâches à accomplir.
* jugez la difficulté de chaque tâche.
* Réfléchissez à l'ordre dans lequel vous avez besoin des méthodes.
* répartissez vous les tâches équitablement.



 Avant de commencer à rédiger les algorithmes :

* Commentez votre code.
* Dans les Docstrings des méthodes, précisez l'auteur de la méthode. 
* Proposez un jeu de tests dès que cela est possible (on pourra utiliser `lab1.txt` pour cela)
* N'oubliez pas les assertions.



## Le module et la classe

L'objectif est la création d'un module contenant la classe `Labyrinthe` :

* qui initialise un labyrinthe à partir d'un fichier texte représentant un labyrinthe  :

    * Un `#` pour un mur.
    * L'emplacement des murs. Le labyrinthe doit être **entièrement** entouré de murs. 
    * La présence dans ce labyrinthe d'un joueur 'X'.
    * La présence dans ce labyrinthe d'une sortie 'S' à atteindre.
    
* Voici des exemples de fichiers texte définissant un tel labyrinthe avec un joueur 'X' et une sortie 'S' :

    ```python
    ##########
    #     X# #
    # ###### #
    #    ### #
    # ## #   #
    # ## # ###
    #  #   # #
    ## ###   #
    ######## #
    #  #     #
    ##   #####
    ####S#####
    ```

    ````python
    ############################
    #X                       # #
    ###### ## ##### ####### ## #
    #      #        #          #
    # #################### #####
    # ####        #            #
    #      ## ### ###### ##### #
    ## ###### ##  ###### ##### #
    #         #        #       #
    ###### ##########  ####### #
    #               #        # #
    ####################S#######
    ````

    ```python
    #################################################################
    #X                  #                       #                   #
    ################### ########## ############ # ################# #
    #                   #          #            # #                 #
    # ############ ###### ########### ########### # #################
    # #                   ########                # # #             #
    # ############################ ################ # ############# #
    #                            # #                #               #
    ############################ # ############# #### ###############
    #             #              #             # #### ###############
    # ###########   ##########################   #### ###############
    # #         #####                        #   #  # ###############
    # ######### ##### ######################   ###    # #           #
    #         # ##### ############################  ### #### ########
    ######### #       #                #            #   #  # ########
    ######### ####### ################ # ############   ## #        #
    #         #                      #   #          #               #
    # ############################## ### ########## ############### #
    #                                #              #               #
    ################################   ############   ######### #####
    #           #                  ##  ######################## #####
    # ############################ ###   ###################### #####
    # ############################ ####   #####################     #
    #                            # ##### ########################## #
    ############ ################# ##### ########################## #
    #            #                 #                                #
    ############   ################################################ #
    # ########## ################################################## #
    #                                                               #
    ####################################################S############
    ```

    

* un Labyrinthe possède :

    * deux attributs précisant sa hauteur et sa largeur.
    * un attribut précisant la position du joueur.
    * un attribut précisant la position de la sortie.

      

L'`interface` de la classe est la suivante :

```python

class Labyrinthe()
 |  Labyrinthe(nom_fichier)
 |  
 |  Définit la classe Labyrinthe
 |  
 |  Methods defined here:
 |  
 |  __init__(self, nom_fichier)
 |      construit un objet labyrinthe à partir du fichier txt passé en paramètre
 |      le labyrinthe a pour attributs
 |      joueur : un tuple donnant la position de joueur
 |      sortie : un tuple donnant la position de la sortie
 |   	lab une liste de listes pour la position de murs du joueur et de la sortie
 |      : param nom_fichier (str)
 |
 |  
 |  __repr__(self)
 |      renvoie une chaine pour la description du labyrinthe
 |      return (str)
 |  
 |  __str__(self)
 |      renvoie une chaine pour afficher le labyrinthe, 
 |      le joueur et la sortie via un print
 |      return (str)
 |  
 |  deplacer(self, direction)
 |      modifie les coordonnées du joueur dans la direction souhaitée si possible
 |       : param direction (str) 'n', 's' , 'e' ou 'o'
 | 
 |  
 |  est_gagne(self)
 |      renvoie vrai si la partie est finie
 |      : return (boolean)
 |    
 |  
 |  est_possible(self, direction)
 |      renvoie True ssi le déplacement dans la direction est possible
 |      (cest à dire sil ny a pas de mur à sa possible prochaine position)
 |      : param direction (str) 'n', 's' , 'e' ou 'o'
 |      : return (boolean)
 |  
```



Bien entendu, la classe utilise d'autres fonctions et méthodes en plus de cette interface :

* Une pour lire les données d'un fichier _texte_ et les renvoyer. La voici. De rien ;)

    ```python
    	def lire(nom_fichier): # ce n'est pas une méthode mais une fonction de la classe
            '''
            lit le fichier txt passé eb paramètre et renvoie son contenu.
            : param nom_fichier (str)
            return (list
            '''
            assert isinstance(nom_fichier, str), 'nom_fichier doit être une chaîne !'
            try :
                # ouvre un canal en lecture vers text.txt :
                lecture = open(nom_fichier, 'r',encoding = 'utf_8') 
            except FileNotFoundError :
                raise # renvoie une erreur si le fichier n'existe pas
            # stocke toutes les lignes du fichier dans la liste toutes_les_lignes :
            toutes_les_lignes = lecture.readlines() 
            lecture.close()
            return toutes_les_lignes
    ```

    

* Une pour générer une liste de liste représentant le labyrinthe à partir de ce que renvoie la méthode précédente :

    ```python
    	def generer_laby(toutes_les_lignes) : # ce n'est pas une méthode mais une fonction de la classe
            '''
            renvoie une liste de listes modélisant le labyrinthe
            : param toutes_les_lignes (list)
            : return (list)
            '''
            lab = []
            
    ```

    

    

* Une méthode pour retrouver la position du joueur et celle de la sortie à partir de la structure choisie plus haut.

    ```python
    	def position_joueur(self):
            '''
            renvoie les coordonnées x, y du joueur dans le labyrinthe
            : return (list) [x, y] 
            '''
        
        def position_sortie(self):
            '''
            renvoie les coordonnées x, y de la sortie du labyrinthe
            : return (tuple)
            '''
    ```

    

    

* pour renvoyer les futures coordonnées au cas où on déplacerait le joueur dans une direction. On pourra ici utiliser le dictionnaire suivant pour déterminer ces coordonnées:

    ```python
    	def future_position(self, direction):
            '''
            renvoie les coordonnées de la prochaine position du joueur
            SI il se déplaçait dans la direction précisée
            : param direction (str) 'n', 'e', 's', 'o'
            : return (tuple) 
            '''
            directions = { 'n' : (0, -1),
                           's' : (0, 1),
                           'e' : (1, 0),
                           'o' : (-1, 0)
                         }
    ```
    
     
    
      

## Utilisation du module



Créez un programme `laby.py`



Codez la fonction `jouer(nom_fichier)` qui déroule un jeu lors duquel le joueur propose des directions pour se déplacer dans le but de sortir du labyrinthe codé dans le fichier texte _nom_fichier_.



```python
# -*- coding: utf-8 -*-
'''
    jeu du labyrinthe
    : Auteurs mieszczak christophe
'''

import module_laby

def jouer(nom_fichier):
    '''
    déroule un jeu lors duquel le joueur propose des direction pour
    se déplacer dans le but de sortir du labyrinthe codé dans 
    le fichier texte nom_fichier.
    : param nom_fichier (str)
    '''
```



__________

Par Mieszczak Christophe

Licence CC BY SA
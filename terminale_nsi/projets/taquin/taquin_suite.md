# Le retour du Taquin

### Principe



Nous avons programmé le jeu du Taquin dans un projet précédent : il nous faut maintenant proposer une résolution de ce jeu : à tout moment, le joueur doit pouvoir demander à voir la solution du puzzle et la machine doit pouvoir, étape par étape, lui montrer comment revenir au puzzle ordonné.



Pour cela, l'astuce est la suivante : nous allons utiliser une structure de Pile afin de stocker tous les coups jouer lors du mélange du puzzle puis lorsque le joueur déplace certaines cases du jeu. Il suffira, pour revenir à la situation initiale, de dépiler les coups stocker dans la pile et de les jouer.



### On y va



Nous aurons besoin d'un nouvel attribut `pile` de la classe `Pile` à notre classe `Taquin` (on n'oubliera pas d'importer le module adéquate). 



Il faut maintenant trouver quelle méthode du `Taquin` est la plus appropriée pour empiler chaque coup joué dans notre pile.



Il reste à coder une dernière méthode `resoudre(self)` qui, tant que le puzzle n'est pas revenu à la position gagnante :

* dépilera la pile
* déplacera la case correspondante (attention, il ne faut pas empiler le coup joué dans la pile ici)
*  affichera l'état du taquin 
* attendra que le joueur presse une touche pour continuer.



Comme toujours, documentez et testez !



N'oubliez pas de modifier le déroulement du jeu pour que le joueur puisse demander la solution, par exemple en proposant 'abandon' plutôt qu'un numéro de case lorsqu'on lui demande un coup.



### Optimisation

Cette résolution n'est pas optimale. Elle se contente de remonter pas à pas les coups joués jusqu'à la position initiale. Il existe probablement une méthode plus rapide... mais elle n'est pas simple à déterminer.



Une optimisation simple est possible cependant : jouer deux fois de suite le même coup ne sert à rien. On peut donc éliminer les séries de deux coups identiques consécutifs dans la pile.



> * Définissez, documentez, testez et codez une méthode `optimiser_pile(self)` qui modifie par effet de bord le contenu de l'attribut `pile` afin de l'optimiser.
> * Modifiez la méthode `résoudre` afin d'incorporer cette optimisation.



_________

Par Mieszczak Christophe

Licence CC BY SA






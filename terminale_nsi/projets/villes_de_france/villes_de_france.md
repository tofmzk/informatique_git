# Villes de France



## Création de la base



Vous trouverez dans le répertoire du projet quatre fichiers au format _csv_ :

* le fichier _nom_villes_france.csv_ qui répertorie les villes de la France avec pour attributs :

    * le code `slug` de la ville, un identifiant unique.
    * le `nom_reel` de la ville avec la bonne orthographe.
    * `nom_2`, `nom_3` et  `nom_4` : des variantes du nom de chaque ville avec différentes typologie.

* le fichier _coordo_villes_france.csv_ avec les attributs :

    * `slug`
    * `dpt` : le département de la ville
    * `longitude`
    * `latitude`
    * `alt_min` : l'altitude minimale de la ville
    * `alt_max` : l'altitude maximale de la ville

* le fichier _population_villes_france.csv_ avec les attributs :

    * `slug`
    * `population_2010`
    * `population_1999`

* Le fichier _departements-regions.csv_ avec les attributs :

    * `num_dep` : le no du département
    * `dep_name` : le nom du département
    * `region_name` : le nom de la région dans lequel se trouve le département.

    

>
>
>A partir de ces fichiers :
>
>* Créez une base de données _SQL_ `villes_france.db` contenant les trois relations.
>
>* Précisez les domaines de chaque attribut de chaque relation. ATTENTION aux numéros des départements !! N'y-a-t-il que des entiers ??
>
>* Préciser les clés primaires et étrangères de chaque relation.
>
>* Ecrivez ci-dessous le schéma relationnel de la base, en y précisant les clés.
>
>  ```sql
>  
>  ```
>  
>  
>  
>  



## Modifier la base



Il y a une erreur dans les données concernant les altitudes de la ville de Paris : la capitale est située entre 28m et 131m d'altitude.

1. Utilisez une requête pour déterminer l'identifiant `slug` de Paris.

    ```sql
    
    ```

    

2. Modifiez l'altitude minimale et maximale de la ville dont vous connaissez maintenant l'identifiant `slug`. Indiquez votre requête :

    ```sql
    
    ```

3. Le petit village de Canteleux ne comptait, en 2010, que 17 habitants... et il a été oublié dans la base de données ! Réparons ce scandale en insérant dans chacune des tables les donnés ci-dessous (précisez chaque fois la requête sous les enregistrements ). Attention tout de même à l'ordre des insertions : les clés étrangères prennent leurs valeurs parmi celles,**pré-existante**, d'une clé primaire d'une autre relation.

| slug  | nom_reel  |   nom_2   |   nom_3   |   nom_4   |
| :---: | :-------: | :-------: | :-------: | :-------: |
| 25208 | Canteleux | canteleux | CANTELEUX | canteleux |

```sql

```

| slug  | dpt  | longitude | latitude | alt_min | alt_max |
| :---: | :--: | :-------: | :------: | :-----: | :-----: |
| 25208 |  62  |  2.3075   | 50.2153  |   95    |   156   |

```sql

```

| slug  | population_2010 | population_1999 |
| :---: | :-------------: | :-------------: |
| 25208 |       17        |       17        |

```sql

```











## Avec DB Browser



En utilisant DB Browser, répondez précisément aux questions ci-dessous. Dans chaque cas, précisez, en plus du résultat, la requête utilisée



### Requête sans jointure



1. Combien y-a-t-il de villes dans notre base de données ?

    ```sql
    
    ```

    

2. Quels sont les différents noms utilisés pour la ville de `Île-aux-Moines` ?

    ```sql
    
    ```

3. Quel est l'identifiant `slug` de `goux les dambelin` ?

    ```sql
    
    ```

4. Combien de villes ont quatre noms qui s'écrivent de quatre façons différentes ?

    ```sql
    
    ```

5. Combien de villes de France ont une population qui a diminué de 1999 à 2010 ?

    ```sql
    
    ```

6. Combien de villes du département 62 ont une altitude_maximale inférieure à 10m ?

    ```sql
    
    ```

7. Quelle est l'altitude maximale de la ville la plus haute de France ? (Il faudra ignorer les villes dont la valeur de _alt_max_ est 'NULL').

    ```sql
    
    ```

8. Quelle est l'altitude moyenne des villes du département 62 ? (on prendra pour altitude pour une ville la grandeur `(alt_max+alt_min)/2` )

    ```sql
    
    ```


### Avec jointure _simple_



1. Listez les noms réels qui commencent par un 'A', des villes dans le département 62 ?

    ```sql
    
    ```

2. Combien y-a-t-il de ville en  `Loire_Atlantique`  ?

    ```txt
    ```

    

2. Listez les noms réels et altitudes maximales des villes du département 59 dont l'altitude maximale se situe sous les 10m.

    ```sql
    
    ```

3. Quels sont les noms réels et population des dix villes les plus peuplées de France en 2010 ?

    ```sql
    
    ```

4. Combien de villes de plus de 20000 habitants se situent entièrement à plus de 1000m d'altitude ?

    ```sql
    
    ```

5. Quelle est l'altitude maximale moyenne des villes du département 59 ? 

    ```sql
    
    ```

7. Dans quelle région est située la ville dont le `slug` est 1 ?

    ```python
    ```

    


### Avec multiples jointures



1. Quel est le nom du département dans lequel se situe la ville de `marchamp` ?

    ```sql
    
    ```

    

1. Listez, par ordre croissant d'habitants, les noms réels et le nombre d'habitants des villes du département 62 qui ont plus de 20000 habitants en 2010 ?

    ```sql
    
    ```

2. Listez, par ordre croissant d'altitudes, le nom, l'altitude et le nombre d'habitants des villes dont l'altitude moyenne `(alt_max + alt_min)/2` est inférieure à 10m et le nombre d'habitants supérieur à 20000.

    ```sql
    ```
    
    
    
2. Listez le nom des les villes dont l'altitude est inférieure à 10m en `Loire-Atlantique`.

    ```sql
    
    ```





## Avec Python



Nous allons utiliser le module `folium` qui permet de générer des cartes à partir de listes de coordonnées GPS ainsi que le module `cartographie` fourni, qui permet de mesurer la distance entre deux positions dont on connaît les coordonnées GPS.



Le module `folium` permet de générer un fichier au format _HTML_ dans le dossier courant. Son ouverture, par un navigateur, montrera une carte sur laquelle il est possible d'ajouter un grand nombre de données.

La fonction ci-dessous permet ainsi d'afficher un marqueur sur la carte, en lui passant en paramètre une liste de tuple contenant des données relatives aux villes, structurées de telle façon que :

* la première valeur de chaque tuple correspond à la latitude
* la seconde valeur de chaque tuple correspond à la longitude 
* la troisième valeur de chaque tuple  correspond au nom de la ville



```python
import folium
def creer_carte(liste, nom_carte = 'carte'):
    """
    place des marqueurs sur un carte au sein du page html, sauvegardée ensuite. 
    :param liste  liste de tuple (lat, long, nom ....)
    :param nom_page_html:(str) nom de la page html (sans extension)
    """
    carte = folium.Map(location = (liste[0][0], liste[0][1]),zoom_start = 14)
    for ville in liste:
        folium.Marker((ville[0], ville[1]), popup = ville[2]).add_to(carte)
    carte.save(nom_carte + '.html')
```



### Afficher une ville nommée



> * Réalisez une fonction `chercher_ville(nom)` qui interroge la base et renvoie une liste de tuple contenant, pour la ville dont un nom a été passé en paramètre :
>     * la latitude
>     * la longitude
>     * le nom réel
>
> * Générez un fichier montrant la position de `OBERHOFFEN-LES-WISSEMBOURG` que vous nommerez `carte_OBERHOFFEN.html`



### Villes sous les 10m d'altitudes



> * Réalisez une fonction `chercher_villes_sous_altitude(altirude)` qui interroge la base et renvoie une liste de tuple contenant, pour toutes les villes dont l'altitude maximale est inférieure à l'altitude passée en paramètre :
>     * la latitude
>     * la longitude
>     * le nom réel
>     * l'altitude maximale
>
> * Générez un fichier montrant la position de ces villes que vous nommerez `carte_altitudes.html`



### Villes les plus peuplées

> * Réalisez une fonction `chercher_villes_peuplees()` qui interroge la base et renvoie une liste de tuple contenant,  pour chaque  ville la plus peuplée de chaque département de la base en 2010 :
>     * la latitude
>     * la longitude
>     * le nom réel
>     * le nombre d'habitants en 2010
>
> * Générez un fichier montrant la position de ces villes que vous nommerez `carte_grosses_villes.html`

 

Le code Python, avec les différentes fonctions utilisées, ainsi que les cartes, sont à rendre avec le projet.





__________

Par Mieszczak Christophe

Licence CC BY SA


# L'algorithme de Dijkstra





<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Edsger_Dijkstra_1994.jpg/1024px-Edsger_Dijkstra_1994.jpg" alt="Edsger Dijkstra - wikipédia - CCA-SA" style="zoom:50%;" />

## Dijkstra



[L'algorithme de Dijkstra](https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra), du nom du mathématicien et informaticien [Edsger Dijkstra](https://fr.wikipedia.org/wiki/Edsger_Dijkstra) dont vous voyez la trombine plus haut,  sert à résoudre le [problème du plus court chemin](https://fr.wikipedia.org/wiki/Problèmes_de_cheminement) dans un graphe pondéré. Ce célèbre algorithme, inventé en 1959, est indispensable pour de nombreuses applications comme le routage dans un  réseau informatique ou routier. Sans lui, pas d'internet et pas de guidage GPS.



Suivez le lien pour visionner [Une petite vidéo d'explication](https://youtu.be/rI-Rc7eF4iw) sur le principe de cet algorithme.



On peut également suivre cette petite animation :

![animation](https://upload.wikimedia.org/wikipedia/commons/5/57/Dijkstra_Animation.gif)



Essayons maintenant de rédiger en langage courant son fonctionnement :

* il est impératif de commencer par le sommet de départ. En revanche, les autres sommets peuvent être étudiés dans n'importe quel ordre.
* A l'initialisation, seul le sommet de départ est accessible, à une distance (métrique) 0. Les autres sommets sont inaccessibles et placés à une distance infinie. 
* Le premier sommet choisi est le sommet de départ. Il sera le seul sommet sans parent.
* Lorsqu'on passe à l'étape suivante on remplit une nouvelle ligne :
    *  on calcule, pour chaque sommet restant, la métrique pour l'atteindre depuis le sommet de départ (il y a un cumul) choisi à l'initialisation en passant par  le dernier sommet choisi à l'étape précédente. 
    * Si cette métrique est inférieure celle du sommet atteignable, on remplace cette dernière par la nouvelle métrique et le sommet choisi à l'étape précédente devient le nouveau parent du sommet atteignable. Sinon on ne change rien.
    * on choisit ensuite le sommet dont la métrique est la plus faible. On précise bien de quel sommet (son parent) il provient. Ce sommet choisi devient alors inaccessible pour la suite du déroulement de l'algorithme et se comporte comme un sommet de départ pour l'étape suivante.



## Faisons tourner en débranché 

Considérons le réseau ci-dessous :

![réseau](media/reseau.jpg)

On peut le symboliser par le graphe pondéré suivant :

![reseau modélisé](media/graphe_reseau.jpg)



Le tableau ci-dessous permet d'appliquer l'algorithme du Dijkstra pour déterminer la table de routage de `A`.

Complétez le !

|  A   |     B      |     C      |     D      |     E      |     F      |     G      |     H      |    Choix     |
| :--: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: | :----------: |
|  0   | inf | inf | inf | inf | inf | inf | inf |  A(0)  |
|  x  |   |  |  |  |  |     |   |  |
|  x   |   |   |  |  |   |     |           |  |
|  x  |           |   |   |  |   |     |           |  |
|  x   |           |            |           |            |            |            |           |              |
|  x   |           |            |           |            |            |            |           |              |
|  x   |           |            |           |            |            |            |           |              |
|  x   |           |            |           |            |            |            |           |              |



Compléter la table de routage simplifiée du routeur `A` :



| Destination | Passerelle | Métrique |
| :---------: | :--------: | :------: |
|      A      |     A      |    0     |
|      B      |     B      |   0,1    |
|      C      |     H      |   0,2    |
|      D      |            |          |
|      E      |            |          |
|      F      |            |          |
|      G      |            |          |
|      H      |            |          |

## Implémentation

Nous allons implémenter l'algorithme de Dijkstra et l'utiliser sur notre réseau.

### Modéliser le réseau

Pour modéliser le réseau, nous allons utiliser un graphe non orienté dont les arêtes sont pondérées. Pour cela nous allons utiliser le dictionnaire d'adjacence dont le début est écrit ci-dessous. Notez qu'il est nommé en majuscule car il s'agit d'une constante globale :

```python
RESEAU = {
    'A' : [('B', 0.1), ('G', 2), ('H', 0,1)],
    'B' : [('A', 0.1), ('D', 2)],
    'C' : [('H' , 0,1), ('D', 2)],
    }
    
```

> Complétez ce dictionnaire.



Afin de limiter le nombre de paramètres dans les fonctions qui vont suivre, nous utiliserons une variable globale `GRAPHE` pour définir le dictionnaire d'adjacence du graphe sur lequel nous allons travailler.

Ainsi, après avoir défini la variable `RESEAU` tout en haut, on ajoutera, tout en bas du code :

```python
#############################DOCTEST ACTIVE   
if __name__ == '__main__' :
    import doctest
    GRAPHE = RESEAU
    doctest.testmod(verbose = True)
```



### Initialisation 

Nous allons avoir besoin de deux types de dictionnaires  :

* un dictionnaire de métrique
* un dictionnaire parent.

Pour le premier dictionnaire nous aurons besoin de la valeur $`+\infty`$ . Il nous faut l'importer du module `math`. Testez dans la console :

```python
>>> from math import inf
>>> inf
???
>>> type(inf)
???
>>> inf > 1000000000000000000
???
>>> inf > 1000000000000000000000000000000000000000000
???
>>> inf + 1000000000
???
```



> * Complétez la fonction ci-dessous :
>
> ```python
> def initialiser_metrique(sommet_depart):
>     '''
>     renvoie un dictionnaire `metrique` dont les clés sont les sommets du graphe 
>     GRAPHE et les valeurs inf sauf pour le sommet de départ qui est à 0.
>     : param sommet_depart (?)
>     : return (dict)
>     >>> initialiser_metrique('A') == {'A': 0, 'B': inf, 'C': inf, 'D': inf, 'E': inf, 'F': inf, 'G': inf, 'H': inf}
>     True
>     '''
> ```
>
> * Testez cette fonction dans la console depuis différents sommets (vous n'avez pas oublié l'import n'est-ce pas ?)





Reste à s'occuper du dictionnaire de parents :

> Complétez la fonction `initialiser_parent()` ci-dessous :
>
> ```python
> def initialiser_parent():
>        '''
>        renvoie un dictionnaire dont les clés sont les sommets du GRAPHE
>        et les valeurs None 
>        : return (dict)
>        >>> initialiser_parent() == {'A': None, 'B': None, 'C': None, 'D': None, 'E': None, 'F': None, 'G': None, 'H': None}
>        True
>        '''
>    ```
> 
>

### Fonctions annexes



Commençons par une fonction qui calcule la _métrique_ entre deux sommets quelconques du graphe `GRAPHE`.

* Si les deux sommets sont voisins, elle revoie la métrique entre eux.
* sinon elle renvoie `inf`



> Complétez la fonction ci-dessous :
>
> ```python
> def metrique(sommet1, sommet2) :
>        '''
>        renvoie la métrique (qui peut être inf) entre les deux sommets dans le graphe
>        : param dic_adjacence (dic)
>        : param sommet1, sommet2 (?) : deux sommets du graphe
>        : return (float)
>        >>> metrique('A', 'G')
>        2
>        >>> metrique('H', 'C')
>        0.1
>        >>> metrique('E', 'G')
>        inf
>        '''
> 
> ```
>
> 



Nous avons également besoin d'une fonction qui renvoie le sommet se situant à une métrique minimum pour faire tourner notre algorithme.



> Complétez la fonction ci-dessous :
>
> ```python
> def minimum(dic_metrique) :
>        '''
>        renvoie la clef du dictionnaire de valeur minimale
>        : param dico_metrique (dict)
>        : return une clé (?)
>        >>> minimum({'A': inf, 'B': 3, 'C': 2, 'D': 1.2, 'E': inf, 'F': 0, 'G': inf, 'H': 1})
>        'F'
>        '''
> ```
>
> 



### L'algorithme de Dijkstra



Nous allons découper cet algorithme en trois parties :

* La première consiste à déterminer l'étape suivante d'une situation donnée à partir d'un sommet choisi
* la seconde, consistera à dérouler l'algorithme de Dijkstra 
* la troisième, à partir des résultats de l'algorithme, afficher une route d'un sommet vers un autre en indiquant les métriques à chaque étape.



Nous allons utiliser trois dictionnaires :

* Le premier `dic_metrique` aura pour clés les sommets de `GRAPHE ` et pour valeurs leur métrique. 
* Le second `dic_ligne` fera presque la même chose mais, supprimera la clé du sommet choisi à chaque étape. Il correspondra aux lignes du tableau.
* Le troisième `dic_parent` permettra de remonter les parents depuis le sommet de destination jusqu'au sommet initial.





Lors de la première étape, on part d'un `dic_metrique` et d'un `dic_parent`  donnés, c'est à dire d'une ligne donnée de notre tableau. 

* Pour chaque sommet du `dic_metrique`, 
    * on va calculer la métrique totale pour rejoindre ce sommet depuis le sommet du départ en passant par le `sommet` choisi passé en paramètre. Il nous faudra nous servir de la fonction `metrique`  et du dictionnaire `dic_metrique`.
    * Si la métrique obtenue est inférieure à celle du sommet, on remplace la métrique de ce sommet par la nouvelle valeur calculée et le `sommet` passé en paramètre devient son parent. On modifie donc ici nos deux dictionnaires.
* Pas besoin de renvoyer les dictionnaires modifiés, on utilise un effet de bord.



> Complétez la fonction ci-dessous :
>
> ```python
> def etape_suivante(sommet, dic_metrique, dic_parent):
>     '''
>     modifie les dictionnaires dic_metrique et dic_parent en calculant une nouvelle
>     ligne dans le tableau de Dijkstra à partir du sommet passé en paramètre.
>     : param sommet (?)
>     : param dic_metrique (dict)
>     : param dic_parent (dict)
>     : EFFET DE BORD sur dic_metrique et dic_parent
>     '''
>    
> ```
> 
>



L'algorithme de Dijkstra va utiliser :

* Initialiser les trois dictionnaires `dic_metrique`, `dic_ligne` et `dic_parent` en utilisant les fonctions vues plus haut.
* Tant qu'il reste des sommets dans `dic_ligne` :
    * on modifie `dic_ligne`  et `dic_parent` en utilisant la fonction `etape_suivante` en partant du `sommet` courant, passé en paramètre.
    * On met à jour les valeurs de `dic_metrique` à partir de celle de `dic_ligne` qui vient d'être modifié.
    * on supprime le sommet courant de `dic_ligne` car il ne sera plus accessible dans les étapes suivantes.
    * On détermine le sommet de `dic_lignes` dont la métrique est minimale (on a une fonction pour cela).
    * le`sommet` courant devient le sommet de métrique minimale 
* Une fois que `dic_ligne` est vide, on a traité tous les sommets. Il ne reste qu'à renvoyer les deux dictionnaires qui nous permettront de trouver notre chemin : `dic_metrique` et `dic_parent`.





> Complétez la fonction ci-dessous :
>
> ```python
> def dijkstra(sommet):
>     '''
>     renvoie un dictionnaire de metriques et un dictionnaire de parents
>     constuits en suivant l'algorithme de Dijkstra en partant du sommet
>     passé en paramètre
>     : param sommet (?)
>     : return tuple of dict
>     '''
>     dic_metrique = initialiser_metrique(sommet) 
>     dic_ligne = initialiser_metrique(sommet)
>     dic_parent = initialiser_parent()
>     while len(dic_ligne) > 0 :
> ```
>
> 





On va maintenant pouvoir chercher un chemin d'un `sommet_depart` vers un `sommet_destination`. 

* On commence par récupérer les deux dictionnaires grâce à la fonction `dijkstra` précédente.
* On va ensuite compléter la `liste_sommets` en partant du `sommet_destination` et en remontant, grâce à `dic_parent` jusqu'au `sommet_depart`, le seul dont le parent est `None`.
* Attention, cette liste sera à l'envers... Il faudra donc l'inverser avant de la renvoyer.



> Complétez la fonction ci-dessous :
>
> ```python
> def etablir_route(sommet_depart, sommet_destination) :
>     '''
>     renvoie une liste de tuple (sommet, metrique) donnant le chemin le plus court du sommet de départ à celui de destination dans le GRAPHE.
>     : param sommet_depart, sommet_destination(?)
>     : return list of tuple
>     >>> etablir_route('A', 'A') == [('A', 0)]
>     True
>     >>> etablir_route('A', 'E') == [('A', 0), ('B', 0.1), ('D', 0.2), ('E', 2.2)]
>     True
>     >>> etablir_route('A', 'F') == [('A', 0), ('H', 0.1), ('F', 2.1)]
>     True
>     '''
>     dic_metrique, dic_parent = dijkstra(sommet_depart)
>     liste_sommets = []
> ```
>
> 



## Réseau routier



Nous avons utilisé l'algorithme pour trouver une route dans un réseau informatique. C'est exactement le même principe lorsqu'un GPS calcule une route sur une carte. Les points importants (carrefour, rond-points, villes ....) sont les sommets d'un immense graphe. L'algorithme appliqué pour trouver sa route est exactement le même que pour un réseau informatique.



Voici, par exemple, une version (très) simplifiée d'une carte routière où son indiquées les distances(en km).

![carte](media/littoral.jpg)



Comme nous l'avons fait pour notre petit `RESEAU` , on modélise ce graphe de la façon suivante :

```python
LITTORAL = {
    'sangatte' : [('calais', 9), ('coquelles', 4), ('peuplingues', 4)],
    'peupligues' : [('sangatte', 4), ('caffier', 12)],
    'coquelles' : [('sangatte', 4), ('frethun', 6), ('caffier', 14)],
    'calais' : [('sangatte', 9), ('marck', 16), ('frethun', 7), ('coquelles', 7)],
    'frethun' : [('coquelles', 6), ('calais', 7), ('guines', 10), ('fiennes', 10), ('les attaques', 9)],
    'caffier' : [('coquelles', 14), ('peuplingues', 12), ('fiennes', 3)],
    'fiennes' : [('caffier', 3), ('frethun', 10), ('guines', 13)],
    'guines' : [('frethun', 10), ('fiennes', 13), ('bouquehault', 6), ('ardres', 8)],
    'marck' : [('calais', 16), ('offekerque', 8), ('les attaques', 7)],
    'les attaques' : [('marck', 7), ('frethun', 9), ('ardres', 8), ('guemps', 5)],
    'ardres' : [('les attaques',8), ('guines', 8), ('louches', 5)],
    'bouquehault' : [('guines', 6), ('louches', 14)],
    'louches' : [('bouquehault', 14), ('ardres', 5), ('nortkerque', 8)],
    'offekerque' : [('marck', 8), ('oye-plage', 6), ('guemps', 4)],
    'guemps' : [('offekerque', 4), ('les attaques', 5), ('audruicq', 11), ('nortkerque', 9)],
    'nortkerque' : [('guemps', 9), ('louches', 8), ('audruicq', 4)],
    'audruicq' : [('nortkerque', 4), ('guemps', 11), ('gravelines', 17)],
    'gravelines' : [('oye-plage', 7), ('audruicq', 17)],
    'oye-plage' : [('offekerque', 6), ('gravelines', 7)]    
    }

```



A la suite des Doctests, afin qu'ils restent valides, on peut changer de `GRAPHE` pour utiliser celui-ci :

```python
#############################DOCTEST ACTIVE   
if __name__ == '__main__' :
    import doctest
    GRAPHE = RESEAU
    doctest.testmod(verbose = True)
    
#############################CHOIX DU GRAPHE
GRAPHE = LITTORAL
```



> * Trouvez une route de Caffier à Oye-Plage.
>
>     ```python
>     
>     ```
>
> * Trouvez une route d' Audruicq à Sangatte.
>
>     ```python
>     
>     ```
>
> * Que faudrait-il changer pour obtenir le chemin le plus rapide et non pas le plus court ?
>
>     ```txt
>     
>     ```
>
>     





___________

Par Mieszczak Christophe

licence CC BY SA

source image : 

* [portrait Dijkstra - wikipédia - CC BY SA](https://commons.wikimedia.org/wiki/File:Edsger_Dijkstra_1994.jpg)
* [animation dijkstra - wikipedia - domaine publique](https://commons.wikimedia.org/wiki/File:Dijkstra_Animation.gif)
* production personnelle 
* image du réseau à partir d'un TP sur [Pixees](https://pixees.fr/)
# -*- coding: utf-8 -*-
'''
    module pour les structures linéaires
    : Auteur Mieszczak C
'''
class Maillon() :
    '''
    une classe pour construire un maillon d'une liste chainee
    '''
    def __init__(self, valeur = None, suivant = None) :
        '''
        construit un maillon avec une valeur et un lien vers le maillon suivant.
        : param valeur (variable) None par défaut
        : param suivant (Maillon) None par défaut
        '''
        self.valeur = valeur
        self.suivant = suivant
        
    def __repr__(self):
        '''
        donne la classe de l'objet et sa valeur
        : return (str)
        '''
        return 'maillon de la class Maillon de valeur ' + str(self.valeur)


class Liste_chainee() :
    '''
    une classe pour implémenter une liste chainee
    '''
    def __init__(self):
        '''
        construit une liste chainee vide de longueur 0
        '''
        self.tete = None
        self.longueur = 0

    def __repr__(self):
        '''
        donne la classe de l'objet et sa taille
        : return (str)
        '''
        return 'Class Liste_chainées de taille ' + str(self.longueur)

    def ajouter(self, valeur):
        '''
        ajoute un maillon de valeur précisé en tête de liste
        : param valeur (???)
        : pas de return
        '''
        nouveau_maillon = Maillon(valeur, self.tete)
        # on crée un nouveau maillon avec la valeur en paramètre et son suivant
        # est l'actuel maillon de tête
        self.tete = nouveau_maillon
        # le nouveau maillon devient le maillon de tête
        self.longueur += 1
        
    def est_vide(self):
        '''
        renvoie True si la liste est vide et False sinon
        : return (boolean)
        >>> l = Liste_chainee()
        >>> l.est_vide()
        True
        >>> l.ajouter(1)
        >>> l.est_vide()
        False
        '''  
        return self.tete == None
    
    def __str__(self):
        '''
        renvoie une chaine pour visualiser la liste
        : return (str)
        >>> l = Liste_chainee()
        >>> print(l)
        ()
        >>> l.ajouter(1)
        >>> print(l)
        (1, ())
        >>> l.ajouter(2)
        >>> print(l)
        (2, (1, ()))
        '''
        chaine = '('
        maillon = self.tete
        while not maillon == None :
            chaine += str(maillon.valeur) + ', ('
            maillon = maillon.suivant
        for _ in range(self.longueur + 1):
            chaine += ')'
        return chaine
    
    def __len__(self):
        '''
        renvoie le nombre d'éléments de la liste
        : return (int)
        >>> l = Liste_chainee()
        >>> len(l)
        0
        >>> l.ajouter(7)
        >>> len(l)
        1
        >>> l.ajouter(1)
        >>> l.ajouter('b')
        >>> len(l)
        3
        '''
        return self.longueur
    
    def __getitem__(self, n):
        '''
        renvoie la valeur du maillon d'indice précisé
        : param n (int) 0 <= n <= longueur de la liste - 1 
        : return (??) la valeur du maillon d'indice n
        >>> l = Liste_chainee()
        >>> l.ajouter(4)
        >>> l.ajouter(3)
        >>> l.ajouter(2)
        >>> l.ajouter(1)
        >>> l[0]
        1
        >>> l[1]
        2
        >>> l[3] 
        4
        '''
        assert(isinstance(n, int)), 'l indice doit être entier'
        assert(0 <= n <= self.longueur - 1), 'l indice doit être compris entre 0 et la longueur de la liste'
        
        maillon = self.tete
        indice = 0 
        while indice < n:
            indice += 1
            maillon = maillon.suivant
            
        return maillon.valeur
    
    def inserer(self, n, valeur):
        '''
        crée un nouveau maillon de valeur valeur et l'insère au rang n
        : param n (int) 0 <= n <= longueur de la liste - 1
        : param valeur (???) la valeur du maillon d'indice n
        : pas de return (effet de bord)
        >>> l = Liste_chainee()
        >>> l.ajouter(4)
        >>> l.ajouter(3)
        >>> l.ajouter(2)
        >>> l.ajouter(1)
        >>> print(l)
        (1, (2, (3, (4, ()))))
        >>> l.inserer(2, 'b')
        >>> print(l)
        (1, (2, (b, (3, (4, ())))))
        '''
        assert(isinstance(n, int)), 'n doit être un int'
        assert(0 <= n <= self.longueur), 'n doit être entre 0 et la longueur de la liste chainée'
        
        maillon = self.tete
        if n == 0:
            ajouter(valeur)
        else:
            for i in range(n - 1):
                maillon = maillon.suivant
            nouveau_maillon = Maillon(valeur, maillon.suivant)
            maillon.suivant = nouveau_maillon
        
        self.longueur += 1
            
    def supprimer(self, n):
        '''
        enlève le maillon d'indice n de la liste chainée
        : param n (int) 0 <= n <= longueur de la liste - 1
        : pas de return (effet de bord)
        >>> l = Liste_chainee()
        >>> l.ajouter(4)
        >>> l.ajouter(3)
        >>> l.ajouter(2)
        >>> l.ajouter(1)
        >>> print(l)
        (1, (2, (3, (4, ()))))
        >>> l.supprimer(2)
        >>> print(l)
        (1, (2, (4, ())))
        >>> l.supprimer(0)
        >>> print(l)
        (2, (4, ()))
        '''
        assert(isinstance(n, int)), 'n doit être un int'
        assert(0 <= n <= self.longueur - 1), 'n doit être entre 0 et la longueur de la liste chainée'
        
        maillon = self.tete
        if n == 0:
            self.tete = maillon.suivant
        else:
            for i in range(n - 1):
                maillon = maillon.suivant
            maillon.suivant = maillon.suivant.suivant
            
        self.longueur -= 1



class Pile():
    '''
    une classe pour les piles
    '''
    def __init__(self):
        '''
        initialise une pile vide
        '''
        self.sommet = None
        
    def est_vide(self):
        '''
        renvoie True si la pile est vide et False sinon
        : return (boolean)
        >>> p = Pile()
        >>> p.est_vide()
        True
        '''
        return self.sommet == None
    
    def empiler(self, valeur):
        '''
        empile la valeur dans la pile
        : param valeur (???)
        : pas de return
        >>> p = Pile()
        >>> p.empiler(85)
        >>> p.sommet.valeur
        85
        >>> p.empiler('jhl')
        >>> p.sommet.valeur
        'jhl'
        '''
        m = Maillon(valeur, self.sommet)
        self.sommet = m

    def depiler(self):
        '''
        renvoie la valeur du sommet et supprimer cette valeur si la pile n'est pas vide
        : return (???) la valeur dépilé
        >>> p = Pile()
        >>> p.empiler(85)
        >>> p.empiler('jhl')
        >>> p.depiler()
        'jhl'
        >>> p.depiler()
        85
        '''
        assert not self.est_vide(), 'on ne peut pas dépiler une pile vide'
        m = self.sommet.valeur
        self.sommet = self.sommet.suivant
        return m

    def __repr__(self):
        '''
        donne la classe de l'objet et son sommet
        : return (str)
        '''
        reponse = ''
        if self.est_vide():
            reponse = 'Classe Pile vide'
        else:
            reponse = 'Classe Pile de sommet ' + str(self.sommet.valeur)
        return reponse

    def __str__(self):
        '''
        renvoie une chaine pour visualiser la liste
        : return (str)
        >>> p = Pile()
        >>> p.empiler(85)
        >>> p.empiler('jhl')
        >>> p.empiler(222)
        >>> print(p)
        222
        jhl
        85
        
        '''
        chaine = ''
        s = self.sommet
        while s != None :
            chaine = chaine + str(s.valeur) 
            s = s.suivant
            if s != None :
                chaine +=  '\n'
        return chaine 

class File:
    '''
    une classe pour les Files
    '''
    def __init__(self):
        '''
        contruit une file vide de longueur 0 possédant une tete et une queue
        '''
        self.tete = None # premier maillon de la queue
        self.queue = None # dernier maillon de la queue

    
    def est_vide(self):
        '''
        renvoie True si la file est vide et False sinon
        : return (boolean)
        >>> f = File()
        >>> f.est_vide()
        True
        '''
        return self.tete == None
    
    def enfiler(self, valeur):
        '''
         ajoute un élement à la file. On distinguera 3
         - le cas où la file est vide
         - le cas où la file n'est pas vide mais la queue est None
         - les autres cas.
        : param valeur (???) la valeur a ajouter
        : pas de return, effet de bord
        >>> f = File()
        >>> f.enfiler(3)
        >>> f.est_vide()
        False
        '''
        if self.est_vide():
            self.tete = Maillon(valeur, self.queue)
        elif self.queue == None:
            self.queue = Maillon(valeur, None)
            self.tete.suivant = self.queue
        else:
            m = Maillon(valeur, None)
            self.queue.suivant = m  
            self.queue = m
        
    def defiler(self):
        '''
        renvoie la valeur en tête de file et la retire de la file si la file n'est pas vide. déclenche un message d'erreur sinon.
        : return (?)
        >>> f = File()
        >>> f.enfiler(3)
        >>> f.enfiler(2)
        >>> f.defiler()
        3
        >>> f.defiler()
        2
        >>> f.est_vide()
        True
        '''
        assert not self.est_vide(), 'on ne peut pas défiler une file vide'
        
        valeur = self.tete.valeur
        self.tete = self.tete.suivant
        return valeur
    
    def __str__(self):
        '''
        renvoie la chaine permettant d'afficher la file
        : return (str)
        >>> f = File()
        >>> f.enfiler(3)
        >>> f.enfiler(2)
        >>> f.enfiler('a')
        >>> print(f)
        (3, (2, (a, ())))
        '''
        chaine = '('
        if not self.est_vide() :
            maillon = self.tete
            nb_maillon = 1
            while not maillon == None :
                chaine += str(maillon.valeur)+ ', ('
                maillon = maillon.suivant
                nb_maillon += 1
            for _ in range(nb_maillon - 1):
                chaine += ')'
        
        chaine += ')'
        return chaine
           
    
    def __repr__(self):
        '''
        renvoie une représentation de la file
        : return (str)
        '''
        return 'Classe File'






            
########################################################
###### doctest : test la correction des fonctions ######
########################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = True) 
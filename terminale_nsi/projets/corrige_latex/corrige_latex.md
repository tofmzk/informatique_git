# Correcteur de code Latex



## Le problème



Le  [Latex]() est une langage de codage de l'information qui permet de réaliser proprement des documents scientifiques.

Il permet notamment d'écrire de splendides formules mathématiques.



Pour insérer un code Latex en Markdown, il faut le placer entre deux symboles `$`.



Testez, par exemple, les codes ci-dessous en insérant, sous le bloc-code, chaque ligne entre deux `$`

``` txt
\frac {23} {12}
\sqrt {2x+3}
U_n
2^3
```





Le problème est qu'il existe différentes façon d'insérer du Latex dans un code Markdown :

* soit ainsi :

    ```txt
    $code latex$
    ```

    

* soit ainsi :

    ```txt
    $$code latex$$
    ```

    

* soit ainsi :

    ```txt
    $`code latex`$
    ```



Or, seule la troisième syntaxe est acceptée par les dépôt du type [GIT](https://framagit.org/dashboard/projects). 



Nous allons donc réaliser un programme qui va transformer un document Markdown dont les balises sont codées avec une des deux premières syntaxes en un document utilisant uniquement la troisième.



## C'est parti



### Chargement et sauvegarde des données du fichier `.md`



On l'a déjà fait de nombreuses fois. 

Nous avons besoin de deux fonctions :

* `lire_donnees(nom_fichier)` qui renvoie une liste de chaînes de toutes les lignes contenues dans le fichier au format _txt_ dont le nom est passé en paramètre 
* `sauver_donnees(lignes, nom_fichier)` qui sauvegarde au format _texte_ les données de la _lignes, une liste de chaînes de caractères, sous le nom _nom_fichier_. 



Pour gagner un peu de temps, les voici :

```python

def lire_donnees(nom_fichier):
    '''
    renvoie une file contentant les caractères du texte du fichier text passé en paramètre
    : param nom_fichier
    type str
    : return
    type File
    '''
    assert type(nom_fichier) is str,'nom_fichier is str'
    try :
        lecture = open(nom_fichier, 'r',encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
        lignes = lecture.readlines() # renvoie toutes les lignes du fichier dans la liste lignes
        lecture.close()        
    except FileNotFoundError :
        raise Exception('nom de fichier incorrect')
    return lignes


def sauver(lignes, nom_fichier):
    '''
    sauvegarde la lignes sous le nom stipulé
    : param lignes
    type list of str
    : param nom_fichier
    type str
    '''
    try :
        ecriture = open(nom_fichier,'w',encoding='utf-8') # ouvre un canal en écriture vers fichier2
        ecriture.writelines(lignes)
        ecriture.close()
    except:
        raise Exception('nom de fichier incorrect')

```





### Structure de File et _conversion_



Nous allons utiliser une file pour _défiler_ le contenu du texte et y faire nos corrections.



> Codez la fonction `convertir_en_file(liste)` qui prend en paramètre une liste de chaînes de caractères et renvoie une file dont chaque élément est un caractère de toutes les chaînes.
>
> * On se servira de notre module `module_lineaire` qui implémente la structure de File (n'oublie pas de l'importer en haut du code)
>
> * Documentez la fonction
>
> * Pour vous aider :
>
>     ```python
>     def convertir_en_file(lignes):
>         '''
>         renvoie une file contenant tous les caractères de la liste
>         un elément de la file est un caractère unique
>         : param lignes
>         typr list of str
>         : return File
>         >>> liste = ['abc', 'd', 'ef']
>         >>> test_file = convertir_enfile(liste)
>         >>> print(test_file)
>         (a, (b, (c, (d, (e, (f, ))))))
>         '''
>     ```
>
>     



Tant qu'à faire, pour sauvegarder les données, nous auront besoin de convertir à nouveau la file en liste. Allons-y !



> * Codez la fonction `convertir_en_ligne(ma_file)` qui convertit la file passée en paramètre en une liste contenant une chaîne de caractères.
>
> * Pour vous aider :
>
>     ```python
>     def convertir_en_ligne(ma_file):
>         '''
>         renvoie une liste dont le seul élément est la chaîne
>         des caractères concaténés de la file
>         : param ma_file
>         type File
>         return list of str
>         >>> liste = ['abc', 'd', 'ef']
>         >>> test_file = convertir_en_file(liste)
>         >>> convertir_en_ligne(test_file)
>         ['abcdef']
>         '''
>     ```
>
>     

### Extraction du code



Lorsque, en défilant la file, on tombe sur un `$`, c'est qu'on a trouvé un code Latex. Nous allons ici coder la fonction qui récupère ce code, en supprimant au passage un éventuelle dollar doublon.



Pour pouvoir tester notre fonction, créez un fichier Markdown contenant uniquement le code ci-dessous. Nommez-le `test1.md`

```txt
$$\frac 1 3 \times \frac 3 4$$
blablabla
```

Créez deux autres fichiers, `test2.md` et `test3.md` contenant les deux autres syntaxes possibles :

* la première : le code est compris entre deux _simples_ dollars (le premier a déjà été lu) :

	```txt
	$\frac 1 3 \times \frac 3 4$
	blablabla
	```
* le second déjà correct et qui ne doit donc pas être modifié :
	```txt
	$`\frac 1 3 \times \frac 3 4`$
	blablabla
	```





_Remarque :_

Lorsque notre fonction sera appelée, on aura déjà défilé le dollar d'_ouverture_ du code latex : en réalité le code ci-dessus était donc inséré entre deux dollars dont le premier a déjà été _lu_.



> Codez la fonction `extraire_code(ma_file)` qui est appelée lorsque le dernier caractère défilé est un dollar (prenez en compte qu'il manque donc le premier dollar à _ma_file_) et renvoie une chaîne de caractères contenant le code latex compris entre deux _simples_ dollar (l'éventuel deuxième dollar devra être enlevé)



> Testez votre fonction en utilisant le Doctest ci-dessous :
>
> ```python
> 	>>> ma_file = convertir_en_file(lire_donnees('test1.md'))
> 	>>> chaine = extraire_code(ma_file)	
> 	>>> print(chaine)	
> 	$\\frac 1 3 \\times \\frac 3 4$
> 	>>> ma_file = convertir_en_file(lire_donnees('test2.md'))
> 	>>> chaine = extraire_code(ma_file)
> 	>>> print(chaine)
> 	$\\frac 1 3 \\times \\frac 3 4$
> ```
>



_Remarque :_

Le caractère `\` est un caractère d'échappement : placé dans une chaîne de caractères, il sert a afficher "tel quel" le caractère qui le suit sans qu'il soit _interprété_.

par exemple, testez et essayez de comprendre le pourquoi de la réponse de la console :

```python
>>> print('\')
???
>>> print('\'')
???
>>> print('\\')
???
```



### insertion des _anti-cotes_



> Codez la fonction `ajouter_anticotes(code)` qui prend en paramètre une chaîne de caractères provenant de la fonction précédente et, si nécessaire, ajoute les anti-cotes après le premier dollar et avant le dernier.
>
> On utilisera le Doctest suivant :
>
> ```python
> >>> ma_file = convertir_en_file(lire_donnees('test1.md'))
>    >>> code = extraire_code(ma_file)
>    >>> code_corrige = ajouter_anti_cotes(code)
>    >>> print(code_corrige)
>     $`\\frac 1 3 \\times \\frac 3 4`$
>    >>> ma_file = convertir_en_file(lire_donnees('test2.md'))
>    >>> code = extraire_code(ma_file)
>    >>> code_corrige = ajouter_anti_cotes(code)
>    >>> print(code_corrige)
>     $`\\frac 1 3 \\times \\frac 3 4`$
>    >>> ma_file = convertir_en_file(lire_donnees('test3.md'))
>    >>> code = extraire_code(ma_file)
>    >>> code_corrige = ajouter_anti_cotes(code)
>    >>> print(code_corrige)
>     $`\\frac 1 3 \\times \\frac 3 4`$
> 
> ```
>
> 





### Défilons



Nous allons défiler la file pour générer une file corrigée.

* Tant que la file n'est pas vide, 
    * on la défile et :
        * si le caractère lu n'est pas un dollar, il n'y a rien à corriger et on enfile simplement le caractère dans la file corrigée.
        * sinon, on va utiliser deux les fonctions  définies plus haut pour :
            * extraire la partie de code Latex comprise entre ce dollar et le suivant, en prenant soin de vérifier s'il n'y a pas deux dollars 	qui se suivent.
            * Insérer les anti-cotes s'il n'y en a pas
            * enfiler le code modifié dans la file corrigée



> 
>
> * Codez la fonction `modifier_syntaxe(ma_file)` qui défile _ma_file_ comme indiquée au dessus et renvoie une file corrigée comme il se doit.
>
> * Comme toujours, documentez la fonction
> * utilisez une gestion d'erreur au cas où il y aurait une erreur critique dans le document à corriger.



Nous utiliserons un fichier Markdown un peu plus gros pour tester ce code. Voici son contenu, nommez le fichier `gros_test.md` :

```txt
# Syntaxe Latex et Gitlab (ou FramaGit)

## Syntaxe no 1

C'est la bonne syntaxe : rien ne doit changer

$`somme(n) = 0  + 1 + 2+ ... + n`$ où $`n \in \N`$

## Syntaxe no 2

il faut insérer un anti-cote après le premier dollar et avant le dollar suivant.
$somme(n) = 0  + 1 + 2+ ... + n$ où $n \in \N$

## Syntaxe no 3

Il faut enlever un dollar (il y en a deux par balise) puis ajouter les anti-cotes comme précédemment.
$$somme(n) = 0  + 1 + 2+ ... + n$$ où $$n \in \N$$

## Syntaxe no 4

on enlève un dollar, mais les anti-cotes sont déjà là donc on n'en ajoute pas :
$$`somme(n) = 0  + 1 + 2+ ... + n`$$ où $$`n \in \N`$$


```



Depuis la console :

```python
>>> ma_file = convertir_en_file(lire_donnees('gros_test.md'))
>>> file_corrigee = modifier_syntaxe(ma_file)
>>> lignes = convertir_en_ligne(file_corrigee)
>>> sauver(lignes, 'gros_test_corrige.md')
```



Reste à allez voir le fichier `gors_test_corrige.md` pour voir s'il est conforme à ce qu'on souhaite...





### La fonction principale :`corriger` 



> 
>
> Codez une fonction `corriger(nom_fichier)`qui automatise les tâches lancées plus haut depuis la console sur le fichier nommé _nom_fichier_.
>
> Documentez la fonction.
>
> 



_Remarque / question :_



Cette fonction étant la seule à être intéressante pour un utilisateur, que devrions nous modifier pour faire de ce programme un module avec une interface intéressante ?





___________________

_Par Mieszczak Christophe, licence CC-BY-SA_





​    
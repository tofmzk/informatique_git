# Syntaxe Latex et Gitlab (ou FramaGit)




## Syntaxe no 1

C'est la bonne syntaxe : rien ne doit changer

$`somme(n) = 0  + 1 + 2+ ... + n`$ où $`n \in \N`$

## Syntaxe no 2

il faut insérer une anti-cote après le premier dollar et avant le dollar suivant.
$somme(n) = 0  + 1 + 2+ ... + n$ où $n \in \N$

## Syntaxe no 3

Il faut enlever un dollar (il y en a deux par balise) puis ajouter les anti-cotes comme précédemment
$$somme(n) = 0  + 1 + 2+ ... + n$$ où $$n \in \N$$

## Syntaxe no 4

on enlève un dollar, mais les anti-cotes sont déjà là donc on n'en ajoute pas :
$$`somme(n) = 0  + 1 + 2+ ... + n`$$ où $$`n \in \N`$$

__________
Par Mieszczak Christophe
licence CC BY SA
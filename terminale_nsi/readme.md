![Logo - source perso](logo.jpg)

# Introduction

* [Le BO](BO_terminale.pdf)

* Le [Markdown](https://fr.wikipedia.org/wiki/Markdown) est le langage de balisage léger utilisé pour ces cours et TP. 
  * Ce langage permet de créer rapidement et efficacement des documents propres ,stylés, très légers et facilement exportables dans de nombreux formats (odt,  pdf, html .... et aussi docx)
  
  * Vous trouverez sur [openclassroom](https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown) de quoi maîtriser la syntaxe
  
      

## Progression

<table>
    <tr>        
        <td colspan = 2 align = center>
            Notion de programme - calculabilité
        </tD>
    </tr>
    <tr>
        <tD colspan = 2 align = center>
            Modularité, tests, assertions, gestion d'erreurs
        </tD>
    </tr>
    <tr>
        <tD colspan = 2 align = center>
            Programmation Orientée Objet
        </tD>
    </tr>
    <tr>
        <tD colspan = 2 align = center>
            Structures linéaires
        </tD>
    </tr>
    <tr>
        <tD align = center>
            Récursivité
        </tD>
        <td align = center rowspan = 1>
            Circuits intégrés            
        </td>
    </tr>
    <tr>
        <td align = center rowspan = 2> 
            Structures d'arbres
        </td>
        <td align = center>
            Les processus
        </td>
    </tr>
    <tr>
        <td align = center>
            Protocoles de routage
        </td>
    </tr>
    <tr>
       <td align = center rowspan = 2>
            Structure de graphes
        </td>
        <td align = center>
            Bases de données
        </td>
    </tr>
     <tr>
        <td align = center>
            Sécurisation des communications
        </td>
    </tr> 
    <tr>
        <td align = center >
            Programmation dynamique
        </td>
        <td align = center rowspan = 2>
            Préparation grand oral
        </td>        
    </tr>
    <tr>
        <td align = center >
            Recherche textuelle  
        </td>       
    </tr>
</table>

## Sommaire

* Langage de programmation :

  * [Notion de programme, calculabilité, décidabilité](programmation/programme/programme.md)
  * [Modularité, Tests, Assertions, Gestion d'erreurs](programmation/modularite/modularite.md)
  * [Divers paradigmes](programmation/poo/paradigmes.md)
  * Programmation Orientée Objet :

      * [Le fil rouge : un pong](programmation/poo/poo.md) et :
          * [le jeu en lui même](programmation/poo/le_jeu.md)
          * [la balle](programmation/poo/la_balle.md)
          * [la raquette](programmation/poo/la_raquette.md)
      * [Un autre exemple : l'agenda](programmation/poo/old/poo_carnet.md)
      * [En bref : récapitulons l'essentiel](programmation/poo/en_bref.md)    

  * [Récursivité](programmation/recursivite/recursivite.md)  et [exercices](programmation/recursivite/exercices.md)    

* Structures de données.

  * Les structures linéaires :

    * [Types abstraits de données](structures_de_donnees/type_abstrait_et_structures_de_donnees.md)

    * [Les structures linéaires](structures_de_donnees/structures_lineaires/structures_lineaires.md)

    * [Exercices](structures_de_donnees/structures_lineaires/exercices.md)

    * [implémenter une liste chaînée](structures_de_donnees/structures_lineaires/implementation_liste.md)

    * [Implémenter une pile](structures_de_donnees/structures_lineaires/implementation_pile.md)

    * [Implémenter une file](structures_de_donnees/structures_lineaires/implementation_file.md)  
  * Les arbres :
      * [Qu'est-ce qu'un arbre](structures_de_donnees/arbres/arbres.md)

      * [Exercices débranchés](structures_de_donnees/arbres/exercices.md)

      * [Parcourir un arbre](structures_de_donnees/arbres/parcourir.md)

      * [Arbres binaires de recherche](structures_de_donnees/arbres/arbres_de_recherche.md)

      * [Implémenter un arbre binaire et un arbre binaire de recherche en Python](structures_de_donnees/arbres/implementation.md)

      * [Algorithmes sur les arbres](structures_de_donnees/arbres/algo_arbre.md)
  * Les graphes
      * [Introduction](structures_de_donnees/graphes/introduction.md)
      * [Exercices sur les graphes](structures_de_donnees/graphes/exercices.md)
      * [Implémentation avec une matrice adjacente](structures_de_donnees/graphes/implementation_matrice.md)
      * [Implémentation avec un dictionnaire](structures_de_donnees/graphes/implementation_dictionnaire.md)

* Algorithmique :

  * Algorithmes sur les graphes :
      * [Parcourir un graphe en profondeur d'abord](structures_de_donnees/graphes/parcourir_graphe_profondeur.md)
      * [Chercher un chemin  un graphe](structures_de_donnees/graphes/recherche_chemins.md)    
      * [Parcourir un graphe en largeur d'abord](structures_de_donnees/graphes/parcourir_graphe_largeur.md)
      * [Chercher un cycle dans un graphe](structures_de_donnees/graphes/cycle.md)


  * [Diviser pour mieux régner](algorithmique/diviser_pour_mieux_regner/tri_fusion.md)

  * Programmation dynamique :
    * [Introduction : la suite de Fibonacci](algorithmique/programmation_dynamique/fibonacci.md)
    * [Problème de la pyramide](algorithmique/programmation_dynamique/pyramide.md)
    * [Le rendu de la monnaie](algorithmique/programmation_dynamique/rendu_monnaie.md)

  * [Recherche textuelle](algorithmique/recherche_textuelle/recherche_textuelle.md)

* Bases de données.

  * [Système de Gestion de Base de Données](bases_de_donnees/sgbd.md)

  * [Le modèle relationnel](bases_de_donnees/modele_relationnel.md)

  * [Créer une base de données](bases_de_donnees/creer_une_base.md)

  * [Langage SQL](bases_de_donnees/sql.md)

  * [Exercice débranché : l'agence de voyage](bases_de_donnees/agence_de_voyage.md)

  * [SQL et Python](bases_de_donnees/sql_et_python.md)    

* Architecture

  * [Les SoC](architecture/soc/soc.md)

  * [Les processus](architecture/processus/les_processus.md) et [exercices](architecture/processus/exercices.md)

  * routage dans un réseau :
    
    * [Le routage](architecture/routage/routage.md)
    * [Exercices](architecture/routage/exercices.md)

  * Sécurisation des communications :
    
    * [Clés, signatures et certificats](architecture/securisation/securisation.md)
    
    * [Exercices](architecture/securisation/securisation _exercices.md)
    
        

## Liste des projets

* [Le taquin - partie 1](projets/taquin/taquin.md) (POO)
* [Labyrinthe - partie 1](projets/labyrinthe/labyrinthe.md) (POO)
* [Module de calculs de fractions](projets/fractions/fractions.md) (POO)
* [Correcteur de code Latex](projets/corrige_latex/corrige_latex.md) (Files)
* [Le retour du Taquin](projets/taquin/taquin_suite.md) (POO et Piles)
* [Le snake](projets/snake/snake.md) (POO et structures linéaires)
* [Les tours de Hanoï](projets/hanoi/tours_de_hanoi.md) ( POO, Piles)
* [Autour des villes de France](projets/villes_de_france/villes_de_france.md) (base de donnée)
* [Maître Hibou](projets/le_hibou/le_hibou.md) (Arbres, récursivité)
* [Le codage de Huffman](projets/huffman/huffman.md) (Arbres, récursivité)
* [Construire un labyrinthe](projets/labyrinthe/construire_labyrinthe.md) (graphes, parcours en profondeur)
* [Labyrinthe - partie 2 : sortir du labyrinthe](projets/labyrinthe/labyrinthe_partie_2.md) (recherche d'un chemin dans un graphe)
* [Labyrinthe - partie 3 : avec pygame](projets/labyrinthe/labyrinthe_ partie_3.md) (utilisation d'un module)
* [Algorithme de Dijkstra](projets/dijkstra/dijkstra.md) (routage et graphe pondéré)



## Sujets de Bac

* [2023 - SQL-Réseau- Files(sans POO)](sujets_bac/sujet_03(sql_reseau_files).pdf)



Exos type bac :

* [bac blanc 2022](entrainement/bac_blanc_2022.md) --> [correction](entrainement/bac_blanc_2022_correction.md)

__________

Par Mieszczak Christophe CC BY SA

_source image : production personnelle_ 
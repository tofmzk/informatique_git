# Implémentation d'une liste chaînée



Créez un programme `module_lineaire.py` dans lequel nous allons définir les classes nécessaires à l'implémentation d'une liste chaînée... et d'autres structures linéaires par la suite.

N'oublions pas les lignes indispensables aux Doctests et l'entête habituelle.

```python
# -*- coding: utf-8 -*-
'''
    module pour les structures linéaires
    : Auteur
'''






#tout en bas####################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = True) 
```




## Un maillon



Commençons donc par construire la classe `Maillon`  qui possède une valeur et un lien vers le maillon suivant. 

```Python
class Maillon() :
    '''
    une classe pour construire un maillon d'nue liste chainee
    '''
    def __init__(self, valeur = None, suivant = None) :
        '''
        construit un maillon avec une valeur et un lien vers le maillon suivant.
        : param valeur (variable)None par défaut
        : param suivant(Maillon)None par défaut
        '''
        self.valeur = valeur
        self.suivant = suivant
    
    def __repr__(self):
        '''
        donne la classe de l'objet et sa valeur
        : return (str)
        '''
        return 'Maillon de valeur ' + str(self.valeur)
    
    
```



Testons un peu :

```python
>>> m = Maillon('a')
>>> m.valeur
???
>>> m.valeur
???
>>> m.suivant
???
>>> m.suivant
>>> m2 = Maillon('b', m)
>>> m2.valeur
???
>>> m2.suivant
???
>>> m2.suivant.valeur
???
```

Comme vous le savez, on ne doit pas accéder directement aux attributs d'une classe depuis l'extérieur de cette classe. Il faut doc codez accesseurs et mutateurs pour chaque attribut.

> Coder les accesseurs `acc_valeur` et `acc_suivant` ainsi que les mutateurs `mut_val` et ` mut_suivant`.





## La classe `ListeChainee`



Définissons maintenant la classe `ListeChainee` qui possède un maillon de _tête_ (vide au départ) et un attribut `longueur` qui correspondra au nombre d'élément(s) dans la liste (0 au départ donc). Ce second attribut n'est pas nécessaire mais va optimiser celle-ci.

```python
class ListeChainee() :
    '''
    une classe pour implémenter une liste chainee
    '''
    def __init__(self):
        '''
        construit une liste chainee vide de longueur 0
        '''
        self.tete = None
        self.longueur = 0
```

Par soucis du détails, voici la méthode `__repr__` qui décrit la classe :

```python
    def __repr__(self):
        '''
        donne la classe de l'objet et sa taille
        : return (str)
        '''
        return 'Class Liste_chainées de taille ' + str(self.longueur)
```

La première méthode (on parle aussi de `primitive` lorsqu'il s'agit des méthodes fondamentales de l'objet) est celle qui ajoute une valeur en tête de liste. 

Pour cela :

* on crée un maillon contenant la valeur et dont le maillon suivant est le maillon de tête de la liste
* on remplace le maillon de tête de la liste par ce nouveau maillon
* on augmente la longueur de la liste de 1

> Complétez :
>
> ```python
> 	def ajouter(self, valeur):
> 		'''
> 		ajoute un maillon de valeur précisé en tête de liste
> 		: param valeur (???)
> 		: pas de return
> 		>>> l = ListeChainee()
> 		>>> l.ajouter(2)
> 		>>> l.tete.acc_valeur()
> 		2
> 		>>> l.ajouter(3)
> 		>>> l.tete.accç_valeur()
> 		3
> 		>>> l.tete.acc_suivant().acc_valeur()
> 		???
> 		>>> l.longueur
> 		2
> 		'''       
> ```



On va avoir besoin d'un `prédicat` pour savoir si la liste est vide ou pas.

> Complétez :
>```python
		def est_vide(self):
        		'''
        		renvoie True si la liste est vide et False sinon
        		: return (boolean)
         	>>> l = ListeChainee()
         	>>> l.est_vide()
         	True
         	>>> l.ajouter(1)
         	>>> l.est_vide()
         	False
        		'''




On doit maintenant pouvoir supprimer la tête de la liste chainée :

> Définissez et codez la primitive `supprimer` qui, si la liste n'est pas vide, supprime le maillon de tête.




Pour visualiser tout cela, ajoutons la méthode `__str__` pour pouvoir afficher la liste :

* on ajoute `(` à la chaine.
* on se place sur le maillon de tête
* Tant que ce maillon n'est pas vide :
    * on ajoute à la chaine la valeur de ce maillon suivie de `,(`
    * on remplace le maillon par son suivant
* il ne reste plus qu'à fermer le bon nombre de parenthèse(s)



> Complétez la méthode ci-dessous :
>```python
		def __str__(self):
         	'''
        		renvoie une chaine pour visualiser la liste
        		: return (str)
        		>>> l = ListeChainee()
        		>>> print(l)
        		()
        		>>> l.ajouter(1)
        		>>> print(l)
        		(1, ())
        		>>> l.ajouter(2)
        		>>> print(l)
        		(2, (1, ()))
        		'''
        		chaine = '('
             # on s place sur le maillon de tete
             maillon = self.tete
        		while maillon != None :
                 # on ajoute la valeur du maillon à la chaine puis ', ('
             # reste à fermer les parenthèses avec une boucle for
                 
             # plus qu'à renvoyer la chaîne
               return chaine 
                
     ```







Testons un peu tout cela :

```python
>>> l = ListeChainee()
>>> l
???
>>> print(l)
???
>>> len(l)
???
>>> l.ajouter(4)
>>> l.ajouter(3)
>>> l.ajouter(2)
>>> l.ajouter(1)
>>> print(l)
???
>>> len(l)
???
```

#### Renvoyer le nième maillon

Pour de nombreuses primitives, on va avoir besoin d'accéder au nième maillon de la liste, s'il existe.

Pour cela, on va avoir besoin d'une méthode `acceder(self, n)` qui renvoie le nième maillon. Pour que ce dernier existe il faut que n soit inférieur strictement à la longueur de la liste. Une assertion nous permettra de garantir cela.

Allez hop, on code en commençant par la spécification et quelques tests


#### Insérer un maillon



Nous avons vu plus haut que l'insertion à un indice _n_ donné était LE point fort des liste chaînée. Nous avons déjà réaliser une insertion en tête de liste (la primitive `ajouter`). Nous allons maintenant réaliser la primitive `inserer(self, n, valeur)` qui insère la _valeur_ à l'indice _n_ de la liste. Attention _n_ est un entier positif ou nul inférieur ou égal à la longueur de la liste.

![modifier liens](https://upload.wikimedia.org/wikipedia/commons/a/a1/Liste_ajout3.png)



Deux cas sont à distinguer :

* si _n_ vaut 0, il suffit d'appeler la méthode `ajouter`.
* sinon :
    * il va falloir atteindre le maillon d'indice _n - 1_ 
    * On crée maintenant un _nouveau_maillon_ avec :
        * pour _valeur_ celle passée en paramètre.
        * pour _suivant_ celui du maillon d'indice _n - 1_ trouvé précédemment

    * On change le _suivant_ du maillon d'indice _n - 1_ pour notre _nouveau_maillon_.
    * On ajoute 1 à l'attribut _longueur_ de la liste.




> Définissez la primitive `inserer(self, n, valeur)` .
>
> * Documentez là
>
> * Utilisez les tests ci-dessous :
>
>     ```python
>     >>> l = ListeChainee()
>     >>> l.ajouter(4)
>     >>> l.ajouter(3)
>     >>> l.ajouter(2)
>     >>> l.ajouter(1)
>     >>> l.inserer(2,'a')
>     >>> print(l)
>     (1, (2, (a, (3, (4, ())))))
>     >>> l.inserer(0,'b')
>     >>> print(l)
>     (b, (1, (2, (a, (3, (4, ()))))))
>     >>> l.inserer(6,'c')
>     >>> print(l)
>     (b, (1, (2, (a, (3, (4, (c, ())))))))
>     ```
> * Utilisez une assertion pour que _n_ ne sorte pas de son domaine : il doit être entier, positif ou nul et inférieur ou égal au nombre d'éléments de la chaîne.



### Supprimons

la méthode `supprimer(self, n)` enlève le maillon d'indice _n_ de la liste en modifiant l'attribut _suivant_ du maillon qui le précède dans la liste pour le relier directement au maillon qui le suit. Attention, _n_ est un entier positif ou nul **strictement** inférieur à la longueur de la liste.



> 
>
> * Définissez et codez cette primitive
>* Documentez la et donnez un Doctest (au moins)
> * Utilisez une assertion pour que _n_ ne sorte pas de son domaine
>
> 



#### Accéder à un indice



La méthode réservée `__getitem__(self, n)` permet un accès à la valeur d'indice _n_ dans la liste via la syntaxe `l[n]`, comme sur une liste Python habituelle.

Pour coder cette primitive pour une liste chaînée :

* On se place dans le maillon de tête de la liste
* on passe n fois de suite au maillon suivant
* on renvoie la valeur du maillon dans lequel on se trouve à la fin de la boucle

> Complétez la méthode  `__getitem__(self, n)` ci dessous :
>
> ```python
> 	def __getitem__(self, n):
> 		'''
> 		renvoie la valeur du maillon d'indice précisé
> 		: param n (int)compris entre 0 et la longueur de la liste exclue
> 		: return (??) la valeur du maillon d'indice n
> 		>>> l = ListeChainee()
> 		>>> l.ajouter(4)
> 		>>> l.ajouter(3)
> 		>>> l.ajouter(2)
> 		>>> l.ajouter(1)
> 		>>> l[0]
> 		1
> 		>>> l[1]
> 		2
> 		>>> l[3] 
> 		4
> 		'''        
>      assert
>      assert
> ```
>
> 
>
> 



## Comparons listes chaînées et tableaux

Nous allons comparer l'efficacité des deux structures en terme de temps en réalisant un grand nombre d'insertions en tête puis un grand nombre d'accès à un indice donné pour chacune des structures.



Créez un programme `comparer_listes.py` et importez les modules dont nous allons avoir besoin :



```python
import random
import math
import module_lineaire
import time
```



Pour commencer, codez les deux fonctions ci-dessous qui vont nous permettre de créer des listes chaînées ou des liste Python :

```python

	def generer_liste_chainee(taille):
    	'''
    	renvoie une liste de type Liste_chainee de longueur taille	
    	comportant des valeurs aléatoires
	  	: param taille (int)
    	: return (ListeChainee) une liste chainee de longueur taille
    	'''
    	liste_chainee = module_lineaire.ListeChainee()

    def generer_liste_python(taille):
    	'''
    	renvoie une liste Python de longueur taille
    	comportant des valeurs aléatoires
    	: param taille (int)
	    : return (list) une liste python de longueur taille
	    '''
	    liste_python = []
```



La fonction `comparer_ajout(taille, nbre_ajouts)` doit réaliser les tâches suivantes :

* Elle générer tout d'abord une liste chaînée de longueur _taille_.

* ensuite elle utilise le module `time` pour obtenir l'instant de départ :

    ```python
    debut = time.time()
    ############Algo
    
    ################
    fin = time.time()
    duree = fin - debut
    ```

* elle poursuit en ajoutant en tête _nbre_ajouts_ valeurs (par exemple 'a') à la liste chaînée.

* elle utilise le module `time` pour mesurer l'instant où la boucle prend fin.

* elle obtient le temps mis par la boucle pour la liste chaînées par simple soustraction _fin - debut_.

* elle refait la même chose avec une liste Python

* pour finir elle renvoie le tuple `temps_chainee, temps_python`.



Allez-y, codez la fonction !

```python
	def comparer_ajout(taille, nb_insertions):
    	'''
	  	renvoie le temps mis par une liste_chaine puis une liste python
	    pour réalise nb_insertions en tête de liste
	    dans des liste de longueur taille
	    : param taille (int)
    	: param nb_insertions (int)
    	return (tuple) temps_list_chainee, temps_liste_python
    	'''
```



> * Comparer les deux structures pour 1000 insertions :
>     * avec des listes courtes (taille = 100)
>     * avec des listes longues (taille = 1000000)
> * Conclusion ?



>  De la même façons, définissez la fonction `comparer_acces(taille, nbre_acces, indice)` qui comparera les temps des deux structures pour accéder à l'indice voulu _nbre_acces_ fois dans des listes de longueur _taille_.
>
>  * Testez avec des listes de petites ou grandes tailles
>  * Prenez des indices faibles ou grands (< _taille_)
>  * Conclusion ?



## Plus loin ?



Nous n'avons pas coder toutes les primitives que peut posséder une liste. Sauriez vous,par exemple, réaliser les méthodes suivantes :

* la méthode réservée `__contains__(self, valeur)` , la méthode `in`, renvoie `True` si la _valeur_ est présente dans la liste et `False` sinon

* la méthode réservée `__setitem__(self, n, valeur)` est le pendant mutateur de la méthode de l'accesseur `__getitem__` codé plus haut : elle modifie la valeur du maillon d'indice _n_ en la remplaçant par celle passée en paramètre.

* la méthode `renverser(self)` qui ré-arrange les éléments de la liste dans le sens inverse.

_____________

Par Mieszczak Christophe

licence CC BY SA


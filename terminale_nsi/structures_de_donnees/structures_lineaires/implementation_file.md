# Les Files



## Les méthodes des files



Une structure de file ne possède que 3 primitives  :

* `est_vide`  renvoie `True`si la pile est vide et `False`sinon.
* `enfiler`  ajoute un élément en queue de file.
* `defiler` enlève et renvoie l'élément en tête de file.



Nous allons implémenter cette structure en Python.



Nous aurons besoin de quelques méthodes supplémentaires :

* `__init__(self)`  qui initialise une file vide avec deux attributs :
    * _tete_ : le maillon de tête (`None` à l'initialisation)
    * _queue_ : le maillon de queue (`None` à l'initialisation)
* `__str__(self)` qui renvoie la chaîne de caractère qui sera interprétée lors d'un `print`. 
* `__repr__(self)` qui renvoie simplement `classe File` pour représenter la classe.



**Nous allons utiliser la structure chaînée** et donc des maillons pour implémenter une file. Ce n'est pas la seule façon de faire comme nous le verrons en exercice, mais elle est très efficace.

Nous allons coder à la suite du module `module_lineaire`, ce qui nous permettra d'utiliser la classe `Maillon` qui y est déjà implémentée.

Notre File aura une tête, comme une liste chaînée classique, mais aussi une queue (un dernier élément), ce qui permettra d'ajouter un élément sans être obligé de parcourir la File à chaque fois.



_Remarque :_

Dans une file, la queue n'est pas le dernier élément mais est composée de tous les éléments qui suivent la tête. Pour notre implémentation, nous n'avons besoin que du dernier élement de la file, c'est pour cela qu'on décide, pour elle, d'appeler queue uniquement ce dernier élément.





Voici l'implémentation, sans le code. Complétez là !

```python
class File:
    '''
    une classe pour les Files
    '''
    def __init__(self):
        '''
        contruit une file vide de longueur 0 possédant une tete et une queue
        '''
        self.tete = None # premier maillon de la queue
        self.queue = None # dernier maillon de la queue

    
    def est_vide(self):
        '''
        renvoie True si la file est vide et False sinon
        >>> f = File()
        >>> f.est_vide()
        True
        '''

    
    def enfiler(self, valeur):
        '''
         ajoute un élement à la file. On distinguera 2
         - le cas où la file est vide 
         - le cas où la file n'est pas vide
         - les autres cas.
        : param valeur (?) la valeur a ajouter
        >>> f = File()
        >>> f.enfiler(1)
        >>> f.enfiler(2)
        >>> f.est_vide()
        False
        '''

        
    def defiler(self):
        '''
        renvoie la valeur en tête de file et la retire de la file si la file n'est pas vide. déclenche un message d'erreur sinon.
        : return (?)
        >>> f = File()
        >>> f.enfiler(1)
        >>> f.enfiler(2)
        >>> f.defiler()
        1
        >>> f.defiler()
        2
        >>> f.est_vide()
        True
        '''

    
    def __str__(self):
        '''
        renvoie la chaine permettant d'afficher la file
        : return (str)
        >>> f = File()
        >>> f.enfiler(3)
        >>> f.enfiler(2)
        >>> f.enfiler('a')
        >>> print(f)
        (3, (2, (a, ())))
        '''
        
    
    def __repr__(self):
        '''
        renvoie une représentation de la file
        : return (str)
        '''
        return 'Classe File'
            
     
 
            
################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose = True)            
        
```





__________

Par Mieszczak Christophe

Licence CC BY SA



  

  

​    
















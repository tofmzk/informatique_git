# Implémentation d'une Pile



## Les méthodes



Une structure de pile ne possède que 4 primitives  :

* son `__init__` qui permet d'initialiser une pile vide.
* `est_vide` (ou `is_empty`) renvoie `True`si la pile est vide et `False`sinon.
* `empiler` (ou `push`) ajoute un élément en tête de Pile.
* `dépiler`(ou `pop`) enlève et renvoie l'élément en tête de Pile.



Nous allons implémenter cette structure en Python.



Nous aurons besoins de quelques méthodes :

* `__init__(self)` l'initialisateur qui crée une pile vide avec un seul attribut :
* _sommet_ :  qui contient soit rien soit le maillon du sommet.


* `__str__(self)` qui renvoie la chaîne de caractère permettant l'utilisation du `print`. Attention, on ne pourra pas utiliser la méthode `depiler` sinon la pile sera détruite à la fin de l'affichage... il faudra donc parcourir la pile de maillon en maillon.
* `__repr__(self)` qui renvoie simplement `Classe Pile` pour représenter la classe.



**Nous allons utiliser la structure chaînée** pour implémenter notre Pile. Ce n'est pas la seule façon de faire comme nous le verrons en exercice mais elle est très efficace.



Nous allons coder à la suite du module `module_lineaire`, ce qui nous permettra d'utiliser la classe `Maillon` qui y est déjà implémentée.



Voici l'implémentation, sans le code ci-dessous. Complétez le !

```python
class Pile:
    '''
    une classe pour modéliser une pile
    '''
    def __init__(self):
        '''
        construit une pile vide
        '''
        self.sommet = None

    def empiler(self, valeur):
        '''
        ajoute la valeur en tête de pile
        '''

    def est_vide(self):
        '''
        renvoie True si la pile est vide et False sinon
        : return
        boolean
        >>> p = Pile()
        >>> p.est_vide()
        True
        >>> p.empiler(1)
        >>> p.est_vide()
        False
        '''



    def depiler(self):
        '''
        enlève si possiblela valeur en tête de pile et la renvoie ou déclenche un message d'erreur si la pile est vide.
        : return
        type ?
        >>> p = Pile()
        >>> p.empiler(1)
        >>> p.empiler(2)
        >>> p.empiler('a')
        >>> p.depiler()
        'a'
        >>> p.depiler()
        2
        '''


    def __str__(self):
        '''
        renvoie une chaine pour visualiser la pile
        : return
        type str
        >>> l = Pile()
        >>> l.empiler(1)
        >>> print(l)
        1
        >>> l.empiler(2)
        >>> print(l)
        2
        1
        '''



    def __repr__(self):
      	'''
      	renvoie une chaine pour décrire la classe
      	'''
    	return 'Classe Pile' 

```





__________

Par Mieszczak Christophe

Licence CC BY SA






















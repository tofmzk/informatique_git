## Structures de données linéaires

Nous parlons ici de `types abstraits` de données dans lesquelles ces dernières sont organisées de façon séquentielle, les unes derrières les autres. On peut alors représenter ces données dans un schéma où elles apparaissent alignées.



En Python, les dictionnaires (_dict_) , les tuples (_tuple_) et les tabeaux (_list_) sont des structures linéaires.

dico = {'A' : 1, 'B' : 2 ....}

tup = ('toto', 'pierre', 'alice')

tab = [24, 35, 78]



Nous aurons la plupart du temps besoin de pouvoir :

* créer une structure vide
* tester si la structure est vide ou non
* accéder aux éléments ou a un élément particulier (le premier ou le dernier) de la structure.
* insérer un nouvel élément dans la structure. 
* compter combien d'éléments sont présents dans la structure.





## Les tableaux



Pour stocker une séquence d'éléments, on peut utiliser une structure en _tableau_.

On commence par _réserver_ une zone, contigüe et ordonnée, en mémoire assez importante pour y ranger ces données.

| adresse mémoire |     0     |     1     |     2     |     3     |     4     |     5     |
| :-------------: | :-------: | :-------: | :-------: | :-------: | :-------: | :-------: |
|    éléments     | élément 1 | élément 2 | élément 3 | élément 4 | élément 5 | élément 6 |



#### Point fort 

Elle permet un accès direct et rapide à n'importe quel élément du tableau à partir de leur indice (rang ou index) : l'élément _n_ est _rangé_ dans la zone mémoire _m_. 



#### Points Faibles

* Un tableau est _statique_ : il est limitée en terme de nombre d'éléments dès sa création par la place qui lui est réservé en mémoire et on ne peut donc pas ajouter de nouveaux éléments si leur nombre maximum, défini initialement, est déjà atteint.

* insérer un élément à une position dans le tableau, tout particulièrement en première position (on dit `en tête` ) demande un grand nombre de manipulations. Voyons le tableau ci-dessous par exemple :

    | mémoire  |  0   |   1   |  2   |  3   |  4   |      |
    | :------: | :--: | :---: | :--: | :--: | :--: | :--: |
    | éléments | -13  | 'abc' |  23  | 'a'  |  -1  |      |
    
    Je souhaite insérer en première position la valeur **2** . Il faut déjà qu'il reste de la place pour le faire. Si c'est le cas (comme ici) , il va falloir décaler toutes les valeurs de la listes d'un rang dans la zone mémoire allouée ce qui nous oblige à réaliser autant de décalages qu'il y a de données dans le tableau (ici 4).
    | mémoire  |  0   |  1   |   2   |  3   |  4   |  5   |
    | :------: | :--: | :--: | :---: | :--: | :--: | :--: |
    | éléments |      | -13  | 'abc' |  23  | 'a'  |  -1  |
    
    Il faut maintenant insérer l'élément dans la première _case_ de la mémoire :

	| mémoire  |   0   |  1   |   2   |  3   |  4   |  5   |
	| :------: | :---: | :--: | :---: | :--: | :--: | :--: |
	| éléments | **2** | -13  | 'abc' |  23  | 'a'  |  -1  |

* Pour supprimer une donnée, il faudra faire le même type de décalage mais dans l'autres sens.

#### Bilan 

 Cette structure est donc adaptée lorsqu'on a besoin d'accéder rapidement aux éléments, mais se révèle _coûteuse_ en terme de nombre d'opérations à effectuer, lorsqu'il s'agit d'insérer ou supprimer des données, particulièrement en début de liste (on dit ` en tête`). Le coût est proportionnel au nombre _n_ d'éléments dans le tableau, c'est à dire d'ordre **_O(n)_**.



#### Et python ?

Dans le langage Python, il est possible de modifier _dynamiquement_ le tableau en augmentant si besoin la zone allouée en mémoire. On peut alors ajouter facilement un élément en bout de tableau (on dit en `queue`) . On parle de `tableaux dynamiques`. Les célèbres listes de Python sont donc en réalité des tableaux. Des tableaux particuliers car dynamiques, mais des tableaux tout de même !

Les listes Python étant des tableaux, l'accès à un élément via son index est très rapide... mais l'insertion ou la suppression restent beaucoup plus lents.

## Les dictionnaires

La structure de `dictionnaire`, aussi appelée `tableau associatif` ressemble fort à un tableau.

Cependant, au lieu d'associer chaque élément à son indice, il associe un élément à une clé.

Un dictionnaire est donc un ensemble non ordonné de de couples `clé : valeur`.

Nous avons déjà étudié les dictionnaires en Python l'année dernière. [Cliquez ici](../../../premiere_nsi/types_construits/dictionnaires.md) pour y jeter un oeil.



## Liste chaînée



Une liste chaînée n'utilise pas un tableau mais une série de _maillons_ chacun composé d'une valeur et d'un lien vers le maillon suivant, le dernier maillon étant vide.



![structure chaînée](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Singly-linked-list.svg/1920px-Singly-linked-list.svg.png)



_Remarque :_  On pourrait représenter cette liste de la façon suivante : (12, (99, (37, () ))).



#### Point fort 

Cette structure est particulièrement performante pour insérer un maillon n'importe où (spécialement en `tête`) car il suffit pour cela de:

* créer un nouveau maillon :

    ![création nouveau maillon](https://upload.wikimedia.org/wikipedia/commons/0/01/Liste_ajout1.png)

     

    

* Modifier les _liens_ entre les maillons :

    ![modifier liens](https://upload.wikimedia.org/wikipedia/commons/a/a1/Liste_ajout3.png))



Il n'y a donc pas besoin de décaler tous les autres maillons, comme c'était le cas pour un tableau : **le coût est faible et constant, quelque soit la taille de la liste chaînée**.



#### Point faible

Pour _atteindre_ un maillon particulier de la liste, il faudra parcourir tous les maillons en partant du premier, le maillon de tête, qui est le seul accessible directement. Il faudra ensuite _remonter_ la liste jusqu'au maillon désiré. l'accession à un maillon particulier via son indice dans la liste est donc beaucoup plus coûteux que pour les tableaux !



### Bilan

On utilisera une liste chaînée lorsque l'insertion ou la suppression des données de la structure sont plus souvent utilisées que l'accès directe à un élément de la structure selon son index.



#### Remarque :

Nous n'étudierons ici que ce type de liste chaînée mais il en existe de nombreuses variantes comme :

* La liste doublement chaînée :

    ![double chaînage](https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Liste_doublement_cha%C3%AEn%C3%A9e.png/800px-Liste_doublement_cha%C3%AEn%C3%A9e.png)

     

    

* La liste chaînée avec boucle où le dernier _maillon_ est relié au premier.



## Les piles



Les `piles` sont des structures de données qui fonctionnent sur le principe du **premier arrivé dernier sortie**. Pour une pile, on parle de liste **LIFO** pour **L**ast **I**n, **F**irst **Out**.



Il nous faut pouvoir : 

* `initialiser` une pile vide.
* savoir si une pile `est vide` ou pas.
* `empiler` un nouvel élément au sommet de la pile.
* `dépiler` l'élément situé au sommet de la pile.

![une pile](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/Stack_%28data_structure%29_LIFO.svg/800px-Stack_%28data_structure%29_LIFO.svg.png)



Par analogie, on peut penser à une pile de crêpe :

*  la première crêpe que vous avez cuite se retrouve au bas de la pile et ce sera la dernière a être mangée.
* la dernière crêpe cuite se retrouve en haut de la pile et sera mangée en premier.

**On est bien dans une structure premier entrée, dernier sorti !**

<img src="https://c.pxhere.com/photos/8e/d3/pancake_stack_pile_breakfast_brunch-1149061.jpg!d" alt="pile de crêpes" style="zoom: 33%;" />



## Les files



Les `files` sont des structures de données qui fonctionnent sur le principe du **premier arrivé premier sortie**. Pour une file, on parle de liste **FIFO** pour **F**irst **I**n, **F**irst **Out**.

Il nous faut pouvoir :

* `initialiser` une file vide
* savoir si une file `est vide` ou non
* `enfiler` un élément à la suite des autres, c'est à dire en queue.
* `défiler` l'élement situé en tête de file.



![une file](https://upload.wikimedia.org/wikipedia/commons/2/2a/FIFO_PEPS.png)



Par analogie, on peut penser à une file d'attente :

*  les gens entrent dans la file par l'arrière c'est à dire à la queue.
*  le premier qui entre dans la file et qui est donc à sa tête sera le premier à en sortir

**On est bien sur le principe du premier arrivé, premier sorti**

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/File_d%27attente_Mus%C3%A9e_de_Varsovie.jpg/800px-File_d%27attente_Mus%C3%A9e_de_Varsovie.jpg" alt="file d'attente" style="zoom: 50%;" />



​    

__________

Par Mieszczak Christophe

Licence CC BY SA

sources images :

* [wikipédia, listes chaînée, domaine public ](https://commons.wikimedia.org/wiki/File:Singly-linked-list.svg)

* [wikipédia, nouveau maillon, CC SA ](https://commons.wikimedia.org/wiki/File:Liste_ajout1.png)

* [wikipédia, double chaînage, domaine public](https://commons.wikimedia.org/wiki/File:Liste_doublement_cha%C3%AEn%C3%A9e.png)

* [wikipédia,pile, CC SA ](https://commons.wikimedia.org/wiki/File:Stack_(data_structure)_LIFO.svg)

* [pxhere, pile de crêpes, CC0](https://pxhere.com/fr/photo/1149061)

* [wikipédia,file, GNU ](https://commons.wikimedia.org/wiki/File:FIFO_PEPS.png)

* [wikipédia, file d'attente, CC BY SA, auteur Mith](https://commons.wikimedia.org/wiki/File:File_d%27attente_Mus%C3%A9e_de_Varsovie.jpg)

  ​    
















# Exercices sur les structures linéaires

## Listes chaînées

Une liste chaînée est composée de maillons.

Le maillon vide est noté `()`

Chaque maillon non vide est représenté par le couple `(valeur, maillon_suivant)`. 

* la valeur est la donnée qui est stockée dans la liste.
* Si ce maillon est attaché à un autre maillon, le `maillon_suivant` indique quel est ce maillon. Sinon, le suivant est le maillon vide.

Le seul maillon accessible directement dans une liste chaînée est le maillon de tête, c'est à dire le premier.

Pour créer un nouveau maillon, il faut donc : 

* soit créer un maillon vide
* soit créer un maillon avec sa valeur et son maillon suivant qui doit avoir été préalablemnt créer.



#### Exercice 

Indiquer comment construire la liste chaînée (A, (B, (C, (D, ()))), maillon par maillon.



#### Exercice 

Considérons la liste chaînée dans laquelle seul le maillon de tête est accessible directement. 

1. Écrire en langage courant un algorithme qui renvoie le nombre de valeurs stockée dans la liste.
2. Écrire en langage courant un algorithme renvoyant la valeur du nième maillon d'une liste chaînée.
3. Écrire en langage courant un algorithme qui insère un maillon de valeur _v_ en tête de liste.
4. Écrire en langage courant un algorithme qui insère un maillon de valeur _v_ à un rang donné _n_ > 0.
5. Écrire en langage courant un algorithme qui supprime le maillon de valeur en tête de liste.
6. Écrire en langage courant un algorithme qui supprime le maillon de rang donné _n_ > 0.





## Piles et Files



#### Exercice 

On forme une  pile en empilant successivement toutes les lettres de l'alphabet par ordre alphabétique en commençant donc par le A. 

1. On dépile une lettre. Quelle est cette lettre ?
2. On dépile maintenant 5 lettres. Quel élément se trouve alors au sommet de la pile ?



#### Exercice 

Même exercice avec une file.



####  Exercice 


On se donne trois piles _P1_, _P2_ et _P3_. 

* La pile _P1_ contient des nombres entiers positifs empilés, par exemple 4, 9, 2, 5, 3 et 8, 8 étant au dessus de la pile.
* Les piles _P2_ et _P3_ sont initialement vides.



##### Partie 1 : on ne code pas.

On utilise une piles de papiers sur lesquels on écrit les valeurs présentes dans la pile _P1_. En manipulant les papiers (dépilant, empilant ...) trouvez les manipulations à faire pour réaliser les exercices ci-dessous. Ecrivez, ensuite, en pseudo-code, l'algorithme à réaliser dans chaque cas.

1. On veut inverser l'ordre des éléments de la pile _P1_.

2. On veut déplacer un entier sur deux dans _P2_ ou _P3_. 

3. On veut déplacer les entiers de _P1_ de façon à avoir les entiers pairs dans _P2_ et les impairs dans _P3_. 

4. On veut faire la même chose mais les nombres dans _P2_ et _P3_ doivent être dans le même ordre qu'il l'étaient dans _P1_



##### Partie 2 : on code.

Implémentez ces fonctions en Python.



#### Exercice 

On dispose uniquement de la structure de donnée de Pile et on souhaite créer, en utilisant deux piles P1 et P2, une nouvelle structure de File. On décide que la P1 contiendra les valeurs de la file. Le sommet de P1 sera la tête de la file.



1. Comment vérifier que la file est vide ou pas ?

2. Comment défiler l'élément de tête de la file ?

3. Comment faire alors pour enfiler un élément en queue de file ?

4. Implémenter, en Python, de cette façon une structure File qu'on nommera `File_2_piles`, dont les primitives sont _enfiler_, _defiler_ et _est_vide_.

     

#### Exercice 

La structure de _liste_ de Python possède deux méthodes particulières :

* `liste.pop()` renvoie l'élément en queue de liste (celui de plus grand indice) et l'enlève de la liste.
* `liste.append(elt)`  ajoute _elt_ en queue de liste.



1. Implémenter une Pile en utilisant un tableau (type list) Python et les méthode `pop` et `append`. 
2. Implémenter une File en utilisant un tableau Python :
    *  On utilisera un seul attribue, tab, initialiser avec `self.tab = [1]`.
    * `self.tab[0]` correspondra toujous à l'indice de la tête de la file. S'il n'y a pas d'élément à cet indice, la file est vide.
    * On utilisera la méthode `append` des tableaux de Python pour enfiler.
    * On décalera la position de la tête de la file pour défiler. Si, après défilement, la file est vide, on réinitialise le tableau avec `self.tab = [1]` pour éviter qu'il ne devienne trop grand.



#### Exercice 

L'écriture polonaise inverse est une manière d'écrire des expressions mathématiques dans laquelle l'opérateur se trouve derrière les opérandes. 

* pour calculer  3 + 4, on écrit 3 4 + 

* 1 3 + 5 * revient à effectuer $`(1 + 3) \times 5`$

    

Pour simplifier, on suppose que :

*  Les seuls symboles opératoires sont ceux des 4 opérations de base. 

* Les opérandes sont des chiffres entiers. 

    



Notre objectif est d'implémenter une fonction `evaluer(expression)` qui renvoie le résultat d'une expression (chaîne de caractères) écrite en notation polonaise inverse. 



##### Exemples

Voici quelques expressions. Quel est, dans chaque cas, le résultat attendu ? 

* 1 3 + 6 +  
*  2 5 * 8 +
*  3 4 * 2 /
* 11 3 + 4 * 10 /



> Définissez la fonction `evaluer(expression)`, de paramètre une chaîne de caractères, dont le but sera de renvoyer le résultat de l'évaluation de l'expression passée en paramètre.
>
> On ne demande pas, pour l'instant, de coder cette fonction.
>
> En revanche, documentez la et donnez deux Doctests autres que ceux déjà proposés plus haut.







##### Première pile

Nous allons construire une première pile en lisant une expression de gauche à droite.

Par exemple, si l'expression est '1 3 + 6 +' la pile obtenue s'affichera ainsi, l'élément en bas de pile étant l'entier 1 et l'élément le plus haut étant l'opérateur '+' :

```txt
+
6
+
3
1
```



>  Définissez et codez la fonction `creer_pile(expression)` dont le paramètre est une chaîne de caractères qui renvoie une pile constituée des éléments rencontrés en lisant l'expression de gauche à droite. On convertira les caractères correspondant à des chiffres en _int_.





##### Deux autres piles

On va construire deux piles à partir de celle créée précédemment :

* la pile des opérations à effectuer.
* la pile des opérandes (les nombres entiers).



Par exemple, pour l'expression '3 4 + 5 *' :

* la pile construite via la fonction précédente sera :

    ```txt
    *
    5
    +
    4
    3
    ```

    

* la pile des opérations contient dans l'ordre + puis * :

    ```txt
    +
    *
    ```

    

* la pile des opérandes contient dans l'ordre 3, 4 puis 5 :

    ```txt
    3
    4
    5
    ```



Déterminez la pile des opérations et celle des opérandes dans les cas suivants :

* 1 3 + 6 +  
*  2 5 * 8 +
*  3 4 * 2 /
* 7 3 + 4 * 5 /



> Codez une fonction `extraire_piles(pile_polonaise)`, de paramètre une _Pile_ renvoyée par `creer_pile`, qui renvoie un tuple constitué de la pile d'opérations et de la pile des opérandes correspondantes.
>
>  
>
> Documentez la fonction, donnez deux Doctests.
>
> 





##### Evaluation



Reprenons notre fonction `evaluer(expression)` qu'il nous reste à coder.

Le principe est le suivant :

* On construit une première pile à partir de l'expression
* On extrait de cette pile la pile de chiffres et la pile d'opérations
* tant que la pile d'opération n'est pas vide :
    * on dépile la pile de chiffre vers une variable, par exemple, _nbre1_
    * on dépile la pile de chiffre vers une variable, par exemple, _nbre2_
    * on dépile la pile d'opération vers une variable, par exemple, _op_
    * on empile dans la pile de chiffres le résultat de l'opération _op_ sur les opérandes _nbre1_ et _nbre2_
* on renvoie le dernier élément de la pile de chiffres



>  
>
> Codez la fonction `evaluer(expression)`.
>
> 







#### Exercice : Parseurs

##### Seulement avec des parenthèses



On considère une chaîne de caractères comportant des parenthèses ouvrantes `(`et fermantes `)`. On se propose de concevoir une fonction qui décèle les anomalies d'écriture. 

Pour vérifier si une expression est correctement parenthésée, une méthode consistant à utiliser une pile est exposée ci-dessous:

- on commence par construire une pile de parenthèses ouvrantes vide.
- pour tout élément de la chaîne de caractère à tester :
    - si cet élément est une parenthèse ouvrante alors on l’empile dans notre pile.
    - si cet élément est une parenthèse fermante :
        - si la pile est vide, alors l’expression est incorrecte (trop de fermantes) et on renvoie faux.
        - sinon, on dépile notre pile de parenthèse(s) ouvrante(s).
- Quand la chaîne a été parcourue entièrement, :
    - la pile doit être vide. si c'est le cas, on renvoie vrai
    - si ce n'est pas le cas, l’expression est incorrecte (trop d'ouvrantes) et on renvoie faux.



> 
>
> Codez la fonction `est_correcte_parentheses(expression)` qui renvoie une erreur `True` si le parenthésage est correct et `False` sinon.
>
> Documentez la fonction et proposez 2 Doctests
>
> 



### Accolades, crochets et parenthèses

On se propose cette fois de réaliser une fonction `est_correcte` qui vérifie les parenthèses, crochets et accolades d'une expression mathématique. Le principe algorithmique est le même que précédemment sauf qu'il s'agit en plus de vérifier que le caractère dépilé correspond en version ouverte au caractère rencontré :

* si on rencontre une accolade ou une parenthèse ou un crochet ouvrant alors on l'empile dans la pile des _ouvrantes_.

* si on rencontre une accolade ou une parenthèse ou un crochet fermant :

    * on dépile la pile des ouvrantes dans une variable.

    * si le caractère ouvrant que l'on vient de dépiler ne correspond pas au fermant que l'on vient de rencontrer, alors on renvoie faux.

        



>  
>
>  Codez la fonction `est_correcte(expression)` qui renvoie une erreur `True` si le parenthésage complet est correct et `False` sinon. Documentez la fonction et proposez 2 Doctests.
>
>  On utilisera le dictionnaire ci-dessous :
>
>  ```python
>  dico = {'(' : ')', '[' : ']', '{' : '}'}
>  ```
>
>  





### En HTML



En HTML ,en se propose cette fois de vérifier le bon balisage du code.

Pour simplifier le problème, on ignorera les balises orphelines et on n'utilisera que quelques balises usuelles :

```txt
<html> </html>
<body></body>
<h1> </h1>
<p></p>
<table></table>
<tr></tr>
<td></td>
<ul> </ul>
<li> </li>
```

On utiliser la constante `CODE` ci-dessous pour nos tests :

```python
CODE = '''
<html>
	<body>
		<h1>
			code test 
		</h1>
		<p> juste quelques lignes de code html </p>
		<table>
			<tr>
				<td> une cellule </td>
				<td> une autre cellule </td>
			</tr>
		</table>
		<ul>
			<li> une puce </li>
			<li> une deuxième puce </li>
		</ul>
	</body>
</html>
'''
				
```

1. Codez une fonction `creer_file(code_html)` qui renvoie une file contenant les balises ouvrantes et fermantes du code html contenu dans la variable code_html (de type str) dans leur ordre d'apparition.
2. Codez une fonction  `est_correcte_html(code_html)` dont le paramètre est une chaîne de caractères contenant le code HTML à vérifier, qui renvoie `True` si le balisage est correcte et `False` sinon. A l'instar de ce qui a été fait pour l'exercice précédent, on utilisera un dictionnaire dont les clés sont les balises ouvrantes et les valeurs les balises fermantes correspondantes.



__________

Par Mieszczak Christophe à partir de documents de Luc Bournonville.

licence CC BY SA








## Exercices sur les arbres 

## La base

### Vocabulaire 



![arbre binaire](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Arbre_binaire_ordonne.svg/408px-Arbre_binaire_ordonne.svg.png)



* Quelle est l'étiquette de la racine ?

* Quelles sont les étiquettes des feuilles de l'arbre ?

* Quelle est l'étiquette de la racine du sous arbre gauche de la racine ?

* Quelle est l'aridité de cet arbre ?

* Quel est le parent du noeud d'étiquette 9 ?

* Quelle est la taille de cet arbre ?

* Quelle est la profondeur du noeud d'étiquette 5 ?

* Quelle est la hauteur de cet arbre ?

* Comment peut-on décrire cet arbre en mode texte ?

    



### Quelques arbres binaires

Dessinez chacun des arbres ci-dessous, donnez sa taille, sa hauteur, le nombre de feuilles, le nombre de nœuds à chaque profondeur.

1. (1, ∆, ∆)
2. (3, (1, ∆, (4, (1, ∆, (5, ∆, ∆)), ∆)), ∆)
3. (3, (1, (1, ∆, ∆), ∆), (4, (5, ∆, ∆), (9, ∆, ∆)))
4. (3, (1, (1, ∆, ∆), (5, ∆, ∆)), (4, (9, ∆, ∆), (2, ∆, ∆)))



### Dessiner des squelettes

On rappelle qu'un squelette d'arbre binaire est un arbre binaire dans lequel on ne tient pas compte des étiquettes.

* Combien y a-t-il de squelettes d'arbres binaires de taille 0, de taille 1 ? Dessinez les !

* Dessinez tous les squelettes d'arbres binaires de taille 2, 3, 4. Combien y-en-a-t-il ? Dessinez les !





### Dessiner et compter des feuilles

Dans chaque cas, faites un schéma avant de répondre.

- Combien de feuilles au minimum/maximum comporte un arbre binaire :

    - de hauteur 3? 
    - de hauteur _h_ ?

- Combien de nœuds au minimum/maximum comporte un arbre binaire :

    - de hauteur 3 ? 
    
- de hauteur _h_ ?
  
  ​    

###  Taille et hauteur : algorithmes récursifs

Proposez des algorithmes récursifs, en pseudo code, pour calculer :

- la taille d'un arbre binaire.
- la hauteur d'un arbre binaire. On conviendra conventionnellement que la hauteur de l'arbre vide ∆ est -1.



### Numéroter les nœuds

La [numérotation de Sosa-Stradonitz](https://fr.wikipedia.org/wiki/Num%C3%A9rotation_de_Sosa-Stradonitz) des nœuds d'un arbre binaire, utilisée notamment en généalogie, est la suivante :

- le nœud racine est numéroté par 1
- si un nœud numéroté par _i_
    - son fils gauche est numéroté par _2i_
    - son fils droit est numéroté par _2i + 1_

Cette numérotation peut être utilisée pour représenter un arbre dans un tableau : l'élément _j_ du tableau mémorise le nœud numéroté par _j_ .

Combien d'éléments doit contenir un tableau utilisé pour représenter

- un arbre binaire complet de _n_ nœuds ?
- un arbre binaire parfait de _n_ nœuds ?
- un arbre binaire quelconque de _n_ nœuds (dans le pire des cas) ?



## Parcourir un arbre

Pour chaque arbre ci-dessous, notez, dans l'ordre, les étiquettes que l'on lit lorsqu'on suit un parcours préfixe, puis infixe, puis postfixe, puis, pour terminer, en largeur d'abord.

### Un premier arbre

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/300px-Binary_search_tree.svg.png" alt="arbre no 1" style="zoom:50%;" />



### Un second arbre

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Unbalanced_binary_tree.svg/600px-Unbalanced_binary_tree.svg.png" alt="arbre équilibré" style="zoom: 33%;" />

### Un Troisième

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/AVLtreef.svg/800px-AVLtreef.svg.png" alt="arbre équilibré" style="zoom:50%;" />



### Un dernier arbre

<img src="media/tree-1.png" alt="avec des lettres" style="zoom:50%;" />







## Arbres binaires de recherche

### Insertion de clés



Considérons l' ABR ci-dessous. 



![arbre no 1](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/300px-Binary_search_tree.svg.png)



Insérez-y les clés 2, 5, 9, 11 et 15.



### Construction d'un ABR



A partir d'un ABR vide,  on souhaite construire un ABR le plus équilibré possible en insérant les clés ci-dessous. Dans chaque cas :

* Construisez un ABR en prenant la première valeur comme clé de la racine.

* Déterminer la clé la plus adaptée à être celle de la racine puis construisez l'arbre.

    

1. Les clés sont : 1-3-5-8-11-14-21-24
2. Les clés sont A-D-E-F-H-I-J-M-N-X. L'ordre est alphabétique.

**Exercice 3 : Arbre Binaire de Recherche**

Un arbre binaire de recherche est équilibré si et seulement si il a, pour sa taille, la hauteur la plus petite possible.

1. Quelle est la plus petite hauteur possible pour des arbre de taille 3, 7, 15 et ? Dessinez les.

2. a. Dessiner l'arbre binaire  de recherche **A1** obtenu en insérant dans cet ordre les clés : 2,1,4,3,5 et 6

    b. Dessiner l'arbre binaire  de recherche **A2** obtenu en insérant dans cet ordre les clés :  5, 6, 3, 1, 4, 2

3. Pour équilibrer un arbre tel que **A1**, on effectue une rotation à gauche de l'arbre dont voici le principe :

    <img src="media/rot_gauche.png" style="zoom: 50%;" />

		  Dessinez l'arbre **A1** après application de cette rotation.  

4. Pour équilibrer un arbre tel que **A2**, on effectue une rotation à droite de l'arbre dont voici le principe :

    <img src="media/rot_droite.png" style="zoom: 50%;" />

		 Dessinez l'arbre **A1** après application de cette rotation.  

5. Expliquer l'intérêt d'utiliser une arbre binaire de recherche équilibré.

6. Dans la classe ABR :
    * implémenter la méhode `rotation_droite(self)` ;
    * implémenter la méthode `rotation_gauche(self)` ;
    * dans la méthode `insérer`, dans le cas d'arrêt où l'arbre est vide, appeler l'une des méthodes précédentes si l'ABR est déséquilibré (pensez à la hauteur...)





Par Mieszczak Christophe

licence CC BY SA

sources images :

* [arbre 1- wikipedia - CC BY SA](https://commons.wikimedia.org/wiki/File:Arbre_binaire_ordonne.svg)

* [arbre 2 - wikipédia - domaine public](https://commons.wikimedia.org/wiki/File:Unbalanced_binary_tree.svg)

* [arbre 3 - wikipedia - domaine public](https://commons.wikimedia.org/wiki/File:AVLtreef.svg)

* [arbre4 - christian prieto CC BY SA](https://cprieto.com/about.html)

* [rotation - wikipedia - CC BY SA](https://commons.wikimedia.org/wiki/File:Tree_rotation_fr.svg)

  
  
    
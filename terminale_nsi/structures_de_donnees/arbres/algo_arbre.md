# Algorithmes sur les Arbres



Nous allons ici réaliser des algorithmes importants pour les arbres binaires.

Aussi allons nous compléter les méthodes de la classe `Arbre`.

Comme la classe `ABR` en hérite, ces méthodes seront également les siennes.



Pour nos doctests, on va utiliser le même arbre que pour notre implémentation :

![arbres de recherche](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/245px-Binary_search_tree.svg.png)





### Taille de l'Arbre



Le calcul de la `taille` d'un arbre (son nombre de noeud) est naturellement récursif :

* S'il est vide, on renvoie 0.
* sinon, on renvoie 1 + `taille`(sous_arbre_gauche)  +  `taille`(sous_arbre_droit))



> Définissez et codez la méthode `taille(self)`, qui renvoie la taille de self. 
>
> * Utiliser bien les méthodes `sag()` et `sad()` et non pas les attributs correspondants qui, eux, pourraient être `None` !
> * Documentez la méthode. Ajoutez y un Doctest au moins.



### Hauteur de l'Arbre



De la même façon, le calcul de la hauteur d'un arbre est naturellement récursif. 

Il existe deux conventions pour la hauteur d'un arbre vide. 

* On peut considérer que sa hauteur est -1 : ce sera notre point du vue.
* Il est parfois convenu que sa hauteur est 0. Si nous prenions ce postulat (ce n'est pas le cas), il suffirait d'ajouter 1 à notre résultat.





> * Trouvez la formule récursive permettant le calcul de la hauteur d'un arbre.
> * Définissez et codez la méthode `hauteur(self)` qui renvoie la hauteur de `self`. 
> * Utiliser bien les méthodes `sag()` et `sad()` et non pas les attributs correspondants qui, eux, pourraient être `None` !
> * N'oubliez ni Docstring ni Doctest.

-

-

-

-

-

-

**SPOILER PLUS BAS !!**

-

-

-

-



* si arbre est vide alors on renvoie -1
* sinon on renvoie 1 + max(hauteur(arbre gauche) , hauteur(arbre droit))



### `__repr__`



> Définissez et codez la méthode `__repr__(self)` qui renvoie une description de l'instance `self`  de la classe sous la forme :
>
> _"Arbre, de taille ... et de hauteur ..."_



### Parcours _préfixe_



Les parcours en préfixe, infixe et postfixe (ou suffixe)  sont naturellement récursifs. Nous avons déjà vu cela dans la partie [sur le parcours d'arbres](parcourir.md)  .



>  
>
> Donnez la formule récursive du parcours préfixe
>
> 



Pour vous aider, voici le début de la méthode :

```python
def prefixe(self):
        '''
        renvoie une liste des étiquettes selon le parcours préfixe
        : return (list)
        >>> 
        '''
                                                
```





> 
>
> Complétez la méthode `prefixe(self):` qui renvoie la liste des étiquettes obtenue en parcourant l'arbre en préfixe.
>
> Donnez la formule récursive avant de commencer à coder.
>
> Utiliser bien les méthodes `sag()` et `sad()` et non pas les attributs correspondants qui, eux, pourraient être `None` !
>
> 





### Parcours _Infixe_



>  
>
> Donnez la formule récursive du parcours infixe
>
> 







> 
>
> En vous inspirant de la méthode précédente, codez la méthode `infixe(self)` qui renverra la liste des étiquettes obtenue en parcourant l'arbre en infixe.
>
> * N'oubliez ni Docstring, ni Doctest.
> * Utiliser bien les méthodes `sag()` et `sad()` et non pas les attributs correspondants qui, eux, pourraient être `None` !
>
> 



### Parcours _postfixe_ (ou _suffixe_).



>  
>
> Donnez la formule récursive du parcours suffixe.
>
> 





> 
>
> En vous inspirant de la méthode précédente, codez la méthode `suffixe(self)` qui renverra la liste des étiquettes obtenue en parcourant l'arbre en postfixe.
>
> * N'oubliez ni Docstring, ni Doctest.
> * Utiliser bien les méthodes `sag()` et `sad()` et non pas les attributs correspondants qui, eux, pourraient être `None` !
>
> 





### Parcours en _largeur d'abord_

Le parcours en largeur est certainement le plus facile à comprendre, mais il est plus difficile à implémenter. 

Nous allons utiliser une structure de File. Nous aurons donc besoin de notre `module_lineaire`.



* On construit une File vide.
* On commence par placer l'arbre dans la File. 
* On crée une liste vide de clés.
* Tant que la file n'est pas vide :
    * on défile la file dans une variable `arbre`.
    * si cet arbre n'est pas vide :
        * on ajoute sa clé au bout de la liste de clés.
        * on enfile son arbre gauche dans la File.
        * on enfile son arbre droit dans la File
* Il ne reste plus qu'a retourner la liste de clés.

Prenons l'arbre que nous utilisons depuis le début de ce TP :  ( 8, **(3, (1, ∆,∆) ,(6, (4, ∆, ∆), (7, ∆, ∆))**, _(10, ∆, (14, (13, ∆, ∆), ∆))_ )  

* au début, cet arbre est dans la File.
* La File n'est pas vide, on la défile est on obtient cet arbre.
    * cet arbre n'est pas vide donc :
        * on ajoute sa clé, 8, à la liste de clés
        * on enfile l'arbre gauche (3, (1, ∆,∆) ,(6, (4, ∆, ∆), (7, ∆, ∆)) dans la file
        * on enfile l'arbre droit (10, ∆, (14, (13, ∆, ∆), ∆))
* La File contient  (10, ∆, (14, (13, ∆, ∆), ∆)) , (3, (1, ∆,∆) ,(6, (4, ∆, ∆), (7, ∆, ∆)) et  elle n'est pas vide :
    * on défile donc l'arbre suivant :  (3, (1, ∆,∆) ,(6, (4, ∆, ∆), (7, ∆, ∆))
    * cet arbre n'est pas vide donc :
        * on ajoute sa clé, 3, à la liste de clés
        * on enfile l'arbre gauche (1, ∆,∆)
        * on enfile l'arbre droit (6, (4, ∆, ∆), (7, ∆, ∆)
* La File contient  (1, ∆,∆), (6, (4, ∆, ∆), (7, ∆, ∆),  (10, ∆, (14, (13, ∆, ∆), ∆)) et n'est pas vide :
    * on défile l'arbre suivant : (10, ∆, (14, (13, ∆, ∆), ∆))
    * cet arbre n'est pas vide :
        * on ajoute sa clé, 10, à la liste de clés
        * On enfile son arbre gauche ∆
        * on enfile son arbre droit  (14, (13, ∆, ∆), ∆)
* la file contient ∆,  (14, (13, ∆, ∆), ∆), (1, ∆,∆), (6, (4, ∆, ∆), (7, ∆, ∆) et n'est pas vide :
    * A vous, ...






>  Complétez le code de la méthode `largeur(self)` ci-dessous :
>
>  ```python
>  	def largeur(self) :
>        '''
>        renvoie un tableau des étiquettes clés selon le parcours en largeur d'abord
>        : return (list)
>        >>> 
>        '''
>    
>    ```
>    
>    Utilisez bien les méthodes `sag()` et `sad()` et non pas les attributs correspondants qui, eux, pourraient être `None` !
>
>  







________

Par Mieszczak Christophe

licence CC BY SA

source image : [arbre binaire - wikipedia - domaine public](https://commons.wikimedia.org/wiki/File:Binary_search_tree.svg)


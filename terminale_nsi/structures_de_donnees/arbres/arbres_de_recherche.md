# Les Arbres Binaires de Recherche (ABR)

## Sékoiça ?



Un arbre binaire de recherche (ABR) est un arbre binaire ordonné de façon à ce que, pour tout noeud d'étiquette _e_ de l'arbre :

* Les étiquettes de tous les noeuds du sous sous-arbre gauche soient inférieures ou égales à _e_.
* Les étiquettes de tous les noeuds du sous sous-arbre droit soient supérieures ou égales à _e_.



Dans le cas d'un arbre de recherche, les étiquettes sont appelées `clés`. 



_Exemple :_

*  Vérifiez que cet arbre est bien un arbre de recherche.
* Représentez le en _mode texte_.

![arbres de recherche](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/245px-Binary_search_tree.svg.png)



Un tel arbre permet des opérations très rapides de recherche, insertion ou suppression (pas au programme de terminale) d'une clé.



_Remarque :_

Qui dit _relation d'ordre_ (plus petit ou plus grand) dit _comparateurs_. Nous avons vu  en première, dans le chapitre sur les [tris](../../../premiere_nsi/algorithmique/tris/tris_tp.md), qu'il existait de nombreuses façons de comparer des objets :

* ordre numérique pour les nombres.
* comparaison de chaînes de caractères selon l'ordre alphabétique.

* comparaison de chaînes de caractères selon leurs longueurs.
* comparaison de chaînes de caractères selon nombre de points au scrabble.
* etc...



Par soucis de simplicité, nous utiliserons dans ce cours les notations `<`, `>` ou `=` pour comparer les clés de nos ABR.



## Recherche d'une clé

Pour rechercher un clé dans un arbre binaire de recherche, on procède un peu de la même façon que pour la dichotomie, de façon récursive.



* Si l'arbre est vide alors la clé n'y est pas, on renvoie 'non présente'.
* Sinon :
    * si l'étiquette du noeud est la clé recherchée, on renvoie 'trouvé'
    * sinon :
        * si l'étiquette du noeud est inférieur à la clé recherchée alors la clé se situe à gauche : on relance donc récursivement la recherche dans le sous-arbre gauche.
        * sinon, on relance la recherche dans le sous-arbre droit.



>   
>
>  Ecrivez la formule récursive de la fonction `rechercher(cle, arbre)`
>
> 



> Reprenons l'ABR ci-dessous. Quel serait le _parcours_ de la recherche pour trouver 7 ? 14 ? 2 ?

![insertion](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/300px-Binary_search_tree.svg.png)

-

-

-

-

-

-

-

-

### Spoiler plus bas, réfléchissez avant de défiler !!

-

-

-

-

-

-

-

-

-

-

-

-



$`rechercher(cle, arbre) = \left\{ \begin{array}{ll}     {~si~arbre~est~vide~:~} renvoyer~"Non~présente" \\    {si~clé~du~noeud~==~cle~:~} renvoyer~"Trouvé" \\    {si~clé~<~clé~du~noeud~:~} renvoyer~rechercher(cle,sous-arbre-gauche) \\    {sinon~:~} renvoyer~rechercher(cle,sous-arbre-droit)  \end{array}\right.`$





Voilà les trajets pour les recherche de 7 (en rouge), 14 (en vert) et 2 (en bleu)

<img src="media/recherche_cle.jpg" alt="recherche des clés" style="zoom:50%;" />





## Insertion d'une clé



L'insertion d'une clé dans un arbre binaire de recherche permet également de construire l'arbre complet, d'où l'importance de cet algorithme.

On veut donc insérer une nouvelle clé (non déjà présente dans l'arbre) _cle_ dans un arbre binaire de recherche. Le principe est le même que celui de la recherche vu au dessus avec une différence : lorsqu'on trouve un arbre vide, au lieu de renvoyer "non présente", on insère un nouveau noeud avec la clé souhaitée.



$`inserer(cle, arbre) = \left\{ \begin{array}{ll}     {~si~arbre~est~vide~:~} inserer~une~feuille~de~clé~cle \\    {si~étiquette~du~noeud~==~cle~:~} renvoyer~"clé~déjà~présente" \\    {si~étiquette~du~noeud~<~cle~:~} inserer(cle,sous-arbre-gauche) \\    {sinon~:~} inserer(cle,sous-arbre-droit)  \end{array}\right.`$



En réalité, insérer la feuille n'est pas si simple... Nous en reparlerons lorsque nous implémenterons les arbres en Python.



_Exemple :_ Reprenons notre ABR. Insérez les valeurs 2 et 11.

![insertion](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/300px-Binary_search_tree.svg.png)



_Re-exemple :_

On souhaite, à partir d'un ABR vide, insérer les clés : 2, 7, 8, 1, 13, 5, 11 et 6.

* Tout d'abord, prenons 2 pour clé de la racine.
* Quelle est la conséquence pour l'arbre ?
* Comment choisir la clé de la racine pour que l'arbre soit aussi _équilibré_ que possible ?



## Complexité de ces deux algorithmes

### ABR équilibré

![arbre équilibré](https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/AVLtreef.svg/1920px-AVLtreef.svg.png)

Lorsque l'ABR est équilibré, le nombre d'étiquettes est (environ)  divisé par deux à chaque fois qu'on choisit de poursuivre la recherche dans un des deux sous-arbres.

Ainsi, si l'ABR a une taille $`n = 2^p`$ , le nombre maximum de comparaisons sera $`p  = log_2(n)`$.

**La complexité est donc logarithmique (ou o(log(n)))** ce qui les rend très très performants.

| taille de l'ABR | nombre de comparaisons au maximum |
| :-------------: | :-------------------------------: |
|       64        |                 6                 |
|      1024       |                10                 |
|      4096       |                12                 |



### Pire des cas

Dans le pire des cas l'ABR est filiforme : chaque noeud a un seul fils.

![filiforme](media/abr_filiforme.png)

Dans ce cas, le nbre de comparaison est égal à la taille de l'ABR :

* avec une taille 10, il faudra au maximum 10 comparaisons pour trouver la bonne clé.
* avec une taille n, il faudra au maximum n comparaisons pour trouver la bonne clé

Dans ce cas **la compléxité est linéaire, c'est à dire o(n)**.

Elle est bien moins intéressante que lorsque l'ABR est équilibré d'où l'importance cruciale de pouvoir équilibrer les ABR. Nous verrons cela en exercice.

| taille de l'ABR | nombre de comparaisons au maximum si ABR équilibré | nombre de comparaisons au maximum si ABR filiforme |
| :-------------: | :------------------------------------------------: | :------------------------------------------------: |
|       64        |                         6                          |                         64                         |
|      1024       |                         10                         |                        1024                        |
|      4096       |                         12                         |                        4096                        |



## Pour finir

Il nous reste maintenant [à implémenter tout ça](implementation.md) !



_____________

Par Mieszczak Christophe Licence CC BY SA

sources images :

* [arbre binaire - wikipedia - domaine public](https://commons.wikimedia.org/wiki/File:Binary_search_tree.svg)

* [ABR équilibré - wikipedia - domaine public](https://commons.wikimedia.org/wiki/File:AVLtreef.svg)

* ABR filiforme - production personnelle

    
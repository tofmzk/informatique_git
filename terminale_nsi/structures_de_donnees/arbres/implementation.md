## Implémenter d'arbres binaires

## La classe Arbre



Nous allons ici définir une classe pour un arbre binaire. Créez le fichier `module_arbre.py`.

### Initialisation de la classe

Un arbre binaire est définit par la clé de sa racine, son sous-arbre gauche et son sous-arbre droit.

Nous l'initialisons ainsi :

```python
class Arbre():
    '''
    une classe pour implémenter des arbres de façon récursive
    '''
    def __init__(self, etq = None, sag = None, sad = None):
        '''
        construit un arbre vide avec un sous_arbre gauche et un sous arbre droit vides
        par défaut
        : params 
        	etq (?) l'étiquette du noeud
        	sag (Arbre) le sous arbre gauche
        	sad (Arbre) le sous-arbre droit
        '''
        self.etiquette = etq
        self.sous_arbre_gauche = sag
        self.sous_arbre_droit = sad
```

> Construisez l'arbre ci-dessous en commençant par les feuilles 
>
> ![arbres de recherche](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/245px-Binary_search_tree.svg.png)

Pour vous aider, voici un début : 

```python
>>> a1 = Arbre(4)
>>> a7 = Arbre(7)
>>> a6 = Arbre(6, a4, a7)

```



Copier ces lignes tout en bas du code de _classe_arbre.py_, elles nous serviront pour les tests

```python
#####################TESTS
if __name__ == '__main__' :
	a1 = Arbre(4)
	a7 = Arbre(7)
	a6 = Arbre(6, a4, a7)   
    etc...
    import doctest
    doctest.testmod(verbose = True)
```



### Prédicat



> Définissez est codez le prédicat `est_vide(self)` qui renvoie `True` si l'Arbre est vide et `False` sinon
>
> N'oubliez pas la Docstring et un Doctest



### Représentation

Nous utiliserons  	la représentation ci-dessous :

```python
    def __repr__(self):
       '''
       renvoie une chaîne de caractère représentant l'arbre
       : return (str)
       '''
       if self.est_vide() :
            chaine = "Instance d'Arbre vide"
        else :
            chaine = "Instance d'Arbre d'étiquette ' + str(self.etiquette)
```

Cette représentation va nous permettre de faire des tests pour les méthodes suivantes

### Accesseurs

> Complétez les accesseurs ci-dessous :
>
> 
>
> ```python
> 	def sag(self):
> 		'''
> 		renvoie le fils gauche de l'arbre. Si ce fils est None, renvoie un Abre vide
> 		: return (Arbre)
> 		>>> a = Arbre()
> 		>>> a.sag()
> 		???
> 		>>> a8.sag()
> 		???
> 		'''
> 
> 	def sad(self):
>   		'''
>   		renvoie le fils droit de l'arbre Si ce fils est None, renvoie un Abre vide
>   		: return (Arbre)
>   		>>> a = Arbre()
> 		>>> a.sad()
> 		???
> 		>>> a8.sad()
> 		???
> 	  	'''
> 	def etq(self):
>     	'''
>      	renvoie l'étiquette de l'arbre
>      	: return (??)
>      	>>> a = Arbre()
>      	>>> a.etq()
>      	???
>      	>>> a8.etq()
>      	???
>      	'''
> ```
>
> 





### Affichage



Nous allons ici coder la méthode `__str__` pour pouvoir afficher un Arbre.

Nous allons pour cela renvoyer une chaîne de caractères où un arbre est représenté par un triplet (étiquette,  sag, sad) où :

* `étiquette` est la valeur du noeud de la racine.
* `sag` est le sous-arbre gauche.
* `sad` est le sous-arbre droit.

Ainsi, l'arbre dessiné ci-dessous, que nous utiliserons tout au long du TP, est représenté par la chaîne de caractères :

 ( 8, (3, (1, ∆,∆), (6, (4, ∆, ∆), (7, ∆, ∆))), (10, ∆, (14, (13, ∆, ∆), ∆)) )  

![arbres de recherche](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/245px-Binary_search_tree.svg.png)



L'affichage est naturellement récursive :

$`affichage(arbre) = \left\{ \begin {array} {ll} {~si~arbre~est~vide~:~} renvoyer~'∆'  \\  {sinon~:~} renvoyer~:'\\ (' + str(etiquette)~+~', '~+\\ ~affichage(sous-arbre-gauche)~+~', '~+\\~affichage(sous-arbre-droit)~+~')'  \end{array}\right.`$



> Codez la méthode `__str__(self)` en utilisant la définition récursive ci-dessus. Utiliser bien les méthodes `sag()` et `sad()` et non pas les attributs correspondants qui, eux, pourraient être `None` et donc ne pas être du type `Arbre`.
>
> ```python
> def __str__(self):
>     '''
>     renvoie une chaîne de caractère représentant l'arbre
>     : return (str)
>     >>> a = Arbre()
>     >>> print(a)
>     ∆
>     >>> print(a3)
>      ( 8, (3, (1, ∆,∆) ,(6, (4, ∆, ∆), (7, ∆, ∆)), (10, ∆, (14, (13, ∆, ∆), ∆)))  
>     '''
> ```
>






## La classe ABR



Nous allons définir et construire la classe ABR pour représenter un arbre binaire de recherche.



### Constructeur

Un ABR est ... un Arbre ... particulier. 

Au départ, il est toujours vide. Par la suite, on va insérer des valeurs dans l'ABR ou vérifier qu'une valeur est déjà présente dans l'ABR en utilisant les algorithmes particuliers des ABR.



Nous allons profiter d'une caractéristique de la programmation orientée objet : **l'héritage**. Certes,c'est hors programme. Mais c'est bien pratique.



Nous implémentons cette classe ainsi :

```python
class ABR(Arbre) :
    '''
    une classe pour implémenter des arbres binaire 
    '''
    def __init__(self):
        '''
        construit un arbre vide avec un sous_arbre gauche et un sous arbre droit vides
        '''
        # on fait appelle au constructeur de la classe parente, la classe Arbre pour construire un ABR vide :
        super().__init__() 
```



La classe ABR hérite de la classe `Arbre` : il en possède par défaut les attributs et les méthodes. Toutes les méthodes de la classe `Arbre` , la **classe mère** ou **super classe**, s'appliqueront donc de la même façon sur la classe `ABR`, la **classe fille** .

Il est donc inutile de coder à nouveau :

* `est_vide`
* `__str__`
* `__repr__`
* `etq` et `mut_etq`
* les parcours `prefixe`, `infixe`, `postfixe` et `largeur`.



Si nous définissons à nouveau une méthode commune aux deux classes, alors cette méthode s'appliquera différemment pour une instance de la classe `ABR` et de la classe `Arbre` . 

  

>  Codez les méthodes  `sag` et `sad` de la classe ABR de façon à ce qu'elles renvoient toujours un ABR.

 

### Insérer une clé dans un ABR

Nous avons vu, dans la partie sur les [ABR](arbres_de_recherche.md) comment insérer une clé (pour les ABR, on utilise le terme `clé` plutôt que `étiquette`). Nous allons coder la méthode correspondante.

Notez que l'insertion dans un ABR nécessite une relation d'ordre et donc un comparateur. Comme nous l'avons fait pour les tris en première, nous allons utiliser un comparateur par défaut qui pourra, si besoin, être remplacé par un autre selon le type des clés de l'ABR.

```python
    def comp_defaut(a, b):
        '''
        renvoie -1 si a < b, 1 si a > b et 0 si a == b.
        : param a et b
        type comparables avec l'opérateur < ou >
        : return 0, -1 ou 1
        type int
        '''
        if a < b :
            return -1
        elif a > b :
            return 1
        else :
            return 0
```

Le principe est le suivant :

* si l'arbre ( `self` donc) est vide :
    * On construit un arbre _gauche_ et un arbre _droit_ vides.
    * la clé de notre arbre (qui était vide) devient la clé passée en paramètre et ses sous-arbres gauche et droit sont les sous-arbres vides que vous venez de construire au dessus.
* sinon si la clé à insérer est égale à celle de l'arbre (de `self` donc), on déclenche une Exception..
* sinon : 
    * si la clé de l'arbre (de `self` donc) est inférieure à la clé passée en paramètre, on insère la clé dans le sous-arbres droit.
    * sinon on insère la clé dans le sous-arbre gauche.

>  
>
>  Complétez le code de la méthode `inserer(self, cle)`.
>
>  ```python
>    def inserer(self, cle, comp = comp_defaut):
>        '''
>        insérer une clé sa place dans l'ABR si celle-ci n'y est pas déjà
>        : param 
>           cles (? ou list de clé)
>           comp (function)le comparateur utilisé
>        : pas de return mais EFFET DE BORD SUR self
>        >>> a = ABR()
>        >>> a.inserer(8)
>        >>> print(a)
>        (8, ∆, ∆)
>        >>> a.inserer(3)
>        >>> a.inserer(1)
>        >>> a.inserer(6)
>        >>> a.inserer(4)
>        >>> a.inserer(7)
>        >>> a.inserer(10)
>        >>> a.inserer(14)
>        >>> a.inserer(13)
>        >>> print(a)
>        (8, (3, (1, ∆, ∆), (6, (4, ∆, ∆), (7, ∆, ∆))), (10, ∆, (14, (13, ∆, ∆), ∆)))
>        '''
>  ```
>



### Rechercher une clé dans un ABR

Il s'agit de coder un prédicat qui décide si une clé est présente dans l'arbre ou pas. 

Allons-y !

```python
    def rechercher(self, cle, comp = comp_defaut) :
        '''
        Renvoie True si la clé est presente et False sinon
        : param cle (?)
        : return (boolean)
        >>> a = ABR()
        >>> a.inserer(6)
        >>> a.inserer(13)
        >>> a.inserer(3)
        >>> a.rechercher(6)
        True
        >>> a.rechercher(13)
        True
        >>> a.rechercher(3)
        True
        >>> a.rechercher(9)
        False
        '''
```





_________

Par Mieszczak Christophe

licence CC BY SA

source image : [arbre binaire - wikipedia - domaine public](https://commons.wikimedia.org/wiki/File:Binary_search_tree.svg)


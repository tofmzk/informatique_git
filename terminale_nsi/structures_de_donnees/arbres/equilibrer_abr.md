# Équilibrer un ABR

Lorsqu'un ABR est équilibré, la compléxité de la recherche et de l'insertion dans cet ABR est logarithmique et donc ces algorimthes sont très performants.

Au contraire lorsque l'abrbre est déséquilibré au maximum (filiforme), ces mêmes algorithmes deviennent linéaires et donc bien moins performants.

Pouvoir équilibrer un ABR est donc crucial.



### Qu'est-ce qu'un ABR équilibré ?



Un ABR est dit équilibré si, pour chacun des ses noeuds, la différence de hauteur entre leur sous-arbre gauche et leur sous-arbre droit est inférieure ou égale à 1.

> Codez les prédicat `est_equilibre(self)` qui renvoie True si l'arbre est équilibré et False sinon.
>
> Prévoyez des tests.














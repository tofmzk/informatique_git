# Parcourir un arbre



Il existe plusieurs façon de parcourir un arbre. On visite toujours dans l'ordre le fils gauche puis le fils droit mais on peut traiter l'étiquette du noeud avant, entre ou après ses fils.

## Parcours préfixe



Lors d'un parcours `préfixe` :

* On traite l'étiquette du noeud en premier.
* On traire ensuite son fils gauche.
* On termine par son fils droit.

_Exemple :_ Sur cet arbre, les étiquettes lues, en parcours préfixe, sont, dans l'ordre 8, 3, 1, 6, 4, 7, 10, 14 puis 13.

<img src="media/prefixe.jpg" alt="parcours préfixe" style="zoom: 67%;" />







## Parcours infixe.



Lors d'un parcours `infixe` :

* On visite d'abord le fils gauche .
* On traite en second l'étiquette du noeud.
* On termine en visitant le fils droit.



_Exemple :_ Sur cet arbre, les étiquettes lues, en parcours infixe, sont, dans l'ordre 1, 3, 4, 6, 7, 8, 10, 13 puis 14

![arbres de recherche](media/infixe.jpg)

_Remarque :_ lors d'un parcours infixe d'un arbre binaire **de recherche**, la liste des étiquettes est toujours dans l'ordre croissant.



## Parcours postfixe (ou suffixe)

Lors d'un parcours `postfixe`  aussi appelé `suffixe`:

* On visite d'abord le fils gauche d'un noeud.
* On traite en second le fils droit du noeud.
* On termine en visitant  l'étiquette du noeud.



_Exemple :_  Sur cet arbre, les étiquettes lues, en parcours postfixe, sont, dans l'ordre 1, 4, 7, 6, 3, 13, 14, 10 puis 8.

![parcours postfixe](media/postfixe.jpg)



## Parcours _en largeur d'abord_

Lors d'un parcours `en largeur d'abord`, on va visiter, de gauche à droite, tous les noeuds de chaque niveau de l'arbre, en commençant par la racine et en descendant vers les feuilles de l'arbre (en bas).



Exemple :_  Sur cet arbre, les étiquettes lues, en parcours largeur d'abord, sont, dans l'ordre 8, 3, 10, 1, 6, 14, 4, 7 puis 13?	

![en largeur d'abord](media/largeur.jpg)









[Rendez-vous dans les exercices](exercices.md)

________

Par Mieszczak Christophe

licence CC BY SA

Source image : 

* [arbre binaire - wikipedia - domaine public](https://commons.wikimedia.org/wiki/File:Binary_search_tree.svg)


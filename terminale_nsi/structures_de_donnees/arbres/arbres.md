# Qu'est-ce qu'un arbre ?

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Arbre_mort_06.jpg/1200px-Arbre_mort_06.jpg" alt="un abre" style="zoom: 25%;" />





## Les arbres en informatique

Les *arbres* sont des structures de données hiérarchiques, non-linéaires (contrairement aux tableaux indexés, listes chaînées, piles, et files) et  naturellement récursives, utilisées pour représenter des ensembles de données structurées hiérarchiquement telles que :

* les arborescences des systèmes de fichiers :

    ![système de fichiers](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/UnixFS1.svg/1920px-UnixFS1.svg.png)

* les DOM en HTML :





<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/DOM-model.svg/800px-DOM-model.svg.png" alt="DOM" style="zoom:50%;" />

* la modélisation d'univers en probabilité :

![arbres de probabilité](https://upload.wikimedia.org/wikipedia/commons/e/e4/Arbre_proba.png)



* les arbres généalogiques dont on va emprunter le vocabulaire :

    ![arbre généalogique](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Carl_Gustav_Bielke_antavla_001.jpg/443px-Carl_Gustav_Bielke_antavla_001.jpg)

    





## Le vocabulaire des arbres.

### Terminologie

- Un arbre est nue structure de donnée hierarchique dont chaque élément est un `nœud`.
    
- un `nœud` est caractérisé par :
    
    - une `donnée` ou`étiquette` ou`élément` ou, dans le cas d'un [arbre binaire de recherche](arbres_de_recherche.md), d'une `clé`
    - un nombre fini de `fils`, qui sont eux-même des arbres. 
    
- Un `arbre vide` ne contient aucun `nœud`. 

- une `arête` relie deux nœuds (On parle aussi de `lien`).

-  Chaque nœud, à l'exception d'un seul, est relié à son `père` (ou `parent`) par exactement une arête entrante. Le seul nœud qui n'a pas de parent est la `racine`.

- Chaque `nœud` peut avoir une ou plusieurs `arêtes` sortantes le reliant à ses `fils`. 

- une `feuille` est un `nœud` sans `fils`.

- un `nœud interne` est un `nœud` qui n'est ni la `racine` ni une `feuille`.

- un `sous-arbre` d'un `nœud` est l'ensemble constitué des `nœuds` descendant de ce `nœud` et de ses  `arêtes`.

  ​    



_Exemple :_

![arbres de recherche](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/245px-Binary_search_tree.svg.png)

###  

* La racine a pour étiquette : 
* Cet arbre possède .... noeuds.
* Le nœud d'étiquette 6 a ... fils. Le nœud d'étiquette 14 a ... fils.
* Le nœud d'étiquette 13 a ... arête entrante.
* Le nœud d'étiquette 6 a ... arêtes sortantes
* Les feuilles ont pour étiquettes :
* Les noeuds internes ont pour étiquettes :



### Mesures sur les arbres

- la `taille` d'un arbre est le nombre de nœuds de l'arbre.

- Un `chemin` est une liste de noeuds reliés par des arêtes. 

- Une `branche` est le chemin le plus court reliant un nœud à la racine.

- la `profondeur` d'un nœud est le nombre d'arêtes sur la branche qui le relie à la racine. La profondeur de la racine est nulle.

- la `hauteur` d'un arbre est la profondeur maximale de l'ensemble des nœuds de l'arbre. On conviendra conventionnellement que la hauteur de l'arbre vide est -1.

- l'`aridité d'un nœud` est le nombre de fils du nœud.

- l'`aridité d'un arbre` est le nombre maximal de fils des nœuds de l'arbre.

- On appelle `squelette` ou `forme d'arbres binaires` tout arbre binaire dans lequel on ne tient pas compte des étiquettes.

    



_Reprenons notre exemple :_

![arbres de recherche](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/245px-Binary_search_tree.svg.png)



* La taille de cet arbre est ...

* La profondeur de nœud d'étiquette 14 est ...

* la hauteur de cet arbre est ...

* L'aridité de 14 est ...

* L'aridité de l'arbre est ...

* Donner un chemin du nœud d'étiquette 3 au nœud d'étiquette 7 : ...

* Donner la branche qui aboutit au nœud d'étiquette 13 : ...

  ​    



## Les arbres binaires

###  Définitions

Un *arbre binaire* est donc un arbre d'aridité deux : chaque nœud a donc 0, 1 ou 2 fils.

Un arbre binaire est :

- soit l'arbre vide, noté ∆ (_U+ 0394_).

- soit un triplet (e, g, d), appelée nœud, dans lequel :

    - e est la valeur (étiquette), de la racine de l'arbre.
    - g est le sous-arbre gauche de l'arbre qu'on appelle `fils gauche`.
    - d est le sous-arbre droit de l'arbre qu'on appelle `fils droit`.

_Remarque :_

Les sous-arbres (le gauche et le droit) d’un arbre binaire non vide sont eux-mêmes des arbres binaires. La structure d’arbre binaire est donc une structure récursive.



_Ci-dessous_ : l'arbre de gauche n'est pas un arbre binaire, celui de droite en est un.

![arbres](https://upload.wikimedia.org/wikipedia/commons/0/02/Nary_to_binary_tree_conversion.png?20070130104308)



_Reprenons notre exemple :_



![arbres de recherche](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Binary_search_tree.svg/245px-Binary_search_tree.svg.png)

* la racine a pour étiquette ...
* la racine a pour sous-arbre gauche est arbre dont la racine a pour étiquette ...
* la racine a pour sous-arbre droit est arbre dont la racine a pour étiquette ...
* On écrit cet arbre de la façon suivante: ( 8, **(3, (1, ∆,∆) ,(6, (4, ∆, ∆), (7, ∆, ∆))**, _(10, ∆, (14, (13, ∆, ∆), ∆))_ )  



### Forme d'arbres binaires

Les arbres binaires sont caractérisés par le fait que chaque nœud possède au plus deux fils. D'autres caractéristiques sont définies, qui permettent par exemple d'identifier des arbres pour lesquels le coût de certaines opérations sera minimal, ou de définir des algorithmes spécifiques à ces arbres.

- Un arbre binaire `filiforme` ou `dégénéré` est un arbre dans lequel tous les *nœuds internes* n'ont qu'un seul *fils*. Un arbre filiforme ne possède donc qu'une unique feuille.

![arbre filiforme](media/filiforme.jpg)

- Un arbre binaire `localement complet` ou arbre binaire *strict* est un arbre dont tous les *nœuds internes* possèdent exactement deux *fils*.
     Autrement dit, un arbre binaire localement complet est un arbre dont chaque nœud possède zéro ou 2 fils. L'arbre vide n'est pas localement complet.

    ![arbre localement complet](media/localement_complet.jpg)

- Un arbre binaire `complet` est un arbre binaire *localement complet* dans lequel toutes les *feuilles* sont à la même *profondeur*. 
    Il s'agit d'un arbre dont tous les niveaux sont remplis.

    ![arbre complet](media/complet.jpg)

    

- Un arbre binaire `parfait` est un arbre dans lequel tous les niveaux sont remplis à l'exception *éventuelle* du dernier, et dans ce cas les feuilles du dernier niveau sont alignées à gauche.

    ![arbre parfait](media/parfait.jpg)

- Un arbre binaire `équilibré` est un arbre dont la différence de profondeur entre les feuilles doit,au maximum, être de 1. Ce type d'arbre permet un accès moyen optimisé à tous les noeuds de l'arbre.
  
    ![arbre équilibré](media/equilibre.jpg)



_____________

Par Mieszczak Christophe Licence CC BY SA

A partir des documents de la formation DIU de l'université de Lille.

Source images :

* [Arbre mort - wikipedia CC - BY - SA](https://fr.wikipedia.org/wiki/Fichier:Arbre_mort_06.jpg)

* [Arbre et système de fichiers - wikipédia - domaine public](https://commons.wikimedia.org/wiki/File:UnixFS1.svg)

* [DOM - wikipédia - CC BY SA](https://commons.wikimedia.org/wiki/File:DOM-model.svg)

* [arbre de proba - wikipédia - CC BY SA](https://commons.wikimedia.org/wiki/File:Arbre_proba.png)

* [arbres binaires ou pas ? - wikipedia ](https://commons.wikimedia.org/wiki/File:Nary_to_binary_tree_conversion.png)

* Formes d'arbres : production personnelle.

    

  ​    
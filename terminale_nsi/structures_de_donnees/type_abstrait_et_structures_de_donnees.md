# Types abstraits et structures de données

L'informatique est la science du traitement automatique des données : la façon dont ces données sont organisées est fondamentale. 

  

En fonction des données sur lesquelles il travaille et des problèmes qu'il doit résoudre, un informaticien peut dresser un cahier des charges décrivant ces données et les opérations qu'ils souhaitent effectuer sur elles. Il s'agit d'une _vue de l'esprit_ qui existe uniquement _dans la tête_ de l'informaticiens. On parle alors d'un `type abstrait de données` . 

  

Il faudra `implémenter`  ce `type abstrait` en construisant, par exemple, une classe adéquate avec ses attributs et ses méthodes , pour obtenir une `structure de données` utilisable concrètement. Il est possible d'implémenter de différentes façons un même `type abstrait` afin d'optimiser certaines méthodes, souvent aux dépens d'autres.



Il existe de nombreuses structures de données différentes. Choisir la plus adaptée permettra de traiter les données de manières plus simple, efficace et rapide. 

* Une `structure de données linéaire`  regroupe des données de manière séquentielle : elles se suivent et peuvent être parcourue les unes après les autres dans un ordre précis. 

* Dans une `structures de données non linéaires`, les données ne se suivent pas toutes de manière séquentielle mais leur organisation optimise certaines algorithmes que nous étudierons cette année. C'est le cas des arbres et des graphes.

    

___

Par Mieszczak Christophe 

Licence CC BY SA




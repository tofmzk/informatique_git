# Parcourir un Graphe en largeur

L'objectif d'un parcours de graphe est de lister tous les sommets présents dans le graphe.



## Principe



L'**algorithme de parcours en largeur** (ou BFS, pour *Breadth First Search* en anglais) permet le parcours d'un graphe de la manière suivante : à partir d'un sommet, on atteint d'abord ses voisins puis les voisins de ses voisins et ainsi de suite jusqu'à visiter tous les sommets. On l'appelle également parcours concentrique et il sert notamment à la recherche des [plus courts chemins](recherche_chemins.md) dans un graphe.



Notez qu'il n'y a pas une unique façon de parcourir un graphe en largeur, l'ordre de parcours des successeurs (ou voisins) étant arbitraire. 



## Exemples

**Exemple 1 : Reprenons notre graphe d'amitiés non orienté.**

![](media/graph_amitie.jpg)

Donner deux parcours en largeur en commençant par le sommet `Mick`.



**Exemple 2 : avec un graphe orienté**

Avec un graphe orienté, le principe est exactement le même. On remplace simplement les voisins par les successeurs.

![](media/graphe_oriente.jpg)

Donner un parcours en largeur en commençant par le sommet `Mick`.



## Implémentation

Créez un fichier _parcourir_graphe.py_. 

Nous avons deux parcours à coder :
* celui des graphes orientés qui partagent des méthodes de même nom;
* celuis des graphes non orientés qui font de même.

Nous utiliserons les graphes d'amitiés construits dans les cours précédents pour les tester.

#### Parcourir un graphe non orienté

Nous allons coder la fonction `parcourir_largeur_non_oriente(graphe, sommet)` qui renvoie un tableau de sommets obtenus par parcours en largeur du `graphe` non orienté à partir du `sommet` de départ.

Il nous faudra utiliser une file de notre module `module_lineaire`.

Le principe est le suivant :

* on crée une liste `liste_sommets_parcourus` vide.
* on crée une instance de `File`.
* on enfile le `sommet` dans la file.
* tant que la file n'est pas vide :
    * on défile la file dans la variable `sommet`.
    * si le `sommet` n'est pas déjà dans la liste des sommets parcourus :
        * on ajoute ce `sommet` à la liste des sommets  parcourus.
        * on récupère la liste des voisins de ce `sommet` dans le `graphe`.
        * on enfile chaque voisin du`sommet` dans la file.
* Pour finir on renvoie la liste des sommets parcourus.



> Pour voir si vous avez bien compris :
>
> * Pourquoi utiliser une file plutôt qu'une pile ?
> * Qu'est-ce qui garantit qu'un sommet ne sera pas visité deux fois ?
>
>  
>
> Complétez la fonction ci-dessous (n'oubliez pas d'importer `module_lineaire` ):
>
> ```python
> def parcourir_largeur_non_oriente(graphe, sommet) :
>     '''
>     renvoie la liste des sommets en parcourant le graphe en largeur en partant de sommet_depart
>     : param graphe (Graphe..) un graphe non orienté 
>     : param sommet (?) le sommet de départ
>     : return (list) liste de sommets
>     '''
>     liste_sommets_parcourus = []
>     file_voisins = module_lineaire.File()
>     file_voisins.enfile(sommet)
>     while not file_voisins.est_vide() :
> ```
> 
>
> 
>Pour terminer, on teste le parcours en largeur sur notre graphe d'amitié non orienté.
> 

_Remarque :_

Il n'existe pas obligatoirement un unique parcours en largeur. Or, la façon dont nous avons implémenter ce parcours donnera toujours le même résultat. On pourrait ajouter une part d'aléatoire dans le parcours en utilisant la méthode `shuffle`  du module `random` qui mélange les élément d'une liste.

Il suffirait de mélanger le tableau des voisins d'un sommet avant de les enfiler. 

#### Parcourir en largeur un graphe orienté

C'est presque la même chose ... mais avec des noeuds, et des successeurs.

> Codez la fonction `parcourir_largeur_graphe_oriente(graphe, noeud)` en vous inspirant de la fonction précédente.







___________

Par Mieszczak Christophe

Licence CC BY SA

source images : production personnelle.

source vidéo : [wikipédia, licence CC BY SA, auteur purpy purple](https://fr.wikipedia.org/wiki/Fichier:MAZE_30x20_DFS.ogv)
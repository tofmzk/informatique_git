# Implémentation d'un Graphe et dictionnaire d'adjacence

## Dictionnaire d'adjacence.

Pour travailler sur les graphes, on peut utiliser leurs _matrices adjacentes_. Cependant cette représentation a des problèmes d'efficacité : 

* Même si les _n_ sommets du graphes n'ont que quelques arêtes entre eux, la matrice utilisera une place en mémoire proportionnelle à $`n \times n`$.
* Pour rechercher les voisins d'un sommet, il est obligatoire de parcourir toute une ligne de la matrice, donc _n_ voisins potentiels, même si ce sommet n'a qu'un seul voisin.
* il est également nécessaire d'ajouter un attribut aux classes construites de cette façon pour donner un _nom_ aux sommets autre qu'un nombre entier entre 0 et _n_ (exclus).



Pour remédier à ce problème, on peut définir le graphe en le décrivant comme ses sommets et l'ensemble des voisins (ou successeurs s'il est orienté) de chacun d'entre eux.



### Graphes orientés



Reprenons, encore, notre graphe d'amis aux amitiés non symétriques :

![amitiés non symétriques](media/graphe_oriente.jpg)

On peut décrire ce graphe ainsi :

* Ray a pour successeur Mick.
* Mick a pour successeurs Ray et Jim.
* Jim a pour successeur Joe.
* Joe a pour successeurs Mick et Etta.
* Etta n'a pas de successeur.
* Ella a pour successeurs Ray et Etta.



Grâce à ces listes de successeurs,  on peut implémenter notre graphe en Python en évitant les problèmes liées aux matrices adjacentes.



_Remarque :_

On peut également regarder les prédécesseurs de chaque sommet. La liste des prédécesseurs de chaque sommet définit elle aussi le graphe mais est moins intuitive.

>   
>
> Donnez la liste des prédécesseurs de chacun des noeuds du graphe.  
>
>   

### Graphes non orientés



Le principe est le même mais on remplace les successeurs par les voisins pour définir le graphe.

![les amis](media/graph_amitie.jpg)

>   
>
> Décrivez ce graphe en donnant la liste des voisins de chaque sommet.
>
>   



## Implémentation en Python



### Graphe orienté

Commençons, comme pour l'implémentation avec une matrice, par les graphes orientés.

#### Initialisons

Pour construire notre graphe, nous n'avons besoins que d'un dictionnaire vide !

```python
class Graph_oriente_dic() :
    '''
    une classe pour les graphes orientés construits avec un dictionnaire d'adjacence
    '''
    def __init__(self):
        '''
        construit le graphe avec pour seul attribut un dictionnaire vide
        '''
        self.adj = {}
```



#### Ajouter un noeud et un arc

Il nous faut deux méthodes :

* une méthode `ajouter_noeud(self, noeud)` qui ajoute la clé _noeud_ au dictionnaire d'adjacence, si ce noeud n'y est pas déjà et ne fait rien sinon.
* une méthode `ajouter_arc(self, noeud1, noeud2)` qui crée les deux noeuds différents via la méthode précédente puis ajoute `noeud2` au tableau des successeurs de `noeud1` dans le dictionnaire d'adjacence, s'il n'y est pas déjà.



Nous n'utiliseront la méthode `ajouter_noeud` que pour ajouter un sommet sans arc, puisque dans tous les autres cas, créer l'arc créera aussi les noeuds.



>     
>
> Codez et documentez ces deux méthodes, n'oubliez pas les assertions.
>
>    



#### Prédicat



>  Complétez le prédicat ci-dessous ci-dessous
>
>```python
>	def a_arc(self, noeud1, noeud2) :
>    		'''
>    		renvoie True si il y a un arc de noeud1 vers noeud2 et False sinon
>    	 	: param noeud1, noeud2 (?) deux noeuds différents
>    		: return (boolean)
>    	  	>>> g = Graphe_oriente_dic()
>    	  	>>> g.ajouter_noeud('A')
>  	  	>>> g.ajouter_arc('B', 'C')
>  	  	>>> g.a_arc('B', 'C')
>  		True
>  	  	>>> g.a_arc('A', 'B')
>  		False
>  		'''
>    ```



#### Les sucesseurs

>    
>
>    Codez et documentez la méthode `successeurs(self, noeud)` qui renvoie le tableau des successeurs du noeud passé en paramètre.
>
>    Cette fonction, avec une implémentation par dictionnaire d'adjacence, tient en une ligne, sans condition ni boucle !
>
>     



#### Affichage et représentation

On souhaite les affichages suivant en console :

```python
>>> g
Graphe orienté de 6 sommets.
>>> print(g)
Ray : Mick 
Mick : Jim Ray 
Jim : Joe 
Joe : Etta Mick 
Etta : 
Ella : Ray Etta 
```



>   
>
> Réalisez les méthodes `__repr__(self)` et `__str__(self)`. Documentez les.
>
>   



#### Application

Dans un nouveau fichier python `graphes_amities.py`, importer notre module.

Codez une fonction `graphe_oriente_dic` qui renvoie le graphe orienté ci-dessous :

![amitiés non symétriques](media/graphe_oriente.jpg)

Testez ensuite votre graphe en l'affichant dans la console.

```python
>>> g = graphe_oriente_dic()
>>> g
???
>>> print(g)
???

```



### Graphe non orienté



Voici le début de l'implémentation :

```python
class Graphe_non_oriente_dic() :
    '''
    une classe pour un graphe non orienté avec une matrice adjacente
    '''
    def __init__(self):
        '''
        construit le graphe avec pour seul attribut un dictionnaire vide
        '''
        self.adj = {}
```



La suite ressemble très (très) fort à l'implémentation du graphe orienté. Il nous faut les méthodes :

* `ajouter_sommet(self, sommet)` qui ajoute la clé _sommet_ au dictionnaire d'adjacence, à condition qu'il n'y soit pas déjà;
* `ajouter_arete(self, sommet1, sommet2)` qui crée les deux sommets via la méthode précédente puis l'arête entre eux (chacun devient voisin de l'autre s'il ne l'est pas déjà).
* `a_arete(self, sommet1, sommet2)` qui renvoie _True_ s'il y a une arête entre les deux sommets et _False_ sinon
* `voisins(self, sommet)` qui renvoie un tableau des voisins du sommet.
* pour finir, les méthodes réservées :
    *  `__repr__` qui renvoie une chaine décrivant le type de graphe et sa taille
    * `__str__` qui renvoie une chaine pour afficher le graphe



> EN AVANT !
>
> N'oubliez pas documentation et test !



#### Application

Reprenez le précédent fichier Python.

Codez une fonction `graphe_non_oriente_dic` qui renvoie le graphe non orienté ci-dessous :

![les amis](media/graph_amitie.jpg)

Testez ensuite votre graphe en l'affichant dans la console.

```python
>>> g = graphe_non_oriente()
>>> g
???
>>> print(g)
???
```











_________

Par Mieszczak Christophe Licence CC- BY - SA
source des images : production personnelle
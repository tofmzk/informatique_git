## Recherche d'un cycle dans un graphe non orienté



Un `cycle` est un chemin qui se termine là où il commence et n'emprunte pas deux fois la même arête (ou arc).

> Donnez deux sont les cycles du graphe non orienté ci-dessous :![](media/graph_amitie.jpg)



Pour recherche un cycle dans un graphe, un parcours en profondeur d'abord est particulièrement adapté.

Pour detecter la présence d'un cycle à partir d'un sommet, il suffit d'effectuer un parcours en profondeur mais de l'arrêter si un voisin du sommet en cours qui n'est pas déjà son parent  à déjà été visité

Remarquez bien qu'on ne peut emprunter deux fois la même arête : c'est pour cela qu'il faut aussi vérifier que le parent du sommet en cours n'est pas le voisin.

**Ex** : On part de Mick donc on l'empile

* on dépile Mick qui est alors visité;

* les voisins de Mick sont Ray, Joe et Jim. Ils n'ont pas été visité et aucun n'est le parent de Mick donc on les empile et on leur donne pour parent Mick;
* on dépile Ray qu iest alors visité;
* les voisins de Ray sont Mick, Ella, et Etta mais Mick et le parent de Ray donc on l'ignore. On empile Ella et Etta en leur donnant Ray pour parent
* On dépile Joe qui est alors visité
* Les voisins de Joe sont Mick et Jim. Mick et le parent de Joe donc on l'ignore. On empile Jim en lui attribuant Joe comme parent
* On dépile Jim qui est alors visité;
* Les voisins de Jim sont Joe et Mick. Joe est le parent de Jim donc on l'ignore. Mick a déjà été visité : STOP IL Y A UN CYCLE !!

Reprenons notre programme _parcourir_graphe.py_ puis ajoutez-y et complétez la fonction ci-dessous en vous inspirant du parcours en profondeur :

```python
def cycle_non_oriente(graphe, sommet):
    '''
    renvoie True si le graphe présnte un cycle à partir du sommet et False sinon
    : params
    	graphe (Graphe non orienté)
    	sommet (?) un sommet du graphe
    : return (bool)
    '''
    a_un_cycle = False
    liste_sommets_parcourus = []
    pile = module_lineaire.Pile()
    pile.empiler(sommet)
    parent = {sommet : None}
    while not pile_voisins.est_vide() and not a_un_cycle :
    
```





_______

Par Mieszczak Christophe

Licence CC BY SA

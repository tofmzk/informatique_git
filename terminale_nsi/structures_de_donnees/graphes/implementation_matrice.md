# Graphe et matrice adjacente

## Représenter un graphe par une matrice

Pour travailler sur les graphes, on utilise ici une représentation en tableaux appelés _matrices adjacentes_.  



### Graphes non orientés

 ![graphe non orienté](media/graph_amitie.jpg)





Dans le tableau ci-dessous, on a placé un `1` lorsqu'il y a une arête entre les deux sommets et un `0` sinon.



|      | Etta | Ray  | Mick | Ella | Joe  | Jim  |
| ---- | :--: | :--: | :--: | :--: | ---- | :--: |
| Etta |  0   |  1   |  0   |  1   | 0    |  0   |
| Ray  |  1   |  0   |  1   |  1   | 0    |  0   |
| Mick |  0   |  1   |  0   |  0   | 1    |  1   |
| Ella |  1   |  1   |  0   |  0   | 0    |  0   |
| Joe  |  0   |  0   |  1   |  0   | 0    |  1   |
| Jim  |  0   |  0   |  1   |  0   | 1    |  0   |



On enlève alors la première ligne et la première colonne pour ne conserver que le _coeur_ du tableau : la matrice du graphe :



$`\begin{bmatrix} 0&1&0&1&0&0 \\ 1&0&1&1&0&0 \\ 0&1&0&0&1&1 \\ 1&1&0&0&0&0 \\ 0&0&1&0&0&1 \\ 0&0&1&0&1&0 \end{bmatrix}`$



_Remarque 1 :_ 
En Python, une matrice est une liste de liste. Comment s'écrirait la matrice ci-dessus ?

 ```python

 ```



_Encore des remarques :_

* Ici, la matrice est symétrique par rapport à ses diagonales : c'est la marque des graphes non orientés. 

* L'ordre du graphe est égal à la largeur et à la hauteur de la matrice.

* La sommes des chiffres de cette matrice est égales à la somme des degrés des sommets et au double du nombre d'arêtes. **Vérifiez le !**

    

### Graphe orienté



Sur le même principe que pour les graphes non orientés, trouvez la matrice adjacente du graphe orienté ci-dessous :

 ![graphe orienté](media/graphe_oriente.jpg)



  

|      | Etta | Ray  | Mick | Ella | Joe  | Jim  |
| ---- | :--: | :--: | :--: | :--: | :--: | :--: |
| Etta |      |      |      |      |      |      |
| Ray  |      |      |      |      |      |      |
| Mick |      |      |      |      |      |      |
| Ella |      |      |      |      |      |      |
| Joe  |      |      |      |      |      |      |
| Jim  |      |      |      |      |      |      |

  

 $`\begin{bmatrix} 0&0&0&0&0&0 \\ 0&0&0&0&0&0 \\ 0&0&0&0&0&0 \\ 0&0&0&0&0&0 \\ 0&0&0&0&0&0 \\ 0&0&0&0&0&0 \end{bmatrix}`$



_Remarques :_

* Ici, la matrice n'est pas symétrique par rapport à ses diagonales : c'est la marque des graphes orientés. 
* L'ordre du graphe est égal à la largeur et à la hauteur de la matrice.
* La sommes des chiffres de cette matrice est égales à la somme des degrés des noeuds et au nombre d'arcs. **Vérifiez le !**



## Passons en Python

Nous allons coder un fichier `module_graphe.py` dans lequel nous allons définir toutes les classes pour les graphes.



### Graphe orienté



#### Initialisation

On va définir un graphe vide avec une matrice adjacente de dimension $`n \times n`$. Nous allons utiliser un booléen (`True` ou `False`) pour noter la présence ou l'absence d'un arc entre les noeuds qui seront numérotés de 0 à n - 1.



> Compléter le code ci-dessous :
>
> ```python
> class Graphe_oriente_mat() :
>        '''
>        une classe pour les graphes avec une matric adjacente
>        '''
>        def __init__(self, n):
>            '''
>         	définit un graphe vide dont les noeuds seront sumérotés de 0 à n - 1
>      	avec pour attributs :
>         - n : la dimension de la matrice adjacente
>         - adj : la matrice adjacente (un tableau de tableaux) ne contenant que des False
>         : param n (int) n > 0
>         '''
> ```
> 
> 

#### Ajouter une arête orientée.



Pour ajouter un arc orienté d'un sommet `noeud1` vers un sommet `noeud2`, il suffit de passer l'élément correspondant à ce lien de notre matrice adjacente à `True`. Attention, l'ordre des noeuds est très important puisque cette arête est orientée dans un sens et pas dans l'autre.



> Compléter la méthode ci-dessous.
>
> ```python
> 	def ajouter_arc(self, noeuds1, noeuds2):
>        	'''
>         	ajoute une arete orientée de noeud1 vers noeuds2  
>         	: param sommet1, sommet2 (int) deux entiers différents entre 0 et n - 1
>         	: pas de return, EFFET DE BORD sur self
>         	>>> g = Graphe_oriente_mat(4)
>         	>>> g.ajouter_arc(0, 1)
>         	>>> g.ajouter_arc(0, 3)
>         	>>> g.ajouter_arc(1, 2)
>         	>>> g.ajouter_arc(3, 1)
>         	>>> g.adj == [[False, True, False, True], [False, False, True, False], [False, False, False, False], [False, True, False, False]]
>         	True
>         	'''
>    ```
> 
>



#### Recherche des voisins 

En ce qui concerne les graphes orientés, on parle de successeur ou de prédécesseurs et pas vraiment de voisins. On peut cependant considérer que les voisins d'un noeud (plutôt un noeud, c'est vrai) sont ses successeurs. Cela sera très pratique pour la suite.



> * Complétez le prédicat ci-dessous ci-dessous
>
> ```python
> 	def a_arc(self, noeud1, noeud2) :
>         '''
>         renvoie True si il y a un arc de noeud1 vers noeud2 et False sinon
>         : param noeud1, noeud2 (int) deux entiers différents entre 0 et n (exclus)
>         : return (boolean)
>         >>> g = Graphe_oriente_mat(4)
>         >>> g.ajouter_arc(0, 1)
>         >>> g.ajouter_arc(0, 3)
>         >>> g.ajouter_arc(1, 2)
>         >>> g.ajouter_arc(3, 1)
>         >>> g.a_arc(0,1)
>         True
>         >>> g.a_arc(1, 3)
>         False
>         '''
> ```
> 
> * En utilisant le prédicat précédent, complétez la méthode ci-dessous :
>
>     ```python
>       	def successeurs(self, noeud):
>             '''
>             renvoie les successeurs du noeud
>             : param noeud (int) entre 0 et n
>             : return liste des successeurs (list)
>             >>> g = Graphe_oriente_mat(4)
>             >>> g.ajouter_arc(0, 1)
>             >>> g.ajouter_arc(0, 3)
>             >>> g.ajouter_arc(1, 2)
>             >>> g.ajouter_arc(3, 1)
>             >>> g.successeurs(1) == [2]
>             True
>             >>> g.successeurs(0) == [1, 3]
>             True
>             >>> g.successeurs(2) == []
>             True
>             '''
>     ```



#### Affichage et description

Nous voudrions que l'affichage est la représentation de notre graphe orienté soit la suivante :

```python
>>> g = Graphe_oriente_mat(4)
>>> g.ajouter_arc(0, 1)
>>> g.ajouter_arc(0, 3)
>>> g.ajouter_arc(1, 2)
>>> g.ajouter_arc(3, 1)
>>> g
Graphe orienté défini par une matrice adjacente de taille 4
>>> print(g)
0 ➔ 1 3 
1 ➔ 2 
2 ➔ 
3 ➔ 1 
```



> Définissez et codez les méthodes `__repr__` et `__str__` permettant cette affichage.



#### Application 



> Dans un fichier python que vous nommerez `graphes_amities.py`, importer notre module.
>
> Codez une fonction `graphe_oriente_mat` qui renvoie le graphe orienté correspondant au réseau social orienté ci-dessous :
>
> ![graphe orienté](media/graphe_oriente.jpg)

Testez ensuite votre graphe en l'affichant dans la console.

```python
>>> g = graphe_oriente_mat()
>>> g
???
>>> print(g)
???

```





### Graphe non orienté



#### Initialisation

On va définir un graphe vide avec une matrice adjacente de dimension $`n \times n`$. Nous allons utiliser un booléen (`True` ou `False`) pour noter la présence ou l'absence d'un arc entre les noeuds qui seront numérotés de 0 à n - 1.

```python
class Graphe_non_oriente_mat(Graphe_oriente_mat) :
   '''
    une classe pour les graphes avec une matric adjacente
    '''
    def __init__(self, n):
        '''
     	définit un graphe vide dont les sommets seront sumérotés de 0 à n - 1
  	avec pour attributs :
        - n : la dimension de la matrice adjacente
        - adj : la matrice adjacente (un tableau de tableaux) ne contenant que des False
        : param n (int) n > 0
        '''
        
```



#### Arêtes



> Complétez les méthodes ci-dessous. Ajoutez un Doctest en vous inspirant des Doctest de la méthode équivalente pour les graphe orientés.
>
> ```python
>     def ajouter_arete(self, sommet1, sommet2):
>            '''
>            ajoute une arete de sommet1 vers sommet2 et une arete de sommet2 vers sommet1
>            : param sommet1, sommet2 (int) deux entiers différents entre 0 et n (exclus)	: pas de return, EFFET De BORD sur self
>            '''
>    
> ```
> 
>``` python
> 	def a_arete(self, sommet1, sommet2) :
>        	'''
>        	 renvoie True si il y a une arête de sommet1 vers sommet2 et False sinon
>        	 : param sommet1, sommet2 (int) deux entiers différents entre 0 et n (exclus)
>         	: return (boolean)
>         	'''
>    ```
> 
>```python
> 	def voisins(self, sommet):
>     	'''
>        	renvoie les voisins du sommet
>        	: param sommet (int) entre 0 et n
>        	: return liste des voisins(list)
>        	'''
>    ```
> 
>



#### Représentation et affichage

>  
>
> Codez et documentez les méthodes  `__repr__`  et `__str__` pour cette classe en vous inspirant de celle de la classe précédente.
>
>  



#### Application

> Reprenez le précédent fichier Python.
>
> Codez une fonction `graphe_non_oriente_mat` qui renvoie le graphe non orienté ci-dessous :![les amis](media/graph_amitie.jpg)

Testez ensuite votre graphe en l'affichant dans la console.

```python
>>> g = graphe_non_oriente_mat()
>>> g
???
>>> print(g)
???
```





_________

Par Mieszczak Christophe Licence CC- BY - SA
source des images : production personnelle
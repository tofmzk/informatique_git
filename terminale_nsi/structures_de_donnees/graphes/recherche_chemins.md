# Recherche de chemins



Il s'agit ici de parcourir les graphes pour déterminer un `chemin`, aussi appelé `chaîne`, entre deux sommets, c'est à dire une succession de sommets reliés par une arête permettant, de voisin à voisin, de passer du sommet de départ au sommet de destination.



## En largeur : le chemin le plus court



Nous allons ici coder un algorithme afin de rechercher le plus court chemin d'un sommet à un autre. Le parcours en largeur est celui utilisé. Il nous faudra trois étapes pour déterminer ce chemin.





### Étape 1 : construction d'un dictionnaire des parents



Notre fonction sera `recherche_parent(graphe, sommet, destination)`.

Elle va parcourir le graphe en largeur jusqu'à destination ou épuisement des sommets et renvoyer un dictionnaire dont les clés sont les sommets et les valeurs le premier parent dont ils sont issus dans le parcours en largeur.

Le principe reprend celui du parcours en largeur auquel, il faut ajouter une structure permettant de retrouver le chemin une fois arrivé à destination. Un dictionnaire, `parent` , est idéal : 

* on crée une instance de `File`.
* on enfile le `sommet` dans la file.
* le sommet de départ n'a pas de parent : `parent = {sommet : None}`.
* tant que la file ne soit pas vide **et** que le `sommet` n'est pas la  `destination`.
    * on défile la file dans sommet.
    * pour chaque voisin du sommet :
        * s'il n'est pas dans `parent` (c'est à dire non visité) :
            * on l'enfile
            * on précise que son parent est `sommet` : `parent[voisin] = sommet`.
* Pour terminer, on renvoie le dictionnaire parent.



>  Complétez la fonction ci_dessous :
>  ```python
>  def rechercher_parent(graphe, sommet, destination):
>    	'''
>    	renvoie un dictionnaire permettant de trouver
>     	le chemin le plus court sommet et destination dans le graphe
>     	: params
>      	graphe (Graphe...)
>      	sommet (?) le sommet de départ 
>      	destination (?) le sommet à atteindre
>     	: return (dict)
>     	'''
>  ```
>
>  Testez dans la console :
>
>  ```python
>  >>> rechercher_parent(mon_graphe, 'mick', 'etta')
>  ???
>  ```
>
>  



### Étape 2 : création d'une pile



Une fois qu'on a un dictionnaire `parent`, on peut remonter, à partir le sommet de `destination`, les sommets qui ont été parcourus jusqu'au  `sommet` de départ.

Pour retrouver le chemin à partir du dictionnaire, on va utiliser une instance de `Pile`:

* On crée une pile vide.
* on empile `destination`
* tant que `parent[destination]` n'est pas `None`:
    * `destination` prend la valeur de `parent[destination]` .
    * on empile `destination`
* on renvoie la pile

> Complétez la fonction ci-dessous :
>
> ```python
> def construire_pile(parent, destination):
>        '''
>        renvoie une pile dans laquelle on a empilé les parents en partant de la destination
>        : param parent (dict)    
>        : param destination (?) le sommet de destination
>        : return (Pile)
>        '''
> ```
>
> Testez ensuite :
>
> ```python
> >>> parent = rechercher_parent(mon_graphe, 'mick', 'etta')
> >>> pile_sommets = construire_pile(parent, 'etta')
> >>> print(pile_sommets)
> ???
> ```
>
> 

### Étape 3 : création du chemin 



Une fois la pile remplie, elle contient, dans l'ordre, les sommets à parcourir pour atteindre la `destination`. Il n'y a plus qu'à la dépiler pour les obtenir.

> Complétez la fonction ci-dessous :
>
> ```python
> def depiler_chemin(graphe, sommet, destination):
>    	'''
>    	renvoie la liste correpondant au chemin le plus court dans le graphe su sommet à la destination
>    	: param graphe (Graphe...)
>    	: param sommet (?) le sommet de départ 
>    	: param destination (?) le sommet à atteindre
>    	: return le chemin (list)
>    	'''
>        chemin = []
>        parent =
>        pile_sommets =
>    ```
> 
>testez ensuite dans la console
> 
>```python
> >>> chemin = depiler_chemin(mon_graphe, 'mick', 'etta')
> >>> chemin
> ???
> ```
> 
>



Nous utiliserons la recherche de chemin le plus court en [exercice](exercices.md) et dans plusieurs projets (comme [labyrinthe](../../projets/labyrinthe/labyrinthe_partie_2.md)). 



## En profondeur

On peut également utiliser un parcours en profondeur pour trouver un chemin d'un sommet à un autre. Cependant, il est de cette façon impossible de savoir si ce chemin et le plus court et même, très probablement, ce ne sera pas le cas.



Dans le cours sur [parcourir un graphe](parcourir_graphe.md), nous avons vu la différence avec un parcours en largeur .

> * Quelle structure sera différente lors d'une recherche en profondeur par rapport à celle en largeur ?
>
> * Ecrivez le principe de l'algorithme de recherche comme nous l'avons fait pour le parcours en largeur
>
> * Implémentez ce parcours :
>
>     ```python
>     def rechercher_parent_profondeur(graphe, sommet, destination):
>         '''
>         renvoie un dictionnaire permettant de trouver
>         un chemin d'un sommet à un autre avec un parcours en profondeur
>         : params
>         	graphe (Graphe...)
>         	sommet (?) le sommet de départ 
>         	destination (?) le sommet à atteindre
>         : return (dict)
>         '''
>     ```
>
> * Testez ensuite votre algorithme dans la console.
>
>     ```python
>     >>> 
>
>     ```
>
> * Quel autre algorithme faudra-t-il ajouter ? Faites le !
>
> * A-t-on toujours le chemin le plus court ?
> ```
> 
> ```





_________

Par Mieszczak Christophe

licence CC BY SA
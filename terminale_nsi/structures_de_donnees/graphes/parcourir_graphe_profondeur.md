# Parcourir un graphe en profondeur d'abord

## Principe

L'**algorithme de parcours en profondeur** d'abord (ou **DFS**, pour *Depth-First Search*) depuis un sommet, visite ce sommet puis refait un parcours en profondeur depuis chaque voisin non déjà visité de ce sommet.

Il explore ainsi chaque chemin depuis le sommet de départ jusqu'à atteindre un cul-de-sac ou avoir visité tous les sommets, puis revient au dernier sommet où il était possible de prendre un chemin différent.

Une application de ce type de parcours et la création de labyrinthes. Voici [une petite vidéo](https://upload.wikimedia.org/wikipedia/commons/transcoded/4/45/MAZE_30x20_DFS.ogv/MAZE_30x20_DFS.ogv.480p.vp9.webm) qui illustre cette construction.



## Exemples

**Exemple 1 : Reprenons notre graphe d'amitiés non orienté.**

![](media/graph_amitie.jpg)

Donner tous les parcours en profondeur en commençant par le sommet `Jim`.





**Exemple 2 : avec un graphe orienté**

Avec un graphe orienté, le principe est exactement le même. On remplace simplement les voisins par les successeurs.

![](media/graphe_oriente.jpg)

Donner un parcours en largeur en commençant par le sommet `Mick`.



## Implémentation 

#### Parcours en profondeur d'un graphe non orienté

Nous  allons placer le code dans le fichier _parcourir_graphe.py_. 

Nous avons deux parcours à coder :

* celui des graphes orientés qui partagent des méthodes de même nom;
* celuis des graphes non orientés qui font de même.

Nous utiliserons les graphes d'amitiés construits dans les cours précédents pour les tester.

#### Parcourir un graphe non orienté

Nous allons coder la fonction `parcourir_profondeur_non_oriente(graphe, sommet)` qui renvoie un tableau de sommets obtenus par parcours en profondeur du `graphe` non orienté à partir du `sommet` de départ.

Il nous faudra utiliser une pile de notre module `module_lineaire`.

Le principe est le suivant :

* on crée une liste `liste_sommets_parcourus` vide.
* on crée une instance de `Pile`.
* on empile le `sommet` dans la pile.
* tant que la pile n'est pas vide :
    * on dépile la pile dans la variable `sommet`.
    * si le `sommet` n'est pas déjà dans la liste des sommets parcourus :
        * on ajoute ce `sommet` à la liste des sommets  parcourus.
        * on récupère la liste des voisins de ce `sommet` dans le `graphe`.
        * on empile chaque voisin du`sommet` dans la pile.
* Pour finir on renvoie la liste des sommets parcourus.



> Pour voir si vous avez bien compris :
>
> * Pourquoi utiliser une pile plutôt qu'une pile ?
> * Qu'est-ce qui garantit qu'un sommet ne sera pas visité deux fois ?
>
> 
>
> Complétez la fonction ci-dessous (n'oubliez pas d'importer `module_lineaire` ):
>
> ```python
> def parcourir_profondeur_non_oriente(graphe, sommet) :
>     '''
>     renvoie la liste des sommets en parcourant le graphe en profondeur en partant de sommet
> 	: param graphe (Graphe...) un graphe non orienté
> 	: param sommet (?) le sommet de départ
>     : return (list) liste de sommets
>     '''
>     liste_sommets_parcourus = []
>     pile_voisins = module_lineaire.Pile()
>     pile_voisins.empiler(sommet)
>     while not pile_voisins.est_vide() :
> ```
>
> 
>
> Pour terminer, on teste le parcours en profondeur sur notre graphe d'amitié non orienté.

_Remarque :_

Il n'existe pas obligatoirement un unique parcours en profondeur. Or, la façon dont nous avons implémenter ce parcours donnera toujours le même résultat. On pourrait ajouter une part d'aléatoire dans le parcours en utilisant la méthode `shuffle`  du module `random` qui mélange les élément d'un tableau indexé.

Il suffirait de mélanger la liste des voisins d'un sommet avant de les empiler. 



#### Parcours non récursif en profondeur un graphe orienté

C'est presque la même chose ... mais avec des noeuds, et des successeurs.

> Codez la fonction `parcourir_graphe_oriente(graphe, noeud)` en vous inspirant de la fonction précédente.



## Implémentation récursive

Nous aurons besoin d'un accumulateur, c'est à dire d'une structure qui mémorise les sommets visités au fur et à mesure. Nous utiliserons pour cela une liste `sommets_visites` .

Le principe du parcours à partir d'un `sommet` est le suivant :

* si `sommets_visites` na' pas été initialisée (c'est donc le premier appel de la fonction récursive) alors on l'initiale en tant que liste vide.
* on ajoute le `sommet ` à la liste de sommets visités.
* pour chaque voisin de ce sommet :
    * si le voisin n'est pas dans la liste de sommets visités alors on appelle le parcours en profondeur du graphe depuis ce sommet, en passant en paramètre la liste de sommets visités.
* Pour finir, on renvoie la liste de sommets visités.



> Complétez la fonction ci-dessous.
>
> ```python
> def parcourir_profondeur_recursif(graphe, sommet, sommets_visites = None) :
>   '''
>   renvoie la liste des sommets en parcourant le graphe en profondeur en partant de sommet
>   : param graphe (Graphe..) un graphe orienté ou non
>   : param sommet (?) le sommet de départ
>   : return (list) liste de sommets 
>   '''
>   if sommets_visites == None :
>       sommets_visites = []
> 
> ```
>
> 







___________

Par Mieszczak Christophe

Licence CC BY SA

source images : production personnelle.

source vidéo : [wikipédia, licence CC BY SA, auteur purpy purple](
# Les Graphes : exercices

### Débranchés



_Exercice 1 :_

Dessinez tous les graphes non orientés ayant exactement trois sommets.



_Exercice 2 :_

Combien existe-t-il de graphes orientés  ayant exactement trois sommets ?



_Exercice 3 :_

Voici la matrice d'un graphe :

$`\begin{bmatrix} 0&1&1&0\\1&0&1&1\\1&1&0&0\\0&1&0&0 \end{bmatrix}`$

* Est-ce un graphe orienté ou non ? Pourquoi ?

* Déterminez le degré de chaque sommet et le sommet de degré maximum.

* Quel est l'ordre du graphe ?  

* Combien a-t-il d'arêtes ?

* En appelant les sommets A,B,C et D, donner une représentation graphique de cette matrice.

* Ce graphe comporte-t-il un cycle ? Si oui donnez en un .

* Ce graphe est-il connexe ?

    


_Exercice 4 :_

Voici la représentation d’un graphe non orienté.



<img src="media/ex4.png" alt="graph" style="zoom:60%;" />

* Quel est l'ordre de ce graphe ?

* Combien d'arêtes comporte le graphe ?

* Quel est le sommet de plus petit degré ?

- Quel est le sommet de plus grand degré ?
- Ce graphe comporte-t-il un cycle ? Si oui donnez en un .
- Ce graphe est-il connexe ?
- Donner le voisinage de A
- Compléter la matrice d'adjacence du graphe ci-dessous.


$`\begin{bmatrix} 0&0&0&0&0 \\ 0&0&0&0&0 \\ 0&0&0&0&0\\ 0&0&0&0&0 \\ 0&0&0&0&0 \end{bmatrix}`$



_Exercice 5 :_ 

Voici un graphe orienté.

<img src="media/ex5.png" style="zoom:33%;" />

* Est-il connexe ?
* compléter par un mot de vocabulaire :
    * A est un .....
    * pour A, C est un ......................... et E est un ........................
    * La flèche entre E et A s'appelle ............
* Compléter la matrice d'adjacence du graphe ci-dessous.


$`\begin{bmatrix} 0&0&0&0&0 \\ 0&0&0&0&0 \\ 0&0&0&0&0\\ 0&0&0&0&0 \\ 0&0&0&0&0 \end{bmatrix}`$

* En vous aidant du tableau ci-dessous, réaliser les listes d'adjacence de successeurs et de prédécesseurs du graphe

|    Sommet     |  A   |  B   |  C   |  D   |  E   |
| :-----------: | :--: | :--: | :--: | :--: | :--: |
|  Successeurs  |      |      |      |      |      |
| Prédécesseurs |      |      |      |      |      |



### Méthodes des graphes



_Exercice 1 :_ d'une implémentation à l'autre

Reprenez les classes que vous avez codées pour implémenter des graphes, orientés ou non.

* Pour les classes implémentées à partir d'une matrice adjacente, codez une méthode `renvoyer_dictionnaire(self)` qui renvoie le dictionnaire d'adjacence du graphe. Documentez la méthode. Proposez un Doctest.
* Pour les classes implémentées à partir d'un dictionnaire d'adjacence, codez une méthode `renvoyer_matrice(self)` qui renvoie la matrice adjacente du graphe. Documentez la méthode. Proposez un Doctest.



_Exercice 2 : ajouts de méthodes_

Reprenons l'implémentation d'une graphe orienté à partir d'un dictionnaire d'adjacence. Notez que les graphes non orientés hériteront de ces méthodes.

Codez et documentez les méthodes ci_dessous :

* `degre(self, sommet)` qui renvoie le degré du sommet, c'est à dire le nombre d'arcs issus de ce sommet.

* `max_degre(self)` qui renvoie le sommet de plus haut degré.

  ​    

Réalisez ces mêmes méthodes pour les graphes orientés implémentés via la matrice adjacente.



### Utilisation d'un graphe

_Exercice 1 : Coloriage et voisinage_

Voici une carte des grandes régions de France : 

![régions de France](https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Regions_France_2016.svg/575px-Regions_France_2016.svg.png)



Créez un fichier `regions.py ` dans lequel nous allons coder la suite de cet exercice.

La fonction `construire_graphe()` ci-dessous qui renvoie un graphe représentant les régions du continent, sans la corse et l'outre-mer qui n'ont pas de voisine et peuvent donc se colorier de n'importe quelle couleur,  de telle façon à ce que :

* Les sommets soient les régions et portent leur nom.
* un arc existe entre chaque région adjacente.

```python
import module_graphe 


def construire_graphe():
    '''
    renvoie un graphe avec les régions de France
    : return (Graphe_non_oriente_dic)
    '''
    france = module_graphe.Graphe_non_oriente_dic()
    france.ajouter_arete('HDF', 'Normandie')
    france.ajouter_arete('HDF', 'Grand Est')
    france.ajouter_arete('HDF', 'Ile de France')
    france.ajouter_arete('Normandie', 'Ile de France')
    france.ajouter_arete('Normandie', 'Pays de la Loire')
    france.ajouter_arete('Normandie', 'Bretagne')
    france.ajouter_arete('Normandie', 'Centre Val de Loire')
    france.ajouter_arete('Ile de France', 'Grand Est')
    france.ajouter_arete('Ile de France', 'Centre Val de Loire')
    france.ajouter_arete('Ile de France', 'Bourgogne Franche-Comté')
    france.ajouter_arete('Grand Est', 'Bourgogne Franche-Comté')
    france.ajouter_arete('Bretagne', 'Pays de la Loire')
    france.ajouter_arete('Pays de la Loire', 'Centre Val de Loire')
    france.ajouter_arete('Pays de la Loire', 'Nouvelle Aquitaine')
    france.ajouter_arete('Centre Val de Loire', 'Bourgogne Franche-Comté')
    france.ajouter_arete('Centre Val de Loire', 'Nouvelle Aquitaine')
    france.ajouter_arete('Centre Val de Loire', 'Auvergne Rhône Alpes')
    france.ajouter_arete('Nouvelle Aquitaine', 'Auvergne Rhône Alpes')
    france.ajouter_arete('Nouvelle Aquitaine', 'Occitanie')
    france.ajouter_arete('Auvergne Rhône Alpes', 'Occitanie')
    france.ajouter_arete('Auvergne Rhône Alpes', 'Provence Alpes Côte d\'Azur')
    france.ajouter_arete('Occitanie', 'Provence Alpes Côte d\'Azur')
    return france


```

> * Quel type de Graphe est utilisé ?
>
>     ```text
>                         
>     ```
>
>     Pourquoi utiliser ce type de graphe ?
>
>     ```text
>                         
>     ```
>
>     



On veut colorier les régions en utilisant le moins de couleurs différentes possibles et de façon à ce que deux régions adjacentes ne soient pas de la même couleur. Pour cela on va utiliser une méthode gloutonne pour coder la fonction `colorier(graphe)` :

* Comme il y a 12 régions, on dispose d'une liste de 12 couleurs : ['bleu', 'vert', 'rouge', 'jaune', 'marron', 'cyan', 'violet', 'orange', 'noir', 'gris', 'rose', 'blanc'].
* On crée une liste des sommets du graphe qu'on trie par nombre de voisins croissant (cf _un peu d'aide plus bas_).
* On enfile les sommets triés dans une File, en commençant par celui qui a le plus grand nombre de voisins puisque la liste de sommets est ainsi faite.
* On crée un dictionnaire `couleurs_disponibles`qui a pour clés les sommets du graphe et pour valeur une liste des couleurs disponibles (attention aux effets de bord).
* On crée un dictionnaire vide `couleur_choisie` qui stockera la couleur choisie pour chaque région.
* tant que la File n'est pas vide :
    * on défile la file et on stocke le sommet obtenu : c'est celui de degré maximum dans la File.
    * on choisit la première couleur parmi celles possibles dans la liste des `couleurs_disponibles` de ce sommet et on ajoute dans le dictionnaire `couleur_choisie` la valeur choisie avec pour clé le nom de la région.
    * on retire la couleur choisie de chaque liste de couleurs des dictionnaires des voisins de notre sommet, si cette couleur est présente dans la liste.
* A la fin de la boucle, on renvoie le dictionnaire de couleurs choisies.

>   
>
> * Pourquoi cette méthode est-elle gloutonne ?
>
> * Codez cet algorithme !!
>
> * Ajoutez une fonction `renvoyer_nombre_de_couleurs(couleur_choisie)` qui renvoie le nombre de couleurs différentes utilisées.
>
>        
>
>       

_Un peu d'aide :_

pour trier les sommets par degré décroissant, le plus facile est de créer une liste de tuple (sommet, degré) puis de trier cette liste selon les degré. Une fois triée, il suffira d'enfiler le premier élément (donc le sommet), de chaque élément de la liste (qui sont les tuples) dans une file.

Testez pour comprendre :

```python
>>> tab = [('b', 3), ('c', 1), ('d', 2) ]
>>> sorted(tab, key = lambda x : x[0])
???
>>> sorted(tab, key = lambda x : x[0], reverse = True)
???
>>> sorted(tab, key = lambda x : x[1])
???
>>> sorted(tab, key = lambda x : x[1], reverse = True)
???
```

### Recherche d'un chemin dans un graphe



On dispose de la liste de mots de 4 lettres ci-dessous.

```python
LISTE_MOTS = ["aime", "auge", "baie", "brie", "bris", "bure", "cage", "cale", "came", "cape",
        "cime", "cire", "cris", "cure", "dame", "dime", "dire", "ducs", "dues", "duos",
        "dure", "durs", "fart", "fors", "gage", "gaie", "gais", "gale", "gare", "gars",
        "gris", "haie", "hale", "hors", "hure", "iris", "juge", "jure", "kart", "laie",
        "lame", "lime", "lire", "loge", "luge", "mage", "maie", "male", "mare", "mari",
        "mars", "mere", "mers", "mime", "mire", "mors", "muet", "mure", "murs", "nage",
        "orge", "ours", "page", "paie", "pale", "pame", "pane", "pape", "pare", "pari",
        "part", "paru", "pere", "pers", "pipe", "pire", "pore", "prie", "pris", "pues",
        "purs", "rage", "raie", "rale", "rame", "rape", "rare", "rime", "rire", "sage",
        "saie", "sale", "sape", "sari", "scie", "sure", "taie", "tale", "tape", "tare",
        "tari", "tige", "toge", "tore", "tors", "tort", "trie", "tris", "troc", "truc"]
```

La question est de trouver par quels mots passer pour aller d'un mot à un autre mot en ne changeant à chaque étape qu'une seule et unique lettre. L'objectif est d'obtenir le plus court chemin possible. Notez bien qu'il n'est pas toujours possible de trouver un chemin entre deux mots quelconques.

_Exemple :_

​	je veux, en partant du mot "aime" arriver au mot "tris" :

* je commence à "aime"
* je passe par "mari" en changeant le e en r
* je passe ensuite par "sari" en changeant le m en s
* j'arrive à "tris" en troquant le a pour un t



Créer un fichier `mots_voisins.py` dans lequel nous placerons notre code.



### Avec une classe Graphe



Pour commencer, on va définir la distance entre deux mots de quatre lettres comme le nombre de lettres différentes qu'ils comportent. Deux mots à une distance de 1 seront alors voisins.

> Complétez la fonction ci_dessous
>
> ```python
> def distance(mot1, mot2) :
>      '''
>      renvoie la distance entre les deux mots de 4 lettres
>      : param mot1 (str) une chaine de 4 caractères
>      : param mot2 (str) une chaine de 4 caractères
>      : return (int)
>      >>> distance('mari', 'sari')
>      1
>      >>> distance('mari', 'rage')
>      2
>      >>> distance('gage', 'cage')
>      1
>      '''
> ```
>
> 



Nous allons avoir besoin de construire la liste des voisins d'un sommet pour continuer.



> Complétez la fonction ci-dessous :
>
> ```python
> def renvoyer_voisins(mot):
>    	'''
>        renvoie la liste des voisins du mot
>        : param mot (str)
>        : return (list) la liste des voisins
>        >>> renvoyer_voisins('cage') == ['auge', 'cale', 'came', 'cape', 'gage', 'gaie', 'gale', 'gare', 'mage', 'nage', 'page', 'rage', 'sage']
>        True
>        '''
> ```
>
> 



Nous allons maintenant utiliser le `module_graphe` pour modéliser nos mots et leurs voisins.

> * Quelle type de graphe utiliser et pourquoi ?
> * Quels sont les sommets du graphe ?
>
> * Quand place-t-on un arc entre un sommet du graphe et un autre ?
>
> * Complétez la fonction ci-dessous :
>
>     ```python
>     def construire_graphe():
>         '''
>         renvoie un graphe modélisant les mots et leurs voisins
>         : return un graphe(...)
>                 '''
>         mon_graphe = module_graphe.Graphe...
>     ```



Il ne reste plus qu'à utiliser l'algorithme de recherche du chemin le plus court entre deux sommets, codés dans le fichier `parcourir.py` travaillé en cours,  pour déterminer le chemin le plus court entre deux mots.

Il faudra donc importer `parcourir` puis utiliser les fonctions adéquates.

> * complétez la fonction `chercher_chemin_dans_graphe(mot_depart, mot_final)` ci-dessous en utilisant le module `parcourir`.
> ```python
> def chercher_chemin_dans_graphe(mot_depart, mot_final) :
>     '''
>     renvoie un chemin reliant le mot_depart au mot_final, s'il y en a un.
>     renvoie None sinon.
>     : param mot_depart (str)
>     : param mot_final (str)
>     : return (list) une liste de mots
>     '''
> ```
>
> * Trouvez le chemin le plus court pour relier divers mots de la liste.
>
> 



## Sans utiliser une classe de Graphe



Une des parties les plus longues du code précédent et d'implémenter le graphe. Dans certains cas, ce graphe peut être gigantesque. En a-t-on vraiment besoin ? 

Pas vraiment.

On pourrait, en conservant les fonctions `distance` et `renvoyer_voisins` déjà réalisées, ajouter un algorithme de recherche spécifique à ce problème, sur le même modèle que celui codé pour la classe dans un cas général. Bien entendu, on ne pourra pas utiliser les méthodes de classes de graphes.



> * Complétez le code ci-dessous :
>
> ```python
> def chercher_chemin(mot_depart, mot_final) :
>     '''
>     renvoie un chemin reliant le mot_depart au mot_final, s'il y en a un.
>     renvoie None sinon.
>     : param mot_depart, mot_final (str)
>     : return un dictionnaire permettant de remonter les sommets sur le chemin (dic)
>     '''
>     file_voisins = module_lineaire.File()
>     file_voisins.enfile(mot_depart)
>     parent = {mot_depart : None}
>     while not file_voisins.est_vide() and not mot_depart == mot_final :
>         
> def construire_pile(parent, destination):
>     '''
>     renvoie une pile dans laquelle on a empilé les parents en partant de la destination
>     : param parent (dict)
>     : param destinantion (?) le sommet de destination
>     : return (Pile)
>     '''
>     
> def depile_chemin(pile_sommets):
>     '''
>     renvoie le chemin le plus court en dépilant la pile
>     : param pile_sommets (Pile)
>     : return le chemin (list)
>     '''
>     
>         
>         
> ```
>
> * Trouvez le chemin le plus court pour relier divers mots de la liste et indiquez vos résultats ci-dessous.
>
> ```python
> 
> ```
>
> 





_________

Par Mieszczak Christophe Licence CC- BY - SA
source des images : 

* [régions de France, wikipédia, CC BY SA, auteur Monsieur Fou](https://commons.wikimedia.org/wiki/File:Regions_France_2016.svg)
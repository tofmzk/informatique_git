# La raquette

## Initialisation

Dans un programme `module_raquette.py`, nous allons déclarer une nouvelle classe pour la raquette du pong : la classe `Raquette`.

Pour définir une raquette qui sera rectangulaire,  et de 10px d'épaisseur, nous avons besoin : 

* de sa position en abscisse (entre 10 et 240), l'ordonnée sera fixée à 230.
* de sa longueur entre 25px et 50px.



> * Déclarez la classe `Raquette` et sa méthode `__init__`.
>
> * N'oubliez pas les assertions.
>
> * un peu d'aide ?
>
>     ```python
>     # -*- coding: utf-8 -*-
>     '''
>     	la classe raquette
>     	: Auteurs :
>     '''
>                     
>     class Raquette():
>         def __init__(self, x):
>             '''
>             initialise une raquette de 40px de longueur à l'abscisse x
>              :param x (int) 50 <= x <= 250
>             '''
>             #####assertions
>             assert ...
>             assert ...
>             #### attributs
>             self.x = ...
>             
>     ```
> 
> 

## Repésentation

Comme pour la classe `Balle`, ajouter une méthode `__repr__` qui renvoie chaine de caractères décrivant la classe `Raquette`.



## Affichage

Définissez maintenant une méthode `afficher` qui dessine un rectangle de n'importe quelle couleur sauf le blanc qui est reservé aux murs, en utilisant le module `pyxel` et la fonction `pyxel.rect(x_coin_gauche, y_coin_haut, longueur, largeur, couleur)`.

N'oubliez pas d'importez `pyxel` .

Dans le programme `pong.py` , initialisez une instance de la classe `Raquette` et affichez la.



## Déplacement



Pour déplacer la raquette, on va utiliser les événements gérés par `pyxel` :

* `pyxel.btn(pyxel.KEY_RIGHT)` renvoie _True_ si on appuie sur la flèche droite du clavier.
* `pyxel.btn(pyxel.KEY_LEFT)` renvoie _True_ si on appuie sur la flèche flèche du clavier.



> * Définissez la méthode `deplacer` de la classe `Raquette` qui déplace la raquette de 5px à droite (respectivement à gauche)  lorsqu'on appuie sur la flèche droite (respectivement à gauche) et que la raquette n'a pas atteint de le bord.
> * Lorsque la raquette est trop à gauche, pensez à la recaler juste sur le bord gauche. Faites de même pour le côté droit.
> * Dans le programme `pong.py` faites se déplacer la raquette en appelant la méthode `deplacer` de la _raquette_ dans la fonction `calculer`. 



## Raquette, Balle et interface

Lorsque la balle touche la raquette alors elle rebondit.

Nous allons devoir, dans la classe `Balle` accéder aux attributs d'une instance de la classe `Raquette` pour vérifier si il y a rebond ou pas. 

**Il est INTERDIT en programmation orientée objet d'accéder aux attributs d'une classe en dehors de celle-ci.**

En d'autre mots, dans une classe donnée, il est toléré comme nous l'avons fait d'utiliser directement les attributs de la classe. Mais cela est prohibé depuis une autre classe.

Lorsqu'on utilise une classe, on doit passer par son **interface**, c'est à dire l'ensemble des méthodes de la classe accessibles à l'utilisateur.

On utilise une méthode particulière qui renvoie la valeur de l'attribut dont on a besoin. Une telle méthode s'appelle **un accesseur**. Voici celui de l'attribut _x_ :

```python
	def acc_x(self):
		'''
		renvoie l'attribut x 
		return (int)
		'''
        return self.x
    
```





C'est le moment de retourner voir [notre balle](la_balle.md).



_________

Par Mieszczak Christophe

Licence CC BY SA
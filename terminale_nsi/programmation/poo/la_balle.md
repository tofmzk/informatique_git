# La balle

La balle du pong a quatre caractéristiques :

* ses coordonnées (x, y) dans la fenêtre graphique.
* deux variables _dx_ et _dy_ qui définissent comment la balle se déplace en abscisse et en ordonnée.

Lorsqu'une balle entre en collision avec un coin de l'écran ou la raquette, elle réagit en conséquence.

## Déclaration d'une classe

Nous allons initialiser une **classe** nommée `Balle`  : il s'agit d'un nouveau type de _variable_ à l'image du type `int` ou du type `list` ... sauf que nous allons le coder nous même.

Une _variable_ dont le type est `Balle` est une **instance** de la classe  `Balle` et aura les caractéristiques et le comportement définis par cette classe.

Créez un fichier `module_balle.py`  et collez-y le code ci-dessous :

```python
# -*- coding: utf-8 -*-
'''
    un module pour la classe Balle
    : Auteur(s) nom, prénom
''' 

class Balle() :
    '''
    une classe pour une balle de casse-brique
    '''
    
```

Nous venons de déclarer la classe `Balle`.

_Remarque :_

Les classes se nomment avec une Majuscule au début du nom. On n'utilise pas le _undercore_ comme pour les variables _classiques_ mais on met une majuscule à chaque début de mot dans le nom. On aurait pû nommer cette classe  `BalleDuCassePong` par exemple.



## Initialiser une instance de classe

Ajoutons maintenant une _fonction_ à cette classe. Le mot _fonction_ est en italic car lorsqu'elle s'applique sur un objet, on les appelle des **méthodes**.

La première méthode va servir à initialiser la classe en déclarant ses caractéristiques qu'on appelle **attributs** :

```python
class Balle() :
    '''
    une classe pour une balle de casse-brique
    '''    
    def __init__(self, x, y, dx, dy):
        '''
        initialise une instance de la classe Balle
        : params
            x (int) 10 <= x <= 290 l'abscisse de la balle
            y (int) 10 <= y <= 250 l'ordonnée de la balle
            dx (int)le décalage en x de la balle 
            dy (int) le décalage en y de la balle 
        '''
        # assertions
        assert isinstance(x, int) and 10 <= x <= 290 , 'x doit être un entier entre 10 et 290'
        assert isinstance(y, int) and 10 <= y <=250, 'y doit être un entier entre 10 et 250'
        assert isinstance(dx, int), 'dx doit être un entier positif'
        assert isinstance(dy, int), 'dy est un entier positif'
        # initialisation
        self.x = x # abscisse de la balle
        self.y = y # ordonnée de la balle
        self.dx = dx # décalage en x
        self.dy = dy  #décalage en y

```

_Remarques importantes :_

* La méthode a un nom particulier compris entre `__` et `__` : c'est un méthode particulière dite `reservée`. Il y en a beaucoup d'autres comme on le verra.
* Dans les assertions, la fonction `isinstance` est utilisée plutôt que `type` pour vérifier si les arguments passés à la méthode `__init__` sont bien du type voulu. On verra vite pourquoi.
* Notez le `self` dans les paramètres de la méthode : il correspond à l'objet de la classe `Balle` sur laquelle la méthode s'applique : un tel objet est une `instance` de la classe Balle. D'où le `isinstance` qui permet de vérifier si un objet est une instance d'une classe ou pas.
* les attributs de cette classe sont déclarés également avec ce `self`  car eux aussi se réfèrent à une `instance` de la classe `Balle`

Pour initialiser une instance de la classe `Balle`, on la déclare ainsi :

```python
>>> nvelle_balle = Balle(100, 150, 1, -1)
```

Testez :

```python
>>> nvelle_balle.x
?
>>> nvelle_balle.y
?
>>> nvelle_balle.dx
?
>>> nvelle_balle.dy
?
```

_Remarques :_

* le `self` qu'on trouve dans les paramètres de la méthode ` __init__` et placé devant les attributs _x_, _y_, _rayon_, _dx_ et _dy_ font référence à l'`instance` de la classe sur laquelle elle s'applique : ici _nvelle_balle_.

## La méthode réservée `__repr__`  

_nvelle_balle_ est une donc `instance` de la classe Balle :

```python
>>> a = 2
>>> type(a)
<class int>
>>> nvelle_balle = Balle(100, 150, 1, -1)
>>> nvelle_balle
<class __main__.Balle>
```

L'affichage n'est pas très joli mais on peut le modifier avec une méthode réservée : `__repr__`.

```python
	def __repr__(self):
        '''
        renvoie une chaine qui décrit l'instance de la classe
        : return (str)
        '''
        return 'Une Balle de coordonnées ('+str(self.x)+', '+str(self.y)+')'
```

Testez à nouveau :

```python
>>> nvelle_balle = Balle(100, 150, 1, -1)
>>> nvelle_balle
???
```

C'est bien plus joli et parlant !



## Afficher la balle

On va _prototyper_ une nouvelle méthode pour afficher une instance de balle.

On utilise ici le module `pyxel` et la fonction `pyxel.circ(x, y, rayon, couleur)`. On va mettre la couleur en blanc (couleur no7) et la balle aura un rayon de 3px.

```python
    def afficher(self):
        '''
        affiche l'instance
        '''
        pyxel.circ(self.x, self.y, 3, 7)
```

_Remarques_ :

*  N'oubliez pas d'importer `pyxel` !

* Vous pouvez changer les couleurs :

    ![](media/couleurs.jpg)

Retournons maintenant dans le programme principle `pong.py` :

* Dans la partie _VARIABLES GLOBALES_ initialisez une instance de la classe balle nommée _balle_.

* Dans la fonction `affichage` appelez la méthode de la _balle_ qui permet de l'afficher :

    ```python
    def afficher():
        '''
        ici on dessine tous les objets du jeu
        '''
    	pyxel.cls(0)# peint la fenêtre en noir
        pyxel.rect(290, 0, 10, 250, 7)#rectangle de droite
        pyxel.rect(0, 0, 300, 10, 7)#rectangle du haut
        pyxel.rect(0, 0, 10, 250, 7)#rectangle de gauche
        balle.afficher() # affiche la balle.
    ```

    _Remarques :_ 

    * Regardez bien comment on appelle la méthode : `balle.afficher()`

        * la méthode `afficher` s'applique sur l'instance`balle`;

        * le `self` présent dans la méthode `afficher` de la classe balle fait référence à l'instance sur laquelle elle s'applique.

    * nous avons deux fonctions qui portent le même nom `afficher` mais ne peuvent être confondues :
        * la méthode `afficher`  de la classe `Balle` s'applique sur une instance de `Balle`;
        * la fonction `afficher` de _pong.py_ n'est pas une méthode de `Balle` mais une fonction appelées 30 fois par seconde qui gère l'affichage de notre jeu.

    

    > Allez-y testez !!

    ## Bouge de là

    Reste à déplacer cette balle. Revenons au module_balle.

    Pour cela on va définir la méthode `deplacer` qui va se contenter dans un premier temps de faire varier les attributs _x_ et _y_ en leur ajoutant respectivement les attributs _dx_ et _dy_.

    ```python
    	def deplacer(self) :
            '''
            deplace la balle et gère les rebonds
            '''
            self.x = self.x + self.dx
            self.y = self.y + self.dy
    ```

    Encore une fois, il faut retourner dans le programme `pong.py` , cette fois-ci dans la fonction `calculer` et on va y appeler cette fonction dur notre instance de `Balle`.

    ```python
    ###################Calculs indispensable au jeu
    def calculer() :
        '''
        ici on effectue tous les calculs nécessaires au jeu
        '''
        balle.deplacer()
    ```

    

> * Testez !
> * La balle ne part pas d'où vous voulez ? Pas assez vite ? Pas dans la bonne direction ? Modifier les arguments lors de l'initialisation de votre _balle_ !



Il faut maintenant que la balle rebondisse lorsqu'elle touche le côté droit, haut ou gauche. Pour le moment,on va également la faire rebondir si elle touche le bas de l'écran. A terme, il y aura la raquette.

* Si la balle rebondit latéralement alors _self.dx_ prend la valeur _-self.dx_.

* Si la balle rebondit verticalement alors _self.dy_ prend la valeur _-self.dy_.

Voici le début du code :

```python
    def deplacer(self) :
        '''
        deplace la balle et gère les rebonds sur les bords du jeu
        '''
        self.x = self.x + self.dx
        self.y = self.y + self.dy
        if self.x + 3 >= 290 : # si on tape à droite
            self.dx = - self.dx #on se déplace en x dans l'autre sens
            self.x = 287 # on recale la balle pile sur le bord
        elif self.x - 3 <= 10 : # si on tape à gauche 
            self.dx = - self.dx #on se déplace en x dans l'autre sens
            self.x = 13 # on recale la balle pile sur le bord
```



> * Complétez le code pour que la balle rebondisse en haut et en bas (le rebond bas est temporaire en attendant la raquette).
> * Testez à nouveau le jeu depuis `pong.py`.



## D'autres balles ?

Vous avez déjà compris que le programme `pong.py` était considérablement simplifié grâce à l'utilisation de notre classe. Mais cette dernière va permettre également d'ajouter une ou des balles supplémentaires très facilement !

Dans le programme _pong.py_, au lieu de créer une seule instance de Balle, on va en mettre plein dans un tableau

```python
import random
##########Variable globale
balles = []
for i in range(50) : #oui 50 balles
    balles.append(module_balle.Balle(random.randint(10, 290),
                                  random.randint(10, 250), 
                                  random.randint(1, 10),
                                  random.randint(1, 10)
                                 )
              )
```

> Modifier les fonctions `calculer` et `afficher`  pour qu'elles agissent sur chaque balle de _balles_ (un petit `for` ...)



Voilà vous avez compris un des nombreux intérêts de la POO.... Pour la suite, nous utiliserons deux balles dans le tableau _balles_.



## Et maintenant la raquette



Allez voir [la partie qui concerne la raquette](la_raquette.md).

Nous reviendrons ensuite peaufiner le comportement de notre balle.



## Rebond sur une raquette

Il faut maintenant que la balle rebondisse sur la raquette mais pas à côté.

On va avoir besoin d'une méthode `collision_raquette` dans notre classe `Balle`à laquelle on passera la _raquette_ sur laquelle la balle va tester une collision.

Attention, cette méthode de la classe `Balle` ne peut pas accéder directement aux attributs de la _raquette_ mais devra passer par les _accesseurs_ de la classe `Raquette` qui font partie de son interface.

```python
	def collision_raquette(self, raquette):
        '''
        test une collision avec la raquette
        : param raquette (Raquette)
        '''
        
```

Pour un rebond basic, on teste si la balle touche la raquette c'est à dire si :

* `balle.x` est comprise en l'abscisse de la raquette - 3 et l'abscisse de la raquette + 43 ;

* `balle.y` est comprise en l'ordonnée de la raquette - 3 et l'ordonnée de la raquette - 3 + `balle.dy`.



Si cela se produit, on recale `balle.y` de façon à ce que la balle soit pile sur la raquette et `balle.dy`   est multiplié par -1.

Normalement, ici , le jeu tourne.





> * Codez la méthode `collision_raquette`
> * Appelez cette méthode dans la fonction calculer de `pong.py` et testez le jeu !
> * supprimez la possibilité à la balle de rebondir en bas de l'écran.



## Game Over

On va avoir besoin d'un accesseur `acc_y` qui renvoie la valeur de l'attribut _y_ d'une balle.

Grace à lui, dans le jeu,on pourra détecter une fin de partie lorsque la balle sortira du jeu par le bas.

Nous avons vu qu'il s'agit là de la bonne technique pour obtenir la valeur d'un attribut en dehors du code de la classe de l'objet. 



> * Codez la méthode `acc_y(self)`
>
> * Retourner voir la suite dans la partie [jeu](le_jeu.md).
>
>     



_________

Par Mieszczak Christophe

Licence CC BY SA
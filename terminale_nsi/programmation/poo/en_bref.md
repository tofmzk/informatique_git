# En bref

La programmation orientée objet (ou POO) est un [paradigme de programmation](paradigmes.md) dans lequel on définit des `classes` afin de créer de _nouveaux types d'objets_, les `instances` de cette classe, dont on précisera les caractéristiques appelées `attributs` et decidera du comportement en codant leurs `méthodes`.

En générale, les classes sont codées dans des modules qu'on importe selon ses besoins.

## La base

### Initialisation 

Une classe est définie ainsi :

```python
class NomDeLaClasse :
    '''
    une classe pour ...
    '''
```

On `initialise` la classe en utilisant la méthode réservée `__init__ ` :

```python
# -*- coding: utf-8 -*-
'''
	classe NomDeLaClasse
    Auteurs : ..
'''

class NomDeLaClasse :
    '''
    une classe pour ...
    '''
    def __init__(self, param1, param2 ...) :
        '''
        initialise la classe
        '''
        
```

_Remarque_ : les méthodes réservées sont des méthodes particulières dont le nom est compris entre deux underscores à droite et à gauche. Il y en a de nombreuses, comme on le verra bientôt.



On déclare une `instance` de la classe, c'est à dire un nouvel objet de ce type, de la façon suivante :

```python
>>> nvel_obj = NomDeLaClasse(arg1, arg2, ...)
```

_Remarque_ : 

Le `self` fait référence à l'instance. Il signifie que la méthode `__init__` ainsi que les autres méthodes de la classe que nous coderons plus loin, s'applique sur elle.



On précise les attributs dans la méthode `__init__` :

```python
class NomDeLaClasse :
    '''
    une classe pour ...
    '''
    def __init__(self, param1, param2 ...) :
        '''
        initialise la classe
        '''
        self.attribut1 = param1
        self.attribut2 = param2
        etc ..
```

On récupère les attributs d'une instance de la façon suivante :

```python
>>> nvel_obj = NomDeLaClasse(arg1, arg2, ...)
>>> nvel_obj.attribut1
arg1
```

_Remarque_ : 

* le `self` fait toujours référence à l'instance. On définit ou demande les attributs d'une instance particulière.
* la méthode `__init__` et toutes les autres qui suivront sont dans la classe et leur code et donc décalée à droite de 4 espaces (ou une tabulation).
* On ne peut acceder directement aux attributs que dans le code de la classe elle même. Et encore... cela est toléré. On en reparle plus bas.

### Les méthodes d'une classe

Pour définir le comportement de la classe, on code ses `méthodes` qui,comme la première méthode `__init__` vont s'appilquer sur une instance de la classe (`self` dans le code) :

```python
	def methode1(self, param1, param2 ...)
    	'''
    	définit un comportement de l'instance
    	'''
        
```

_Remarque_ : 

* le `self` fait toujours référence à l'instance. 
* cette méthode et toutes les autres qui suivront sont dans la classe et leur code et donc décalé à droite de 4 espaces (ou une tabulation).

On appelle la méthode d'une instance de la fonçon suivante :

```python
>>> nvel_obj = NomDeLaClasse(arg1, arg2, ...)
>>> nvel_obj.methode1(arg1, arg2, ...)
```



## Accesseurs et mutateurs

En POO, la façon d'utiliser les classes est très cadrée. Nous verrons ici quelques règles de bonnes pratiques.



Comme pour les modules, les classes ont une interface : ce sont les méthodes dont l'utilisateur de la classe va se servir.

Les attributs ne font pas partie de l'interface  : on ne doit donc pas y acceder directement de l'exterieur de la classe mais passer par des méthodes particulières appelées `accesseurs` (_getters_ en anglais) et `mutateurs` (_setters_ en anglais) :

* une méthode `accesseur` renvoie la valeur d'un attribut.
* une méthode `mutateur` permet de modifier la valeur d'un attribut.



A l'intérieur du code de la classe, il est conseillé d'utiliser également les accesseurs et mutateurs même s'il est toléré d'accéder directement aux attributs de cette classe. La raison est que cela permet en cas d'évolution du code de ne modifier que les accesseurs et mutateurs sans avoir à modifier le reste du code.



## Méthodes réservées

Il existe des méthodes dites _réservées_ qui permette de coder des comportement particuliers.

### `__repr__` 



La méthode réservée `__repr__` renvoie une chaine de caractère qui décrit l'objet.

sans elle, si vous demandez ce qu'est un objet, la réponse n'est pas très prècise :

```python
>>> nvel_objet
<__main__.NomDeLaClasse object at 0x7f25d9e5fb80>
```



On la code ainsi :

```python
	def __repr__(self):
        return 'instance de la classe NomDeLaClasse`
```

_Remarque_ :  selon l'objet, on peut ajouter à cette description des caractéristiques de l'objet...

La conséquence est la suivante :

```python
>>> nvel_objet
'instance de la classe NomDeLaClasse`
```

### `__str__`

La méthode reservée `__str__` renvoie une chaine de caractères qui sera interprétée lors d'un `print`.

On la code ainsi :

```python
	def __str__(self):
        return 'affiche spécial à définir'
```

On a alors :

```python
>>> print(nvel_objet)
'affiche spécial à définir'
```



On peut donc définir comment un objet sera affiché via la commande `print`.

### Autre méthodes réservées

Il n'y a pas que le print qui peut être redéfini ! Certains opérateurs entre objets (le `+`, le `<`, le `in`, le `len` ....) peuvent être codés spécifiquement pour un objet donné.

Par exemple, pour la classe `int` et pour la classe `str` , l'opération `+` n'est pas la même. Le `+` est en réalité la méthode réservée `__add__` qui s'applique différemment sur une instance d'`int` et sur une instance de`str`.



Voici une liste non exhaustive de ces méthodes reservées :

|       définition        |   appel   |                       effet                        |
| :---------------------: | :-------: | :------------------------------------------------: |
|    `__str__(self)__`    | print(g)  | renvoie une chaîne de caractère décrivant l'objet. |
|     `__len(self)__`     |  len(g)   | renvoie un entier définissant la taille de l'objet |
|    `__repr(self)__`     |     g     |        renvoie une chaîne décrivant l'objet        |
| `__contains__(self, x)` |  g in x   |   renvoie `True` si x est dans g, `False` sinon    |
| `__getitem__(self , i)` |   g[i]    |           renvoie le i ème élément de g            |
|  ` __eq__(self, obj2)`  | g == obj2 |    renvoie `True` si g == obj2 et `False` sinon    |
| ` __leq__(self, obj2)`  | g < obj2  |    renvoie `True` si g < obj2 et `False` sinon     |
|  `__add__(self, obj2)`  | g + obj2  |         renvoie le résultat de l'addition          |
|  `__mul__(self, obj2)`  | g * obj2  |      renvoie le résultat de la multiplication      |





___________

Par Mieszczak Christophe

Licence CC BY SA




# Différents paradigmes



Il existe différents paradigmes de programmation :

* En seconde, on commence à programmer avec le [paradigme impératif](https://fr.wikipedia.org/wiki/Programmation_imp%C3%A9rative) : les instructions se suivent les unes derrières les autres. Ce mode de programmation est utilisé pour des programmes simples car il devient vite lourd à gérer, écrire et déboguer. 
* L'année dernière, nous avons commencé à découper le code en petites fonctions. Nous avons insisté, dans le chapitre [modularité](../modularite/modularite.md), sur l'intérêt de  faire cela. Le paradigme de programmation que l'on appelle le [paradigme fonctionnel](https://fr.wikipedia.org/wiki/Programmation_fonctionnelle) fait cela en évitant à tout prix deux choses :
    * les variables globales : toute variable doit être définie dans une fonction est, donc, locale.
    * les effets de bord : une fonction ne doit pas en générer dans ce paradigme.
* Nous avons également programmé avec le [paradigme événementiel](https://fr.wikipedia.org/wiki/Programmation_fonctionnelle) dans le chapitre [interactions homme - machine sur le web](../../../premiere_nsi/interactions_homme_machine_web/evenements_et_pages_web.md) où les événements qui se produisent dans une page web (clic, mouvement de souris, ...) déclenchent des fonctions.
* Dans ce chapitre, nous allons aborder le `paradigme objet`. Il va nous permettre de définir de nouvelles structures de données, appelées `classes`, possédant des `attributs` et des `méthodes` .

_________

Par Mieszczak Christophe

licence CC BY SA
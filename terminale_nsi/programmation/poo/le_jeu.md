# Déroulement du jeu

Nous allon utiliser le module `pyxel`.

Créez un fichier `pong.py` et collez-y la structure d'un jeu codé avec `pyxel` que voici :

```python
# -*- coding: utf-8 -*-
'''
	le jeu du pong
	: Auteurs :
####### importation des modules nécessaires
import pyxel

######## VARIABLES GLOBALES

###################Calculs indispensable au jeu
def calculer() :
    '''
    ici on effectue tous les calculs nécessaires au jeu
    '''
    

###################Affichage des objets du jeu
def afficher():
    '''
    ici on dessine tous les objets du jeu
    '''


##############lancement du programme
def jouer():
    pyxel.init(300, 250, 'PoNg')
    pyxel.run(calculer, afficher)
```



Voilà comment cela marche : 

*  Tout en haut de ce code vous lisez la ligne `import pixel` : cette ligne permet de charger les instructions du module `pyxel` nécessaire à la réalisation du jeu.
*  Juste après l'importation du module _pyxel_, il y a une zone réservée à la déclaration des variables dont nous aurons besoin. On y reviendra plus tard.
*  la fonction `calculer`  détermine un _bloc de code_ où l'on placera tous les calculs nécessaires au déroulement du jeu : trajectoire de la balle, position de la raquette, intersections de la balle avec les bord de l'écran et la raquette. 
*  la fonction `afficher` détermine un _bloc de code_ dans lequel on va dessiner toutes les parties du jeu : le fond, le contour et la balle.
*  Tout en bas vous lisez la définition de la fonction `jouer`. Cette dernière va lancer le jeu :
    * `pyxel.init(300, 250, 'PoNg')` initialise une fenêtre graphique de 300 pixels de large sur 250 pixels de hauts.
    * `pyxel.run(calculer, afficher)` va, environ 30 fois par seconde, appeler deux _fonctions_

Testez la seule fonction qui contient du code en tapant dans la console :

```python
>>> jouer()
```

Une fenêtre s'ouvre. Voilà comment cela se présente :

![le jeu](media/fenetre.jpg)

On va juste tracer 3 rectangles pour délimiter les bords du jeu :

```python
def afficher():
    '''
    ici on dessine tous les objets du jeu
    '''
	pyxel.cls(0)# peint la fenêtre en noir
    pyxel.rect(290, 0, 10, 250, 7)#rectangle de droite
    pyxel.rect(0, 0, 300, 10, 7)#rectangle du haut
    pyxel.rect(0, 0, 10, 250, 7)#rectangle de gauche
```

Allez-y et testez !



_Remarques_ :

* L'instruction `rect(coin_x, coin_y, longueur, largeur, couleur)` affiche un rectangle à partir des coordonnées du coin haut et gauche.

* Vous pouvez changer les couleurs :

    ![](media/couleurs.jpg)



## Balle et raquette

Maintenant qu'on a un environnement on va avoir besoin d'une [balle](la_balle.md) et d'une [raquette](la_raquette.md).

Suivez ces deux liens avant de revenir ici pour la suite.

## Fin de partie

Grâce à l'accesseur `acc_y` de la classe `Balle`, on peut récupérer la position de la balle en ordonnée et tester si elle dépasse 250, c'est à dire si la balle descend plus bas que le bas de l'écran.

Dans ce cas, on repositionne _balle_ et _raquette_ dans leur état initial.

> * Ajouter à la méthode `calculer` les lignes nécessaires.
>
> * Ajouter à la méthode `afficher` une condition pour que raquette et balle(s) ne s'affichent que si la partie n'est pas perdue et que si elle l'est, le message `GaMe OvEr' apparait à l'écran.
>
> On utilisera `pyxel.text(x, y, texte_a_afficher, couleur)`.



Normalement, ici , le jeu tourne.

Cependant, le code de _pong.py_ n'est pas parfait : il y a des variables globales !

Voyons la suite pour éviter cela.



## Un module et une classe pour le jeu

Il y a des variables globales dans ce programme.

C'est moche...et fortement déconseillé.

Comment faire pour s'en passer ?

### La classe jeu

Créez un fichier `module_jeu.py` dans lequel vous importez les module `pyxel`, `module_balle`  et `module_raquette`.

Dans ce fichier, déclarez et initialisez la classe `Jeu` sans paramètre autre que `self` et dont les attributs sont :

* _raquette_, une instance de `Raquette` initialisée de façon à être centrée.
* _balle_, une instance de la classe `Balle` initialisée de façon à se trouver juste sur la raquette avec _dx = 4_ et _dy = -4_.
*  _nbre_de_vies_, un entier initialisé à 2



Coder également une méthode `__repr__` pour cette classe.

### Jouer

Définissez et codez, en utilisant le code du programme _pong.py_ : 

* la  méthode `calculer`  :
    * qui va gérer le déplacement de la raquette, de la _balle_ et les collisions _balle-raquette_. 
    * qui va tester si la balle sort de la zone de jeu. Dans ce cas le nombre de vies diminue de 1.(S'il est à 0, la partie est perdue).

* une méthode `afficher` qui va afficher toutes les balles, la _raquette_, le nombre de vies et, en cas de fin de partie, le Game Over.

* une méthode `jouer` qui initialise une fenêtre de jeu et lance les deux méthodes précédentes 30 fois par seconde.

    



### pong.py



> Modifier le programme _pong.py_ pour qu'en l'exécutant, vous lanciez une partie.



_______________

Par Mieszczak Christophe

Licence CC BY SA
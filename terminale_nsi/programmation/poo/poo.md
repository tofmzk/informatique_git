# Programmation Orientée Objet

Le fil rouge : Nous allons coder un jeu de Pong avec le paradigme objet.

![](media/pong.png)

Pour ce jeu, nous allons avoir besoin de plusieurs objets :

* La balle

* La raquette

* Le jeu en lui même

      

Chacun de ces objets a des caractéristiques (position, dimensions ...) et un comportement qui lui est propre.

Chaque objet sera utilisé pour permettre le [déroulement du jeu](le_jeu.md).

___________________

Par Mieszczak Christophe

Licence CC BY SA


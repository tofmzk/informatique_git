# Programmation Orienté Objet (POO)

## Différents paradigmes



Il existe différents paradigmes de programmation :

* En seconde, on commence à programmer avec le [paradigme impératif](https://fr.wikipedia.org/wiki/Programmation_imp%C3%A9rative) : les instructions se suivent les unes derrières les autres. Ce mode de programmation est utilisé pour des programmes simples car il devient vite lourd à gérer, écrire et déboguer. 
* L'année dernière, nous avons commencé à découper le code en petites fonctions. Nous avons insisté, dans le chapitre [modularité](../modularite/modularite.md), sur l'intérêt de  faire cela. Le paradigme de programmation que l'on appelle le [paradigme fonctionnel](https://fr.wikipedia.org/wiki/Programmation_fonctionnelle) fait cela en évitant à tout prix deux choses :
    * les variables globales : toute variable doit être définie dans une fonction est, donc, locale.
    * les effets de bord : une fonction ne doit pas en générer dans ce paradigme.
* Nous avons également programmé avec le [paradigme événementiel](https://fr.wikipedia.org/wiki/Programmation_fonctionnelle) dans le chapitre [interactions homme - machine sur le web](../../../premiere_nsi/interactions_homme_machine_web/evenements_et_pages_web.md) où les événements qui se produisent dans une page web (clic, mouvement de souris, ...) déclenchent des fonctions.
* Dans ce chapitre, nous allons aborder le `paradigme objet`. Il va nous permettre de définir de nouvelles structures de données, appelées `classes`, possédant des `attributs` et des `méthodes` .



## Le fil rouge



Nous allons prendre en fil rouge l'exemple d'un carnet d'adresses pour illustrer ce chapitre.

Pour chaque contact de ce carnet d'adresses,on précisera :

* son nom et prénom
* un e-mail 
* son numéro de téléphone

Il devra être également être possible de modifier les caractéristiques d'un contact.



Le carnet d'adresse est une liste de contacts. L'utilisateur devra pouvoir : 

* savoir si un contact existe déjà.

* ajouter un nouveau contact ou en retirer un.

* afficher les contacts.

    

  ​    


## La classe Contact



Nous allons définir une `classe` que nous nommerons `Contact`. Remarquez la majuscule à `Contact`|, elle n'est pas là par erreur : le nom d'une classe est toujours écrit en Camel Case et non en underscore case. Ainsi, il y a une majuscule au début du nom de la classe, et une majuscule à chaque mot qui compose le nom de la classe.

Cette `classe` devra posséder les caractéristiques attendues pour notre contact.



### Initialiser une classe





Créez un programme `Carnet.py` puis commençons notre code en déclarant notre objet :

```python
# -*- coding: utf-8 -*-
'''
	le carnet d'adresse
	: Auteurs :
'''
class Contact() :
    '''
    une classe pour définir un contact
    '''
```



Il nous faut maintenant  initialiser notre classe en utilisant une méthode (c'est à dire une fonction spécifique à la classe). La syntaxe est un peu particulière car il s'agit d'une fonction spécifiquement utilisée pour initialiser la classe.

```python
# -*- coding: utf-8 -*-
class Contact() :
    '''
    une classe pour définir un contact
    '''
    def __init__(self, nom_prenom = '', e_mail = '', tel = '') :
        '''
        initialise un contact
        : param nom_prenom (str)
        : email (str)
        : tel (str)
        '''
        assert(isinstance(nom_prenom, str)), 'nom_prenom doit être un str'
        assert(isinstance(e_mail, str)), 'e_mail doit être un str'
        assert(isinstance(tel, str)), 'tel doit être un str'

    
```



_Remarques :_

* remarquez la tabulation après la déclaration de la classe.  Tout ce qui concerne cette classe devra être ainsi tabulé.
* Les assertions garantissent les pré-conditions : les trois paramètres sont du type `str`.
*  Cette fonction s'appliquant sur une instance de la classe elle même, elle porte le nom de `méthode`.
* La méthode `__init__` a trois paramètres. `self` fait référence à l'objet lui même, `instance` de la classe `Contact`. 

Testez :

```python
>>> c = Contact('Steve Boulot', 'steve.boulot@pomme.fr', '0724761213')
>>> c
???
>>> type(c)
???

```



_Remarques :_

* `c` est une `instance` de la classe `Contact` : c'est à lui que `self` se réfère.

* l'expression `c = Contact(...)` déclenche deux choses :

    * la création de l'objet `c` lui même.
    * le déclenchement de la méthode `__init_` chargée d'initialiser l'objet. Cette méthode particulière est appelée `initialisateur` . 

    



Notre objet a trois caractéristiques qu'on appelle `attributs`. Nous allons les définir :

```python
# -*- coding: utf-8 -*-
class Contact() :
    '''
    une classe pour définir un contact
    '''
    def __init__(self, nom_prenom = '', e_mail = '', tel = '') :
        '''
        initialise un contact
        : param nom_prenom (str)
        : email (str)
        : tel (str)
        '''
        assert(isinstance(nom_prenom, str)), 'nom_prenom doit être un str'
        assert(isinstance(e_mail, str)), 'e_mail doit être un str'
        assert(isinstance(tel, str)), 'tel doit être un str'
        self.nom = nom_prenom
        self.email = e_mail
        self.tel = tel
```



On peut accéder à ces `attributs` directement de la façon suivante :

```python
>>> c = Contact('Steve Boulot', 'steve.boulot@pomme.fr', '0724761213')
>>> c.nom
???
>>> c.email
???
>>> c.tel
???
```

Remarquez que, par défaut, les trois paramètres ont pour valeur des chaînes vide.

```python
>>> c = Contact()
>>> c.nom
???
>>> c.email
???
>>> c.tel
???
```



Nous avons défini des attributs publics... qui peuvent donc être modifiés par l'utilisateur :

```python
>>> c.nom = "Steve Boulot'
>>> c.nom
???
```



### Accesseurs et mutateurs

Comme nous l'avons déjà vu, nous allons utiliser des accesseurs pour atteindre les attributs de la classe et des mutateurs pour les modifier afin de déconnecter la structure choisie pour ces attributs, même si elle est simple ici, du reste du code. Si à l'avenir on décide de changer ces structures, seuls ces mutateurs et accesseurs seront à re-coder.



Les fonctions d'une classe qui s'appliquent sur une instance de la classe sont appelées `méthodes`.

*  Pour préciser qu'elle s'applique sur l'instance elle même, on le précise dans les paramètres, d'où la présence du `self`. 
* La méthode faisant partie de la classe, on la place après la méthode  `__init__`, sans omettre la tabulation.



> Complétez les accesseurs ci-dessous dans `Contact`.
>
> ```python
> # -*- coding: utf-8 -*-
> class Contact() :
>     '''
>     une classe pour définir un contact
>     '''
>     def __init__(self, nom_prenom, email, tel) :
>         '''
>         initialise un contact
>         : param nom_prenom (str)
>         : email (str)
>         : tel (str)
>         '''
>         assert(isinstance(nom_prenom, str)), 'nom_prenom doit être un str'
>         assert(isinstance(email, str)), 'email doit être un str'
>         assert(isinstance(tel, str)), 'tel doit être un str'
>         self.nom = nom_prenom
>         self.email = email
>         self.tel = tel
>         
>        def get_nom(self) :
>            '''
>            renvoie la valeur de l'attribut nom
>            : return (str)
>            '''
>            return self.nom
>  
>        def get_email(self):
>            '''
> 
>            '''
>      
>      
>        def get_tel(self):
>            '''
> 
>            '''
>      
> ```
>
> 

Testez dans la console :

```python
>>> c = Contact('Boulot Steve', 'steve.boulot@pomme.fr', '0724761213')
>>> c.get_nom()
???
>>> c.get_email()
???
>>> c.get_tel()
???
```



_Remarques :_

* une méthode s'appelle différemment d'une fonction :
    * L'objet sur laquelle elle s'applique est d'abord précisé (ici le `c`)
    * un `.` suit le nom de l'objet
    * ensuite seulement vient la méthode et ses éventuels paramètres (sauf le `self` qui est justement l'objet précisé devant la méthode, ici c'est donc `c` ).
* les accesseurs (et les mutateurs à venir) sont des `méthodes` , c'est à dire des fonctions qui s'appliquent sur des instances de la classe Contact. Elles s'appellent donc avec des parenthèses, contairement aux attributs.



> De la même façon, complétez les mutateurs ci-dessous :
>
> ```python
>        def set_nom(self, nouveau_nom) :
>            '''
>            modifie la valeur de l'attribut nom
>            : pas de return 
>            '''
>            assert(isinstance(nouveau_nom, str)), 'le nom doit être de type str'
>            self.nom = nouveau_nom
>  
>        def set_email(self, nouvel_email):
>            '''
> 
>            '''
>      
>      
>         def set_tel(self, nouveau_tel):
>             '''
> 
>             '''
>     
> ```
>
> 

Comme pour les accesseurs, faites des tests dans la console pour changer les valeurs de attributs du contact `c1` ci-dessous :

```python
>>> c = Contact('Boulot Steve', 'steve.boulot@pomme.fr', '0724761213')
>>> c.set_nom('Jobs Steve')
>>> c.get_nom()
???

```





### Afficher ou décrire une instance de Contact



On doit pouvoir afficher proprement un contact. Pour cela, on va coder une fonction dans la classe `Contact` qui s'appliquera sur une instance de cette classe, une `méthode` donc.



La méthode faisant partie de la classe, on la place à la suite du code sans omettre la tabulation :

```python
   
    def afficher(self) :
        '''
        renvoie une chaîne décrivant le contact 
        : return (str)
        '''
        return self.get_nom() + '\n' + 'email : ' + self.get_email() + '\n' + 'tel : ' + self.get_tel()
    

```

_Remarques :_

* Le `\n` est un caractère d'échappement permettant de passer à la ligne lorsqu'on interprête la chaîne de caractères avec la  `print`.

* Notez l'utilisation des accesseurs 

    

Testons : 

```python
>>> c = Contact('Steve Boulot', 'steve.boulot@pomme.fr', '0724761213')
>>> c.afficher()
???
>>> print(c.afficher)
???
```



Bon, c'est lourd ! Heureusement, il existe des méthodes spéciales ou réservées qui portent des noms particuliers et ont des tâches très spécifiques. Par exemples, notre `print` affiche la chaîne renvoyée par la méthode `__str__`. 

Renommons la méthode `afficher` en `__str__` :

```python
 def __str__(self) :
        '''
        afficher le contact dans la console
        '''
        return self.get_nom() + '\n' + 'email : ' + self.get_email() + '\n' + 'tel : ' + self.get_tel()
    
```





Testez :

```python
>>> c = Contact('Steve Boulot', 'steve.boulot@pomme.fr', '0724761213')
>>> print(c)
???
```



Et oui, le fameux `print` utilisé si souvent dans nos fonctions d'affichage et en réalité une méthode codée selon le type de l'objet à afficher.

Il existe de nombreuses méthodes de ce genre, nous en utiliserons selon nos besoin. En voici quelques unes parmi les plus importantes. 

|       définition        |   appel   |                       effet                        |
| :---------------------: | :-------: | :------------------------------------------------: |
|    `__str__(self)__`    | print(g)  | renvoie une chaîne de caractère décrivant l'objet. |
|     `__len(self)__`     |  len(g)   | renvoie un entier définissant la taille de l'objet |
|    `__repr(self)__`     |     g     |        renvoie une chaîne décrivant l'objet        |
| `__contains__(self, x)` |  g in x   |   renvoie `True` si x est dans g, `False` sinon    |
| `__getitem__(self , i)` |   g[i]    |           renvoie le i ème élément de g            |
|  ` __eq__(self, obj2)`  | g == obj2 |    renvoie `True` si g == obj2 et `False` sinon    |
| ` __leq__(self, obj2)`  | g < obj2  |    renvoie `True` si g < obj2 et `False` sinon     |
|  `__add__(self, obj2)`  | g + obj2  |         renvoie le résultat de l'addition          |
|  `__mul__(self, obj2)`  | g * obj2  |      renvoie le résultat de la multiplication      |



L'une de ces méthodes nous intéresse : `__repr__`. Elle renvoie une description de la classe. Utilisons la pour nos classe `Contact` :

```python
    def __repr__(self) :
        '''
        renvoie une chaîne qui décrit l'objet
        '''
        return 'Instance de la classe Contact'
```

Testez :

```python
>>> c = Contact('Steve Boulot', 'steve.boulot@pomme.fr', '0724761213')
>>> c
???
```



C'est tout de même plus parlant que le `<__main__.Contact object at 0x7f25d9e5fb80>` que nous obtenions auparavant.



### Publics ou privés

Il est possible de rendre des attributs ou  des méthodes privés. Pour cela, il suffit de les nommer en commençant par un double `underscore` :`__`. 

* Un attribut privé ne pourra être atteint par l'utilisateur de la classe.
* Une méthode privée ne pourra être lancer par l'utilisateur de la classe.



> En utilisant, via le menu de Thonny, la méthode du _Edition_ puis _rechercher et remplacer_, rendez les attributs privés. 

  



## La classe Carnet



### Initialisation de la classe



Comme pour la classe `Contact` on va initialiser notre classe `Carnet`. Ce sera très simple : cette classe ne possède qu'un seul attribut : un tableau vide qui, plus tard, accueillera les contacts.

```python
class Carnet():
	'''
	une classe pour un carnet
	'''
	def __init__(self):
        '''
        initialise la classe
        '''
        self.contacts = []
```

_Remarques :_

Il s'agit d'une nouvelle classe. Elle ne fait pas partie de la classe `Carnet`. On la code donc à sa suite, c'est pour cela que le code commence à la marge, sans tabulation.



### Accesseurs 

Comme précédemment, nous avons besoin d'accéder à l'attribut de la classe et de connaître le nombre de contacts qu'elle contient.



> Complétez les accesseurs ci-dessous :
> ```python
>     def get_contacts(self) :
>         '''
> 		renvoie une liste de contacts 
> 		: return (list)une liste de contacts
>         '''
> 
>      def get_nb_contacts(self) :
>          '''
>          renvoie le nbre de contacts présents dans l'Carnet
>          : return (int)
>          '''
> ```
>
> 

_Remarque :_

La méthode réservee `__len__(self)` renvoie le résultat de la fonction `len`. On peut l'ajouter pour pouvoir directement utiliser l'instruction `len(carnet)`.

```python
	def __len__(self) :
    	'''
      	renvoie le nbre de contacts présents dans l'Carnet
      	: return (int)
      	'''	
        return self.get_nb_contacts()
    
```

Testez dans la console :

```python
>>> carnet = Carnet()
>>> len(carnet)
???
```



### Décrire la classe



> Codez la méthode `__repr__` renvoyant une description de la classe `Carnet` sous forme suivante :
>
> ```python
> >>> a = Carnet()
> >>> a		
> Carnet contenant 0 contact(s)
> ```
>
> On utilisera les accesseurs.



### Mutateur

Il nous faut pouvoir ajouter ou retirer un contact.

Nous allons nommer ces méthodes comme celles que vous connaissez pour les `list`.



>  Complétez les méthodes ci-dessous :
>
> ```python
>     def append(self, contact) :
>         '''
>         ajoute le contact à carnet
>         : param contact (Contact)
>         : effet de bord sur self
>         '''
>         assert(isinstance(contact, Contact)), 'contact doit être une instance de Contact'
>         
>     def remove(self, contact):
>         '''
>         retire le contact du carnet
>         : param contact (Contact)
>         : effet de bord sur self
>         '''
>         assert(isinstance(contact, Contact)), 'contact doit être une instance de Contact'
>         
> ```
>
> 

Testons un peu :

```python
>>> c1 = Contact('Steve Boulot', 'steve.boulot@pomme.fr', '0724761213')
>>> c2 = Contact('Bill Porte', 'bill.porte@mirosoft.fr', '0700721317')
>>> a = Carnet()
>>> a.append(c1)
>>> a.get_contacts()
???
>>> a
???
>>> a.append(c2)
>>> a.get_contacts()
???
>>> a
???
>>> a.remove(c1)
>>> a
???
>>> a.get_contacts()
???

```

### Encore une méthode `__str__`

Ce serait bien pratique de pouvoir afficher les contacts de l'Carnet par ordre alphabétique. Pour cela, nous avons besoin d'une méthode `__str__` qui renvoie une chaîne de caractères permettant d'afficher le contenu de l'Carnet dans la console. 

Voici un rendu possible :

```python
>>> c1 = Contact('Boulot Steve', 'steve.boulot@pomme.fr', '0724761213')
>>> c2 = Contact('Porte Bill', 'bill.porte@grosoft.fr', '0700721317')
>>> a = Carnet()
>>> a.append(c1)
>>> a.append(c2)
>>> print(a)
Boulot Steve
email : steve.boulot@pomme.fr
tel : 0724761213
________________
Porte Bill
email : bill.porte@mirosoft.fr
tel : 0700721317
________________
```



>  Définissez, documentez et codez la méthode `__str__` de la classe `Carnet`.
>
> On utilisera la méthode `__repr__` de la classe `Contact` et l'accesseur de la classe `Carnet` !



### Prédicat



Il nous faut savoir si un contacts est présent ou non dans le carnet.

Nous avons donc besoin d'une méthode qui prendra en paramètre une instance de la classe `Contact`. Si, dans cette dernière est présente dans la liste des contacts alors la méthode renvoie `True` sinon, elle renverra `False`.

Il se trouve que cette méthode correspond au `in` que vous connaissez pour la classe `list` et qu'il s'agit de la méthode réservée `__contains__` vu plus haut.



> Complétez la méthode ci-dessous sans oublier l'assertion :
>
> ```python
>     def __contains__(self, personne) :
>         '''
>         renvoie True personne est dans le carnet
>         et False sinon
>         : param personne (Contact)
>         : return (boolean)
>         '''
>         assert(
>         
> ```
>



Testons :

```python
>>> c1 = Contact('Steve Boulot', 'steve.boulot@pomme.fr', '0724761213')
>>> c2 = Contact('Bill Porte', 'bill.porte@mirosoft.fr', '0700721317')
>>> a = Carnet()
>>> a.append(c1)
>>> c1 in a
???
>>> c2 in a
???
```



Revenons maintenant sur la méthode `append`.

> Modifiez la méthode `append` pour qu'elle n'ajoute le contact que si ce dernier n'est pas déjà dans le carnet et déclenche une exception sinon.
>
> _Remarque :_ on pourra utiliser une assertion où la commande `raise` pour déclencher une exception.



## Interface d'une classe

l'utilisateur de la classe doit connaître pour utiliser cette classe. Il faut éviter de la surcharger avec des méthodes dont il n'a pas besoin.



Dans la console, testez  :

```python
>>> help (Carnet)
???
```



>   
>
> Quelles sont les méthodes indispensables pour l'utilisateur de cette classe ?
>
>   



Pour éviter la surcharge, il faut rendre les méthodes inutiles pour l'utilisateur privées. Pour cela, nous avons déjà vu qu'il suffit de les nommer en commençant par un double `underscore` :`__`.



> En utilisant le menu _Édition_ puis _Rechercher et remplacer_ , renommer les méthodes qui surchargent l'interface de façon à les rendre privées.
>
> Dans la console, testez :
>
> ```python
> >>> help (Carnet)
> ???
> ```
>
> 





_________________

_Par Mieszczak Christophe, licence CC-BY-SA_





​    
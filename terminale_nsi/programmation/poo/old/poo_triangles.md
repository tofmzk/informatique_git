# Programmation Orienté Objet (POO)



Il existe différent paradigme de programmation :

* En seconde, on commence à programmer de manière impérative : les instructions se suivent les unes derrières les autres. Ce mode de programmation est utilisé pour des programmes simples car il devient vite lourd à gérer.
* L'année dernière, nous avons commencé à découper le code en petite fonction. Nous avons vu dans le chapitre [modularité](../modularite/modularite.md) tout l'intérêt de ce paradigme de programmation que l'on appelle le paradigme fonctionnel.
* Dans ce chapitre, nous allons aborder le paradigme _objet_. Il va nous permettre de définir de nouvelles structures de données, appelées `classes`, possédant des `attributs` et des `méthodes` .



## Classes, attributs et initialisation

Nous allons reprendre notre module `module_triangle` vu au chapitre précédent et le reprogrammer dans le paradigme objet.



Nous allons définir une `classe` que nous nommerons _Triangle_ . Remarquez la majuscule à _**T**riangle_. Elle n'est pas là par erreur:   **le nom d'une classe doit toujours commencer par une majuscule !**



Créez un **nouveau** programme `module_triangle.py` puis commençons notre code en déclarant notre objet. 

```python
# -*- coding: utf-8 -*-
'''
    Classe Triangle pour les triangles binaires
    : Auteur Mieszczak Christophe
'''

class Triangle():
    '''
    une classe pour les triangles  binaires
    '''


```



Il nous faut maintenant construire notre classe en utilisant la méthode` __init__`. Notez qu'on parle bien de `méthode` : il s'agit d'une fonction qui va s'appliquer sur l'objet que l'on va construire :



```python
# -*- coding: utf-8 -*-
'''
    Classe Triangle pour les triangles binaires
    : Auteurs Mieszczak Christophe
'''

class Triangle():
    '''
    une classe pour les triangles  binaires
    '''
    
    def __init__(self, ligne_depart):
        '''
        construit un triangle à partir de la ligne de départ
        : param ligne_depart contenant des '0' ou des '1'
        type str
        '''
        assert(isinstance(ligne_depart, str)), 'le paramètre est du type str'
        assert(len(ligne_depart) > 1), 'la première ligne contient au moins deux bits'
        for bit in ligne_depart :
            if bit != '0' and bit != '1' :
                raise 'le paramètre ne contient que des 0 ou des 1'

```



_Remarques :_

* remarquez la tabulation après la déclaration de la classe.  Tout ce qui concerne cette classe devra être ainsi tabulé.
* La méthode `__init__` a trois paramètres :
    * `self` fait référence à l'objet lui même, `instance` de la classe `Triangle`.
    * `ligne_depart` est la première ligne à partir de laquelle le triangle sera construit.
* Les assertions garantissent les pré-conditions.



Testez :

```python
>>> t = Triangle('01001')
>>> t
???
```



_Remarque :_

* t est une `instance` de la classe `Triangle` : c'est à lui que `self` se réfère.

* l'expression `g = grille(3, 3)` déclenche deux choses :

    * la création de l'objet `g` lui même.
    * le déclenchement de la méthode `__init_` chargée d'initialiser les attributs de l'objet. Cette méthode particulière est appelée `constructeur` .

    



Notre  objet a deux caractéristiques principales qu'ont appelle `attributs`: sa hauteur et sa largeur. Nous allons définir ces deux `attributs` :

```python
    def __init__(self, largeur = 3, hauteur = 3):
        '''
        initialise une grille vide de dimensions précisé
        : param longueur et largeur
        type int
        '''
        assert( (isinstance(largeur, int)
             and isinstance(hauteur, int)
             and largeur > 0 and hauteur >0)
            ), 'les paramètres sont des entiers strictement positifs'
        
        self.largeur = largeur
        self.hauteur = hauteur
```



On peut accéder à ces `attributs` directement de la façon suivante :

```python
>>> g = Grille(4,3)

>>> g.largeur
???
>>> g.hauteur
???

```



Nous avons défini des attributs publics... qui peuvent donc être modifiés par l'utilisateur :

```python
>>> g = Grille(4,3)
>>> g.largeur
???
>>> g.largeur += 1
>>> g.largeur 
???
```



Cela est parfois pratique mais c'est ici une source de problème !! Rendons ces attributs privés avec le classique double underscore `__` :

```python
def __init__(self, largeur = 3, hauteur = 3):
        '''
        initialise une grille vide de dimensions précisé
        : param longueur et largeur
        type int
        '''
        assert( (isinstance(largeur, int)
             and isinstance(hauteur, int)
             and largeur > 0 and hauteur >0)
            ), 'les paramètres sont des entiers strictement positifs'
        
        self.__largeur = largeur
        self.__hauteur = hauteur
```



Il faut maintenant choisir une structure de donnée pour notre grille, par exemple, comme nous l'avons fait en première, une liste de listes, une case vide contenant un 0.

Cependant, selon le principe d'`encapsulation` déjà vu pour les modules, il faut que la structure choisie pour l'objet soit _transparente_ pour l'utilisateur qui devra pouvoir _exploiter_ l'objet de la même façon quelque soit la structure choisie. Nous allons donc utiliser une structure _privée_ en faisant commencer son nom par `__` :



```python
    def __init__(self, largeur = 3, hauteur = 3):
        '''
        initialise une grille vide de dimensions précisé
        : param longueur et largeur
        type int
        '''
        assert( (isinstance(largeur, int)
             and isinstance(hauteur, int)
             and largeur > 0 and hauteur >0)
            ), 'les paramètres sont des entiers strictement positifs'
        
        self.__largeur = largeur
        self.__hauteur = hauteur
        self.__grille = []
        for _ in range(hauteur) :
            self.__grille.append([0] * largeur)
```



## Un premier de prédicat



Il serait bon de pouvoir vérifier si des coordonnées sont valides. Pour cela, on va réaliser la méthode `est_valide(self, x, y)` qui renvoie `True` si les coordonnées (x, y) sont valides et `False` sinon.

```python
 def est_valide(self, x, y):
        '''
        retourne True si le coup est valise et False sinon
        : param x,y
        type int
        '''
        return ((isinstance(x, int) 
                and isinstance(y, int)
                and x > -1
                and x< self.__largeur
                and y > -1
                and y < self.__hauteur
                )
               )
        
```

Testez :

```python
>>> g = Grille(3, 3)
>>> g.est_valide(-1, 0)
???
>>> g.est_valide(1,2)
???
>>> g.est_valide(3, 3)
???
```





_Remarque :_

* Une telle méthode qui renvoie Vrai ou Faux et donne une indication sur notre objet est appelée `prédicat`.
* le paramètre `self` de la `méthode` fait référence à l'objet, ici _g_ sur lequel elle s'applique. C'est pour cela qu'on appelle la `méthode` en tapant   `g.est_valide(x, y)` et non pas directement `est_valide(x, y)`.
* Cette méthode n'a pas besoin de faire partie de l'interface car elle n'est utile qu'en interne. On va donc la déclarer de façon privée.

```python
 def __est_valide(self, x, y):
        '''
        retourne True si le coup est valise et False sinon
        : param x,y
        type int
        : return 
        type Boolean
        '''
        return ((isinstance(x, int) 
                and isinstance(y, int)
                and x > -1
                and x< self.__largeur
                and y > -1
                and y < self.__hauteur
                )
               )
        
```





## Accesseur et mutateur



L'utilisateur n'aura pas accès à l'attribut `__grille`, puisqu'il est privé : il faut lui fournir un `accesseur`, c'est à dire une méthode permettant de lire dans la grille.



```python
 def lire(self, x, y):
        '''
        renvoie le contenu de la grille aux coordonnées (x, y)
        : param x,y
        type int
        '''
        assert(self.__est_valide(x,y)), 'les paramètres sont des entiers positifs ou nuls inférieurs aux dimensions de la grille'
        return self.__grille[y][x]
```

Testez :

```python
>>> g = Grille(4,3)
>>> g.lire(0,0)
???
>>> g.lire(3, 2)
???
>>> g.lire(4,3)
???
```



_Remarque :_

* L' assertion garantit une fois de plus les pré-conditions.

  ​    

Il faut maintenant définir une méthode permettant d'écrire dans la grille : c'est un `mutateur`.




>  De la même façon, définissez une méthode `ecrire(self, x, y, symbole)` permettant de remplacer le contenu de la grille aux coordonnées (x, y) par le symbole, un caractère, passé en paramètre. 
>
> N'oubliez ni la documentation ni les assertions.



Testez :

```python
>>> g = Grille(3, 3)
>>> g.ecrire(1, 2, 'x')
>>> g.lire(1, 2)
???
```



## Autres prédicats

Il serait bon de pouvoir vérifier si une _case_ de notre grille est vide ou pas, si la grille est pleine ou pas et si la partie est gagnée ou pas.



>  Réalisez les méthodes:
>
>  * `est_vide(self, x, y)` qui renvoie `True` si la case de la grille de coordonnées(x, y) est bien vide et `False` sinon.
>
>  * `est_pleine(self)`qui renvoie `True` si la grille est pleine et `False` sinon.
>
>  * `est_gagnee(self, joueur)` qui renvoie `True` si le _joueur_ a gagné la partie.
>
>      
>
>  > Ne pas oubliez les pré-conditions  !
>  >
>  > Utilisez les accesseurs !





> En procédant de cette façon :
>
> * Quelles méthodes faudrait-il changer si la structure de données utilisée pour notre grille change ?
> * Est-ce que cela changera quelque chose pour l'interface de l'objet pour l'utilisateur ?



## Méthodes réservées 

Il existe des méthodes dont les noms sont réservées. Nous en avons déjà vu une : `__init__`. Il y en a a d'autres dont voici quelques exemples :

| définition    | appel  | effet                                             |
| :-----------: | :----: | :-----------------------------------------------: |
| `__str__(self)__` | print(g) | renvoie une chaîne de caractère décrivant l'objet. |
| `__len(self)__` | len(g)|renvoie un entier définissant la taille de l'objet|
| `__repr(self)__` | g |renvoie une chaîne décrivant l'objet|
|`__contains(self, x)__` |g in x| renvoie `True` si x est dans g, `False` sinon|
|`__getitem(self , i)__`|g[i]|renvoie le i ème élément de g|
|` __eq(self, obj2)`| g == obj2| renvoie True si g == obj2 et False sinon|
|` __leq(self, obj2)`| g < obj2| renvoie True si g < obj2 et False sinon|
|`__add(self, obj2)__`| g + obj2|renvoie le résultat de l'addition|
|`__mul(self, obj2)__`| g * obj2|renvoie le résultat de la multiplication|



_Remarque :_ 

Pour des classes existantes, vous pouvez listez les méthodes réserver en utilisant la fonction `dir(classname)`:

```python
>>> dir(float)
???
>>> dir(list)
???
```





Ajoutons une méthode `__str__` qui servira à afficher la grille via un `print` en nous servant de la fonction réalisée l'année dernière :

```python
    def __str__(self):
        '''
        renvoie la chaine de caractères utilisée pour afficher l'objet via l'instruction print
        '''
        chaine = ''
        for y in range(self.__hauteur) :
            chaine += '+-' * self.__largeur + '+' + '\n'
            for x in range(self.__largeur) :
                if self.lire(x, y) == 0 :
                    contenu = ' '
                else :
                    contenu = str(self.lire(x, y))
                chaine += '|' + contenu 
            chaine +='|\n'
        chaine += '+-' * self.__largeur + '+' + '\n'
        return chaine
```



_Remarque :_

Pour rappel, le `\n` est un caractère d'échappement permettant de passer à la ligne.



Testons un peu :

```python
>>> g = Grille(3, 3)
>>> print(g)
???
>>> g.ecrire(1, 2, 'x')
>>> print(g)
???
>>> g
???
```



> Définissez une méthode `__repr(self)__` renvoyant une chaîne de caractère décrivant l'objet comme une grille de dimensions précisées.
>
> Le but est d'obtenir ceci, par exemple :
>
> ```python
> >>> g = grille(3, 3)
> >>> g
> ???
> ```
>
> 



## Que devient le jeu du morpion ?



Reste à coder le jeu en lui même dans un fichier _deroulement_jeu.py_.

En reprenant le code déjà fait l'année dernière avec le paradigme fonctionnel, on a besoin de trois fonctions :

* Une pour demander le coup du joueur en vérifiant sa validité
* Une pour changer de joueur
* Une pour le déroulement du jeu



Pour gagner un peu de temps, voici le début du code de ce programme, encore une fois issu du projet réalise en première. Lisez le, comprenez le et  complétez le !!



```python
# -*- coding: utf-8 -*-
'''
    déroulement du jeu de grille
    : Auteurs noms, prénoms
'''

import module_grille

def demander_coup(ma_grille, no_joueur):
    '''
    demande un coup au joueur sous la forme x, y
    : param ma_grille
    type Grille
    : param no_joueur 0 ou 1
    type int
    : return le coup joué
    type tuple
    '''
    x, y = -1, -1
    while (not ma_grille.est_valide(x, y)
           or not ma_grille.est_vide(x, y)
           ):
        coord = input('joueur ' + str(no_joueur) +' votre coup x,y : ')
        x, y = int(coord[0]), int(coord[2])
    return x, y

def changer_joueur(no_joueur):
    '''
    change le joueur en cours
    : param no_joueur 0 ou 1
    type int
    : return 0 ou 1
    type int
    '''
    return (no_joueur + 1) % 2

def jouer():
    '''
    déroule une partie tant que la grille n'est pas pleine ou la parti gagnée
    puis annonce le résultat
    '''
    symboles = ['X', 'O']
    no_joueur = 0
    ma_grille = module_grille.Grille(3, 3)
    


```





## Héritage



**Cette partie est hors-programme**.... Cependant il s'agit d'une des principales caractéristiques de la POO. En voici donc un bref aperçu...



Nous venons de définir un objet permettant de jouer à un jeu de grille. Cependant, si l'on veut jouer au puissance 4, il nous faudra une grille particulière qui hérite des attributs et méthode de la classe `Grille` mais on en également d'autres qui lui sont propres.



Créez un programme _module_p4.py_.



```python
# -*- coding: utf-8 -*-
'''
    jeu du puissance4
    : Auteurs noms, prénoms
'''

```



Nous allons définir une classe `P4`  qui _hérite_ de la classe `Grille` :

```python
# -*- coding: utf-8 -*-
'''
    module pour le morpion : définition de la classe P4
    : Auteurs noms, prénoms
'''

import module_grille

class P4(module_grille.Grille):
    '''
    une classe pour le jeu du morpion héritée de la classe Grille
    '''
```



Le constructeur de notre classe `P4` sera le même que celui de la classe `Grille` a une différence près : ses dimensions seront fixées à 6 * 7.

Pour ce faire, on va appeler la méthode `__init__` de la classe parent `Grille` en utilisant `super()` :

```python
# -*- coding: utf-8 -*-
'''
    module pour le morpion : définition de la classe P4
    : Auteurs noms, prénoms
'''

import module_grille

class P4(module_grille.Grille):
    '''
    une classe pour le jeu du morpion héritée de la classe Grille
    '''
    def __init__(self):
        super().__init__(6, 7) # apelle la méthode de la classe parente, c'est à dire la classe Grille
```

Testons cela :

```python
>>> m = P4()
>>> m
???
>>> print(m)
???
>>> m.ecrire(1, 6, 'X')
>>> print(m)
???
```



Remarques :

* C'est fait : nous avons défini une nouvelle classe 
* la classe `P4` possède les attributs et les méthodes de sa classe parent.



### Méthode particulière de la classe `P4`



_Remarque :_

On est possible  :

* d'ajouter de nouvelles méthodes non présentent dans la classe parente si besoin.
* de surcharger une méthode existante c'est à dire la reprendre mais avec des paramètres supplémentaires.





> 
>
> Quelles méthodes de la classe `Grille` doivent être modifiées pour la classe `P4` ?
>
> 



Il suffit, pour les remplacer, de coder dans la classe `P4` une méthode qui porte le même nom et utilise les mêmes paramètres que la méthode de la classe parente, la classe `Grille`.



Cette méthode écrasera alors celle de la classe parent lors de l'appel.

```Python
class P4(Grille) :
    '''
    une classe pour jouer au puissance 4
    '''
    def __init__(self):
        super().__init__(6, 7)
        
    def ecrire(self, x, symbole):
        '''
        ecrit, si possible, le symbole dans la plus basse ligne de la colonne x
        '''
        assert(self.est_valide(x, 0)), 'le paramètre est un entier entre 0 et 5'
        y = 0
        while y < 7 and self.est_vide(x, y) :
            y += 1
        super().ecrire(x, y - 1 , symbole) 
        
    
    def est_gagnee(self, symbole):
        '''
        renvoie True si 4 symboles sont alignés dans la grille et False sinon
        : param symbole
        type str
        '''
        #verticalement
        for x in range(6):
            colonne = ''
            for y in range(7):
                colonne += str(self.lire(x, y))
            if symbole * 4 in colonne :
                return True
        #horizontalement
        for y in range(7):
            ligne = ''
            for x in range(6):
                ligne += str(self.lire(x, y))
            if symbole * 4 in ligne :
                return True
        # diagonale 1
        for x in range(6):
            for y in range(7):
                diagonale = ''
                x_depart = x
                y_depart = y
                #vers haut-droite
                while x_depart < 6 and y_depart > -1 :
                    diagonale += str(self.lire(x_depart, y_depart))
                    x_depart += 1
                    y_depart -= 1
                # vers bas-gauche
                x_depart = x
                y_depart = y
                while x_depart > -1 and y_depart < 7 :
                    diagonale = str(self.lire(x_depart, y_depart)) + diagonale
                    x_depart -= 1
                    y_depart += 1
                if symbole * 5 in diagonale :
                    return True
        # diagonale 2
        for x in range(6):
            for y in range(7):
                diagonale = ''
                x_depart = x
                y_depart = y
                #vers haut-gauche
                while x_depart > -1 and y_depart > -1 :
                    diagonale += str(self.lire(x_depart, y_depart))
                    x_depart -= 1
                    y_depart -= 1
                # vers bas-droite
                x_depart = x
                y_depart = y
                while x_depart < 6 and y_depart > -1 :
                    diagonale = str(self.lire(x_depart, y_depart)) + diagonale
                    x_depart += 1
                    y_depart -= 1
                if symbole * 5 in diagonale :
                    return True
        return False
        
```









### Quels intérêts ?



* Définir une nouvelle classe facilement et rapidement
* En supposant la classe parente correcte, seules les nouvelles méthodes devront être déboguées et correctes. Cela évite donc de créer des erreurs supplémentaires.
* En cas de modification du cahier des charges, faire évoluer le code est beaucoup plus facile.



> Supposons la classe `P4` terminée.
>
> * Quelle(s) modification(s) faudrait-il apporter au code du __deroulement_jeu.py_._ pour jouer au puissance_4 ?
> * Si on veut jouer au puissance_4 sur une grille de dimension quelconque, quelle modification faudra-t-il apporter ?







_________________

_Par Mieszczak Christophe, licence CC-BY-SA_





​    
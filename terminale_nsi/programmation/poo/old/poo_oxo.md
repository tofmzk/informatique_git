# Programmation Orienté Objet (POO)

## Différents paradigmes



Il existe différents paradigmes de programmation :

* En seconde, on commence à programmer avec le [paradigme impératif](https://fr.wikipedia.org/wiki/Programmation_imp%C3%A9rative) : les instructions se suivent les unes derrières les autres. Ce mode de programmation est utilisé pour des programmes simples car il devient vite lourd à gérer, écrire et déboguer. 
* L'année dernière, nous avons commencé à découper le code en petites fonctions. Nous avons insisté, dans le chapitre [modularité](../modularite/modularite.md), sur l'intérêt de  faire cela. Le paradigme de programmation que l'on appelle le [paradigme fonctionnel](https://fr.wikipedia.org/wiki/Programmation_fonctionnelle) fait cela en évitant à tout prix deux choses :
    * les variables globales : toute variable doit être définie dans une fonction est, donc, locale.
    * les effets de bord : une fonction ne doit pas en générer dans ce paradigme.
* Nous avons également programmé avec le [paradigme événementiel](https://fr.wikipedia.org/wiki/Programmation_fonctionnelle) dans le chapitre [interactions homme - machine sur le web](../../../premiere_nsi/interactions_homme_machine_web/evenements_et_pages_web.md) où les événements qui se produisent dans une page web (clic, mouvement de souris, ...) déclenchent des fonctions.
* Dans ce chapitre, nous allons aborder le `paradigme objet`. Il va nous permettre de définir de nouvelles structures de données, appelées `classes`, possédant des `attributs` et des `méthodes` .



## Classes, attributs et initialisation

L'année dernière, nous avons réalisé plusieurs projets basés sur des jeux de grille (morpion, jeu de la vie par exemple). De nombreux jeux se basent sur ce principe : puissance_4, othello, echec, dames ....



Nous allons définir une `classe` que nous nommerons _Grille_ et qui sera un `objet`utilisable pour le jeu du _OXO_ ou _TicTacToe_. Remarquez la majuscule à _**G**rille_. Elle n'est pas là par erreur: le nom d'une classe doit toujours commencer par une majuscule !



Cette `classe` devra fournir une `interface` permettant de jouer au _OXO_ . L'utilisateur devra pouvoir :

* _Construire_ une grille vide de dimension 3*3

* _jouer un coup_ dans une case, à condition que le coup soit valide bien entendu.

* _afficher_ la grille

* _savoir_ si la partie est terminée :

    * soit parce que la grille est pleine.

    * soit parce qu'un joueur a gagné.

        



Pour cela, nous auront besoin de fonctions annexes pour :

* savoir si les coordonnées proposées pour jouer sont bien dans la grille
* savoir si une case est vide ou pas
* savoir si un coup est valide, c'est à dire vérifie les deux conditions précédentes
* savoir si la grille est pleine
* savoir si un joueur a gagné
* lire une valeur dans la grille
* écrire une valeur dans la grille





Créez un programme `module_grille.py` puis commençons notre code en déclarant notre objet :

```python
# -*- coding: utf-8 -*-
'''
    module pour jeux de grille : définition de la classe Grille pour le OXO .. et plus
    : Auteurs noms, prénoms
'''

class Grille():
    '''
    une classe pour représenter une grille de OXO ... et plus
    '''
```



Il nous faut maintenant  initialiser notre classe en utilisant une méthode (c'est à dire une fonction spécifique à la classe). La syntaxe est un peu particulière car il s'agit d'une méthode spécifiquement utilisée pour initialiser la classe :

```python
class Grille():
    '''
    une classe pour représenter une grille de OXO ... et plus
    '''
    def __init__(self, largeur = 3, hauteur= 3):
        '''
        initialise une grille vide de dimensions précisé (3*3 par défaut)
        : param longueur et largeur
        type int
        '''
        assert( (isinstance(largeur, int)
             and isinstance(hauteur, int)
             and largeur > 0 and hauteur >0)
            ), 'les paramètres sont des entiers strictement positifs'
```



_Remarques :_

* remarquez la tabulation après la déclaration de la classe.  Tout ce qui concerne cette classe devra être ainsi tabulé.
* La méthode `__init__` a trois paramètres :
    * `self` fait référence à l'objet lui même, `instance` de la classe `Grille`.
    * `largeur` et `hauteur` sont les paramètres de la méthode `__init__`qui indiquent les dimensions de la grille.
* Les assertions garantissent les pré-conditions.



Testez :

```python
>>> g = Grille()
>>> g
???
```



_Remarque :_

* g est une `instance` de la classe `Grille` : c'est à lui que `self` se réfère.

* l'expression `g = grille()` déclenche deux choses :

    * la création de l'objet `g` lui même.
    * le déclenchement de la méthode `__init_` chargée d'initialiser les attributs de l'objet. Cette méthode particulière est appelée `constructeur` .

    



Notre  objet a deux caractéristiques principales qu'ont appelle `attributs`: sa hauteur et sa largeur. Nous allons définir ces deux `attributs` :

```python
    def __init__(self, largeur = 3, hauteur = 3):
        '''
        initialise une grille vide de dimensions précisé
        : param longueur et largeur
        type int
        '''
        assert( (isinstance(largeur, int)
             and isinstance(hauteur, int)
             and largeur > 0 and hauteur >0)
            ), 'les paramètres sont des entiers strictement positifs'
        
        self.largeur = largeur
        self.hauteur = hauteur
```



On peut accéder à ces `attributs` directement de la façon suivante :

```python
>>> g = Grille()

>>> g.largeur
???
>>> g.hauteur
???

```



Nous avons défini des attributs publics... qui peuvent donc être modifiés par l'utilisateur :

```python
>>> g = Grille(4,3)
>>> g.largeur
???
>>> g.largeur += 1
>>> g.largeur 
???
```



Cela est parfois pratique mais c'est ici une source de problème !! Rendons ces attributs privés avec le classique double underscore `__` :

```python
def __init__(self, largeur = 3, hauteur = 3):
        '''
        initialise une grille vide de dimensions précisé
        : param longueur et largeur
        type int
        '''
        assert( (isinstance(largeur, int)
             and isinstance(hauteur, int)
             and largeur > 0 and hauteur >0)
            ), 'les paramètres sont des entiers strictement positifs'
        
        self.__largeur = largeur
        self.__hauteur = hauteur
```



Il faut maintenant choisir une structure de donnée pour notre grille, par exemple, comme nous l'avons fait en première, une liste de listes, une case vide étant représentée par un 0.

Cependant, selon le principe d'`encapsulation` déjà vu pour les modules, il faut que la structure choisie pour l'objet soit _transparente_ pour l'utilisateur qui devra pouvoir _exploiter_ l'objet de la même façon quelque soit la structure choisie. Nous allons donc utiliser une structure _privée_ en faisant commencer son nom par `__` :



```python
    def __init__(self, largeur = 3, hauteur = 3):
        '''
        initialise une grille vide de dimensions précisé
        : param longueur et largeur
        type int
        '''
        assert( (isinstance(largeur, int)
             and isinstance(hauteur, int)
             and largeur > 0 and hauteur >0)
            ), 'les paramètres sont des entiers strictement positifs'
        
        self.__largeur = largeur
        self.__hauteur = hauteur
        self.__grille = []
        for _ in range(hauteur) :
            self.__grille.append([0] * largeur)
```



## Un premier de prédicat



Il serait bon de pouvoir vérifier si des coordonnées sont valides. Pour cela, on va réaliser la méthode `est_dans_grille(self, x, y)` qui renvoie `True` si les coordonnées (x, y) passées en paramètre ne sont pas hors de la grille et `False` sinon.

```python
 def est_dans_grille(self, x, y):
        '''
        retourne True si le coup est dans la grille et False sinon
        : param coordonnee
        type tuple
        '''
        return (isinstance(x, int) 
                and isinstance(y, int)
                and x > -1
                and x< self.__largeur
                and y > -1
                and y < self.__hauteur
                )          
        
```

Testez :

```python
>>> g = Grille()
>>> g.est_dans_grille(-1, 0)
???
>>> g.est_dans_grille(1, 2)
???
>>> g.est_dans_grille(3, 3)
???
```





_Remarque :_

* Une telle méthode qui renvoie Vrai ou Faux et donne une indication sur notre objet est appelée `prédicat`.
* le paramètre `self` de la `méthode` fait référence à l'objet, ici _g_ sur lequel elle s'applique. C'est pour cela qu'on appelle la `méthode` en tapant   `g.est_valide(x, y)` et non pas directement `est_valide(x, y)`.
* Cette méthode n'a pas besoin de faire partie de l'interface car elle n'est utile qu'en interne. On va donc la déclarer de façon privée.

```python
 def __est_dans_grille(self, x, y):
        '''
        retourne True si le coup est valise et False sinon
        : param x,y
        type int
        : return 
        type Boolean
        '''
        return ((isinstance(x, int) 
                and isinstance(y, int)
                and x > -1
                and x< self.__largeur
                and y > -1
                and y < self.__hauteur
                )
               )
        
```





## Accesseur et mutateur



Nous allons définir `accesseur`, c'est à dire une méthode permettant de lire une case de la grille qui rendra le reste du code indépendant de l'implantation choisie pour la grille.



```python
 def lire(self, x, y):
        '''
        renvoie le contenu de la grille aux coordonnées (x, y)
        : param x,y
        type int
        '''
        assert(self.__est_dans_grille(x,y)), 'les paramètres sont des entiers positifs ou nuls inférieurs aux dimensions de la grille'
        return self.__grille[y][x]
```



_Remarque :_

* L' assertion garantit une fois de plus les pré-conditions.

  

Testez :

```python
>>> g = Grille()
>>> g.lire(1, 2)
???
```





Il faut maintenant définir une méthode permettant d'écrire dans la grille : c'est un `mutateur`.




>  De la même façon, définissez une méthode `__ecrire(self, x, y, symbole)` permettant de remplacer le contenu de la grille aux coordonnées (x, y) par le symbole, un caractère, passé en paramètre. 
>
> N'oubliez ni la documentation ni les assertions.



Testez :

```python
>>> g = Grille()
>>> g.ecrire(1, 2, 'x')
>>> g.lire(1, 2)
???
```



## Autres prédicats



>  Réalisez les méthodes:
>
>  * `est_vide(self, x, y)` qui renvoie `True` si la case de la grille de coordonnées(x, y) est bien vide et `False` sinon.
>
>  * `est_jouable(self, x, y)`qui renvoie `True` s'il est possible de jouer en (x, y ) et `False` sinon.
>
>  * `est_pleine(self)`qui renvoie `True` si la grille est pleine et `False` sinon. 
>
>  * `est_gagnee(self, symbole)`  où symbole est un caractère, qui renvoie `True` si 3 symboles, sont alignés dans la grille.
>
>      
>
>  > Ne pas oubliez les docstrings  !
>  >
>  > Utilisez les accesseurs !



### Jouons !

On va considérer que la grille est numérotée ainsi :

```python
+-+-+-+
|0|1|2|
+-+-+-+
|3|4|5|
+-+-+-+
|6|7|8|
+-+-+-+
```





> Définissez la méthode `jouer(self, no_case, symbole)` où _no_case_ est un entier entre 0 et 8 :
>
> * si le coup est jouable, place le symbole, c'est à dire un caractère, dans la case correspondante **et** renvoie `True`
> * sinon renvoie `False` 



Allez, un peu d'aide :

```python
def jouer(self, numero_case, symbole):
        '''
        s'il est possible de jouer dans la case no n, modifie le contenu de la grille aux coordonnées 
        en y insérant le symbole et renvoie TRUE. sinon, ne modifie rien et renvoie False
        : param no_case est le no de la case à jouer
        : param symbole un caractère
        type str
        : return
        type boolean
        '''        
        x, y = numero_case % self.__largeur , numero_case // self.__largeur

```







## Méthodes réservées 

Il existe des méthodes dont les noms sont réservées. Nous en avons déjà vu une : `__init__`. Il y en a a d'autres dont voici quelques exemples :

| définition    | appel  | effet                                             |
| :-----------: | :----: | :-----------------------------------------------: |
| `__str__(self)__` | print(g) | renvoie une chaîne de caractère décrivant l'objet. |
| `__len(self)__` | len(g)|renvoie un entier définissant la taille de l'objet|
| `__repr(self)__` | g |renvoie une chaîne décrivant l'objet|
|`__contains(self, x)__` |g in x| renvoie `True` si x est dans g, `False` sinon|
|`__getitem(self , i)__`|g[i]|renvoie le i ème élément de g|
|` __eq(self, obj2)`| g == obj2| renvoie `True` si g == obj2 et `False` sinon |
|` __leq(self, obj2)`| g < obj2| renvoie `True` si g < obj2 et `False` sinon |
|`__add(self, obj2)__`| g + obj2|renvoie le résultat de l'addition|
|`__mul(self, obj2)__`| g * obj2|renvoie le résultat de la multiplication|



_Remarque :_ 

Pour des classes existantes, vous pouvez listez les méthodes réserver en utilisant la fonction `dir(classname)`:

```python
>>> dir(float)
???
>>> dir(list)
???
```





Ajoutons une méthode `__str__` qui servira à afficher la grille via un `print` en nous servant de la fonction réalisée l'année dernière :

```python
	def __str__(self):
        '''
        renvoie la chaine de caractères utilisée pour afficher l'objet via l'instruction print
        '''
        chaine = ''
        for y in range(self.__hauteur) :
            chaine += '+-' * self.__largeur + '+' + '\n'
            for x in range(self.__largeur) :
                contenu = str(self.lire(x, y))
                if contenu == '0' :
                    contenu = ' '
                chaine += '|' + contenu
            chaine +='|\n'
        chaine += '+-' * self.__largeur + '+' + '\n'
        return chaine
```



_Remarque :_

Pour rappel, le `\n` est un caractère d'échappement permettant de passer à la ligne.



Testons un peu :

```python
>>> g = Grille()
>>> print(g)
???
>>> g.jouer(4, 'x')
>>> print(g)
???
>>> g
???
```



> Définissez une méthode `__repr(self)__` renvoyant une chaîne de caractère décrivant l'objet comme une grille de dimensions précisées.
>
> Le but est d'obtenir ceci :
>
> ```python
> >>> g = grille()
> >>> g
> Objet de classe Grille de dimensions 3*3
> ```
>
> 



## Déroulons



Reste à coder le jeu en lui même dans un fichier _deroulement_jeu.py_.



Le déroulement du jeu est le suivant :

* on initialise le joueur en cours et les symboles utilisés ('X' et 'O' par exemple)
* tant que la grille n'est pas pleine et que le joueur en cours n'a pas gagné :
    * on demande le coup du joueur
    * si le coup est valide alors on joue ce coup, sinon, on redemande
    * si le joueur n'a pas gagné on change de joueur
* si le joueur a gagné on annonce sa victoire sinon on annonce un match nul



Pour gagner un peu de temps, voici le début du code de ce programme, encore une fois issu du projet réalise en première. Lisez le, comprenez le et  complétez le !!



```python
# -*- coding: utf-8 -*-
'''
    deroulement du jeu
    : Auteurs mieszczak christophe
'''

import module_grille

def jouer():
    '''
    déroule une partie tant que la grille n'est pas pleine ou la partie gagnée
    puis annonce le résultat
    '''
    symbole = ['X', 'O']
    no_joueur = 0
    ma_grille = module_grille.Grille()
    while not ma_grille.est_pleine() and ...
```





## Héritage



**Cette partie est hors-programme**.... Cependant il s'agit d'une des principales caractéristiques de la POO. En voici donc un bref aperçu...



Nous venons de définir un objet permettant de jouer à un jeu de grille. Cependant, si l'on veut jouer au puissance 4, il nous faudra une grille particulière qui hérite des attributs et méthodes de la classe `Grille` mais en possède également d'autres qui lui sont propres.



A la suite de `module_grille`, définissons une nouvelle classe `P4` , classe _fille_ de la classe `Grille`, son _parent_ dont elle _hérite_.

Le constructeur de notre classe `P4` sera le même que celui de la classe `Grille` a une différence près : ses dimensions seront fixées à 6 * 7.

```python
class P4(Grille) :
    '''
    une classe pour jouer au puissance 4
    '''
    def __init__(self):
        super().__init__(7, 6) #super() permet d'utiliser la méthode de la classe parente
```



Testons cela :

```python
>>> p = P4()
>>> p
???
>>> print(p)
???
>>> help(p)
???
```



Remarques :

* C'est fait : nous avons défini une nouvelle classe 
* la classe `P4` _hérite_ les attributs et les méthodes de sa classe parent.



### Méthode particulière de la classe `P4`

OK, mais jouer au puissance 4 requiert quelques modifications ...

Il est possible :

* d'ajouter de nouvelles méthodes ou attributs non présentent dans la classe parente si besoin.
* de surcharger une méthode existante c'est à dire la reprendre mais avec des paramètres différents.





> 
>
> Quelles méthodes de la classe `Grille` doivent être modifiées pour la classe `P4` ?
>
> 



Puisque nous sommes hors programme, voici les méthodes modifiées :

```python
class P4(Grille) :
    '''
    une classe pour jouer au puissance 4
    '''
    def __init__(self):
        super().__init__(7, 6)
        
    def jouer(self, x, symbole):
        '''
        si possible, écrit le symbole dans la plus basse ligne de la colonne x et renvoie True
        sinon, renvoit false
        : return 
        type boolean
        MODIFIE la grille de jeu
        '''
        if self.est_jouable(x, 0):
            y = 1
            while self.est_jouable(x, y) :
                y += 1
            print(x+(y-1)*7)
            return super().jouer(x + (y - 1) * 7 , symbole)
        else :
            return False
        
    def __verifie_horizontal(self, symbole):
        '''
        renvoie True si 4 symboles sont alignés horizontalement
        dans la grille et False sinon
        : param symbole
        type str
        '''
        #horizontalement
        for y in range(6):
            ligne = ''
            for x in range(7):
                ligne += str(self.lire(x, y))
            if symbole * 4 in ligne :
                return True
        return False
    
    def __verifie_vertical(self, symbole):
        '''
        renvoie True si 4 symboles sont alignés dans la grille et False sinon
        : param symbole
        type str
        '''
        #verticalement
        for x in range(7):
            colonne = ''
            for y in range(6):
                colonne += str(self.lire(x, y))
            if symbole * 4 in colonne :
                return True
        return False
    
    def __verifie_diagonales(self, symbole):
        '''
        renvoie True si 4 symboles sont alignés diagonalement
        dans la grille et False sinon
        : param symbole
        type str
        '''
        # diagonale 1
        for x in range(7):
            for y in range(6):
                diagonale = ''
                x_depart = x
                y_depart = y
                #vers haut-droite
                while x_depart < 7 and y_depart > -1 :
                    diagonale += str(self.lire(x_depart, y_depart))
                    x_depart += 1
                    y_depart -= 1
                # vers bas-gauche
                x_depart = x
                y_depart = y
                while x_depart > -1 and y_depart < 6 :
                    diagonale = str(self.lire(x_depart, y_depart)) + diagonale
                    x_depart -= 1
                    y_depart += 1
                if symbole * 5 in diagonale :
                    return True
        # diagonale 2
        for x in range(7):
            for y in range(6):
                diagonale = ''
                x_depart = x
                y_depart = y
                #vers haut-gauche
                while x_depart > -1 and y_depart > -1 :
                    diagonale += str(self.lire(x_depart, y_depart))
                    x_depart -= 1
                    y_depart -= 1
                # vers bas-droite
                x_depart = x
                y_depart = y
                while x_depart < 7 and y_depart > -1 :
                    diagonale = str(self.lire(x_depart, y_depart)) + diagonale
                    x_depart += 1
                    y_depart -= 1
                if symbole * 5 in diagonale :
                    return True
        return False
        
    def est_gagnee(self, symbole):
        '''
        renvoie True si 4 symboles sont alignés verticalement
        dans la grille et False sinon
        : param symbole
        type str
        '''
        return (self.__verifie_horizontal(symbole)
                or self.__verifie_vertical(symbole)
                or self.__verifie_diagonales(symbole)
                )
    
```



### Quels intérêts ?



* Définir une nouvelle classe facilement et rapidement.
* En supposant la classe _parent_ correcte, seules les nouvelles méthodes devront être déboguées et correctes. Cela évite donc de créer des erreurs supplémentaires.
* En cas de modification du cahier des charges, faire évoluer le code est beaucoup plus facile.
* le _déroulement du jeu_ ne changera quasiment pas pour jouer à puissance 4.



> 
>
> Quelle(s) modification(s) faudrait-il apporter au code du __deroulement_jeu.py_._ pour jouer au puissance 4 ?
>
> 







_________________

_Par Mieszczak Christophe, licence CC-BY-SA_





​    
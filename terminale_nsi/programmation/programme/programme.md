# Programme, calculabilité, décidabilité

## Introduction



Comme nous l'avons déjà vu, on peut considérer [Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing) comme le père de l'informatique, née des suite de ses travaux entre 1930 et 1940, même si bien d'autres, comme [Van Neuman](https://fr.wikipedia.org/wiki/John_von_Neumann), qui a donné son nom à [l'architecture](../../../premiere_nsi/architecture/microprocesseurs/architecture_van_neuman.md) des ordinateurs toujours utilisée aujourd'hui, ou [Alonso Church]() ont participé à son éclosion.



Alan Turing a beaucoup travaillé sur les problème de la calculabilité et de la décidabilité en arithmétique, deux problèmes étroitement liés.



## Notion de programme

Un programme est la description d'opérations à effectuer mécaniquement .



Nous avons vu en première que **tout programme est une lui même une donnée** qui peut être utilisée et modifiée par d'autres programmes :

* On peut voir un système d'exploitation comme un programme qui fait _tourner_ d'autres programmes, qui les _utilise_ en quelque sorte.
* Nous codons essentiellement en Python en NSI, un langage interprété. Le programme Python installé sur la machine utilise les données du code en les interprétant.
* Nous avons également parlé des compilateurs qui transforment le code de certains langages dits _compilés_, comme le célèbre _langage C_, en langage binaire compréhensible par la machine qui va l'exécuter.
* Pour télécharger un logiciel, on utilise un gestionnaire de téléchargement qui est lui-même un logiciel.
* En **1996**, un vol de la fusée _Ariane 5_ se termine en explosion après quelques secondes ... à cause d'un dysfonctionnement informatique. Depuis ce date, la France a développé de nombreux programmes permettant de valider d'autres programmes.



## Calculabilité

On peut intuitivement caractériser de nombreuses fonctions mathématiques par une suite de manipulations décrites par des combinaisons de symboles :

* une multiplication est le résultats d'additions successives 
* une division se ramène une succession de multiplications et de soustractions
* ...



Peut-on toujours décomposer toute fonction de cette façon ? 

* En 1930,  [Alonso Church]() identifie une classe de fonctions mathématiques que l'on peut décomposer et qui sont donc _calculables_.

* En 1936, [Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing) présente la machine qui porte son nom. Nous en avons déjà parlé l'année dernière  [ICI](../../../premiere_nsi/architecture/microprocesseurs/architecture_van_neuman.md). Il montre que l'ensemble des  fonctions calculables de Church sont identiques à l'ensemble des fonctions programmables sur sa machine.



>  La **thèse de Church-Turing** affirme que tout traitement réalisable mécaniquement peut être accompli par une machine de Turing : **tout programme d'ordinateur, peu importe le langage dans lequel il est écrit, peut donc être traduit en une machine de Turing.**
>
> Cette thèse formalise la notion de `calculabilité` : 
>
> Si on peut réaliser un programme sur la machine de Turing alors il est calculable.



Toutes les fonctions sont-elles calculables ? Et bien non. La plupart des fonctions ne le sont pas ! Nous allons étudier un cas particulier dans la partie suivante.



## Décidabilité

### Introduction



Un des cauchemar de développeur est de réaliser un algorithme qui, dans certaines conditions, ne se termine pas et donc boucle à l'infini.

En voici un exemple :

```python
def puissance(x, n):
    if n == 0 :
        return 1
    else :
        return x * puissance(x, n - 1)
```



> * Faites tourner cet algorithme à la main avec n = 3.
> * Réfléchissez aux conditions qui entraînent une boucle infini...





Le rêve serait de réaliser un programme capable de déterminer si un algorithme se termine ou pas : ce problème s'appelle le `problème de l'arrêt`.



### Décidabilité du problème de l'arrêt

#### Qu'est-ce que  ce truc là ?



> Une propriété est dite `décidable` s'il est existe une fonction calculable capable de dire en un temps raisonnable si elle est vraie ou fausse.



Nous allons procéder en raisonnant _par l'absurde_ pour démontrer que le problème de l'arrêt est indécidable, comme Turing l'a démontré en 1936.

Pour commencer, voici [une petite vidéo](https://youtu.be/92WHN-pAFCs) qui illustre cela.



#### Formalisons tout cela.



Supposons qu'il existe un algorithme _D_ capable de calculer le problème de l'arrêt, c'est à dire, pour tout algorithme _A_ de décider, en un temps fini de déterminer si _A_ s'arrête ou pas :



Pour tout A : $`D(A)   = \left\{ \begin{array}    ~ Vrai~ si~ A~ se~termine\\    Faux~ si~ A~ ne~ se ~termine~ pas \end{array}\right.`$



Considérons l'algorithme $`\overline D`$  qui, pour tout algorithme _A_, commence par exécuter $`D(A)`$ puis :

* lance une boucle sans fin si $`D(A)`$ renvoie Vrai c'est à dire si A se termine

* s'arrête si $`D(A)`$ renvoie Faux c'est à dire si A ne se termine pas.

    

 Ainsi , pour tout A : $`\overline D(A)   = \left\{ \begin{array}    ~ boucle~sans~fin~ si~ D(A) ~ renvoie ~ Vrai \\   se~termine~ si~ D(A)~ renvoie~ Faux \end{array}\right.`$

  

Que se passe-t-il alors lorsqu'on calcule $`\overline D(\overline D)`$ ?

$`\overline D(\overline D)   = \left\{ \begin{array}    ~ boucle~sans~fin~ si~ D(\overline D) ~ renvoie ~ Vrai \\   se~termine~ si~ D(\overline D)~ renvoie~ Faux \end{array}\right.`$



Il y a là une contradiction majeure :

* Si _D_ décide que $`\overline D`$ se termine alors ...
* Si _D_ décide que $`\overline D`$ ne se termine pas pas alors ...



Nous avons fait une supposition au départ. Celle-ci, après un raisonnement logique, aboutit à une conclusion absurde : la supposition de départ était donc fausse. Ainsi :



**L'algorithme capable de décider de la terminaison de tout algorithme est incalculable**



## Conséquence



Il ne fau pas espérer disposer un jour d'un algorithme capable, sans jamais se tromper, de corriger nos codes ou ne vérifier si ces derniers ne risquent pas de planter. En conséquence, il va nous falloir nous organiser de façon à limiter au maximum les risques d'erreur !









_________________

Par Mieszczak Christophe, licence CC-BY-SA.






# Récursivité



![récursivité](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Droste_Cacao_Alcalinise_blikje%2C_foto4.JPG/1200px-Droste_Cacao_Alcalinise_blikje%2C_foto4.JPG)

## Un exemple pour commencer



Le but ici est de calculer $`somme(n) = 0  + 1 + 2+ ... + n`$ où $`n \in \N`$



La première technique est d'utiliser une boucle. Il s'agit d'une `fonction itérative`. Allez -y :

```python
def somme(n):
    '''
    renvoie la somme 0+1+2+3+...+n avec n entier positif ou nul
    : param n
    type int
    : return la somme
    type int
    '''
    
```



Il y a une autre façon de voir les choses. on peut définir la somme de la façon suivante :

$`somme(n) = \left\{ \begin{array}{ll}     0 {~si~} n = 0\\     n + somme(n -1){~sinon.} \end{array}\right.`$



De cette façon : 

* $`somme(0) = 0`$

* $`somme(1) = somme(0) + 1 = 0 + 1 = 1`$

* $`somme(2) = somme(1) + 2 = 1 + 2 = 3`$

* $`somme(3) =`$

    

> * La définition de $`somme(n)`$ dépend de $`somme(n - 1)`$ : c'est une définition `récursive` .
> * Pour pouvoir calculer` somme(n)` il faut impérativement connaître le `cas de base` (aussi appelé `cas d'arrêt`) :$`somme(0) = 0`$ qui garantit la terminaison de l'algorithme.



Cette façon nouvelle de voir les choses s'implémente assez facilement en Python :



```python
def somme(n):
    '''
    renvoie la somme 0+1+2+3+...+n avec n entier positif ou nul
    : param n
    type int
    : return la somme
    type int
    '''
    if n == 0 : # on commence par le cas de base
        return 0
    else : 
        return somme(n - 1) + n
```

_Remarque :_ 

Une fonction récursive, contrairement au bon usage habituel dans une fonction, va le plus souvent avoir plusieurs `return` : au minimum un pour le cas de base (il peut y en avoir plusieurs) et un pour l'appel récursif (il peut y en avoir plusieurs également).



* Testez cette fonction avec différentes valeurs de n

```python
>>> somme(5)
???
>>> somme(100)
???
```



* Prenons maintenant une petite valeur de _n_, par exemple 3, et voyons ce qui se passe en exécutant le code pas à pas (en mode debogage) ...

    

* On réalise un appel récursif chaque fois que la fonction s'appelle elle même. Combien d'appels sont nécessaires pour calculer `somme(4)` ?



* Voyons ce qui se passe si on donne une valeur de n hors domaine de définition :

```python
>>> somme(-5)
???
>>> somme(3.4)
???
```



>  Notre définition n'est valable que pour $`n \in \N`$ . Pour d'autres valeurs, le cas de base pourrait ne jamais être atteint, ce qui empêche la récursion de se terminer. Python réalise donc un grand nombres d'appels récursifs jusqu'à atteindre une _profondeur_ de récursion trop importante qui déclenche une exception. 



_Remarque:_

On peut trouver et modifier la valeur maximale de la profondeur.

```python
>>> import sys
>>> sys.getrecursionlimit()
???
>>> sys.setrecursionlimit(4000) 
>>> sys.getrecursionlimit()
???
```



Pour éviter les erreurs de domaine, on peut contrôler le type et la plage dans laquelle se situe _n_, en utilisant des assertions :

```python
def somme(n):
	'''
    renvoie la somme 0+1+2+3+...+n avec n entier positif ou nul
    : param n
    type int
    : return la somme
    type int
    '''
    assert(isinstance(n, int)),'le paramètre doit être un entier'
    assert(n >= 0),'le paramètre doit être positif ou nul'
    if n == 0 :
        return 0
    else : 
        return somme(n - 1) + n
```



* Testez à nouveau :

```python
>>> somme(-5)
???
>>> somme(3.4)
???
```





## Une bonne définition récursive



Lorsque la définition récursive est bien posée, il est en général assez simple de l'implémenter en Python. Il faut toutefois respecter certaines règles pour réussir cette définition :

* Il faut être certain que la récursion va s'arrêter, c'est à dire être certain de tomber à un moment sur le cas de base.
* Il faut s'assurer que les paramètres de la fonction sont bien dans le domaine de définition prévu.
* Il faut également vérifier qu'il y a une définition pour chaque valeur du domaine de définition.



Allons maintenant faire les [exercices](exercices.md) 1 et 2.



## Cas plus complexes



### Multiples cas de base.



Reprenons l'exemple  $`somme(n) = 0  + 1 + 2+ ... + n`$ où $`n \in \N`$.

* Nous avons vu que si _n_ vaut 0 alors _somme(n)_ vaut 0.

* Si _n_ vaut 1 alors _somme(n)_ = 0 + 1 = 1 : on pourrait prendre un deuxième cas de base pour éviter ce calcul un peu ridicule : si _n_ = 1 alors _somme(n)_= 1. La définition deviendrait alors :

    $`somme(n) = \left\{ \begin{array}{ll}     0 {~si~} n = 0\\ 1 ~si~n=1\\    n + somme(n -1){~sinon.} \end{array}\right.`$


>   
>
> Combien d'appels récursifs sont nécessaires pour calculer `somme(4Allons faire l'[exercice](exercices.md) 3.
>



### appels récursifs multiples.



Dans certains cas, la définition s'appelle plusieurs fois. Voici par exemple la définition de la célèbre  [suite de Fibonacci](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci) définie pour tout entier $`n \in \N`$ par :

$`U_n = \left\{ \begin{array}{ll}     1 ~si~ n = 0    \\ 1 ~si~n=1 \\ U_{n-1} + U_{n-2} ~sinon \end{array}\right.`$



>  
>
> Calculez _à la main_ (sur feuille) le 5 premiers termes de la suite.
>
> 



L'implémentation en Python serait :

```python
def u(n):
    '''
    renvoie la valeur du terme de rang n de la suite de fibonacci
    :param n (int)
    : return (int)
    >>> u(0)
    1
    >>> u(1)
    1
    >>> u(2)
    2
    >>> u(3)
    3
    >>> u(4)
    5
    '''
    assert(isinstance(n, int)),' n est entier'
    assert(n >= 0),' n est positif ou nul'
    if n == 0 :
        return 1
    elif n == 1 :
        return 1
    else :
        return u(n - 1) + u( n - 2)
```

Testez :

```python
>>> u(5)
???
>>> u(6)
???        
```



> Combien d'appels récursifs sont nécessaires pour calculer :
>
> * u(3) 
> * u(4)
> * u(5)



_Remarque :_

Cette fonction n'est pas très efficace... Pour l'améliorer, il faudrait pouvoir _mémoriser_ les calculs déjà effectués pour ne pas avoir à les refaire à chaque fois : on fait alors de la programmation dynamique. Nous verrons cela un peu plus tard.



Allons faire les [exercices](exercices.md) 4 et suivant ...



### Récursions imbriquées



Voici une fonction que l'on doit à [John Mc Carthy](https://fr.wikipedia.org/wiki/John_McCarthy) :

$`f(n) = \left\{ \begin{array}{ll} n-10 ~si~ n > 100    \\  f(f(n +11)) ~sinon \end{array}\right.`$



Remarquez que l'appel récursif est imbriqué à l'intérieur d'un autre appel récursif. Il est difficile ici d'être certain que le cas de base permet la terminaison de la récursion.



* Calculez à la main $`f(101), f(100), f(99)`$
* Implémenter cette fonction en Python
* Calculer, en utilisant la console, $`f(80),f(50), f(25)`$
* Testez les valeurs de $`f(n)`$ pour tout $`n \leq 100`$.



## Le mot de la fin ?



Ben non, on n'en a pas fini avec la récursivité !! 



De nombreuses structures de données, que nous allons étudier prochainement, comme les arbres ou les graphes, l'utilisent naturellement.



Des stratégies de résolution de problèmes, comme _diviser pour mieux régner_, l'utilisent également.



_________________

Par Mieszczak Christophe, licence CC-BY-SA



sources images wikipédia:

* [image récursivité, wikipédia, licence CC0](https://en.wikipedia.org/wiki/Recursion)

* [courbe de Koch - CC BY SA par Christophe Dang Ngoc Chan](https://commons.wikimedia.org/wiki/File:Koch_anime.gif)

* [flocon de Koch - domaine public](https://commons.wikimedia.org/wiki/File:Von_Koch_curve.gif)

* [courbe de Koch avec carrés - CC BY SA par Prokofiev](https://commons.wikimedia.org/wiki/File:Quadratic_Koch_curve_type1_iterations.png)

* [Tri rapide - CC BY SA par RolandH](https://commons.wikimedia.org/wiki/File:Sorting_quicksort_anim.gif)
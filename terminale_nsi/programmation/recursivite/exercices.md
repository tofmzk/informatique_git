# Exercices

### Exercice : erreurs de définition



Dans chaque cas, trouvez ce qui ne va pas dans la définition récursive :



1. $`pour~n \in \N,~ f(n) = \left\{ \begin{array}{ll}     32 {~si~} n = 0\\     f(n +1) - 4 {~sinon.} \end{array}\right.`$

```txt

```

2. $`pour~n \in \N~ avec~n>0,~ f(n) = \left\{ \begin{array}{ll}     1 {~si~} n = 0\\     n + f(n - 2) {~sinon.} \end{array}\right.`$

```txt

```

3. $`pour~n \in \N~avec~n>0,~ f(n) = \left\{ \begin{array}{ll}     1 {~si~} n = 0\\     n + f(n - 1) {~si~n>1.} \end{array}\right.`$

```txt

```



### Exercice  : trouver une définition

Trouvez une définition récursive pour les problèmes ci-dessous puis coder les en Python.



1. Calculer $`x^n`$ pour tout $`x`$ réel non nul et $`n`$ entier positif ou nul.

     

2. Calculer $`n!`$ pour tout entier $`n`$ positif ou nul.

    

3. Calculer $`n \times p`$ pour tous entiers positifs $`n`$ et $`p`$ en ne faisant que des additions.

    

4. Cherchez une définition de `retourner(chaine)`dont le but est de _retourner_ une chaîne de caractère ('abcde' devient 'edcba') .

     

5. Cherchez une définition dont le but est de réaliser la figure ci-dessous avec le paramètre de profondeur _n_ > 0  égal au nombre de côté(s) de la spirale et un second paramètre _l > 0_ égal à la longueur du côté à tracé.

![spirale](media/spirale1.jpg)

​																		

### Exercice  : multiples cas de base

* Redonnez une définition récursive avec deux cas de base de  $`x^n`$ pour tout $`x`$ réel et _n_ entier positif ou nul.

  ​    

* Un [palindrome](https://fr.wikipedia.org/wiki/Palindrome) est un mot dont l'ordre des lettres reste le même qu'on le lise de droite à gauche ou de gauche à droite ('121, 'été, 'radar', 'unroccornu' ou 'engagelejeuquejelegagne' sont des palindromes). Cherchez une définition récursive de `est_palindrome(mot)` qui renvoie `True` si le mot est un palindrome et `False` sinon.



### Exercice  : triangle de Pascal

Le [triangle de Pascal](https://fr.wikipedia.org/wiki/Triangle_de_Pascal) permet de déterminer de façon récursive les coefficients binomiaux qui sont utilisés dans de nombreux domaines. Ce coefficient, que l'on notera _C(n, p)_,  dépend de deux paramètres entiers positifs ou nuls _n_ et _p_. Voici le début du triangle :

|         | p = 0 | *p* = 1 | *p* = 2 | *p* = 3 | *p* = 4 | **p = 5** | **p = 6** |
| ------- | ----- | ------- | ------- | ------- | ------- | --------- | --------- |
| *n* = 0 | 1     |         |         |         |         |           |           |
| *n* = 1 | 1     | 1       |         |         |         |           |           |
| *n* = 2 | 1     | 2       | 1       |         |         |           |           |
| *n* = 3 | 1     | 3       | 3       | 1       |         |           |           |
| *n* = 4 | 1     | 4       | 6       | 4       | 1       |           |           |
| n = 5   |       |         |         |         |         |           |           |
| n = 6   |       |         |         |         |         |           |           |

* trouvez le _truc_ et complétez les lignes manquantes.

* quelles conditions y-a-t-il sur _n_ ? sur _p_ ?

* trouvez la définition récursive de _C(n, p)_ **puis** codez là en Python.

    

### Exercice  : PGCD



Le _pgcd_ de deux entiers positifs _a_ et _b_ (avec _a_ > _b_) et le plus grand diviseur commun à _a_ et à _b_. Il est très utile en Mathématiques, pour simplifier des fractions par exemple.

Pour le déterminer, on utilise la définition récursive suivante, où reste(_a_, _b_) est le reste de la division euclidienne de _a_ par _b_ :



$`pgcd(a, b) = \left \{ \begin{array} \\ b~si~reste(a,~b)~=~0    \\ pgcd(b,~reste(a,~b))~sinon  \end{array}\right.`$



Implémentez la fonction `pgcd(a, b)` en Python.




### Exercice  : Chiffres romains

Nous allons réaliser maintenant un programme permettant d'évaluer  un nombre romain, mais auparavant on va introduire les chiffres romains: 			

- M = 1000
- D =  500
- C =  100
- L =   50
- X =   10
- V =    5
- I =    1

Voici quelques exemples de l'écriture des nombres romains

- 1 s'écrit I.
- 2 s'écrit II.
- 3 s'écrit III.
- 4 s'écrit IV.
- 5 s'écrit V.
- 6 s'écrit VI.
- 7 s'écrit VII.
- 8 s'écrit VIII.
- 9 s'écrit IX.
- 10 s'écrit X.



La règle : 

* si un symbole est seul, on le traduit simplement :
    * X vaut 10.
* s'il y a deux symboles :
    * si le premier est inférieur au symbole suivant, alors on enlève ce symbole au suivant :
        * IX : I est inférieur à X donc on calcule X - I = 10 - 1 = 9
        * XC : X est inférieur à C donc on calcule C- X = 100 - 10 = 90
    * sinon, on les ajoute :
        * XI = 10 + 1 = 11
        * CX = 100 + 10 = 110



* s'il y a plus de deux symboles, on regarde les deux premiers et on les ajoute à ceux qui restent.
    * XCVI= 100 - 10 + VI = 100 - 10 + 5 + 1 = 96
    
    * CDLXI = ...
    
        



Donnez une définition récursive de `romain(chaine)` et implémentez la en Python. On pourra se servir des exemples comme doctests.

### Exercice  :  Ne pas louper le Koch

La [courbe de Koch](https://fr.wikipedia.org/wiki/Flocon_de_Koch) est un dessin réalisé par récursion.

Pour la dessiner, nous avons besoin des paramètres _l_ (entière et positive) , la longueur du segment de base, et _n_ (un entier positif) la profondeur de récursion.

Voici une animation montrant le tracé d'une telle courbe avec un niveau de profondeur 1 (oui, il manque la profondeur 0, le cas de base )  puis 1 puis 2..

![courbe de Koch](https://upload.wikimedia.org/wikipedia/commons/b/bf/Koch_anime.gif)



Ecrivez une définition récursive de la courbe puis implantez là en Python en utilisant le module `turtle`.



_Remarque :_

On peut faire évoluer la courbe de Koch en [flocon de Koch](https://fr.wikipedia.org/wiki/Flocon_de_Koch) en définissant une fonction annexe qui appelle trois fois la courbe de Koch en tournant dans le bon sens. On obtient alors la construction suivante :

![flocon de Koch](https://upload.wikimedia.org/wikipedia/commons/f/fd/Von_Koch_curve.gif)



_Remarque :_

Une autre évolution possible consiste à remplacer les triangles équilatéraux par des carrés. En serez vous capable ?



![courbe de Koch avec carrés](https://upload.wikimedia.org/wikipedia/commons/a/a6/Quadratic_Koch_curve_type1_iterations.png)

### Exercice : Le triangle de Serpinsky

Il s'agit encore d'une fractale :

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/SierpinskiTriangle.svg/800px-SierpinskiTriangle.svg.png" alt="triangle de serpinsky" style="zoom:33%;" />



Voici le cas de base (de profondeur 0) :

![cas de base](media/serpinsky_0.jpg)

Voici un triangle de profondeur récursive égale à 1 : 

![n = 1](media/serpinsky_1.jpg)

Ecrivez une fonction récursive `serpinsky(n , l)` qui trace cette figure avec une profondeur _n_ entière et positive et une longueur _l_ positive.



### Exercice : Anagrammes

les anagrammes d'un mot sont la liste, sans doublon, de tous les mots qu'on peut écrire en permutant toutes les lettres du mot, que le mot ait un sens, ou non.



Par exemple, les anagrammes de :

* 'oui' sont 'oui', 'uoi', 'uio', 'oiu', 'iou' et 'iuo'.
* '121' sont '121', '211' et '112'



Commençons, à la main, par déterminer les anagrammes de '12' puis '123' puis '1234'. Essayez de dégager une stratégie.



Pour obtenir la liste des anagrammes, on va utiliser trois fonctions :

* la première fonction, itérative (c'est à dire utilisant des boucles), sera `insererer_un_caractere(caractere, liste_mots)` qui renverra la liste de tous les mots obtenus en insérant le `caractere` à toutes les positions possibles dans tous les mots présents dans la `liste_mots`.

* la seconde est la fonction récursive `anagrammes(mot)` qui utilisera la précédente pour renvoyer la liste des anagrammes (avec d'éventuels doublons) :

    * déterminer son cas de base.
    * donnez sa définition récursive.
    * codez la en Python !

* la dernière fonction sera `elimine_doublon(liste_mots)` et renverra une liste de mot en éliminant les doublons présents dans la liste passée en paramètre.

### Exercice : petite série d'exos pour finir

Pour chaque exercice, écrire la définition récursive avant de coder en Python.

1. Écrire une fonction récursive qui prend en paramètre un tableau d'entier et en renvoie la somme.
2. Écrire une fonction récursive qui prend en paramètre une chaine de caractères et en renvoie la longueur
3. Écrire une fonction récursive qui décide (renvoie _True_ ou _False_) si un entier est présent dans un tableau ordonnée en utilisant la dichotomie.
4. Écrire une fonction récursive qui prend en paramètre un tableau d'entier est décide s'il est trié (par ordre croissant) ou pas.
5. Écrire une fonction récursive qui prend en paramètre un tableau d'entiers et renvoie le tableau trié par ordre croissant en utilisant un tri selection.



______________
Par Mieszczak Christophe
Licence CC BY SA

source images :

* [courbe de Koch - CC BY SA par Christophe Dang Ngoc Chan](https://commons.wikimedia.org/wiki/File:Koch_anime.gif)

* [flocon de Koch - domaine public](https://commons.wikimedia.org/wiki/File:Von_Koch_curve.gif)

* [courbe de Koch avec carrés - CC BY SA par Prokofiev](https://commons.wikimedia.org/wiki/File:Quadratic_Koch_curve_type1_iterations.png)

* [Triangle de Serpinsky - wikipedia - domaine public](https://commons.wikimedia.org/wiki/File:SierpinskiTriangle.svg)

    




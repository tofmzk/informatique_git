# Modularité, tests, assertions et  gestion des erreurs



L'objectif ici est de réaliser un module et de l'utiliser afin d'étudier le problème des triangles binaires.

Nous allons, dans le même temps, voir une méthode de conception du code où le code lui même sera la dernière chose que l'on fera. Nous y reviendrons ...



Le principe de la programmation modulaire est de créer ds bibliothèques de fonctions qui pourront éventuellement être utiliser dans différents programmes avec pour objectifs :

* d'éviter de coder plusieurs fois la même fonction pour plusieurs projets différents : on dit qu'on `factorise` le code.
* de simplifier et d'éclaircir le code des projets qui utilisent ces modules.



Nous allons traiter un exemple de création d'un module avec les `triangles binaires`.



Un triangle binaire est un triangle généré à partir d'un côté composé de 0 et de 1. A partir de cette première _ligne_, on obtient la suivante en réalisant un `ou exclusif` entre chaque bit de la ligne précédente.

Pour rapple, voic la table de vérité du ou exclusif, aussi noté xor ou $`\oplus`$:

|  a   | b    | a xor b  |
| :--: | :----: |:-----:|
| 0 | 0 | 0 |
| 1 | 0 | 1 |
| 0 | 1 | 1 |
| 1 | 1 | 0 |





Voici, par exemple, deux triangles binaires :

```txt
Triangles générés à partir d'une ligne de longueur 2 :
 0 0     1 0     0 1      1 1  
  0       1       1        0


Triangle généré à partir d'une ligne de longueur 3 :
1 0 1
 1 1
  0
```






> Générez le triangle binaire à partir de la ligne de départ : `01101`.
>
> ```txt
> 
> ```

 



## Interface d'un module



L'interface d'un module est composé des fonctions nécessaires à l'utilisateur de module et seulement d'elles. Ces fonctions doivent bien évidemment être documentées.



Notre interface comprend deux fonctions :

* la fonction `generer_triangle(ligne_depart)` qui renverra un triangle binaire complet à partir de la ligne de départ passée en paramètre.
* la fonction `afficher(triangle)` qui affichera le triangle dans la console.



Créez un programme `module_triangles.py`  comportant une entête correcte (encodage caractère et description du module) et les deux fonctions de l'interface.

```python
# -*- coding: utf-8 -*-
'''
    génération et affichage d'un triangle binaire
    : Auteur(s) nom, prénom
''' 

def generer_triangle(ligne_depart):
    '''
    renvoie un triangle binaire complet à partir de la ligne de départ passée en paramètre.
    : param ligne_depart (???)
    '''
    
def afficher_triangle(triangle):
    '''
    affiche le triangle dans la console
    : param triangle (???)
    : pas de return, pas d'effet de bord
    '''
```



Il s'agit maintenant de décider d'une structure de données pour notre première ligne et pour notre triangle.



> * Quelles sont les structures possibles ?
> * Choisissez-en une et compléter les Docstrings.



Depuis la console testez :

```python
>>> import module_triangle
>>> help(module_triangle)
???
```



_Remarques :_

* L'utilisateur a facilement accès à la documentation de l'interface. Celle-ci lui permet d'appeler correctement ses fonctions.

* L'utilisateur **NE DOIT PAS AVOIR BESOIN** de connaître le fonctionnement des divers algorithmes du module pour utiliser ceux dont il a besoin: on parle d'`encapsulation` des algorithmes qui sont transparents pour l'utilisateur.

    

## Doctests



Les `Doctests` auront ici deux intérêts :

* Ils donneront une indication sur la correction du code
* Ils seront un exemple d'utilisation de l'interface pour l'utilisateur.



Ajoutons les lignes pour déclencher les `Doctests` en fin de code...

```Python
# -*- coding: utf-8 -*-
'''
    génération et affichage d'un triangle binaire
    : Auteurs noms, prénoms
''' 

def generer_triangle(ligne_depart):
    '''
    renvoie un triangle binaire complet à partir de la ligne de départ passée en paramètre.
    : param ligne_depart (???)
    : return le triangle (???)
    '''
    
def afficher_triangle(triangle):
    '''
    affiche le triangle dans la console
    : param triangle (???)
    : pas de return, pas d'effet de bord
    '''
    
    
    

    
################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__":
    import doctest
    doctest.testmod(verbose = True) 
```



_Remarque :_

la condition `__name__ == "__main__"`  est vraie si le programme n'est pas exécuté en tant que module. Ainsi, si vous importez ce programme en tant que module dans un autre programme, ces `Doctests` ne s'exécuteront pas et ne parasiteront donc pas ceux du programme _principal_.



> 
>
> Créez deux Doctest pour la fonction `generer_triangle` en respectant la structure choisie.
>
> 



Depuis la console testez :

```python
>>> import module_triangle
>>> help(module_triangle)
???
```





### Vérifier les pré-conditions avec des assertions



Une fonction demande, pour être correcte, d'utiliser des paramètres du bon type dans un intervalle prévu par le celui qui l'a définie. C'est ce qu'on appelle les `pré-conditions`.

L'utilisation de mauvais paramètres peut avoir des conséquences désastreuses comme nous l'avons vu dans le chapitre précédent.



On utilise donc des **assertions** pour vérifier que les valeurs passées aux paramètres (les _arguments_ donc) sont du bon type.

Il y a deux écritures possibles équivalentes pour vérifier qu'une elt est du bon type, par exemple du type list, et déclencher une exception sinon :

```python
assert type(elt) == list, 'on attend un paramètre de type list'
assert isinstance(elt, list),  'on attend un paramètre de type list'
```



> 
>
> Pour les deux fonctions de notre interface, codez les assertions. On vérifiera également que les éléments de chaque liste sont eux aussi du type attendu.
>
> 



## Gestion des erreurs



 Parfois, les assertions ne suffisent pas à être certain d'éviter les erreurs ou devraient être beaucoup trop nombreuses pour le faire.

On peut alors utiliser une méthode de gestion d'erreurs en utilisant la structure :

```python
try : 
    # ici on insère le code
except :
    # ici on insère le code a exécuter s'il y a une erreur dans le bloc try
     

```

Si une erreur survient dans le bloc `try :`, au lieu de générer une exception et de quitter le code, on _sautera_ dans le bloc `except :` où l'on pourra :

* Soit déclencher une exception définie grâce à la méthode `raise` :

    ```python
    raise Exception("description de l'exception")
    ```

* soit corriger cette erreur.



> 
>
> Dans la fonction `afficher_triangle`, remplacer les assertions par une gestion d'erreur qui déclenche l'exception "paramètre incorrect" en cas de problème.
>
> 



## Découpage en sous-problèmes et fonctions privées



Revenons à notre fonction `generer_triangle`.



Pour générer un triangle à partir de sa première ligne, il y a diverses opérations à effectuer : lesquelles ??

```txt

```

Nous allons réaliser deux fonctions supplémentaires afin de rendre chacune des fonctions aussi petite et simple que possible :

* L'une pour réaliser un ou exclusif entre deux bits
* L'autre pour générer une ligne à partir de la précédente.



```python
def ou_exclusif(bit1, bit2):
    '''
    
    '''
    
def generer_ligne_suivante(ligne_precedente):
    '''
    
    '''
    

```



>  
>
>  * Définissez ces fonctions 
>
>  * Ecrivez leur `Docstrings`.
>
>  * Ecrivez deux `Doctests` par fonction.
>
>      
>



Depuis la console, testez à nouveau :

```python
>>> import module_triangle
>>> help(module_triangle)
???
```



Cela pose un problème : l'utilisateur n'a pas besoin de connaître ces deux fonctions et pourtant elles surchargent l'interface !

Il faut donc les rendre privées, c'est à dire réservées au module et invisibles à l'utilisateur. On dit que ces fonctions privées sont `encapsulées` dans le module.

Pour cela, il suffit de commencer leur nom par deux `underscores` `__`

```python
def __ou_exclusif(bit1, bit2):
    '''
    
    '''
    
def __generer_ligne_suivante(ligne_precedente):
    '''
    
    '''
    

```

_Remarque :_ N'oubliez pas d'ajouter les `__` dans les Doctests !

Testons à nouveau :

```python
>>> import module_triangle
>>> help(module_triangle)
???
```



_Remarque :_

Est-il nécessaire de coder des assertions pour ces deux fonctions ? Pourquoi ?



## Codage des algorithme



Nous avons :

* Pensé l'interface, c'est à dire les fonctions indispensables à l'utilisateur.
* Documenté, testé et fixé les pré-contraintes de ces fonctions
* Découpé le problème en sous-problèmes simples en définissant des fonctions privées invisibles pour l'utilisateur
* Documenté et testé ces fonctions privées.



Il nous reste à coder les algorithmes. Allons-y !



## Utilisation du module



Créez un programme `triangles.py`.

```python
# -*- coding: utf-8 -*-
'''
    autour des triangles binaires
    : Auteurs noms, prénoms
''' 

import module_triangles
```



Nous allons travaillé en binôme.



L'objectif est :

* de pouvoir générer et afficher tous les triangles binaires dont le premier côté à une longueur _n_ > 0.
* De pouvoir déterminer si un triangle est complet ou pas, c'est à dire s'il est composé autant de 0 que de 1.
* de pouvoir afficher tous les triangles binaires complets dont le premier côté à une longueur _n_ > 0.



> * Listez les différentes tâches à accomplir .
> * Découpez ces tâches en fonctions aussi petites et simples que possible.
> * Répartissez-vous les fonctions de façon équitables.
> * Définissez, documentez (avec le nom de l'auteur de la fonction) et créez un Doctest pour chacune des fonctions.
> * Codez ces fonctions.
> * Réunissez votre travail et testez le.



_Un peu d'aide ?_:

* Quel est le plus grand entier que l'on peut écrire en binaire avec un _mot_ (c'est à dire une série de bits) de longueur _n_ ?

* On pourra utiliser la fonction `bin(nbre)` qui convertit un nombre entier en chaîne de caractères contenant sa valeur en binaire.

    Testez :

    ```python
    >>> bin(4)
    ???
    >>> bin(4)[2:]
    ???
    >>> bin(9)
    ???
    >>> bin(9)[2:]
    ???
    ```

    





_________________

Par Mieszczak Christophe, licence CC-BY-SA


# Les chaines de caractères

Bon, il n'y a pas que des nombres dans la vie. Il existe de nombreux _types_ différents de variables. 

Par exemple, les chaînes de caractères qu'on définit soit entre deux guillemets `""` soit entre deux apostrophes `''` .

Testez cela dans la console :

```python
>>> chaine_1 = 'Hello World'
>>> print(chaine_1)
???
>>> type (chaine_1)
???
```

_Remarque :_

* le nom d'une chaine doit toujours être écrit en minuscule, sans accent et sans espace. On peut utiliser les caractère `_` (le underscore sur la touche 8) pour marquer une séparation dans un nom long.



On peut ajouter deux chaînes : c'est une _concaténation_. Essayez :

```python
>>> a = ' Yoda '
>>> b = 'je suis'
>>> a + b 
???
>>> b + a
???
```

En Python, les chaînes de caractères sont du type _str_, abréviation de _string_ qui signifie _chaine_. Oui ... d'où le nom du célèbre sous-vêtement.

Cependant, on ne peut pas ajouter n'importe quoi :

```python
>>> a = 'Je mesure '
>>> b = 1.12
>>> c = 'm'
>>> a + b + c
???
```

Cela ne marche pas car les variables ne sont pas du même _type_. Il es tpossible d'ajouter ensemble deux nombres ou ensemble deux chaines mais pas un nombre et une chaine. 

Il faut utiliser des conversions : 

* soit la fonction `str(nombre)` qui convertit le nombre en chaîne.
* soit la fonction `int(chaîne)` qui convertit, si possible, la chaîne en entier.
* soit la fonction `float(chaîne)` qui convertit, si possible ,la chaîne en flottant.

```python
>>> str(b)
???
>>> a + str(b) + c
```

Au fait qui est _a_ ? _b_ ? _c_ ?

Pour plus de clarté, on nomme les variables en minuscule, sans accent, sans espace (on utilise `_` à la place, le tiret du `8` aussi appelé _underscore_) ni opérateur dans le nom. Certains noms de variable sont interdits car déjà utilisés par Python : `str` ou `int` par exemple.

C'est tout de suite plus clair :

```python
>>> nom = 'Yoda '
>>> taille = 1.12
>>> nom + ' mesure ' + str(taille) + 'm.'
???
```



C'est bien joli tout cela mais on ne peut rien sauvegarder dans cette console ! Il est temps de coder notre premier programme.



## Premier programme



Allez dans le menu déroulant choisir _Fichier / Nouveau_ :

![nouveau fichier](media/untitled.jpg)



Dans la languette en haut, vous lisez _untitled_ : le fichier n'a pas de nom. Pour le nommer, il faut le sauvegarder en suivant le menu _Fichier / Enregistre sous_. Nommez le, par exemple, _premier_programme.py_ :

![premier programme](media/premier_prog.jpg)



Dans la partie liée au code, tapons notre premier petit programme :

```python
nom = 'Yoda'
ma_taille = 1.12
print('Je suis ' + nom)
print('Je mesure ' + str(ma_taille) + 'm')
```

* Remarquez les espaces entre les opérateurs `=`  et `+`. Cela fait partie des bonnes pratiques en Python :
    * toujours mettre un espace de chaque côté des opérateurs.
    * toujours un unique espace uniquement après chaque virgule.
* La fonction `print` permet d'afficher un résultat dans la console.



Pour exécuter le code, cliquez sur la flèche verte de la barre de menu.

* Le code est automatiquement sauvegardé.
* Le résultat éventuel s'affiche dans la console.
* Dans le panneau de droite, vous pouvez lire quelques informations...

![exécution](media/execution.jpg)



Pour rendre le volet de droit un peu plus utile pour nous, on va le remplacer : suivez le menu _Affichage_, décochez _Assistant_ et cochez _Variables_.

Les valeurs des variables apparaissent à droite. Pratique pour vérifier si elles valent bien ce qu'on a prévu !

![varaibles](media/variables.jpg)



On peut également exécuter le programme _pas à pas_ en cliquant sur le petit insecte situé à côté de la flèche. _Insecte_ , en Anglais, se dit _bug_ : ce mode d'exécution est très pratique pour vérifier si le programme se déroule comme il faut et pour traquer les erreurs !

![pas à pas](media/pas_a_pas.jpg)

* Pour avancer pas à pas, cliquez sur la première flèche à droite du _bug_....
* Regarder les variables apparaître au fur et à mesure qu'elles sont déclarées.



Pour _poser une question_ à l'utilisateur, on peut utiliser l'instruction `input()`. Testez dans la console :

```python
>>> nom = input('Quel est ton nom ? ')
...
>>> nom
???
>>> type(nom)
???
```

Remarquez bien que la variable dans laquelle vous stockez la réponse est du type _str_ : c'est une chaîne de caractères. Si vous attendez un entier ou un flottant, il faudra penser à convertir cette réponse grâce aux instructions `int(valeur)` ou `float(valeur)`, comme nous l'avons déjà fait plus haut.



> Créez un nouveau fichier nommé _yoda.py_ puis écrivez un  programme qui :
>
> * demande "Quel est votre nom? ", par exemple "Yoda"
>
> * demande "Quel est votre âge ?" par exemple ("505")
>
> * affiche dans la console : 
>
>     _Bonjour Yoda._
>
>     _Tu as 505 ans._
>
>     _Dans 10 ans, tu auras 515 ans._



## Structures de base en Python

Au collège, vous avez travaillé avec _Scratch_. Vous avez appris à utiliser des blocs, des structures conditionnelles, des boucles ... Voyons leur équivalent en Python.



### Les structures conditionnelles

Si ... alors ... sinon si .... sinon. Vous avez déjà vu ça avec Scratch :



![si](media/si.jpg)

En Python cela se traduit ainsi :

```python
if condition :
    #conséquence1
elif condition2 :
    #conséquence2
else :
    #conséquence3
```

* On n'oublie pas les `:`  qui marquent le début de la conséquence.
* Les `#` permettent d'écrire des remarques non prises en compte lors de l'exécution.
* Le code conséquence de la condition est tabulé (décalé de 4 espaces) à droite.
* Le `elif` est une contraction de `else if` c'est à dire `sinon si`. 
* S'il ne reste plus qu'un seul cas possible, le `elif` est inutile. On utilise, en dernier recours, `else`.



Revenons à Yoda :

* S'il me dit mesurer plus de 1.80m , on lui répond qu'il se prend pour Chewbacca.
* sinon, si il dit qu'il mesure plus de 1.20, on lui répond qu'il se prend pour Luc Skywalker.
* sinon, si il dit qu'il mesure plus 0.8m, on lui répond que ce n'est pas la taille qui compte.
* sinon on lui répond qu'il se sous-estime.



Cela nous donne :

```python
taille = input('Combien mesures tu ?')
taille = float(taille)
if taille > 2 :
    print('Maitre, vous vous prenez pour Chewbacca')
elif taille > 1.4 :
    print('Maitre, vous vous prenez pour Luc Skywalker')
elif taille > 0.8 :
    print("Maitre, ce n'est pas la taille qui compte")
else :
    print('Maitre, vous vous sous-estimez')
```



_Remarquez que la troisième réponse utilise une apostrophe. Par conséquent, on utilise les guillemets pour définir la chaîne de caractères._



> Créer un nouveau fichier _age_yoda.py_ puis codez un programme qui : 
>
> * demande l'âge de Yoda 
>
> * s'il a moins de 100 ans, affiche "Vous un tout jeune maitre !"
>
> * sinon s'il a moins de 200ans, affiche "Vous êtes adolescent maitre !"
>
> * sinon s'il a moins de 400 ans, affiche  "Vous êtes dans la fleur de l'âge maitre !"
>
> * sinon affiche  "Vous vous faites vieux maitre !"



_Remarque :_ Lorsqu'on écrit des conditions, on peut utiliser les opérateurs `or`, `and` ou `not`. On en reparlera plus tard...



### Les boucles.

Vous avez déjà travaillé sur les boucles avec Scratch :

![boucle](media/boucle.jpg)



En Python, on utilise principalement deux types de boucles : 

* les boucles _tant que_ .

* les boucles _pour_ .

  

### Boucle Pour



En Python, les boucles `pour` sont assez complexes. Nous en verrons ici une version simplifiée et limitée : celle qui parcourt une plage d'entiers.

La boucle `pour i allant de a (inclus) à b (exclus)` s'écrit ainsi :

```Python
for i in range(a, b):
    #ici on place le code de la boucle
```

* On n'oublie pas les `:`  qui marquent le début de la boucle.

* Le code à l'intérieur de la boucle est tabulé (décalé de 4 espaces) à droite.

* La variable _i_ prendra les valeurs allant de _a_ **inclus** à _b_ **exclus**. _a_ est facultatif s'il vaut 0.

    

> Créez un fichier _yoda_fait_repeter.py_ puis codez un programme qui demande à Yoda combien de fois il souhaite qu'on répète une phrase et affiche ensuite autant de fois que demandé la phrase "Que la force soit avec toi !" dans la console.





### Boucle tant que



On traduit la boucle `tant que` de la façon suivante en Python :

```python
while condition :
    # ici on écrit le corps de la boucle
```

* On n'oublie pas les `:` qui marquent le début de la boucle.
* Tant que la condition reste vraie, le code écrit dans le corps de la boucle s'exécutera... en boucle.
* Le code à l'intérieur de la boucle est tabulé (décalé de 4 espaces) à droite.

Regardez le code ci-dessous :

```python
taille = input('Combien mesures-tu ?')
taille = float(taille)
while taille < 0.8 or taille > 1.2 :
    if taille > 1.8 :
        print ('Maitre, vous vous prenez pour Chewbacca')
    elif taille > 1.2 :
        print('Maitre, vous vous prenez pour Luc Skywalker')
    else :
    	print('Maitre, vous vous sous-estimez')
    taille = input('Combien mesures-tu ?')
    taille = float(taille)
print("Maitre, ce n'est pas la taille qui compte")
```

* Que fait-il ?
* Remarquez que l'on a ajouté des lignes avant la boucle : à quoi servent-elles ?



> Créez un nouveau fichier puis codez un programme qui :
>
> Demande à Yoda l'âge qu'il a.
>
> Tant que l'âge donné par yoda est inférieur à 600 :
>
> * Si l'âge proposé est inférieur à 300, on affiche "Je ne peux vous croire Maitre !" 
> * sinon affiche "Ca me parait un peu jeune Maitre ... "
> * repose ensuite la question
>
> Affiche à l'issue de la boucle : "Vous avez donc ... ans !!"
>



### Fonction

Nous avons jusque là créé un fichier pour chaque programme. On peut aussi regrouper des morceaux de code dans des structures particulières appelées _fonction_ et placer plusieurs fonctions dans le même fichier.

On spécifie une fonction de la façon suivante :

```python
def nom_de_la_fonction(parametre1, parametre2, ...):
    '''
    ici on écrit une documentation (ou spécification) c'est à dire une explication de ce que fait la fonction
    ce ne sera pas pris en compte à l'exécution
    '''
```

* le nom de la fonction s'écrit en minuscule en séparant les différentes parties par un _underscore_ `_` (sous le 8).

* Les paramètres sont facultatifs. Il peut ne pas y en avoir ou en avoir plusieurs selon les cas. **Les parenthèses sont en revanche obligatoires, même s'il n'y a rien dedans.**

* On n'oublie pas les `:` qui marquent le début de la fonction.

* Les remarques écrites en dessous s'appellent une `DocString`, une documentation ou une spécification. Elles servent à mieux comprendre ce que doit faire la fonction. 

* Tout le code à l'intérieur de la fonction est tabulé (décalé avec la touche `tab`). Si on enlève la tabulation, on sort de la fonction.

  

Créez un nouveau fichier nommé _discussion.py_.

Commençons par la fonction ci-dessous :

```python
def demande_a_yoda():
    '''
    pose une question à yoda et affiche sa réponse
    '''
    nbre = int(input('Combien de fois dois-je répéter ? "))
    print(nbre)
```



Exécutez le code : il ne se passe rien. Il faut **appeler la fonction**, par son nom, dans la console (Notez les parenthèses, obligatoires même s'il n'y a pas de paramètre) :

```python
>>> demande_a_yoda()
???
```

Le problème, en procédant ainsi, est qu'on ne peut pas stocker le résultat de notre fonction : elle est juste affichée dans le console.

Pour pouvoir le faire, il faut éviter le `print`, qui se contente d'afficher, et utiliser `return` qui renvoie un résultat qu'on pourra exploiter en quittant la fonction.

```python
def demande_a_yoda():
    '''
    renvoie la réponse d'une question posée à yoda
    '''
    nbre = int(input('Combien de fois dois-je répéter ? "))
    return nbre
```



Testez dans la console :

```python
>>> n = demande_a_yoda()
???
>>> n
???
```



On peut faire utiliser une valeur par une fonction en utilisant un **paramètre** :

```python
def padawan_parle(n):
    '''
    affiche n fois "que la force soit avec vous"
    '''
    for i in range(n) :
        print("Que la force soit avec vous !")
```

Testez dans la console :

```python
>>> padawan_parle(2)
???
>>> padawan_parle(4)
???
```



Vous pouvez appeler la fonction depuis la console mais aussi depuis une autre fonction.

```python
def discussion() :
    '''
    pose une question à Yoda puis fait parler le padawan
    '''
    nbre = demande_a_yoda()
    padawan_parle(nbre)
```



Testez dans la console :

```Python
>>> discussion()
???
```



_Remarques :_

* Déclarer des fonctions permet de _factoriser_ le code c'est à dire d'écrire à un endroit et un seul des morceaux de code qu'on pourra réutiliser en les appelant, sans être obligé de les re-taper.
* On essaie toujours d'utiliser les fonctions les plus courtes et simples possible afin de limiter la difficulté du code et les risques d'erreurs. Ainsi, on découpera une fonction trop grosse en plusieurs petites fonctions.









___

Par Mieszczak Christophe avec l'aide d'Anne Boenisch

Licence CC BY SA

source images : production personnelle.










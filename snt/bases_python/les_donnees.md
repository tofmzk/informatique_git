# L'importance des données

L'informatique est **la science du traitement automatique des données** : stocker des données, y accéder et les traiter en utilisant des algorithmes est le un point fondamental de cette science. 

Pour **stocker** des données, Il faut les _ranger_ quelque part dans la mémoire de l'ordinateur :

- soit dans la mémoire volatile , celle qui va tout oublier lorsqu'on coupera l'alimentation (les barettes mémoires, la célèbre RAM)	
- soit dans une mémoire non volatile comme un disque dur, un disque SSD ou une clé USB.



Pour **accéder** aux données il faut savoir où et comment elles sont rangées et de quelle façon elles sont codées, que ce soit dans la RAM ou sur un disque quelque part dans le cloud.



Il existe un grand nombre de façons de ranger des données plus ou moins complexes dans des structures elles aussi plus ou moins complexes et performantes. En voici quelques unes :

| Données de base |  Structures linéaires  |                            Arbres                            |                           Graphes                            |
| :-------------: | :--------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|     a = 12      | Tableau [0, 1, 2, ...] | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Arbre_binaire_ordonne.svg/288px-Arbre_binaire_ordonne.svg.png" alt="arbres" style="zoom: 33%;" /> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/6n-graf.svg/330px-6n-graf.svg.png" alt="graphe" style="zoom:33%;" /> |

En seconde, nous allons parler ici uniquement de données de base :

* Les _nombres_ entiers ou à virgules;
* les _chaînes de caractères_ (le texte) ;
* les _booléens_ (Vrai ou Faux).

________

Par Mieszczak Christophe

Licence CC BY SA

Sources images :

* [Arbre - wikipédia](https://commons.wikimedia.org/wiki/File:Arbre_binaire_ordonne.svg)

* [Graphe - wikipédia](https://commons.wikimedia.org/wiki/File:6n-graf.svg)

    
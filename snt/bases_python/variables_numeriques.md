# Variables numériques 

## En Mathématiques

En Mathématiques, il y a différents types de nombres :

* les entiers naturels : 0, 1, 2, 3 ....
* les entiers relatifs dans lesquels sont inclus les entiers naturels : ... -2, -1, 0, 1, 2, 3
* les décimaux dans lesquels sont inclus les relatifs : - 1, 234 par exemple
* les rationnels dans lesquels sont inclus les décimaux : $\frac 1 3$ par exemple
* les irrationnels qui ne peuvent pas s'écrire sous forme de fraction : $\pi$ ou $\sqrt 2$ par exemple

L'ensemble de ces nombres sont appelés les nombres réels.

En informatiques, on ne peut stocker qu'une quantité limité de données : impossible donc de mémoriser toutes les décimales de $\pi$ et donc de stocker sa valeur exacte. De la même façon, tout nombre dont la partie décimale est infinie ne pourra être stocké de manière exacte en mémoire. 

Ainsi, les types de nombres en informatique ne peuvent être exactement les mêmes qu'en mathématiques. On va essentiellement travailler sur deux cas de figure :

* **Les entiers** : selon les langages de programmation, il peut y avoir plusieurs types d'entiers selon leurs grandeurs c'est à dire selon la place qu'ils vont occuper en mémoire. 
* **les nombres à virgules** qu'on appelle **les flottants** et qui ne sont pas tout à fait des nombres décimaux.

## Les entiers en Python

Les nombres entiers en Python sont du type **int**, une abréviation de integer qui signifie entier en anglais.

On peut bien entendu effectuer des opérations entre les entiers.

Dans la console entrez les commandes ci-dessous et complétez les résultats:

```python
>>> a = 20
>>> b = 4
>>> type(a)
???
>>> a + b
???
>>> a - b
???
>>> a * b
???
>>> b ** 2
???
>>> a // b
???
>>> a % b
???

```

_Remarque :_

* le nom d'une chaine doit toujours être écrit en minuscule, sans accent et sans espace. On peut utiliser le caractère `_` (le underscore sur la touche 8) pour marquer une séparation dans un nom long.



Vous venez d'utiliser quatre opérateurs :

* `+` l'addition de deux entiers est un entier;

* `-` la différence entre deux entiers est un entier;

* `*` le produit de deux entiers est un entier;

* `**` élever un entier à une puisance entière donne un résultat entier;

* `//` le quotient de la division euclidienne d'un entier par un entier est un entier;

* `%` le reste de la division euclidienne d'un entier par un entier est un entier.

    

Pourquoi `//` et pas `/`  ? Parce ce qu'en divisant un entier par un entier, on obtient presque toujours des virgules ... et donc plus un entier mais un flottant et c'est une autre histoire dont on parle plus bas.



Les ordinateurs sont extrêmement performants pour calculer avec des nombres entiers. Testez (vous pouvez changer les valeurs) :

```python
>>> 123456789 ** 123456789
?
```

On peu comparer deux entiers grâce aux opérateurs ci-dessous :

```python
>>> a < b
???
>>> a > b 
?
>>> a == b
???
```

_Remarques :_

* La réponse est chaque fois `True` ou `False` : il s'agit du type **bool** pour boolean en Anglais.
* pour tester si deux entiers sont égaux, l'opérateur est `==` et pas `=` qui sert à autre chose comme on le verra plus loin.



## Les flottants

Tout nombre ayant une partie décimale est codé sous le type **float**, abréviation de **floating point**. On utilise un `.` et non une `,` entre la partie entière et la partie décimale d'un flottant.

Testez : 

```python
>>> type(0.1)
???
>>> type(2.0)
???
>>> type(5 / 2)
???
>>> type(4 / 2)
???
>>> type(4 // 2)
???
```

Vous voyez ici la différence entre `/` dont le résultat est tourjous un flottant et `//` dont le résultat est toujours un entier.

Particularité des flottants : ce ne sont pas exactement des décimaux mais des approximations de décimaux :

```python
>>> 0.1 + 0.1
???
>>> 0.1 + 0.1 + 0.1
???
```

Ouch... 

Ce n'est pas un problème lié à Python, cela vient de la façon dont on code les nombres à virgules en informatique : c'est performant mais approximatif. 

Cela reste très précis quand même :

* L'erreur est d'environ $10^{-16}$ 
* si on mesure un objet en mètre, cela permet une précision de l'ordre du milionnième de la taille d'un atome... Ça devrait suffire...

Oui mais du coup, le test d'égalité entre deux flottants en informatiques n'a pas vraiment de sens. On peut juste vérifier si deux flottants sont proches ou pas mais pas s'ils sont parfaitement égaux.

```python
>>> 0.1 + 0.1 + 0.1 == 0.3
???
>>> round(0.1 + 0.1 + 0.1 , 10) == 0.3
???   
>>> round(0.1 + 0.1 + 0.1 , 14) == 0.3
???   
>>> round(0.1 + 0.1 + 0.1 , 16) == 0.3
???   
```



__________

Par Mieszczak Christophe

Licence CC BY SA

# Les fonctions

Nous avons jusque là créé un fichier pour chaque programme. On peut aussi regrouper des morceaux de code dans des structures particulières appelées _fonction_ et placer plusieurs fonctions dans le même fichier.

On spécifie une fonction de la façon suivante :

```python
def nom_de_la_fonction(parametre1, parametre2, ...):
    '''
    ici on écrit une documentation (ou spécification) c'est à dire une explication de ce que fait la fonction
    ce ne sera pas pris en compte à l'exécution
    '''
    # ici on place le code de la fonction
    
    #pour finir on renvoie le résultat de la fonction
    return ...
```

* le nom de la fonction s'écrit en minuscule en séparant les différentes parties par un _underscore_ `_` (sous le 8) comme les variables.
* Les paramètres sont facultatifs. Il peut ne pas y en avoir ou en avoir plusieurs selon les cas. **Les parenthèses sont en revanche obligatoires, même s'il n'y a rien dedans.**
* On n'oublie pas les `:` qui marquent le début de la fonction.
* Les remarques écrites en dessous s'appellent une `DocString`, une documentation ou une spécification. Elles servent à mieux comprendre ce que doit faire la fonction. 
* Tout le code à l'intérieur de la fonction est tabulé (décalé avec la touche `tab`). Si on enlève la tabulation, on sort de la fonction.
* Le `return` final permet de _renvoyer_ le résultat de la fonction.



## Fonctions et Mathématiques

En mathématiques, vous utilisez des fonctions depuis longtemps. Par exemple, la fonction x² qui a tout nombre associe la valeur x² :

* f(2) = 2² = 4
* f(3) = 3 ² = 9
* etc ...

 En Python, on la code ainsi :

```python
def f(x):
    '''
    renvoie x au carré
    '''
    return x ** 2
```

Créez un nouveau programme _carre.py_, coller-y ce code et exécutez le.

Il ne se passe rien car i lfaut appeler la fonction depuis la console :

```python
>>> f(2)
???
>>> f(3)
???
```

* `x` est le **paramètre** de la fonction;
* la valeur que vous donnez à `x` (ici 2 puis 3) est appelé **argument**;
* la fonction d'appelle par son nom avec l'argument entre parenthèse;
* si il n'y a pas de paramètre, on met des parrenthèses sans rien dedans.



Grâce au `return`, on peut récupérer le résultat et l'utiliser. Testez dans la console :

```python
>>> a = f(3)
>>> b = f(4)
>>> c = f(5)
>>> a + b == c
???
```

> A quel célèbre théorème ces calculs font allusions ?



## Factoriser du code

Créez un nouveau fichier nommé _oui_ou_non.py_

Coller la fonction ci-dessous :

```python
def demande():
    '''
    demande de réponse oui ou non et renvoie la réponse
    '''
    reponse = '' # on définit la variable avant le while
    while reponse != 'oui' and reponse != 'non' :
        reponse = input('Répondez par oui ou non : ')
    return reponse
```

Exécutez le code : il ne se passe rien. On a déjà dit qu'il faut **appeler la fonction**, par son nom, dans la console (Notez les parenthèses, obligatoires même s'il n'y a pas de paramètre) :

```python
>>> demande()
???
```

Ce serait pas mal de poser une vraie question dont la réponse pourrait-être oui ou non. Pour cela, on va utiliser un paramètre. 

```python
def demande(question):
    '''
    pose une question, demande une réponse (oui ou non)et la renvoie
    '''
    reponse = ''
    while reponse != 'oui' and reponse != 'non' :
        print(question)
        reponse = input('Répondez par oui ou non : ')
    return reponse
```

Testez depuis la console :

```python
>>> demande('Avez-vous compris ?')
???
```

Normalement vous êtes en train de vous demander à quoi ça une telle fonction peut bien servir. Non ? Sisi !

Imaginons un programme où vous devez très souvent poser à l'utilisateur une question dont la réponse est oui ou non. Et bien grâce à cette fonction, ce ne sera plus la peine de coder à chaque fois la partie qui pose la question : il suffira d'appeler la fonction `demande` et d'utiliser le résultat qu'elle vous renvoie.



Testez le code ci-dessous :

``` python
def demande():
    '''
    pose une question et affiche sa réponse
    '''
    reponse = ''
    while reponse != 'o' and reponse != 'n' :
        reponse = input('Répondez par oui ou non')
    return reponse

reponse = demande('Aimez-vous le Jazz ?')
if reponse == 'oui' :
    print('Moi aussi !')
    
```

> Si la réponse n'est pas 'oui', demandez à l'utilisateur s'il aime le rock. Si il répond 'oui', félicitez le pour son bon goût. Sinon, demandez lui s'il aime la musique classique ...



_Remarques :_

* Déclarer des fonctions permet de _factoriser_ le code c'est à dire d'écrire à un endroit et un seul des morceaux de code qu'on pourra réutiliser en les appelant, sans être obligé de les re-taper.
* On essaie toujours d'utiliser les fonctions les plus courtes et simples possible afin de limiter la difficulté du code et les risques d'erreurs. Ainsi, on découpera une fonction trop grosse en plusieurs petites fonctions.



___

Par Mieszczak Christophe 

Licence CC BY SA










# Entrées et sorties

On parle ici d'interface homme-machine en Python :

*  comment faire pour demander des informations à l'utilisateur ?
* comment faire pour afficher des résultats ?

## print

On commence par l'affichage : la fonction qui permet cela est `print`. 

Testez donc en console :

```python
>>> print('Hello World')
???
>>> print(2 + 3)
???
```

On peut également afficher plusieurs choses sur la même ligne, en les séparant par une `,` :

```python
>>> print('Je mesure', 1.8, 'm')
???
```

La fonction `print` fait plus qu'afficher : elle l'interprète.

Testez :

```python
>>> print('- Bonjour\n-Bonjour\n-Tout va bien ?\n-oui merci !')
???
>>> print('H\tE\tL\tL\tO')
???
>>> print('Voilà comment on affiche une apostrophe :\' ')
???
```

> * Comment est interprété `\n` ?
> * Comment est interprété `\t` ?
> * Comment est interprété `\'` ?
> * Comment faire pour afficher un `\` ?



Par défaut, à la fin d'un `print`, on va à la ligne. Mais on peut changer en modifiant par quel caractère on termine la chaine lors d'un `print`.

Créez un programme _tests_print.py_ et coller le code ci-dessous :

```python
print('Hello', end ='')
print('World')
print('fini')

```

Éxécutez ce code et testez le !

le `end =''` remplace le saut à la ligne par un caractère vide... du coup on ne va pas à la ligne



## input



La commande `input(question)` affiche la chaine de caractères _question_ placée entre parenthèses, attend un réponse de l'utilisateur et la renvoie.

Testez :

```python
>>> age = input('Quel âge as-tu ? ')
???
>>> print(age)
???
>>> type(age)
```

Remarquez bien que la réponse est toujours une chaine de caractères ,même lorsqu'on attend un nombre !!

Si l'on souhaite obtenir un entier ou un flottant, il faudra faire une conversion :

```python
>>> age = int(input('Quel âge as-tu ? '))
???
>>> print(age)
???
>>> type(age)
>>> taille= float(input('Quelle est ta taille ? ')
???
>>> print(taille)
???
>>> type(age)
```



______

Par Mieszczak Christophe

Licence CC BY SA


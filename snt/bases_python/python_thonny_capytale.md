# Python , Capytale et Thonny

## Introduction

[Python](https://fr.wikipedia.org/wiki/Python_(langage)) est un langage de programmation créé en 1989. C'est à la fois un langage simple pour débuter en programmation grâce à une syntaxe proche du langage humain et un langage puissant et polyvalent utilisé dans de nombreux domaines (création web, IA, data, robotique, cybersécurité...). Netflix, les scripts de Discord ou  les filtres photographiques de GIMP sont codés en Python.

Il existe différentes versions de Python :

* Python3.11 est la version courante en sept 2023 (mais ça évolue tout le temps ...)
* microPython standard (celui de vos calculatrices)
* microPython pour _micro:bit_ est l'interpréteur pour coder en Python sur une carte _micro:bit_



Python est un langage interprété : on tape le code des programmes dans un éditeur de code qui, lorsqu'on lui demande d'_exécuter_ le code, utilise un interpréteur Python installé sur votre machine. Cet interpréteur lit le code et le traduit à la machine au fur et à mesure. 

De nombreux éditeurs de code en Python permettent programmer en Python. Nous utiliserons, la plupart du temps :

* [Thonny](https://thonny.org/) qui est un excellent éditeur pour débutants tout en permettant de développer des projets ambitieux.
*  [Capytale](https://capytale2.ac-paris.fr/web/accueil) qui propose, entre autres choses, un notebook accessible directement depuis les applications de l'ENT.



## Capytale

Vous accédez à Capytale via l'ENT. Connectez vous à votre compte ENT, allez dans les applications et trouvez le connecteur Capytale et cliquez dessus :

![connecteur](media/capytale.png)



Sur la page d'accueil, sélectionnez `Accédez à vos activités` :

![activités](media/vos_activites.png)

Vous trouverez ici les activités que vous avez travaillées (rien au départ donc) mais vous pouvez également avoir accès à des activités vi la zone de texte `Accéder à une activité` en y tapant un code donné par votre professeur. Testez le code ci-dessous :

![](media/recherche.png)



Cliquez maintenant sur `les_variables` : Vous avez ouvert un Notebook.

Il est constitué de parties de cours écrit en markdown et de parties de code Python exécutables directement dans le document. 

### Le markdown

Cliquez sur la première partie du cours intitulée `Variable numérique` : le code du bloc de markdown est alors modifiable  :

![](media/markdown.png)

* Changer le sous titre `En Mathématiques` par `Les nombres en Mathématiques`.
* Cliquez sur la zone de code puis sur le bouton `Exécuter` dans le menu du haut.

### Le code Python

Sans lire le document, ce n'est pas l'objet ici, descendez vers la première zone de code :

![](media/notebook.png)

Cliquez sur la zone de code puis sur le bouton `Exécuter` dans le menu du haut.

Le code s'éxécute donc et ses résultats seront exploitables dans toute la suite du notebook.



Sur le [dépôt du cours](https://framagit.org/tofmzk/informatique_git/-/blob/master/snt/readme.md), vous trouverez d'autres codes d'activité sous Capytale.

## Thonny.

[Thonny](https://thonny.org/) est installé par défaut sur les machines du lycée. Vous pouvez simplement l'installer chez vous en suivante [ce lien](https://thonny.org/)).

Ouvrez donc Thonny.

![console](media/console.jpg)

* En haut, il y a un petit _menu_.
* La partie en bas est la _console_ .
* C'est entre le menu et la console qu'on tapera le code par la suite.
* A droite, un volet nous donnera des informations.



### Le console

La console est un endroit où l'on peut déjà taper du code en Python. 

Le code sera exécuté immédiatement dès validation de la ligne.

Testons un peu cela avec les opérations de base entre des entiers :

```Python
>>> 5 + 2
???
>>> 5 - 2
???
>>> 5 * 2
???
>>> 5 / 2
???

```

C'est bien joli tout cela mais on ne peut rien sauvegarder dans cette console ! Il est temps de coder notre premier programme.

### Premier programme

Allez dans le menu déroulant choisir _Fichier / Nouveau_ :

![nouveau fichier](media/untitled.jpg)

Dans la languette en haut, vous lisez _untitled_ : le fichier n'a pas de nom. Pour le nommer, il faut le sauvegarder en suivant le menu _Fichier / Enregistre sous_. Nommez le, par exemple, _premier_programme.py_ :

![premier programme](media/premier_prog.jpg)

Dans la partie liée au code, tapons notre premier petit programme :

```python
a = 1
b = 2
print(a + b)
```

Remarquez que :

* un espace est placé de chaque côté des opérateurs `=`  et `+`. Cela fait partie des bonnes pratiques en Python :
    * toujours mettre un espace de chaque côté des opérateurs.
    * toujours un unique espace uniquement après chaque virgule.
* La fonction `print` permet d'afficher un résultat dans la console.

Pour exécuter le code, cliquez sur la flèche verte de la barre de menu.

* Le code est automatiquement sauvegardé.
* Le résultat éventuel s'affiche dans la console.



Voilà pour une petite présentation de Thonny. Nous apprendrons tout au long de l'année à nous en servir davantage.

_______________

Par Mieszczak Christophe 

Licence CC BY SA

source images : production personnelle.










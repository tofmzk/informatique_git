# Les structures conditionnelles

Si ... alors ... sinon si .... sinon. Vous avez déjà vu ça avec Scratch :



![si](media/si.jpg)

En Python cela se traduit ainsi :

```python
if condition :
    #conséquence1
else :
    #conséquence3
```

* On n'oublie pas les `:`  qui marquent le début de la conséquence.

* Les `#` permettent d'écrire des remarques non prises en compte lors de l'exécution.

* Le code conséquence de la condition est tabulé (ou décalé de 4 espaces) à droite.

* La _condition_ doit valoir soit `True` (vrai) soit `False` (faux) : c'est un booléen (le type _bool_ en Python).

    

### Premier exemple

Regardez le code ci-dessous :

```python
a = int(input('Donnez moi un nombre entier non nul'))
if a != 0 :
    print('l\'inverse de a est ', 1/ a)
        
```

> * Expliquer ce qu'il fait et à quoi sert la condition 
>
> * Ajouter ci-dessous deux lignes de code de façon à ce que le message 'On demandait un entier NON NUL.' s'affiche si a vaut 0.
>
>     ```python
>     a = int(input('Donnez moi un nombre entier non nul'))
>     if a != 0 :
>         print('l\'inverse de a est ', 1/ a)
>     ...
>     	...
>     ```
>
>     



### elif

En Python, il existe un `sinon si` le `elif` contraction de `else if`. On va l'utiliser lorsqu'il y a plus qu'une condition et son alternative.

Regardez le code ci-dessous :

```python
taille = input('Combien mesures tu ?')
taille = float(taille)
if taille > 2 :
    print('Maitre, vous vous prenez pour Chewbacca')
elif taille > 1.4 :
    print('Maitre, vous vous prenez pour Luc Skywalker')
else :
    print('Maitre, vous vous sous-estimez')
```

> * Expliquer ce que fait ce code.
>
> * Ajouter deux lignes de façon à ce qu'une taille entre 0.8m et 1.4m entraine l'affichage "Maitre, ce n'est pas la taille qui compte"
>
>     ```python
>     taille = input('Combien mesures tu ?')
>     taille = float(taille)
>     if taille > 2 :
>         print('Maitre, vous vous prenez pour Chewbacca')
>     elif taille > 1.4 :
>         print('Maitre, vous vous prenez pour Luc Skywalker')
>     elif ...
>     	....
>     else :
>         print('Maitre, vous vous sous-estimez')
>     ```
>
>     

### A vous de coder



> Créer un nouveau fichier _age_yoda.py_ puis codez un programme qui : 
>
> * demande l'âge de Yoda 
>
> * s'il a moins de 100 ans, affiche "Vous un tout jeune maitre !"
>
> * sinon s'il a moins de 200 ans, affiche "Vous êtes adolescent maitre !"
>
> * sinon s'il a moins de 400 ans, affiche  "Vous êtes dans la fleur de l'âge maitre !"
>
> * sinon affiche  "Vous vous faites vieux maitre !"

```python
```



### OU, ET, NON

Lorsqu'on écrit des conditions, on peut utiliser les opérateurs `or`, `and` ou `not`. 

Regarder le code ci-dessous : à quelle condition une année est-elle bissextile ?

```python
annee = int(input('Donnez une année : '))
if annee % 4 == 0 and annee % 100 != 0 or annee % 400 == 0 :
    print(annee,'est une année bissextile.')
else :
    print(annee,'n\'est pas est une année bissextile.')
```



____________________________________________________

Par Mieszczak Christophe 

Licence CC BY SA

source images : production personnelle.










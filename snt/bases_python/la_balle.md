# Ça r'bondit !

Pour introduire les bases du Python, nous allons coder un petit "jeu" : Une balle qui rebondit sur les contour d'un écran et accélère progressivement

Nous parlerons de variables entières,  de booléens, de chaînes de caractères, de structures conditionnelles et du module _Pyxel_. On rencontrera aussi des fonctions mais il est inutile d'en savoir beaucoup sur elles ici.

Nous utiliserons Thonny pour le mener à bien.

## Pyxel

Nous allons utiliser un _module_, c'est à dire une bibliothèque de fonctions, dédié à la création de jeu en 2D : pyxel.

1. Ouvrez Thonny.
2. Enregistrer un nouveau fichier sous le nom _balle.py_ dans le répertoire où se trouve ce cours.



Avec pyxel, la structure du code est toujours la même. Collez le code ci-dessous dans la partie programme (en haut) de Thonny.

```python
# importation des modules nécessaires
import pyxel

#########VARIABLES GLOBALE
#ici on définit toutes les variables globales


###################Calculs indispensable au jeu
def calculer() :
    '''
    ici on effectue tous les calculs nécessaires au jeu
    '''
    

###################Affichage des objets du jeu
def afficher():
    '''
    ici on dessine tous les objets du jeu
    '''


##############lancement du programme
pyxel.init(300, 250, 'Balle')
pyxel.run(calculer, afficher)

```

Voilà comment cela marche : 

*  Le caractère `#` permet de placer des remarques afin de mieux comprendre le code. Ce qui le suit n'est pas pris en compte lors de son exécution.
*  Tout en haut de ce code vous lisez la ligne `import pixel` : cette ligne permet de charger les instructions du module `pyxel` nécessaire à la réalisation du jeu.
*  Juste après l'importation du module _pyxel_, il y a une zone réservée à la déclaration des variables dont nous aurons besoin. On y reviendra plus tard.
*  la fonction `calculer`  détermine un _bloc de code_ où l'on placera tous les calculs nécessaires au déroulement du jeu : trajectoire de la balle, intersections de la balle avec les bord de l'écran ...
*  la fonction `afficher` détermine un _bloc de code_ dans lequelon va dessiner toutes les parties du jeu : le fond, le contour et la balle.
*  Tout en bas vous lisez les deux lignes de codes qui vont lancer le jeu :
    * `pyxel.init(300, 250, 'Balle')` initialise une fenêtre graphique de 300 pixels de large sur 250 pixels de hauts.
    * `pyxel.run(calculer, afficher)` va, environ 30 fois par seconde, appeler deux _fonctions_

Exécutez le programme  :

<img src="media/fenetre.jpg" alt="fenêtre" style="zoom:50%;" />

Vous n'avez pas besoin d'en savoir beaucoup sur les fonctions pour le moment si ce n'est :

* que le _bloc_ fonction commence par `def` suivi du nom de la fonction et de parenthèses.
* que le code à l'_intérieur_ d'une fonction est tabulée (ou décalée de 4 espaces) vers la droite.
* que la suppression marque la fin de la fonction.



Nous en apprendrons plus sur elles au fur et à mesure de l'année.



## Dessiner les contours du jeu

Ajoutons un petit cadre à notre jeu en traçant trois rectangles grâce à l'instruction `pyxel.rect(coin_x, coin_y, longueur, largeur, couleur) `  :

```python
def afficher():
    '''
    ici on dessine tous les objets du jeu
    '''
    pyxel.cls(0)
    pyxel.rect(0, 0, 300, 10, 7)
    pyxel.rect(0, 0, 10, 250, 7)
    pyxel.rect(290, 0, 10, 250, 7)
     

```

Notez bien que les instructions sont décalées de 4 espaces (ou d'une tabulation) à droite : c'est ce décalage qui permet de savoir qu'elles sont dans la fonction et pas en dehors.

Vous pouvez modifier cles couleurs car _pyxel_ permet d'en utiliser 16 que voici :

<img src="media/couleurs.jpg" alt="couleurs" style="zoom:50%;" />



Éxécuter le code pour démarrer le programme.



> Ajouter au code de la fonction `afficher` une ligne pour dessiner un rectangle  en bas de l'écran de 10 pixels de haut sur la largeur de l'écran.



## Afficher la balle

Pour gérer la balle, nous aurons besoin de  variables :

* deux pour les coordonnées de la balle dans la fenêtre : on les nommera _x_ et _y_.
* une pour le rayon de la balle qu'on appellera _r_.
* deux pour définir comment les coordonnées vont varier : on les nommera _dx_ (qui fait varier _x_) et _dy_ (qui fait varier _y_)

Pour commencer, décidons que la balle se place aux coordonnées (150, 125), quelle aura pour rayon 5  et qu'elle va se déplacer d'un pixel vers la droite et d'un pixel vers le haut à chaque appel de la fonction `calculer`.

Pour définir les variables nécessaires, on va se placer en haut du code et y ajouter les lignes suivantes :

```python
#########VARIABLES GLOBALE
#ici on définit toutes les variables globales
x = 150
y = 125
r = 5
dx = 1
dy = 1
```

Vous venez de définir quatre **variables globales**, c'est à dire qui existent dans tout le code, dont **le type est entier** (_int_ en Python, abréviation de _interger_ en anglais)

On va maintenant dessiner la balle dont le centre sera en (_x_, _y_), dont le rayon sera _r_ et dont vous choisirez la couleur (cf image plus haut). Pour cela, l'instruction est `pyxel.circ(abscisse_du_centre, ordonnée_du_centre, rayon, numero_de_la_couleur)`.

Dès qu'on veut afficher quelque chose, on se place dans la fonction `afficher`. ajoutez-y la ligne de code qui permet de dessiner le cercle souhaité :

```python
def afficher():
    '''
    ici on dessine tous les objets du jeu
    '''
    global x, y, r # on utlise les variables globales x, y et r
    pyxel.rect(290, 0, 10, 250, 7)#rectangle de droite
    pyxel.rect(0, 0, 300, 10, 7)#rectangle du haut
    pyxel.rect(0, 0, 10, 250, 7)#rectangle de gauche
    pyxel.rect(0, 240, 300, 250, 7)#rectangle du bas
```

Notez bien que :

* les instructions sont toujours décalées de 4 espaces (ou d'une tabulation) à droite : c'est ce décalage qui permet de savoir qu'elles sont dans la fonction et pas en dehors;
* la ligne `global x, y, r` indique qu'on se sert de nos variables globales définies plus haut. 



Exéctutez le code !



## Bouge de là

Poue faire bouger la balle, il faut modifier les variables _x_ et _y_. Pour cela, on se place cette fois dans la fonction `calculer` :

```python
def calculer() :
    '''
    ici on effectue tous les calculs nécessaires au jeu
    '''
    global x, y, dx, dy, r # précise qu'on utilise les variables globales x, y, dx et dy
    x = x + dx # x devient x + dx
    y = y + dy # y devient y + dy
```

Ici, on utilise les variables globale :

* on ajoute la valeur de _dx_ à _x_.
* on ajoute la valeur de _dy_ à _y_.
* _r_ servira très bientôt

Une fois le code inséré, exécutez le !



La balle laisse une trace derrière elle. C'est moche... Pour éviter cela, il faut effacer la fenêtre à chaque fois avant de tout re-dessiner grâce à l'instruction `pyxel.cls(no_de_couleur)` :

```python
def afficher():
    '''
    ici on dessine tous les objets du jeu
    '''
    global x, y, r # utilise les variables globalesx, y et fin
    pyxel.cls(0)
    pyxel.rect(0, 0, 300, 10, 7)
    pyxel.rect(0, 0, 10, 250, 7)
    pyxel.rect(290, 0, 10, 250, 7)
    pyxel.circ(x, y, r, 2)
```

Testez à nouveau !! (n'oubliez pas d'exécuter le code avant).



> Trouvez puis testez ce qu'il faut modifier pour que :
>
> * la balle se déplace plus vite
> * la balle aille vers le haut
> * la balle aille vers la gauche



## Rebondir

Reprenons nos valeurs de départ pour _dx_ et _dy_ afin que la balle parte vers la gauche et le haut.

A un moment, elle va toucher le rectangle de droite.

La ligne de code qui dessine ce rectangle est `pyxel.rect(290, 0, 10, 250, 2)` : la balle touchera ce rectangle lorsque son centre sera à moins de 5 pixels (son rayon) de 290 !

<img src="media/balle_droite.png" alt="balle touche à droite" style="zoom:33%;" />

![zoom](media/zoom_balle.png)



Ainsi, **si** _x_+ _r_ est supérieur ou égal à 290 **alors** la balle doit rebondir vers la gauche : pour cela il suffit que _dx_ devienne _-dx_. 

La structure conditionnelle en Python est la suivante :

```python
if condition_est_vraie :
    #conséquence tabulée à droite (ou décalée de 4 espaces)
```



Voilà ce que cela donne en utilsant cette structure :

```python
def calculer() :
    '''
    ici on effectue tous les calculs nécessaires au jeu
    '''
    global x, y, dx, dy, r # précise qu'on utilise les variables globales x, y, dx et dy
    x = x + dx # x devient x + dx
    y = y + dy # y devient y + dy
    if x + r >= 290 : # notez les : en fin de ligne
        dx = -dx #notez le décalage qui permet de savoir que c'est la conséquence
```

Exécutez  le code !



> Ajoutez les conditions pour que la balle rebondisse en haut (_dy_ devient _-dy_) et à gauche (_dx_ redevient _-dx_) et en bas (_dy_ devient _dy_).
>
> Testez le code à chaque essais.

### Plus vite

Lors d'un rebond à gauche, si -9 <= dx <= 9 , alors on va multiplier _dx_ par 1.1, c'est à dire l'augmenter de 5% :

```python
def calculer() :
    '''
    ici on effectue tous les calculs nécessaires au jeu
    '''
    global x, y, dx, dy, r
    x += dx
    y += dy
    if x + r >= 290 :
        dx = -dx
        if -10 <= dx <= 10 :
            dx = dx * 1.1
    elif x - r <= 10 :
        dx = - dx
    elif y - r <= 10 :
        dy = -dy
    elif y + r >= 240 :
        dy = -dy
```

Modifier votre code et testez !



> * Compléter le code de façon à ce que _dx_, s'il est compris entre -9 et 9, soit multiplier par 1.1 à chaque rebond gauche.
> * Faites de même pour _dy_ après un rebond en haut ou en bas.



### Fin de partie

Pour quitter _proprement_ le "jeu", on décide qu'il s'arrêtera lorsque l'utilisateur appuiera sur la barre _espace_ su clavier.

La structure conditionnelle ci-dessous fait le job : 

```python
if pyxel.btn(pyxel.KEY_SPACE) : # si on appuie sur la touche space
	pyxel.QUIT() # on quitte pyxel
```

>   
>
> Insérer ce code à sa place à la fin de la fonction `calculer` et testez le !
>
>   

### Interface

Plutôt que de placer la balle toujours au même endroit, toujours avec le même rayon, demandez à l'utilisateur :

*  l'abscisse entière  _x_ de la balle.
* l'ordonnée entière _y_ de la balle.
* le rayon _r_ entier de la balle.






___________

Par Mieszczak Christophe

Licence CC BY SA


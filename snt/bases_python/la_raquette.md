# La raquette

Ceci est la suite du TP [La balle](la_balle.md) qui aboutit au code ci-dessous :

```python
import pyxel

#########VARIABLES GLOBALE
#ici on définit toutes les variables globales
x = 150 # abscisse du centre de la balle
y = 240 # ordonnée du centre de la balle
dx = 2 # variation du x pour déplacer la balle
dy = -2 # variation du y pour déplacer la balle
fin_de_partie = False # vaudra True à la fin de la partie

###################Calculs indispensable au jeu
def calculer() :
    '''
    ici on effectue tous les calculs nécessaires au jeu
    '''
    global x, y, dx, dy, fin_de_partie
    x = x + dx
    y = y + dy
    if x + 5 >= 290 : # rebond à droite
        dx = -dx
    if x - 5 <= 10 : # rebond à gauche
        dx = -dx
    if y - 5 <= 10 : # rebond en haut
        dy = - dy
    if y - 5 > 250 : # fin de partie
        fin_de_partie = True

###################Affichage des objets du jeu
def afficher():
    '''
    ici on dessine tous les objets du jeu
    '''
    global x, y, fin_de_partie
    if not fin_de_partie : # si la partie n'est pas finie
        pyxel.cls(0) # peint la fenêtre en noir
        pyxel.circ(x, y, 5, 7) # dessine la balle
        pyxel.rect(0, 0, 300, 10, 7) # dessine les contours
        pyxel.rect(0, 0, 10, 250, 7)
        pyxel.rect(290, 0, 10, 250, 7)
    else : # sinon (si la partie est finie
        message_de_fin = 'GaMe OvEr'
        pyxel.text(150, 140, message_de_fin, 7)


##############lancement du programme
def jouer():
    '''
    lance le jeu
    '''
    pyxel.init(300, 250, 'PoNg')
    pyxel.run(calculer, afficher)

```

## Les variables

On a besoin de deux variables globales pour les coordonnées du coin haut et gauche de la raquette.

Appelons les `rx` et `ry`. Au départ, la raquette est aux coordonnées (130, 245)

> Ajouter ces deux variables dans la partie _Variables globales_



Nous nous servirons de ces variables dans les fonctions `calculer` et `afficher`

> Ajouter ces deux variables à la liste des variables globales utilisées dans ces deux fonctions, après la commande `global`



## Afficher la raquette

Dans la fonction `dessiner`, ajouter une ligne de code afin de dessiner un rectangle depuis les coordonnées (rx, ry), de longueur 40px, d'épaisseur 5px et de couleur blanche.



Éxécutez le code pour tester vos modifications : au départ, la raquette doit se situer juste sous la balle.



## Déplacer la raquette

Pour déplacer la raquette de gauche à droite, il faut modifier la variable `rx`.

C'est l'utilisateur qui décide comment la raquette se déplace. Il doit pour cela interagir avec le programme.

## On organise le code

Définissez la fonction ci-dessous sous la fonction `calculer` . Nous y gérerons les événements pour déplacer la raquette :

```python
def deplacement_raquette() :
    '''
    gère les événements pour déplacer la raquette
    '''
    global rx, ry # on va utiliser les variables rx et ry qui sont globales
```

On en profite pour ré-organiser notre code en définissant une fonction pour les mouvements de la balle et en y copiant le code qui lui correspond (celui qui est actuellement dans la fonction `calculer`):

```python
def deplacement_balle():
    '''
    calcule les mouvements de la balle
    '''
    global x, y, dx, dy, fin_de_partie #les variables globales dont on a besoin ici
    x = x + dx
    y = y + dy
    if x + 5 >= 290 : # rebond à droite
        dx = -dx
    if x - 5 <= 10 : # rebond à gauche
        dx = -dx
    if y - 5 <= 10 : # rebond en haut
        dy = - dy
    if y - 5 > 250 : # fin de partie
        fin_de_partie = True
```

La fonction `calculer` se simplifie considérablement : on va seulement y appeler ces deux nouvelles fonctions :

```python
def calculer() :
    '''
    ici on effectue tous les calculs nécessaires au jeu
    '''
    deplacement_raquette() #on appelle la fonction pour gérer la raquette
	deplacement_balle() #on appelle la fonction qui déplace la balle
```

Avouer qu'ainsi, c'est beaucoup plus lisible !

## Le clavier

Lorsque l'utilisateur appuie sur une touche du clavier, il déclenche un _événement_.

Les événements qui correspondent à l'appuie sur les flèches du clavier sont :

* `pyxel.btn(pyxel.KEY_RIGHT)` qui vaut `True` si on appuie sur la flèche droite.
* `pyxel.btn(pyxel.KEY_LEFT)` qui vaut `True` si on appuie sur la flèche gauche.
* `pyxel.btn(pyxel.KEY_UP)` qui vaut `True` si on appuie sur la flèche haute.
* `pyxel.btn(pyxel.KEY_DOWN)` qui vaut `True` si on appuie sur la flèche bas.

Dans la fonction `deplacement_raquette` , ajouter le code ci-dessous :

```python
def deplacement_raquette() :
    '''
    gère les événements pour déplacer la raquette
    '''
    global rx, ry #les variables globales dont on a besoin ici
    if pyxel.btn(pyxel.KEY_RIGHT) : #si on appuie sur la flèche droite
        rx = rx + 5 #rx augmente de 5px
```

> * Testez le code !
> * Complétez le code et testez le au fur et à mesure pour que :
>     * la raquette ne dépasse pas du bord droit de l'écran;
>     * la raquette puis se déplacer vers la gauche sans sortir de l'écran;
>     * la raquette puisse monter sans aller en dessous de l'ordonnée 200;
>     * la raquette puisse descendre sans aller au dessus de l'ordonnée 245.

## Renvoyer la balle

Pour l'instant la balle passe à travers la raquette : il faut gérer la collision.

Créez la fonction `collision` ci-dessous :

```python
def collision():
    '''
    gère les collisions balle-raquette
    '''
    global rx, ry, x, y #les variables globales dont on a besoin ici
```

N'oubliez pas d'ajouter la ligne dans la fonction `calculer` pour appeler cette nouvelle fonction.



On rappelle que :

* le centre de la balle a pour coordonnée (x, y) et que son rayon est 5px.
* le coin en haut à gauche de la raquette a pour coordonnées (rx, ry) et que la raquette mesure 40px de long sur 5px de haut.

> * Complétez la condition ci-dessous pour que la balle et la raquette entrent en collision.
>
>     ```txt
>     la partie gauche de la balle doit atteindre la raquette -> x - 5 >= rx
>     la partie droite de la balle ne doit pas dépasser la raquette -> x + 5 <= ...
>     la partie basse de la balle doit atteindre la raquette -> y + 5 >= ...
>     la partie basse de la balle ne doit pas dépasser la raquette -> y + 5 <= ...
>     en résumé il faut :
>     x + 5 >= rx et x - 5 <= ... et y - 5 >= ... et y + 5 <= ...
>     ```
>
> * Que se passe-t-il pour _dx_ et _dy_ lorsque cela arrive ?
>
> * Complétez la fonction`collision` et testez là !

## Évolutions

On va améliorer un peu le gameplay.

### Accélération

Lorsque la balle touche la raquette _dx_ et _dy_ sont multipliée par 1.1 et augmentent donc de 10%.

Le type des variable _dx_ et _dy_ ne sera alors plus entier mais **flottant** : ce seront des nombres à virgule ... ou plutôt à point.

> Modifiez la fonction `collision` pour qu'à chaque collision avec la raquette, la balle accèlère.



### Score

Au départ, on accorde au joueur le généreux score de 1.

Chaque fois que la balle rebondit sur la raquette le score double. 

Ce dernier s'affiche en haut à droite de l'écran.

> Allez hop, on code tout cela !



### Mauvais rebond

Lorsque la balle touche le bord de la raquette (entre 4 pixel côté extérieur et 10px côté intérieur) et que sa direction entraine un choc frontal avec un coin, la balle repart dans l'autre sens, sa vitesse est multipliée par 0.9 (elle diminue de 10%) et le score quadruple.

> Allez hop, on code !

### Slice

Lorsque la balle touche le bord de la raquette (entre 4 pixel côté extérieur et 10px côté intérieur) et que sa direction n'entraine pas un choc frontal avec un coin, la balle repart dans l'autre sens, sa vitesse horizontale est multipliée par 1.2 (elle augmente de 20%) et le score quadruple.

>  Allez hop, on code !

_________

Par Mieszczak Christophe

Licence CC BY SA
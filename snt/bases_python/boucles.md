# Les boucles.

Vous avez déjà travaillé sur les boucles avec Scratch :

![boucle](media/boucle.jpg)



Elles permettent de faire répter un certain nombre de fois des actions (ici avancer et tourner).

En Python, on utilise principalement deux types de boucles : 

* les boucles _pour_.

* les boucles _tant que_.

    

## Boucle Pour

En Python, les boucles `pour` sont assez complexes. Nous en verrons ici une version simplifiée et limitée : celle qui parcourt une plage d'entiers.

La boucle `pour i allant de a (inclus) à b (exclus)` s'écrit ainsi :

```Python
for i in range(a, b):
    #ici on place le code de la boucle
```

* On n'oublie pas les `:`  qui marquent le début de la boucle.
* Le code à l'intérieur de la boucle est tabulé (décalé de 4 espaces) à droite.
* La variable _i_ prendra les valeurs allant de _a_ **inclus** à _b_ **exclus**. 
* _a_ est facultatif s'il vaut 0.

Testez le code suivant dans la console :

```python
>>> for i in range(10) :
    	print('Zut,on tourne en rond !')
```

  

> Créez un fichier _yoda_fait_repeter.py_ puis codez un programme qui demande à Yoda combien de fois il souhaite qu'on répète et affiche ensuite autant de fois que demandé la phrase  "Que la force soit avec toi !".



## Boucle tant que

On traduit la boucle `tant que` de la façon suivante en Python :

```python
while condition :
    # ici on écrit le corps de la boucle
```

* On n'oublie pas les `:` qui marquent le début de la boucle.
* Tant que la condition reste vraie, le code écrit dans le corps de la boucle s'exécutera... en boucle.
* Le code à l'intérieur de la boucle est tabulé (décalé de 4 espaces) à droite.

Une telle boucle est utilisée lorsqu'on ne peut pas savoir à l'avance combien de fois on doit recommencer la boucle. Par exemple, testez le code ci-dessous :

```python
>>> resultat = -2
>>> while resultat != 42 :
    	resultat = int(input('Combien font 6 fois 7 ?'))
```

* Impossible de prévoir combien de tours fera la boucle donc impossible d'utiliser une boucle pour.
* Notez que la variable _resultat_ qui sert dans la condition du `while` doit être définie à l'avance et permettre le début de la boucle.

______

Par Mieszczak Christophe 

Licence CC BY SA

source images : production personnelle.










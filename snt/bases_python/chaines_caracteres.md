# Les chaines de caractères

Bon, il n'y a pas que des nombres dans la vie. Il existe de nombreux _types_ différents de variables. 

Par exemple, les chaines de caractères qu'on définit soit entre deux guillemets `""` soit entre deux apostrophes `''` .

Testez cela dans la console :

```python
>>> chaine_1 = 'Hello World'
>>> print(chaine_1)
???
>>> type (chaine_1)
???
```

_Remarque :_

* Le nom d'une chaine doit toujours être écrit en minuscule, sans accent et sans espace. On peut utiliser le caractère `_` (le underscore sur la touche 8) pour marquer une séparation dans un nom long.
* En Python, les chaines de caractères sont du type **str**, abréviation de _string_ qui signifie _chaine_. Oui ... d'où le nom du célèbre sous-vêtement.
* le `print` permet d'afficher le contenu d'une chaine. 



On peut ajouter deux chaînes : c'est une _concaténation_. Essayez :

```python
>>> a = ' Yoda '
>>> b = 'je suis'
>>> a + b 
???
>>> b + a
???
```

Cependant, on ne peut pas ajouter n'importe quoi :

```python
>>> ma_taille = 1.12
>>> phrase = 'Je mesure ' + ma_taille +'m.'
???
```

Cela ne marche pas car les variables ne sont pas du même _type_. Il est possible d'ajouter ensemble deux nombres ou ensemble deux chaines mais pas un nombre et une chaine. 

Il faut utiliser des conversions : 

* soit la fonction `str(nombre)` qui convertit le nombre en chaine.
* soit la fonction `int(chaine)` qui convertit, si possible, la chaine en entier.
* soit la fonction `float(chaine)` qui convertit, si possible ,la chaine en flottant.

```python
>>> phrase = 'Je mesure ' + str(ma_taille) +'m.'
>>> print(phrase)
???
```



> Corriger et tester le code ci-dessous afin que l'affichage se fasse sans erreur :
>
> ```python
> >>> mon_age = 600
> >>> ma_taille = 1.12
> >>> print('Mon age actuel est ' + mon_age +'ans.')
> ???
> >>> print('Debout sur un tabouret de 20cm, ma taille est ' + ma_taille + 0.2)
> ???
> ```
>
> 



___

Par Mieszczak Christophe

Licence CC BY SA












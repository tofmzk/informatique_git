from capture_donnees import*
import folium

def creer_liste_villes():
    """
    renvoie une liste contenant les coordonnées gps, le numéro de département,
    le nom et  la population des villes répondant à certains critères mpdifiables ci-dessous
    """
    donnees = stockage(charger_donnees('villes_metropole_corse.csv'))
    dept = donnees[0]
    nom = donnees[1]
    code_postal = donnees[2]
    population = donnees[3]
    surface = donnees[4]
    longitude = donnees[5]
    latitude = donnees[6]
    altiMin = donnees[7]
    altiMax = donnees[8]   
    villes_selectionnees = []

    for i in range(0,len(population)):
        ##########################################
        # modifier ici les critères de sélection #
        ##########################################
        if altiMin[i] < 5 and population[i] > 5000:     
            villes_selectionnees.append([[latitude[i] ,longitude[i]], dept[i], nom[i], population[i]])
        
    return villes_selectionnees

        

def creer_carte():
    """
    crée et sauvegarde un fichier html affichant une carte sur laquelle seront placées les villes figurant
    dans la liste villes_selectionnees
    """
    
    villes_selectionnees = creer_liste_villes()
    # on place une sécurité pour éviter la création d'une carte "ingérable"
    if len(villes_selectionnees) <= 400:
        carte = fo.Map(location = villes_selectionnees[0][0],zoom_start=6)
        for i in range(1, len(villes_selectionnees)):
            liste = villes_selectionnees[i][0]
            folium.Map(location = liste, zoom_start = 6).add_to(carte)
            folium.CircleMarker(liste, radius = 2).add_to(carte)

        carte.save('carte_france.html')
    else:
        print("Nombre de villes", len( villes_selectionnees), "important, le fichier HTML risque d'être trop volumineux et le temps de traitement très long !")

def afficher_villes():
    """
    affiche les numéros de département, les noms, la population des villes
    selectionnees dans la liste villes_selectionnees
    """
    liste_villes = creer_liste_villes()
    if len(liste_villes) <= 80:
        for i in range(len(liste_villes)):
            print('Dept: {}  Nom: {}   Population: {} '.format(liste_villes[i][1], liste_villes[i][2], liste_villes[i][3]))
    else:
        print("Nombre de villes supérieur à 80, pas d'affichage, modifier seuil si nécessaire")

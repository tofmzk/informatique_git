import folium as fo

def charger_donnees(file):
    """
    charge le fichier csv et renvoie une liste
    """
    fichier = open(file,'r', encoding = "utf8")
    x = fichier.readlines()
    fichier.close()
    return x



    
def stockage(x):
    """
    stockage de toutes les données dans des listes
    dédiées. Renvoie une liste de listes
    """
    donnees = []
    for _ in range(9):
        donnees.append([])

    for i in range(1, len(x)):
        ligne = x[i].split(';')
        donnees[0].append(ligne[0])
        donnees[1].append(ligne[1])
        donnees[2].append(ligne[2])
        donnees[3].append(int(ligne[3]))
        donnees[4].append(ligne[4])
        donnees[5].append(float(ligne[5].replace(',','.')))
        donnees[6].append(float(ligne[6].replace(',','.')))
        donnees[7].append(int(ligne[7]))
        donnees[8].append(int(ligne[8]))
    return donnees



# Pokémons et données structurées


# Le format CSV



Le format _CSV_ est un format texte utilisé pour sauvegarder des données en utilisant une structure simple.



Les fichiers du TP se trouve dans le dossier `Pokémons`.

Ouvrez, **avec un éditeur de texte** (Notepad++, Geany, Sublime text ...) , le fichier _pokemon_stats_1.csv_ . 



Voici un extrait des données de ce fichier :

```csv
name;classification;attack;defense
Swablu;Cotton Bird Pokémon;40;60
Budew;Bud Pokémon;30;35
Minun;Cheering Pokémon;40;50
Metapod;Cocoon Pokémon;20;55
Chikorita;Leaf Pokémon;49;65
Pinsir;Stagbeetle Pokémon;155;120
Cinccino;Scarf Pokémon;95;60
Elgyem;Cerebral Pokémon;55;55
Foongus;Mushroom Pokémon;55;45
Ariados;Long Leg Pokémon;90;70
Aipom;Long Tail Pokémon;70;55
Accelgor;Shell Out Pokémon;70;40
Dialga;Temporal Pokémon;120;120
Starly;Starling Pokémon;55;30
Ledyba;Five Star Pokémon;20;30
Politoed;Frog Pokémon;75;75
Dodrio;Triple Bird Pokémon;110;70
Aggron;Iron Armor Pokémon;140;230
Mankey;Pig Monkey Pokémon;80;35
```



Il s'agit d'une base de données au sujet des Pokémons, vous l'aviez deviné !

![logo pokémon _ source wikipedia - public domain](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/269px-International_Pok%C3%A9mon_logo.svg.png)

La base nous donne des informations sur ces petites bestioles selon plusieurs critères appelés `attributs`, ou `champs` présents dans la première ligne et séparés par des `;`

* `name ` désigne bien sûr le nom des bêbêtes.
* `classification` nous précise la classe du Pokémon.
* `attack` sa force d'attaque. Plus elle est élevée, plus le Pokémon est dangereux.
* `defense` son niveau de défense. Plus il est élevé, plus le Pokémon est résistant.



Sous les `attributs` , on trouve leurs `valeurs` elles aussi séparées par un `;`.



Remarquez que le _séparateur_ n'est pas toujours un `;`. C'est parfois une `,` ,par exemple après un enregistrement de _LibreOffice_.calc.



Pourriez-vous me dire, en observant ces données :

* Combien y-a-t-il de Pokémons ?

* Quelles sont les caractéristiques de _Comfey_ ?

* Qui a la meilleure défense ?

* Combien de Pokémons ont une attaque supérieure à 70 ?

* Parmi les Pokémons qui ont une attaque supérieure à 140, quels sont les trois meilleurs en défense ?

* Existe-t-il des Pokémons dont l'attaque et la défense son supérieur à 100 ? Si oui, combien ?

  
  
  Pas facile ... Heureusement, il y a d'autres moyens de visualiser et de traiter ces données.



# Avec un tableur



Ouvrez le fichier _pokemon_stats_1.csv_ avec un tableur (celui de la suite _libreOffice_ par exemple, pour utiliser un logiciel libre).

Notez :

* Que le jeu de caractères choisi est l'_UTF-8_.

![ouverture tableur](media/tableur1.png)



Ce dernier affiche alors les données sous la forme d'un tableau : c'est déjà plus facile de s'y retrouver.



![aperçu no1](media/tableur2.png)



Utilisez la barre de défilement pour déterminer l'effectif de Pokémons :

 ```

 ```





On peut effectuer diverses opérations sur nos colonnes. Pour chercher le Pokemon nommé _Comfey_, le plus simple est de réaliser un tri sur l'attribut `name`, c'est à dire sur la colonne A, puis de chercher où est _Comfey_ :

* Sélectionnez à la souris les 4 colonnes.

* Suivez le menu `Données/Trier ...`

* Choisissez de trier selon la colonne `name`, par ordre croissant.

    ![tri croissant](media/tri.png)

    

* Maintenant que la colonne A est dans l'ordre alphabétique, retrouvez _Comfey_ et ses caractéristiques. **ALLEZ-Y  et notez votre réponse ci-dessous :**

    ``` 
    
    ```

    

Pour trouvez qui a la meilleure défense, il faut trier par ordre décroissant avec la clé de tri `attack`. **ALLEZ-Y  et notez votre réponse ci-dessous :**

```

```



Pour savoir combien de Pokémons ont une attaque supérieure à 70, on va utiliser la fonction `nb.si (plage; critère)` :

* `plage_`désigne l'ensemble des cellules concernées. On peut la sélectionner à la souris ou l'écrire sous la forme, par exemple, _B2:B10_.
* `critère` désigne la relation à vérifier. Il se note entre guillemets. Par exemple "= 50"  ou ">90".
* la fonction renvoie le nombre de cellule(s) dans la plage qui vérifie(nt) le `critère`. 

**ALLEZ-Y  et notez votre réponse ci-dessous :**

```

```





* Pour savoir, parmi les Pokémons qui ont une attaque supérieure à 140, quels sont les trois meilleurs en défense, il faut :

    * _Trier_ le tableau complet avec comme clé de tri `attack`.
    * _Extraire_ les Pokémons dont l'attaque dépasse 140 en copiant-collant la zone adéquate dans une autre feuille du tableur.
    * Dans cette autre feuille trier à nouveau, par ordre décroissant, en utilisant la clé adéquate.

    **ALLEZ-Y  et notez votre réponse ci-dessous :**

    ```
    
    ```

    

* Pour la dernière partie, vous savez déjà tout ce qu'il faut savoir pour vérifier l'existence de Pokémons dont l'attaque et la défense sont supérieurs à 100 et trouver leur nombre.

    **ALLEZ-Y  et notez votre réponse ci-dessous :**

    ``` 
    
    ```

    



​    

## Utilisons Python (avec pandas)



Lancez un éditeur Python, comme _Thonny_, puis importez _pandas_. La librairie _pandas_ permet de manipuler et visualiser des données, notamment des données au format csv.



#### Mettre les données au bon format

Commençons par ouvrir le fichier _pokemon_stats_1.csv_  et par stocker ses données dans une variable , par exemple _poke1_ :

```Python
>>> import pandas
>>> poke1 = pandas.read_csv('pokemon_stats_1.csv',sep=';')
>>> poke1
???
>>> poke1.head(25)
???
>>> poke1.tail(10)
???
```

_head(n)_ permet de visualiser les _n_ premiers pokémons, _tail(n)_ les _n_ derniers. 


La base contient des données rangées sous des `attributs` dans les colonnes. Pour se remémorer la liste des attributs, et leur nature, on utilise  `dtypes`:

```python
>>> poke1.dtypes
???
```



Pour récupérer la liste des valeurs d'un attribut, on utilise `poke1['name']`

```python
>>> poke1['name']
??? ouch...
```

Nous pouvons réaliser des tris, calculer des sommes, des maxima, ...

```python
>>> poke1.sort_values('attack')
???
>>> poke1.sort_values('attack',ascending=False)
???
>>> df.sort_values(['attack','defense'],ascending=False)
???
>>> poke1['attack'].sum()
???
```



#### Répondons aux questions



On peut déterminer l'effectif de notre base de données de Pokémons, ou le nombre de valeurs distinctes dans une colonne :

```python
>>> len(poke1)
???
>>> poke1['name'].nunique()
???
>>> poke1['classification'].nunique()
???
>>> poke1['attack'].nunique()
???
```



Pour retrouver notre Pokémon _Comfey_, on cherche tous les pokemons dont le nom est égal à _Comfey_ :

```Python
>>> poke1[poke1['name'] == 'Comfey']
???
```

On peut aussi chercher tous les Pokémons dont la défense est supérieure à 200 :


```Python
>>> poke1[poke1['defense'] > 200]
???
```


Pour déterminer qui a la meilleure défense, on commence par tester la méthode `max()` :

```Python
>>> poke1['defense'].max()
???
```



On peut chercher les Pokémons qui ont 230 points de défense, mais on peut aussi chercher ceux qui ont une défense égale à la défense maximale : 

```Python
>>> poke2 = poke1[poke1['defense' == poke1['defense'].max()]
>>> poke2
???
```




À vous! Quel est le nombre de Pokémons dont l'attaque est supérieure à 70 ? Vous pouvez le faire en 2 commandes (trouver tous les Pokémons que l'on veut, puis les compter), ou en une seule.

```Python
>>> poke2 = poke1[???]
>>> ???
???
```





Pour déterminer, parmi les Pokémons dont l'attaque dépasse 140, les trois meilleurs en défense, il faudra extraire les Pokémons intéressants, puis les trier par rapport à leur valeur d'attaque dans l'ordre décroissant. Allez y !

```Python
>>> 
>>> 
```





Pour trouver le nombre de nos Pokémons qui ont une attaque et une défense supérieures à 100, il faudra enchaîner les deux conditions puis déterminer l'effectif de la base qu'on aura extraite.

```Python
>>> ???

```

_PS : On peut le faire en une seule ligne ..._



## Une deuxième base



#### Avec un tableur



Retournons dans notre tableur. Ouvrez cette fois le fichier `pokemon_stats_2.csv`:



Quels sont les attributs de cette base ?

```

```



Grâce au tableur, déterminer et copier ci-dessous les plus gros des Pokémons.

```

```



Quel est le poids moyen d'un Pokémons ?

On pourra utiliser la fonction `somme(plage)`

```

```



Combien y-a-t-il de types de Pokémons différents ? 

Pour répondre à cette question, on pourrait utiliser un tri... Lister les types ci-dessous :

``` 

```



#### Retour vers Python (avec pandas)



Saurez-vous utiliser _pandas_ pour répondre à nouveau aux questions ci-dessus ?



Commençons par lire le fichier :

```python
>>> poke2 = pandas.read_csv('pokemon_stats_2.csv',sep=';')
```



Quels sont les attributs et leur nature de cette base ?

```python
>>> ???
???
```



Quels sont les plus gros des Pokémons ? Combien sont-ils ?

```python
>>> ???
???
>>> ???
???
```



Pour calculer le poids moyen d'un Pokémon, il faut réaliser une somme sur l'attribut correspondant. Pour cela, on utilise la méthode `sum`. Il faudra également déterminer l'effectif de _poke2_. 

```python
>>> ???
>>> poke2['weight_kg'].sum()
???
>>> poids_moyen = ???
>>> poids_moyen
???
```



Combien y-a-t-il de types de Pokémons différents ? Rappelez-vous de _nunique()_. 

``` python
>>> ???
???
```

Pour les lister, il vous faudra sélectionner la colonne _type_ puis supprimer les doublons :

``` python
>>> poke2['type'].drop_duplicates()
???
```



## Joindre les 2 bases



On se pose maintenant quelques questions :

* Les Pokémons les plus gros sont-ils les plus forts ?
* Y-a-t-il un type particulier qui produit les Pokémons qui ont les meilleures valeurs en défense ?



Le problème est que les données nécessaires sont réparties dans deux bases différentes : on a besoin de les joindre pour répondre à nos questions !



* Considérons la première base : Existe-t-il un attribut capable d'identifier de manière unique une ligne de notre base de donnée ? Un tel attribut est appelé **identifiant**, ou **clé primaire**.

    ```
    
    ```

    

* Pour joindre nos deux bases, il faudrait qu'elles aient un attribut commun qui soit un identifiant dans au moins une des deux bases. Y-en-a-t-il un ?

    ```
    
    ```

    

    Reste à faire correspondre les valeurs des deux bases ... Des idées ???
    
    



#### Avec un tableur



Vous allez commencer par ouvrir les deux fichiers _csv_ sur lesquels on travaille puis copier les deux tableaux côte à côte.



Il faut maintenant faire correspondre les attributs. Pour cela, triez **séparément**, les deux tableaux selon l'attribut `name` : les colonnes correspondent dorénavant !



Supprimez les colonnes inutiles pour plus de lisibilité.



Répondez maintenant aux questions ci-dessous en utilisant les tris adéquats.

* Les Pokémons les plus gros sont-ils les plus forts ?
* Y-a-t-il un type particulier qui produit les Pokémons qui ont les meilleures valeurs en défense ?



```

```




#### En Python (avec pandas)



chargez les données des deux fichiers  _pokemon_stats_1.csv_ et _pokemon_stats_2.csv'_ dans _poke1_ et _poke2_.

```python
>>> ???
>>> ???
```



Cherchons un identifiant :

```python
>>> len(poke1)
???
>>> poke1['defense'].nunique()
???
>>> poke1['name'].nunique()
???
>>> poke2['name'].nunique()
???
```



Pour joindre les deux bases selon l'identifiant `name`, on utilise la méthode `merge` :

```python
>>> poke3 = pandas.merge(poke1,poke2, on='name', how='inner')
>>> len(poke3)
???
>>> poke3
???
>>> poke3.dtypes
???
```



Nous allons maintenant sélectionner uniquement les attributs qui nous intéressent : 

```python
>>> poke3 = poke3[['type','defense']]
>>> poke3
???
```



Il n'y a plus qu'à effectuer les tris adéquats pour répondre aux questions.



* Les Pokémons les plus gros sont-ils les plus forts ?

    ```python
    >>> ???
    ```

    

* Y-a-t-il un type particulier qui produit les Pokémons qui ont les meilleures valeurs en défenses ?

    ```python
    >>> ???
    ```



## Construisons une petite base



Selon leur type, chaque Pokémon a un pire ennemi :

|  type    |  worst_enemy    |
| :--: | :--: |
|water | electric|
|flying|dragon|
|dark|fairy|
|fairy|ghost|
|electric|ground|
|poison|fighting|
|psychic|rock|
|fire|water|
|ground|flying|
|rock|water|
|dragon|poison|
|ice|fire|
|grass|flying|
|normal|poison|
|steel|fire|
|fighting|ghost|
|ghost|psychic|



> * Avec un éditeur de texte (ou un copier- coller sous _libreOffice_), créez le fichier _csv_ correspondant à ce tableau
>
> * Sauvegardez le sous le nom _pokemon_enemies.csv_



Remarquez que, dans cette base, l'attribut ``type`` est un identifiant : nous allons pouvoir nous en servir pour fusionner cette base à une autre.



Nous allons nous en servir pour fabriquer un fichier unique _pokemons.csv_ contenant toutes les données à propos des Pokémons en joignant les trois bases contenues dans les fichiers :

* _pokemons_stats_1.csv_
* _pokemons_stats_2_.csv_
* _pokemon_enemies.csv_



_Remarque importante ! :_

Attention au séparateur utilisé dans votre fichier _csv_ :



* pour _pandas_, le séparateur par défaut est la virgule, mais quand vous utilisez `read_csv` il suffit de préciser dans le paramètre _sep_ le séparateur choisi.

	```python
	>>> poke3 = pandas.read_csv('pokemon_ennemies.csv')  # si le séparateur est une ,
	```
	 ou

	```python
	>>> poke3 = pandas.read_csv('pokemon_ennemies.csv',';') # si le séparateur est un ;
	```

	





> Quel type de Pokémon est le plus redouté par l'ensemble de nos Pokémons ?
>
> (C'est à dire quel type est le plus représenté dans l'attribut `worst_enemy` ?)





_______

Licence CC-BY-SA

Mieszczak Christophe, avec l'aide précieuse d'Anne Parrain de l'université Artois

pour la formation académique SNT de l'académie de Lille








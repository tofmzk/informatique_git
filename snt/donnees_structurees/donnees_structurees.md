# Les données

### Les données sont partout

Nous avons vu que les sites web sont référencés dans des bases de données qu'utilisent le moteurs de recherche lorsque vous faites des requêtes, que vos données personnelles sont collectées en masse à chacun de vos _clics_ lorsque vous naviguez sur la toile, utilisez les réseaux sociaux, commandez un article en ligne... 

Les bases de données, si elles ont parfois un côté dérangeant, sont indispensables à la recherche scientifiques (entre autre) qui s'appuie sur elles la plupart du temps. Les avancées réalisées dans le domaine de l'IA, par exemple, n'existeraient pas sans elles.



**Les données sont partout, en quantité astronomique !!**



Quand on voit la difficulté qu'on peut avoir à gérer un simple tiroir mal rangé et rempli de chaussettes dépareillées, on peut se poser deux questions :

* Comment stocker/ranger ces données de manière efficace ?
* Comment les utiliser tout aussi efficacement ?



Il existe de nombreuses façon de faire cela. En terminale, en NSI, nous parlerons du langage SQL. En seconde, nous allons nous intéresser aux **données en table** que l'on stocke dans un tableau.



### Généralités



Pour décrire un objet (individu, voiture, entreprise, état etc...) on peut utiliser des mots (chaines de caractères) ou des nombres: on les appelle des **données**. 

Cependant, une donnée brute ne sert pas à grand chose. Pour pouvoir l'exploiter et la traitée, il est impératif de les structurer !



|     Type de données     |                       Caractéristiques                       |                      Métaphore associée                      |
| :---------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
|   Données structurées   | Organisation logique, format identique, appréhendable par un ordinateur. |            Couverts rangés dans un tiroir précis             |
| Données non structurées | Entassement d'objets divers sans logique, formats différents, nécéssite un traitement humain | des choses entassés avec d'autres babioles dans une caisse dans une brocante |





### Données structurées
Regrouper des données de manière organisée (donc structurée) permet d'en faciliter l'exploitation et permet de produire de nouvelles informations. 



Ainsi, une voiture peut se caractériser par les données "VW", "POLO", "ESSENCE", "95600", "2013" pour décrire respectivement la marque, le modèle, l'énergie, le kilométrage et l'année de mise en circulation. Ces caractéristiques sont appelées **descripteurs**.  Les données **qualitatives** se présentent sous forme de chaines de caractères, les données **quantitatives**, sous forme de nombres.





| Marque  | Modèle | Energie     | Km    | Année |
|---------|--------|-------------|-------|-------|
| VW      | Polo   | Essence     | 95600 | 2013  |
| Renault | Zoé    | Electricité | 12300 | 2017  |
| Peugeot | 308    | Diesel      | 56700 | 2015  |



Le tableau ci-dessus utilise 5 **descripteurs** pour décrire des véhicules.  Ces véhicules partageant les mêmes **descripteurs**, constituent une **collection**. 



Une **collection** est généralement représentée sous forme d'une table (ou d 'un tableau). Chaque ligne de la table correspond à un objet, l'intersection d'une ligne (**objet**) et d'une colonne (**descripteur**) est une **donnée**. 



Une **base de données** regrouppe plusieurs **collections** et permet de faire des opérations entre ces **collections** (tris, recoupements ...)



> Questions :
>
> + Combien y a t-il des données dans le tableau précédent ?  
> + Combien y a t-il de données numériques ?





_Remarques concernant le programme de statistique en mathématiques_ :

La statistique a son langage propre souvent lié à l'étude de "l'homme". Ainsi, pour le tableau donné en introduction sur les véhicules, on peut établir les correspondances suivantes:

| Statistique           | SNT                  |
| :-------------------- | :------------------- |
| Population            | Collection           |
| Caractère             | Descripteur          |
| Individu              | Objet                |
| Valeur                | Valeur               |
| Caractère qualitatif  | Chaine de caractères |
| Caractère quantitatif | Nombre               |



### Métadonnées :

Les métadonnées sont des de informations associées aux données (structurées ou non) qui les décrivent , permettent de les retrouver ou de les trier.

Par exemple, la date de sauvegarde, l'auteur d'un mail permet de retrouver un mail dont on connait l'auteur ou la date d’envoi.



> Trouvez des exemples de formats qui enregistrent, en plus des données nécessaires à ces formats, des données supplémentaires.



 

### Le stockage des données

Où et comment sauvegarder ses données ?

Les techniques ne dates pas d'hier et ont beaucoup évolué :

- L'ancêtre : [la carte perforée](https://fr.wikipedia.org/wiki/Carte_perfor%C3%A9e)

- la sauvegarde locale sur :

    - la  [disquette](https://fr.wikipedia.org/wiki/Disquette), dès à la fin des année 60, ne stockaient que quelques centaines de kilooctets.

        ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Diskettes.jpg/439px-Diskettes.jpg?20111008200417)

    - le [dique dur](https://fr.wikipedia.org/wiki/Disque_dur) qui date de 1956  et a évolué e tcontinue d'évoluer. Il disposait de quelques mégaoctets avec un coût de 10000 dollars/mo et propose aujourd'hui plusieurs terraoctets de stockage pour un coût 

        ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/IBM_old_hdd_mod.jpg/714px-IBM_old_hdd_mod.jpg?20051214110624)

    

    - le [cdrom](https://fr.wikipedia.org/wiki/CD-ROM) dans les année 80 qui évoluera en DVD puis blueray permettront d'enregistrer de quelques centaines de méga-octets à quelques gigaoctets.

        ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/CD-R_Back.jpg/621px-CD-R_Back.jpg?20050418222430)

    - la clé USB que vous connaissez tous, est rapidement passée de quelques centaines de kilooctets à des centaines de gigaoctets

    - Les disques SSD à cheval entre le disue dur et la clé usb sont actuellement ce qui se fait de plus rapide pour un ordinateur personnel.

- la sauvegarde distante : 

    - sur un cloud (synchronisé ou pas, partagé ou pas)  

    - dans des [data centers](https://fr.wikipedia.org/wiki/Centre_de_donn%C3%A9es) , des _fermes_ de serveurs reliés à internet et gros consommateurs d'énergie, qui propose des capacités de stockage [dépassant la 100 pétaoctets](https://www.tomshardware.fr/stocker-60x-internet-dans-un-seul-data-center/)

        


_Petit rappel :_

1ko = ..........o

1mo = ......ko

1go = .........mo

1to = .........go

1po = .......to



> * Par quel facteur la capacité de stockage d'une disquette de 1970 (environ 500ko) a été multipliée pour arrivée à un disque SSD de 2to que l'on trouve autour de 300€ dans le commerce ?
> * Quel est le prix au mo d'un tel disque SSD ?
> * De combien de disque de 500go a-t-on besoin pour ouvrir un data center de 100po ?



_Bien choisir son stockage_ :



> Quel stockage choisir pour :
>
> * Un maximum de rapidité ?
> * Un maximum de sécurité en terme de protection des données ?
> * Un maximum de sécurité en terme de pérénité des données (pas de risque de perte lié au support) ?
> * Un minimum de consommation énergétique ?
> * avoir un accès aux données facilement depuis différentes machines.







### Aspect légal  

Un aspect déjà traité dans la partie réseaux sociaux :

- la [CNIL ](https://fr.wikipedia.org/wiki/Commission_nationale_de_l%27informatique_et_des_libert%C3%A9s) est la commission nationale de l'informatique et des libertés. Elle est chargée de veiller à ce que l'informatique soit au service du citoyen et qu'elle ne porte pas atteinte à l'identité humaine, aux droits de l'homme, à la vie privée et aux libertés individuelles. RGPD

- Depuis 2016, le [RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es) est le réglement de l'union européenne en matièrede données à caractère personnel.

- En 2018, le [cloud act](https://fr.wikipedia.org/wiki/CLOUD_Act) est la réponse des États-Unis au RGPD. Il contraint  les fournisseurs de services établis aux états-unis de fournir toute donnée stockée sur leurs serveurs sur mandat.





## Sitographie
##### Lien vers conférence IGEN
lien vidéo formation nationale février 2019: https://magistere.education.fr/dgesco/course/view.php?id=1538&section=9



##### Liens vers les sites fournissant les données relatives à l'énergie

+ Dépendance énergétique UE: http://www.nosdonnees.fr/dataset/d-pendance-nerg-tique-des-pays-de-l-union-europ-enne  
La dépendance des pays européens en énergie fossile de 1990 à 2011
Le taux de dépendance énergétique indique la part de l’énergie qu’une économie doit importer. En cas de taux de dépendance négatif, l’économie est dite «exportatrice nette d’énergie». Un taux de dépendance supérieur à 100 % indique que des produits énergétiques sont stockés. 

+ Données sur énergies: https://www.lelementarium.fr/focus/energie/

+ Relevés températures Strasbourg NASA: https://data.giss.nasa.gov/tmp/gistemp/STATIONS/tmp_615071900000_5_0_1/station.csv

+ Relevés températures Sydney (Australie) NASA: https://data.giss.nasa.gov/tmp/gistemp/STATIONS/tmp_501947670000_5_0_1/station.csv

    


##### Lien vers la caractéristiques des communes de France:

+ Villes avec dénominations seules: https://sql.sh/ressources/sql-villes-france/villes_france.csv



##### Liens pour stockage et/ou aspects juridiques 
Lien Marianne sur RGPD et CLOUD ACT: https://www.marianne.net/monde/cloud-act-malgre-la-rgpd-les-etats-unis-l-assaut-de-vos-donnees-personnelles

Autre lien sur le même sujet (plus technique):  
https://www.mindnews.fr/article/14396/le-cloud-act-americain-est-il-compatible-avec-le-rgpd-europeen/

Pour le stockage des données et les aspects juridiques: https://www.assistancescolaire.com/eleve/2nde/sciences-numeriques-et-technologie/reviser-le-cours/2_snt_04



##### Liens pour usage du numérique/stockage et consommation d'énergie

+ LeMondeInformatique: Le numérique, c'est vraiment propre ?.
https://www.lemondeinformatique.fr/actualites/lire-le-numerique-c-est-vraiment-propre-77546.html
+ Les Echos START: Cet article émet autant de CO2 que manger du poulet pendant un mois.
https://start.lesechos.fr/actus/environnement/cet-article-emet-autant-de-co2-que-manger-du-poulet-pendant-un-mois-16770.php#xtor=RSS-13

______________

Chrsitophe Mieszczak

A partir des documents de Luc Bournonville CC-BY-SA

source images :

* [disquette - wikipédia](https://commons.wikimedia.org/wiki/File:Diskettes.jpg)
* [disque dur - wikipedia](https://commons.wikimedia.org/wiki/File:IBM_old_hdd_mod.jpg)
* [cdrom-wikipédia](https://commons.wikimedia.org/wiki/File:CD-R_Back.jpg)
* [ferme de serveur nasa - wikipedia](https://commons.wikimedia.org/wiki/File:Us-nasa-columbia.jpg)

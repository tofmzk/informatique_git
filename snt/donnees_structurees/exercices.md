# Exercices

Les fichiers se trouvent dans le dossier _ressources_.

##### Exercice 1

En utilisant les données du fichier `energie_solaire.csv`, vérifier que le rapport puissance / surface est constant proche de 700. 

##### Exercice 2

À l'aide du fichier `production_par_annee_france.csv`:

+ Créer deux colonnes dans lesquelles on calculera la part de l'énergie nucléaire et de l'énergie hydraulique par rapport à la production totale.  
+ Tracer les courbes des ces rapports en fonction de l'année. 

##### Exercice 3

Le fichier `reserve_tep_2018.csv` fournit les réserves avérées en énergie fossiles des principaux producteurs.

+ Calculer le total par pays (et le total monde).
+ En 2018, la consommation mondiale a été de 13865 millions de tonnes équivalent pétrole.  DE combien d'années de réserves l'humanité dispose t-elle en s'appuyant sur la consommation de 2018 ?

##### Exercice 4

Le fichier `production_tep_2016.csv`fournit (en milions de tonnes équivalent pétrole) la production de le France , de l'Europe et du Monde suivant le type d'énergie.  
On souhaite que les sélecteurs soient les énergies et désignent ainsi les colonnes.

+ Sélectionner toutes les données, couper. 
+ Procéder à un "collage spécial" en choisissant `transposer`.
+ Calculer les productions totales. 
+ Réaliser un histogramme comparatif entre la France et le monde. 

##### Exercice 5

Le fichier `temp_strasbourg_france_1949-2019`fournit les températures moyennes de la ville de Strasbourg par mois, par trimestres et par années.

+ Constuire la courbe des températures moyennes annulles en fonction de l'année. 

+ Ajouter une `courbe de tendance` en sélectionnant `linéaire`et en affichant l'équation. 
+ De combien de degré(s), s'élève la température par année selon ce modèle ? Quelle température moyenne peut-on prévoir en 2030 ? en 2050 ?  
+ Comparer avec les villes de Sidney en Australie et Santiago au Chili.

##### Exercice 6

Le fichier `villes_metropole_corse.csv` fournit les coordonnées GPS, les altitudes minimales et maximales,le code postal, la superficie et le nombre d'habitants (2010) des villes de France métropolitaine et des deux départements corses.

+ Réfléchir à l'opportunité d'utiliser des  nombres pour le code postal.
+ Calculer la population totale en 2010.
+ Compter le nombre de communes de plus 10000 habitants
+ Compter le nombre de communes dont l'altitude minimale est inférieure à 10 m.

##### Exercice 7 : avec Python et module folium

⊥ Lien avec géolocalisation

Création d'une carte de France sous forme de page web avec des villes répondant à certains critères. 

Dans le dossier `python`se trouve un programme `selectionner_villes.py`. Ce programme charge l'ensemble des données du fichier `villes_metropole_corse.csv`. Il permet de créer une page web (`carte_france.html`) sur laquelle apparaissent les villes correspondant à certains critères modifiables en ligne 25.  Pour créer la page web, il suffit de lancer la fonction `creer_carte()`.  
Attention: pour une altitude inférieure à 10m comme seul critère, le nombre de communes est important et la page web créer comporte 106 000 lignes pour 5 Mo. Le temps de conception est très long et le chargement de la page délicat, un plantage du PC est à prévoir. une sécurité a donc été placée à un maximum de 400 communes. Ce seuil est modifiable en ligne 40.  
Par ailleurs, une fonction `afficher_villes()`, affichera les villes (dept, nom, nb d'habitants) dans la console (avec limite de taille à 80 (modifable) cette fois.
![exemple rendu](/home/t450/Documents/Boulot/informatique_git/snt/donnees_structurees/media\france.png)



##### Exercice 8 : Activité sur les données personnelles

⊥ Lien avec le web

+ Ouvrir la page `cookie.html`dans le navigateur Firefox ou cliquer [ici](../cookie/cookie.html); 
+ Remplir le formulaire et valider la saisie en cliquant sur `Valider`.
+ Ouvrir l'outil de développement Web en appuyant sur F12.  
+ Dans la rubrique `Stockage`, cliquer sur les liens apparaissant dans les rubriques suivantes et inspecter le contenu:  
 + Cookie  
 + Stockage de session
 + Stockage local.
+ Que remarque t-on ? Quelle est la date d'expiration du cookie (ajouter 1 h) ?
+ Fermer le navigateur, relancer la page sans remplir le formulaire et inspecter à nouveau la rubrique `Stockage`.  
+ Quelles données sont encore présentes ?  
+ Quand l'heure d'expiration du cookie est atteinte, rouvir à nouveau la page et observer le cookie sans remplir le formulaire.
 + Le cookie a t-il réellement disparu ?
 + Valider le formulaire sans le remplir et observer à nouveau le cookie.
# Les fichiers _csv_

### Késako ?

Un tableau peut être représenté à l'aide d'un tableur comme _Libreoffice Calc_ ou  _Excel de Microsoft_. Ces logiciels enregistrent les données sous différents formats de fichiers (.ods, .xlsx etc..).

 Dans ce cas, comme pour les images, des métadonnées peuvent être ajoutées au contenu du fichier. Cependant, un format pratique est le **.csv**, signifiant _comma separated values_ pour valeurs séparées par des virgules. 

Dans un fichier .csv, les données sont présentes sous forme de texte séparées par un caractère qui peut être la virgule mais on emploie plus souvent le "point-virgule". Il s'agit surtout d'employer un caractère de séparation qui n'est employé dans aucune case du tableau. 

Un simple editeur de texte comme le `bloc notes` ou `notepad++` permet de visionner le contenu du fichier.
Ainsi, la table précédente a été enregristrée dans le fichier `voiture.csv`. Le contenu, vu par Notepad++ est le suivant:

![image notepad](media/voiture.png)


### Utilisation d'un tableur
Le tableur `calc`de la distribution gratuite `LibreOffice`permet de lire les fichiers `.csv`et de manipuler les données. 
##### Chargement d'un fichier .csv
+ les fichiers se trouvent dans le dossiers _ressources_.
+ Ouvrir le fichier souhaité à l'aide de l'onglet `Fichier`
+ Au chargement du fichier, une fenêtre comme celle-ci s'ouvre:  
![copie calc](media\calc.png)
+ Dans le menu `Jeu de caractères`, choisir `Unicode UTF-8` .
+ Dans `Options de séparateur`, ne sélectionner que  `Point-virgule` (ou autre si besoin).  
On obtient une grille comme celle-ci:  
![image fichier  voiture csv](media\voiture_csv.png)



> Observer la manière dont sont écrites les données dans les colonnes.
> Quelle remarque peut-on formuler ?



##### Quelques fonctionnalités du tableur:
+ tri de données (par ordre croissant ou décroissant (suivant plusieurs colonnes)  
+ calcul de moyennes, d'écarts-types, des minima, maxima...  
+ construction des diagrammes en bâtons, de camembert, de courbes...
+ réalisation de tests  
+ ....



> Rendez-vous dans les [exercices](exercices.md)





______________

Chrsitophe Mieszczak

A partir des documents de Luc Bournonville CC-BY-SA

source images : productions personnelles


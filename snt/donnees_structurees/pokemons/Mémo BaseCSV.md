# Mémo BaseCSV

### initialisation et sauvegarde

```python
>>> base = BaseCSV(nom_fichier.csv, separateur = ';')
nomfichier.csv type str
separateur vaut ';' par défaut. A préciser si le séparateur de la vase est différent

>>> base.sauver(nom_fichier.csv)
sauvegarde la base sous le nom spécifié
```





### Accesseurs

```Python
>>> base.get_cles() 
renvoie la liste des clés de la base

>>> base.get_valeurs(cle, doublon = False)
renvoie la liste des valeurs de la clé sans doublon par défaut, avec doublons si doublon vaut True.

>>> base.get_types()
renvoie le dictionnaire des types des clés 'str', 'num', 'heure' ou 'date'

>>> base.set_type(cle, type_de_la_cle)
convertit, si possible, la clé précisée dans le type demandé 'str', 'num', 'heure' ou 'date'

>>> base.set_donnees(dictionnaire_de_donnees)
ajoute, si possible, la ligne contenue dans le dictionnaire avec des clés valides, à la base.
```



### Prédicat

````python
>>> base.est_primaire(cle)
Renvoie True si la cle est primaire dans la base et False sinon
````



### Méthodes réservées

````python
>>> print(base)
affiche la base dans la console sous forma d'un tableau

>>> base
décrit le contenue de la base

>> len(base)
renvoie le nombre de lignes de la base
````



### Méthodes

````python
>>> base.trier(cle, decroissant = False)
trie la base selon la cle, par défaut par ordre croissant. 

>>> base2 = base.selectionner(cle1, cle2 ...)
genere une nouvelle base, la base2, contenant uniquement les clés de la base passées en argument

>>> base2 = base.avec(cle, operateur, valeur)
genere une nouvelle base, la base2, contenant les lignes de la base vérifiant l'opération entre la cle et la valeur passée en paramètre.
cle : une cle de la base
opérateur : '=', '<', '<=', '>', '>=', '<>', '!=' 
valeur : une valeur de même type que la cle
    
>>> base3 = base.joindre(base2, cle)
crée, si possible, la base3 en joingnant la base et la base2 selon la clé spécifiée

>>> base.somme(cle)
renvoie la somme des valeur de la clé

>>> base.maximum(cle)
renvoie la valeur maximale de la clé

>>> base.minimum(cle)
renvoie la valeur minimale de la clé

>>> base.effectif(cle = '', operateur = None, valeur =0):
renvoie l'effectif des valeurs de la clé.
Si la clé n'est pas précisé, la fonction renvoie l'effectif de la base.
si operateur est précisé, il s'agira de l'effectif des valeurs de la cle lui correspondant

>>> base2 =base.chercher(chaine)
renvoie la base2, une base dont chaque ligne a une valeur qui contient la chaine


````










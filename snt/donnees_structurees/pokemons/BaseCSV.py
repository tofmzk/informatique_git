#!/usr/bin/python3
# -*- coding: utf-8 -*-

import collections
from operator import attrgetter
import time

"""
:mod: `csvSNT` module
                         
:author: Académie de Lille _ Mieszczak Christophe
         
:date: nov 2019
defini la classe Basecsv
Interface simplifiée de manipulation de fichier csv
"""

                                                     

class CSVError(Exception):
    """
    exception pour fichiers csv
    """
    def __init__(self, msg):
        self.message = msg

class BaseCSV:
    """               
    une classe pour gerer et manipuler les fichiers csv
    """
    OPERATEURS = {
                         '='  : lambda x, y : x ==  y ,
                         '==' : lambda x, y : x ==  y ,
                         '>'  : lambda x, y : x > y   ,
                         '>=' : lambda x, y : x >= y  ,
                         '<'  : lambda x, y : x < y   ,
                         '<=' : lambda x, y : x <= y  ,
                         '!=' : lambda x, y : x != y  ,
                         '<>' : lambda x, y : x != y
                     }
#######################################################################################################################################
###################Constructeur
#######################################################################################################################################    
    
            
    def __init__(self, *args, separateur =';'):
        """
        constructeur soit depuis un fichier csv soit depuis une liste de namedtuple
        param arg
        type str si nom de fichier csv ou listes contenant la liste des clés, la liste des types des clés puis la liste des namedtuples
        self.__cles liste des clés
        self.__type_cle dict des type des clés
        self.__donnees list de namedtuples contenant les données
        """      
        if type(args[0]) == str :
           self.__init_from_file(args[0], separateur)   
        else :
            self.__init_from_list_of_data([args[0], args[1], args[2]])
 
    ###################
 
    def __init_from_file(self, nom_fichier, separateur):
        """
        constructeur soit depuis un fichier csv
        param nom_fichier
        type str
        self.__cles liste des clés
        self.__type_cle dict des type des clés
        self.__donnees list de namedtuples contenant les données
             
        """        
        if type(nom_fichier) == str :
            try :
                lecture = open(nom_fichier, 'r', encoding = 'utf_8') # ouvre un canal en lecture vers text.txt
            except :
                 raise CSVError('Fichier impossible à ouvrir ou inexistant !')
            lines= lecture.readlines() # renvoie toutes les lignes du fichier dans la liste
            for i in range(0,len(lines)): # enleve les "\n" et convertit en list
                lines[i] = lines[i].rstrip("\n").split(separateur)
            # recherche de champs
            try :
                self.__donnees = []
                self.__cles = []
                self.__type = {}
                for cle in lines[0]:
                    self.__cles.append(cle)
                    self.__type[cle]='str'
            except :
                raise CSVError('Impossible de lire les clés')
            # generation collection
            try :
                base = collections.namedtuple('base', self.__cles)
            except :
                raise CSVError("Syntaxe des clés incorrecte : pas d'espace, pas de ponctuation !")
            
            del lines[0] # on enlève les champs
            temp_dict = {}
            for ligne in lines :
                for i in range(len(ligne)) :
                   temp_dict[self.__cles[i]] = ligne[i]    
                self.__donnees.append(base(**temp_dict))

    ###################

    def __init_from_list_of_data(self, list_of_data):
        """
        constructeur soit depuis une liste de namedtuple
        list_of_data listes contenant la liste des clés, la liste des types des clés puis la liste des namedtuples
        type list
        self.__cles liste des clés
        self.__type_cle dict des type des clés
        self.__donnees list de namedtuples contenant les données
             
        """
        if not isinstance(list_of_data, list) or not isinstance(list_of_data[1], dict) or not isinstance(list_of_data[2], list) :
            raise CSVError('Argument(s) incorrect(s)')    
        try :
            self.__donnees =[]
            self.__cles = []
            self.__type = {}
            for cle in list_of_data[0] :
                self.__cles.append(cle)
            for cle in list_of_data[1].keys():
                self.__type[cle] = list_of_data[1][cle]
            for ligne in list_of_data[2] :
                self.__donnees.append(ligne)
        except :
            raise CSVError('Argument(s) incorrect(s) ')    
    
    
#######################################################################################################################################
#### Assesseurs            
#######################################################################################################################################           
    
    def get_cles(self):
        '''
        renvoie la liste des clefs de la base
        >>> b1 = BaseCSV('doctest.csv')
        >>> b1.get_cles()
        ['Prénoms', 'Noms', 'Sexes', 'Dossard', 'Date_Naissance']
        '''
        return self.__cles
 
             ###################
 
    def est_valide(self, cle) :
        '''
            renvoie vrai si cle valide faux sinon
            param cle
            type str
            return boolean
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.est_valide('Prénoms')
            True
            >>> b1.est_valide('Prenoms')
            False
        '''        
        return isinstance(cle, str) and cle in self.get_cles()
 
              ###################
 
    def get_types(self):
        '''
            renvoie le dictionnaire des type des cles
             >>> b1 = BaseCSV('doctest.csv')
             >>> b1.get_types() == {'Prénoms': 'str', 'Noms': 'str', 'Sexes': 'str', 'Dossard': 'str', 'Date_Naissance': 'str'}
             True
             
        '''
        return self.__type
 
              ###################
 
    def get_type(self, cle) :
        '''
            renvoie le type de la cle
            param cle
            type str
            return type de la cle 'str', 'num'
            type str
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.get_type('Noms')
            'str'
        '''
        if self.est_valide(cle) :
            return self.__type[cle]
        else :
            raise CSVError('La cle doit être dans ' + str(self.get_cles()))

             ###################

    def set_type(self, cle, type_cle):
        '''
            modifie le type d'une clé
            param cle une cle
            type str
            param type_cle un type 'str', 'num', 'heure' ou 'date'
            type str
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.set_type('Dossard', 'num')
            >>> b1.get_types() == {'Prénoms': 'str', 'Noms': 'str', 'Sexes': 'str', 'Dossard': 'num', 'Date_Naissance': 'str'}
            True
            >>> b1.get_valeurs('Dossard')
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        '''
        TYPES_VALIDES = ['num', 'str','heure', 'date']
        if self.est_valide(cle) and type_cle in TYPES_VALIDES:
            self.__donnees = self.__convertir_base(cle, type_cle)
            self.__type[cle] = type_cle
            
        else :
            if not self.est_valide(cle) :
                raise CSVError('La cle doit être dans ' + str(self.get_cles()))
            else :
                raise CSVError('type non valide. Doit être dans '+ str(types_valides))

             ###################

    def get_donnees(self):
        '''
            renvoie les données de la base
            return les données
            type list de namedtuple
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.get_donnees()
            [base(Prénoms='Sidney', Noms='Robert', Sexes='M', Dossard='1', Date_Naissance='29/06/72'), base(Prénoms='Paien', Noms='Gilbert', Sexes='M', Dossard='2', Date_Naissance='30/10/68'), base(Prénoms='Vincent', Noms='Riquier', Sexes='M', Dossard='3', Date_Naissance='28/01/02'), base(Prénoms='Saville', Noms='Marier', Sexes='M', Dossard='4', Date_Naissance='13/03/99'), base(Prénoms='Namo', Noms='Lereau', Sexes='M', Dossard='5', Date_Naissance='23/10/06'), base(Prénoms='Romaine', Noms='Hughes', Sexes='F', Dossard='6', Date_Naissance='12/11/06'), base(Prénoms='Archard', Noms='Rivard', Sexes='M', Dossard='7', Date_Naissance='23/09/06'), base(Prénoms='Cheney', Noms='Chassé', Sexes='M', Dossard='8', Date_Naissance='13/09/76'), base(Prénoms='Avelaine', Noms='CinqMars', Sexes='F', Dossard='9', Date_Naissance='12/12/72'), base(Prénoms='Sidney', Noms='Charest', Sexes='M', Dossard='10', Date_Naissance='01/05/98')]
             
        '''
        return self.__donnees
    
              ###################
    
    def set_donnees(self, ligne):
        '''
            ajoute la ligne aux données de la base à condition que les clés de la ligne soient toutes dans la base et que
            les types des clés et des valeurs soient tous str
            param ligne
            type dict
        '''

        for cle in ligne.keys():
            if not isinstance(cle, str) or not isinstance(ligne[cle], str) :
                raise CSVError('clés et valeurs doivent être du type str')
        for cle in self.__cles : 
            if not cle in ligne.keys() :
                ligne[cle]= 'None';
        try :
            for cle in ligne.keys() :
                if self.__type[cle] !='str' :
                    ligne[cle] = BaseCSV.__convertir_valeur(ligne[cle], 'str', self.__type[cle])
        except :
            raise CSVError('Données incompatibles')
        base = collections.namedtuple('base', self.__cles)
        self.__donnees.append(base(**ligne))
            
 
              ###################
 
    def get_valeurs(self, cle, doublons = False):
        '''
        renvoie la liste des valeurs d'une clé
        si doublon est à False, les doublons sont éliminés
        param cle une cle
        type str
         >>> b1 = BaseCSV('doctest.csv')
         >>> b1.get_valeurs('Noms')
         ['Robert', 'Gilbert', 'Riquier', 'Marier', 'Lereau', 'Hughes', 'Rivard', 'Chassé', 'CinqMars', 'Charest']
        '''
        liste_valeurs =[]
        if self.est_valide(cle) :
            indice = self.indice_cle(cle)                
            for val in self.get_donnees() :
                liste_valeurs.append(val[indice])
            #Eliminons les doublons (par defaut)
            if doublons :
                return liste_valeurs
            else :
                liste_sans_doublon = []
                for elt in liste_valeurs :
                    if not elt in liste_sans_doublon :
                        liste_sans_doublon.append(elt)
                return liste_sans_doublon
        else :
            raise CSVError('La cle doit être dans ' + str(self.get_cles()))
        
 
        
######################################################################################################################################               
###Méthodes privées######################################################################################################################################
######################################################################################################################################   
 
    def __match(valeur1, operateur, valeur2):
        '''
            renvoie True or False selon que valeur_ligne et valeur matchent avec l'opérateur
            param valeur_ligne, valeur 2 valeur de même type
            param operateur "=" ou "<" ou ">" "<=" ou ">= ou '!=' ou '<>'"
            type str
            return True or False
            Boolean            
        '''
        

        return BaseCSV.OPERATEURS[operateur](valeur1, valeur2)
        
    
                ###################        
    
    def __convertir_valeur(valeur, old_type, type_final):
        '''
            Renvoie la conversion de la valeur au type final si possible
            sinon, génére une erreur
            param valeur
            type ?
            param type_final
            un type parmi les TYPES_VALIDES
            type str
            
        '''
        #### de num vers .....
        if type(valeur) == int or type(valeur) == float :
            if type_final =='str' :
                try :
                    return str(valeur)
                except :
                    raise CSVError('Conversion en str impossible')
            elif type_final == 'num' :
                return valeur
            else :
                raise CSVError('Conversion impossible, type non géré')
            
        #### de str vers    
        elif type(valeur) == str :
            if type_final == 'num' :
                try :
                    return int(valeur)
                except :
                    try :
                        return float(valeur)
                    except :
                        raise CSVError('Conversion en num impossible')
            elif type_final == 'str' :
                return valeur
            
            elif type_final =='heure' :
                try :
                    return time.strptime(valeur, "%H:%M:%S")
                except :
                    raise CSVError('Conversion en heure impossible, le format doit être H:M:S')
            elif type_final == 'date':
                try :
                    return time.strptime(valeur, "%d/%m/%y")
                except :
                    raise CSVError('Conversion en date impossible, le format doit être J/M/A')

            else :
                raise CSVError('Conversion impossible, type non géré ')
        
        #### de time vers ....
        elif type(valeur) == time.struct_time :
            if type_final == 'str' :
                try :
                    if old_type == 'heure' :
                        return time.strftime("%H:%M:%S", valeur)
                    else :
                        return '??'
                       # return time.strftime("%d/%m/%y", valeur)
                    
                    
                except :
                    raise ('Conversion en str impossible')
        ##### de date vers ...
        else :
            raise CSVError('Conversion impossible, type non géré !')
 
             ###################        
    
    def __convertir_base(self, cle, type_cle):
        '''
            renvoie une base où la cle a été converti dans le type type_cle si possible sinon ne change rien
            param cle
            type str
            param type_cle
            type str
            return
            type BaseCSV
        '''
        base = collections.namedtuple('base', self.get_cles())
        donnees =[]
        try :
            
            for ligne in self.get_donnees():
                temp_dict = {}
                for i in range(len(ligne)):
                    if self.get_cles()[i] == cle :
                        valeur = BaseCSV.__convertir_valeur(ligne[i],self.get_type(cle), type_cle)
                    else :
                        valeur = ligne[i]
                    temp_dict[self.get_cles()[i] ] = valeur
                if temp_dict != {} :
                    donnees.append(base(**temp_dict))
                              
            return donnees
        except :
            raise CSVError('Conversion impossible !!')
        
######################################################################################################################################
###Méthodes de l'objet BaseCSV
######################################################################################################################################   

    
    def indice_cle(self, cle):
        '''
            renvoie l'indice dela clé
            param cle une clé
            type str
            return indice de la clé
            type int
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.indice_cle('Noms')
            1

        '''
        if self.est_valide(cle) :
            indice = -1      
            for i in range(len(self.get_cles())) :
                if self.get_cles()[i] == cle :
                    indice = i
            if indice == -1 :
                raise CSVError('La cle doit être dans ' + str(self.get_cles()))
            return indice           
        else :
            raise CSVError('La cle doit être dans ' + str(self.get_cles()))

            ###################            

    def est_primaire(self, cle) :
        '''
            renvoie true si la clé est primaire False sinon
            param cle une cle
            type str
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.est_primaire('Dossard')
            True
            >>> b1.est_primaire('Sexes')
            False
            >>> b1.est_primaire('Prénoms')
            False
        '''
        if self.est_valide(cle) :
            for valeur in self.get_valeurs(cle):
                selection_avec_cle = self.avec(cle, '=', valeur)
                if len(selection_avec_cle) !=1 :
                    return False
            return True
        else :
            return False

            ###################        

    def trier(self, cle, decroissant = False):
        '''
            trie la base selon la clé
            param cle
            type str
            param decroissant l'ordre est inversé si décroissant = True. vaur False par défaut
            type  boolean
            return une base
            type BaseVSC
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.trier('Sexes')
            >>> b1.get_donnees()
            [base(Prénoms='Romaine', Noms='Hughes', Sexes='F', Dossard='6', Date_Naissance='12/11/06'), base(Prénoms='Avelaine', Noms='CinqMars', Sexes='F', Dossard='9', Date_Naissance='12/12/72'), base(Prénoms='Sidney', Noms='Robert', Sexes='M', Dossard='1', Date_Naissance='29/06/72'), base(Prénoms='Paien', Noms='Gilbert', Sexes='M', Dossard='2', Date_Naissance='30/10/68'), base(Prénoms='Vincent', Noms='Riquier', Sexes='M', Dossard='3', Date_Naissance='28/01/02'), base(Prénoms='Saville', Noms='Marier', Sexes='M', Dossard='4', Date_Naissance='13/03/99'), base(Prénoms='Namo', Noms='Lereau', Sexes='M', Dossard='5', Date_Naissance='23/10/06'), base(Prénoms='Archard', Noms='Rivard', Sexes='M', Dossard='7', Date_Naissance='23/09/06'), base(Prénoms='Cheney', Noms='Chassé', Sexes='M', Dossard='8', Date_Naissance='13/09/76'), base(Prénoms='Sidney', Noms='Charest', Sexes='M', Dossard='10', Date_Naissance='01/05/98')]
            >>> b1.set_type('Dossard', 'num')
            >>> b1.trier('Dossard')
            >>> b1.get_valeurs('Dossard')
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
           
        '''
        if not self.est_valide(cle) :
            raise CSVError('La cle doit être dans ' + str(self.get_cles()))
        else :
            self.__donnees =  sorted(self.get_donnees(), key = attrgetter(cle), reverse = decroissant)

     
            
                ######################
    
    def selectionner(self, *args):
        '''
            renvoie une base en selectionnant uniquement les clés passées en argument
            param args liste de clés de la base
            return
            type BaseCSV
             >>> b1 = BaseCSV('doctest.csv')
             >>> b2 = b1.selectionner('Prénoms','Noms')
             >>> b2.get_cles()
             ['Prénoms', 'Noms']
             
        '''
        try :
            base = collections.namedtuple('base', args)
            donnees =[]
            for ligne in self.get_donnees():
                temp_dict = {}
                temp_types ={}
                temp_cles = []
                for cle in args :
                    for i in range(len(ligne)):
                        if self.get_cles()[i] == cle :
                            temp_dict[cle] = ligne[i]
                            temp_cles.append(cle)
                            temp_types[cle] = self.get_type(cle)
                if temp_dict !={} :
                    donnees.append(base(**temp_dict))
            return BaseCSV(temp_cles, temp_types, donnees)
        except :
            raise CSVError('La cle doit être dans ' + str(self.get_cles()))
        
            ######################
        
 
            
    def avec(self, cle, operateur, valeur):
        '''
        renvoie une base extraite de façon à ce cle et valeur renvoient true avec l'operateur choisi
        param cle une cle de la base
        type str
        param operateur "=" ou "<" ou ">" "<=" ou ">=" ou '!='
        param valeur une valeur de la base
        type str
        return une base
        type BaseCSV
        >>> b1 = BaseCSV('doctest.csv')
        >>> b2 = b1.avec('Prénoms','<','B')
        >>> b2.get_donnees()
        [base(Prénoms='Archard', Noms='Rivard', Sexes='M', Dossard='7', Date_Naissance='23/09/06'), base(Prénoms='Avelaine', Noms='CinqMars', Sexes='F', Dossard='9', Date_Naissance='12/12/72')]
        
        
        '''
        if not self.est_valide(cle) :
            raise CSVError('La cle doit être dans ' + str(self.get_cles()))
        
        if operateur not in BaseCSV.OPERATEURS :
            raise CSVError("L'operateur doit être dans "+str(operateurs_valides));
        try :
            base = collections.namedtuple('base', self.get_cles())
            donnees =[]
            for ligne in self.get_donnees():
                temp_dict = {}
                if BaseCSV.__match(ligne[self.indice_cle(cle)], operateur, valeur) :
                    for i in range(len(ligne)):
                        temp_dict[self.get_cles()[i]] = ligne[i]                        
                if temp_dict != {} :
                    donnees.append(base(**temp_dict))
            return BaseCSV(self.get_cles(), self.get_types(),donnees)
        except :
            raise CSVError('Clé et valeur incompatibles')

            ######################

    def est_joignable(self, base, cle):
        '''
            renvoie vraie si la cle permet de joindre self et base
            param base
            type BaseCSV
            param cle
            type str
            return True ou une erreur
            type boolen
            >>> b1 = BaseCSV('doctest.csv')
            >>> b2 = BaseCSV('doctest2.csv')
            >>> b1.est_joignable(b2,'Dossard')
            True
            
        '''
        if not isinstance(base, BaseCSV) :
            raise CSVError('Le premier paramètre doit être une BaseCSV')
        if (self.est_primaire(cle) or base.est_primaire(cle)) and self.est_valide(cle) and base.est_valide(cle) :
            return True
        else :
            raise CSVError('Les bases ne sont pas joignables selon cette clé')
    
    def __joindre_bases(base1, base2, cle_primaire) :
        '''
            joint la base1 et la base2 selon la clé qui est primaire pour la base1 (au moins)
            param base1, base2
            type BaseCSV
            param cle
            type str
            return une base CSV
            type BasCSV
        '''
        # création de la liste de clés
        liste_cles= []
        dict_types = {}
        for cle in base2.get_cles() :
            liste_cles.append(cle)
            dict_types[cle] = base2.get_type(cle)
        for cle in base1.get_cles() :
            if not cle in liste_cles :
                liste_cles.append(cle)
                dict_types[cle] = base1.get_type(cle)
        # création de la liste donnee pour joindre les 2 bases
        
            
        donnees =[]
        for no_ligne in range(len(base2.get_donnees())):
            ligne =[]
            # on commence par la base 2
            for elt in base2.get_donnees()[no_ligne] :
                ligne.append(elt)
            # ajout d'informations via la base1
            ligne_base1 = base1.avec(cle_primaire, '=', base2.get_donnees()[no_ligne][base2.indice_cle(cle_primaire)])
            if len(ligne_base1) > 0 :
                for colonne in range(len(ligne_base1.get_donnees()[0])):
                    if not base2.est_valide(ligne_base1.get_cles()[colonne]) :
                        ligne.append(ligne_base1.get_donnees()[0][colonne])
            else :
                ligne.append('None')
            # on ajoute la ligne aux données
            donnees.append(ligne)
        # conversion en baseCSV
        donnees_csv =[]
        for ligne in donnees :
            temp_dict={}
            for i in range(len(ligne)) :
                temp_dict[liste_cles[i]] = ligne[i]
            base = collections.namedtuple('base',liste_cles )
            donnees_csv.append(base(**temp_dict))
        return BaseCSV(liste_cles, dict_types, donnees_csv)

        return donnees
    
    def joindre(self, base, cle):
        '''
            joint self et  base  via la cle si possible
            param base
            type BaseSNT
            param cle
            type str
            return la jointure
            type BaseSNT
            >>> b1 = BaseCSV('doctest.csv')
            >>> b2 = BaseCSV('doctest2.csv')
            >>> b3 = b1.joindre(b2, 'Dossard')
            >>> b3.get_donnees()
            [base(Dossard='1', Heure='1', Minutes='8', Secondes='55', Prénoms='Sidney', Noms='Robert', Sexes='M', Date_Naissance='29/06/72'), base(Dossard='3', Heure='1', Minutes='21', Secondes='23', Prénoms='Vincent', Noms='Riquier', Sexes='M', Date_Naissance='28/01/02'), base(Dossard='4', Heure='0', Minutes='56', Secondes='29', Prénoms='Saville', Noms='Marier', Sexes='M', Date_Naissance='13/03/99'), base(Dossard='5', Heure='1', Minutes='6', Secondes='20', Prénoms='Namo', Noms='Lereau', Sexes='M', Date_Naissance='23/10/06'), base(Dossard='6', Heure='1', Minutes='17', Secondes='8', Prénoms='Romaine', Noms='Hughes', Sexes='F', Date_Naissance='12/11/06'), base(Dossard='7', Heure='0', Minutes='46', Secondes='31', Prénoms='Archard', Noms='Rivard', Sexes='M', Date_Naissance='23/09/06'), base(Dossard='8', Heure='0', Minutes='48', Secondes='10', Prénoms='Cheney', Noms='Chassé', Sexes='M', Date_Naissance='13/09/76'), base(Dossard='10', Heure='1', Minutes='6', Secondes='38', Prénoms='Sidney', Noms='Charest', Sexes='M', Date_Naissance='01/05/98'), base(Dossard='2', Heure='1', Minutes='8', Secondes='12', Prénoms='Paien', Noms='Gilbert', Sexes='M', Date_Naissance='30/10/68'), base(Dossard='9', Heure='1', Minutes='9', Secondes='1', Prénoms='Avelaine', Noms='CinqMars', Sexes='F', Date_Naissance='12/12/72')]
        '''
        if self.est_joignable(base, cle) :
            if self.est_primaire(cle) :
                return BaseCSV.__joindre_bases(self, base, cle)
            else :
                return BaseCSV.__joindre_bases(base, self, cle)
            
            ######################
    
    def somme(self, cle):
        '''
            renvoie la somme des valeurs correspondantes à la cle
            param cle
            type str
            return la somme
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.set_type('Dossard', 'num')
            >>> b1.somme('Dossard')
            55
            
        '''
        if not self.est_valide(cle) :
            raise CSVError('La cle doit être dans '+str(self.get_cles()))
        if not self.get_type(cle) == 'num':
            raise CSVError('La cle doit être de type numerique')
        val_a_ajouter = self.get_valeurs(cle, True)
        total = 0
        try :
            for nbre in val_a_ajouter :
                total += nbre
            return total
        except :
            raise CSVError('Somme non calculable')
        
        ######################

    def maximum(self, cle):
        '''
            renvoie le max des valeurs correspondantes à la cle
            param cle
            type str
            return la somme
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.maximum('Dossard')
            '9'
            >>> b1.set_type('Dossard','num')
            >>> b1.maximum('Dossard')
            10
            
        '''
        if not self.est_valide(cle) :
            raise CSVError('La cle doit être dans '+str(self.get_cles()))
        try :
            return max(self.get_valeurs(cle))
        except :
            raise CSVError('Somme non calculable')
        

        ######################
        
    def minimum(self, cle):
        '''
            renvoie le in des valeurs correspondantes à la cle
            param cle
            type str
            return la somme
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.minimum('Dossard')
            '1'
            >>> b1.set_type('Dossard','num')
            >>> b1.minimum('Dossard')
            1
            
        '''
        if not self.est_valide(cle) :
            raise CSVError('La cle doit être dans '+str(self.get_cles()))
        try :
            return min(self.get_valeurs(cle))
        except :
            raise CSVError('Somme non calculable')
        

        ######################         
        
    def  effectif(self, cle = '', operateur = None, valeur =0):
        '''
            renvoie l'effectif des valeurs de la clé. Si la clé n'est pas précisé, la fonction renvoie l'effectif de la base.
            si operateur est précisé, il s'agira de l'effectif des valeurs de la cle lui correspondant
            param cle une cle
            type str
            param operateur "=" ou "<" ou ">" "<=" ou ">=" absent par défaut
            type str
            param valeur valeur de comparaison si l'operateur est précisé
            return
            type int
            >>> b1 = BaseCSV('doctest.csv')
            >>> b1.effectif()
            10
            >>> b1.effectif('Sexes')
            2
            
        '''
        if cle == '' :
            return len(self)
        
        if not self.est_valide(cle) :
            raise CSVError('La cle doit être dans '+str(self.get_cles()))
        total = 0
        if operateur == None :
            return len(self.get_valeurs(cle))
                
        else :
            base2 = self.avec(cle, operateur, valeur)
            return len(base2)
        ######################


                

        
    def chercher(self, chaine):
        '''
            renvoie une base dont chaque ligne a une valeur qui contient la chaine
            param valeur une valeur présente ou non dans la base
            return liste des lignes où apparait la valeur
            type list
            >>> b1 = BaseCSV('doctest.csv')
            >>> b2 = b1.chercher('1')
            >>> b2.get_donnees()
            [base(Prénoms='Sidney', Noms='Robert', Sexes='M', Dossard='1', Date_Naissance='29/06/72'), base(Prénoms='Paien', Noms='Gilbert', Sexes='M', Dossard='2', Date_Naissance='30/10/68'), base(Prénoms='Vincent', Noms='Riquier', Sexes='M', Dossard='3', Date_Naissance='28/01/02'), base(Prénoms='Saville', Noms='Marier', Sexes='M', Dossard='4', Date_Naissance='13/03/99'), base(Prénoms='Namo', Noms='Lereau', Sexes='M', Dossard='5', Date_Naissance='23/10/06'), base(Prénoms='Romaine', Noms='Hughes', Sexes='F', Dossard='6', Date_Naissance='12/11/06'), base(Prénoms='Cheney', Noms='Chassé', Sexes='M', Dossard='8', Date_Naissance='13/09/76'), base(Prénoms='Avelaine', Noms='CinqMars', Sexes='F', Dossard='9', Date_Naissance='12/12/72'), base(Prénoms='Sidney', Noms='Charest', Sexes='M', Dossard='10', Date_Naissance='01/05/98')]
            >>> b1.set_type('Dossard','num')
            >>> b2 = b1.chercher(1)
            >>> b2.get_donnees()
            [base(Prénoms='Sidney', Noms='Robert', Sexes='M', Dossard=1, Date_Naissance='29/06/72')]
            
        '''
        liste_donnees =[]
        try :
            for ligne in self.get_donnees() :
                for valeur in ligne :
                    if type(valeur) == str and type(chaine) == str:
                         if chaine in valeur :
                            liste_donnees.append(ligne)
                            break
                    elif (type(valeur) in [int, float]) and (type(chaine) in [int, float]):
                        if valeur == chaine :
                            liste_donnees.append(ligne)
                            break
            return BaseCSV(self.get_cles(), self.get_types(), liste_donnees)
        except :
        
            raise CSVError('Impossible de mener la recherche sur cette chaine')
  
        ######################  

    def sauver(self, nom):
        '''
            sauvegarde les données de la base dans le fichier csv nommé nom
            param nom un nom de fichier sans extension
            type str
            
        '''
        csv_list= [''] * (len(self) + 1)
        old_type=[]
        for i in range(len(self.get_cles())) :
            csv_list[0] += self.get_cles()[i]
            old_type.append(self.get_type(self.get_cles()[i]))
            if i < len(self.get_cles()) - 1:
                csv_list[0] += ';'
        indice =0
        for ligne in self.get_donnees():
            indice += 1
            for i in range(len(ligne)) :
                csv_list[indice] += BaseCSV.__convertir_valeur(ligne[i],old_type[i], 'str')
                if i< len(ligne) -1 :
                    csv_list[indice] += ';'
        try :
            ecriture=open(nom,'w',encoding='utf-8') 
            for ligne in csv_list :
                ecriture.write(ligne +'\n')
            ecriture.close()
        except :
            raise CSVError('impossible de sauvegarder les données')
                    

#######################################################################################################################################
## Méthodes reservées
###############################################################################################################################        
        
    def __len__(self):
        '''
            renvoie le nbre d'entrées dans la base self
            >>> b1 = BaseCSV('doctest.csv')
            >>> len(b1)
            10
        '''
        try :
            return len(self.get_donnees())
        except :
            return 0
        
        ######################  
    
    def __chercher_longueur_max(self):
        '''
            renvoie un dictionnaire des largeurs maximales des valeur pour l'affichage en prenant les clés pour champs
            return
            type dictionnaire
        '''
        largeur_max ={}
        for cle in self.get_cles():
                largeur_max[cle] = len(cle)
                if len(self.get_type(cle)) +2 > largeur_max[cle] :
                    largeur_max[cle] = len(self.get_type(cle)) +2
                for valeur in self.get_valeurs(cle) :
                    valeur = BaseCSV.__convertir_valeur(valeur,self.get_type(cle), 'str')
                    if len(valeur) > largeur_max[cle] :
                        largeur_max[cle] = len(valeur)
        return largeur_max
    
    def __str__(self):
        '''
            affiche la base
        '''
        try :
            
            chaine =''
            ## recherche largeur max d'une colonne
            largeur_max = BaseCSV.__chercher_longueur_max(self)
            # affichage cles           
            for cle in self.get_cles():
                avant = (largeur_max[cle] - len(cle)) // 2
                apres = largeur_max[cle] - len(cle) - avant
                chaine += '|' + ' '*avant + cle +' ' * apres  
            chaine +='| \n'
            # affichage type des clés
            for cle in self.get_cles() :
                avant = (largeur_max[cle] - len(self.get_type(cle))) // 2 -1
                apres = largeur_max[cle] - len(self.get_type(cle)) - avant -2
                chaine += '|' + ' ' * avant +'(' + self.get_type(cle) +')'+  ' ' * apres  
            chaine +='| \n'

            # affichage des lignes
            for ligne in self.get_donnees() :
                indice = 0
                for valeur in ligne:
                    valeur = BaseCSV.__convertir_valeur(valeur, self.get_type(self.get_cles()[indice]), 'str')
                    avant = (largeur_max[self.get_cles()[indice]] - len(valeur)) // 2
                    apres = largeur_max[self.get_cles()[indice]] - len(valeur) - avant
                    chaine += '|' + ' '*avant + valeur +' ' * apres
                    indice += 1
                chaine +='| \n'
                  
            return chaine
        except :
            raise CSVError("Erreur d'impression")

        ######################
        
    def __repr__(self):
        rep = 'Base de donnée au format csv. \n'
        rep += ' clés : '
        for cle in self.get_cles():
            rep += cle + '-'
        rep  +=  "\n nbre d'entrées : " + str(len(self))            
        return rep
            


       
        
        
################################################
###### doctest : test la correction des fonctions
################################################

if __name__ == "__main__": #si on est dans le prog principale on execute le doctest
    import doctest
    doctest.testmod(verbose=False)

# TP Filius



## Késako et Installation 

_Filius_ est un logiciel libre et gratuit conçu pour réaliser des simulations de réseaux informatiques sous Windows ou Linux.

Vous pourrez l'installer à partir de ce lien : [Filius]()

Il s'agit d'un logiciel allemand mais, lors de l'installation, vous pourrez choisir la langue française ou anglaise.



## Partie 1 : un petit LAN



>  
>
> Avant de commencer cette partie, allez consulter le cours sur [les constituants d'un réseau](internet/constituants.md).
>
> 





C'est fait ? Bien. Lancez Filius.



![filius 1 - perso](media/filius_1.jpg)



* Mettez le logiciel en mode _construction_ en cliquant sur le marteau.
* Placez trois ordinateurs. Notez que _portable_ et _ordinateur_ n'ont de différent que l'apparence.
* Placez un switch en glissant les constituants de la colonnes de gauche.
* sélectionnez le câble et reliez le switch aux ordinateurs.



Nous voilà avec un petit LAN.

![filius 2 - perso](media/filius_2.jpg)



Sélectionnez un des ordinateurs, portables ou non, puis cliquez sur la flèche dans la barre du bas pour ouvrir ses propriétés.

![filius 3 - perso](media/filius_3.jpg)



>  
>
> Sauvegardez votre petit réseau physique.
>
>  



* Sous le nom des hôtes du réseau apparaissent les propriétés de l'ordinateur dans le réseau. 

    * **L'adresse MAC** : unique au monde et _inscrite_ dans la carte réseau de la machine.
    * **L'adresse IP** : elle servira à repérer de façon unique la machine sur le réseau local
    * **Le masque** : servira à savoir dans quel réseau se trouve une machine
    * **La passerelle** : servira plus tard à sortir du réseau 
    * **Le serveur DNS** : servira plus tard à atteindre des serveur web par leur nom de domaine

    

* cochez la case `utiliser l'adresse IP comme nom` , cela nous simplifiera la vie.



Vous venez de construire un **réseau physique** en connectant les machines entre elles par des câbles. Mais il n'est pas fonctionnel pour l'instant. Il faut maintenant configurer les machines pour leurs permettre de communiquer entre elles : on va configurer le **réseau logique**.



## Adresses IP

### Adressage

Une fois le réseau physiquement correctement connecté, on peut créer des sous réseaux logiques grâce à un adressage et un masque qui détermineront quelles machines du même réseau physique peuvent comuniquer entre elles.

> 
>
>  Avant de continuer, allons jeter un oeil au cours [adresses IP](adresses_IP.md) pour apprendre ce qu'est une adresse IP et un masque . 
>
> 



Nous allons maintenant configurer notre réseau :

* Le nom du réseau sera 192.168.1.0 

* On va utiliser le masque 255.255.255.0

    

> Donner des adresses IP valides pour que les trois machines soient sur ce réseau.



### Testons notre réseau 

Passez en mode simulation en cliquant sur la flèche verte.

Cliquez sur  une machine puis sur l'icône _installation de logiciel_.

Sélectionnez  _ligne de commande_ (c'est à dire un SHELL) puis cliquez sur la flèche gauche et, pour finir, appliquez la modification : un nouvel icône d'application apparait à côté .

![filius4 - perso](media/filius_4.jpg)

Lancer _ligne de commande_ en cliquant sur l'icone.

Tapez la commande `ipconfig` pour voir la configuration de cet ordinateur.

Pour vérifier la possibilité d'atteindre un autre ordinateur, on utilise la commande `ping adresseIP`. Cette dernière essaie de joindre l'adresse IP précisée en lui envoyant un petit _paquet_ puis attend une réponse éventuelle et enfin affiche le temps que l'envoie du message et le retour de l'accusé de réception ont pris.

Essayez de joindre les deux autres hôtes.

  

![filius 5 - perso](media/filius_5.jpg)



Installez _ligne de commande_ sur les autres hôtes et testez les de même.



**Notre petit réseau est donc configuré, les hôtes peuvent communiquer entre eux.**



> 
>
>  Sauvegardez votre travail !!
>
>  





## Passerelle

### Second réseau



Comme vous l'avez fait pour le premier réseau, créez sur la même _feuille_ de Filius un second réseau d'adresse 192.168.0.0 avec le masque 255.255.255.0 de façon à ce que :

* Ce nouveau réseau ne doit pas être relié physiquement au premier pour l'instant. 
* Il comporte 3 ou 4 machines avec des adresses IP adéquates



Testez avec quelques pings la bonne configuration du réseau.

On va maintenant permettre aux deux réseaux de communiquer entre eux.



### Configuration de la  passerelle

Nous avons deux réseaux distincts avec le masque 255.255.255.0 : 

* le premier a pour adresse 192.168.1.0
* le second a pour adresse 192.168.0.0



Pour les relier, il faut utiliser un routeur qui servira de passerelle entre les deux réseaux.

Sous Filius, selectionner un routeur avec 2 interfaces (une par réseau à relier) et relier le aux deux réseaux par un câble.

Cliquez maintenant sur le routeur : celui-ci à deux adresses IP car nous lui avons donné deux interfaces. L'adresse IP d'une interface doit être compatible avec le réseau qu'elle relie :

* Donnez à notre routeur l'adresse IP 192.168.1.254 du côté du réseau d'adresse 192.168.1.0 
* Donnez à notre routeur l'adresse IP 192.168.0.254 du côté du réseau d'adresse 192.168.0.0

![routeur](media/routeur.png)

_Remarques :_

* En cliquant sur une languette avec un câble rouge, on visualise quel réseau on relie (en vert)
* On attribue souvent les dernière adresses disponibles sur le réseau (d'où le 254)



Ce n'est pas tout à fait terminé : il faut maintenant dire aux machines du réseau où est la passerelle qui leur permettra de sortir de leur réseau :

* Il faut cliquer sur chaque machine du réseau 192.168.1.0 et indiquer que leur passerelle a pour IP 192.168.1.254

* de même, pour le réseau 192.168.0.0, on doit préciser sur chaque machine que leur passerelle a pour adresse 192.168.0.254
  
    



### Internet



Repassons en mode exécution. 

Effectuez des ping entre une machines d'un réseau et une de l'autre réseau.



Ça marche ? vous avez donc relier deux réseaux locaux ensemble pour en faire un plus gros. Voilà ce qu'est internet : l'interconnexion d'un immense nombre de réseaux entre eux !





> SAUVEGARDEZ VOTRE TRAVAIL SOUS LE NOM `deux_reseaux`. 
>
> ON VA EN AVOIR A NOUVEAU BESOIN PLUS TARD QUAND ON PARLERA DU WEB !



___________

Par Christophe Mieszczak CC BY SA

_source images : impressions d'écran, production personnelle_ 






























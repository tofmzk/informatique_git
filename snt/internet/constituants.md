# Les constituants d'un réseau



## Les hôtes : clients et serveurs



* Tout d'abord il y a les **hôtes** (ou host), c'est à dire toutes les machines échangeant des données sur le réseau :

    * Ordinateurs

    * Smartphone

    * Tablettes

    * Imprimantes.

    * Télévision

    * Console vidéo

    * Appareils connectés (thermostats, réfrigérateur, four, volets roulants ...)

      ​    

* Parmi ces hôtes, il y a :

    * Les clients : par extension, ce sont les machines qui utilisent un logiciel, appelé client, qui envoie des requêtes l'autre type d'hôte, c'est à dire les serveurs.

    * Les serveurs : par extension, ce sont les machines qui utilisent un logiciel, appelé serveur, qui attend les demandes des clients et y répond, s'il le peut.

      ​    

## Interconnexions



Les interconnexions permettent au données de transité d'un équipement à un autre. 



* La carte réseau qui permet de traduire les données en un format transportable sur les médias cités juste en dessous.

  ​    

    ![carte reseau - wikipedia - GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Network_card.jpg/220px-Network_card.jpg)

    ​	

    

* Des câbles, comme le rj45 ci-dessous, sont un média habituel des réseaux.

    ![câble rj45 - wikipedia CC BY SA [auteur](https://commons.wikimedia.org/wiki/User:David.Monniaux)](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Ethernet_RJ45_connector_p1160054.jpg/200px-Ethernet_RJ45_connector_p1160054.jpg)

* les médias sans sans fil (Wifi - 3G - 4G - 5G ...) sont de plus en plus présents.





## Les périphériques réseau



* Pour relier plusieurs machines entre elles, on utilise un commutateur , appelé `switch` en anglais.

![image switch - wikipédia - CC BY SA auteur [geek2003](https://commons.wikimedia.org/w/index.php?title=User:Geek2003&action=edit&redlink=1)](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/5600-Stack-Front-Avaya_HiRes.jpg/220px-5600-Stack-Front-Avaya_HiRes.jpg)

* En reliant plusieurs machines entre eux à l'aide d'un switch, on crée un `LAN`. 

![lan1 - copie d'écran perso](media/filius_2.jpg)



* Pour communiquer, les appareils doivent pouvoir être identifiés. Un peu  comme chaque maison à son adresse, les appareils connectés s'identifient à l'aide d'une adresse appelée `adresse IP`. Les adresses IP sont distribuées de manière unique pour chaque machine par un serveur `DHCP` (Dynamic Host Configuration Protocol) .



![lan2 - photo d'écran perso](media/lan2.png)



* Un `routeur` (router en anglais)  assure la circulation des informations entre les **différents** réseaux. Avec les switchs, les routeurs sont responsable de la _livraison_ des données entre les hôtes.


​    ![routeur - wikipedia - CC BY SA auteur[Hellisp](https://commons.wikimedia.org/wiki/User:Hellisp)](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Dlink_wireless_router.jpg/300px-Dlink_wireless_router.jpg)



_Remarque :_

Une box internet joue le rôle de switch, de serveur DHCP, de routeur et  assure la liaison entre le réseau local et le **F**ournisseur d'**A**ccès à  **I**nternet (FAI).   



_Remarque :_

Pour fonctionner, Internet a besoin d'une quantité de câbles hallucinante. Une partie d'entre eux sont fabriqués... à Calais.

Regardez un peu [ici](https://www.submarinecablemap.com/) pour en avoir un aperçu des câbles sous-marins.



## Les protocoles



Pour communiquer, les ordinateurs (ou tous autres appareils connectés entre eux) utilisent des règles appelées `protocoles`. Ils existent de nombreux protocoles, chacun d’entre eux réalisant une tâche bien précise. 

* Le protocole Ethernet sert à la commutation de paquets.
* Le protocole IP, permet de communiquer sur internet.
* Le protocole TCP contrôle la transmission des données pour une communication plus sûre afin d'être certain que les données reçues sont bien celles qui ont été envoyées.
* Le protocole UDP permet, comme TCP, la transmission de données mais sans contrôle. On n'est jamais certain que les données reçues sont bien celles qui ont été envoyées. On l'utilise pour les applications où l'envoi de données doit être rapide et répété et où une erreur n'est pas catastrophique (streaming, jeux vidéo en ligne par exemple).





___________

Par Christophe Mieszczak  CC BY SA

_sources images  wikipédia, licence CC BY SA (auteur dans le line) ou GNU, et productions personnelles_






import folium
import math

tab_coordo = []

def ajouter(lat, long) :
    '''
    ajoute une coordonnée au tableau de coordonnées
    : pas de return
    : modifie tab_coordo par effet de bord
    '''
    tab_coordo.append((lat, long)) #, 'routeur no ' + str(len(tab_coordo))))
    
def creer_carte(nom_carte) :
    '''
    génère une carte à partir de tab_coordo
    : param nom_carte (str) le nom du fichier qui sera créé au format html
    '''
    distance = 0
    carte = folium.Map(location = (tab_coordo[0][0], tab_coordo[0][1]),zoom_start = 14)
    folium.PolyLine(tab_coordo, color="blue", weight=2.5, opacity=0.8).add_to(carte)
    old_lat, old_long = tab_coordo[0][0], tab_coordo[0][1]
    for coordo in tab_coordo:
        distance += calculer_distance(coordo[0], coordo[1], old_lat, old_long)
        folium.Marker((coordo[0], coordo[1]), popup = str(distance) +' km').add_to(carte)
        old_lat, old_long = coordo[0], coordo[1]
    
    carte.save(nom_carte + '.html')
    
def calculer_distance (lat1, long1, lat2, long2) :
    """
    Calcul la distance en km entre les deux coordonnées géographiques
    : param lat1, lat2, long1, long2 (float)
    : return: float
    """
    lat1, lat2 = math.radians(lat1), math.radians(lat2)
    long1, long2 =math.radians(long1), math.radians(long2)
    d = math.acos(math.sin(lat1)*math.sin(lat2)+math.cos(lat1)*math.cos(lat2)*math.cos(long2-long1))*6371
    return d    
    
    
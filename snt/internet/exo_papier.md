# Un réseau version papier

### Réseau local

On considère le réseau local ci-dessous qui a pour adresse IPv4 **`___.___.___.___`** et utilise le masque **`255.255.255.0`**.



<img src="media/exo_papier_1.jpg" style="zoom: 80%;" />





​	

1. Citez trois composants d'un réseau .

    ```txt
    
    
    
    ```

2. Que signifie le sigle `IP` ? 

    ```txt
    
    ```

3. Qu'est-ce qu'une adresse IP privée ? Quel est son intérêt ?

  ```txt
  
  
  ```

4. Qu'est-ce qu'une adresse IP publique ? Quel est son intérêt ?

    ```txt
    
    
    ```

5. Quelle est l'adresse de `broadcast` de ce réseau ? A quoi sert-elle ?

    ```txt
    
    
    ```

6. Combien d'hôtes peut-on connecter à ce réseau au maximum ? Justifier. 

    ```txt
    
    ```

7. La norme `IPv6` code les adresses IP sur 16 octets (au lieu de 4 en `IPv4`). Quel est son intérêt ?

    ```txt
      
        
    ```

8. Complétez le schéma en haut de cette page en donnant une adresse IPv4 convenable à chaque hôte du réseau.

9. Donnez une commande qui permet de vérifier que deux hôtes de ce réseau local peuvent communiquer :
	```TXT
	```

### Routeur

On veut maintenant relier votre réseau à celui d'un de vos voisins. Choisissez un groupe voisin et demandez l'adresse IP de leur réseau.

1. Quel équipement doit-on utiliser pour servir de passerelle entre deux réseaux  ?

	```txt

	```

2. Complétez les adresses IP ci-dessous afin que les deux réseaux locaux puissent échanger des données.

    <img src="media/exo_papier_2.jpg" style="zoom: 80%;" />
    
3. Donnez une commande qui permet de vérifier que les deux réseaux peuvent échanger des données  :

	```txt
	
	```
   



### Modèle Client et serveur

1. Qu'est-ce qu'Internet ?

	```txt
   
    
    
	```

2. Qu'est qu'un serveur web? Quel est son rôle ?

	```txt
   
    
    
	```

3. Qu'est-ce qu'un client ? Comment interagit-il avec un serveur web (donner un exemple) ?
	```txt
	 
	  
	  
	```
	
4. Qu'est qu'un serveur DNS (Domain Name Serveur) ? Quel est son rôle ?

	```txt
	 
	  
	  



___________

Par Mieszczak Christophe Licence CC BY SA

source images : production personnelle à partir du logiciel Filius.
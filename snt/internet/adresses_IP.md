# `I`nternet `P`rotocole



## L'adresse IP

L'adresse `IP` , abréviation de `Internet Protocole`, est une adresse numérique permettant d'identifier les  appareils connectés à un réseau. 

Actuellement, deux versions coexistent :

*  IPv4 , la version 4, celle à laquelle nous allons nous intéresser en particulier. 
* IPv6, la version 6 entrée en service en 2010.

### IPv4

Les  adresses IP, en version 4, prennent la forme d'une série de 4 nombres séparés par des  points :

* Ces 4 nombres sont des  entiers codés sur un octet (donc allant de 0 à 255), par exemple 192.168.0.1

* Une adresse IPv4 se code donc sur 4 octets c'est à dire sur 32 bits.

    

Un rapide calcul: $$256^4 = 4 294 967 296$$, nous donne le nombre d'adresses IPv4 possibles. 

Compte-tenu du développement d'internet, ce nombre est aujourd'hui trop petit (au passage, il y a aujourd'hui plus d'objets connectés à internet que d'humain, c'est pour cela que l'on parle de l'internet des objets).



### IPv6

Afin d'augmenter le nombre d'adresses possibles, une  nouvelle version, IPv6, est entrée en service en 2010 et cohabite avec la norme IPv4 : Les adresses IP ne  sont plus codées sur $$4 \times 8$$ bits mais $$16 \times 8$$ bits, souvent codé en hexadécimal. Le nombre  d'adresses disponibles théoriquement est donc colossal : $`256^{16} = 3,3 \times 10^{38}`$. 





##  Adresse IP publique et privée 

### Analogie

![courrier](https://upload.wikimedia.org/wikipedia/commons/6/68/Postwatchletters.jpg?20051017142808)

Vous devez envoyer une lettre à un ami qui habite dans l'appartement 18 d'un immeuble.

*  Si vous n'avez pas d'autres informations, le courrier ne pourra pas arriver jusqu'à lui : il y a une multitude d'immeubles qui ont un appartement portant le no 18. Il n'est en effet pas possible que chaque appartement devait avoir un no unique sinon il faudrait utiliser des milliards de chiffres pour les nommer.
* Ce numéro 18 n'a de sens que **localement** dans un immeuble donné et pas de l'extérieur. On dit qu'il est **privé** et on ne peut envoyer un courrier uniquement avec cette adresse. 
* Nous avons besoin d'une **adresse unique au monde** pour envoyer notre courrier  : c'est celle de l'immeuble qui est situé au 3 boulevard du net à Calais, France. Avec cette adresse impossible de se tromper d'immeuble. C'est une **adresse publique** qui est attribuée à un unique endroit dans le monde.



Nous allons donc adresser notre lettre au _3 boulevard du net à Calais, France à destination de l'appartement 18_. 



### Adressage IP

Il existe différents types d'adresse pour un hôte sur un réseau :

* Certaines adresses sont reservées à un usage spécifique :
    * L'adresse `127.0.0.0` est utilisée pour les tests de boucle locale avec notamment l’adresse IP `127.0.0.1` qui est l’adresse « localhost » c'est-à-dire de boucle locale de votre PC. 
    *  L'adresse `0.0.0.0` est elle aussi réservé e(et utilisé notamment pour définir une route par défaut sur un routeur)    

* une adresse IP **publique** :

    * est une adresse unique au niveau mondial.  
    * permet aux ordinateurs du réseau de communiquer entre eux sur internet. 
    * est délivrée par votre FAI (**F**ournisseur d'**A**ccès à **I**nternet) au moment de l’installation et de la synchronisation du box. 
    * Une plage d'adresses leur est reservée en IPv4 mais aujourd'hui la plupart sont en IPv6 afin d'avoir un plus grand nombre de possibilités.

* Une adresse IP **Privée** :

    * est une adresse utilisée uniquement sur les réseaux privés. Ces adresses sont utilisées au sein d’une organisation (entreprises, lycées, réseaux domestiques, …) et permettent de communiquer localement à l'intérieur du réseau privé. 

    * est unique sur le réseau local mais pas au niveau mondial : deux ordinateurs de deux réseaux privés différents peuvent avoir la même adresse, comme le no18 de l'appartement précedemment.

    * Toujours comme pour notre appartement no18, une adresse IP privée ne permet pas d'accéder à internet. IL faut passer par un appareil intermédiaire du réseau privé qui possède une adresse IP publique : un routeur, comme celui qui est dans votre box.

    * Les adresses IP publiques se situent dans une plage réservées. Elles comencent toujours soit par `192.168` soit par `172.16` soit par `10`.

      
      
       

Quand on se connecte à internet, via une _box_,  cette box est identifiée par une **adresse IP publique** attribuée par le FAI ( (**F**ournisseur d'**A**ccès **I**nternet).

Cette  adresse IP peut être soit **fixe** ou **dynamique** :

* Une adresse fixe, comme  son nom l'indique, ne change jamais. On est donc dans ce cas aisément  traçable. 

* Une l'adresse IP dynamique est attribuée par le FAI à chaque nouvelle  connexion de la box à internet. Cette adresse est  attribuée sur des  plages d'adresses IP réservées. Le traçage n'est donc pas immédiat mais  le FAI est capable de fournir les informations permettant  l'identification.

    


Voyons le cas de votre PC :

* Si vous êtes sous Windows 2000, Windows XP ou Windows Vista :
    *  Allez sur **Démarrer**, puis **Exécuter** puis tapez **«** cmd /k ipconfig /all **».** 
    
* sous Windows 7, Windows 8, Windows 10)  :
    *   Allez dans **Démarrer**, puis **Exécuter** puis **Cmd**  Tapez la commande « **ipconfig /all** »   La liste de toutes les cartes Ethernet de votre PC apparaît, avec leurs adresses IP et les informations suivantes : Nom de la connexion dans les propriétés de Windows  Adresse MAC de la carte réseau ;  Adresse « IPv4 » privée de votre ordinateur.  
    
    ![SHELL windows](https://upload.wikimedia.org/wikipedia/commons/5/5e/Ipconfigall.png?20120719081007)
    
* sous  Linux :
    * Dans la console,  tapez la commande `ifconfig` (il peut être nécessaire d'installer les outils réseaux en tapant `sudo apt-get install net-tools` dans la console).

* Pour connaitre l' adresse IP publique de votre serveur (la box) il suffit de se rendre sur le site [*mon-ip.com*](http://www.mon-ip.com/ip-dynamique.php)

![SHELL linux](https://upload.wikimedia.org/wikipedia/commons/e/e9/Ifconfig_2008.png?20080917205629)

_Remarque :_

Au niveau de la machine, l'adresse IP s'écrit en binaire avec uniquement des 0 et des 1.

_Exemple :_

La copie d'écran ci-dessous rend compte de tous les appareils  connectés à un réseau domestique. On peut y lire les adresse IP (et aussi les adresses MAC juste en dessous).

L'adresse MAC (pour **M**edia **A**ccess **C**ontrol),  parfois nommée adresse physique, est un identifiant physique stocké dans une carte réseau, sorte de numéro d'immatriculation, elle est unique au monde et ne change jamais.



![IP - source perso](media/IP.png)



## Réseaux et sous-réseaux privés

_Pour la suite, nous nous bornerons à l'utilisation de l'IPv4. sur un réseau privé._

### Sous-réseaux



Dans votre réseau privé, chez vous par exemple, chaque machine connectée a une adresse IP privée unique dans ce réseau.



Il est très courant d'avoir besoin de créer plusieurs sous-réseaux dans un réseau privé. Dans un réseau domestique (chez vous), on peut par exemple différencier le réseaux des parents de celuis des enfants. Au lycée, on peut différencier les ordinateurs selon la salle dans laquelle ils se trouvent ...



Il faut donc que l'adresse IP permettent de savoir dans quel sous-réseau se situe chaque machine : pour cela on va utiliser un **masque**.



### Les masques

Un masque est codé sur 4 octets, comme une adresse IP, est va nous permettre : 

* de situer la machine dans le sous-réseau auquel elle appartient. 
* de connaitre l'adresse du réseau lui même.
* de connaitre une adresse particulière appelée adresse de broadcast qui permet d'envoyer un message à toutes les machines du sous-réseau en même temps.



Nous en verront dans cette leçon une version simplifiée des masques en ne s'intéressant qu'à trois masques particuliers qui sont les plus répandus.



####  `255.255.255.0` 

Avec ce masque, les `3` premiers octets (car il y a 3 fois 255) de l'adresse IP sont fixes :

* L'adresse du réseau est obtenue en conservant les 3 premiers octets de l'adresse IP et en mettant un 0 au dernier octet.

* L'adresse de broadcast (de diffusion en français) , qui permet d'envoyer un message à toutes les machines de ce réseau, est obtenue en conservant les 3 premiers octets de l'adresse IP et en mettant un 255 au dernier octet.

* Les machines qui ont en commun ces 3 octets dans leur adresse sont sur le même réseau. Le dernier octet sert à numéroter la machine elle même avec un entier entre 1 et 254.

* On note souvent en abrégé un tel masque en ajoutant un `/24` à la fin de l'adresse IP (24 = 3 * 8 pour 3 octets _bloqués_). Il s'agit de la norme nommée `CIDR` (pour **C**lassless **I**nter-**D**omain **R**outing).

  | IP Machine               | 192  | 168  | 1    | 2    |
  | ------------------------ | ---- | ---- | ---- | ---- |
  | Masque                   | 255  | 255  | 255  | 0    |
  | IP réseau                | 192  | 168  | 1    | 0    |
  | IP diffusion (broadcast) | 192  | 168  | 1    | 255  |
  
    

_Exemple :_

Un réseau utilise le masque `255.255.255.0`

Trois machines ont pour adresse `192.168.4.11/24`, `192.168.4.31/24` et `192.168.4.8/24`

* L'adresse de Broadcast est 

* L'adresse du sous-réseau est 

* Une machine a pour adresse 192.168.2.3/24. Est-elle sur ce réseau ?

    

    

_Conséquences :_

* Un tel masque permet de faire 256 sous-réseaux différents dont les adresses iront de 192.168.`0`.0 à 192.168.`255`.0 

* Sur chaque sous-réseau, on peut placer 254 ordinateurs dont le numéro ira de 1 à 254.

    

#### `255.255.0.0`

Avec ce masque, les `2` premiers octets ( car il y a 2 fois 255) de l'adresse IP sont fixes :

* L'adresse du réseau est obtenue en conservant les 2 premiers octets de l'adresse IP et en mettant un 0 aux deux derniers.

* L'adresse de broadcast, qui permet d'envoyer un message à toutes les machines de ce sous-réseau, est obtenue en conservant les 2 premiers octets de l'adresse IP et en mettant un 255 au deux derniers.
* Les machines qui ont en commun ces 2 octets dans leur adresse sont sur le même sous-réseau. Les deux  derniers octets servent à numéroter la machine elle même avec un entier entre 1 et 256²-2 (les adresses du réseau et de broadcast ne peuvent être attribuées à une machine).
* On note souvent en abrégé un tel masque en ajoutant un `/16` à la fin de l'adresse IP (16 = 2 * 8 pour 2 octets _bloqués_). C'est toujours la norme nommée `CIDR`(pour **C**lassless **I**nter-**D**omain **R**outing).



| IP Machine               | 172  | 16   | 1    | 2    |
| ------------------------ | ---- | ---- | ---- | ---- |
| Masque                   | 255  | 255  | 0    | 0    |
| IP réseau                | 172  | 16   | 0    | 0    |
| IP diffusion (broadcast) | 172  | 16   | 255  | 255  |





_Exemple :_

* Trois machines de ce réseau ont pour adresse `192.168.2.1/16`, `192.168.2.3/16` et `192.168.4.8/16`

    * L'adresse de Broadcast est 

    * L'adresse du réseau est 

    * Une machine a pour adresse `192.168.21.3/16` . Est-elle sur ce réseau ?

        

_Conséquence :_

* Un tel masque ne permet pas de faire de sous-réseaux mais permet de placer plus de machines dans le même réseau.

    

#### `255.0.0.0`

Avec ce masque, seul le premier octet ( car il y a 1 fois 255) de l'adresse IP est fixe. L'IP commence par `10.` et les 3 octets suivants servent à numéroter les machines du réseau.

Si on utilise la norme `CIDR`, on ajoutera un `\8` à la fin de l'adresse IP (8 pour 8bits = 1 octet).

Ce masque fonctionne sur le même principe que les deux autres mais il est très rarement utilisé. Il ne sert qu'à créé un unique réseau géant contenant un très grand nombre de machines (256³ moins les 2 adresses réservées pour le réseau et la diffusion). 





| IP Machine               | 10   | 13   | 128  | 2    |
| ------------------------ | ---- | ---- | ---- | ---- |
| Masque                   | 255  | 0    | 0    | 0    |
| IP réseau                | 10   | 0    | 0    | 0    |
| IP diffusion (broadcast) | 10   | 255  | 255  | 255  |



____

Par Christophe Mieszczak 

Licence CC BY SA

source image : 

* production personnelle

* [SHELL windows - wikipedia](https://commons.wikimedia.org/wiki/File:Ipconfigall.png)

* [SHELL linux - wikipédia](https://commons.wikimedia.org/wiki/File:Ifconfig_2008.png)

* [courrier - wikipédia](https://commons.wikimedia.org/wiki/File:Postwatchletters.jpg)

    








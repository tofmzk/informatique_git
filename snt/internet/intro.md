# Introduction



## Qu'est-ce qu'un réseau ? qu'est-ce qu'internet ?

On appelle réseau (`network`)  un ensemble d'équipements reliés entre eux pour échanger des informations. Il en existe différents type :

* Réseaux téléphoniques
* Réseaux sociaux
* Réseaux de télévisions
* Réseaux informatiques



Nous allons ici parler des machines qui échangent des données dans un réseau informatique.



> Citer au moins 5 équipements pouvant faire partie d'un réseau informatique.
>
> - Un ordinateur bien sûr
> -  ...



On distingue différents types de réseaux:  

- le réseau local  appelé `LAN` pour **L**ocal  **A**rea **N**etwork: formé des machines réunies dans  une même pièce ou bâtiment ( quand ce réseau utilise la communication  par Wifi on parle de `WLAN` pour **W**ireless **L**ocal  **A**rea **N**etwork) . 

- le réseau urbain `MAN`  pour **M**etropolitan **L**ocal  **A**rea **N**etwork : réseau à l'échelle d'une ville ou d'une agglomération.  

- le réseau étendu `WAN`  pour **W**ide **L**ocal  **A**rea **N**etwork : réseau reliant plusieurs sites ou ordinateurs du monde entier. 

  
  
  Voici un petit LAN :
  
  ![réseau](https://upload.wikimedia.org/wikipedia/commons/4/46/Internet.JPG?20070201233028)
  
  
  
    

## `Internet`  : le réseau des réseaux :

* En **1958**, La `DARPA (Defense Advanced Research Projects Agency)` voit le jour. Cette agence gouvernementale américaine a pour but de veiller à la constante suprématie des États unis en  matière technologique et scientifique

* En **1962** la `DARPA` soutient le  projet du professeur Licklider qui a pour but de mettre en réseau les  ordinateurs des universités américaines afin que ces dernières puissent échanger des informations plus rapidement (même à des  milliers de kilomètres de distance). 

* En **1968**, `ARPANET`,1er réseau informatique à grande échelle de l'histoire voit le jour.  

* Le **29 octobre 1969**, le 1er message (le mot "login") est envoyé depuis l'université de Californie à Los Angeles vers  l'université de Stanford via le réseau `ARPANET` (les 2 universités sont  environ distantes de 500 km). C'est un demi-succès, puisque seules les  lettres "l" et "o" arriveront à bon _port_. 

* En **1972**, 

    * 23 ordinateurs sont  connectés à `ARPANET` (on trouve même des ordinateurs en dehors des États  unis).
    * En parallèle au projet `ARPANET`, d'autres réseaux voient le jour,  problème, ils utilisent des protocoles de communication hétéroclite et 2 ordinateurs appartenant à 2 réseaux  différents sont incapables de communiquer entre eux puisqu'ils  n'utilisent les mêmes protocoles. 

* En **1974** , [Vint Cerf](https://fr.wikipedia.org/wiki/Vint_Cerf) et [Robert Khan](https://fr.wikipedia.org/wiki/Robert_Elliot_Kahn) vont mettre au point le protocole `TCP`  qui sera très rapidement couplé au protocole `IP` pour donner le modèle `TCP/IP` qui est l'objet de cette leçon.

    * Le modèle `TCP/IP` va très rapidement s'imposer comme un  standard. Les différents réseaux (`ARPANET` et les autres) vont l'adopter. 
    * Cette adoption va permettre d'interconnecter tous ces réseaux (2 machines appartenant à 2 réseaux différents vont pouvoir communiquer  grâce à cette interconnexion). 
    * **Internet était né** (le terme Internet  vient de `internetting` qui signifie Connexion entre plusieurs  réseaux) même si le terme exacte _internet_ n'apparaît qu'en **1983**. 



![internet](https://upload.wikimedia.org/wikipedia/commons/1/14/Base_de_Datos_distribuida_atravez_de_internet.jpg?20060904025012)



Internet continue de se développer. Il s'est constitué progressivement en reliant entre eux des réseaux utilisant des normes différentes,  au fur et à mesure de l'apparition de technologies : ordinateurs, smartphones, objets connectés... Le modèle `TCP/IP` permet l'inter-connexion de ces systèmes hétérogènes,indépendamment des supports de transmission (câble, onde ...), des normes d'infrastructures réseaux, des systèmes d'exploitation et des applications utilisées.

| Année                                                      | Évènement                                                    |
| ---------------------------------------------------------- | ------------------------------------------------------------ |
| [1983](https://fr.wikipedia.org/wiki/1983_en_informatique) | Adoption du protocole [TCP/IP](https://fr.wikipedia.org/wiki/TCP/IP) et du mot « [Internet](https://fr.wikipedia.org/wiki/Internet) » et Premier serveur de noms de sites (serveur [DNS](https://fr.wikipedia.org/wiki/Domain_Name_System)). |
| [1984](https://fr.wikipedia.org/wiki/1984_en_informatique) | 1 000 ordinateurs connectés.                                 |
| [1987](https://fr.wikipedia.org/wiki/1987_en_informatique) | 10 000 ordinateurs connectés.                                |
| [1989](https://fr.wikipedia.org/wiki/1989_en_informatique) | 100 000 ordinateurs inter-connectés.                         |
| [1990](https://fr.wikipedia.org/wiki/1990_en_informatique) | Disparition d'[ARPANET](https://fr.wikipedia.org/wiki/ARPANET) (démilitarisé). Remplacé par Internet (civil). |
| [1991](https://fr.wikipedia.org/wiki/1991_en_informatique) | Annonce publique du [World Wide Web](https://fr.wikipedia.org/wiki/World_Wide_Web) ([Tim Berners-Lee](https://fr.wikipedia.org/wiki/Tim_Berners-Lee)). |
| [1992](https://fr.wikipedia.org/wiki/1992_en_informatique) | 1 000 000 ordinateurs connectés.                             |
| [1993](https://fr.wikipedia.org/wiki/1993_en_informatique) | Apparition du [Navigateur web](https://fr.wikipedia.org/wiki/Navigateur_web) [NCSA Mosaic](https://fr.wikipedia.org/wiki/NCSA_Mosaic). |
| [1996](https://fr.wikipedia.org/wiki/1996_en_informatique) | 36 000 000 ordinateurs connectés.                            |
| [2000](https://fr.wikipedia.org/wiki/2000_en_informatique) | Explosion de la [bulle Internet](https://fr.wikipedia.org/wiki/Bulle_Internet) (368 540 000 ordinateurs connectés). |
| [2014](https://fr.wikipedia.org/wiki/2014_en_informatique) | La barre du milliard de sites web est franchie[6](https://fr.wikipedia.org/wiki/Histoire_d'Internet#cite_note-6). |
| [2021](https://fr.wikipedia.org/wiki/2021_en_informatique) | 4 600 000 000 ordinateurs connectés.                         |



## `Internet` n'est pas le `web` !



Le web, apparu en 1990, est une des possibilités offertes par internet. Il est constitué de toutes les ressources accessibles via internet et reliées entre elles par des liens hypertextes. Nous l'étudierons dans un prochain chapitre.

En résumé : 

* Internet est un réseau de machines.
* le web est un réseau de ressources (documents).



______________

Par Mieszczak Christophe

Licence CC BY SA

[internet - source wikipédia](https://commons.wikimedia.org/wiki/File:Base_de_Datos_distribuida_atravez_de_internet.jpg)

[un lan - source wikipédia](https://commons.wikimedia.org/wiki/File:Internet.JPG)

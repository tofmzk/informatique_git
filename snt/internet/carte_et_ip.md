# Par où passentles paquets ?

L'objectif est de visualiser sur une carte du monde par où passent les paquets lorsque vous envoyer des requêtes à un serveur.

## Tracer les IP

On va demander les adresses IP des routeurs par lesquels vont passer les paquets de données lorsque, par exemple, vous essayer de joindre le serveur dans le nom de domaine est _google.fr_.



Pour cela, ouvrez un SHELL. 

* Sous Linux, tapez la commande `traceroute google.fr`

* Sous Windows, ouvrez une fenêtre de commande Windows (Windows + R puis cmd) et tapez la commande `tracert google.fr`

  (équivalent en ligne : https://ping.eu/traceroute)

Voici le résultat obtenu sur mon PC. Sur le votre, il peut être différent !

![SHELL](media/tracert.png)

* Notez les adresses IP des routeurs traversés (tant que l'adresse commence par `192.168` ou `172.16`, vous êtes encore sur votre réseau local) :

    ```txt
    194.149.174.112 est le premier routeur pour moi.
    ...
    
    
    ```



## Latitude et longitude

Allez visiter le site [trouver IP](https://trouver-ip.com/index.php) qui permet de géolocaliser les machines à partir de leur IP publique.

A partir des IP trouvées plus haut , déterminez les coordonnées géographiques (latitude puis longitude) des routeurs empruntés et complétez les coordonnées correspondantes aux IP trouvées plus haut dans [ce document tableur](latitude_longitude.ods)

|      |      |      |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |
|      |      |      |



## Générer une carte en Python

Python peut, grâce au module **_folium_**, générer une carte en y présentant les endroits repérés par le couple latitude, longitude.

*  Vous trouverez dans votre dossier le fichier **_carte_ip.py_** : ouvrez le avec Thonny et exécutez le.
* Vous disposez maintenant des fonctions suivantes :
    * `ajouter(latitude, longitude) ` permet d'ajouter les coordonnées passées en paramètre à celles qu'on veut faire apparaitre sur une carte.
    * `generer_carte(nom_carte)` permet de générer un fichier au format _html_ en nommant le fichier avec la valeur passée en paramètre.

Une fois le script en exécution dans Thonny, allez dans la console,

- Ajoutez donc les coordonnées géographique que vous avez trouvées plus haut à l'aide de la fonction `ajouter(lat, long)` . Dans la fenêtre "variables", vous devez voir la liste `tab_coordo[...]` se compléter au fur et à mesure.
- Générez une carte à l'aide de la fonction `creer_carte("nom-de-la-carte")` et visualisez le trajet de vos données en ouvrant le fichier HTML dans un navigateur.
- Vous pouvez estimer la distance entre les points les plus éloignés de votre carte en utilisant la fonction `calculer_distance(lat1, long1, lat2, long2 )`, le résultat est exprimé en km.



## Encore



De la même façon, générez les cartes du trajet des paquets pour atteindre les serveurs de _enthdf.fr_, _facebook.fr_ , _tiktok.com_ ...



___________

Par  Mieszczak Christophe

avec la participation d'Eric Fiolet

licence CC BY SA








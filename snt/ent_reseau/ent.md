# Utiliser l'ENT

**E**space **N**umérique de **T**ravail

## Se connecter via un navigateur

Pour trouver la page de connexion de l'ENT, il suffit de le demander à un moteur de recherche :

![Trouver l'ENT](media/trouver_ent.jpg)

Il reste juste à cliquer sur le premier lien pour arriver sur la page de choix de connexion :

* Si c'est votre première visite, ajoutez cette page à vos favoris.
* choisissez `Elève ou Parent`.
* choisissez`Lycée.`
* choisissez `Académie de Lille`.
* cochez sur la case correspondante à votre situation (lieu public ou chez vous ?).
* cliquez sur `Se connecter`.

![choix de connexion](media/se_connecter.jpg)

Il faut maintenant entrer vos identifiants. Ces identifiants sont très importants et vous suivront toute votre scolarité au lycée (peut être plus si vous y continuez en BTS) :

* **Ne les donnez à personne !!** : il aurait accès a des données personnelles et pourrait causer des dégâts, même involontairement.
* **Ce sont bien des vôtres qu'il s'agit, pas ceux de vos parents ni ceux d'un camarade ! **

![identifiants](media/identifiants_franceconnect.png)

## Page d'accueil

Une fois identifié, vous arriver sur la page d'accueil de l'ENT.

![Accueil](media/accueil.jpg)

En haut à droite, vous pouvez voir quelques icônes. Les principaux sont :

* La maison qui vous fait revenir à la page d'accueil.
* les 9 petits carrés qui vous envoient sur la page des applications, comme _Pronote_ ou _Casier_ .
* l'enveloppe qui vous donne accès à la messagerie.

Commençons par aller jeter un œil aux applications en cliquant sur ces petits carrés.

![Applications](media/applis2.jpg)

Cliquez sur `gérer mes favoris` et cliquez sur le `+` pour enregistrer les applications que vous utiliserez le plus ( comme _Pronote_, _Casier_ , _Forum_ ou _Annuaire_ ) dans les _Favoris_. Ces applications sont alors accessibles directement : en survolant nos neuf petits carrés, un raccourci apparaîtra.

Allez-y : 

* mettez ces applications en _Favoris_.
* survolez les neufs petits carrés ... 
* Ca marche ? Très bien, revenez à la page d'accueil.

## Utiliser la messagerie

Allons maintenant dans la messagerie.

![Messagerie](media/messagerie.jpg)



* A gauche,  vous voyez un petit menu très classique pour avoir accès au message envoyés, aux brouillons et à la corbeille.
* en haut à droite vous avez le bouton `Nouveau message` : cliquez dessus.



![nouveau message](media/mail_vers_classe.jpg)

Dans la zone de texte située après le `A ?`, nous allons tenter deux trois choses :

* Commencez à taper le début du nom de votre classe (certaines possibilités seront bloquées avec un compte _élève_) :
    * enseignants du groupe : vous permet d'envoyer un message à tous les enseignants
    * élèves du groupe : vous permet d'envoyer un message à tous les élèves de la classe
    * parents du groupe : de même avec les parents 

Pour envoyer un message à une personne précise dont vous connaissez le nom, il suffit de commencer à taper ce nom dans la zone de texte. choisissez ensuite parmi les propositions (attention aux homonymes).



Pour envoyer un message à une personne dont vous ne trouvez pas l'adresse directement via la zone de texte, il faut utiliser l'annuaire. Nous l'avons mis en raccourci tout à l'heure. allons - y !

![Agenda](media/adresse.jpg)

Dans le menu de droite, On peut faire une recherche par nom ou sélectionner les caractéristiques de la personne à joindre (notez qu'il n'est pas obligatoire de tout choisir) :

* sa classe (élève ou professeur qui y intervient) si besoin.
* son profil (enseignant / parent / élève / personnel) .
* sa fonction.

On clique ensuite sur la petite loupe et le résultat de la requête apparaît.



> * Envoyez un mail à toute la classe en souhaitant bonne rentrée à tout le monde
> * Envoyez un mail à vos voisins directs uniquement en leur disant bonjour, je suis ton voisin et je m'appelle ...
> * Envoyez moi un mail en me disant bonjour et en vous présentant un peu :
>     * je m'appelle ...
>     * j'aime bien telle ou telle chose.
>     * l'année prochaine, j'aimerais plutôt m'orienter .... ou je n'ai pas trop d'idée pour l'instant.
>     * ???
> * Ouvrez les mails. Répondez aux individus mais pas au groupe.
> * Une fois un mail devenu inutile, mettez le dans la corbeille (en cas de fausse manipulation, on peut encore le récupérer)
> * Videz la corbeille (les mails qui y étaient sont perdus ...)



## Utiliser le casier

Le casier est utile pour envoyer des documents à une personne ou un groupe sur l'ENT

Allons-y : 

![Casier](media/casier.jpg)

Il suffit de cliquer sur le `Déposer dans un casier` et de suivre les instructions :

![deposer dans un casier](media/deposer_casier.jpg)

* soit vous glissez le document dans la zone en pointillé, soit vous cliquez sur `Parcourir` et allez chercher le fichier dans vos répertoires.
* Dans la zone de texte, on procède exactement comme pour la messagerie en choisissant un groupe, un enseignant ou un élèves (en utilisant le carnet si besoin).
* il n'y a plus qu'à envoyer.

_Remarques :_

* Plus le fichier est volumineux, moins c'est pratique : utilisez le bon format (on en reparlera) !

* si vous voulez mettre plusieurs fichiers d'un coup dans le casier, il faut les placer dans une archive :

    * sélectionnez à la souris les fichiers à réunir.

    * cliquez sur le bouton de droite de la souris

    * suivez le menu _envoyer vers / fichier zip_ 

    * nommez votre archive : il n'y a plus qu'à l'envoyer.  


> 
>
> Imaginons que votre voisin soit malade : Envoyez lui ce document !
>
> 



## Utiliser le forum

Allons cette fois dans le forum. Toujours via notre icône aux neuf carrés.

![Accueil forum](media/accueil_forum.jpg)



S'il y a déjà une catégorie (un sujet) qui est partagée avec vous, vous pouvez la rejoindre directement en cliquant dessus. Sinon il faut en créer une en cliquant sur le bouton en haut à droite. 

Il vous suffit de donner un nom à votre catégorie. Ici, par exemple, ci-dessous, le nom est _SNT_.



![créer une catégorie](media/creer_categorie.jpg)



Il faut maintenant partager votre catégorie avec qui vous voulez : un groupe d'élèves , la classe entière, un ou des professeurs ... A vous de voir selon vos besoin.



![partager la catégorie](media/partager_categorie.jpg)



Dernière chose : il faut attribuer les droits. Ceux avec qui vous avez partagé, auront-ils seulement le droit de lire ? pourront-ils contribuer ou même modérer la catégorie ? A vous de voir...



![gérer les droits](media/attribuer_droit.jpg)



Maintenant que la catégorie est crée et partagée, vous pouvez lancer une discussion dans le cadre de la catégorie. Pour cela, cochez la case devant la catégorie et le bandeau orange apparaît en bas de l'écran : il n'y a plus qu'a créer une `Nouvelle discussion` à laquelle tous les membres de votre catégorie pourront participer.

_Remarque :_

Depuis ce même bandeau, vous pouvez modifier le partage ou les droits attribués plus haut... Si vous êtes propriétaire de la catégorie bien entendu.





![créer une discussion](media/nouvelle_discussion.jpg)



Et voilà, plus qu'à donner un titre à cette nouvelle discussion et c'est parti ...



![créer une discussion dans la catégorie](media/creer_discussion.jpg)



## Accéder à votre espace personnel sur le réseau

Vous pouvez, depuis l'ENT, accéder à votre espace personnel (le disque H: du réseau) et au dossier commun de votre classe (le disque O: du réseau). Vous pouvez alors déposer des documents sur vos disques ou en récupérer.



Pour cela, dans vos applications, cliquez sur _OwnCloud_ :

![own cloud](media/owncloud.jpg)



Vous arrivez alors sur une page web qui vous demande de vous identifier. Entrez les mêmes identifiant (prenom.nom) et mot de passe que vous utilisez pour vous connecter sur un ordinateur du lycée.

![owncloud identification](media/owncloud_id.jpg)



Vous arrivez alors sur une page web qui présente l'arborescence des répertoires auxquels vous avez accès (pas forcément les mêmes que sur l'image ci-dessous) : votre espace personnel, le commun de votre classe ....

![espace personnel](media/owncloud_repertoires.jpg)



### Télécharger un dossier ou un fichier

* **Il n'est pas possible d'ouvrir ou de visualiser directement le contenu d'un fichier, il faudra le télécharger! **
* En cliquant avec le bouton de gauche sur un répertoire, vous visualiser son contenu. 
* En survolant à la souris un répertoire (ou un fichier) une petite case cochable apparaît. Si vous la cochez, un _télécharger_ apparaît. En cliquant sur cet icône, vous téléchargerez votre sélection.

![own-cloud télécharger](media/owncloud_telecharger.jpg)

Vous récupérez alors une _archive_ de format _zip_ qu'il suffira de dézipper.



### Déposer des documents

Sur la même page web, lorsque vous êtes dans un répertoire sur lequel vous avez un droit en écriture (c'est à dire sur lequel vous pouvez déposer des documents),il vous est possible de créer un nouveau dossier ou de déposer des documents.

Il suffit pour cela de cliquer sur l'icône `+` ,en haut de la page, et de suivre les indications.

![owncloud, uploader](media/owncloud_uploader.jpg)

Vous pouvez alors charger, dans le dossier courant, un fichier ou une archive.



## Pronote



Vous pouvez accéder à _Pronote_ via l'ENT ou via une application installable sur votre smartphone. Si vous utilisez l'application, vous utiliserez les mêmes codes que pour l'ENT.



Via l'ENT, cliquez sur le raccourci _Pronote_ (en survolant les neufs petits carré en haut à droite).

![Accès pronote](media/acces_pronote.jpg)



Nous voilà donc sur _Pronote_. On ne va pas ici regarder toutes ses possibilités dans le détail.



* Vous pourrez suivre les notes que vous obtenez via le menu `Notes`  (dans la vue élève, vous ne pouvez pas mettre de note bien entendu):

    ![mes notes](media/notes.jpg)

    

    



* Vous pourrez vérifier votre emploi du temps et les classes où vous avez cours,  via le menu `Mes données` ou `Emploi du temps` :

    ![emplois du temps](media/emplois_du_temps.jpg)

    

* Notez que le cahier de texte est consultable depuis Pronote en cliquant sur l'icône correspondant :

    ![cahier de texte](media/cahier_texte.jpg)

    

* Parfois, en cas de nécessité (problème lié à l'ENT par exemple), vous pourrez contacter ou être contacté en suivant le menu `Communication`:

    * vous y trouverez un _casier_, qui fonctionne sur le même modèle que celui de l'ENT. En choisissant `Envoi` vous pourrez envoyer des fichiers aux destinataires que vous choisirez. La boite de `Reception` stockera ceux qu'on vous enverra.

        

        ![casier pronote](media/casier_pronote.jpg)

    * Vous pourrez y lire des informations ou répondre à des sondages. Encore une fois deux modes de fonctionnements : `Réception` et `Diffusion` d'une information ou d'un sondage.

      ​    

        ![information/sondages](media/info_pronote.jpg)

        

    * Vous pouvez également créer ou participer à une discussion :

        

        ![discussion](media/discussion.jpg)

    
    
    

_____________

Par Mieszczak Christophe et Boenisch Anne

Licence CC BY SA

source images : production personnelle.
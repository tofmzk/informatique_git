# Le réseau du lycée



## Importance des identifiants





Vous avez reçu un identifiant et un mot de passe vous permettant de vous connecter au réseau du lycée.

Ils sont :

* **importants**  : sans eux, impossible de se connecter sur un ordinateur du lycée.
* **confidentiels** : si un autre que vous se connecte avec vos identifiants :
    * il a accès à vos documents privés, peu les effacer/modifier/diffuser contre votre gré.
    * vous risquez d'être responsable de ses actions sur le réseau.
* **donnés pour toute votre scolarité au lycée**.



En cas de perte des identifiants, il est possible d'en récupérer de nouveaux auprès du responsable du réseau... Lorsqu'il en aura le temps.



## Utiliser le réseau



Une fois connecté sur un poste du lycée, vous disposez d'un espace de stockage que vous pouvez exploiter.

Ouvrez l'_explorateur de fichier_ en cliquant sur l'icône _Poste de travail_ ou via le raccourci _Windows_(touche _Windows_ à gauche de la barre _espace_) + E.



Vous remarquez que vous avez accès à différents disques :

![les différents disques](media/disques.jpg)



### Le disque `programmes`



* Il est utilisé pour l'installation de ... programmes. Si si.
* Vous n'avez aucun droit dessus et ne vous en servirez jamais.



###  Le disque `local` 



Ici, il est appelé  _OS(C:)_ :
* Il est _physiquement_ présent dans la machine sur laquelle vous vous êtes connecté. Si vous y sauvegardez des données, vous le faites uniquement sur ce poste précis. **Vous ne pourrez donc pas retrouver vos documents sur un autre poste.**
* Il faut posséder des droits particuliers (que vous n'avez pas en tant qu'élève) pour y installer quelque chose.
* Ce disque est susceptible d'être mis à jour par le responsable du réseau via une nouvelle _image_ ou un nettoyage : toute donnée déposée sera alors perdue.



En conséquence, **vous n'utiliserez JAMAIS le disque `local` pour sauvegarder vos données.**



### Le disque `amovible`



Il s'agit ici d'une clé USB nommée _TofKey(D:)_ .

* Il pourrait être n'importe quel support amovible, disque dur ou SSD ...

* Il permet si besoin de sauvegarder vos données et de les rapatrier chez vous. 

* Attention à la propagation de virus informatiques : pensez à vérifier votre support régulièrement avec un anti-virus et un anti-malware.
* Ne prêtez pas votre clé à d'autres personnes et passez plutôt par l'utilisation des disque `commun`  ou `privé`(voir plus bas) pour échanger vos données. Cela afin d'éviter les problèmes liés :
    * à la perte du disque et donc des données.
    * à la propagation de virus informatiques.
    * à la propagation de virus, comme le Covid19.





### Le disque `commun` 



* Il n'est pas _physiquement_ présent sur le poste mais sur le serveur du lycée : vous pouvez ainsi y accéder de n'importe quel poste de l'établissement.
* **Il est accessible pour toute personne de votre groupe/classe, élève ou professeur.**

* Un membre n'a accès qu'aux répertoires des groupes dont il fait parti. Une seule classe si c'est un élève.

![disque commun](media/disque_commun.jpg)



* Nous nous en servirons :
    * afin d'échanger des documents avec un professeur de la classe.	
    * afin d'échanger des documents avec un élève de la classe ou avec toute la classe. 
    * **de façon privilégiée par rapport à l'échange/prêt de clés USB qu'il faut éviter (voir plus haut)**.



* Dans tout les cas, vos données ne sont pas à l'abri sur ce disque. Tout membre de la classe peut :
    * y accéder.
    * Les copier.
    * Les modifier.
    * Les effacer.
    * Les diffuser.



**En conséquence :**

* **N'y déposez JAMAIS des documents sensibles ou personnels**.
* **Toute donnée y est déposée TEMPORAIREMENT, le temps de la transférer sur votre disque `privé`, un support `amovible` ou un `cloud` comme celui de l'`ENT`.**



### Le disque `public`



* **Il est accessible pour toute personne possédant un identifiant et un mot de passe sur le réseau.**  
* Il n'est pas _physiquement_ présent sur le poste mais sur le serveur : vous pouvez y accéder de n'importe quel poste de l'établissement.
* Seules certaines personnes (les enseignants ente autres) ont un droit en écriture : les élèves peuvent y récupérer des documents mais ni les modifier ni les supprimer ni même les déplacer.



Il sera utilisé  afin d'échanger des documents avec un professeur dans le cas de documents qui ont vocation à rester disponibles longtemps pour les élèves.



## Le disque `privé`





Il s'agit du disque qui _porte votre nom_ :

* **Vous êtes le seul à y accéder**, grâce à votre identifiant et mot de passe : **surtout ne les donnez à personne !**
* Il n'est pas _physiquement_ présent sur le poste mais sur le serveur : vous pouvez ainsi y accéder de n'importe quel poste de l'établissement.
* **C'est là que vous stockerez vos données en sécurité.**



Pour éviter d'en faire un _fourre-tout_ ingérable, il faut définir une organisation hiérarchique, une _arborescence_ de répertoires. On peut envisager, par exemple, un dossier par matière et , pour chaque matière, un dossier par chapitre. Commençons avec la _SNT_ :



* Allez sur votre disque `privé` :

    ![disque privé](media/disque_prive.jpg)



* Placez vous dans le répertoire `Mes documents`et créez un dossier `SNT`

    ![créer un dossier](media/creer_dossier.jpg)

    

* Dans le dossier `SNT`, créez un nouveau dossier `Environnement Numérique de Travail`. Remarquez, dans le volet de gauche, l'arborescence de vos répertoires qui s'affiche.

    ![arborescence](media/arborescence.jpg)



* Allez sur le disque `public` , copiez les documents que j'y ai déposé, **temporairement**, à votre intention et copiez les dans votre dossier `Environnement Numérique de Travail` : tant que vous ne les effacez pas, il resteront là, à votre disposition, depuis n'importe quel poste du lycée.

## Gérer ses fichiers

Vous aurez vite beaucoup de choses sur votre disque. BEAUCOUP.



Pour vous en sortir, il faut organiser une arborescence de répertoire, c'est à dire un arbre re répertoires et sous-repertoires dans lesquels placer vos fichiers de façon logique.

> 
>
> Construisez une arborescence avec quelques matières (mathématiques, français ...) 
>
> 



_Remarques :_

* Lorsque vous supprimez un fichier, il est automatiquement placé dans la _Corbeille_. Votre place est limitée, pensez à vider cette corbeille de temps en temps.
* Faites le ménage sur votre disque `privé`  régulièrement ou, très vite, vous serez débordé.
* Soyez rigoureux, ne mettez pas vos fichiers n'importe où !



## Depuis chez vous



Vous pouvez accéder à votre espace de travail depuis chez vous, en passant par l'ENT. **Il faut pour cela que vous connaissiez vos codes !**. 

Nous en reparlerons dans [ce document](ent.md)



________

Par Mieszczak christophe

Licence CC BY SA

Source images : production personnelle.
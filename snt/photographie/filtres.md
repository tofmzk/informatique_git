# Filtres photographiques



## codage d'un fichier image numérique

Une image numérique est composée de pixels.

Une telle image est caractérisée par :

* ses `dimensions` : largeur et hauteur.
* sa `définition` : le nombre de pixels par unité de longueur, souvent en `ppp`(pixel per pouce)

Pour une dimension données, plus la définition d'une image est élevée plus elle contient de pixel :

![dimensions et définition](https://upload.wikimedia.org/wikipedia/commons/3/32/Resolution_test.jpg)



Lorsqu'une image est codée en [RVB(ou RGB)](https://fr.wikipedia.org/wiki/Rouge_vert_bleu) chaque pixel est codé par ses trois composantes **R**ouge, **V**erte et **B**leue. 

En jouant sur la valeur de chaque composante codée sur un octet, entre 0 et 255, on décide de l'intensité de chacune d'elle est on peut générer ainsi $`256 \times 256 \times 256 = 16`$ millions de couleurs. Chaque pixel est donc codé sur 24 bits.





![RVB - source wikipédia - public domain](https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/AdditiveColorMixing.png/220px-AdditiveColorMixing.png)

_Remarques :_ 

* Ce n'est pas la seule façon de coder une couleur en informatique. On peut, par exemple utilisé le mode [TSL](https://fr.wikipedia.org/wiki/Teinte_Saturation_Luminosit%C3%A9), [YUV](([CMJN](https://fr.wikipedia.org/wiki/CMJN)) ou [CMJN](https://fr.wikipedia.org/wiki/CMJN) ....

* [En cliquant ici, vous pourrez utiliser un nuancier RVB mais pas que... ](https://htmlcolorcodes.com/fr/). 
* Certains formats d'images (comme le png) supporte une composant supplémentaire : la composante **a**lpha qui gère la transparence. Ce n'est pas le cas, par exemple, du format JPG.



L'image est organisée comme un tableau de pixels, chaque pixel ayant des coordonnées correspondant à la position du pixel dans l'image : 

* le premier pixel de l'image a pour coordonnées (0, 0).
* le dernier pixel à pour cordonnées (largeur de l'image - 1, hauteur de l'image - 1)



## Principe des filtres



En effectuant des opérations mathématiques sur les 3 composantes des pixels d’une image, on transforme l'aspect de l'image : on réalise un filtre.  	



Sous [Photofiltre](http://photofiltre.free.fr/frames.htm) ou [GIMP](https://www.gimp.org/downloads/) , vous pouvez en  tester quelques uns… Nous allons voir le fonctionnement de plusieurs d'entre eux.



## Le module PIL

`PIL` est un module Python qui permet de gérer les données d'une images, de les lire, de les modifier et de les sauvegarder. 

Ce module n'est pas installé par défaut : chez vous, il faudra demander à Thonny d'installer le _paquet_ en suivant le menu _Outils / Gérer les paquets_ puis en recherchant `pillow`  (en tapant ce nom dans la barre de recherche) puis en cliquant sur le bouton _intaller_.

Créez un programme _filtre.py_ et enregistrez le dans le repertoire `\filtres` ou vous trouverez quelques images, comme _tomates.jpg_ par exemple,  dont on se servira dans la suite.

Pill gère les images au format `jpg` et  `png` .

Commençons par quelques tests afin de découvrir le fonctionnement de PIL. 

Dans la zone du programme tapez le code ci-dessous puis exécutez le :

```python
from PIL import Image
mon_image = Image.open('tomates.jpg')
```

Dans la console, tapez les instructions ci-dessous et copiez le résultat obtenu :

```python
>>> type(mon_image)
???
>>> mon_image.size[0]
???
>>> mon_image.size[1]
???
>>> mon_image.show()
>>> mon_image.getpixel((0,0))
???
```

Vous l'avez compris :

* nous avons chargé dans la variable `mon_image` les données de l'image nommée _tomates.jpg_.

*  `mon_image`  est de la classe `PIL image`

* `mon_image.size[0]` renvoie la largeur de l'image en pixel.

* `mon_image.size[1]` renvoie la hauteur de l'image en pixel.

* `mon_image.show()` ouvre le logiciel par défaut pour afficher une image.

* `mon_image.getpixel((x,y))` (notez que le paramètre est un `Tuple`) renvoie un `Tuple` (r, v, b, a) décrivant le pixel de coordonnées (x, y) où :

    * r est la composante _rouge_ de 0 (pas de rouge) à 255 (très rouge)
    * v est la composante _verte_ de 0 (pas de vert) à 255 (très vert)
    * b est a composante _bleu_ de 0 (pas de bleu) à 255 (très bleu)
    * a est la composante _alpha_ qui détermine la transparence de 0 (transparent) à 255 (opaque). Elle n'apparait que si le format de votre image la supporte (présent en PNG mais pas en JPG donc)

    Ainsi le pixel de coordonnées (0, 0) a pour composantes :

     * r = 
     * v = 
     * b = 
     * a =



On peut également modifier les composantes d'un pixel et sauvegarder l'image obtenue :

```python
>>> mon_image.putpixel((0,0), (255, 255, 255))
>>> mon_image.save('image_modifiee.jpg')
```

Ici : 

* on a remplacé les composante du pixel de coordonnées (0, 0) par (255, 255, 255) c'est à dire un pixel blanc. Notez l'absence de la composante _alpha_ : on ne l'utilise que pour un format d'image qui la supporte (PNG mais pas JPG par exemple. Oui, je me répète :) ).
* on a sauvegardé l'image modifiée sous le `image_modifiée.jpg`.



Avec _Photofiltre_ ou _Gimp_, ouvrez cette nouvelle image et zommez sur le pixel en haut à gauche ...



## Premiers filtres

Nous allons tout d'abord nous intéresser aux filtres qui fonctionnent de la façon suivante :

- Pour chaque pixel d’une image :
    - On récupère les composantes r, v, b du pixel.
    - On effectue une opération, qui dépend du filtre voulu, sur ces composantes et on obtient ainsi de nouvelles composantes.
    - On remplace les anciennes composantes par les nouvelles.



C'est le cas de filtres photos très connus : 

* les filtres rouge / vert / bleu 

* le filtre négatif

* le filtre niveau de gris

* le filtre noir ou blanc

    

_Remarque :_

pour la dernière fois (sisi) la composante alpha peut également être modifiée ... si le format d'image utilisé la supporte.



### rouge / vert / bleu

Pour réaliser un filtre rouge, il suffit, pour chaque pixel de l'image :

* de lire les composantes r, v, b du pixel
* de remplacer ces composantes par r, 0, 0 



L'image sera alors en niveau de rouge uniquement.



Puisqu'il s'agit de votre premier filtre, voici comment on procède avec une image au format JPG, sans composante alpha :

```python
from PIL import Image
def rouge(image):
    '''
    renvoie une nouvelle image en niveau de rouge
    '''
    new_img = Image.new("RGB", (image.size[0],image.size[1]))   # la nouvelle image
    for x in range(image.size[0]) :
        for y in range(image.size[1]) :
            r, g, b = image.getpixel((x, y)) # on lit le pixel sur l'image de départ
            new_img.putpixel((x, y), (r, 0, 0)) # on ecrit le pixel modifié dans new_img
    return new_img
```

_Remarque :_

vous avez bien noté que pour éviter un effet de bord sur l'image passée en paramètre, on commence par créer une image de même dimension qu'elle avant de modifier puis de renvoyer cette dernière.



Allez, on teste depuis la console :

```python
>>> mon_image = Image.open('tomates.jpg')
>>> image_modifiee = rouge(mon_image)
>>> image_modifiee.show()
>>> mon_image.show()
```





> Les filtres bleu et vert fonctionnent sur le même principe. Codez les !
>
> N'oubliez pas de les documenter !



### Négatif

Le principe du filtre négatif est de remplacer chaque composante par son complémentaire dans [0,255].

Ainsi, les composantes _r, v, b_ seront _transformées_ en _255 - r, 255-v, 255 - b_

Saurez vous déterminer les composantes de chaque pixel ci-dessous après application d'un filtre négatif ?

![exercice filtre négatif](media/ex_filtre_negatif.jpg)



> Définissez, documentez et codez la fonction `negatif(image)` qui renvoie une image négative de celle passée en paramètre sans effet de bord.
>
> Testez votre filtre sur l'image _tomates.jpg_ et regardez bien la correspondance des couleurs.



### Niveau de gris

En utilisant un logiciel de retouche d'image (Gimp ou Photofiltre), ouvrez une image couleur. 

- En utilisant l' outils _pipette_, cliquez sur différents pixels de l'image pour en observer les composantes rouge, vert et bleu. 

- Passez maintenant cette image en mode niveaux de gris (menu _Image_ puis _mode_ puis _niveau de gris_ puis revenez en mode _RVB_) puis ré-itérez l'opération précédente. Qu'observez-vous ?



  **Pour passer d'une image couleur à une image en niveaux de gris, il faut remplacer chaque composante par une même valeur _L_, appelée luminance (à ne pas confondre avec la Luminosité du mode TSL). La luminance est le résultat d'un calcul de moyenne entière sur les 3 composantes du pixel. L est donc un entier entre 0 et 255.**



![dégardé gris](media/niveau_de_gris.png)



**Exercice :**

En utilisant un calcul de moyenne non pondérée (on ajoute puis on divise par 3), déterminez la luminance des pixels ci-dessous :

![luminance exo1](media/luminance1.jpg)



_Remarque :_

Impossible de distinguer les 3 dernières couleurs en niveaux de gris… Pour éviter cela, on va utiliser  une moyenne **pondérée**. Les pondérations dépendent de la sensibilité aux couleurs primaires de l’œil humain et du support utilisé pour afficher l'image.  



Une formule couramment utilisée est 	`L = (21 * r	+ 71 * v + 8 * b)//100`, le `//` désignant 	l’opérateur division entière.



> En utilisant la formule pondérée pour le calcul de la luminance, définissez, documentez et codez la fonction `niveau_de_gris(image)` qui renvoie une image en niveau de gris obtenue à partir de celle passée en paramètre.



### Noir ou blanc

Le filtre noir ou blanc, en plus de l'image, à besoin d'un second paramètre qui servira de _seuil_ :

* pour chaque pixel de l'image :
    * on calcule sa luminance L.
    * si L est inférieur au seuil alors on rend le pixel noir
    * sinon, on rend le pixel blanc.



> En utilisant la formule pondérée pour le calcul de la luminance, définissez, documentez et codez la fonction `noir_ou_blanc(image, seuil)` qui renvoie une image en noir ou blanc obtenue à partir de celle passée en paramètre sans effet de bord.
>
> Le _seuil_ est un entier entre 0 et 255. 
>
> Testez ce filtre sur l'image _murano_burano.jpg_ en faisant varier le  seuil.



## Autres filtres

Nous arrivons ici dans les filtres pour lesquels on a besoin de connaître un groupe de pixels autour du pixel que l'on souhaite modifié. Ça se corse un peu ...



### Filtre contour

Il existe différents filtres de contour. Nous allons nous contenter de réaliser un filtre contour sur une image en noir ou blanc. Il faudra donc commencer par appliquer ce dernier filtre sur l'image avant de poursuivre.



- L'objectif est de modifier un pixel central en fonction de ceux qui l'entourent. Pour cela, nous allons compter combien il y a de pixel(s) blancs(s) autour de lui. Dans l'exemple ci-dessous, le pixel central (pix)  est entouré de 5 pixels blancs. 

    ![un pixel](media/filtre_contour.jpg)  


  - Si le nombre de pixel(s)  blanc(s) est compris entre 3 et 6 inclus, alors on considère qu'on est près d'un contour et on passe le pixel central en noir.
  - Sinon, on considère qu'on est dans une zone unicolore (toute blanche ou toute noire) et on passe le pixel central en blanc.

  _Remarque :_

ce filtre ne peut s'appliquer qu'aux pixels entourés d'autres pixels : on excluera donc ceux pour qui ce n'est pas le cas de nos boucles (les pixels en bordure de l'image).



> Définissez, documentez et codez la fonction `contour(image, seuil)` qui renvoie le contour d'une image en noir ou blanc obtenue à partir de celle passée en paramètre (pas obligatoirement en noir ou blanc) et sans effet de bord.
>
> Le _seuil_ est un entier entre 0 et 255. Une assertion vérifiera cette précondition.
>
> L'image _chat.jpg_ est idéale pour tester votre filtre.



### Mosaïque

Le principe d'une mosaïque est de réaliser des goupes carrés de pixels et de remplacer les composantes de chaque pixel du groupe par la moyenne des composantes de tous les pixels de ce groupe.



Saurez-vous coder un tel filtre ?



_____________

Par Mieszczak Christophe

licence CC BY SA



sources images :

* [dimension et résolution - wikipedia - CC BY SA](https://commons.wikimedia.org/wiki/File:Resolution_test.jpg)
* [rgb - wikipedia - domaine public](https://commons.wikimedia.org/wiki/File:AdditiveColorMixing.png)
* pixels _carrés_ rvb - production personnelle
* filtre contour - production personnelle


# Photographique numérique



# Principes de la photographie

### La base

Que l'appareil soit argentique ou numérique, le principe de base est de capturer la lumière de façon à impressionner un capteur qui est : 



* Une pellicule pour un argentique. Une fois la photo prise, la pellicule est marquée (impressionnée). Elle ne peut pas être réutilisée, elle est difficilement modifiable est doit être développée selon un procédé chimique pour obtenir une image sur papier. 

![pellicule photo - source PixaBay - Libre](https://cdn.pixabay.com/photo/2014/11/28/19/32/filmstrip-549120_960_720.jpg)



-  capteur électronique CDD ou CMOS pour un appareil numérique. Une fois la photo prise, ce capteur transmet une importante quantité de données stockées dans un volumineux fichier informatique au format RAW et, parfois, directement interprétées et converties en un format plus léger (comme le célèbre JPG). Le capteur est réutilisable, le fichier peut être lu par un logiciel pour être affiché sur de nombreux supports différents (écran,  papier).

![capteur numérique - source wikipédia - licence GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Capteur_CCD_et_CMOS.jpg/800px-Capteur_CCD_et_CMOS.jpg)



 

* La pellicule est fragile et doit être conservée avec précautions pour ne pas se dégrader. Le stockage nécessite un gros volume de rangement… mais ne consomme aucune énergie. 
* Les données des capteurs génèrent des fichiers informatiques. On peut en stocker de grandes                                              quantités sur un volume très restreint… mais cela à un coût énergétique.





>  **Argentique ou électronique, le principe est le même : laisser entrer une certaines quantité de lumière pour la impressionner un capteur. **
>
> **La différence réside dans le capteur mais elle est de taille :**
>
> - **le papier argentique est un support physique, palpable.**
>
> - **le capteur numérique, lui, renvoie des _données_**



## Capturer la lumière



Voici un schéma du fonctionnement d'un appareil photo qu'il soit argentique ou numérique. 



![schéma de fonctionnement d'un appareil photographique](media/schema_appareil_photo.jpg)

* Un appareil photo argentique possède  un boitier contenant un système optique qui guide la lumière vers le capteur (une pellicule) ou vers le viseur qui permet de visualiser ce qu'on photographie.

	

* Le boitier d'un appareil numérique peut également contenir un système optique mais le capteur est électronique et on y trouvera également la mémoire pour stocker les données et l'ordinateur qui effectuera les algorithmes de traitement de ces données.



## Ouverture et vitesse

Le grand public, habitué des appareils photographiques modernes,  l'oublie et utilise des modes pré-programmés mais la photo, numérique ou  pas, repose sur deux modes principaux qui sont liés :

- la priorité à l'ouverture (liée au diaphragme et à l’objectif)  
- la priorité à la vitesse (temps d'exposition du capteur). 



Parlons un peu de la vitesse :



- Plus le temps d'exposition est long, plus la quantité de lumière  qui frappe le capteur est importante mais plus le risque de flou est  grand.
- Afin de bien comprendre l'impact du réglage de la vitesse sur la photo obtenue, cliquez [sur le lien suivant](https://www.phototraces.com/photography-tips/shutter-speed-chart/)





Parlons Maintenant de l'ouverture qui, selon l’objectif utilisé, détermine, elle, le champ de profondeur :

- Une ouverture importante donnera une faible profondeur de champ :  c'est le mode portrait de votre appareil par exemple. La vitesse  d'obturation sera rapide, la photo net mais l'arrière plan flou (fondu).
- Une ouverture plus petite augmente la profondeur de champ… Mais pour  conserver une quantité suffisamment importante de lumière, la vitesse  de prise de vue diminue (le temps d'exposition augmente) et le risque de  flou devient important.



**Exercice :** 

Quels réglages ont été utilisés pour les photos ci-dessous ? Justifier !

![Quel réglage ?](https://cdn.pixabay.com/photo/2018/08/26/14/19/hillclimb-3632359_960_720.jpg)

![Quel réglage ?](https://c.pxhere.com/photos/43/55/asphalt_blur_dark_dusk_evening_expressway_fast_guidance-1501781.jpg!d)

[source PHXhere - Gratuit pour usage personnel et commercial](https://pxhere.com/fr/photo)



Cependant, on peut également jouer sur la sensibilité du capteur afin  de parer au manque ou au trop plein de lumière : c'est le réglage de  [l'ISO](https://fr.wikibooks.org/wiki/Photographie/Les_premiers_pas/La_sensibilit%C3%A9_ISO) :

* Si une ouverture trop petite induit une vitesse trop lente, et donc  un risque de flou important, on va rendre le capteur plus sensible : la  quantité de lumière nécessaire diminue donc on pourra augmenter la  vitesse. On prendra alors une pellicule avec un ISO plus important ou,  sur un numérique, on augmentera la sensibilité du capteur via le  logiciel de l'appareil (qui le fait automatiquement le plus souvent).

* Cependant, augmenter la sensibilité du capteur favorise l'apparition  de parasites (bruit) sur celui-ci et augmente le grain de la photo : le  procédé a donc des limites...

![réglage ISO -b source wikipédia - Creative Commons](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Sensibilit%C3%A9_ISO_auto_DSC7348EC.jpg/800px-Sensibilit%C3%A9_ISO_auto_DSC7348EC.jpg) 







>  **AINSI, VITESSE ET OUVERTURE SONT INTIMEMENT LIEE !! **
>
> - Une photo prise très vite aura une profondeur de champ restreinte 
> - Une photo avec une grande profondeur de champ aura une vitesse restreinte et un risque de flou 





## Capteur Numérique

Lors de la prise d'une photographie, les rayons lumineux atteignent la surface sensible d’un capteur qui les transforment en une suite de 0 et de 1  : 



**Un appareil photo numérique ne renvoie pas un image mais des données !**



 Il existe actuellement deux différents types de capteurs : les _CCD_ et les _CMOS_. 

 

La surface des capteurs est constituée de multiples _photosites_. Chacun d’entre eux engendrera, sur l'image finale, un point (ou pixel = *picture element*).

* Lorsqu'ils sont frappés par les rayons lumineux, les photosites produisent une impulsion électrique. Celle-ci possède une intensité proportionnelle à la quantité de lumière reçue. L'impulsion est par la suite codée sur un octet (suite de huit bits de valeur 0 ou 1, soit 256 possibilités différentes). 
* Afin de reconstituer les couleurs, chaque photosite est recouvert d'un filtre coloré, ne laissant passer que les rayons d'une certaine couleur (rouge, vert , bleu pour les capteurs fonctionnant en *synthèse additive*).                                            

* **Pour imiter la physiologie de l’œil humain, plus sensible à la couleur verte, il a deux fois plus de filtres verts que de rouges ou de bleus**.  Ce déséquilibre s’explique par l’omniprésence de la composante verte du monde rempli de végétaux dans lequel les animaux évoluèrent pour aboutir aux humains que nous sommes. L'organe visuel devait d'être capable de distinguer plus particulièrement les différentes nuances du vert afin d'augmenter les chances de survie (prédation, cueillette, dissimulation…).

 

* Chaque photosite mesure avec précision l'intensité lumineuse d'une seule couleur primaire. Il manque alors toujours de l'information : les valeurs de rouge et de vert pour les pixels correspondant aux photosites recouverts d'un filtre bleu, de bleu et de vert pour ceux pourvus d'un filtre rouge, etc. Le processeur intégré à l'appareil numérique doit, pour récupérer les mesures manquantes, calculer les informations complémentaires en se basant sur la couleur mesurée par les pixels adjacents. Cette méthode donne lieu à d'excellents résultats la plupart du temps, mais révèle parfois ses limites dans des conditions extrêmes (forts contrastes notamment).

 

* Puisque chaque canal de couleur est codé sur un octet, soit 256 valeurs différentes possibles, nous obtenons une étendue de 256 x 256 x 256 = 16,8 millions de couleurs envisageables (c'est le mode *true color*, ou 24 bits). Par ailleurs, quelques appareils milieu et haut de gamme sont capables de gérer des couleurs d'une profondeur plus importante (30 bits, 36 ou même 48 pour certains modèles professionnels). Cela permet notamment une lecture plus fidèle des zones sous ou surexposées, afin d'en ressortir un maximum de détails. Toutefois, l'image est le plus souvent convertie en mode 24 bits. L’ensemble de ces données est alors stockées dans un fichier de données brutes volumineux au format *raw* ou converties au format plus léger *jpg.*

 

_Quelques sources intéressantes :_

[[L'APPAREIL PHOTO NUMÉRIQUE]](https://photoinformatique.wordpress.com/tag/photosites/)

[Université Lille1 ](https://docplayer.fr/25093498-Techniques-de-dematricage-d-images-couleur-camera-couleur-filtre-cfa-algorithmes.html)

[wikipédia](https://fr.wikipedia.org/wiki/Capteur_photographique)

[Clubic](https://www.clubic.com/article-14325-2-la-photographie-numerique-comment-ca-marche.html)



## Les apports du numérique

En argentique, la pellicule réagit à la lumière et fournit une image unique dépendant des réglages effectués par le photographe et de la qualité de l'appareil photo (optique → physique, pellicule → chimie).

Le photographe doit faire des choix :

* Une profondeur de champ réduite mais un arrière-plan flou.

* Une grande profondeur de champ mais une image moins nette.

- Un premier plan très clair mais un arrière-plan manquant de détails.

- Une pellicule avec une sensibilité ISO bien choisie selon la quantité de lumière qui oblige à prendre toutes les photos de la pellicule avec cette même sensibilité. 

 

>  **La photographie numérique affranchit le photographe de tous ces compromis.**
>
> **En traitant les données reçues des capteurs et en les _interprétant_, on peut alors réaliser des photos très compliquées voir impossibles à prendre en argentique**
>
> **La qualité de l'image obtenue après traitement des données dépend en grande partie de la qualité du traitement : c'est le règne de l'algorithme !!**

 

Le smartphone (sortez le, si si ...) intègre une application pour la photographie qui, , la plupart du temps,  ne donne pas accès aux réglages manuels (ou seulement en partie) mais propose des modes pré-programmés (portait, paysage, sport …) bien pratiques mais cachant le fonctionnement de l'appareil.

 

L’application *OpenCamera* , par exemple, permet d’accéder aux réglages de l'appareil photo. Voici quelques exemples qui permettent de voir ce que votre smartphone fait avec les données de ses capteurs grâce aux algorithmes de traitement d'images.

 

#### Luminosité.

Lancer l'application _open Camera_ . L'icône ci-dessous va nous permettre de forcer le réglage de luminosité .

![icone luminosité ](media/opencam_luminosite.jpg)

Prendre une photographie dans une pièce ouverte sur un extérieur éclairé.  propose deux options de réglage d'exposition.



- On veut voir l'intérieur et il faut augmenter la luminosité : à ce moment, l'extérieur, surexposé, sera fondu.

![sur-exposition](media/sur_exposition.jpg)

_Source personnelle – libre de droit_



- On veut voir l'extérieur : il faut sous-exposer la photo mais l'intérieur sera sombre.

![sous-exposition](media/sous_exposition.jpg)

_Source personnelle – libre de droit_



> L'**algorithme** HDR permet de s'affranchir de cette contrainte. Le principe est simple : l'appareil va prendre simultanément plusieurs photographies en partant de réglages sous-exposés jusqu'à des réglages sur-exposés. Il va ensuite reconstituer une photographie de façon à ce que l'exposition soit correcte partout.



Quittez _Opencam_, lancer l'application photo de votre smartphone, s'il possède le mode HDR, activez le et testez une photographie par la fenêtre.

![mode HDR](media/hdr.jpg)

Pour en savoir plus sur le HDR, suivez ce lien : [Tout savoir sur le HDR ?](https://www.nikonpassion.com/quest-ce-que-le-hdr/)



 

####   Profondeur de champ 



Ouvrir le menu en cliquant sur les icônes ci-dessous, sélectionner le focus auto .

![icônes](media/opencam_mise_au_point.jpg)

L'appareil effectue la mise au point sur la zone sélectionnée en cliquant sur l’écran. 

Prendre une photo en mettant un index devant l'objectif. En cliquant sur ce doigt, ce dernier sera net mais l'arrière-plan sera fondu et inversement en cliquant sur l'arrière-plan.

![arrière-plan net- Source personnelle - libre de droit](media/arriere_net.jpg)

 

  ![avant net -Source personnelle - libre de droit](media/avant_net.jpg)  



​                                    

>  L'algorithme **focus stacking** permet de s'affranchir de cette contrainte. Comme pour le HDR, il va prendre plusieurs clichés en faisant varier la profondeur de champ puis reconstituer, à partir de ces multiples données, une unique image optimisée d'un bout à l'autre de la profondeur de champ.

​               

* La première mouche est flou en arrière plant, la seconde floue en avant, la troisième résulte du traitement des deux premières. 

![macro photographie - source wikipédia - creative commons](https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Focus_stacking_Tachinid_fly.jpg/1920px-Focus_stacking_Tachinid_fly.jpg)

 

_Quelques liens_ :

[Comment faire du focus stacking ? ](https://aventures-de-photographe.fr/focus-stacking/)

[encore du focus stacking](https://www.fototripper.com/focus-stacking-in-photoshop-vs-f22-for-landscapes/)



 

### Post-production



Une fois le cliché numérique pris, il est enregistré dans votre appareil soit au format RAW (données brutes, utilisées par les professionnels ou les amateurs éclairés) soit déjà interprété par un algorithme de l'appareil et compressé (perte de données) en _JPG_(le plus souvent).

 

Les logiciels de retouche d'images permettent de modifier les données du fichier source via de nombreux filtres. **LE** logiciel libre incontournable est **GIMP** , concurrent du celèbre **Photoshop** , lui en licence propriétaire. Il existe des logiciels plus légers et simples d'utilisation, à défaut d'être aussi puissant, comme **Photofiltre** , gratuit pour un usage éducatif.



Des applications de retouche sont également disponibles pour les smartphones. Certaines traitent même l'image de la caméra en directe.



**Revers à la médaille : les photos peuvent être modifiées dans un objectif nuisible et générer des fake news, entre autres ….**

 



____________

Mieszczak Christophe Licence CC - BY - SA
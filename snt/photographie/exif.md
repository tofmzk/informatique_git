# EXIF


### La problématique de l’identification d’une photographie



Voici une reproduction d’une carte postale ancienne :



![villa - source wikipédia - domaine public](https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/ZZ_68_-_MARSEILLE_-_La_Corniche_%28Le_Proph%C3%A8te%29.jpg/800px-ZZ_68_-_MARSEILLE_-_La_Corniche_%28Le_Proph%C3%A8te%29.jpg)



Sans la note manuscrite et l’intitulé de la carte, il serait presque impossible de savoir où et à quelle époque cette photographie a été prise. Les informations restent très limitées : impossible de connaitre la date exacte, l’heure, la saison, l’auteur de la photo…. 



 ### L'apport du numérique





Ces problèmes liés à l’archivage et à l’identification trouvent une réponse dans la photographie numérique. 

Outre les informations indispensables à la génération d’une image par un logiciel, le fichier qui permet de représenter une image contient aussi divers renseignements concernant la conception de l’image. Ces informations sont des *métadonnées*, plus spécifiquement appelées ici données *EXIF* (*Exchangeable Image File Format*). Ainsi, peut-on trouver : la marque et le modèle de l’appareil, date et
heure de prise de vue, la localisation GPS etc…



*Exercice :*

* Lancer le logiciel *Photofiltre* ou *GIMP2*et ouvrir l’image *photo_perso_EXIF.jpg (source personnelle)* du répertoire `media`.

	![photo perso](media/photo_perso_EXIF.jpg)



* SI vous utilisez _GIMP_, allez dans le menu « image », sélectionner la dernière rubrique en bas « métadonnées ».

<img src="media/exif_gimp.jpg" alt="exif et gimp"  />

* Si vous utilisez _photofiltre_, allez dans le menu « Fichier », sélectionner « propriétés de l’image » 

	Dans la fenêtre qui s’ouvre, cliquer sur l’onglet « EXIF ».

	

![exif et photofiltre](media/exif_photofiltre.jpg)

**Exercice :**

* A quelle date et à quelle heure a été prise cette photo ?
* Quelle est la marque et la référence de l'appareil utilisé ?
* La sensibilité ISO détermine la sensibilité d’un capteur à la lumière. Cette sensibilité varie en général de 100 (lumière forte) à 800 (lumière faible). La lumière était-elle forte lors de la prise de vue ?
* De combien de pixels l’image est-elle constituée ? 
* Trouver l’endroit précis où a été prise cette image et le nom du monument photographié. Le site [https://www.coordonnees-gps.fr/conversion-coordonnees-gps](https://www.coordonnees-gps.fr/conversion-coordonnees-gps)  vous sera d'une aide précieuse.





_Remarque :_



Détail amusant, l'image numérique de notre carte postale contient des données EXIF... Il s'agit probablement de données liées à l'appareils utilisé pour la scanner.




### Des données modifiables



Ces métadonnées, sauvegardées au moment de la photographie, sont modifiables. Vous pouvez donc ajouter des informations sur une photographie après sa prise (géolocalisation par exemple, date, heure ...) mais cette possibilité peut être utilisé de façon à 'truquer' la photo ou à se l'approprier.



- Sous windows10, en demandant l’affichage du « volet de détails », une bonne partie des données exif sont affichées et modifiables.



**Exercice :**

Modifiez la date et l'heure de la photographie précédente pour la date de la rentrée scolaire à 10h00.



* Avec GIMP2 (dans la dernière version) , choisir « Modifier les métadonnées »...



**Exercice : un peu de géolocalisation**

Modifiez les coordonnées GPS de façon à faire croire que cette photo a été prise près de la mairie de Calais.

Encore une fois, le site [https://www.coordonnees-gps.fr/conversion-coordonnees-gps](https://www.coordonnees-gps.fr/conversion-coordonnees-gps)  vous sera d'une aide précieuse.





 ### base de données



Les données Exif peuvent servir à classer ou trier les photographies (ou plutôt les fichiers
correspondants).

* Dans l'explorateur de fichier, vous pouvez trier les images par dates, heures, auteurs... Ce sont les données EXIF qu'utilise l'OS.

* Il est possible de concevoir un programme (en Python par exemple) qui, accédant aux données Exif de tout un lot de fichiers, serait capable d’extraire par exemple tous ceux qui concernent des prises de vues effectuées une année donnée ou même mieux dans une zone géographique
	donnée ( en dessous du  45ième parallèle par exemple).

* Lorsque vous publiez une photo sur les réseaux sociaux  :

	* ces derniers sont en mesure de vous géolocaliser. Cela est souvent utilisé à des fins de publicités ciblées en vous étudiant vos habitudes  (où aimez-vous aller, à quelle heure, pour quoi faire ...)

	* Tout internaute qui sait utiliser ces données 'cachées' sera également où vous êtes ... et notamment saura que vous n'êtes pas chez vous.

____________

Mieszczak Christophe Licence CC - BY - SA
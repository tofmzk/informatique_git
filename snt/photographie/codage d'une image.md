# Codage d'une image 
# Des 0 et des 1	

En informatique, toutes les données sont codées uniquement à partir de 0 et de 1. Ce type de codage est un codage binaire. Physiquement, Il est lié au passage, ou non, d'un courant électrique. 



### La base 10



Nous, humain du _XXIème_  siècle,  avons l'habitude de compter en base 10 : nous utilisons 10 symboles, les chiffres 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9.

Ainsi nous utilisons, pour compter, ces 9 chiffres dans l'ordre :



0

1

2

3

4

5

6

7

8

9



Arrivé là, nous n'avons plus de symbole ! 

On dit alors que nous avons un paquet de 10 chiffre, une dizaine et 0 chiffre seul, c'est à dire 0 unité. 

Et on continue ainsi ...

| dizaines | unités |
| :------: | :----: |
|    1     |   0    |
|    1     |   1    |
|    1     |   2    |
|    1     |   3    |
|    1     |   4    |
|    1     |   5    |
|    1     |   6    |
|    1     |   7    |
|    1     |   8    |
|    1     |   9    |

Arrivé là , plus de chiffre à nouveau. Pas grave, on prend 2 dizaines et on continue ainsi jusqu'à 99

| dizaines | unités |
| -------- | ------ |
| 2        | 0      |
| ...      |        |
| 2        | 9      |
| ...      | ...    |
| 3        | 0      |
| ...      | ...    |
| 9        | 0      |
| ..       | ..     |
| 9        | 9      |

Arrivé là, de nouveau plus de chiffre mais on ne peut plus augmenter les dizaines qui ont également utilisés tous les chiffres disponibles. Du coup, fait un paquet de cent, qu'on appelle centaine, on dit que, pour l'instant on a 0 dizaine et 0 unité et on continue...

| centaines | dizaines | unités |
| --------- | -------- | ------ |
| 1         | 0        | 0      |
| 1         | 0        | 1      |
| ..        | ..       | ..     |
| 1         | 0        | 9      |
| 1         | 1        | 0      |
| ...       | ...      | ...    |
| 1         | 9        | 9      |
| 2         | 0        | 0      |
| ...       | ...      | ...    |
| 9         | 9        | 0      |
| ..        | ..       | ..     |
| 9         | 9        | 9      |

Vous avez compris : ici, nous allons faire un paquet de mille, 0 centaine, 0 dizaine, 0 unité et c'est reparti...



Ainsi, on peut décomposer un nombre, par exemple 456, de la façon suivante :



456 = 4 * 100 + 5 * 10 + 6 * 10 soit 4 centaines, 5 dizaines et 6 unités



### En base 2

````
Il n'y que 10 types de personnes : ceux qui savent compter en base 2 et les autres.
````



En base 2, en binaire donc, nous n'avons que deux chiffres : 0 et 1



Allons-y comptons :

0

1

Plus de chiffre !! saurez-vous continuez à compter en base 2

...







**Exercice :**

Saurez vous retrouver la valeur en base 10 des nombres ci-dessous écrits en base 2 ?

${1010_2~ = 0 + 1 * 2 + 0 *2^2^ + 1 * 2^3 = `$

$`1111_2`$ =

$`111101_2`$ =



**Rem :**

* Avez-vous noté le 'petit ² à droite des nombres écrits en base 2 ? Ils sont là pour préciser la base et éviter de se trouver devant une chose du genre 10 = 2 qui s'écrira plutôt $`10_2 = 2`$

* Pour organiser les données, on regroupe ces 0 et ces 1, appelés _bits_, par paquets de 8 bits, appelés _octets_.



**Exercice  :**



* Comment écrit-on le 0 sur un octet ?







* Quel est le plus grand nombre entier de la base 10 que l'on peut coder sur un octet ? 







### Conséquences 

* Nous savons coder avec uniquement des 0 et des 1 n'importe quel nombre entier.
* Il est possible de coder des entiers négatifs et même des décimaux en base 2, même si nous ne verrons pas cela en détails ici.  A partir de là, il est possible de coder n'importe quoi : du texte, des images, du son , des vidéos ...



# Le format PBM pour Portable BitMap



Le codage d'une image au format *PBM* comporte des informations diverses pour permettre sa  manipulation afin de , par exemple, afficher l’image. Ces images PBM sont habituellement stockées dans des fichiers dont l'extension est **.pbm**



Les informations nécessaires à ce codages sont :

* **P1** stipule qu'il s'agit d'un format PBM. Il en existe d'autres, il faut donc le préciser.

* Un caractère d'espacement (espace ou nouvelle ligne)

- La largeur de l'image 

- Un caractère d'espacement

- La hauteur de l'image 

- Un  caractère d'espacement

- Les données de l'image : 
	- L'image est codée ligne par ligne en partant du haut

	* Chaque ligne est codée de gauche à droite
	*  Un pixel noir est codé par un 1,
	* Un pixel blanc est codé par un 0 (d’où le nom *bit map*)
	*  Les caractères d'espacement à l'intérieur de cette section sont ignorés

 

*Exemple : voici le code d'une image en PBM* 



P1

20 12
00000000000000000000
00001111111111100000
00001100000001100000
00001100000001100000
00001100000001100000
00001100000001100000
00000111111111000000
00001100000001100000
00001100000001100000
00001100000001100000
00001100000001100000
00001111111111100000



**Exercice 1 :**

*  Copier le contenu de ce cadre dans un éditeur de texte ou Notepad

* Cliquer sur l'icone _enregistrer_ en forme de disquette puis :

	*  choisissez _.txt normal text file_ pour le type de fichier
	*  Nommer le fichier _huit.ppm_
	*  enregistrez !

* Ouvrir le fichier obtenu et zoomer. On utilisera GIMP ou Ink, deux logiciels libres.

	

**Exercice 2 :**

Créer une image représentant la lettre J ci-dessous.



![J](https://upload.wikimedia.org/wikipedia/commons/a/ad/Example_of_ASCII-art_turned_into_a_bitmap_scale20.pbm.png)



**Exercice 3:**

Voici, ci-dessous, le contenu d’un fichier codant une image au format PBM.

Compléter les informations manquantes et décrire ce que représente l’image correspondante.





9 

110000011

011000110

001101100

000111000

001101100

011000110

110000011



**Exercice 4:**

Aller une petite dernière : codez moi ce smiley au format pbm !



![smiley](media\smiley.jpg)



## Le format PGM : Portable Grey Map. 

Difficile de rendre une image attrayante avec seulement le noir et le blanc. Le format *PGM* permet de rendre des dégradés de gris sur une échelle allant de 0 (noir) à 255 (blanc).

La structure de l’image est sensiblement la même que pour le *PBM*, on remplace les 0 et les 1 par les nombres correspondant « au code de gris ». Plus ce nombre est élevé, plus le gris est proche du blanc.



Les informations nécessaires au codages sont :

* **P2 ** stipule qu'il s'agit d'un format PBM. Il en existe d'autres, il faut donc le préciser.

- Un caractère d'espacement (espace, nouvelle ligne)

- Largeur de l'image

- Un caractère d'espacement

- Hauteur de l'image

- Un caractère d'espacement

- La valeur maximale utilisée pour coder les niveaux de gris, cette valeur doit être inférieure à 65536 

- Un caractère d'espacement

- Données de l'image : 
	- L'image est codée ligne par ligne en partant du haut

	* Chaque ligne est codée de gauche à droite

	* Chaque pixel est codé par un nombre entre les 0 et la valeur maximale prévue.

	* Les valeurs sont séparés par un caractère d'espacement. 

	* Un pixel noir est codé par la valeur 0, un pixel blanc est codé par la valeur maximale prévue.

	* Chaque niveau de gris est codé par une valeur entre ces deux extrêmes, proportionnellement à son intensité.

		

_Exemple : voici le code d'une image en PGM_

P2
10 10
50
00 00 00 00 00 00 00 00 00 00

00 10 10 10 10 10 10 10 10 00

00 10 20 20 20 20 20 20 10 00

00 10 20 30 30 30 30 20 10 00

00 10 20 30 40 40 30 20 10 00

00 10 20 30 40 40 30 20 10 00

00 10 20 30 30 30 30 20 10 00

00 10 20 20 20 20 20 20 10 00

00 10 10 10 10 10 10 10 10 00

00 00 00 00 00 00 00 00 00 00



**Exercice 1 :**

-  Copier le contenu de ce cadre dans un éditeur de texte ou Notepad
-  Cliquer sur l'icone _enregistrer_ en forme de disquette puis :
	- choisissez _.txt normal text file_ pour le type de fichier
	- Nommer le fichier _gris.pgm_
	- enregistrez !
-  Ouvrir le fichier obtenu et zoomer. On utilisera GIMP ou Ink, deux logiciels libres.



**Exercice 2:** 



La copie d’écran ci-dessous montre une image de format *PGM*

![PGM](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Kinmen_sorghum_party.jpg/800px-Kinmen_sorghum_party.jpg)

​                                                  

 

* Quelle est la taille minimale de cette image en pixels ?



* Combien y a-t-il de nuances de gris en plus du noir et du blanc ?



*  Proposer un codage de l’image.



## Images en Rouge, Vert, Bleu



Le format correspondant aux deux formats simples précédents, adapté à la couleur est le format PPM (Portable PixMap).

Le début du fichier est très ressemblant aux deux précédents. On retrouve les mêmes paramètres, type (P3), ligne de commentaire, dimensions puis la valeur maximale pour l’intensité des couleurs. Celle-ci est couramment 255 pour décrire toute la palette de couleurs au standard RVB.



_Exemple : entête d'un fichier PPM_



P3 # le type de codage

10 10 # les dimensions

255 #nuance maximale



 On code ensuite chaque ligne pixel par pixel avec les trois composantes Rouge puis Vert puis Bleu. Chaque couleur est codé par un entier, allant de 0 à la valeur maximale choisie, 0 correspondant à l'absence de couleur et la valeur maximale à la couleur vive.



_Exemple : voici un codage possible_

P3
3 2
255
150 0 0 0 255 0 0 255 255
120 200 075 255 0 255 075 0 0

 

**Exercice 1 :**

* Combien de pixel compte cette image ?



* Copier le contenu de ce cadre dans un éditeur de texte ou Notepad

	* ##### Cliquer sur l'icone _enregistrer_ en forme de disquette puis :

	* ##### choisissez _.txt normal text file_ pour le type de fichier

	* Nommer le fichier _couleur.ppm_

	* Enregistrez !

* Ouvrir le fichier obtenu et zoomer. On utilisera GIMP ou Ink, deux logiciels libres.



_Remarque : A propos du poids d'une image_



Prenons le cas d’une image de 300 px par 300 px. 

* Combien y-a-t-il de pixel dans mon image ?



* De combien d'entiers ais-je besoin pour la coder ?



* Combien d'octet cela représente-t-il ?



* Plus qu'a diviser par 1000 et vous aurez le poids de l'image en kilooctet (ko)



* Plus qu'a re-diviser par 1000 et vous aurez le poids de l'image en mégaoctet (mo)

 



## JPJ, PNG ....



Pour coder une image en couleur, on a besoin de connaitre la valeur des 3 composantes Rouge, Vert et Bleu. Vous avez vu qu'une image brute peut vite prendre une place gigantesque !



En cliquant [ici](http://www.in2white.com/) vous pourrez vous amuser à zoomer et dé-zoomer  sur les détails d'une image de 365 gigapixels...



On utilise alors des astuces pour faire en sorte que ces images prennent moins de place : on les compresse !



Il existe deux formats principalement utilisés parmi des centaines d'autres:

* le format *.JPG* (ou *.JPEG*) qui compresse une image de façon très performante ... en contrepartie d'une perte de données et donc de qualité.
* le format *.PNG* qui fait de même mais sans perte de données ... au détriment du taux de compression.



**Exercice :**

* Ouvrez Gimp
* Créez une image de 300px sur 300 px
* Réalisez un dégradé de couleurs.
* C'est beau ?
* Exportez l'image au format *.jpg* avec un fort taux de compression c'est à dire une qualité faible.
* Fermer votre image
* Ouvrez l'image que vous avez sauvegarder
* C'est beau ?





____________

Mieszczak Christophe Licence CC - BY - SA
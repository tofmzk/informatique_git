#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: `rvb-tsl` module

:author: Académie de Lille _ Mieszczak Christophe
         Univ. Lille

:date: mars 2019

Conversion image RVB <-> TSL.

"""

from PIL import Image
from math import fabs,floor

#### fonctions necessaires aux conversions

def maxRVB(r,v,b):
    if r>b and r>v:
        return r
    else :
        if v>b :
            return v
        else :
            return b

def minRVB(r,v,b):
    if r<b and r<v :
        return r
    else:
        if v<b :
            return v
        else:
            return b

def cRVB(r,v,b):
    return max(r,v,b)-min(r,v,b)

################ rvb->tsl

def teinteRVB(r,v,b):
    """retourne la teinte correspondante aux composante r,v,b entre 0 et 360ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â°"""
    if cRVB(r,v,b)==0:
        return -1
    if maxRVB(r,v,b)==r:
        return (60*(((v-b)/cRVB(r,v,b))%6))
    if maxRVB(r,v,b)==v:
        return (60*(((b-r)/cRVB(r,v,b)+2)%6))
    if max(r,v,b)==b:
        return (60*(((r-v)/cRVB(r,v,b)+4)%6))

def saturationRVB(r,v,b):
    """retourne la luminosite  correspondante aux composante r,v,b entre 0 et 100%"""
    if (1-fabs(2*luminositeRVB(r,v,b)/100-1))==0:
        return 0
    else:
        return ((cRVB(r,v,b)/(1-fabs(2*luminositeRVB(r,v,b)/100-1)))/255*100)


def luminositeRVB(r,v,b):
    """retourne la saturation  correspondante aux composante r,v,b entre 0 et 100%"""
    return (((maxRVB(r,v,b)+minRVB(r,v,b))/2)/255*100)

def RVBversTSL(r,v,b):
    """retourne les 3 composantes TSL correspondantes aux 3 composants RVB
    ex :
        t,s,l=RVBversTSL(150,100,200) stocke dans les varibles t,s et l les 3 composantes TSL correspondantes aux 3 composantes RVB
    """
    return (teinteRVB(r,v,b),saturationRVB(r,v,b),luminositeRVB(r,v,b))

##########################tsl->rvb
def rougeTSL(t,s,l):
    """ retourne la composante rouge correspondante au codage t,s,l"""
    s=s/100
    l=l/100
    t=t/60
    c=(1-fabs(2*l-1))*s
    m=l-0.5*c
    x=c*(1- fabs(t%2-1))
    valeurs=[m*255,(c+m)*255,(x+m)*255,m*255,m*255,(x+m)*255,(c+m)*255]
    return floor(valeurs[floor(t+1)])


def vertTSL(t,s,l):
    """ retourne la composante verte correspondante au codage t,s,l"""
    s=s/100
    l=l/100
    t=t/60
    c=(1-fabs(2*l-1))*s
    m=l-0.5*c
    x=c*(1- fabs(t%2-1))
    valeurs=[m*255,(x+m)*255,(c+m)*255,(c+m)*255,(x+m)*255,m*255,m*255]
    return floor(valeurs[floor(t+1)])


def bleuTSL(t,s,l):
    """b(t,s,l) retourne la composante bleu correspondante au codage t,s,l"""
    s=s/100
    l=l/100
    t=t/60
    c=(1-fabs(2*l-1))*s
    m=l-0.5*c
    x=c*(1- fabs(t%2-1))
    valeurs=[255*m,m*255,m*255,(x+m)*255,(c+m)*255,(c+m)*255,(x+m)*255]
    return floor(valeurs[floor(t+1)])


def TSLversRVB(t,s,l):
    """retourne les 3 composantes RVB correspondantes aux 3 composants TSL
    ex :
        r,v,b=TSLversRGB(100,50,20) stocke dans les varibles r,v et b les 3 composantes RVB correspondantes aux 3 composantes TSL
    """
    return (rougeTSL(t,s,l),vertTSL(t,s,l),bleuTSL(t,s,l))

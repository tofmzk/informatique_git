<p style='color:black;background-color : red;font-size:64px;text-align : center' > Photographie numérique </p>

# Historique



Le premier procédé photographique, ou héliographie, a été inventé par**[Nicéphore Niepce](http://www.photo-museum.org/fr/)** vers **1824**. 

Les images étaient obtenues avec du bitume de Judée étendu sur une plaque d’argent, après un temps de pose de plusieurs jours. Le principe de la photographie argentique était né. 

<img src="http://www.open-museeniepce.com/protectimage?img=1975.149.3.1P38B.jpg" width ='500px' height="300px">



([Source OpenMuséeNiépce – libre de droit](http://www.open-museeniepce.com/recherche-photos/photo,3418))



Au cours du temps, le procédé a évolué grâce aux progrès de la chimie (capteur), de la physique (optique) et de la mécanique (mécanismes de l'objectif et du boîtier) :

* L’invention du négatif intervient en **1864**, à l’initiative de **William Henry Fox Talbot**, mais ce négatif n’a encore rien à voir avec celui que nous connaitrons plus tars… Il faut attendre l’industriel américain George Eastman, fondateur de la société Kodak, pour concevoir la pellicule moderne.  Doucement, la photographie commence à se démocratiser...

	

	![pellicule photo - source PixaBay - Libre](https://cdn.pixabay.com/photo/2014/11/28/19/32/filmstrip-549120_960_720.jpg)



* En **1869**, **Louis Ducos du Hauron** réussit la première photographie en couleurs. Il réalisa trois photos d’un même sujet, au travers d’un filtre respectivement rouge, bleu et jaune. En superposant exactement les trois images, il obtint la restitution des couleurs. 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Duhauron1877.jpg/220px-Duhauron1877.jpg" width ='500px' height="300px">

[source wikipedia - domaine public](https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Duhauron1877.jpg/220px-Duhauron1877.jpg)







* Quelques années plus tard, en 1935, la pellicule couleur fait son apparition et se répand à grande échelle dans les années 50. La photographie devient grand public.

	

![clic clac merci Kodak](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Folding_Pocket_Kodak_Camera_ad_1900.jpg/384px-Folding_Pocket_Kodak_Camera_ad_1900.jpg)

![appareil photo](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Rolleiflex_camera.jpg/455px-Rolleiflex_camera.jpg)

![reflex](https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Leica_M2_Summicron_35.jpg/800px-Leica_M2_Summicron_35.jpg)

![appareil photo](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Olympus_XA_camera_and_film.jpg/800px-Olympus_XA_camera_and_film.jpg)



* En 1975, [Steven Sasson](https://fr.wikipedia.org/wiki/Steven_Sasson), un ingénieur américain travaillant chez KODAK met au point [le premier appareil photo numérique](https://pix-geeks.com/premier-appareil-photo-numerique-1975/?__cf_chl_tk=40GvfRmc0rXKai38.dTfN5NaGmU13p6dF60p24RMenI-1709758070-0.0.1.1-1279). Ce prototype pèse 3,6 kg et capte des images de 100 × 100 pixels en noir et blanc grâce à un nouveau capteur electronique qui remplace la pellicule. L'enregistrement de la photo, sur port d'une bande magnétique sur [cassette](https://fr.wikipedia.org/wiki/Cassette_audio), prend 23 secondes. Suivez donc [ce lien](https://www.laboiteverte.fr/le-premier-appareil-photo-numerique/) pour en apprendre davantage !



Puis tout s’accélère….

* En **1992**, Logitech lance un appareil numérique, à connecter sur un micro-ordinateur, d'une définition de376 × 284 pixels, et stocke 36 photos sur sa mémoire intégrée de 1 Mo.

![LOGITECH 6 Source wikipedia - Creative Commons Partage Condition](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Logitech_FotoMan-P4191196-white.jpg/600px-Logitech_FotoMan-P4191196-white.jpg)

* En **1999** sort le Nikon D1, premier appareil reflex totalement numérique destiné au marché des
  professionnels, d'une définition de 2,7mégapixels. Il pèse encore 1,1kg.

![Nikon D1 6  Source wikipedia - Creative Commons Partage](https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Nikon_D1_8373.jpg/600px-Nikon_D1_8373.jpg)



*  En **2000** , Samsung et Sharp sortent le premier téléphone capable de prendre 20 photos de 0,35
  mégapixels. 

![premier téléphone capable de prendre des photos - Source wikipedia – licence GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Sharp_J-SH04_CP%2B_2011.jpg/681px-Sharp_J-SH04_CP%2B_2011.jpg)





* En **2005**, Canon sort le premier réflexe numérique 24 × 36 à prix abordable, référence pour les
  photo-journalistes jusqu'en 2008

![Canon 2005 - source wikipedia – licence CC-BY-SA](https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Canon_EOS_650.jpg/797px-Canon_EOS_650.jpg)



* En **2007**, le premier téléphone de la marque à la pomme capture des images de 2 mégapixels.

  ![premier AïePhone - Source wikipedia - Creative Commons](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Iphone_3GS-1.jpg/331px-Iphone_3GS-1.jpg)





* Toujours en **2007**, Kodak, pourtant inventaire du premier appareil photo numérique, annonce la
  fermeture de son dernier laboratoire en France.

![kodak - source wikipedia - domaine public](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Kodak_pocket_camera_advertisement_1900.JPG/423px-Kodak_pocket_camera_advertisement_1900.JPG)



* En **2019**, les appareils photos numériques, et la plupart des smartphones sont capables de
  prendre simultanément plusieurs photos de très hautes résolutions, avec une grande profondeur de couleurs et de les combiner pour améliorer leur rendu ou obtenir des effets spéciaux.



![le mot de la fin](https://i.pinimg.com/originals/7d/22/ce/7d22cec6a3fa3c3250de622c5a7263d2.jpg)

____________

Mieszczak Christophe Licence CC - BY - SA
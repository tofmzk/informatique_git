#  Les petits mondes



Toutes les applications de réseaux sociaux mettent également en œuvre un autre type d'algorithme : **les algorithmes de recommandation**.

A partir des traces laissées par l'internaute membre du réseau (toutes les opérations qu'il a effectuées) ces algorithmes proposent d'entrer en relation avec d'autres membres, de consulter des contenus mais affichent également des publicités.

Ces algorithmes fonctionnent de manière implicite : 

* ils observent (et gardent la trace) de tous les contenus et autres membres du réseau que l'internaute membre a regardé (et à quelle fréquence) 
* ils analysent la structure de son «réseau» (les membres auxquels il est relié et ses centres d'intérêts). Le but est de découvrir ses goûts de manière à le mettre en relation avec d'autres membres qui les partagent et également de lui proposer des publicités susceptibles de l'intéresser. Ce dernier aspect est souvent au centre du modèle économique de l'application.



Dans un premier temps, le fait que votre réseau vous connait engendre une impression agréable : on vous propose ce que vous aimez, on vous met en contact avec des gens qui vous ressemble ...

Mais ces algorithmes contribuent fortement à vous enfermer dans des «petits mondes»  : au lieu d'ouvrir des nouveaux centres d'intérêt et de nouveaux horizons à leurs membres, ils ont tendance à les faire évoluer dans un monde finalement assez clos. 



[Voici, par exemple, un article sur ce phénomène qui aurait eu une incidence sur l'élection de Donald Trump](https://www.konbini.com/fr/tendances-2/filter-bubble-reseaux-sociaux-ont-louper-trump/).



> A votre avis, quelles autres conséquences grave peut avoir cet enfermement dans des petits mondes ?



Ce phénomène  peu parfois être à l'origine de comportement radicalisé engendrant haine et violences : quand on ne rencontre que des personnes qui ont exactement les mêmes opinions que nous, qu'on ne nous propose que des contenus qui tendent à renforcer nos convictions, on peut avoir l'impression, à tort, que notre opinion est la seule valable, que tout le monde, ou presque, la partage et qu'il n'y a aucune autre voie possible ou acceptable ... Attention, un réseau social n'est pas le reflet exact de la société et une opinion n'est qu'une opinion, pas la vérité absolue. 



_________

Par Mieszczak Christophe

Licence CC BY SA


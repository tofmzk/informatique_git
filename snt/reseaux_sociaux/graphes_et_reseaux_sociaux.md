# Les Graphes : Introduction

Les structures de graphes sont des structures relationnelles non hiérarchiques,  utilisées pour modéliser de nombreux problèmes :

* le réseau routier
* les réseau électrique
* Internet
* Les réseaux sociaux

    ![réseaux socaux](https://cdn.pixabay.com/photo/2018/11/29/21/51/social-media-3846597_960_720.png)



Nous allons ici utiliser ces derniers pour introduire la notion de graphes.



## Amitiés réciproques et graphes non orientés



Un petit groupe de personnes fréquente  un réseau social sur lequel les utilisateurs partagent, ou non, un lien d'amitié réciproque avec d'autres utilisateurs



Le tableau ci-dessous récapitule les liens existant dans le petit groupe.

|      | Etta | Ray  | Mick | Ella | Joe  | Jim  |
| ---- | :--: | :--: | :--: | :--: | ---- | :--: |
| Etta |      |  x   |      |  x   |      |      |
| Ray  |  x   |      |  x   |  x   |      |      |
| Mick |      |  x   |      |      | x    |  x   |
| Ella |  x   |  x   |      |      |      |      |
| Joe  |      |      |  x   |      |      |  x   |
| Jim  |      |      |  x   |      | x    |      |



>  Citez des exemples de réseaux qui fonctionnent sur ce modèle.



On peut représenter ces liens par le graphe ci-dessous où : 

* Chaque _rond_, appelé **sommet**, correspond à un nom.
* Chaque lien d'amitié réciproque entre deux personnes, appelé **arête**, est symbolisé par un trait entre les deux sommets les représentant :

![graphe de l'amitié](media/graph_amitie.jpg)

* Ainsi, un graphe est un ensemble de **sommets** reliés entre eux par des **arêtes** .



> Dans chaque situation ci-dessous, que l'on peut représenter par un graphe, précisez ce que modéliseraient les sommets et les arêtes :
>
> 



### _"Un peu"_ de vocabulaire

 

* Lorsqu'il y a un arc entre un sommet `S1` et un sommet `S2`, on dit que ces deux sommets sont **adjacents** ou **voisins**. L'ensemble des sommets adjacents/voisins d'un sommet constitue son **voisinage**.

    >  Quel est le voisinage de Ray ?
    >
    > ```text
    > 
    > ```
    >
    > 



* On appelle **ordre** du graphe son nombre de sommets. 

    > Quel est l'ordre de notre graphe ?
    >
    > ```text
    > 
    > ```
    >
    > 



* Le nombre d'arêtes partant d'un sommet est son **degré**. Listons les degrés de chaque sommet :

	| Etta | Ray  | Mick | Ella | Joe  | Jim  |
	| :--: | :--: | :--: | :--: | :--: | :--: |
	|  1   |  3   |      |      |      |      |



* Un **chemin** est une suite de sommets reliés par des arêtes. La **longueur** de ce chemin est **le nombre d'arêtes** qu'il comporte.

	>  Quel est la longueur du chemin  `Ray - Mick - Jim` ?
	>
	> ```text
	> 
	> ```
	>
	> 

* Il existe des **chemins** particuliers :

    * S'il n'empruntent pas deux fois le même sommet, on dit qu'ils sont **élémentaires**.
    * S'il n'empruntent pas deux fois le même arc, on dit qu'ils sont **simples**.
    * Un chemin simple qui commence et termine par le même sommet est appelé **un cycle**

    > Dans notre graphe :
    >
    > * Donner un chemin élémentaire de Ray à Jim.
    >
    >     ```text
    >     
    >     ```
    >
    > * Donner un chemin simple non élémentaire de Ray à Jim
    >
    >     ```txt
    >         
    >     ```
    >
    >     
    >     
    > * Donner trois cycles.
    >
    >     ```text
    >         
    >     ```
    >
    >     

* S'il existe toujours un chemin reliant deux sommets quelconques du graphe, on dit alors que ce graphe est **connexe**.

    > Le graphe est-il connexe ?
    >
    > ```txt
    > 
    > ```
    >
    > 

    

* La **distance** entre deux sommets est la longueur du plus court chemin qui les relie.

    > Quelle est la distance entre Ray et Jim ?
    >
    > ```text
    > 
    > ```
    >
    > 




## Je te suis, ils te suivent ... les graphes orientés



Les liens ne sont pas toujours symétriques. Sur certains réseaux sociaux, un utilisateur peut en suivre d'autres sans que ceux-ci les suivent forcément. Les _arêtes_ sont alors **orientées** et ne fonctionnent que dans un seul sens.





Voici par exemple un graphe orienté d'un tel réseau social.

![graphe orienté](media/graphe_oriente.jpg)

> Citez des exemples de réseaux qui fonctionnent sur ce modèle.



_Remarque :_

Selon qu'on parle d'un graphe orienté ou non, le vocabulaire peut varier... 

* On parle d'**arcs** pour un graphe non orienté mais plutôt **d'arêtes** s'il est orienté.
* On parle de **sommets** pour un graphe non orienté mais plutôt **de noeuds** s'il est orienté.
* On parle de **chemins** pour un graphe non orienté mais plutôt **de chaînes** s'il est orienté.
* On parle de **voisinage** d'un sommet pour un graphe non orienté mais de **successeurs** (les noeuds vers lesquels _pointe_ le noeud) de **prédécesseurs** (les noeuds qui _pointent_ vers notre noeud) d'un **noeud** s'il est orienté.
* Pour finir le **degré** d'un noeud est le nombre de ses successeurs.




> Complétez ci-dessous le tableau des successeurs et prédécesseurs.
>
> |      | Successeurs | Prédécesseurs | Degré |
> | ---- | :--: | :--: | :--: |
> | Etta |      |      |  |
> | Ray  |      |      |  |
> | Mick |      |      |  |
> | Ella |      |      |  |
> | Joe  |      |      |  |
> | Jim  |      |      |  |
>



> Notre graphe orienté est-il connexe ? Justifier.
>
>   
>
> 

_________

Par Mieszczak Christophe Licence CC- BY - SA
source des images : 

* [pixabay licence](https://pixabay.com/fr/vectors/m%C3%A9dias-sociaux-connexions-3846597/)
* production personnelle
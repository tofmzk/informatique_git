# Modèle économique et données privées



### Quand c’est gratuit, c’est vous le produit !

Les modèles économiques des applications de réseautage social sont symbolisés par ce slogan .



Comment pouvez vous être _le produit_ ? Grâce à vos données personnelles : Elles permettent de cibler vos attentes et son chèrement recherchées.



* [Les données personnelles](https://fr.wikipedia.org/wiki/Données_personnelles), leur exploitation et leur commercialisation, sont devenues au fil des  années un marché considérable : le marché des données personnelles a une valeur estimée à **300 millions de dollars**. 

* De nombreuses entreprises ont élaboré les premiers modèles fondés sur l’analyse et/ou la revente des  données personnelles pour développer leur service. L’économie des  données personnelles repose en particulier sur l’évolution de deux  modèles :

	- Les modèles bifaces qui se sont construits sur une logique de troc implicite : données personnelles contre service gratuit.
	- Les modèles de services pour lesquels les données personnelles sont  un véritable carburant : grâce à la personnalisation, les services sont  plus efficaces et pertinents pour l’utilisateur et bénéficient tant à  l’entreprise qu’au client (dans la limite des règles de consentement).

	

* Le sujet des données personnelles fait l’objet de nombreux débats  animés opposant souvent deux conceptions idéologiques et culturelles :  celle libérale, d’influence anglo-saxonne et celle protectionniste,  d’influence européenne. Autrement dit, la donnée personnelle est un  véritable or noir pour l’économie numérique mais constitue également un  risque majeur pour [les libertés individuelles](https://fr.wikipedia.org/wiki/Libertés_fondamentales), ce qui impacte la manière d’innover. 



>   
>
> * Comment peut-on utiliser vos données personnelles pour gagner de l'argent  ?
> 	* 



### Autres financements

* Les accès  « premium » payants qui offrent des services suplémentaires : 236 millions de dollars de recettes en 2011 pour  l’ensemble des sites communautaires. 
* Les "dons" des membrs du réseaux : Vous êtes fan d'un _influenceur_ d'un réseau, vous pouvez lui faire des dons ou financer ses prochaines publications. 
* Avec les réseaux sociaux, un nouveau métier est né : les influenceurs. Ces derniers, souvent par placement de produits, gagne de l'argent via les réseaux. Ces derniers peuvent réclamer une commisio, c'est à dire une part de leurs bénéfices.



> 
>
> Citez des exemples de réseaux sociaux qui utilisent ces modèles économiques.
>
> 









_________

Par Mieszczak Christophe			Publié sous licence CC-BY-SA

sources images :

* [réseaux sociaux - pixabay](https://pixabay.com/fr/illustrations/r%C3%A9seaux-sociaux-ic%C3%B4nes-twitter-1863613/)
* [Cyber voleur, pngwall CC 4.0 BY-NC](http://www.pngall.com/web-security-png/download/2631)
* [Harcèlement - pixabay](https://pixabay.com/fr/illustrations/ordinateur-harc%C3%A8lement-sur-internet-5777377/)
* [privé - pixabay](https://pixabay.com/fr/illustrations/bouclier-priv%C3%A9-public-internet-www-105499/)







* vos centres d'intérêts    ➔  publicité ciblée, meilleur connaissance des goût du public pour les entreprises afin de faciliter la création de nouveau produit 'tendance', 
* géolocalisation    ➔  publicité ciblée, meilleure connaissance de vos centre d'intérêt
* photographie publiée   ➔ droit de la photographie cédée ? réutilisation de la photographie dans la publicité
* utilisation du micro des smartphone  ➔  [Facebook a démenti se servir des micros des smartphones](https://www.phonandroid.com/facebook-capable-espionner-grace-micro-smartphone.html)  pour espionner les gens, _pour l'instant_.  Cependant, lorsque vous installez Facebook sur votre portable, il en demande l'accès et il faut accepter de lui donner pour que l'installation se poursuive. Pour lui interdire d'utiliser le micro, il faut ensuite retourner dans les paramètre du smartphone, dans le menu _applications_ puis _autorisations_ pour en désactiver l'accès.
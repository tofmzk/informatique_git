# Exposés autour des réseaux sociaux



Vous trouverez ici des sujets d'exposés :

* Formez des groupes de trois ou quatre personnes.
* Choisissez un sujet parmi ceux présentés ci-dessous.



Vous avez deux semaines pour le préparer avant de l'exposer au tableau devant la classe. Vous pouvez utiliser les ressources disponibles sur le [git](https://framagit.org/tofmzk/informatique_git/-/blob/master/snt/readme.md) concernant les réseaux sociaux, exploiter vos connaissances de ces réseaux et faire vos propres recherches.



Réalisez un diaporama (au moins 10 diapositives) traitant des questions suivantes selon le sujet choisi.



## Sujet 1 : Présenter un réseau social.



Choisissez un réseau social de votre choix (accessible légalement pour des personnes de votre âge) et présentez le en préparant un diaporama sur chacun des points suivants :

1. Carte d'identité du réseau : date de création, propriétaire, nationalité, Nombre d'abonnés, finalité.

2. Particularités de ce réseau qui lui permettent de se démarquer des autres. Quel est le public visé ?

3. Quel est le modèle économique de ce réseau (comment gagne-t-il de l'argent ?)

4. Donner des apports positifs de ce réseau social. Citez des exemples dans l'actualité, si possible.

5. Donnez des exemples de dérives ou d'effets négatifs de ce réseau. Citez des exemples dans l'actualité, si possible. 

   

_Remarque :_

Plusieurs groupes peuvent choisir ce sujet sur des réseaux sociaux différents.

 

## Sujet 2 : configurer un compte

Il existe trop de réseaux sociaux pour parvenir présenter de façon exhaustive la gestion des paramètres de confidentialité. Choisissez l'un d'entre eux. Réaliser un diaporama traitants des questions suivantes sur un réseau social de votre choix  :

1. Quel est le thème global du réseau social ? Quel est le type de communauté qu’il tente de fédérer ?

2. Quelles Informations faut-il  transmettre lors de la création du compte ?

3. Quand l'accès à la configuration du compte vous est-elle proposer par le réseau ? Comment accéder à cette configuration en dehors de ces propositions ?

4. Quelles données le réseau collecte-t-il par défaut ? Peut-on, via la configuration du compte, restreindre cette collecte ?

5. Le réseau peut-il utiliser le micro ou la caméra de votre téléphone ? Si oui, quel est son intérêt et comment l'en empêcher ? 

6. Lorsqu'on publie sur ce réseau, peut-on configurer le compte de façon à rendre ses données totalement privée ? Totalement publique ? Les restreindre à un plus petit groupe (si oui lequel ?) 

7. Légalement, quels pourraient être les conséquences d'une publication jugée insultante ou considérée comme harcèlement ou mensongère selon que la publication soit privée ou publique ?

    



## Sujet 3 :  RGPD



Réalisez un diaporama traitant des points suivants :

1. Qu'est-ce que c'est ? De quand cela date-t-il ? 

1. Quel organisme veille au respect du RGPD ?

2. Quels sont ses  objectifs ? 

3. Comment protégeait-on les données avant ? Quels sont les changements principaux de cette loi par rapport à ses prédécesseurs ?

4. Donner des exemples où la RGPD a conduit à des procès de grand nom des réseaux sociaux.

5. La protection des données s'applique-t-elle de la même façon partout dans le monde ? 

5. Comment connaitre ce qu'une entreprise, comme Google, connait de vous ?

5. Qu'est-ce que le déférencement ? Le droit à l'oubli ?

    

## Sujet 4 : les fake news

1. Quels sont les différents type de fakenews ?

2. Quels peuvent être les objectifs d'une fakenews ?

3. Quels sont les dangers des fakenews ?

4. Quels sont les bons reflexes pour vérifier si une information est crédible ou pas ?

5. Comment trouver la source d'une image ?

    

## Sujet 5 : La e-reputation

1. Qu'est-ce que c'est ?

2. Pourquoi avoir une bonne e-reputation est important ?

3. Pourquoi utiliser un pseudo est-il, dans ce cadre, important ?

4. Peut-on surveiller ce qui est publié sur nous ?

5. Peut-on connaitre ce qui est publié sur nous sur le web ?

6. Peut-on supprimer ce qui est publié sur nous ?

    

## Sujet 6 : Autour du mot de passe



1. Qu'est-ce-qu'un bon  mot de passe  ? Suivez grâce à ce [lien](https://www.cnil.fr/fr/les-conseils-de-la-cnil-pour-un-bon-mot-de-passe) les conseils de la CNIL à ce sujet.
2. Comment vérifier la fiabilité de votre mot de passe ? 
3. Quels sont les dangers d'avoir un mot de passe faible ? 
4. Quelles conséquences peut avoir le piratage de vos données personnelles (documents, photos, boite mail ...) ? 
4. Pourquoi est-il si important d'utiliser un mot de passe robuste pour accéder à un compte sur un réseau social ? 

## Sujet 7 : Le cyber-harcèlement

1. Qu'est-ce que c'est ?
2. En quoi les réseaux sociaux peuvent faciliter, amplifier et aggraver les attitudes de harcèlement ?
3. Quelles conséquences peut avoir un cyber-harcèlement ? Qui en porte la responsabilité ? Quelles sont les peines encourrues ?
4. Que faire si on s'aperçoit d'un phénomène de cyber-harcèlement ? Comment aider un harcelé ?



___

Par Mieszczak Christophe 

licence CC BY SA
# Introduction



## Repères historiques

La notion de «réseau social» apparaît bien avant les réseaux sociaux sur internet; elle est étudiée **à la fin du 19ème siècle** en sciences sociales, puis à nouveau dans le courant du vingtième siècle. Un réseau social est un ensemble de liens entre individus, cet ensemble constituant une **communauté** qui partage des convictions ou des valeurs. L'analyse des réseaux sociaux utilise des graphes pour représenter
un réseau. Le terme de «réseau social» désigne également des applications web qui offrent de mettre en relation des internautes afin de discuter de leurs centres d'intérêts.

![les réseaux - la revue des médias - CCA](https://larevuedesmedias.ina.fr/sites/default/files/styles/landscape_1600x/public/reseaux_sociaux.jpg?itok=mNky7DEP)



Quelques repères historiques :

![historique 1](media/historique.png)



Une petite frise :

![frise](media/historique_anglais.jpeg)  





Le développement des réseaux sociaux introduit **un nouveau type de liens sur le Web**, qui ne relève pas de l’hypertexte : il s’agit de l’abonnement à des relations/des amis et de la possibilité de recommander de l’information en fonction du réseau ainsi constitué..





# Références

- [MOOC SNT](https://www.fun-mooc.fr/courses/course-v1:inria+41018+session01)
- [Conférence Class'Code Pays de Loire -- Université de Nantes](https://www.youtube/com/watch?v=TaMnLOcpYEw)
- Au coeur des réseaux : des sciences aux citoyens, *Fabien Tarissan*, Éditions Le Pommier, 2019
- Initiation à la théorie des graphes, *Christiant Roux*, Ellipses, 2009
- [Les réseaux sociaux aujourd'hui](https://www.ofce.sciences-po.fr/pdf/revue/7-126.pdf), *Michel Forsé*, Revue de l'OFCE, Presses de Sciences-Po, 126, p.1-15, 2012

Documents élaborés par la formation SNT de l'académie de Lille.

_________

Par Mieszczak Christophe

Publié sous licence CC-BY-SA
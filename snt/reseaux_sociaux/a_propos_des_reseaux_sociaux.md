# Les réseaux sociaux

![les réseaux](https://cdn.pixabay.com/photo/2016/11/27/19/03/social-networks-1863613_960_720.png)

## Principe de base 



Le principe d'un réseau social est de mettre en relation des individus. Lorsqu'il est informatique, le réseau, en utilisant un outils technologique moderne et efficace, accentue cette facilité de mise en relation, l'échange d'informations sous diverses formes  et crée ainsi des communautés virtuelles.



**Par définition, un réseau social DOIT enregistrer des données sur ses utilisateurs et permettre de les diffuser **.



Ces réseaux sont un **formidable outils de communications** :

* Ils permettent un nouveau mode d'expression de sa personnalité, de ses passions.
* Ils permettent de créés des liens par affinités entre des individus qui ne se seraient jamais rencontrés sinon et crée ainsi une forme d'ouverture au monde.
* Ils permettent de garder facilement le contact avec des amis ou des membres de la famille.
* Ils permettent de présenter et promouvoir efficacement  une association, une entreprise.



Pour toutes ces raisons, s'en passer est difficile. 



Cependant, il faut avoir conscience des risques et des dérives de ces pratiques pour ne pas en devenir une victime... ou un bourreau.





## Un peu de droit



### Les usages informatiques font évoluer le droit.

![cyber voleur](http://www.pngall.com/wp-content/uploads/2016/04/Web-Security-Download-PNG.png)





Qu'est-ce- que voler ? Autrefois, voler, c'était _soustraire le bien d'autrui pour son utilisation personnelle_.



Puis arrive internet et le web avec la possibilité d'échanger des données... avec ou sans l'accord du _propriétaire_ de ces données :

- Est-il légal d'aller télécharger les plans d'une invention sans l'accord de l'inventeur ?
- Est-il légale de télécharger des photos, des vidéos ou de la musique ?



Si on reprend la définition juridique du _vol d'autrefois_ ce n'est pas du vol : celui qui télécharge ne substitue pas le bien d'autrui puisque ce dernier conserve sa propriété...



Ce n'est ici qu'un exemple mais les usages informatiques poussent les juristes à repenser les textes pour combler des vides juridiques dans lesquels les fraudeurs tentent de s'engouffrer.





### Domaine privé et public.

![privé](https://cdn.pixabay.com/photo/2013/04/18/05/39/shield-105499_960_720.jpg)

- Quelle est la différence entre le domaine publique et le domaine privé ?

  - Le domaine du privé s'applique lorsque les personnes présentes sont en nombre limité et son liées par un intérêt commun.  La famille, un cercle restreint d'amis, un petit groupe de travail font donc partis du domaine privé. Reste à interpréter ce qu'on entend part _petit_ groupe ...
  - Le reste est du domaine publique.



* **A) L’article 8 de la CEHD (Cour Européenne des Droit de l'Homme)** envisage la protection à la vie privée et familiale : _"  Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance._ 

	

- Les conséquences juridiques ne sont pas les même selon le domaine :

	- Une insulte faite dans le domaine privée entraîne une peine allant de **40€ à 1500€** selon les circonstances aggravantes (insultes raciales, sexistes, insultes envers des personnes ayant un statut particulier ....) .
	-  Si elle est faite dans un cadre publique, les peines encourues vont de **12000€ à 45000€ d'amende avec un risque d'emprisonnement pouvant aller jusque  un an**.

	

	>  Sur un réseau social, un message posté uniquement visible par ses amis ,s'ils sont en petit nombre car la notion d'_ami_ sur un réseau social est souvent très discutable, reste du domaine privé. Un message visible par tout le monde ou destiné à un très grand nombre d'_amis_ sera du domaine public : **ATTENTION A CE QUE VOUS POSTEZ !!**

	

- Les principales atteintes à la vie privée sont :

	- L'intrusion dans la sphère intime.
- La publication d'éléments privé.
	
	- La diffamation ou la vérité tronquée.
- L'usurpation d'identité.



### Persistance des données



Lorsque vous publiez des données sur un réseau social, vous laissez une trace qui mettra des années à s'effacer, si jamais elle s'efface.



* Nous avons déjà vu pus haut que sur certains (la plupart) réseaux, publier accorde les droits de la publication au réseau : ce que vous avez posté ne vous _appartient_ plus. Difficile, très difficile, de revenir en arrière. 
* Une photo peut être partagée de très nombreuses fois, ces partages faisant perdre le contrôle de la publication : impossible alors d'effacer toute trace de cette photo.
* Ce que vous publiez, même sur un groupe privé, sera stocké sur un serveur, quelque part dans le monde. Si vous supprimer cette publication, elle restera des années encore sur ce serveur.



Le  [droit à l'oubli](https://fr.wikipedia.org/wiki/Droit_%C3%A0_l%27oubli) est un concept discuté et mis en pratique dans l'union européenne. Il permet un individu de demander le retrait du web de certaines informations. En 2010, une charte a été crée : tous ses signataires s'engagent à proposer un moyen afin de pouvoir supprimer toute donnée personnelle. Ni Facebook ni Google ne l'ont signé ...

Il faudra attendre 2018 et le RGPD (voir ci-dessous) pour que le droit à l'oubli soit écrit dans un texte de loi.



### Règles Générales Protection des Données

Depuis 2018, [le règlement général sur la protection des données (RGPD)](https://www.vie-publique.fr/actualite/dossier/securite-internet/reglement-general-protection-donnees-quoi-s-agit-il.html) responsabilise les organismes publics et privés qui traitent les données des citoyens européens. Que la société soit européenne ou non. 



Les grands principes du RGPD sont :
• **le consentement** : le site doit demander l’autorisation d’enregistrer vos données.
• **la transparence** : l’entreprise doit expliquer ce qu’elle fait des données enregistrées.
• **le droit des personnes** : les utilisateurs doivent pouvoir récupérer leurs données pour les porter sur un autre site, doivent disposer d’un **droit à l’oubli** …
• **la responsabilité** : les sociétés doivent nommer un responsable des données, signaler aux utilisateurs les fuites d’informations personnelles et doivent concevoir des systèmes de traitement protégeant les données de part la conception même du système.



Avant le RGPD, la [CNIL](https://www.cnil.fr/) était déjà en charge du problème mais avec des moyens bien limités tant du point de vue du personnel que des possibilités de répression en cas de fraude.  La grosse différence aujourd'hui, avec la RGPD, c'est le montant à payer en cas de fraude avérée. 

* [En 2016, la CNIL condamne google](https://www.lefigaro.fr/flash-eco/2016/03/24/97002-20160324FILWWW00272-la-cnil-condamne-google-a-payer-100-000.php)
* [En 2019, la CNIL condamne google](https://www.lemonde.fr/pixels/article/2019/01/21/donnees-personnelles-la-cnil-condamne-google-a-une-amende-record-de-50-millions-d-euros_5412337_4408996.html)



> En 2019, le chiffre d'affaire de Facebook  s'élève à environ [330€... par seconde.](https://www.courrierinternational.com/article/2014/10/30/combien-gagne-facebook-en-une-seconde)
>
> * Combien gagne-t-il en une minute, en un mois, en une année ?
> * Que représente une amende de 100000€ pour Facebook ?
> * Aujourd'hui, l'amende peut atteindre 10% du chiffre d'affaire annuel .... Ça pique !



### Harcèlement en ligne



- Les réseaux sociaux peuvent être le support d’un harcèlement numérique, par le biais de  photographies partagées sans consentement ou impossibles à retirer, par la diffusion de fausses nouvelles, de dénonciations ou de calomnies. Des pratiques, des outils et des services permettent de se protéger, lutter et dénoncer de tels agissements.

	- Article 222-33-2-2 du code pénal [Légifrance](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000029336939&cidTexte=LEGITEXT000006070719&dateTexte=20140806) : _Le fait de harceler autrui par des propos ou comportements répétés ayant pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel, est puni de **deux ans d'emprisonnement et de 30 000 € d'amende**._
	- Le site [Non au harcèlement](https://www.nonauharcelement.education.gouv.fr/) offre des ressources permettant de sensibiliser au harcèlement et fournit des fiches indiquant comment réagir aux situations de harcèlement (victimes, témoins, parents et personnels concernés).

	![harcèlement](https://cdn.pixabay.com/photo/2020/11/25/22/23/computer-5777377_960_720.png)

### Droits d'auteurs et propriété intellectuelle



Qu'est-ce que cela a à voir ici ? 



* Vous avez bien compris déjà que, lorsqu'on publie quelque chose sur un réseau social,  il faut prendre en garde de ne pas enfreindre la sphère privé d'une tierce personne et encore moins de la diffamer ou de la harceler. 

* Il faut également prendre garde à ne pas diffuser des œuvres sans en avoir le droit ! L'auteur de ces œuvres ou l'organisme qui en possède les droits de diffusion, pourraient se retourner contre vous !

* Avez vous lu les conditions d'utilisation de votre réseau social ? Qu'a-t-il le droit de faire avec ce que vous y postez ? Avec vos photos ou vos créations ? La plupart du temps, vous perdez les droits de ce que vous publiez. A ce sujet, Voilà ce que l'on peut lire si l'on prend le temps nécessaire sur les conditions d'utilisation d'un compte Facebook :

	

	**"Le contenu et les informations que vous publiez sur  Facebook vous appartiennent, et vous pouvez contrôler la façon dont nous partageons votre contenu grâce aux paramètres de confidentialité et  des applications. (...).** 

	**Pour le contenu protégé par des droits de propriété intellectuelle**, comme les photos ou les vidéos (contenu de propriété intellectuelle), **vous nous donnez expressément la permission suivante**, conformément à vos paramètres de confidentialité et des applications : **vous nous accordez une licence non exclusive, transférable,  sous-licenciable, sans redevance et mondiale pour l’utilisation des  contenus de propriété intellectuelle que vous publiez sur Facebook ou en relation avec Facebook** (licence de propriété intellectuelle).  Cette licence de propriété intellectuelle se termine lorsque vous  supprimez vos contenus de propriété intellectuelle ou votre compte, sauf si votre contenu a été partagé avec d’autres personnes qui ne l’ont pas supprimé.  (...)

	

	> **Lorsque vous publiez  du contenu ou des informations avec le paramètre Public, cela signifie  que vous permettez à tout le monde, y compris aux personnes qui  n’utilisent pas Facebook, d’accéder à ces informations et de les  utiliser, mais aussi de les associer à vous (c’est-à-dire votre nom et votre photo de profil).**









## Identité numérique et e-réputation



* **L'[identité numérique](https://fr.wikipedia.org/wiki/Identit%C3%A9_num%C3%A9rique) d'une personne physique est composée de toutes les informations présentes à son sujet sur Internet**. L'identité est utile pour soi et également pour les autres. Elle permet d'une part de dire qui l'on est, et d'autre part aux autres de savoir qui l'on est. Il y a une différence notable entre ces deux notions, car l'identité peut être perçue de différentes manières. 

![identité numérique - source Flickr - CC](https://live.staticflickr.com/108/276533601_b0bfdd0135_z.jpg)





* La _confiance_ que notre entourage nous accorde, se mesure par ce que l'on appelle **la réputation**. 

	* Le mot "*réputation*" vient du latin _reputatio_ qui signifie _évaluation_. **La réputation est donc une évaluation sociale. ** 
	* Pour décrire notre perception d'une personne, on s'exprime le plus souvent par des **opinions**. Or une opinion est un jugement de valeur qui ne repose pas forcément sur des faits ou des données objectives, observables et quantifiables. Parfois elle repose sur des faits observables et indiscutables. Parfois  il s’agit de rumeurs, de préjugés, voire de mensonges visant à détruire  un rival.  **Cette réputation peut donc être juste ou injuste**.
	* La [e-réputation](https://fr.wikipedia.org/wiki/E-r%C3%A9putation) est un terme qui n'a qu'une quinzaine d'année d'existence. Elle est apparue en même temps que les réseaux sociaux, dans les années 2000.
	* Aujourd'hui, quand une personne fait un  mauvais compte rendu sur une autre personne ou une entreprise, son commentaire  est non seulement figé pour l’éternité, mais il est accessible partout  sur la planète pour n’importe qui. D’une certaine manière, on peut dire  que notre réputation est maintenant mondiale et publique, qu’on le  veuille ou non. 
	* Peut-on y échapper ? Certains pensent qu'en ignorant  les nouvelles technologies, en évitant les réseaux sociaux entre autres, ils évitent d'avoir cette e-réputation. Mais , en réalité, ils ne peuvent empêcher d'autres internautes de parler d'eux et ignorent ce qu'ils peuvent bien dire !  Il vaut mieux gérer sa réputation sur Internet plutôt que de laisser quelqu’un d’autre s’en occuper à notre place. Nous devons prendre notre destin numérique en main sinon il se fera à notre insu ou plutôt à  l’insu de notre plein gré ! 

	

> Trouvez des exemples de persistance de l'information sur les réseaux sociaux influençant une réputation.
>
> * Ancien tweet ressortis plusieurs années après.
>
> * Photographie compromettantes prises il y a longtemps ...
>
> 	
>
> Trouvez des exemples où une e-réputation est importante :
>
> * Vente d'occasion ou pas ➔ réputation du vendeur
>
> * Location de vacances ( Airbnb ...) ➔ évaluation locataire / logeur
>
> * Restauration ➔ évaluation 
>
> * entretient d'embauche
>
> 	
>
> [Trouvez des outils pour surveiller sa e-réputation](https://www.codeur.com/blog/outils-e-reputation/) :
>
> * Google Alert (mais c'est Google...)
> * [web mii](https://webmii.com/?language=fr)
> * etc ...
>
> 



> Discussion : que faire pour se bâtir une bonne e-réputation ?
>







## Modèle économique et données privées

Les modèles économiques des applications de réseautage social sont symbolisés par le slogan **« quand c’est gratuit, c’est vous le produit »**.



Comment pouvez vous être _le produit_ ? Grâce à vos données personnelles : Elles permettent de cibler vos attentes et son chèrement recherchées.

* [Les données personnelles](https://fr.wikipedia.org/wiki/Données_personnelles), leur exploitation et leur commercialisation, sont devenues au fil des  années un marché considérable : le marché des données personnelles a une valeur estimée à **300 millions de dollars**. 

* De nombreuses entreprises ont élaboré les premiers modèles fondés sur l’analyse et/ou la revente des  données personnelles pour développer leur service. L’économie des  données personnelles repose en particulier sur l’évolution de deux  modèles :

	- Les modèles bifaces qui se sont construits sur une logique de troc implicite : données personnelles contre service gratuit.
	- Les modèles de services pour lesquels les données personnelles sont  un véritable carburant : grâce à la personnalisation, les services sont  plus efficaces et pertinents pour l’utilisateur et bénéficient tant à  l’entreprise qu’au client (dans la limite des règles de consentement).

	

* Le sujet des données personnelles fait l’objet de nombreux débats  animés opposant souvent deux conceptions idéologiques et culturelles :  celle libérale, d’influence anglo-saxonne et celle protectionniste,  d’influence européenne. Autrement dit, la donnée personnelle est un  véritable or noir pour l’économie numérique mais constitue également un  risque majeur pour [les libertés individuelles](https://fr.wikipedia.org/wiki/Libertés_fondamentales), ce qui impacte la manière d’innover. 



> Activité :
>
> * Comment peut-on utiliser vos données personnelles pour gagner de l'argent :
> 	* vos centres d'intérêts    ➔  publicité ciblée, meilleur connaissance des goût du public pour les entreprises afin de faciliter la création de nouveau produit 'tendance', 
> 	* géolocalisation    ➔  publicité ciblée, meilleure connaissance de vos centre d'intérêt
> 	* photographie publiée   ➔ droit de la photographie cédée ? réutilisation de la photographie dans la publicité
> 	* utilisation du micro des smartphone  ➔  [Facebook a démenti se servir des micros des smartphones](https://www.phonandroid.com/facebook-capable-espionner-grace-micro-smartphone.html)  pour espionner les gens, _pour l'instant_.  Cependant, lorsque vous installez Facebook sur votre portable, il en demande l'accès et il faut accepter de lui donner pour que l'installation se poursuive. Pour lui interdire d'utiliser le micro, il faut ensuite retourner dans les paramètre du smartphone, dans le menu _applications_ puis _autorisations_ pour en désactiver l'accès.





* Enfin, la dernière grande source de revenus concerne les accès  « premium » payants : 236 millions de dollars de recettes en 2011 pour  l’ensemble des sites communautaires. Les cas les plus connus en France  sont Viadeo ou Linkedin, les réseaux professionnels de référence. IL en existe d'autres comme le réseau des ''sportif'', Strava.



## Données et web dynamique



* Autrefois le web était statique :
    1. Un client demandait une ressource à un serveur
    2. Le serveur cherchait si la ressource était disponible
    3. Le serveur renvoyait la ressource telle quelle au client : tout client recevait la même page.



* Aujourd'hui il est dynamique :
    1. Un client demandait une ressource à un serveur
    2. Le serveur analyse la requête grâce à des algorithme utilisant des bases de données.
    3. Le _statut_ du client et les droits régissant ces données décident de ce que le client peut voir ou non
    4. une page web est générée dynamiquement à partir des données que le serveur peut utiliser.
    5. Le serveur renvoie au client une page web créée spécialement pour lui.



> Il est possible de gérer les droits d'accès à vos données :
>
> * Qui peut les voir ?
> * Que peut-on en faire ?



## Algorithmes de recommandation et bulle sociétale

Toutes les applications de réseaux sociaux mettent également en œuvre un autre type d'algorithme : les algorithmes de recommandation qui, à partir des traces laissées par l'internaute membre du réseau (toutes les opérations qu'il a effectuées) lui proposent d'entrer en relation avec d'autres membres, de consulter des contenus mais affichent également des publicités.

Ces algorithmes fonctionnent de manière implicite : ils observent (et gardent la trace) de tous les contenus et autres membres du réseau que l'internaute membre a regardé (et à quelle fréquence) et analysent la structure de son «réseau» (les membres auxquels il est relié et ses centres d'intérêts). Le but est de découvrir ses goûts de manière à le mettre en relation avec d'autres membres qui les partagent et également de lui proposer des publicités susceptibles de l'intéresser. Ce dernier aspect est souvent au centre du modèle économique de l'application.

Ces algorithmes contribuent à enfermer les membres dans des «petits mondes» en leur proposant des contenus sur des sujets qu'ils ont déjà consultés et des contacts qui partagent les mêmes centres d'intérêt. Les réseaux sociaux ont donc ce défaut : au lieu d'ouvrir des nouveaux centres d'intérêt et de nouveaux horizons à leurs membres, ils ont tendance à les faire évoluer dans un monde finalement assez clos. 



[Voici, par exemple, un article sur ce phénomène qui aurait eu une incidence sur l'élection de Donald Trump](https://www.konbini.com/fr/tendances-2/filter-bubble-reseaux-sociaux-ont-louper-trump/).



Ce phénomène  peu parfois être à l'origine de comportement radicalisé engendrant haine et violences : quand on ne rencontre que des personnes qui ont exactement les mêmes opinions que nous, qu'on ne nous propose que des contenus qui tendent à renforcer nos convictions, on peut avoir l'impression, à tort, que notre opinion est la seule valable, que tout le monde, ou presque, la partage et qu'il n'y a aucune autre voie possible ou acceptable ... Attention, un réseau social n'est pas le reflet exact de la société et une opinion n'est qu'une opinion, pas la vérité absolue. 



## Références

- [MOOC SNT](https://www.fun-mooc.fr/courses/course-v1:inria+41018+session01)
- Wikipédia
- [Conférence Class'Code Pays de Loire -- Université de Nantes](https://www.youtube/com/watch?v=TaMnLOcpYEw)
- Au coeur des réseaux : des sciences aux citoyens, *Fabien Tarissan*, Éditions Le Pommier, 2019
- Initiation à la théorie des graphes, *Christiant Roux*, Ellipses, 2009
- [Les réseaux sociaux aujourd'hui](https://www.ofce.sciences-po.fr/pdf/revue/7-126.pdf), *Michel Forsé*, Revue de l'OFCE, Presses de Sciences-Po, 126, p.1-15, 2012

Documents élaborés par la formation SNT de l'académie de Lille.

_________

Par Mieszczak Christophe			Publié sous licence CC-BY-SA

sources images :

* [réseaux sociaux - pixabay](https://pixabay.com/fr/illustrations/r%C3%A9seaux-sociaux-ic%C3%B4nes-twitter-1863613/)
* [Cyber voleur, pngwall CC 4.0 BY-NC](http://www.pngall.com/web-security-png/download/2631)
* [Harcèlement - pixabay](https://pixabay.com/fr/illustrations/ordinateur-harc%C3%A8lement-sur-internet-5777377/)
* [privé - pixabay](https://pixabay.com/fr/illustrations/bouclier-priv%C3%A9-public-internet-www-105499/)
#  Le monde est petit

## Six degrés de séparation ?


L'expérience Milgram (Milgram, 1967 ;  Travers et Milgram, 1969) pas celle qui électrocute, celle des courriers qui doivent parvenir à un courtier par des échanges directs de la main à la main. Le courrier arrivera en quatre jours mais seulement 64 chaînes ne se sont pas rompues et en moyenne, il y avait 5,2 intermédiaires.

De cette expérience, il tire la théorie des **Six degrés de séparation** : tous les individus de la planète sont les sommets d'un graphe dont le diamètre est de longueur 6.



![Milgram - wikipédia - domaine public](https://upload.wikimedia.org/wikipedia/commons/8/88/Six_degrees_of_separation_01.png)



Les réseaux sociaux permettent de réduire cette chaîne à moins de 5 ([Facebook moins de 4](https://www.nouvelobs.com/rue89/rue89-sur-les-reseaux/20160205.RUE2124/3-5-degres-de-separation-si-vous-etes-sur-facebook.html))  Si l'on vit dans le même pays, la France par exemple, cette chaîne tombe à 3.

Cette notion a fait l'objet d'un film [*Six degré de séparation* par Fred Schepisi.](http://www.allocine.fr/video/player_gen_cmedia=19440971&cfilm=12754.html)

On peut voir la [vidéo du mooc SNT](https://www.youtube.com/watch?v=nn1mIqW9oYQ) qui illustre une cette notion.



> * Considérons 'ensemble des enseignants et des élèves du lycée. A quel _distance_ pensez-vous être d'un élève quelconque que vous ne connaissez pas du tout ?
> * A quelle distance pensez vous être du ministre de l'éducation ? Du président de la république ? Du président des états-unis d'Amérique ?



Oui mais : 

- En réalité, en ce qui concerne les réseaux sociaux, le lien entre deux individus ne symbolise pas nécessairement une «vrai» relation. Cela peut-être un membre du même club sportif ou de la même école avec laquelle ou lequel on a jamais parlé...  

    

- Lady Gaga, Donald Trump ou Barack Obama sont des personnalités très connues. Plus elles sont connues, plus il y a de chances que vous connaissiez quelqu'un qui les connaît. Ce qui réduit le nombre d'intermédiaires. Mais ce n'est pas le cas de Gérard, ermite au fin fond de la Lozère : Il vous faudra bien plus de relations pour arriver jusqu'à lui.





## The oracle of Bacon

Un _jeu_ a été créé en 1994 par trois étudiants de l'Albright College, Craig Fass, Brian Turtle et Mike Ginelli. Ils se sont amusé à trouver des liens entre un acteur et Kevin Bacon : un acteur est en relation avec Kevin Bacon s'ils ont tourné un film ensemble (analogie avec le nombre de Ërdos en mathématiques). Ainsi, un acteur avec un nombre de Bacon de 3, est un acteur qui a joué avec un autre qui a joué avec un autre acteur qui a joué avec Kevin Bacon. Le jeu est devenu un site (et même une application tablette ou téléphone) : [The Oracle of Bacon](https://oracleofbacon.org).

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Kevin_Bacon_SDCC_2014.jpg/431px-Kevin_Bacon_SDCC_2014.jpg" alt="kevin bacon - source wikipédia - creative commons" style="zoom:50%;" />

L'objectif est d'illustrer les 6 degrés de séparation et de montrer la vitesse à laquelle un tel graphe peut se densifier

> * Sur le site [The Oracle of Bacon](https://oracleofbacon.org/help.php), tapez un nom d'acteur et collecter les degrés de séparations btenus. 
> * Essayez de trouver un acteur le plus éloigné possible de Kévin Bacon.
> * Essayez de trouver deux acteurs les plus éloignés possibles



## Les Wikiraces

L'objectif est de partir d'une page quelconque pour arriver à une autre page apparemment sans rapport en cliquant sur le moins de liens possibles. 







___

Par Mieszczak Christophe, à partir des documents de la formation académique SNT 

 Licence CC- BY - SA

source images :

* [Six degré de séparation, wikipédia CC BY SA](https://commons.wikimedia.org/wiki/File:Six_degrees_of_separation.svg)
* [Kevin Bacon, wikipédia, CC BY SA](https://commons.wikimedia.org/wiki/File:Kevin_Bacon_SDCC_2014.jpg)
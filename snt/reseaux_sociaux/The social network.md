# The social network



## Introduction



* Autour du personnage central:
    * Quel est son nom  ? 
    * Quel domaine étudie-t-il  ? 
    * Pourquoi tient-il plus que tout à intégrer un _final club_ ? 
    * Pour quelle raison la demoiselle avec qui il discute le quitte-t-il ?
    * Relevez une phrase marquante qu'il prononce en rapport avec ce qu'il va créer.

* Quel langage informatique utilise-t-il lorsqu'il publie sur son post ?
* Comment se venge-t-il de son ex petite ami ? 



## Vie privée

* Comment  _Mark Zuckerberg_  constitue-t-il sa première base de donnée de photos ?
* Quel est le principe de son application de _Face Smash_ ? Rencontre-elle le succès ?
* Comment se défend-t-il devant les accusations de piratage et d'utilisation de données privées ?
* Pourquoi _Mark Zuckerberg_ a-t-il inclu dans les fonctionnalités des critères de confidentialités dans _The Facebook_ ?



## Procès



* Quel est le principe de _Harvard connection_ pour lequel _Mark Zuckerberg_ est embauché ?

* Quelles sont les idées de base du projet que Marck propose  à son ami lors de la fête des caraïbes ?

* De quoi _Mark Zuckerberg_ est-il accusé par les inventeurs de _Harvard connection_ ?

* Que gagne au procès les fondateurs de _Harvard connection_ ?

* Qu'obtient Eduardo Saverin ?

    



## Déploiement



* Où _The Facebook_ est-il déployé au tout début ?

* Dans quels endroits particuliers le déploiement se poursuit-il aux états-unis?

* Citer une technique utilisée pour forcer la domination de _FaceBook_ dans les universités où un réseau semblable existait déjà.

* Sean Parker :

    * Qui est-il ?
    *  Quel est son rôle dans le déploiement de _The Facebook_ ?
    * Qu'est-ce qui précipite sa chute ?

    

    



## Modèle économique

* Pourquoi ne pas avoir monétiser _The Facebook_ tout de suite ?

* Quel est le montant du premier gros investissement dans la société _Facebook_ ?

* Quelle conséquence a la signature des papiers concerant la repartition des actions pour Eduardo Saverin ?

    

    

    

    




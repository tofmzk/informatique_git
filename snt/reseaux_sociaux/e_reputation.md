# Identité numérique et e-reputation



**L'[identité numérique](https://fr.wikipedia.org/wiki/Identit%C3%A9_num%C3%A9rique) d'une personne physique est composée de toutes les informations présentes à son sujet sur Internet**. L'identité est utile pour soi et également pour les autres. Elle permet d'une part de dire qui l'on est, et d'autre part aux autres de savoir qui l'on est. Il y a une différence notable entre ces deux notions, car l'identité peut être perçue de différentes manières. 

![identité numérique - source Flickr - CC](https://live.staticflickr.com/108/276533601_b0bfdd0135_z.jpg)





La _confiance_ que notre entourage nous accorde, se mesure par ce que l'on appelle **la réputation**. 

* Le mot "*réputation*" vient du latin _reputatio_ qui signifie _évaluation_. **La réputation est donc une évaluation sociale. ** 
* Pour décrire notre perception d'une personne, on s'exprime le plus souvent par des **opinions**. Or une opinion est un jugement de valeur qui ne repose pas forcément sur des faits ou des données objectives, observables et quantifiables. Parfois elle repose sur des faits observables et indiscutables. Parfois  il s’agit de rumeurs, de préjugés, voire de mensonges visant à détruire  un rival.  **Cette réputation peut donc être juste ou injuste**.
* La [e-réputation](https://fr.wikipedia.org/wiki/E-r%C3%A9putation) est un terme qui n'a qu'une vingtaine d'années d'existence. Elle est apparue en même temps que les réseaux sociaux, dans les années 2000.
* Aujourd'hui, quand une personne fait un  mauvais compte rendu sur une autre personne ou une entreprise, son commentaire  est non seulement figé pour l’éternité, mais il est accessible partout  sur la planète pour n’importe qui. D’une certaine manière, on peut dire  que notre réputation est maintenant mondiale et publique, qu’on le  veuille ou non. 
* Peut-on y échapper ? Certains pensent qu'en ignorant  les nouvelles technologies, en évitant les réseaux sociaux entre autres, ils évitent d'avoir cette e-réputation. Mais , en réalité, ils ne peuvent empêcher d'autres internautes de parler d'eux et ignorent ce qu'ils peuvent bien dire !  Il vaut mieux gérer sa réputation sur Internet plutôt que de laisser quelqu’un d’autre s’en occuper à notre place. Nous devons prendre notre destin numérique en main sinon il se fera à notre insu ou plutôt à  l’insu de notre plein gré ! 



> * Trouvez des exemples de persistance de l'information sur les réseaux sociaux influençant une réputation.
>
> 
>
> * Trouvez des exemples où une e-réputation est importante :
>
> 
>
> * [Trouvez des outils pour surveiller sa e-réputation](https://www.codeur.com/blog/outils-e-reputation/) 
>
> 



> * Discussion : que faire pour se bâtir une bonne e-réputation ? 







_______

Par Mieszczak Christophe

Licence CC BY SA
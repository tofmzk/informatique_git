# Un peu de droit

### Les usages informatiques font évoluer le droit.

![cyber voleur](http://www.pngall.com/wp-content/uploads/2016/04/Web-Security-Download-PNG.png)





Qu'est-ce- que voler ? Autrefois, voler, c'était _soustraire le bien d'autrui pour son utilisation personnelle_.



Puis arrive internet et le web avec la possibilité d'échanger des données... avec ou sans l'accord du _propriétaire_ de ces données :

- Est-il légal d'aller télécharger les plans d'une invention sans l'accord de l'inventeur ?
- Est-il légale de télécharger des photos, des vidéos ou de la musique ?



Si on reprend la définition juridique du _vol d'autrefois_ ce n'est pas du vol : celui qui télécharge ne substitue pas le bien d'autrui puisque ce dernier conserve sa propriété...



Ce n'est ici qu'un exemple mais les usages informatiques poussent les juristes à repenser les textes pour combler des vides juridiques dans lesquels les fraudeurs tentent de s'engouffrer.



### Qui est responsable d'une publication ?



Le réseau social ? l'auteur de la publication ? quelqu'un d'autre ?



* Les réseaux sociaux ne sont jamais responsables de ce qui est publié par les internautes. Ils se placent comme des herbergeurs qui offrent un espace _libre_ de publication. Ils n'ont qu'une seule obligation : réagir lorsqu'une publication est signalée par les membres du réseau comme étant inapropriée (racisme, insultes, harcellement, fakenews ....)
* L'auteur de la publication est responsable de ce qu'il publie ... s'il a la majorié numérque, c'est à dire 15 ans. Il peut publié dès 13 ans, sous la responsabilité de ses responsables légaux qui sont alors responsables (ça fait beaucoup de responsable dans la même phrase, oups) de tout ce qu'il peut publier.





### Domaine privé et public.

![privé](https://cdn.pixabay.com/photo/2013/04/18/05/39/shield-105499_960_720.jpg)

- Quelle est la différence entre le domaine publique et le domaine privé ?

  - Le domaine du privé s'applique lorsque les personnes présentes sont en nombre limité et son liées par un intérêt commun.  La famille, un cercle restreint d'amis, un petit groupe de travail font donc partis du domaine privé. Reste à interpréter ce qu'on entend part _petit_ groupe ...
  - Le reste est du domaine publique.



* **A) L’article 8 de la CEHD (Cour Européenne des Droit de l'Homme)** envisage la protection à la vie privée et familiale : _"  Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance._ 

	

- Les conséquences juridiques ne sont pas les même selon le domaine :

	- Une insulte faite dans le domaine privée entraîne une peine allant de **40€ à 1500€** selon les circonstances aggravantes (insultes raciales, sexistes, insultes envers des personnes ayant un statut particulier ....) .
	-  Si elle est faite dans un cadre publique, les peines encourues vont de **12000€ à 45000€ d'amende avec un risque d'emprisonnement pouvant aller jusque  un an**.

	

	>  Sur un réseau social, un message posté uniquement visible par ses amis ,s'ils sont en petit nombre car la notion d'_ami_ sur un réseau social est souvent très discutable, reste du domaine privé. Un message visible par tout le monde ou destiné à un très grand nombre d'_amis_ sera du domaine public : **ATTENTION A CE QUE VOUS POSTEZ !!**

	

- Les principales atteintes à la vie privée sont :

	- L'intrusion dans la sphère intime.
- La publication d'éléments privé.
	
	- La diffamation ou la vérité tronquée.
- L'usurpation d'identité.



### Persistance des données



Lorsque vous publiez des données sur un réseau social, vous laissez une trace qui mettra des années à s'effacer, si jamais elle s'efface.



* Nous avons déjà vu pus haut que sur certains (la plupart) réseaux, publier accorde les droits de la publication au réseau : ce que vous avez posté ne vous _appartient_ plus. Difficile, très difficile, de revenir en arrière. 
* Une photo peut être partagée de très nombreuses fois, ces partages faisant perdre le contrôle de la publication : impossible alors d'effacer toute trace de cette photo.
* Ce que vous publiez, même sur un groupe privé, sera stocké sur un serveur, quelque part dans le monde. Si vous supprimer cette publication, elle restera des années encore sur ce serveur.



Le  [droit à l'oubli](https://fr.wikipedia.org/wiki/Droit_%C3%A0_l%27oubli) est un concept discuté et mis en pratique dans l'union européenne. Il permet un individu de demander le retrait du web de certaines informations. En 2010, une charte a été crée : tous ses signataires s'engagent à proposer un moyen afin de pouvoir supprimer toute donnée personnelle. Ni Facebook ni Google ne l'ont signé ...

Il faudra attendre 2018 et le RGPD (voir ci-dessous) pour que le droit à l'oubli soit écrit dans un texte de loi.



### Règles Générales Protection des Données

Depuis 2018, [le règlement général sur la protection des données (RGPD)](https://www.vie-publique.fr/actualite/dossier/securite-internet/reglement-general-protection-donnees-quoi-s-agit-il.html) responsabilise les organismes publics et privés qui traitent les données des citoyens européens. Que la société soit européenne ou non. 



Les grands principes du RGPD sont :
• **le consentement** : le site doit demander l’autorisation d’enregistrer vos données.
• **la transparence** : l’entreprise doit expliquer ce qu’elle fait des données enregistrées.
• **le droit des personnes** : les utilisateurs doivent pouvoir récupérer leurs données pour les porter sur un autre site, doivent disposer d’un **droit à l’oubli** …
• **la responsabilité** : les sociétés doivent nommer un responsable des données, signaler aux utilisateurs les fuites d’informations personnelles et doivent concevoir des systèmes de traitement protégeant les données de part la conception même du système.



Avant le RGPD, la [CNIL](https://www.cnil.fr/) était déjà en charge du problème mais avec des moyens bien limités tant du point de vue du personnel que des possibilités de répression en cas de fraude.  La grosse différence aujourd'hui, avec la RGPD, c'est le montant à payer en cas de fraude avérée. 

* [En 2016, la CNIL condamne google](https://www.lefigaro.fr/flash-eco/2016/03/24/97002-20160324FILWWW00272-la-cnil-condamne-google-a-payer-100-000.php)
* [En 2019, la CNIL condamne google](https://www.lemonde.fr/pixels/article/2019/01/21/donnees-personnelles-la-cnil-condamne-google-a-une-amende-record-de-50-millions-d-euros_5412337_4408996.html)



> En 2019, le chiffre d'affaire de Facebook  s'élève à environ [330€... par seconde.](https://www.courrierinternational.com/article/2014/10/30/combien-gagne-facebook-en-une-seconde)
>
> * Combien gagne-t-il en une minute, en un mois, en une année ?
> * Que représente une amende de 100000€ pour Facebook ?
> * Aujourd'hui, l'amende peut atteindre 10% du chiffre d'affaire annuel .... Ça pique !



### Harcèlement en ligne



- Les réseaux sociaux peuvent être le support d’un harcèlement numérique, par le biais de  photographies partagées sans consentement ou impossibles à retirer, par la diffusion de fausses nouvelles, de dénonciations ou de calomnies. Des pratiques, des outils et des services permettent de se protéger, lutter et dénoncer de tels agissements.

	- Article 222-33-2-2 du code pénal [Légifrance](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000029336939&cidTexte=LEGITEXT000006070719&dateTexte=20140806) : _Le fait de harceler autrui par des propos ou comportements répétés ayant pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel, est puni de **deux ans d'emprisonnement et de 30 000 € d'amende**._
	- Le site [Non au harcèlement](https://www.nonauharcelement.education.gouv.fr/) offre des ressources permettant de sensibiliser au harcèlement et fournit des fiches indiquant comment réagir aux situations de harcèlement (victimes, témoins, parents et personnels concernés).

	![harcèlement](https://cdn.pixabay.com/photo/2020/11/25/22/23/computer-5777377_960_720.png)

### Droits d'auteurs et propriété intellectuelle



Qu'est-ce que cela a à voir ici ? 



* Vous avez bien compris déjà que, lorsqu'on publie quelque chose sur un réseau social,  il faut prendre en garde de ne pas enfreindre la sphère privé d'une tierce personne et encore moins de la diffamer ou de la harceler. 

* Il faut également prendre garde à ne pas diffuser des œuvres sans en avoir le droit ! L'auteur de ces œuvres ou l'organisme qui en possède les droits de diffusion, pourraient se retourner contre vous !

* Avez vous lu les conditions d'utilisation de votre réseau social ? Qu'a-t-il le droit de faire avec ce que vous y postez ? Avec vos photos ou vos créations ? La plupart du temps, vous perdez les droits de ce que vous publiez. A ce sujet, Voilà ce que l'on peut lire si l'on prend le temps nécessaire sur les conditions d'utilisation d'un compte Facebook :

	

	**"Le contenu et les informations que vous publiez sur  Facebook vous appartiennent, et vous pouvez contrôler la façon dont nous partageons votre contenu grâce aux paramètres de confidentialité et  des applications. (...).** 

	**Pour le contenu protégé par des droits de propriété intellectuelle**, comme les photos ou les vidéos (contenu de propriété intellectuelle), **vous nous donnez expressément la permission suivante**, conformément à vos paramètres de confidentialité et des applications : **vous nous accordez une licence non exclusive, transférable,  sous-licenciable, sans redevance et mondiale pour l’utilisation des  contenus de propriété intellectuelle que vous publiez sur Facebook ou en relation avec Facebook** (licence de propriété intellectuelle).  Cette licence de propriété intellectuelle se termine lorsque vous  supprimez vos contenus de propriété intellectuelle ou votre compte, sauf si votre contenu a été partagé avec d’autres personnes qui ne l’ont pas supprimé.  (...)

	

	> **Lorsque vous publiez  du contenu ou des informations avec le paramètre Public, cela signifie  que vous permettez à tout le monde, y compris aux personnes qui  n’utilisent pas Facebook, d’accéder à ces informations et de les  utiliser, mais aussi de les associer à vous (c’est-à-dire votre nom et votre photo de profil).**



### 









_________

Par Mieszczak Christophe			Publié sous licence CC-BY-SA

sources images :

* [réseaux sociaux - pixabay](https://pixabay.com/fr/illustrations/r%C3%A9seaux-sociaux-ic%C3%B4nes-twitter-1863613/)
* [Cyber voleur, pngwall CC 4.0 BY-NC](http://www.pngall.com/web-security-png/download/2631)
* [Harcèlement - pixabay](https://pixabay.com/fr/illustrations/ordinateur-harc%C3%A8lement-sur-internet-5777377/)
* [privé - pixabay](https://pixabay.com/fr/illustrations/bouclier-priv%C3%A9-public-internet-www-105499/)
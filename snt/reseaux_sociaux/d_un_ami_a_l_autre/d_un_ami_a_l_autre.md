# D'un ami à l'autre

Voici un graphe représentant les liens d'amitié réciproques dans un petit groupe de personnes sur un réseau social.

![graphe non orienté](media/graph_amitie.jpg)

>  Créez un programme `graphe.py`  : Nous allons coder un algorithme pour se déplacer sur le chemin le plus court d'un sommet à un autre.



### D'un ami au suivant

Soit le code ci-dessous :

 ```python
 import random 
 
def ami_ray():
    '''
    
    '''
    alea = random.randint(0, 2)
    ami = ''
    if alea == 0 :
        ami = 'ella'
    elif alea == 1 :
        ami = 'etta'
    else :
        ami = 'mick'
    return ami
 ```
>   
>
>   * A quoi sert la ligne `import random` ?
>
>   * Copiez ce code dans votre programme et complétez la Docstring de la fonction.
>
>   * En vous inspirant de cette fonction, codez une fonction équivalente pour chaque membre du réseau social.
>
>   * Testez chaque fonction dans la console au fur et à mesure.
>
>       

## Au suivant !

Il nous faut maintenant une fonction qui renvoie l'ami d'un des membres du graphe. Pour cela, complétez la fonction ci-dessous :

```python
def ami_suivant(ami_en_cours) :
    '''
    renvoie un ami parmi ceux de l'ami_en_cours passé en paramètre
    qui peut être 'ray', 'mick', 'etta', 'ella', 'jim' ou 'joe'
    '''
    ami_suivant = ''
    if ami_en_cours == 'ray' :
        ami_suivant = ami_ray()
    elif ...
    
    
```

_Remarque :_

Cette fonction demande un paramètre _ami_ : il faut donc le lui passer :

```python
>>> ami_suivant('mick')
???
>>> ami_suivant('ella')
???
```





## Cherchons un chemin d'un ami à un autre, au hasard



Nous allons maintenant chercher **un** chemin au hasard pour relier un membre de départ à un membre de destination parmi tous les chemins possible.

* Le nombre de protagonistes étant faible et tout le monde étant joignable, on considère qu'un chemin est toujours possible.

* Nous n'avons aucune garantie que ce chemin soit le plus court. Ce sera juste un chemin parmi d'autres.

* L'algorithme est le suivant :

    ````markdown
    On part d'un membre qui est le membre de depart 
    On souhaite arriver à un membre de destination
    
    Le chemin contient pour l'instant uniquement le membre
    Tant que le membre n'est pas celui de destination 
        On remplace le membre de départ par un de ses amis choisi au hasard
        On ajoute le membre au chemin
    On renvoie le chemin    
    ```



_Remarque 1 : la boucle tant que_

En Python, la structure d'une boucle _tant que_ est :

```python
while condition :
    #ici, après indentation onplace le code de la boucle
#comme d'habitude, en enlevant l'indentation, on sort de la boucle
```

Testez dans la console :

```python
>>> i = 0
>>> while i < 10:
    	i = i + 1
    	print(i)
    print('fini !')
???
```



_Remarque 2 :_

Nous allons utiliser une `structure de liste`. Sans entrer dans les détails, il s'agit d'une _variable_ particulière capable de stocker plusieurs données les unes derrières les autres. On retrouve ces données grâce à leur indice dans la liste.

Testons un peu dans la console :

```Python
>>> chemin = ['etta', 'ella', 'mick']
>>> chemin[0]
???
>>> chemin[1]
???
>>> chemin[2]
???
```



Pour ajouter un élément à une liste, on utilise la méthode `append` :

```python
>>> chemin.append('ray')
>>> chemin 
???
>>> chemin.append('mick')
>>> chemin
???
```





 > * Définissez la fonction `chercher_chemin`de paramètres `membre__depart` et `membre_destination` , deux chaînes de caractères différentes représentant deux membres, qui renvoie une liste possible de membres par qui passer pour les joindre en  choisissant au hasard.
 >
 > * Pour vous aider (vous compléterez la Docstring) :
 >
 >     ```python
 >     def chercher_chemin(membre_depart, membre_destination) :
 >         '''
 >             
 >         '''
 >         chemin = [membre_depart]
 >         membre = membre_depart
 >         while membre != membre_destination :
 >             
 >         
 >     ```
 >
 >
 > * Testez votre fonction via la console :
 >
 >     ```python
 >     >>> chercher_chemin('jim','eta') # à faire plusiseurs fois
 >     ???
 >     >>> ....
 >     ```
 >
 >     
 >



## Cherchons le chemin le plus court



* Pour trouver le chemin le plus court, on suppose que si on cherche au hasard un chemin un _grand nombre de fois_, l'un de ces chemins sera le plus court. **Une telle stratégie s'appelle une marche aléatoire**.



* On applique donc l'algorithme suivant :

````markdown
On veut rechercher le plus court chemin en réalisant nbre_XP recherches d'un chemin au hasard

Pour i dans la plage allant de 0 inclus et nbre_XP exclue
    On cherche un chemin au hasard
    si i vaut 0 alors on initialise une variable chemin_court égale à ce chemin
    sinon si la longueur du chemin est inférieure à celle du chemin_court alors 
       il devient le chemin_court
On renvoie chemin_court pour terminer

````

_Remarque :_

Pour obtenir la longueur d'une liste, on utilise la méthode `len` :

```python
>>> chemin = ['etta', 'ray', 'mick']
>>> len(chemin)
???
```





> * Définissez la fonction `rechercher_chemin_court`de paramètres `membre_depart`  et `membre_destination` , deux membres différents, et `nbre_XP` un entier décidant du nombre de recherches de chemins au hasard à effectuer.
>
> * Cette fonction renvoie le plus court chemin trouvé parmi les`nbre_XP` chemins trouvés au hasard.  
>
> * Pour vous aider (documentez cette fonction) :
>
>     ```python
>     def rechercher_chein_court(membre_depart, membre_destination, nbre_XP) :
>         '''
>             
>         '''
>         for i in range(nbre_XP) :
>             chemin = chercher_chemin(membre_depart, membre_destination)
>             if i == 0 :
>                     
>     ```
>
>     
>
> * Testez votre fonction via la console :
>
>     ````Python
>     >>> chercher_chemin_plus_court('mick','ella',5)
>     ???
>     >>> chercher_chemin_plus_court('mick','ella',100)
>     ???
>     ````
>
>     

_Remarque :_

Il existe des algorithmes beaucoup plus performants pour déterminer des chemins dans un graphe. On les étudie en détails en NSI, en terminale.





__________

Par Mieszczak Christophe - licence CC BY SA

source image : production personnelle.
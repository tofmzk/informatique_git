# Le Markdown

## Pourquoi choisir le Markdown ?

* Qu'est-ce :
	
* Un format texte utilisant un système de balises simples,libres et gratuits.
	
	​    
	
* Pourquoi faire  ?

  * Rédiger des documents proprement et efficacement.

  * Créer des documents légers (jusqu'à 100 fois plus léger que l'équivalent sous _Word_, _LibreOffice_ ou _pdf_), facilement et rapidement échangeables, modifiables et peu consommateurs d'énergie, ce qui n'est pas négligeable aujourd'hui.

  * Utiliser un format pérenne (le format _txt_) et libre qui ne nécessite qu'un éditeur de texte (bon ok, un peu évolué pour pouvoir interpréter le code).

  * il y en a beaucoup d'applications qui interpète le Markdown :

      * un de mes petits préférés est [Typora](https://www.typora.io/) ... mais, sous windows, les versions ultérieures à 1.0 sont payantes.
      * [Texts](http://www.texts.io/) est simple et gratuit

  * Permet l'utilisation de _notebook_ comme [Jupyter](https://jupyter.org/), présent dans Capytale,  ou de platerforme comme _Gitlab_.

  * Directement intégrable à un site web :

    * soit en installant un module Markdown.
    * soit en ajoutant quelques lignes de PHP en haut de la page.
    * soit en exportant votre document au format _HTML_ en un clic.

  * Export possible vers de multiples formats :_pdf_, _odt_ ... Au prix d'une augmentation de taille assez spectaculaire.

      

* Combien ça coûte ?
	
	* 15 min d'apprentissage et un peu d'entraînement.





# Un système de balise légère dérivé du HTML



## Les titres

````markdown
			# Ceci est un titre H1
## Ceci est un titre H2
### Ceci est un titre H3
````



Testez en dessous !



## Gras, italic, indice et exposant

- `écrire entre deux apostrophes situées sous la touche 7`  permet de surligner ainsi.

- `_italique_` s'affiche ainsi : *italique*

- `**gras**` s'affiche ainsi : **gras**

- `**_gras-italique_**` s'affiche ainsi : **_gras-italique_**

    




## Les listes



````markdown
* ceci est une puce niveau 1
	* ceci est une puce niveau 2 indentée à droite via la touche tabulation
		* ceci est une puce niveau 3 indentée à droite via la touche tabulation
````



Testez en dessous !





````markdown
1. ceci est une puce numérotée niveau 1 
	1. ceci est une puce numérotée niveau 2 indentée à droite via la touche tabulation
		
````



Testez en dessous !



## Les liens hypertext



````markdown
[description du lien](url du lien)
par exemple :
[le markdowns - wikipédia](https://fr.wikipedia.org/wiki/Markdown)
````



_Remarque :_ L'url peut être local ou non. 



Testez ici





## Les images



````markdown
![description de l'image](url du lien)
par exemple :
![logo markdown - source wikipédia - CC](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/208px-Markdown-mark.svg.png)
````





_Remarque :_ L'url peut être local ou non. 





Testez ici !



## Les tableaux 



````markdown
| Titre 1       |     Titre 2     |        Titre 3 |
| :------------ | :-------------: | -------------: |
| Colonne       |     Colonne     |        Colonne |
| Alignée à     |   Alignée au    |      Alignée à |
| Gauche        |     Centre      |         Droite |
````







Testez ici !



## Bloc codes



Vous ouvrez un bloc code, en commençant par une série de quatre "apostrophes du 7"  obtenu en pressant _ALT GR_ et _7_.

Voici par exemple un bloc code python ...

```txt
```python
def vive_le_md():
	return 'vive le md !'
​```
```



... qui aura le rendu ci-dessous :

````Python
def vive_le_md():
	return 'vive le md !'
````









## HTML au secours



Vous voulez aller plus loin ? Le _Markdown_ est un dérivé du HTML. Si vous codez en HTML des objets plus complexes, ils seront interprétés...



le code ci-dessous ...

```html
<table>
	<tr>
        <td colspan = 2 align = center style="background-color: yellow; color : red"> un tableau plus complexe </td>
    </tr>
    <tr>
        <td style='color : blue' align = 'left'> pas mal </td>
        <td style='color : blue' align = 'right'> non ? </td>
    </tr>
</table>
```

... donnera :

<table>
	<tr>
        <td colspan = 2 align = center style="background-color: yellow; color : red"> un tableau plus complexe </td>
    </tr>
    <tr>
        <td style='color : blue' align = 'left'> pas mal </td>
        <td style='color : blue' align = 'right'> non ?</td>
    </tr>
</table>



## Latex



Pour écrire proprement des formules mathématiques on utilise le _Latex_. Encore un langage à balise, plus _fouillé_ celui là.

* On l'insère ainsi : 

    ```txt
    $`code Latex`$
    ```

    

* On ajoute de anti-côtes  de chaque côté de la formule pour la montrer en ligne sur Gitlab. pour obtenir $`\frac 1 3`$ on tape :

    ```txt
    $`\frac 1 3 `$
    ```

    



Syntaxe usuelle :

``` latex
a \times b 
\frac {num} {dénom}
\sqrt {radicande}
a ^ {exposant}
a_{indice}

```



Pour en savoir, plus, rendez vous sur [ce site](https://www.tuteurs.ens.fr/logiciels/latex/manuel.html).



_Remarque :_

Oui, on pourrait créer un document entièrement en _Latex_... Mais ce serait bien plus fastidieux que de le faire ne _Markdown_ et bien moins accessible_ pour les élèves.





## Exporter vos documents



Via le menu _fichier/exporter_ vous pouvez exportez vos documents en un clic vers de multiples formats ( sous linux, c'est la spécialité de Pandoc). 

Cela au prix d'une considérable augmentation de la quantité de données ...





____________

Mieszczak Christophe 

Licence CC - BY - SA


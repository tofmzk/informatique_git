 # Coder une page web

## Pour bien démarrer



Vous trouverez, dans le répertoire _fakebook_ un fichier HTML,  _fakebook.html_ , et , une page de style _style.css_. 



* Faites un double clic sur le fichier _fakebook.html_ afin de l'exécuter c'est à dire de l'ouvrir avec un navigateur (Firefox est parfait). Pour l'instant, la page est vide mais vous apercevez le mot _Fakebook_ dans l'onglet de la page en haut. 
* Faites maintenant un clic droit sur le le fichier _fakebook.html_ et ouvrez le avec un éditeur de texte (notepad++, sublime text, Geany ...) afin d'en voir le code.



On y trouve la _structure_ classique d'une page HTML :

```HTML
<!DOCTYPE html>
<HTML>
	<HEAD>
		<meta charset="utf-8" />
		<link href="style.css" rel="stylesheet" type="text/css"  media="screen"/>  
        <TITLE>
			FakeBook
		</TITLE>
	</HEAD>
	<BODY>
		
	</BODY>
</HTML>

	
```

* La première balise, `<!DOCTYPE html>`, précise le type du document.
* Tout le code HTML doit être placé entre les balises `<HTML>` et `</HTML>`
* Les balises `<TITLE>..</TITLE>` permettent de stipuler le titre visible dans la languette de la page, ici _Fakebook_ que nous avons aperçu plus haut.
* Les balises `<HEAD>..</HEAD>` servent à indiquer les caractéristiques de la page :
    * le type de codage de caractères. C'est la _méthode_ utilisée pour encoder les caractères. Elle dépend, en gros, de l'alphabet utilisé.  L'UTF-8 est le codage le plus répandu au monde aujourd'hui (nous l'étudions en première NSI).
    * les liens vers d'autres pages, comme la page _style.css_ dont on se servira plus tard.
* les balises `<BODY>..</BODY>` délimitent la zone de code dans laquelle on mettra ce que l'internaute verra, c'est le _corps_ de la page et c'est entre ces balises que l'on va travailler.



_Remarque :_ 

Notez les tabulations, c'est à dire les décalages : le contenu d'une paire de balises doit être tabulé d'un cran vers la droite par rapport à ces balises. Ces tabulations sont INDISPENSABLES à une bonne lecture du code.



## Objectif

Nous allons réaliser une page _FakeBook_ . En voici un exemple :

![fakebook](media/fakebook.jpg)

Votre page devra comporter les mêmes éléments à savoir :

* Le gros titre servant à afficher le bandeau _Fakebook_
* Votre nom approximatif.
* Une photo vous représentant... plus ou moins, en respectant les droits d'auteurs.
* Un petit tableau comportant votre date de naissance, le nom de vos parents ... tout étant factice bien entendu.
* Votre profession imaginaire.
* Une liste à puces de vos fausses distinctions honorifiques
* Une liste à puces de vos passions imaginaires.
* Un lien, au moins, vers une de _vos oeuvres_ fictives.



## Balises nécessaires.

Nous allons commencer par placer les balises nécessaires  entre les balises `<BODY> ..</BODY>`. Nous nous occuperons du style (couleur, arrière plan, type de police de caractères ...) seulement ensuite...

Il y a deux type de balises :

* La plupart des vont par paires : une balise ouvrante ... et une fermante qui est presque la même avec un `/` en plus.
* Certaines balises sont seules : on dit qu'elles sont orphelines (snif) et elles terminent par un `/`.



Voici une petite liste des balises les plus courantes :

* Les balises de titre :

    ```HTML
    <H1>
    	Titre de niveau 1 (gros titre)
    </H1>
    <H2>
    	Titre de niveau 2 (plus petit)
    </H2>
    ```

* La balise qui trace une ligne de séparation est  `<HR/>` .

* Les balises de _paragraphe_ qui ne contiennent que du texte :

    ```HTML
    <P>
      	Ici j'écris mon texte.  	
    </P>
    ```

* La balise `<BR/>` (_Back Riot_ ou _retour chariot_ pour les nostalgique de la machine à écrire mécanique) permet d'aller à la ligne.

* La balise *image* :

    ```html
    <IMG src="url de l'image" title="commentaire"/>
    ```

    * _Le commentaire apparaît quand on passe sur la photo avec le curseur._

    * _l'url peut être locale (sur votre disque à côté du fichier fakebook.html) ou distant (sur un serveur). Nous privilégierons une url locale en raison du prochain TP filius_

* Voyons maintenant les tableaux : la balise`<TABLE>` marque le début du tableaux, `<TR>` ouvre une nouvelle ligne et `<TD>` ouvre une nouvelle cellule. 

    ```HTML
    <TABLE> <!-- début du tableau -->
    	<TR> <!-- ouverture de la première ligne du tableau -->
    		<TD> <!-- ouverture de la première cellule du tableau -->
    			cellule 1 ligne 1
    		</TD><!-- fin de la première cellule du tableau -->
    		<TD>
    			cellule 2 ligne 1
    		</TD>
    	</TR> <!-- fin de la première ligne du tableau -->
    	<TR> <!-- ouverture de la deuxième ligne du tableau -->
    		<TD> 
    			cellule 1 ligne 2
    		</TD>
    		<TD>
    			cellule 2 ligne 2
    		</TD>
    	</TR> <!-- fin de la deuxième ligne du tableau -->
    </TABLE>	<!-- fin du tableau -->
    ```

    _Par défaut, les tableaux n'ont pas de bordure. On ajoutera une bordure aux cellules dans la feuille style.css. Pour l'instant, impossible donc d'en voir les contours... mais ils sont bien là !_

* Les balises de liste ( les puces) :

    ```HTML
    <UL> <!-- série de puces non numérotées-->
    	<LI> <!-- première puce -->
    		première puce
    	</LI> <!-- fin première puce -->
    	<LI>
    		deuxième puce
    	</LI>
    </UL>
    
    <OL> <!-- série de puces numérotées-->
    	<LI>
    		première puce
    	</LI>
    	<LI>
    		deuxième puce
    	</LI>
    </OL>
    ```
    
* La balise de lien hypertexte :

    ```HTML
    <A href='url du lien'> description du lien </A>
    ```

    

    > **En avant et amusez vous bien !!**
    >
    > **N'oubliez pas de sauvegarder de temps en temps, cela n'est pas automatique**

    

## Un peu de style : le CSS

A côté du fichier _fakebook.html_ se trouve le fichier _style.css_. ouvrez-le avec votre éditeur de texte. Nous allons coder ici le style de la page HTML. 



Séparer le fond (en HTML) de la forme (en CSS) permet de modifier facilement l'aspect d'une page HTML sans être obligé de la réécrire. Seule la page CSS sera modifiée.



* On peut repérer les objets HTML dans la page css de plusieurs façon :

    * par leur balise. Toute les balises identiques seront impactée.

        ```CSS
        H1 {
        	/*ici on écrira le code du style des balises <H1>*/
        }
        ```

        

    * par un identifiant ajoutée dans la balise en HTML. Seule cette balise sera concernée, deux balises ne pouvant partager un même identifiant.

        Dans la page **HTML** : ajout de l'identifiant _id_

        ```HTML
        <H2 id = "titre_particulier">
        	Ceci est un titre très particulier
        </H2>
        ```

        Dans le fichier **CSS** : on récupère l'identifiant en le nommant après un #

        ```CSS
        #titre_particulier{
        	/*ici on écrira le code du style de la balise qui a cet identifiant*/
        }
        ```
        
    
    
    
	* par une classe ajoutée dans des balises en HTML. Plusieurs balises peuvent partager la même classe. Les balises de même classe seront toutes impactées par le style :
    
    	Dans la page **HTML** : ajout des classes _class_
    
    	```HTML
      <OL>
      	<LI class = "mes_puces">
            	puce 1
         	</LI>
         	<LI class = "mes_puces">
             	puce 2
         	</LI>  
      </OL>
      ```
    	
      Dans le fichier **CSS** : on récupère les classes en les nommant après un .
    	
      ```CSS
    	mes_puces{
        	/*ici on écrira le code du style de toutes les balises qui partagent cette classe*/
      }
      ```
    
    
    
* il faut maintenant connaître quelques fonctionnalités du CSS.

    * En ce qui concerne les couleurs :

        ```css
        color : red ; /* met la couleur en rouge */
        background-color : rgb(230, 230, 230); /*met l'arrière plan dans la couleur définie en rgb, on peut utiliser également le nom de la couleur*/
        ```
    
        
    
    * En ce qui concerne le texte :
    
        ```CSS
        font-family : tahoma; /* choisit la police tahoma */
        font-size : 18px; /* fixe la taille de la police à 18 pixels*/
        font-weight : bold; /* met le texte en gras */
        font-style : italic; /* met le texte en italic*/
        text-align : center; /* centre le texte dans son conteneur*/
        ```
        
        
        
    * En ce qui concerne les dimensions, quand l'objet en a :

        ```CSS
        width : 300px; /* fixe la largeur à 300 pixels*/
        height : 350px; /* fixe la hauteur à 350 pixels*/
        ```
        
        
        
    * En ce qui concerne les bordures (pour les cellules `<TD>`des tableaux par exemple) :
    
        ```CSS
        border : 2px solid black; /* une bordure de 2px en trait continue noir*/
        border-radius : 20px; /* arrondi les angles sur 20px*/
        box-shadow : 10px 10px 10px black; /* ajoute une ombre -> effet 3D */
        ```
        
        
        
    * En ce qui concerne les marges extérieures :
    
      ``` css
      margin : 10px; /*une marge de 10px tout autour */
    	margin-left  : 0px; /* une marge de 0px à gauche, pas de marge donc*/
      margin-right  : 10px; /* une marge de 10px à droite*/
    	margin-top  : 0px; /* une marge de 0px en haut, pas de marge donc*/
      margin-bottom : 10px; /* une marge de 10px en bas*/
      ```
    
    
    
    * En ce qui concerne les marges intérieures :
    
    	```css
      padding : 10px; /*une marge intérieure de 10px tout autour du contenu*/
    	padding-left  : 10px; /* une marge de 10px à gauche*/
      padding-right  : 10px; /* une marge de 10px à droite*/
      padding-top  : 10px; /* une marge de 10px en haut*/
    	padding-bottom : 10px; /* unemarge de 10px en bas*/
    	```





> **En avant et amusez vous bien !!**
>
> **N'oubliez pas de sauvegarder de temps en temps, cela n'est pas automatique**



___________

Par Mieszczak Christophe - Licence CC - BY - SA

Modifications apportées par Anne Boenisch

Source image : production personnelle
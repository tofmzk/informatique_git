# Markdown

## Objectif

Il s'agit ici est de coder une page HTML dont le visuel sera le suivant :

![](markdown.jpg)

Pour ce faire vous disposez :

* du fichier `markdown.html` dans lequel le squelette de la page est entammée et qu'il vous faut compléter.
* de la partie ci-dessous qui vous donne les balises HTML dont vous avez besoin.

## Les balises

* Les balises de titre :

    ```HTML
    <H1>
    	Titre de niveau 1 (gros titre)
    </H1>
    <H2>
    	Titre de niveau 2 (plus petit)
    </H2>
    ```

* La balise qui trace une ligne de séparation est  `<HR/>` .

* Les balises de _paragraphe_ qui ne contiennent que du texte :

    ```HTML
    <P>
      	Ici j'écris mon texte.  	
    </P>
    ```

* La balise `<BR/>` (_Back Riot_ ou _retour chariot_ pour les nostalgique de la machine à écrire mécanique) permet d'aller à la ligne.

* La balise *image* :

    ```html
    <IMG src="url de l'image" title="commentaire"/>
    ```

    * _Le commentaire apparaît quand on passe sur la photo avec le curseur._

    * _l'url peut être locale (sur votre disque à côté du fichier fakebook.html) ou distant (sur un serveur). Nous privilégierons une url locale en raison du prochain TP filius_

* Voyons maintenant les tableaux : la balise`<TABLE>` marque le début du tableaux, `<TR>` ouvre une nouvelle ligne et `<TD>` ouvre une nouvelle cellule. 

    ```HTML
    <TABLE> <!-- début du tableau -->
    	<TR> <!-- ouverture de la première ligne du tableau -->
    		<TD> <!-- ouverture de la première cellule du tableau -->
    			cellule 1 ligne 1
    		</TD><!-- fin de la première cellule du tableau -->
    		<TD>
    			cellule 2 ligne 1
    		</TD>
    	</TR> <!-- fin de la première ligne du tableau -->
    	<TR> <!-- ouverture de la deuxième ligne du tableau -->
    		<TD> 
    			cellule 1 ligne 2
    		</TD>
    		<TD>
    			cellule 2 ligne 2
    		</TD>
    	</TR> <!-- fin de la deuxième ligne du tableau -->
    </TABLE>	<!-- fin du tableau -->
    ```

    _Par défaut, les tableaux n'ont pas de bordure. On ajoute une bordure aux cellules dans la feuille style.css. Si vous regardez bien votre fichier HTML, vous noterez la présence d'une balise de style tout en haut : c'est à sela qu'elle sert !_

* Les balises de liste ( les puces) :

    ```HTML
    <UL> <!-- série de puces non numérotées-->
    	<LI> <!-- première puce -->
    		première puce
    	</LI> <!-- fin première puce -->
    	<LI>
    		deuxième puce
    	</LI>
    </UL>
    
    <OL> <!-- série de puces numérotées-->
    	<LI>
    		première puce
    	</LI>
    	<LI>
    		deuxième puce
    	</LI>
    </OL>
    ```
    
* La balise de lien hypertexte :

    ```HTML
    <A href='url du lien'> description du lien </A>
    ```



__________

Par Mieszczak Christophe licence CC BY SA
# TP Filius

Nous prenons ici la suite du TP Filius entamé sur le thème *internet*.

# Serveur web

* Passez en mode exécution (flèche verte). 

* Choisissez un des ordinateurs du réseau et cliquez dessus.

* Installez un serveur web sur cet ordinateur et démarrez le.

    

Un serveur n'est pas réellement un ordinateur. Il s'agit d'un logiciel qui tourne en permanence et attend les commandes, qu'on appelle _requêtes_, qu'on lui envoie. S'il le peut, il renvoie alors une réponse à ces requêtes. 

On peut installer un logiciel _serveur_ sur n'importe quel ordinateur d'un réseau. Celui-ci, par extension, est alors appelé serveur web. 





## Clients web



Maintenant qu'on a un serveur, les autres hôtes seront ses clients. Comme précedemment, mettez vous en mode _exécution_ (flèche verte), cliquez sur les futurs clients et, sur chaque client, installez un _navigateur web_ . 

Comme pour les serveurs, un client web est un logiciel qui envoie des requêtes à un serveur web. L'ordinateur sur lequel il est installé est appelé par extension _client_. 

Il est tout a fait possible d'être à la fois client et serveur mais, le plus souvent, lorsqu'il fait tourner un serveur, un ordinateur ne fait que cela.



### Se connecter à un site web via une IP



Connectons nous au petit site web hébergé par notre serveur. Toujours en mode simulation :

* Le serveur web doit être démarré.
* Cliquez sur les clients :
    * Lancer le navigateur web.
    * Tapez l'adresse IP du serveur dans la barre d'adresse ...
    * Regardez bien quels câbles se colorent lors de l'exécution de la requête.



Votre client a envoyé une reqête _GET_ au serveur qui, en retour, vous a envoyé la page index.html

Allez regarder ce qu'affiche l'application serveur de notre ordinateur serveur en cliquant dessus :

![filius8 perso](media/filius_8.jpg)





## Se connecter grâce à un nom de domaine.

Losqu'on navigue sur le web, il est rare d'utiliser les adresses IP des serveurs. La plupart du temps, ces derniers ont un _nom de domaine_. 

Pour en savoir plus avant de continuer ce TP, [allez jeter un oeil là](acceder_a_un_site_web.md)

...

Nous allons maintenant ajouter un serveur DNS qui permettra de donner un nom de domaine à notre site web.

Passez en mode _construction_.

* Ajouter un ordinateur :

    * Nommez-le _serveur DNS_. 
    * Donnez lui l'adresse cohérente (on lui attribue la plupart du temps la dernière adresse IP de son réseau).
    * Configurez les deux clients en précisant pour chacun d'eux l'adresse IP du serveur DNS que nous venons de créer.



Repassez en mode simulation :

* installer un serveur DNS sur notre serveur DNS. Comme pour le _serveur web_, le _serveur DNS_ est une application. On nomme par extension _serveur DNS_ l'ordinateur sur lequel cette application tourne en permancence.

* exécutez le serveur et ajoutez le nom de domaine _www.filius.com_ associé à l'adresse IP du serveur web

* démarrez le serveur DNS

    ![filius7 - perso](media/filius_7.jpg)





* Cliquez sur un client (testez les 2 clients !):
    * Lancer le navigateur web
    * Tapez le nom de domaine dans la barre d'adresse.
    * Regardez bien quels câbles se colorent lors de l'exécution de la requête.





___________

Par Christophe Mieszczak CC BY SA

_source images : impressions d'écran, production personnelle_ 










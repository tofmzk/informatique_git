# Le Web

## Un peu d'histoire

1. En quelle année et par qui a été inventé le web ?

    ```txt
    ```

2. Expliquer ce que sont le web et internet et les liens qui les unissent.

    ```txt
    
    
    
    ```

## Généralités

3. Expliquer le principe du modèle _client - serveur_.

    ```txt
    
    
    
    ```

4. Expliquer la différence entre les protocoles `HTTP` et `HTTPs`.

    ```python
    
    
    ```

4. Qu'est-ce qu'une URL ?

    ```txt
    ```

5. Qu'est-ce qu'un moteur de recherche ? Citez-en 3.

    ```txt
    
    
    ```

7. Lorsqu'on tape une requête dans un moteur de recherche, où et grâce à quoi la réponse est-elle calculée ?

    ```txt
    
    
    ```

7. Donner 3 critères que peut un moteur de recherche pour classer ses  réponses à une requête.

    ```python
    
    
    
    ```
    
7. Donner les avantages et les inconvénients des moteurs de recherche qui conservent vos données personnelles. Citer deux moteurs qui respectent votre vie privée et deux qui ne le font pas (précisez qui est qui bien entendu !).

    ```txt
    
    
    
    
    
    
    ```
    

## HTML et CSS

9. Quel sont les rôles respectifs du HTML et du CSS dans une page web ?

```txt


```

10. On veut placer le gros titre _Ma page web_ en haut d'une page web. Donner le code HTML nécessaire.

```HTML



```

11. Écrire à droite du code ci-dessous le rendu qu'en fera un navigateur.

```HTML
<UL>
    <li> le HTML </li>
    <li> le CSS </li>
</UL>
```

12. On veut réaliser un tableau de 2 lignes contenant 2 cellules. Donner le code HTML nécessaire.

```txt








	


```

13. On veut que toutes les balises `<H3> ` apparaissent en bleu et en 24px de haut. Donner le code CSS nécessaire.

```txt


```

14. On veut mettre une bordure rouge et un fond gris aux cellules des tableaux. Donner le code CSS nécessaire.

```txt



```

15. On veut qu'une seule balise `<H2>` parmi celles présentes sur la page web ait un style particulier. Comment procéder ?

```python



```

16. On veut que seule la première ligne de chaque tableau ait un style particulier. Expliquer comment procéder.

```python



```







________

Par Mieszczak Christophe  Licence CC BY SA

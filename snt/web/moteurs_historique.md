# Petit Historique

Le WEB serait difficilement utilisable sans les moteurs de recherche. Il est en effet constitué de
milliards de documents liés entre eux par des hyperliens. La première idée a été de mettre en place
des annuaires mais elle a été très vite abandonnée face à la très forte production de contenu.



Voici ,par exemple une représentation du web autour du site [wikipédia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal)



![le web autour de wikipéda- CCA](https://upload.wikimedia.org/wikipedia/commons/b/b9/WorldWideWebAroundWikipedia.png)



* Avant 1990, le web était très peu développé et proposait une petite quantité de ressources parmi lesquels **les utilisateurs naviguaient un peu au hasard**. Mais, avec l'explosion des ressources, il a fallu trouver un moyen plus pertinent pour naviguer dans l'immensité du web. 

    



* Le premier moteur de recherche est [Archie](https://fr.wikipedia.org/wiki/Archie_(moteur_de_recherche)) : une fois par mois, Archie vérifier l'intégralité de la toile pour archiver les nouveautés... Très vite il s'est montré dépassé par la croissance exponentielle de celles-ci.  Pour rappelle :

    

    | Année       | Nombre de sites web consultables |
    | ----------- | -------------------------------- |
    | 1991 (août) | 1                                |
    | fin 1991    | 10                               |
    | 1992        | 263                              |
    | 1993        | 6237                             |
    | 1994        | 10 022                           |
    | 1995        | 62 800                           |
    | 1997        | 1 000 000                        |
    | 2000        | 10 000 000                       |
    | 2003 (fév.) | 35 863 952                       |
    | 2004        | 57 000 000                       |
    | 2005        | 74 000 000                       |
    | 2006        | 101 000 000                      |
    | 2007        | 155 000 000                      |
    | 2008        | 186 000 000                      |
    | 2009 (nov.) | 207 316 960                      |
    | 2010 (fév.) | 233 636 281                      |
    | 2011 (avr.) | 312 693 296                      |
    | 2012 (jan.) | 582 716 657                      |
    | 2014 (nov.) | 947 029 805                      |



* En 1993 arrive le moteur de recherche [Excite](https://fr.wikipedia.org/wiki/Excite). Il combine en réalité deux moteurs de recherche existants. Pour chaque site existant, un petit texte de présentation est rédigé par un journaliste (Jim Bellows) afin d permettre aux utilisateurs de s'y retrouver plus facilement. Mais les internautes ne lisent pas trop les commentaires et , là encore, la quantité de données astronomique rend le projet compliqué à maintenir.

    

* D'autres moteurs de recherche apparaissent petit à petit, _Yahoo_, _Lycos_, _AltaVista_ ...

    

* En 1998 arrive le moteur de l'entreprise **Google**. Il fonctionne notemment avec un nouvel algorithme nommé _PageRank_, du nom de son inventeur _Jim Page_, qui classe les pages web en fonction de leur popularité ([voir le TP à ce sujet](pagerank.md)). Le succès de Google est ENORME et balaie la concurrence.  D'autres moteurs apparaissent dont le fonctionnement reprend celui de ce célèbre moteur de recherche.

* Il existe aujourd'hui une multitude de moteurs de recherche, Google est loin d'être le seul :

    * [QWANT](https://www.qwant.com/) : un moteur européen qui promet la protection des données
    * [bing](https://fr.wikipedia.org/wiki/Bing_(moteur_de_recherche)) le moteur de Microsoft
    * [Yahoo](https://fr.wikipedia.org/wiki/Yahoo%21)  un pionnier
    * [DuckDuckGO](https://duckduckgo.com/?t=hr) un moteur libre qui promet la protection des données
    * [ECOSIA](https://www.ecosia.org/) dont nous reparlons plus loin.



## Les moteurs menacés ?

En 2022, une révolution gronde dans le ciel des moteurs de recherche. Même le géant Google est en alerte rouge car il se sent menacé par une IA mise à disposition du grand public et capable d'écrire des textes en réponse à quasiment n'importe quelle question qu'on lui pose en langage naturel dans n'importe quelle langue : [chatGPT de l'entreprise openAI](https://openai.com/blog/chatgpt/).



Dès sa sortie, des milliards d'utilisateurs se sont rués sur chatGPT qui ne cherche pas des données toutes prêtes en rapport avec la question comme un moteur de recherche mais génère lui même une réponse en fonction de la question. Même si elle n'est pas infaillible, cette IA est réellement impressionnante et répond dans de nombreux cas bien plus pertinemment qu'un moteur de recherche.



Par exemple voici la réponse du moteur de Google à la question "Quel type de VTT dois-j choisir pour débuter ?" :

![requete google](media/requete_google.png)



Et voici celle de chatgpt :

![chatgpt](media/chatgpt.png)





Alerte rouge pour les moteurs de recherche, Google en tête : il va falloir évoluer pour survivre !

___________

Mieszczak Christophe

licence CC BY SA
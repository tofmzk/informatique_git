# Accéder à un site web



## Site web, DNS et adresse IP.

Lorsque nous saisissons `http://www.ac-lille.fr` dans la barre d'adresse d'un navigateur, nous indiquons au navigateur  que nous souhaitons entrer en communication avec le serveur hébergeant  le site `ac-lille.fr`en utilisant le protocole `HTTP` (**H**yper**T**ext **T**ransfert **P**rotocol) qui est le protocole de communication du web.



_Remarque :_

 Un protocole est  un ensemble de règles qui permettent à deux  machines de communiquer. Il en existe un grand nombre qui ont chacun un utilisation précise.  Le protocole `HTTPS` (**S** pour **S**ecure), par exemple, permet de sécuriser les échanges sur le web.



En fait, en premier lieu, le navigateur va interroger un `serveur DNS` (pour **D**omain **N**ame **S**erver). Celui-ci va renvoyer l'adresse `IP`du serveur hébergeant le site dont le `nom de domaine`est ac-lille.fr . L'adresse IP (pour Internet Protocol)  est le numéro d'identification attribué à ce nom de domaine.  



> Si votre OS est Windows ou Linux :
>
> * Ouvrir une fenêtre de commande Windows (windows + R puis cmd).
> * Saisir (par exemple) `nslookup ac-lille.fr` 
> (équivalent en ligne : https://ping.eu/nslookup/ )
> * vont apparaitre l'adresse IP de l'envoyeur et celle du destinataire. 



```txt
Serveur:	???
Address:	???

Réponse ne faisant pas autorité:
Nom:	
Address: 

```

* Le serveur DNS utilisé à pour adresse  :

* L'adresse IP du serveur qui héberge le site web www.ac-lille.fr est : 



> Trouver les adresses IP des sites:  
>
> - enthdf.fr  
>
>     ```txt
>             
>     ```
>
>     
>
> - amazon.co.uk  
>
>     ```txt
>             
>     ```
>
>     
>
> - google.fr
>
>     ```txt
>             
>     ```
>     
>
>
> - facebook.com  
>
>     ```txt
>             
>     ```
>





Il devient alors possible de se connecter au site en tapant l'adresse IP directement dans la barre d'adresse du navigateur.



> Dans la barre d'adresse de votre navigateur, tapez l'adresse IP de notre site.
>
> _Remarque :_
>
>  il se peut que l'adresse qui apparait dans le navigateur soit changée en `www1.ac-lille.fr`. Ceci signifie que l'on a été redirigé vers un autre serveur pour  alléger le serveur principal qui risque d'être saturé. Cela ne change  rien au chargement de la page qui sera la même. 



Il est même possible de localiser géographiquement l'ordinateur qui héberge le site.



> * Allez sur ce site : [localiser une adresse IP](https://trouver-ip.com/index.php)
> * Localisez le serveur utilisée pour héberger www.ac-lille.fr



_Remarque :_

Tout hôte d'un réseau, qu'il soit serveur ou client, possède sa propre adresse IP. Nous détaillerons cela dans un autre cours.



## URL





Quand la communication est établie, la page qui se charge est généralement celle nommée `index`(en php ou html). Pour le site ac-lille.fr, il s'agit de `index.php`.  



> Faire l'essai en saisissant `www.ac-lille.fr/index.php` dans la barre d'adresse de votre navigateur.



La partie `/index.php`s'appelle l'`URL`(**U**niform **R**essource **L**ocator). Elle permet  d'identifier la ressource et  d'indiquer l'endroit où elle se situe sur le serveur. Elle fournit en  fait le chemin d'accès à la ressource, dans l'arborescence du serveur.

Par exemple, en cliquant sur l'onglet `Région académique Hauts de France`, rubrique `Jeunesse, engagement et sport`, on charge la page `https://www1.ac-lille.fr/jeunesse-engagement-sports-education-populaire-et-vie-associative-122701`. L'URL de cette page est donc `/jeunesse-engagement-sports-education-populaire-et-vie-associative-122701`.



> * Allez, sur le site meteofrance.com, trouver les prévisions météo pour le Pas-de-Calais.
>
> * Quelle est l'URL de cette ressource ?
>
>     ```txt
>                     
>     ```
>
>     







___________

Par Christophe Miesczak avec la participation d'Eric Fiolet

Licence CC BY SA








# Les cookies

### Qu'est-ce que c'est ?



Un cookie est défini par le protocole de communication HTTP comme  étant une suite d'informations envoyée par un serveur HTTP à un client  HTTP, que ce dernier retourne lors de chaque interrogation du  même serveur HTTP sous certaines conditions.

C'est un petit fichier texte que nous attrapons en  naviguant sur un site internet et qui vient se cacher dans notre disque  dur. 

* Un cookie ne peut pas dépasser 4 Ko

* Un client ne peut pas avoir plus de 300 cookies sur son disque

* Un serveur ne peut créer que 20 cookies maximum chez le client

* il a un nom, une date d'expiration et une durée de vie maximale de 13 mois.

    



### A quoi il sert ?

Les cookies ont de nombreuses utilisations autre que pour vous  proposer des pubs.

*  Ils peuvent vous permettre d'enregistrer  votre mot de passe si votre navigateur ne vous le propose pas avant. 
* Ils vous aide également à mémoriser les actions que vous avez pu faire  dessus c'est-à-dire les paramètres tels que la langue ou encore  l'affichage. 
* Les cookies vont pouvoir informer l'éditeur du site sur le  comportement des utilisateurs (combien de temps ils ont passé sur le  site, quelles pages ils ont visitées...). 
* Ils vont vous  permettre de transférer plus facilement les pages sur différents réseaux sociaux.  



### Leurs "dangers"

Les cookies peuvent vous tracer, donc enregistrer de  nombreuses informations sur vous. Ce qui reste très contesté puisque  c'est considéré comme une violation de propriété. Ce qui montre une  certaine faiblesse de la sécurité sur internet.   

A ce sujet, la loi est très clair : l'utilisateur doit être averti de leur utilisation et l'acceptée.

Un cookie étant un fichier présent sur votre disque, il est possible et facile des les effacer :

* **Avec Google Chrome** :

    1. Cliquez sur l'icône ≡ en haut à droite de la fenêtre pour ouvrir le menu de réglage et choisissez Paramètres.
    2. Dans la fenêtre qui s'affiche, cliquez sur "Paramètres avancés" puis sur Effacer les données de navigation.
    3. Cochez la case Cookies et autres données de site (ainsi que d'autres si vous le souhaitez) et cliquez sur Effacer les données. Fermez  ensuite la fenêtre des réglages.

* Avec **Firefox** :

    1. Cliquez sur l'icône ≡ en haut à droite de la fenêtre pour ouvrir le menu de réglage et choisissez Paramètres.
    2. Dans la fenêtre qui s'affiche, cliquez sur "Paramètres" puis, dans le menu de gauche, choisissez "Vie privée et sécurité"
    3. Défilez la fenêtre de droite jusqu'à trouver **Cookies et données de sites** puis Effacer les donnée

    

    >  
    >
    > Effacez les cookies de votre machine.
    >
    >  

    ___

    Par Mieszczak Christophe licence CC BY SA

    










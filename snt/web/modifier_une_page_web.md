# Visualiser et modifier du code HTML et CSS

### Premières modifications

Rendez vous sur la pgae d'accueil de l'ENT :

![](media/ent.jpg)

Appuyez maintenant sur la touche `F12` : vous venez d'ouvrir le _debogueur_ et pouvez maintenant visualiser le code HTML, CSS (enre autres) de cette page :

![](media/debogueur.jpg)

Cliquez sur la petite flèche située juste à gauche de la languette `Inspecteur` .

Allez maintenant cliquer sur la ligne `Portail de connexion à l'ENT` . 

Remarquez que ce texte est écrit entre `<h1 ...>` et `</h1>` : il s'agit de balises de titre.

Dans la languette `Inspecteur`, la ligne de code HTML correspondante est selectionnée et vous pouvez la modifier : Allez- y !

![](media/salut_les_copains.jpg)

Regardez maintenant dans la partie juste à droite : vous y trouvez le code CSS de cette ligne, c'est à dire son style. 

On peut également le modifier en passant par exemple la couleur (_color_) en bleu. Pour cela, on clique sur le rond situé derrière la ligne commençant par `color :` et on y indique la couleur souhaitée (_blue_ ici) :

![](media/ent_css.jpg)

On peut également ajouter des commandes CSS pour modifier le style de la ligne sélectionnée :

![](media/ajouts_css.jpg)



### A vous de jouer

> Modifiez la ligne `Nos partenaires` , passez là en rouge en 32px et en italic.



### Balises images



Juste en dessous, il y a plusieurs images.

 Regardez la partie HTML. Toutes ces images correspondent aux balises `<img src = '.....'/>` ,`src` etant l'abrèviation de _source_ : il s'agit de l'URL de l'image, c'est à dire le chemin permettant d'y accéder, son adresse (nous verrons qu'il y a plusieurs type d'URL un peu plus tard).



> * Faites une recherche d'image sur le web. 
>
> * Assurez-vous que cette image utilise une licence qui vous permette de l'utiliser (domaine public, créative common, GNU ..)
>
> * Copier l'URL de l'image grâce à un clic droit : le navigateur peut parler de _l'adresse de l'image_ ou du _lien de l'image_
>
> * Coller ce lien derrière l'attribut `src` dans le code de la page web
>
> * Dans la partie CSS, modifiez la taille des images du site.
>
>     



## On a piraté l'ENT ??

Raffraichissez votre page maintenant.............

Ah.......................

Les modifications ont disparu !!

A votre avis pourquoi ?????

_________

Par Mieszczak Christophe licence CC BY SA
# Envoyer une requête à un serveur

Nous prenons ici la suite du TP Filius entamé sur le thème *internet*.

## Clients et serveurs

Nous avons installé un serveur web sur l'un des hôtes dans le TP précédent. 



Maintenant qu'on a un serveur, les deux autres hôtes seront ses clients.

* Renommez les en _client 1_ et _client 2_.
* Sur les deux clients, installez un _navigateur web_ 
* Sauvegardez votre travail sous le nom _réseau_.





### Se connecter à un site web via une IP



Connectons nous au petit site web hébergé par notre serveur. Toujours en mode simulation :

* Le serveur web doit être démarré.
* Cliquez sur les clients :
    * Lancer le navigateur web.
    * Tapez l'adresse IP du serveur dans la barre d'adresse ...
    * Regardez bien quels câbles se colorent lors de l'exécution de la requête.



## Se connecter grâce à un nom de domaine.

Nous allons maintenant ajouter un serveur DNS qui permettra de donner un nom de domaine à notre site web.

Passez en mode _construction_.

* Ajouter un ordinateur :

    * Nommez-le _serveur DNS_. 
    * Donnez lui l'adresse cohérente (on lui attribue la plupart du temps la dernière adresse IP de son réseau).
    * Configurez les deux clients en précisant pour chacun d'eux l'adresse IP du serveur DNS que nous venons de créer.



Repassez en mode simulation :

* installer un serveur DNS sur notre serveur DNS. Comme pour le _serveur web_, le _serveur DNS_ est une application. On nomme par extension _serveur DNS_ l'ordinateur sur lequel cette application tourne en permancence.

* exécutez le serveur et ajoutez le nom de domaine _www.filius.com_ associé à l'adresse IP du serveur web

* démarrez le serveur DNS

    ![filius7 - perso](media/filius_7.jpg)





* Cliquez sur un client (testez les 2 clients !):
    * Lancer le navigateur web
    * Tapez le nom de domaine dans la barre d'adresse.
    * Regardez bien quels câbles se colorent lors de l'exécution de la requête.



_Remarque :_

* Dans les deux cas, par IP ou nom de domaine, nous avons envoyé une requête _GET_ au le serveur qui, en retour, vous a envoyé la page index.html

* Allez regarder ce qu'affiche l'application serveur de notre ordinateur serveur :

![filius8 perso](media/filius_8.jpg)

 





___________

Par Christophe Mieszczak CC BY SA

_source images : impressions d'écran, production personnelle_ 










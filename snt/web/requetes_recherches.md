 # Des requêtes et des recherches

## Au commencement, il y a la requête

La requête est la _question_ que vous posez au moteur de recherche. Avant même d'être envoyée, elle passe par 3 étapes :

* un correcteur orthographique;

* Un anti-dictionnaire qui enlève les mots inutiles, les articles par exemple et les mots de moins de 3 lettres;

* l'analyse du champ lexical des mots conservés pour la recherche.

    

Les moteurs sont tellement évolués qu'ils sont capables d'interpréter des requêtes même lorsqu'elles sont mal exprimées. Cependant les optimiser permet d'obtenir des réponses plus pertinentes. Pour la plupart de ces moteurs :

* l'ordre des mots est important : les premiers seront prioritaires;
* si la requêtes est placée entre guillemets, le moteur recherchera des sites contenant exactement cette requête;
* mettre un moins devant un mot permet d'ignorer les pages contenant ce mot;
* ou peut utiliser des opérateurs logiques _OR_ ou _AND_ entre deux requêtes;
* commencer une requête par`site : addresse_site` indique au moteur de faire une recherche exclusivement sur ce site.



> Après avoir écouté une chanson :
>
> * j'ai compris que quelques bribes de paroles :  _... I feel just like ... Every time you see ..._
>
> * Un passage me pose problème : je ne sais pas s'il le chanteur prononce _And that's the reason why..._ ou _and that's the reason who i am_
>
> 
>
> En utilisant une seule requête :
>
> 1. Pouvez-vous retrouver de quel morceau il s'agit ? 
> 2. Après Santana et Buddy Miles, qui a également interprété ce titre ?




## Comment un moteur recherche-t-il ?



Regardons une [petite vidéo d'introduction](https://youtu.be/iKMm6SXO0wA).

Tout un travail est fait en permanence par les moteurs :

- L'exploration : des _robots_, appelés _spiders_, parcourent le web en permanence, de lien en lien,  et récupèrent les données intéressantes.
  
- L'indexation : les mots significatifs sont classés et enregistrés dans une base de données.
  
  - Le contenu des pages est analysé en termes de mots clés. Les pages sont ensuite classées selon leur pertinence, leur degré de confiance, ... A chaque mot clé va alors correspondre une liste d’adresses de pages WEB. C’est **un index**.
    - Toutes les pages ne seront pas sauvegardées. Certaines pages provenant de sites illégaux ou de mauvaise réputation sont blacklistées. 
    - Les données sauvegardées sont stockées dans d'énormes _data-centers_.


  ![data center - source wikipédia - GNU](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/BalticServers_data_center.jpg/800px-BalticServers_data_center.jpg)

  ​

Les moteurs de recherche préparent à l’avance les résultats des requêtes les plus populaires : "Facebook", "Youtube", "Vidéo", "TV", "Jeux", ... Ainsi, ils donnent directement les résultats sans nécessairement avoir à chercher dans l'index. 



Une fois la requête analysée le moteur parcourt ses bases de données et affiche les sites correspondant le mieux à la requête selon un ordre qui dépend du moteur de recherche. 









## Données personnelles et bulle sociétale



Certains moteurs de recherches utilisent vos données personelles qu'ils collectent à chacun de vos clics et à chacune de vos requêtes et à chacune de vos visites ou de vos posts sur les réseaux sociaux (utube appartient à Google ...) . Le moteur de [Google](https://www.google.fr) , par exemple, use et abuse de ces données. A l'opposé, le moteur [Duck Duck Go](https://duckduckgo.com), par exemple,  n'en collecte aucune. L'usage de vos données par le moteur a un impact important.

La collecte de vos données avec une requête imprécise concernant un domaine qui vous intéresse particulièrement permet d'obtenir tout de même les résultats que vous attendez la plupart du temps. Par exemple, si vous êtes passionné de VTT, une simple requête "vélo" vous proposera en priorité des pages web présentant des VTT.

Sur un moteur qui ne collecte pas vos données, cette requête affichera tout site en rapport avec tout type de vélos. Il faudra donc être plus précis pour obtenir une réponse adéquate.



> * Testez la requête _météo_ sous [Google](https://www.google.fr) puis sous [Duck Duck Go](https://duckduckgo.com) .
> * Une analyse des requêtes les plus populaires est donnée par Google [ici](https://trends.google.fr/trends/?geo=FR). N'hésitez pas a en tester quelques unes.



Oui mais, cela a une fâcheuse conséquence : vos données vous enferment dans ce qu'on appelle un **petit monde ou une bulle sociétale**. c'est un phénomène que l'on retrouve partout où vos données sont utilisées par un algorithme pour vous proposer des réponses jugées pertinentes pour votre profil :

* Vous aimez les VTT ? On ne vous montrera pas qu'il existe d'autres pratiques....
* Vous êtes fan d'un certain type de musique ? Vous risquez de ne pas pouvoir en découvrir d'autres.
* Vous avez jusque là regardé uniquement des films de science fiction ? On ne vous conseillera que des films de ce genre.
* Vous êtes convaincu qu'une idée est la seule valable ? Vos requêtes iront dans votre sens et vous conforteront dans vos _croyances_.

!!! note Activité

* En 2011, un grand nombre d’internautes a soumis pendant plusieurs semaines des requêtes sur Google associant le nom d’une société « la lyonnaise de garantie » et le terme « escroc ». La fonctionnalité Suggest de Google en a pris acte, et proposait à toute personne saisissant le nom de l’entreprise d’y accoler ce terme indubitablement injurieux. Dans cette affaire judiciaire qui a fait
    grand bruit, le moteur de recherche a d'abord été condamné par une cour d’appel, mais la décision a été par la suite censurée par la Cour de cassation au motif que le rapprochement critiqué était le fruit d’un processus purement automatique et ne pouvait être imputé à la volonté de nuire de l'exploitant du moteur de recherche. _Emmanuel Netter, Numérique et droit privé, 2017_
* « Si *** se contente de saisir dans le moteur de recherche de son entreprise une suite de caractères en apparence absconse, comme « C-362/14 », l’algorithme comprendra qu’il s’agit d’un numéro d’arrêt rendu par la Cour de justice de l’Union européenne, car cet utilisateur déterminé a l’habitude de consulter le site de la juridiction. La même requête formulée par une personne ayant
    d’autres centres d’intérêt conduira à des résultats différents. » _Francis Donnat, directeur des politiques publiques de Google France, lors d’une conférence donnée le 7 décembre 2015._
* « Si une personne politiquement marquée à droite et une autre plutôt à gauche lancent une recherche Google sur les lettres « BP », la première reçoit, en tête de page, des informations sur les possibilités d’investir dans la British Petroleum, la seconde sur la dernière marée noire qu’a causée la compagnie pétrolière britannique. » _Eli Pariser, The Filter Bubble, 2012_
* « Latanya Sweeney, une chercheuse en informatique afro-américaine, a remarqué que, lorsqu’elle tapait son nom dans le moteur de recherche de Google, elle voyait apparaître la publicité « Latanya Sweeney arrested ? ». Cette publicité propose un service de consultation en ligne […] qui permet […] de savoir si les personnes ont un casier judiciaire. Or, le nom de ses collègues blancs
    n’était pas associé au même type de publicité […].» _Dominique Cardon, A quoi rêvent les algorithmes, 2015_



!!!









Vous l'avez compris, les algorithmes qui utilisent votre profil vous enferment dans cette bulle dans lequel tout va dans votre sens, chacun est d'accord avec vous et où la confrontation avec des points de vue différents n'existe pas. Ce phénomène est bien plus dangereux qu'il n'y parait : il enferme les gens dans un mode de pensée et crée des communautés hermétiques les unes aux autres. Comprenez bien que le danger n'est pas d'avoir des opinions, des idées ou des envies différentes mais de croire que seules les siennes sont valables, que tout le monde pense la même chose et que, par conséquent, s'il existe des gens qui ne sont pas d'accord alors ils ne peuvent qu'avoir tort et être insupportables voir détestés ... 





### Suppression des données 



La collecte systématique de données personnelles engendre la désagréable et nauséabonde impression d'être en permanence scruté et suivi. Ce n'est pas qu'une impression. Une entreprise comme Google connait tout de vous et vous trace en permanence, principalement dans le but de vous envoyer la publicité ciblée qui constitue une part importante des revenues de l'entreprise. 

En Europe, le [RGPD](https://www.cnil.fr/fr/comprendre-le-rgpd) (réglement général pour la protection des données) protègent les utilisateurs d'utilisations abusives de leurs données. Pour les collecter, une entreprise doit prévenir les utilisateurs qu'elle le fait, avoir leur accord, expliquer pourquoi elle en a besoin et permettre de les effacer. Lorsque vous créez un compte sur Google ou sur un réseau social, vous acceptez leurs conditions d'utilisation qui précisent tout cela.



Il est tout à fait possible de supprimer une partie de ces données :

* sur votre propre machine, des [cookies](cookies.md) stockent des informations et votre historique de navigation mémorise les sites que vous avez visités. Vous pouvez effacer tout cela en suivant le menu de votre navigateur concernant l'historique.
* Les serveurs de l'entreprise dont vous utilisez conservent également ces données. Vous pouvez leur demander d'en supprimer une partie : c'est le droit à l'oubli.



>  * Ouvrez un navigateur et effacez son historique.
>  *  Pour supprimer vos données avec Google, suivez donc [ce lien](https://support.google.com/websearch/troubleshooter/9685456?hl=fr).





## Business is business...

Un moteur de recherche cherche a gagner de l'argent. Il y a plusieurs façon de faire :

* rendre payant le référencement pour les entreprises qui apparaitront plus ou moins haut dans le classement lors d'une recherche.
* afficher des publicités ciblées selon la recherche et faire payer les publicitaires : en 2017 Google gagnait près de 100 miliiards de dollars par an par ce moyen.
* vendre vos centres d'intérêts déterminés via vos requêtes aux entreprises que cela peut intéresser dans un but publicitaire.
* utiliser des sponsors intéresser par l'image du moteur de recherche.
* demander aux utilisateurs une participation volontaire pour soutenir le moteur de recherche.

> Quels moyens de gagner de l'argent nécéssite de conserver vos données personnelles ? 



**62 % des internautes ne dépassent pas la première page des résultats d’une recherche sur internet.**

 **D’où l’importance d’y figurer !**



L’image ci-dessous montre où se porte le regard de l’internaute sur une page Google. Vous aurez :

* 100% de visibilités vous êtes en 1er, 2e ou 3e position

* 85% de visibilité pour la 4e position

* 60% de visibilité pour la 5e position

* 50% de visibilité pour la 6e et 7e position

* 30% de visibilité pour la 8e et 9e position

* 20% de visibilité si vous êtes en 10e position



![img](media/recherche.jpg)



Ainsi, les moteurs de recherche vont proposer  des liens sponsorisés avant les résultats dits naturels de la requête de l’internaute. De plus,des publicités seront aussi présentes sur les sites clients.





## Un peu d'écologie ?



Les moteurs de recherche sont devenus incontournables. Selon les Officiels américains, les *data centers*  consomment maintenant 3 % de l’énergie mondiale et devrait être à 6 % en 2020. D’où la question, une requête sur Google, ça dégage combien de C02 ?



Un chercheur de Harvard, M. Wissner-Gross, a calculé que chaque requête sur le moteur de recherche Google produit 7 g de C02 du fait de l’immense quantité d’énergie consommée par les quelque 500.000 serveurs du moteur de recherche américain.



Ces 7 grammes sont à multiplier par [un nombre incroyable de requêtes par jour](https://www.compteur.net/compteur-nombre-recherche-google/)  tout au long de l’année. Selon lui, cela représente autant d’énergie que la consommation d’un pays comme le Laos.



Existe-t-il de ce point de vue des moteurs de recherches plus _vertueux_ sur le plan écologique ?



> Le moteur **[ECOSIA](https://www.ecosia.org/)** propose justement de contre-carrer la production de CO2 en plantant des arbres et en recourant à l'énergie ''verte''.
>
> Suivez [ce lien](https://fr.wikipedia.org/wiki/Ecosia) pour en apprendre d'avantage... 



___________

Par Mieszczak Christophe - Licence CC - BY - SA

Sources images : wikipedia, CC BY SA
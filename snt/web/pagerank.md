 # Le Page Rank

## Qu'est-ce que c'est que ce truc là ?



 Le PageRank ( du nom de son inventeur, Larry Page, cofondateur de Google) est l'algorithme d'analyse des liens hypertextes utilisé pour le classement des pages Web par le moteur de recherche Google. Le PageRank n'est qu'un indicateur parmi d'autres dans l'algorithme qui permet de classer les pages du Web dans les résultats de recherche. 

Le principe de base est d'attribuer à chaque page une valeur  proportionnelle au nombre de fois que passerait par cette page un utilisateur parcourant le Web en cliquant aléatoirement, sur un des liens apparaissant sur chaque page. 

 A chaque instant, des "robots", appelés "spiders", parcours la toile, de lien en lien, et mettent ainsi sans cesse à jour le PageRank des pages du web. 



## Activité 1 : avec une pièce de monnaie



On schématise une page web par un cercle et les liens entre les pages par des flèches. Trois pages A, B et C sont donc représentées de la façon suivante :


![activité 1](media/pagerank1.jpg)



L'objectif est de déterminer la fréquence avec laquelle on visite chaque page lorsqu'on parcourt le graphe de façon aléatoire.

* Au départ le nombre de visite de chaque page est nul.

* On choisit une page de départ au hasard et on incrémente de 1 son nombre de visite (il passe donc à 1).

* A l'aide d'une pièce de monnaie équilibrée, nous allons nous promener sur ce graphe en respectant les règles ci-dessous:

	* Si d'une page ne part qu'un seul lien alors on le suit.
	* Si d'une page partent deux liens, on décide que l'un d'eux correspondra à face, l'autre à pile et on lance la pièce. Selon le résultat, on emprunte le lien correspondant.
	* A chaque nouvelle page atteinte, on augmente son nombre de visite(s) et on se déplace à nouveau. 

Réalisez cette expérience un grand nombre de fois puis donnez un PageRank à nos trois pages, c'est à dire leurs fréquences de visite.



## Activité 2 : simulation et Python



- Créez un nouveau programme Python et appelez le _pageRank.py_

- Il faut importer le  module *random* , qui permet les choix aléatoires, en tapant en haut du code  :

    ```Python
    import random
    ```




### Depuis A.



* Définissons une fonction _depuis_A_, sans paramètre qui renverra soit 'B' soir 'C' en choisissant au hasard de façon équiprobable entre B et C.

    ```python
    def depuis_A() :
        '''
       renvoie soit 'B' soit 'C' au hasard
        '''
    ```

    

* il y a une chance sur 2 de choisir soit 'B' soit 'C' depuis A. On va donc choisir un nombre au hasard en tre 0 et 1 pour simuler cela :

    ```python
    def depuis_A() :
        '''
       renvoie soit 'B' soit 'C' au hasard
        '''
        nbre_alea = random.randint(0, 1)
    ```

    

* **Si** _nbre_alea_vaut 0 **alors** la fonction renverra 'B' sinon elle renverra 'C'. Nous avons besoin d'une **structure conditionnelle**. La voici :

    ```python
    if condition1 : # condition1 peut soit valoir True (vrai) soir False (faux)
        # ici on code ce qui se passe si condition est vraie
    elif condition2 : #elif est un condensé de else if c'est à dire sinon si
        # ici on code ce qui se passe si condition2 est vraie
    else : # sinon 
        # ici on code ce qui se passe si les conditions précédentes sont toutes fausse.
    ```

    

    Pour notre fonction, cela donne donc :

    ```python
    def depuis_A() :
        '''
       renvoie soit 'B' soit 'C' au hasard 
        '''
        nbre_alea = random.randint(0,1)
        if nbre_alea == 0 : # le double égal vérifie l'égalité et renvoie True ou False
            return 'B' # la fonction renvoie B si nbre_alea vaut 0
        else : #sinon
            return 'C' la fonction renvoie 'C' sinon
    ```

* Depuis la console, testons notre fonction :

    ```python
    >>> depuis_A()
    ???
    >>> depuis_A()
    ???
    ```

_Remarque :_

Testez l'instruction ci-dessous :

```python
>>> depuis_A
???
```

`depuis_A` et `depuis_A()` ne donne pas le même résultat :

* `depuis_A` renvoie le type de `depuis_A` qui est une fonction.
* `depuis_A()` renvoie le résultat de la fonction `depuis_A`.



### depuis B et depuis C



> Sur le même modèle que la fonction précédente, réalisez les fonctions :
>
> * _depuis_C_ qui renvoie 'A' ou  'B'
> * _depuis_B_ qui renvoie 'A'.
>
> Testez vos fonctions depuis la console



### Choix du départ



> Définissez une fonction _choix_depart_, sans paramètre, qui renvoie, en choisissant aléatoirement de façon équiprobable le site de départ, c'est à dire 'A' ou 'B' ou 'C'
>
>  
>
> Testez votre fonction depuis la console



### Parcourir notre petite toile

Nous allons maintenant coder une fonction qui va nous permettre de classer les sites par popularité en réalisant 100 sauts d'un site à un autre. Appelons la _spider_. Elle renverra les fréquences de visites de 'A', 'B' et 'C'.

```python
def spider() :
    '''
    renvoie les fréquences de visites de 'A', 'B' et 'C' en réalisant 100 sauts
    d'un site vers un autre
    '''
```

* Commençons par choisir le site courant parmi 'A', 'B' ou 'C' en utilisant la fonction _choix_depart_ :

    ```python
    def spider() :
        '''
        renvoie les fréquences de visites de 'A', 'B' et 'C' en réalisant 100 sauts
        d'un site vers un autre
        '''
        site_courant = ???
    ```

* Initialisons maintenant trois variables correspondant au nombre de visite des sites A, B et C :

  ```python
  def spider() :
      '''
      renvoie les fréquences de visites de 'A', 'B' et 'C' en réalisant 100 sauts
      d'un site vers un autre
      '''
      site_courant = choix_depart()
      nbre_A = ?
      nbre_B = ?
      nbre_C = ?
  ```
  
* Nous allons faire une boucle afin de réaliser le nombre d'expériences souhaité. Pour cela, nous avons besoin d'une boucle **for** dont voici la structure :

    ```python
    for i in range(100) : # répéte 100 fois
    	#le code a répété est tabulé à droite
    # en enlevant la tabulation, on sort de la boucle 
    ```
    
    
    
* Complétons notre fonction afin de réaliser _nbre_XP_ sauts :

    ```python
    def spider() :
        '''
        renvoie les fréquences de visites de 'A', 'B' et 'C' en réalisant nbre_xp sauts
        d'un site vers un autre
        '''
        site_courant = choix_depart()
        nbre_A = 0
        nbre_B = 0
        nbre_C = 0
        for i in range(???):
            
    ```
    
* **Si** _site_courant_ vaut 'A' **alors** _nbre_A_ augmente de 1, **sinon si ** _site_courant_ vaut 'B' **alors** _nbre_B_ augmente de 1, **sinon** _nbre_C_ augmente de 1. Saurez vous coder cela ?

    ```python
    def spider() :
        '''
        renvoie les fréquences de visites de 'A', 'B' et 'C' en réalisant 100 sauts
        d'un site vers un autre
        '''
        site_courant = choix_depart()
        nbre_A = 0
        nbre_B = 0
        nbre_C = 0
        for i in range(100):
            if site_courant == 'A' :
                nbre_A = nbre_A + 1
                ???
    ```
    
* Une fois le bon compteur incrémenté, c'est à dire augmenté, il faut choisir le prochain site :

    * **si** le _site_courant_ est 'A' **alors** le _site_courant_ est choisi entre 'B' et 'C' en utilisant la fonction `depuis_A`.
    * **sinon si** le _site_courant_ est 'B' **alors** le _site_courant_ est choisi entre 'A' et 'C' en utilisant la fonction `depuis_B`.
    * **sinon**  le _site_courant_ est choisi en utilisant la fonction `depuis_C`.

    ```python
    def spider() :
        '''
        renvoie les fréquences de visites de 'A', 'B' et 'C' en réalisant 100 sauts
        d'un site vers un autre
        '''
        site_courant = choix_depart()
        nbre_A = 0
        nbre_B = 0
        nbre_C = 0
        for i in range(100):
            if site_courant == 'A' :
                nbre_A = nbre_A + 1
                site_courant = depuis_A()
            elif site_courant == 'B' :
            	???
                       
                       
    ```
    
    
    
* une fois la boucle terminée, on enlève la tabulation et on renvoie les trois fréquences de visite, c'est à dire _nbre_A / 100_,   _nbre_B / 100_ et  _nbre_C / 100_.

    ```python
    def spider() :
        '''
        renvoie les fréquences de visites de 'A', 'B' et 'C' en réalisant 100 sauts
        d'un site vers un autre
        '''
        site_courant = choix_depart()
        nbre_A = 0
        nbre_B = 0
        nbre_C = 0
        for i in range(100):
            if site_courant == 'A' :
                nbre_A = nbre_A + 1
                site_courant = depuis_A()
            elif site_courant == 'B' :
                ???
        return ?
    ```
    
    

*  Notre boucle fait toujours 100 sauts. Et si on veut on faire plus ? On va utiliser un **paramètre** : cela permet de passer à la fonction une valeur lorsqu'on l'appelle :

    ```python
    def spider(nbre_de_sauts) :
        '''
        renvoie les fréquences de visites de 'A', 'B' et 'C' en réalisant 
        le nombre de sauts précisés via le paramètre nbre_de_sauts
        d'un site vers un autre
        '''
        site_courant = choix_depart()
        nbre_A = 0
        nbre_B = 0
        nbre_C = 0
        for i in range(100):
            if site_courant == 'A' :
                nbre_A = nbre_A + 1
                site_courant = depuis_A()
            elif site_courant == 'B' :
                nbre_B = nbre_B + 1
                site_courant = depuis_B()
            else :
                nbre_C = nbre_C + 1
                site_courant = depuis_C()
        return nbre_A / 100, nbre_B / 100, nbre_C / 100
    ```

    Dans la console, on apelle maintenant la fonction en précisant la valeur qu'on attribue au paramètre _nbre_de_sauts_. Cette valeur est appelée _argument_.

    ```python
    >>> spider(1000)
    ???
    >>> spider(10000)
    ???
    ```

* Pour l'instant, on fait toujouts 100 sauts !! Il reste à modifier la boucle pour qu'elle effectue _nbre_de_sauts_ tours et ne pas oublier de modifier le calcul des fréquences au niveau du _return_. En avant !!

    Une fois ces modifications effectuées, testez votre fonction avec un nombre d'expériences important. Quel est le site le plus populaire ? 

​    



## Activité 3 : en autonomie

> Sauriez-vous adapter le travail précédent pour calculer le PageRank des pages A,B,C et D modélisées par le graphe ci-contre ? Testez votre algorithme avec 100 puis 1000 puis 10000 déplacements. Classez A,B,C et D par popularité.

![_](media/pagerank2.jpg)





## Activité 4 : cas particuliers



Parcourez aléatoirement le graphe ci-dessous et trouvez quel est le problème.

![activité 4  - pb1](media/pagerank3.jpg)



Pour pallier à ce problème, on décide que si la page visitée ne redirige pas vers une autre page, alors on choisit au hasard la page suivante parmi les autres.

​	

> Sauriez-vous adapter vos algorithmes pour calculer le PageRank des pages A,B,C et D modélisées par le graphe ci-contre ?
>
> 
>
> Testez votre algorithme avec 100 puis 1000 puis 10000 déplacements. Classez A,B,C et D par popularité.

 



## Activité 5 : encore un cas particulier



Parcourez aléatoirement le graphe ci-dessous et trouvez quel est le problème.

![cas particulier 2](media/pagerank4.jpg)



 Pour pallier à ce problème on décide que, à chaque page visitée quelle qu'elle soit, on choisit une fois sur vingt la page suivante au hasard parmi toutes les pages.




>  Sauriez-vous adapter vos algorithmes pour calculer le PageRank des pages A,B,C et D modélisées par le graphe ci-contre?
>
> 
>
> Testez votre algorithme avec 100 puis 1000 puis 10000 déplacements. Classez A,B,C et D par popularité. 



___________

Par Mieszczak Christophe - Licence CC - BY - SA

 sources images : production personnelle





 
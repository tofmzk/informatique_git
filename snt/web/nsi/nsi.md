# NSI

## Objectif

Il s'agit ici est de coder une page HTML dont le visuel sera le suivant :

![objectif](objectif.png)

## C'est cadeau

La page d'accueil de tout site web doit impérativement s'appeler `index.html`. 

Vous trouverez dans le même répertoire que ce TP : 

* un fichier `index.html` dans lequel le squelette de la page est entamé.
* les images  _logo.jpg_ et _qrcode.png_ dont vous aurez besoin.



Ouvrez `index.html` avec un editeur de texte (notepad ++ sous windows ou Geany sous linux par exemple)

AJoutez les balises afin d'obtenir le visuel attendu. 

Quelles balises ? Regardez donc la partie suivante pour les découvrir !



## Les balises

* Les balises de titre :

    ```HTML
    <H1>
    	Titre de niveau 1 (gros titre)
    </H1>
    <H2>
    	Titre de niveau 2 (plus petit)
    </H2>
    ```

* La balise qui trace une ligne de séparation est  `<HR/>` .

* Les balises de _paragraphe_ qui ne contiennent que du texte :

    ```HTML
    <P>
      	Ici j'écris mon texte.  	
    </P>
    ```

* La balise `<BR/>` (_Back Riot_ ou _retour chariot_ pour les nostalgique de la machine à écrire mécanique) permet d'aller à la ligne.

* La balise *image* :

    ```html
    <IMG src="url de l'image" title ="commentaire"/>
    ```

    * _Le commentaire apparaît quand on passe sur la photo avec le curseur._

    * _l'url peut être locale (sur votre disque à côté du fichier index.html) ou distant (sur un serveur). Nous privilégierons une url locale en raison du prochain TP filius_

* Les balises de liste ( les puces) :

    ```HTML
    <UL> <!-- série de puces non numérotées-->
    	<LI> <!-- première puce -->
    		première puce
    	</LI> <!-- fin première puce -->
    	<LI>
    		deuxième puce
    	</LI>
    </UL>
    
    ```
    
* La balise de lien hypertexte :

    ```HTML
    <A href='url du lien à préciser '> description du lien </A>
    ```
    
    * dans la partie _href_, on indique l'URL vers laquelle pointe le lien. Elle peut être distante, locale ou faire partie de la page web elle même.
    * _la description du lien_ correspond à ce que verra l'utilsateur de la page. C'est ce sur quoi il cliquera.

​	

__________

Par Mieszczak Christophe licence CC BY SA
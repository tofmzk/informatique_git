# Dijkstra



<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Edsger_Dijkstra_1994.jpg/1024px-Edsger_Dijkstra_1994.jpg" alt="Edsger Dijkstra - wikipédia - CCA-SA" style="zoom:50%;" />

## Objectif et principe



L' algorithme de Dijkstra, du nom du mathématicien et informaticien [Edsger Dijkstra](https://fr.wikipedia.org/wiki/Edsger_Dijkstra) dont vous voyez la trombine plus haut,  sert à résoudre le [problème du plus court chemin](https://fr.wikipedia.org/wiki/Problèmes_de_cheminement) dans un graphe pondéré. Il permet, par exemple, de déterminer un plus court chemin pour se  rendre d'une ville à une autre connaissant le réseau routier d'une  région. Plus précisément, il calcule des plus courts chemins à partir  d'une source vers tous les autres sommets dans un graphe orienté pondéré par des réels positifs. 



[Une petite vidéo d'explication](https://youtu.be/rI-Rc7eF4iw)





## Application 

On cherche le chemin le plus court de la ville A vers la ville J. Voici le graphe pondéré des différentes villes entre A et J.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/DijkstraBis01.svg/800px-DijkstraBis01.svg.png" alt="graph - wikipédia - GNU" style="zoom:67%;" />



Le tableau ci-dessous permet d'appliquer l'algorithme du Dijkstra. 

Complétez le !

|  A   |     B      |     C      |     D      |     E      |     F      |     G      |     H      |     I      |     J      | Choix |
| :--: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: | :--------: | :---: |
|  0   | oo | oo | oo | oo | oo | oo | oo | oo | oo | A(0)  |
|  x   |  **85_A**  |   217_A    | oo|   173_A    | oo | oo |oo | oo | oo | B(85) |
|  x   |            |            |            |            |            |            |            |            |            |       |
|  x   |            |            |            |            |            |            |            |            |            |       |
|  x   |            |            |            |            |            |            |            |            |            |       |
|  x   |            |            |            |            |            |            |            |            |            |       |
|  x   |            |            |            |            |            |            |            |            |            |       |
|  x   |            |            |            |            |            |            |            |            |            |       |
|  x   |            |            |            |            |            |            |            |            |            |       |
|  x   |            |            |            |            |            |            |            |            |            |       |



>  Quel est le chemin le plus court de A vers J ?
>
> ```txt
> 
> ```
>
> 



Là où cet algorithme est vraiment fort c'est qu'on peut également trouver le chemin le plus court de la ville A vers n'importe quelle autre ville :



> Quel est le chemin le plus court de A vers G ?
>
> ```txt
> 
> ```
>
> 
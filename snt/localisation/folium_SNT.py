# -*- coding: utf-8 -*-
import math, folium, os, webbrowser



def nmea_to_liste(fichier = 'nmea.txt'):
    '''
    renvoie une liste à partir d'un fichier NMEA.
    param fichier NMEA au format txt par défaut 'nmea.txt'
    type str
    return une liste du type [ [Type de trame, Heure d'envoi, Latitude, ..], [Type de trame, Heure d'envoi, Latitude, ..], ...]
    type liste
    '''
    mon_fichier = open(fichier, 'r', encoding= "utf8")
    lignes = mon_fichier.readlines()
    liste_trames = []
    for ligne in lignes:
        ligne = ligne.replace('\n','')
        ligne = ligne.replace('$','')
        liste = ligne.split(',')
        liste_trames.append(liste)
    return liste_trames
        
def type_trame(fichier = 'nmea.txt'):
    '''
    renvoie la liste des constellations présentes dans le fichier nmea
    param fichier NMEA au format txt par défaut 'nmea.txt'
    type str
    return liste de constellations
    type list
    '''
    liste_trames = nmea_to_liste(fichier)
    liste_constellations=[]
    for trame in liste_trames :
        if trame[0][:2] == 'GP' and not 'GPS_USA' in liste_constellations :
            liste_constellations.append('GPS_USA')
        elif trame[0][:2] == 'GA' and not 'Galliléo' in liste_constellations :
            liste_constellations.append('Galliléo')
        elif (trame[0][:2] == 'BD' or trame[0][:2] == 'GB')and not 'Beidou_Chine' in liste_constellations :
            liste_constellations.append('Beidou_Chine')
        elif trame[0][:2] == 'GL' and not 'Glonass_Russie' in liste_constellations :
            liste_constellations.append('Glonass_Russie')
    return liste_constellations
            
def trame_to_coordonnees(liste_trames):
    '''
    renvoie une liste de tuple de coordonnées du type [ (lat, long), (lat, long) ...]
    param liste_trames : une liste de trames NMEA
    type list
    return liste de coordonnées
    type list    
    '''
    liste_coordonnees = []
    for trame in liste_trames :
        if trame[0][2:] == 'GGA' and trame[2] != '' and trame[4] != '': # seules les trames GGA non vides sont prises en compte ici
            lat = int(trame[2][:2]) + int(trame[2][2:4]) /60 + int(trame[2][5:])/360000
            long = int(trame[4][:3]) + int(trame[4][3:5]) /60 + int(trame[4][6:])/360000
            liste_coordonnees.append((lat, long))
    return liste_coordonnees


def nmea_to_map(fichier = 'nmea.txt', nom_carte = 'carte'):
    '''
    genere un fichier HTML affichant une carte et un itinéraire selon le fichier source de type nmea
    param fichier un nom de fichier nme au format txt
    param non_carte le nom de la carte
    type str
    '''
    trames = nmea_to_liste(fichier)
    coordonnees = trame_to_coordonnees(trames)
    creer_carte(coordonnees, nom_carte)
    


def distance (depart, arrivee) :
    """
    Calcul la distance en km entre depart et arrivee
    :param depart: liste ou tuple de coordonnées de géoloc, (lat, long) ou [lat, long]
    :param arrivee: idem pour ville d'arrivée
    :return: float
    :CU: coordonnées en degrés décimaux 
    >>> distance([50.1234, 1.1234], [50.6789, 1.6789])
    73.24930106281151
    """
    # conversion des données en radians
    lat1, lat2 = math.radians(depart[0]), math.radians(arrivee[0])
    long1, long2 = math.radians( depart[1]), math.radians(arrivee[1])
    # calcul de la distance en km
    d = math.acos(math.sin(lat1) * math.sin(lat2) + math.cos(lat1) * math.cos(lat2) * math.cos(long2-long1)) * 6371
    return d

def distance_totale(fichier = 'nmea.txt'):
    '''
    renvoie la longueur du parcours contenu dans le fichier txt au format nmea
    param fichier
    type string
    return la longueur en km
    type float
    '''
    distance_totale = 0
    liste_trames = nmea_to_liste(fichier)
    liste_coordonnees = trame_to_coordonnees(liste_trames)
    for i in range(len(liste_coordonnees) - 1):
        distance_totale += distance(liste_coordonnees[i], liste_coordonnees[i + 1])
    return distance_totale

def temps_total(fichier = 'nmea.txt'):
    '''
    renvoie la durée d'enregistrement en seconde du fichier 
    param fichier
    type string
    return le temps d'enregistrement en heure
    type float
    '''
    liste_trames = nmea_to_liste(fichier)
    horaire = liste_trames[0][1] #horaire de depart
    h_depart = int(horaire[:2])
    min_depart = int(horaire[2:4])
    sec_depart = float(horaire[4:])
    horaire = liste_trames[len(liste_trames) - 1][1] #horaire de fin
    h_fin = int(horaire[:2])
    min_fin = int(horaire[2:4])
    sec_fin = float(horaire[4:])
    return h_fin + min_fin / 60 + sec_fin / 3600 - (h_depart + min_depart / 60 + sec_depart / 3600)




def creer_carte(circuit, nom_carte = 'carte'):
    """
    place des points sur un carte au sein du page html, sauvegardée ensuite. 
    :param circuit: (list) liste de coordonnées données sous forme de tuple
    :param nom_page_html:(str) nom de la page html (sans extension)
    """
    carte = folium.Map(location = circuit[0],zoom_start = 14)
    folium.PolyLine(circuit, color="blue", weight=5, opacity=0.8).add_to(carte)
    carte.save(nom_carte + '.html')
    dossier_courant = os.getcwd()
    webbrowser.get('firefox').open_new_tab(dossier_courant + '/' + nom_carte + '.html' )



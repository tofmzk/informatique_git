# Où suis-je ?



### En forêt.



Le principal instrument de localisation, GPS (_Global Positioning System_), a été conçu par l’armée américaine dans les années soixante. Le premier satellite GPS sera lancé en 1978.

En 1983, quand un vol de la _Korean Airlines_ dévie de sa trajectoire dans l’espace aérien russe et est abattu par un chasseur soviétique, le président Reagan demande à ce que le  système de navigation GPS soit autorisé pour un usage civil, afin que de telles erreurs de navigation, et ses conséquences dramatiques, ne soient plus possibles. Cependant le premier récepteur _GPS_  _transportable_ n'arrivera qu'en 1985 et leur utilisation ne se démocratisera vraiment qu'à partir des années 2000 où on les verra apparaître dans les voiliers de plaisance, les voitures, puis dans les téléphones portables.



Mais nous sommes dans les années 80 et je suis perdu en pleine forêt, sans téléphone portable et sans GPS. 

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Sous-bois_de_la_for%C3%AAt_de_Bannoncourt.JPG/800px-Sous-bois_de_la_for%C3%AAt_de_Bannoncourt.JPG" alt="perdu - wikipedia - CCA" style="zoom: 50%;" />



Heureusement, dans mon sac à dos, j'ai une carte papier, sur laquelle on aperçoit quelques endroits stratégiques , notamment, trois clochers, et une montre à mon poignet.



<img src="media/carte.jpg" alt="petite carte" style="zoom:50%;" />



Je sais qu'à midi pile, les clochers sonneront, et justement, midi approche et je tends l'oreille :

* à 12 h 00' 23 s,  le son du clocher au nord-ouest parvient jusqu'à moi.
* 3 secondes plus tard, c'est celui du nord-est qui résonne.
* encore 3 secondes plus tard, le dernier clocher se joint aux deux autres.



> Sachant que le son se propage à 340 m/s, expliquez comment me localiser et quels sont les éléments indispensables pour y réussir.

```txt

```





> Ouvrez le fichier Géogébra [ci-joint](carte.ggb) et localisez moi. 



> * De quoi dépend la précision de la localisation ?

```txt

```



## Les systèmes de localisation 

 			

Il y en a actuellement une trentaine de satellites de géolocalisation positionnés de sorte à ce qu’à tout moment, quatre à six satellites au moins sont visibles depuis tout point de la Terre. 

![les sattelites - source wikipédia - Domaine public](https://upload.wikimedia.org/wikipedia/commons/9/9c/ConstellationGPS.gif)

Couplés aux cartes numériques, des systèmes GPS, comme [GLONASS](https://fr.wikipedia.org/wiki/GLONASS) russe,  la Chine et son système [Beidu ou COMPASS](https://fr.wikipedia.org/wiki/Beidou) ou  celui de l’Inde et du Japon, permettent de se situer. Il n’est pas toujours efficace en ville ou en forêt, et peut être complété par d’autres moyens de localisation comme la détection de bornes Wi-Fi proches. D’autres systèmes plus précis, dont [Galileo](https://fr.wikipedia.org/wiki/Galileo_(syst%C3%A8me_de_positionnement)), qui repose sur une constellation de 30 satellites, placés  en orbite à 23.000 km de la Terre, sont en cours de déploiement.



<img src="https://upload.wikimedia.org/wikipedia/commons/c/c7/Glonass-receiver.jpg" alt="recepteur GLONASS - wikipedia - domaine public" style="zoom:50%;" />





Comment ça marche ?

 Un peu comme notre localisation en forêt... 



[Une petite vidéo pour comprendre le fonctionnement du système Galiléo](https://youtu.be/e79tSIpLiDk)



> Pour voir si vous suivez :
>
> 
>
> * Combien de satellites  sont nécessaires pour une localisation précise ?
>
> 	````
> 	
> 	````
>
> 	
>
> * A quoi sert le dernier satellite ?
>
> 	````
> 	
> 	
> 	````
>
> 	
>
> * A quelle vitesse se déplace le signal émis par un satellite ?
>
> 	````
> 	
> 	
> 	````
>
> 	
>
> * A quoi servent les antennes sur terre ?
>
> 	````
> 	
> 	
> 	
> 	````
>
> 	 
>
> * Pourquoi la mesure du temps est-elle primordiale ?
>
> 	```
> 	
> 	
> 	
> 	```
>
> 	_Remarque : le TP mesure du temps, étudie cela en détails ..._





Il existe d'autres [systèmes pour se localiser](https://fr.wikipedia.org/wiki/G%C3%A9olocalisation). Différentes méthodes sont parfois utilisées et combinées pour affiner le calcul de la position :

* grâce aux antennes _GSM_, les antennes relais des téléphones portables.
* selon le réseau _WIFI_ que vous captez.
* grâce à votre adresse _IP_.
* en utilisant des puces _RFID_.



## Latitude et longitude.



Nous avons l'habitude de visualiser notre position directement sur une carte électronique numérisée qui, grâce aux coordonnées envoyées par nos GPS, nous positionne directement sans que nous ayons besoin de connaître comment l'application associée procède.



Mais on veut savoir ! Non ? Si si !



Le GPS renvoie une _trame_ , appelée trame `NMEA` que nous étudierons plus tard, qui contient, entre autres, nos coordonnées géographiques, la latitude et la longitude.

* La latitude est donnée en degré par rapport à l'équateur. Le pôle nord est à 90°N et le pôle sud à 90°S.

* La longitude est donnée en degré par rapport au méridien de référence, le célèbre [méridien de Greenwich](https://fr.wikipedia.org/wiki/M%C3%A9ridien_de_Greenwich) à partir duquel on se place soit à l'est soir à l'ouest.

	

![lattitude et longitude - wikipédia- domaine public](https://upload.wikimedia.org/wikipedia/commons/b/bc/FedStats_Lat_long.png)



* Lorsqu'on connaît sa latitude et sa longitude, on peut se repérer sur Terre :

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Latitude_and_longitude_graticule_on_an_ellipsoid.svg/660px-Latitude_and_longitude_graticule_on_an_ellipsoid.svg.png" alt="lattitude et longitude - wikipédia - domaine public" style="zoom:50%;" />





* Regardez la carte ci-dessous. Sous quelles latitudes et longitudes se situe la France ? La Nouvelle Zélande ?

![carte du monde - wikipedia - CCA](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Longitude_and_latitude_definition.gif/800px-Longitude_and_latitude_definition.gif)



* Des cartes numériques, comme [Geoportail](https://www.geoportail.gouv.fr/) , fournissent de nombreuses informations.

  * Allez y faire un tour afin de trouver les coordonnées géographiques du lycée. 

  ```txt
  Lattitude :
  Longitude :
  ```

  * Quelles autres type de données peut-on y trouver ?

  ````
  
  
  
  
  
  
  
  ````

  

* Il existe également des cartographie gratuite, crée de façon collaborative et sous licence libre. C'est le cas d'[OpenStreetMap](https://www.openstreetmap.fr/). Il est possible de s'y inscrire et de contribuer à la création d'une carte en y ajoutant des données.







_______

Licence CC-BY-SA

Mieszczak Christophe, formation académique SNT de l'académie de Lille






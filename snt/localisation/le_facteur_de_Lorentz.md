

# Le facteur de Lorentz



# Mesurer le temps

La précision des horloges qui mesurent le temps sur terre et dans les satellites est cruciale.



Le signal du satellite voyage à 300000 km/s. A l'instant où il l'envoie, le satellite incorpore au message l'heure précise, selon sa propre horloge atomique, à laquelle ce message est parti. Lorsque ce message est capté sur terre, on calcule l'écart entre l'heure d'émission et l'heure de réception et on en déduit la distance au satellite. Nous avons vu qu'avec 3 distances de ce genre, on pouvait se localiser sur terre.



Imaginons que nous nous servons d'une montre précise à 1 ms près soit 0,001 s .

Grâce à ce type de montre, on mesure qu'un signal arrive sur terre en 0,06 s. En réalité, la précision de la montre nous permet d'encadrer le temps du voyage du signal dans [0,059; 0,061].



On rappelle la célébrissime formule liant la vitesse, le temps et la distance, toutes trois exprimées avec des unités cohérentes : $`d = v \times t`$.



> En Python, réalisez une fonction `distance`, de paramètre _temps_, un temps en seconde, qui renvoie la distance à laquelle se situe le satellite en km. 
>
> ```python
> import math
> C = 300000000 # celerité de la lumière en m/s
> 
> def distance(temps):
>     '''
>     envoie la distance à laquelle se situe le satellite en km
>     param temps : un temps en secondes
>     type float
>     return la distance en km
>     type float
>     '''
>     
> ```
>
> 



Dans quel intervalle de distances se situe notre satellite ?

```python
>>> distance(0.059)
???
>>> distance(0.061)
???
```



Qu'en pensez vous ?





# A bicyclette ?



Le [facteur de Lorentz](https://fr.wikipedia.org/wiki/Facteur_de_Lorentz) ne distribue pas le courrier. 



Il s'agit d'un paramètre, noté $`\gamma`$, qui intervient dans de nombreuses formules de la relativité restreinte d'un certain Einstein, Albert de son prénom. 



Imaginons un observateur terrestre immobile qui regarde passer une fusée très très rapide, traversant le ciel à une vitesse $`v`$. Simultanément, l'observateur et le pilote de la fusée déclenchent un chronomètre très précis. Tandis que, sur terre, le chronomètre mesure un laps de temps $`\Delta t`$ , la vitesse _dilate_ le temps sur la fusée et son pilote mesure un temps $`\Delta \tau`$. 



Lorentz a démontré que $`\Delta t = \gamma . \Delta \tau`$ avec  $` \gamma =\frac {c}{\sqrt{c² - v²}}`$ où $`c`$ et la célérité de la lumière et $`v`$ la vitesse de la fusée, tous les deux en _m/s_.



> En Python, réalisez une fonction nommée `lorentz` de paramètre _v_, une vitesse  en km/h, qui renvoie la valeur du facteur de Lorentz. On pensera à convertir _v_ en _m/s_ .
>
> ```python
> def lorentz(v):
>  '''
>  renvoie le facteur de Lorentz correspondant à une vitesse v
>  param v : la vitesse en km/h
>  type int
>  return le paramètre de Lorentz
>  type float
>  '''
> 
> ```
>
> 

 

Un satellite de géolocalisation se déplace à environ 15000 km/h. Un vélo à 30 km/h. Quel est le facteur de Lorentz dans chaque cas ? Qu'en pensez-vous ?

```python
>>> lorentz(15000)
???
>>> lorentz(30)
???
```



> En Python, réalisez une fonction `decalage` de paramètre _temps_, un temps en seconde, et _v_,  une vitesse en km/h, qui renvoie le décalage induit par le facteur de Lorentz entre l'horloge de l'observateur et celle de l'objet qui se déplace à la vitesse _v_ pour le laps de temps _temps_.
>
> ```python
> def decalage(temps, v):
>     '''
>     renvoie le décalage induit par le facteur de Lorentz entre l'horloge de 
>     l'observateur et celle de l'objet qui se déplace à la vitesse _v_ pour le laps de 
>     temps _temps_
>     param v : la vitesse en km/h
>     type float
>     param temps : le laps de temps en secondes
>     type float
>     return le décalage de temps
>     type float    
>     '''
> ```
>
> 



Sur une journée complète, de combien l'horloge d'un satellite va se décaler ? Sur un an ? Sur les 52 ans écoulés depuis le lancement du premier satellite de géolocalisation ? 

```python
>>> 24 * 3600
???
>>> decalage(? , ?)
???
```



A quels erreurs de distance cela conduirait-il si on ne tenait pas compte du facteur de Lorentz (n'oubliez pas que le résultat est en km...) ?

```python
>>> d = decalage(?, ?)
>>> distance(d)
???
```



_______

Licence CC-BY-SA

Mieszczak Christophe, formation académique SNT de l'académie de Lille






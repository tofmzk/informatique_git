# Trames NMEA



# Avec le portable

_Près-requis :_

Faire installer l'application gratuite _NMEATools_ sur les smartphones qui le peuvent. Quelques smartphones suffiront pour une classe. Il s'agit, comme son nom l'indique, d'un outils pour enregistrer et visualiser des trames NMEA.



On emmène les élèves dans la cour, gps du portable activé. On lance l'application _NMEATools_.

<img src="media/menu.jpg" alt="menu - perso"  />



Dans le menu, on choisit _NMEA Enregistreur_ est on regarde les trames défiler en bas de l'écran :

<img src="media/enregistrement.jpg" alt="trames - perso"  />



Une trame NMEA est . En stoppant l'enregistrement, vous pouvez l'enregistrer au format _txt_  puis retourner dans le menu principal. En choisissant _NMEA viewer_ vous pouvez visualiser ces trames.

<img src="media/les_trames.jpg" alt="les trames - perso"  />



> Chez vous, ou au lycée, récupérez le fichier txt de votre téléphone sur une clé usb. On s'en servira plus tard.



# Trames NMEA



Les trames NMEA sont constituées d'une succession de données séparées par des virgules. Par exemple :

_$GPGGA,165704.083,5073.8879,N,00225.6084,E,1,10,0.8,41.9,M,47.1,M,,*54_



Chaque donnée a un rôle bien prècis :

| Type de trame|Heure d'envoi|Latitude|Nord/Sud | Longitude|Est /Ouest| qualité| Nb satellites| Précision| Altitude|Unité|Réservé au contrôle|
|:-------:|:-------:|:-------:|:-----:|:-----:|:------:|:------:|:------:|:---:|:----:|:--:|:----------:|
|     GPGGA  |   165704.083  | 5073.8879|     N   | 00225.6084|  E|   1|10|  0.8| 41.9| M |     47.1,M,,*54   |



* le _$_ marque le début d'une trame.

* Concernant le _Type de trame_ :

    * les deux premiers caractères indiquent la constellation de satellites qui a servi a envoyer la trame.

        * GP pour GPS (USA)  

        + GA pour Galiléo (Europe) 
        + BD ou GB pour Beidou (Chine) 
        + GL pour Glonass (Russie)  

    * Les trois caractères suivants servent à caractériser la trame, on s'intéresse plus précisément à :

        * GGA : trame courante utilisée pour le positionnement terrestre. 
        * RMC, trame utilisée pour le positionnement maritime que  nous ne détaillerons pas ici. 

*  Le format de l'heure d'envoi est HHMMSS,SSS  . La précision est donc la milliseconde. Attention, il s'agit de l'heure `UTC`. 

*  Latitude, Longitude. 

    * Les coordonnées sont exprimées en <u>dixièmes</u> de degrés décimaux à 5 décimales. Dans l'exemple, le point est situé à:
        * 50,738879 degrés de latitude Nord 
        * 2,256084 degrés de longitude Est. Le point de strouve donc \"à gauche\" du méridien de Greenwich\". 

* L'indicateur de qualité est difficilement exploitable, il apporte des renseignements assez techniques( positionnement fixe du satellite etc...).  

* Le nombre de satellites visibles : plus celui-ci est élevé et plus la mesure a des chances d'être précise. En intérieur, en montagne (vallée très encaissée) ou en ville (ruelles étroites), il est possible qu'il y ait très peu voire aucun satellite visible.

* La précision : dans l'exemple, elle est de 0,8m. Le positionnement est donc assez fiable 

*  Altitude : dans l'exemple, l'altitude est de 41.9 mètres, 

* unité : ici le mètre, prècise l'unité des 3 grandeurs précédentes.

*  Le reste de la trame ne nous intéresse pas, il comporte des caractères permettant le contrôle de l'ensemble des informations présentes dans la trame pour permettre la détection d'éventuelles erreurs de transmission.

> Reprenez le fichier _txt_ que vous avez enregistrer sur votre smartphone :
>
> * Quelles constellations de satellite vous a envoyé une trame ?
> * Combien de satellites ont communiqué avec vous ?
> * Quelle est la précision du positionnement ?
> *  A quelle heure a été reçue votre première trame ?
> * A quelle altitude étiez vous ?
> * Quelles étaient vos coordonnées  ? 



Les GPS utilisées pour les randonnées sauvegardent souvent ces trames sous un autre format ne conservant que les informations dont ils ont besoin : heure, coordonées et altitude ....



Il existe différents formats, tous des fichiers textes organisés de différentes façon. Le plus courant est le format [gpx](https://fr.wikipedia.org/wiki/GPX_%28format_de_fichier%29) mais  il en existe de nombreux autres comme le [KML](https://fr.wikipedia.org/wiki/Keyhole_Markup_Language) utilisé par _google earth_, l'_ASC_ de _Michelin_, l'_OV2_ de _TomTom_ et même une vieille connaissance, le _csv_ .



Pour en savoir davantage, rendez vous [ICI](http://www.gpspassion.com/forumsen/topic.asp?TOPIC_ID=17661)



# Python et Folium



Le module _folium_ de Python permet de visualiser et positionner sur des cartes des lieux localisés par leur lattitude et longitude. On peut positionner un unique endroit, un grand nombre ou même un itinéraire. 

Ci-joint, un fichier Python `folium_SNT.py`. Le code Python est clairement hors programme en seconde. On y travaille sur des listes, des tuples et des chaines de caractères. En revanche, il est possible d'utiliser directement les fonctions ci-dessous :

* `type_trame(fichier)` renvoie une liste des constellations de satellites qui ont envoyé des trames. Si le fichier n'est pas précisé, le fichier `nmea.txt`fourni est utilisé.
* `nmea_to_map(fichier, nom_carte)`génére une page HTML nommée `nom_carte.html` et contenant une carte sur laquelle apparaissent les points dont les coordonnées sont précisées dans la trame NMEA codée dans le fichier. Par défaut, `nmea.txt` est chargée et le fichier est nommé `carte.html`. Firefox se lance, s'il est installé, et ouvre le fichier créé.
* `distance_totale(fichier)` renvoie la distance totale en _km_ du parcours codé dans le fichier passé en paramètre (`nmea.txt` par défaut).

* `temps_total(fichier)` renvoie la durée en heures du parcours codé dans le fichier passé en paramètre (`nmea.txt` par défaut).



> En utilisant `folium_SNT.py` , reprenons le fichier _txt_ enregistré avec _NMEATools_ (ou prenez le fichier `nmea.txt` fourni.
>
> * Quelle est la distance totale parcourue ?
> * Combien de temps a duré l'enregistrement ?
> * Quelle était votre vitesse moyenne en _km/h_ ?
> * afficher votre trajet sur une carte.



# Enregistrement et visualisation de parcours

Il existe de nombreux sites et applications permettant d'enregistrer directement un parcours via un recepteur GPS ou de convertir les trames d'un format à un autre



Voici quelques sites gratuits, souvent collaboratifs :

* [randoGPS](https://www.randogps.net/) , [trace GPS](http://www.tracegps.com/), [utagawa](https://www.utagawavtt.com/)

* [VisuGPX](https://www.visugpx.com/recherche/) 

* [GPS visualizer](https://www.gpsvisualizer.com/convert_input)

    

Des réseaux sociaux  :

* [Strava](https://www.strava.com/) , [openrunner](https://www.openrunner.com/) ...









____________________________________

Licence CC-BY-SA

Mieszczak Christophe, formation académique SNT de l'académie de Lille






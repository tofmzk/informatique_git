# Connectés



Beaucoup les objets que nous utilisons au quotidien sont  désormais « connectés ». Cela signifie qu'ils font partie du réseau des réseaux : internet. Ils possèdent, pour une grande partie leur propre page web au point que ces dernières sont déjà plus nombreuses que celles créées par les humains. On estime même que ces objets pourraient  atteindre un nombre estimé entre 50 et 80 milliards très rapidement.



_Remarque :_

Le roi des objets connectés est votre smartphone. Certains objets dits "connectés" ne le sont pas réellement : ils ne se connectent pas directement à internet mais via votre téléphone en passant par le bluetooth.



> 
>
> Listez des objets connectés que vous utilisez chaque jour et des objets dits connectés qui ne le sont pas vraiment.
>
> 



Tous ce objets connectés sont un apport indiscutable qui nous aide au quotidien. Mais, il faut tout de même les utiliser avec raison car, comme toute avancée technologique, ils apportent du bon et du mauvais. 



Ces appareils nous simplifient la vie de bien des façons au point qu'il devient difficile d'envisager de s'en passer : 

* Ils permettent de communiquer plus efficacement que n'importe quelle invention de n'importe quelle époque
* Ils nous donnent accès à une somme de connaissances colossale instantanément depuis n'importe quel endroit (ou presque)
* ils nous assistent quotidiennement au lycée, au travail ou dans nos loisirs... 
* Leur conception et maintenance ont créé de nouvelles branches professionnelles qui recrutent sans cesse. 

Oui, mais 

* leur construction est polluante, utilise des métaux rares, des parties non recyclables, une importante quantité d'énergie et de ressource. 
* Leur utilisation consomme de l'énergie, beaucoup car ils sont très nombreux !
* Ils peuvent collecter vos données privées si vous n'y prenez garde et peuvent vous même espionner à votre insu. 
* Certains d'entre eux rendront obsolètes des quantités de professions.
* Ils peuvent vous rendre dangereusement accros ...



> * Comment limiter l'impact environnemental des objets connectés ? 
> * Quels types de métiers sont créés grâce à eux ? Lequels risquent de disparaitre par leur faute ?
> * Donner des exemples quotidiens de leurs utilités et de leurs dangers. 





________

Par Mieszczak Christophe Licence CC BY SA





















________

Par Mieszczak Christophe

licence CC BY SA
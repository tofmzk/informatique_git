# Interface Homme - Machine



### introduction



Une  IHM (**I**nterface **H**omme - **M**achine) permet à un utilisateur d'interagir avec une machine. La souris et  le clavier sont des exemples d'IHM, les écrans tactiles, et les manettes de jeux également et il en existe beaucoup d'autres !

Par _machine_, on entend bien évidemment _ordinateur_, mais pas seulement. En effet, les _machines_ peuvent  aussi être des **systèmes embarqués**, c'est à dire des systèmes qui embarquent avec eux de l'informatique et des algorithmes, comme les robots, ou des **objets connectés**, c'est à nécessitant d'être _branchés_ sur un réseau et inter-agissant avec lui, comme les appareils domotiques (les réfrigérateur, les thermostats connectés ou les alarmes, par exemple) .



_Remarque :_

On abuse souvent de l'appellation _objet connecté_ pour beaucoup d'objets qui n'en sont pas car ils ne sont que rarement branchés sur un réseau et n'interagissent que peu, ou pas du tout, avec lui.

Par exemple, la plupart des montres connectées font plutôt parties des _systèmes embarqués_ car elles ne nécessitent pas une connexion permanente au réseau. Seules celles qui doivent être connectées au smartphone pour pleinement fonctionner sont réellement connectées.



> Citez des exemples de soit disant objets connectés qui n'en sont pas :
>
> ````txt
> 
> ````
>
> 



### Constituants d'une IHM



Les constituants essentiels des ces objet sont les `capteurs`et le `actionneurs` : 		

- Les `capteurs` sont des composants qui envoient des informations aux programmes en
	convertissant des grandeurs physiques en une donnée exploitable par le programme. Les informations envoyées sont des entrées pour le programme.

	

- Les `actionneurs` sont des composants qui modifient le comportement (sortie) du système embarqué en transformant les informations reçues par le programme.	



![capteurs et actionneurs - source perso](media/capteurs_actionneurs.jpg)





> Prenons l'exemple d'un thermostat connecté.
>
> * Quels sont ses capteurs ?
>
> 	````txt
> 	
> 	````
>
> * Quels sont ses actionneurs ?
>
> 	````txt
> 	
> 	````
>
> 	









___________

Par Christophe Mieszczak Licence CC BY SA

Images source : production personnelle.


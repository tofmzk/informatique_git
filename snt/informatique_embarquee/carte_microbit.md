# La carte _micro:bit_



## Présentation



La carte _micro:bit_ est une carte embarquée qui peut, mais ce n'est pas vraiment ce pourquoi elle est conçue,  faire office d'objet connecté dans certain cas. Elle a été conçue dans un objectif pédagogique.

![	](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/BBC_Microbit.jpg/713px-BBC_Microbit.jpg)

Pour avoir les informations complètes sur cette carte, aller [visiter cette page du site web micro:bit](https://archive.microbit.org/fr/guide/features/).



> * Quels sont les `capteurs`de cette carte ?
>
> 	````txt
> 																
> 	````
>
> 	
>
> * Quels sont les `actionneurs` de cette carte ?
>
> 	```` txt
> 	
> 	````
>
> * Par quel(s) moyen(s) cette carte peut-elle devenir un objet connecté et faire partie d'un réseau ?
>
> 	````python
> 																
> 	````
>
> 	 	



## micro:bit et micro-Python

La carte _micro:bit_ se programme en Python, ou plutôt une version de Python dédiée au micro-contrôleurs, le microPython. On peut utiliser un [l'editeur micro:bit](https://python.microbit.org/v/2.0) en ligne comme, un éditeur comme _Thonny_ ou même depuis un téléphone. Nous utiliserons la seconde solution. 



* Branchez votre carte sur un port _usb_.
* Lancer _Thonny_. 
* En suivant le menu _Aide/A propos de Thonny_, vérifiez que vous utilisez une version au moins égale à la 3.2 sinon, mettez votre logiciel à jour avant de continuer.
* Suivez le menu _Outils/Options_ puis sélectionnez la languette _Interpréteur_.
* Dans le menu déroulant, choisissez l'interpréteur _MicroPython (BBC micro:bit)_. Remarquez qu'il faudra penser à repasser sur l'interpréteur par défaut pour exécuter un code _normal_.
* Pour terminer, dans le menu déroulant juste en dessous, sélectionnez l'emplacement de la carte : _embed serial port_.



Thonny est prêt, reste à créer un programme. Sauvegardez le sur la carte _micro:bit_ sous le nom `les_bases.py`.

La première chose à faire est d'importer la bibliothèque `microbit` . Pour simplifier l'écriture du code nous allons importer le module ainsi :

````python
from microbit import *
````



#### REMARQUE : SIMULATEUR EN LIGNE

​	SI vous ne disposez pas d'une carte _micro:bit_, vous pouvez utiliser un simulateur en ligne comme celuis 	que vous trouverez [ICI](https://create.withcode.uk/) :

* Importez le module `microbit`.
* Exécutez le code en cliquant sur la flèche en bas à droite de l'écran.



## Utilisons les actionneurs



### Affichage

Il y a plusieurs façon d'afficher des choses sur les 25 LEDS de la cartes en utilisant l'objet `display`qui possède plusieurs méthodes :

* la méthode `clear`eteint toute les LEDs. On l'appelle ainsi :

  ````python
  from microbit import *
  display.clear()
  ````

  

* la méthode `scroll`permet de faire défiler du texte, c'est à dire une chaîne de caractères. Testez :

  ```python
  from microbit import *
  display.clear()
  display.scroll('Hello World !')
  ```
  
* La méthode `show`permet d'afficher un caractère (et d'autres choses comme nous le verrons plus loin). Testez :

    ```python
    from microbit import *
    display.clear()
    display.show('5')
    ```

    

* Bien entendu, on peut utiliser une variable du type `str` (chaîne de caractères). Testez :

    ```python
    from microbit import *
    display.clear()
    i = '5'
    display.show(i)
    ```

* On peut également utiliser une variable de type numérique (`int` pour entière ou `float` pour  _décimale_) mais il faudra la convertir en chaîne de caractères (`str`)  pour utiliser la méthode `show`. Testez :

    ```python
    from microbit import *
    display.clear()
    i = 5 # il s'agit d'une variable de type int (entière)
    display.show(str(i)) # str(i) convertit l'entier a en chaîne utilisable avec show
    ```
    
    

> Réaliser un algorithme affichant un compte à rebours de 5 à 0.
>
> * On utilisera la méthode `sleep(temps)`  , où _temps_ est exprimé en millisecondes, qui fait une pause dans l'exécution du code durant le délais demandé. L'instruction `sleep(1000)` fera _dormir_ le programme une seconde.
>
> * On utilisera une boucle `pour`  dont la structure est la suivante :
>
>     ```python
>    for i in range(n) : # réalise uneboucle de n tours.
>         #ci-dessous on place le code de la boucle après une tabulation
>    
>     # à la fin de la boucle on retire la tabulation
>    ```
>    
> * Une fois terminé, coller votre code ci-dessous :
>
>     ```python
>                                                     
>     ```
>
>     
>
>     
>



* La méthode `show`permet aussi d'afficher une image. 

	* Certaines sont déjà définies, comme `Image.HAPPY` utilisée ci-dessous. Vous en trouverez la liste [ICI](https://microbit-micropython.readthedocs.io/en/latest/tutorials/images.html). Testez en quelques unes :

	  ``` python
	  from microbit import *
	  display.show(Image.HAPPY)
	  ```
	  
	* Vous pouvez également définir vos images vous même. Chaque LED pouvant être allumée avec une intensité allant de 0 (éteinte) à 9 (intensité maximale). Voici un joli bateau :
	
	    ```python
	    from microbit import *
	    image =  Image("05050:"
                     "05050:"
	                   "05050:"
                     "99999:"
	                   "09990")
	    display.show(image)
	    ```
	
	    
	
	

>  
>
>  Modifier l'image pour qu'elle représente un objet de votre choix.
>
>  Une fois terminé, coller votre code ci-dessous :
>
>  ```python
>  ```
>
>  
>
>  



Un petit programme : le lancé de dé



> Nous allons utiliser la carte pour simuler le lancé d'un dé :
>
> * Codez 6 images pour qu'elles représentent chacune une face d'un dé, et ainsi, les numéros de 1 à 6 et nommez les _un_, _deux_, _trois_ ...
>
> * importer le module `random` afin de pouvoir choisir un nombre aléatoire en ajoutant en haut du code, à la suite de `from microbit import *` la ligne ci-dessous :
>
>     ```python
>     from random import *
>     ```
>
>     
>
> * Choisissez un nombre entre 1 et 6 de la façon suivante : 
>
>     ```python
>     nbre = randint(1, 6)
>     ```
>
> * Selon la valeur de _nbre_, afficher l'image correspondante. On utilisera une structure conditionnelle.
>
> 



* La méthode `display.set_pixel(x, y, intensite)` permet de fixer, entre 0 (éteinte) et 9 (intensité maximale) la luminosité de la LED de coordonnées (x, y). Les LEDs sont repérées de la façon suivante :

  ![micro:bit - wikipédia CC BY SA auteur : [Ugopedia](https://commons.wikimedia.org/w/index.php?title=User:Ugopedia&action=edit&redlink=1)](https://upload.wikimedia.org/wikipedia/commons/c/cb/Micro_bit_position_des_DEL.png)

  Ainsi, pour faire clignoter deux fois la deuxième LED à une seconde d'intervalle, le code est le suivant. Testez le et 

  ```python
  from microbit import *
  nbre_de_fois = 2
  delais = 1000
  for i in range(nbre_de_fois) : # pour i allant de 0 à nbre_de_fois - 1
  	display.set_pixel(1, 0, 9)# on allume la LED
  	sleep(delais)# on attend le délais précisé
  	display.set_pixel(1, 0, 0)# on éteind la LED
  	sleep(delais)# on attend le délais précisé
  ```

>   
>
>   Modifier ce code afin de faire clignoter la LED centrale de la carte 10 fois en une seconde.	
>
>     Une fois terminé, coller votre code ci-dessous :
>   
>     ```python
>     ```

  

Voici maintenant une série de petits défis. Vous n'êtes pas obligés de tous les réaliser. Saurez vous y faire face ?

 

**Exercice 1:**

> 
>
> Réaliser un algorithme qui allume une LED à la fois un certain temps puis l'éteint de façon à donner l'impression que la lumière allumée parcours la première ligne de LED.
>
> Nous allons utiliser une structure de boucle de type `for`  où l'indice de boucle sera l'abscisse de la LED.
>
> Une fois terminé, coller le code ci-dessous :
> 
>```python
> ```
>
> 



**Exercice 2:**


> Réaliser un algorithme qui allume une LED à la fois un certain temps puis l'éteint de façon à donner l'impression que la lumière allumée parcours la dernière colonne de LED.
>
> 
>
> Une fois terminé, coller le code ci-dessous :
>
> ```python
>```

**Exercice 3:(\*)**


> Réaliser un algorithme qui allume une LED à la fois un certain temps puis l'éteint de façon à donner l'impression que la lumière allumée fait le tour de l'_écran_.
>
> Une fois terminé, coller le code ci-dessous :
>
> ```python
>```

**Exercice 4(\*\*):** 

> Réaliser un algorithme qui déplace un certain nombre de fois aléatoirement la LED allumée sur l'écran.
>
> Il nous faudra importer en haut du code le module proposant les méthodes liées au hasard :
>
> ```python
> from microbit import *
> from random import *
> ```
> 
>
> 
>La méthode qui renvoie un nombre aléatoire de façon équiprobable entre a et b inclus est :
> 
>```python
> nbre_alea = randint(a, b) 
> ```
> 
>
> 
>Le principe de l'algorithme est le suivant :
> 
>* La position de départ de la LED allumée est fixée à x = 2 et y = 2
> * On répète un certain nombre de fois :
>     * On choisit un nombre aléatoire
>     * S'il vaut 1, on modifie x defaçon à ce que la LEDse déplace si possible à gauche.
>     * Sinon, s'il vaut 1, on modifie x defaçon à ce que la LEDse déplace si possible à droite.
>     * Sinon, s'il vaut 3, on modifie y defaçon à ce que la LEDse déplace si possible en haut.
>     * Sinon, s'il vaut 4, on modifie y defaçon à ce que la LEDse déplace si possible en bas.
> 
> Il nous faudra donc utiliser une structure conditionnelle qui s'écrit ainsi :
> 
>```python
> if condition_1 : # si la condition_1 est vraie
>    # après une tabulation on écrit ici la conséquence 
> elif condition_2 : # sinon si la condition_2 est vraie
>    # après une tabulation on écrit ici la conséquence 
> elif condition_3 : # sinon si la condition_3 est vraie
>     # après une tabulation on écrit ici la conséquence 
> #etc .....
> else : # sinon (c'est la dernière possibilitéé)
>     # après une tabulation on écrit ici la conséquence    
> 
> ```
>
> En avant !!
>
> Une fois terminé, coller le code ci-dessous :
> 
> ```python
> ```



### Broches de connections

Il y a 25 connecteurs externes sur la tranche du _micro:bit_, que l'on nomme _broches_ ou _pins_ en anglais. on peut programmer des moteurs, des LEDs, un haut-parleur ou tout autre composant électrique à l'aide de ces broches. Il est donc possible d'ajouter l'actionneur de son choix en reliant cet actionneur, avec des pinces crocodiles par exemple, à une broche et à la terre (GND) pour fermer le circuit.

![les broches](https://microbit-micropython.readthedocs.io/en/latest/_images/pin0-gnd.png)



Nous n'allons pas développer cette partie d'avantage ici : pour une documentation complète à ce sujet, [allez-ici](https://archive.microbit.org/fr/guide/hardware/pins/). 





## Utilisons les capteurs



### Boutons

Nous disposons de deux boutons : à gauche le bouton _a_ et, à droite, le bouton _b_ modélisé par deux objets en Python, `button_a`et `button_b`.

Pour tester si on appuie sur un de ces boutons, on utilise la méthode `is_pressed()` qui renvoie _True_ ou _False_.

Testez le code ci-dessous  et complétez sa Docstring.

``` python
from microbit import *
while not button_b.is_pressed() : # tant que B n'est pas pressé
    if button_a.is_pressed() : # si on presse le bouton A
        display.show(Image.HAPPY)
    else : # sinon
        display.show(Image.SAD)
display.clear()


```



Voici maintenant une série de petits défis. Vous n'êtes pas obligés de tous les réaliser. Saurez vous y faire face ?



**Exercice 1:**

>  Reprenons notre compte à rebours dont vous avez copié le code plus haut.
>
>  Modifier ce code façon à ce que presser sur le bouton _a_ déclenche le compte à rebours.
>
>  
>
>  _Remarque :_
>
>  Si vous utilisez le simulateur en ligne, vous pouvez cliquer sur les boutons quand même ...
>
>    
>
>  Une fois terminé, coller le code ci-dessous :
>
>  ```python
>  ```

**Exercice 2 :**



> Codez un algoritme tel que :
>
> * La  LED centrale s'allume au départ.
> * presser le bouton_a la déplace d'un cran à gauche puis termine l'algorithme.
> * presser le bouton_b la déplace d'un cran à droite puis termine l'algorithme.
>
>  Une fois terminé, coller le code ci-dessous :
>
>  ```python
>  ```
>  

**Exercice 3 (\*) :**

> Reprenez l'algorithme précédent en le modifiant de façon à pouvoir déplacer la LED vers la gauche ou la droite en pressant les boutons plusieurs fois de suite tant qu'on ne sort pas des _limites_ de l'écran. Si on _sort_ des limites de l'écran alors l'algorithme se termine en affichant "FIN".
>
> 
>
> Une fois terminé, coller le code ci-dessous :
>
> ```python
> `



### Capteur de lumière

Une LEDs peu fonctionner soit comme affichage, soit comme capteur : en les _inversant_ l'écran va devenir un point d'entrée et un capteur de lumière basique, permettant de détecter la luminosité ambiante. Si on utilise les LEDs comme capteurs, on ne peut s'en servir comme affichage. C'est l'un ou l'autre...



Lisez le code ci-dessous. Que fait-il ? Testez le en laissant plus ou moins de lumière passer sur l'écran.

```` python
from microbit import *
while True : # réalise une boucle sans condition de fin.
    if button_a.is_pressed() : # si on presse le bouton A
        print(display.read_light_level()) # le print permet un affichage dans la console
    if button_b.is_pressed() : # si on presse le bouton A
        break #on quitte la boucle

````

**Exercice : **

> Réalisez un algorithme qui après 2s, mesure la quantité de lumière perçu par les LEDs et affiche en scrollant sur l'écran le message `ALARME !!` si cette intensité dépasse un seuil à déterminer.
>
> Faites en sorte que le message d'alarme ne s'arrête qu'après une pression sur le bouton A
>
> Une fois terminé, copier le code ci-dessous :
>
> ```python
> ```
>
> 



### Capteur de température

Ce capteur permet à la carte de mesurer la température actuelle de l’appareil, en degrés Celsius.

Testez :

```` Python
from microbit import *
display.scroll(str(temperature()))

````

**Exercice :**

> Réaliser une fonction `alarme_temperature()`, qui, sur une boucle sans fin,  affiche la température sur l'écran toute les secondes et, si cette température dépasse 30°C, affiche "ALARME !!" .
>
> _Remarque :_
>
> * Si vous utilisez le simulateur en ligne, vous pouvez changer la température en sélectionnant la languette correspondante au dessus de la carte
>
>
> Une fois terminé, copier le code ci-dessous :
>
> ```python
> ```
>
> 



### Accéléromètre

La carte _micro:bit_ possède un accéléromètre qui mesure les déplacement selon 3 axes :

- _X_ de gauche à droite
- _Y_ d'avant en arrière
- _Z_ de haut en bas

Les méthodes  `get_x`, `get_y` et  `get_z` de `accelerometre` permettent de récupérer ces valeurs. Testez :

````python
from microbit import *
while not button_a.is_pressed() : # tant que B n'est pas pressé
    reading = accelerometer.get_x()
    if reading > 20:
        display.show("D")
    elif reading < -20:
        display.show("G")
    else:
        display.show("-")
display.clear()

````



_Remarque :_

Si vous utilisez le simulateur en ligne, vous pouvez bouger la carte en sélectionnant la languette `accelerometer` au dessus de la carte.



**Exercice 1:**

>  Modifier le code précédent en testant l'axe des _y_  de façon à afficher :
>
> * 'A' pour _Avant_ lorsque la carte est penchée en avant.
> * 'a' pour _arrière_ lorsque la carte est penchée en arrière
>
> Coller ensuite votre code ci-dessous :
>
> ```pyton
> ```
>
> 

**Exercice 2:**

>  Modifier le code précédent en testant l'axe des _z_  de façon à afficher :
>
> * 'H' pour _Haut_ lorsque la carte monte.
> * 'B' pour _Bas_ lorsque la carte descend
>
> Coller ensuite votre code ci-dessous :
>
> ```pyton
> 
> ```
>
> 

**Exercice 3 :**

> Réaliser un algorithme qui affiche le message "ALARME" dès qu'on déplace la carte.
>
> Coller ensuite le code ci-dessous :
>
> ```python
> ```
>
> 

**Exercice 4 :**

> Réaliser un algorithme qui affiche le message "ALARME" dès qu'on déplace la carte.
>
> 
>
> Coller ensuite le code ci-dessous :
>
> ```python
> ```
>
> 

### Boussole

La boussole détecte le champ magnétique de la Terre.

* La boussole  doit être étalonnée avant de pouvoir être utilisée. Cela se fait par un petit _jeu_ ou vous devez pencher et tourner la carte dans toutes les directions pour allumer toutes les LEDs de l'écran. Testons un peu cela grâce au programme ci-dessous :

  ````python
  from microbit import *
  display.scroll(str(compass.is_calibrated()))
  compass.calibrate()
  display.scroll(str(compass.is_calibrated()))
  ````

  Vous l'avez compris, la méthode `is_calibrated()` renvoie `True` (vrai) si le compas est calibré et `False` (ben oui, faux) sinon.

  

* Nous pouvons maintenant récupérer le _cap_ grâce à la méthode `heading()`de `compass` qui renvoie un entier entre 0 et 359 : 

	* 0 pour une direction vers le Nord

	* 90 pour une direction vers l'Est

	* 180 pour le Sud

	* 270 pour l'Ouest

		

		![rose des vents - wikipédia CC BY SA, auteur : Denelson](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Compass_Card_B%2BW.svg/600px-Compass_Card_B%2BW.svg.png)

	````python
	from microbit import *
	if not compass.is_calibrated() : # si le compas n'est pas calibré
	    compass.calibrate() # on le calibre
	while not button_a.is_pressed() : # Ca on connait ...
	    display.show('--' + str(compass.heading()) + '--') #on affiche le cap (converti en str) entre deus séries de '--'
	display.clear()
	        
	
	
	````

**Exercice 1 (\*): compas de relévement**

> Réaliser un algorithme qui, tant que le bouton A n'est pas pressé affiche la direction '-N-', '-NE-', '-E-', '-SE-', '-S-', '-SO-', '-O-' ou '-NO-' selon le cap obtenu.
>
> Coller ensuite votre code ci-dessous :
>
> ```pyton
> ```
>
> 





**Exercice 2 (\*\*): boussole graphique**

> Réaliser un algorithme afichant sur l'écran de la carte une série de 3 points démarrant au centre de l'écran et indiquant la direction du nord à 45° près. L'algorithme devra se terminer lorsque le bouton A sera pressé.
>
> 
>
> _Remarques :_
>
> * Vous pouvez reprendre le code de l'exercice précédent.
>
> * Vous aurez besoin de définir 8 images, une par direction possible.
>
>     



### Les broches



Nous avons vu que l'on pouvait utiliser les broches en sortie, comme actionneurs donc, vers un haut-parleur par exemple. Mais on peut également les utiliser en entrée avec un capteur supplémentaire par exemple.



Cette possibilité démultiplie les possibilités de la carte. Cependant, l'objectif n'est pas ici d'exploiter la micro:bit au maximum et nous ne nous étendrons pas là dessus. 



Vous pouvez toutefois vous documentez pour aller un peu plus loin en suivant [ce lien](https://archive.microbit.org/fr/guide/hardware/pins/)



___________

Par Christophe Mieszczak Licence CC BY SA

Images source : wikipédia , CC BY SA,  auteurs par ordre d'apparition : [Ugopedia](https://commons.wikimedia.org/w/index.php?title=User:Ugopedia&action=edit&redlink=1),  [Ravi Kotecha](https://commons.wikimedia.org/wiki/User:Ravi_tt22), [Denelson](https://commons.wikimedia.org/wiki/User:Denelson83)




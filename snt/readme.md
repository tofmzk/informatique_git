# SNT 

## Préambule

* Ce cours est écrit en [Markdown](https://fr.wikipedia.org/wiki/Markdown) , un format texte libre, léger, facilement utilisable et modifiable. Vous trouverez [ici](markdown/markdown_express.md) une petite formation au ce langage de balisage léger utilisé, par exemple, pour écrire Wikipédia himself. On discutera du choix de ce format que vous apprendrez à utiliser au fur et à mesure.
* Au lycée nous travaillons sur un environnement particulier :
  
    * [Utilisation du réseau](ent_reseau/reseau.md)
    * [l'**E**nvironnement **N**umérique de **T**ravail](ent_reseau/ent.md)
    

## Des chapitres aux liens étroits

* Notion transversale : Programmation en Python :

    * les bases du Python

        * [Python, Thonny et Capytale](bases_python/python_thonny.md)
        * [Les données](bases_python/les_donnees.md) →Code Capytale  `23cf-1891554`
        * [Les variables numériques](bases_python/variables_numeriques.md) →Code Capytale `56e2-1887257`
        * [Les chaines de caractères](bases_python/chaines_caracteres.md) →Code Capytale  `861d-1888810`
        * [Les entrées / sorties](bases_python/entrees_sorties.md) →Code Capytale `1865-1889063`
        * [Les structures conditionnelles](bases_python/conditions.md)  →Code Capytale `c934-1889693`
        * [Les boucles](bases_python/boucles.md)   →Code Capytale `1a13-1889947`
        * [Les fonctions](bases_python/fonctions.md)   →Code Capytale `7d2c-1892381`

    * [la balle qui rebondit](bases_python/la_balle.md) : pour appliquer tout cela en s'amusant.
        
          

* Informatique embarquée et objets connectés

    * [Cours : Interface homme_machine](informatique_embarquee/IHM.md)
    * [Cours : des 0 et des 1](informatique_embarquee/0_1.md)
    * [TP : La carte _micro:bit](informatique_embarquee/carte_microbit.md)
    * [Les objets connectés](informatique_embarquee/connectes.md)

* Internet 

    * Ce [TP Filius](internet/TP_Filius.md) permet de balayer les notions ci-dessous :

        * [Introduction](internet/intro.md)
        * [Les constituants d'un réseau](internet/constituants.md)
        * [Adresses IP](internet/adresses_IP.md)
    * [Un réseau sur papier](web/exo_papier.md)
    * [Carte et IP : par où passent les paquets ?](internet/carte_et_ip.md)

* Le web :

    * [Introduction et  historique](web/introduction_historique.md)
    * HTML et CSS : 
        * [Baliser](web/balises.md)
        * [Modifier une page web](web/modifier_une_page_web.md)
        * [Coder une page simple en HTML et découvrir les balises](web/nsi/nsi.md)
        * [FakeBook : coder une page en HTML et CSS](web/fakebook/fakebook.md)
        * [TP débranché](web/html_debranche.md)
    * Le modèle client/serveur :
        *  [TP filius](web/tp_filius) 
        * [Le principe du modèle](web/clients_et_serveurs.md)
        * [Acceder à un site web](web/acceder_a_un_site_web.md)
        * [Les cookies](web/cookies.md)
    * Les moteurs de recherche :
        * [Petit historique](web/moteurs_historique.md)
        * [Requêtes et recherches](web/requetes_recherches.md)
        * [PageRank : algorithme de classement des pages web](web/pagerank.md)    
    
* Les réseaux sociaux
    * [Introduction et historique](reseaux_sociaux/introduction_historique.md)
    * [Recherches et exposés autour des réseaux sociaux](reseaux_sociaux/recherches_autour_des_reseaux_sociaux.md)
    * [Le monde est petit](reseaux_sociaux/le_monde_est_petit.md)
    * [Les petits mondes](reseaux_sociaux/petits_mondes.md)
    * [Un peu de droit](reseaux_sociaux/droit.md)
    * [L'identité numérique](reseaux_sociaux/e_reputation.md)
    * [Le modèle économique](reseaux_sociaux/modele_economique.md)
    * [Graphes et réseaux sociaux](reseaux_sociaux/graphes_et_reseaux_sociaux.md)
    * [D'un ami à l'autre](reseaux_sociaux/d_un_ami_a_l_autre/d_un_ami_a_l_autre.md)  
    
* Les données structurées 

    * [Les données structurées  ](donnees_structurees/donnees_structurees.md)
    * [Les fichiers csv](donnees_structurees/csv.md)
    * [Manipuler des données : exercices](donnees_structurees/exercices.md)
    * [Anonyme, vraiment ?](donnees_structurees/anonymat/groupes_sanguins.md)
    * [Attrappez les tous : Pokémons, Panda et Python](donnees_structurees/pokemons.md) 
    * [Métadonnées : les EXIF](photographie/exif.md)

* Localisation

    * [Où suis-je ? Introduction](localisation/ou_suis_je.md)

    * [Les trames NMEA](localisation/trames_NMEA.md)

    * [Le facteur de Lorentz](localisation/le_facteur_de_Lorentz.md)

    * [Trouver sa route avec l'algorithme de Dijkstra](localisation/dijkstra.md)  

* La photographie numérique :

  * [Petit historique](photographie/historique.md)

  * [Principe de la photographie, avantages du numérique](photographie/principe de la photographie.md)

  * [Filtres et Python](photographie/filtres.md)

  


___________

Par Mieszczak Christophe - Licence CC - BY - SA

